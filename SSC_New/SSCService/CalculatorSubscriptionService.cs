﻿using System;
using System.ServiceProcess;
using System.IO;
using System.Configuration;
using System.Timers;
using CalculateDLL;
using Symbolics;
using System.Data;

namespace SSCService
{
    public partial class CalculatorNotificationService : ServiceBase
    {
        Timer timer = new Timer();
        public CalculatorNotificationService()
        {
           // InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            WriteToFile("Calculator subscription notification service started");
            ScheduleService();
        }

        protected override void OnStop()
        {
            WriteToFile("Calculator subscription notification service stopped");
        }

        public void GetSubscriptioDetails()
        {
            try
            {
                WriteToFile("-------------------Check all user's subscription details---------------------");
                Advisor objAdvisor = new Advisor();
                DataTable dtAdvisorDetails = objAdvisor.getSubscribedAdvisorDetails();

                foreach (DataRow row in dtAdvisorDetails.Rows)
                {
                    DateTime custSubscriptionDate = Convert.ToDateTime(row[Constants.UpdateDate].ToString());
                    string existingSubscribePlan = row[Constants.ChoosePlan].ToString();
                    string custEmail = row["Email"].ToString();
                    string custName = row["Firstname"].ToString();

                    switch (existingSubscribePlan)
                    {
                        case "Monthly":
                            DateTime notificationDate = custSubscriptionDate.AddDays(15);
                            
                            if (DateTime.Now == notificationDate)
                            {
                                WriteToFile("Monthly subscription expiration notification date: " + notificationDate);
                                WriteToFile("Send email to monthly subscribed advisor" + ", Advisor name = " + custName + " Advisor email = " + custEmail);
                                SendNotificationMail(custEmail, custName, existingSubscribePlan, custSubscriptionDate.ToString(), custSubscriptionDate.AddMonths(1).ToString());
                            }
                            break;

                        case "Annual":
                            DateTime custSubscriptionExpDate = custSubscriptionDate.AddYears(1);
                            notificationDate = custSubscriptionExpDate.AddDays(-15);
                           
                            if (DateTime.Now == notificationDate)
                            {
                                WriteToFile("Annual subscription expiration notification date: " + notificationDate);
                                WriteToFile("Send email to annual subscribed advisor" + ", Advisor name = " + custName + " Advisor email = " + custEmail);
                                SendNotificationMail(custEmail, custName, existingSubscribePlan, custSubscriptionDate.ToString(), custSubscriptionExpDate.ToString());
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                WriteToFile("Database Error" + ex.ToString());
            }
        }

        public void SendNotificationMail(string email, string advisorName, string subscriptionPlan, string subscriptionDate, string subscriptionExpirationDate)
        {
            string body = Constants.MailBodyForSubscription1 + advisorName;
            body += Constants.MailBodyForSubscription2;
            body += Constants.YourPlanDetail;
            body += Constants.SubscriptionType + subscriptionPlan + "<br>";
            body += Constants.SubscriptionDate + subscriptionDate + "<br>";
            body += Constants.ExpireOn + subscriptionExpirationDate.ToString() + "<br><br>";
            body += Constants.MailBodyForSubscription3;
            body += Constants.MailBodyForSubscription4;
            try
            {
                MailHelper.sendMail(email, Constants.MailSubjectForSubscription, body, string.Empty);
                WriteToFile("Email sent successfully: ");
            }
            catch (Exception ex)
            {
                WriteToFile("Error sending the email" + ex.ToString());
            }
        }

        public void ScheduleService()
        {
            try
            {
                string mode = ConfigurationManager.AppSettings["Mode"].ToUpper();
                WriteToFile("Notification service mode: " + mode + " {0}");

                //Set the Default Time.
                DateTime scheduledTime = DateTime.MinValue;

                int intervalMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["IntervalMinutes"]);
                scheduledTime = DateTime.Now.AddMinutes(intervalMinutes);
                TimeSpan timeSpan = scheduledTime.Subtract(DateTime.Now);

                string schedule = string.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
                WriteToFile("Notification service scheduled to run after: " + schedule + " {0}");

                //Get the difference in Minutes between the Scheduled and Current Time.
                int dueTime = Convert.ToInt32(timeSpan.TotalMilliseconds);
                timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
                timer.Interval = dueTime; //number in milisecinds
                timer.Enabled = true;
                GetSubscriptioDetails();
                WriteToFile("Next schedule time: " + TimeSpan.FromMilliseconds(dueTime).TotalHours + "Hrs");
            }
            catch (Exception ex)
            {
                WriteToFile("Notification service error on: {0} " + ex.Message + ex.StackTrace);

                //Stop the Windows Service.
                using (ServiceController serviceController = new ServiceController("SimpleService"))
                {
                    serviceController.Stop();
                }
            }
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            try
            {
                GetSubscriptioDetails();
            }
            catch (Exception ex)
            {
                WriteToFile("Notification service Error on: {0} " + ex.Message + ex.StackTrace);

                //Stop the Windows Service.
                using (ServiceController serviceController = new ServiceController("NotificationService"))
                {
                    serviceController.Stop();
                }
            }
        }

        private void WriteToFile(string text)
        {
            string path = "C:\\SSCServiceLogs\\ServiceLog" + DateTime.Today.ToString("yyyy-MM-dd") + ".txt";

            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(string.Format(text, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
                writer.Close();
            }
        }
    }
}
