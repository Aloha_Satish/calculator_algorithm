﻿using System;
using System.Collections.Generic;
using System.Data;

namespace CalculateDLL
{
    public class Users
    {

        #region Variables
        public const string A = "A";
        public const string C = "C";
        public const string I = "I";
        public const string UserEmail = "Email";
        public const string Password = "Password";
        public const string CustomerId = "CustomerID";
        #endregion Variables

        #region userInfo

        /// <summary>
        /// This method used to get Users Details by Email
        /// </summary>
        public DataTable getUserDetails(string pEmail)
        {
            try
            {
                List<QueryParameter> listEmail = new List<QueryParameter>();
                listEmail.Add(new QueryParameter("Email", pEmail));

                Database dbCustomer = new Database();
                DataTable dtUserID = dbCustomer.RunStoredProcedureSelectParameter("getUserDetails", listEmail);
                return dtUserID;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Users.cs - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// 06/21/2019 : Used to update subscription
        /// </summary>
        /// <param name="userID"></param>
        public static void UpdateSubscription(string userID)
        {
            try
            {
                List<QueryParameter> lstCustomeDetails = new List<QueryParameter>();
                lstCustomeDetails.Add(new QueryParameter("UserID", userID));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateUserSubscriptionDetails", lstCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// 06/21/2019 : Used to update subscription
        /// </summary>
        /// <param name="userID"></param>
        public void UpdateUserSubscriptionPlanByID(string userID, string subscriptionPlan)
        {
            try
            {
                List<QueryParameter> lstCustomeDetails = new List<QueryParameter>();
                lstCustomeDetails.Add(new QueryParameter("UserID", userID));
                lstCustomeDetails.Add(new QueryParameter("ChoosePlan", subscriptionPlan));
                Database dbCustomer = new Database();

                dbCustomer.RunStoredProcedureSelectParameter("updateUserSubscriptionPlanByID", lstCustomeDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customer Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method used to get Users Details by Email
        /// </summary>
        public void updateUserDetails(string pUserID, string pActiveFlag)
        {
            try
            {
                List<QueryParameter> listEmail = new List<QueryParameter>();
                listEmail.Add(new QueryParameter("UserId", pUserID));
                listEmail.Add(new QueryParameter("ActiveFlag", pActiveFlag));
                Database dbCustomer = new Database();
                DataTable dtUserID = dbCustomer.RunStoredProcedureSelectParameter("updateUserLoginDetails", listEmail);

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Users.cs - Error - " + ex.ToString());
            }
        }

        public DataTable getUserDetailsByUserID(string pUserID)
        {
            try
            {
                List<QueryParameter> listEmail = new List<QueryParameter>();
                listEmail.Add(new QueryParameter("UserID", pUserID));
                Database dbCustomer = new Database();
                DataTable dtUserDetailsByUserID = dbCustomer.RunStoredProcedureSelectParameter("getUserDetailsByUserID", listEmail);
                return dtUserDetailsByUserID;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Users.cs - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Used to send the email of account credentials of customer 
        /// </summary>
        /// <param name="strCustReference">Customer Reference Number</param>
        /// <param name="strEmail">Customer Email Address</param>
        /// <param name="strPassword">Customer Password</param>
        public void GetUserCredentials(string strCustReference, ref string strEmail, ref string strPassword)
        {
            try
            {
                Database dbCustomer = new Database();
                List<QueryParameter> listCustomerID = new List<QueryParameter>();
                string strRoleChar = strCustReference.Substring(0, 1);
                if (strRoleChar.Equals(A))
                {
                    QueryParameter paramCustomerID = new QueryParameter("AdvisorAdminID", strCustReference);
                    listCustomerID.Add(paramCustomerID);
                    DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getAdvisorDetailsByID", listCustomerID);
                    strEmail = dtDetails.Rows[0][UserEmail].ToString();
                    strPassword = Encryption.Decrypt(dtDetails.Rows[0][Password].ToString());
                }
                else if (strRoleChar.Equals(C))
                {
                    QueryParameter paramCustomerID = new QueryParameter(CustomerId, strCustReference);
                    listCustomerID.Add(paramCustomerID);
                    DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getCustomerDetails", listCustomerID);
                    strEmail = dtDetails.Rows[0][UserEmail].ToString();
                    strPassword = Encryption.Decrypt(dtDetails.Rows[0][Password].ToString());
                }
                else if (strRoleChar.Equals(I))
                {
                    QueryParameter paramCustomerID = new QueryParameter("InstitutionAdminID", strCustReference);
                    listCustomerID.Add(paramCustomerID);
                    DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getInstitutionalDetailsByID", listCustomerID);
                    strEmail = dtDetails.Rows[0][UserEmail].ToString();
                    strPassword = Encryption.Decrypt(dtDetails.Rows[0][Password].ToString());
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Users.cs - GetUserCredentials() - Error - " + ex.ToString());
            }
        }

        #endregion userInfo
    }
}
