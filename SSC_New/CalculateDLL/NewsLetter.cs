﻿using CalculateDLL.TO;
using System;
using System.Collections.Generic;
using System.Data;

namespace CalculateDLL
{
    /// <summary>
    /// Security Information for Divorced Customer
    /// </summary>
    public class NewsLetter
    {
        #region CRUD Operation

        /// <summary>
        /// Use for insert data into table Institutional and UserLoginDetails
        /// </summary>
        public void insertNewsLetterDetails(NewsLetterTO pObjNewsLetter)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("LetterBody", pObjNewsLetter.LetterBody));
                listNewsLetterDetails.Add(new QueryParameter("LetterSubject", pObjNewsLetter.LetterSubject));
                listNewsLetterDetails.Add(new QueryParameter("LetterUpdateBy", pObjNewsLetter.LetterUpdateBy));
                listNewsLetterDetails.Add(new QueryParameter("PublishDate", pObjNewsLetter.PublishDate));
                listNewsLetterDetails.Add(new QueryParameter("Periodic", pObjNewsLetter.Periodic));
                listNewsLetterDetails.Add(new QueryParameter("InstitutionList", pObjNewsLetter.InstitutionList));
                listNewsLetterDetails.Add(new QueryParameter("AdvisorsFlag", pObjNewsLetter.AdvisorsFlag));
                listNewsLetterDetails.Add(new QueryParameter("CustomersFlag", pObjNewsLetter.CustomersFlag));
                /* Initialize Database Object */
                Database dbInstitutional = new Database();
                dbInstitutional.RunStoredProcedureSelectParameter("insertPeriodicNewsLetter", listNewsLetterDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method return list of Institutional
        /// </summary>
        /// <param name="pCustomerID"></param>
        public DataTable getNewsLetterDetailsByID(string LetterUpdateBy = null)
        {
            try
            {
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getNewsLetterDetails", null);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Return sorted get NewsLetter Details
        /// </summary>
        public DataTable getNewsLetterDetailsFilter(string searchName = null, string updateDate = null, string searchText = null)
        {
            try
            {
                
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureWithoutParameter("getNewsLetterDetails");
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Update Institutional Details
        /// </summary>
        /// <param name="objCustomers"></param>
        public void updateNewsLetterInfo(NewsLetterTO pObjNewsLetter)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("LetterBody", pObjNewsLetter.LetterBody));
                listNewsLetterDetails.Add(new QueryParameter("LetterSubject", pObjNewsLetter.LetterSubject));
                listNewsLetterDetails.Add(new QueryParameter("LetterUpdateBy", pObjNewsLetter.LetterUpdateBy));
                listNewsLetterDetails.Add(new QueryParameter("NewsLetterId", pObjNewsLetter.NewsLetterId));
                listNewsLetterDetails.Add(new QueryParameter("PublishDate", pObjNewsLetter.PublishDate));
                listNewsLetterDetails.Add(new QueryParameter("Periodic", pObjNewsLetter.Periodic));
                listNewsLetterDetails.Add(new QueryParameter("InstitutionList", pObjNewsLetter.InstitutionList));
                listNewsLetterDetails.Add(new QueryParameter("AdvisorsFlag", pObjNewsLetter.AdvisorsFlag));
                listNewsLetterDetails.Add(new QueryParameter("CustomersFlag", pObjNewsLetter.CustomersFlag));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateNewsLetterDetails", listNewsLetterDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Advisor Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// Delete Institutional Details
        /// </summary>
        /// <param name="objCustomers"></param>
        public void deleteNewsLetterInfo(string NewsLetterID)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("NewsLetterId", NewsLetterID));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("deleteNewsLetterByID", listNewsLetterDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// Delete Institutional Details
        /// </summary>
        /// <param name="objCustomers"></param>
        public void updateNewsLetterStatus(string NewsLetterID,string Status)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("NewsLetterId", NewsLetterID));
                listNewsLetterDetails.Add(new QueryParameter("LetterStatus", Status));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("updateNewsLetterStatusByID", listNewsLetterDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// Use for insert data into table AttachmentFiles and Newsletter
        /// </summary>
        /// <param name="File"></param>
        public void insertAttachmentFileDetails(AttachmentFilesTO File)
        {
            try
            {
                Database dbAttachment = new Database();
                dbAttachment.RunStoredProcedureMultiParameterWithByte("insertAttachmentFileDetails", File.NewsLetterId, File.FileName, File.ContentType, File.Content,File.FileSize);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - insertAttachmentFileDetails - " + ex.ToString());
            }
        }
        public void updateAttachmentFileDetails(AttachmentFilesTO File)
        {
            try
            {
                Database dbAttachment = new Database();
                dbAttachment.RunStoredProcedureMultiParameterWithByte("updateAttachmentFileDetails", File.NewsLetterId, File.FileName, File.ContentType, File.Content, File.FileSize);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - insertAttachmentFileDetails - " + ex.ToString());
            }
        }
        /// <summary>
        /// This method return latest NewsletterID
        /// </summary>
        public DataTable getNewsLetterIdLatest(string LetterUpdateBy = null)
        {
            try
            {
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getNewsLetterIdLatest", null);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Id - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method return list of FileName and size
        /// </summary>
        /// <param name="pCustomerID"></param>
        public DataTable getAttachmentDetailsByNewsLetterId(string NewsLetterID)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("NewsLetterId", NewsLetterID));
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getAttachmentDetailsByNewLetterId", listNewsLetterDetails);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Delete Attachment
        /// </summary>
        /// <param name="objCustomers"></param>
        public void deleteAttachmentByNewsLetterId(string NewsLetterId, string FileName)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("NewsLetterId", NewsLetterId));
                listNewsLetterDetails.Add(new QueryParameter("FileName", FileName));
                Database dbCustomer = new Database();
                dbCustomer.RunStoredProcedureSelectParameter("deleteAttachmentByNewsLetterId", listNewsLetterDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("NewsLetter Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// This method return Institutional Mail
        /// </summary>
        /// <param name="pCustomerID"></param>
        public string getInstitutionMailByID(string InstituteId)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("InstitutionAdminID", InstituteId));
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getInstitutionalEmail", listNewsLetterDetails);
                return dtDetails.Rows[0]["Email"].ToString();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Institution Email - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method return Advisors Mail List
        /// </summary>
        /// <param name="pCustomerID"></param>
        public DataTable getAdvisorsMailByID(string InstituteId)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("InstitutionId", InstituteId));
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getAdvisorsEmailById", listNewsLetterDetails);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Advisors Email - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// This method return Customers Mail List
        /// </summary>
        /// <param name="pCustomerID"></param>
        public DataTable getCustomersMailByID(string AdvisorId)
        {
            try
            {
                List<QueryParameter> listNewsLetterDetails = new List<QueryParameter>();
                listNewsLetterDetails.Add(new QueryParameter("AdvisorId", AdvisorId));
                Database dbCustomer = new Database();
                DataTable dtDetails = dbCustomer.RunStoredProcedureSelectParameter("getCustomersEmailById", listNewsLetterDetails);
                return dtDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("Customers Email - Error - " + ex.ToString());
                return null;
            }
        }

        #endregion  
    }
}
