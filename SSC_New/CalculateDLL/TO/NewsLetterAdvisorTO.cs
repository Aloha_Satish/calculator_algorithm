﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDLL
{
    public class NewsLetterAdvisorTO
    {
        #region Properties
        public string LetterSubject { get; set; }
        public string LetterBody { get; set; }
        public string LetterUpdateDate { get; set; }
        public string LetterUpdateBy { get; set; }
        public string NewsLetterId { get; set; }
        public string PublishDate { get; set; }
        public string Periodic { get; set; }
        public string AdvisorList { get; set; }
        public string InstitutionID { get; set; }
        public string CustomersFlag { get; set; }
        #endregion
    }
}
