﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDLL
{
    public class NewsLetterTO
    {
        #region Properties
        public string LetterSubject { get; set; }
        public string LetterBody { get; set; }
        public string LetterUpdateDate { get; set; }
        public string LetterUpdateBy { get; set; }
        public string NewsLetterId { get; set; }
        public string PublishDate { get; set; }
        public string Periodic { get; set; }
        public string InstitutionList { get; set; }
        public string AdvisorsFlag { get; set; }
        public string CustomersFlag { get; set; }
        #endregion
    }
}
