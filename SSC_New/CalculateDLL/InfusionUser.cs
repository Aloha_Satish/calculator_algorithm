﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDLL
{
    public class InfusionUser
    {

        /// <summary>
        /// Infusion User Name
        /// </summary>
        private string _Name;
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        /// <summary>
        /// Infusion User Last Name
        /// </summary>
        private string _LastName = string.Empty;
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }

        /// <summary>
        /// Infusion Email
        /// </summary>
        private string _Email;
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }


        /// <summary>
        /// Infusion State
        /// </summary>
        private string _State;
        public string State
        {
            get { return _State; }
            set { _State = value; }
        }

        /// <summary>
        /// Infusion State
        /// </summary>
        private string _City;
        public string City
        {
            get { return _City; }
            set { _City = value; }
        }


    }
}
