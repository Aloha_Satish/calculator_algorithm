﻿using System;
using System.Collections.Generic;
using System.Data;

namespace CalculateDLL
{
    public class ModuleData
    {
        /// <summary>
        /// To get description from table
        /// </summary>
        /// <param name="ModuleName"></param>
        /// <returns></returns>
        public DataTable getModuleDataDescription(string ModuleName)
        {
            try
            {
                List<QueryParameter> listModuleDescription = new List<QueryParameter>();
                listModuleDescription.Add(new QueryParameter("ModuleName", ModuleName));
                Database dbModuleData = new Database();
                DataTable dtDescription = dbModuleData.RunStoredProcedureSelectParameter("getModuleDataDescription", listModuleDescription);
                return dtDescription;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("ModuleData Details - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Update description of the ModuleData 
        /// </summary>
        /// <param name="ModuleName"></param>
        /// <param name="Description"></param>
        public void updateModuleData(string ModuleName, string Description)
        {
            try
            {
                List<QueryParameter> listModuleData = new List<QueryParameter>();
                listModuleData.Add(new QueryParameter("ModuleName", ModuleName));
                listModuleData.Add(new QueryParameter("Description", Description));
                Database dbModuleData = new Database();
                dbModuleData.RunStoredProcedureSelectParameter("updateModuleData", listModuleData);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("ModuleData Details - Error - " + ex.ToString());
            }
        }

        /// <summary>
        /// to get module name list from table
        /// </summary>
        /// <returns></returns>
        public DataTable getModuleName()
        {
            try
            {
                Database dbModuleData = new Database();
                DataTable dtModuleName = dbModuleData.RunStoredProcedureSelectParameter("getModuleNamelist",null);
                return dtModuleName;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("ModuleName List - Error - " + ex.ToString());
                return null;
            }
        }

    }
}
