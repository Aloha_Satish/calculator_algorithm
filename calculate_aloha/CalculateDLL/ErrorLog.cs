using System;
using System.IO;

namespace CalculateDLL
{
    public class ErrorLog : Exception
    {
        #region LogException

        /// <summary>
        /// Log the Error in Log Files
        /// </summary>
        /// <param name="errorMessage">ErrorMessage</param>
        public static void WriteError(string errorMessage)
        {
            try
            {
                string path = @"E:\CalculateErrorLog" + DateTime.Today.ToString("yyyy-MM-dd") + ".txt";
                if (!File.Exists(path))
                {
                    File.Create(path).Close();
                }
                using (StreamWriter w = File.AppendText(path))
                {
                    w.WriteLine("{0} : {1}", DateTime.Now.ToString(), errorMessage);
                    w.Flush();
                    w.Close();
                }
            }
            catch { }
        }

        #endregion LogException
    }
}
