﻿using System;
using System.Collections.Generic;
using System.Data;

namespace CalculateDLL
{
    /// <summary>
    /// Security Information for Divorced Customer
    /// </summary>
    public class SecurityInfoSingle
    {
        
        #region Properties

        public string CustomerID
        {
            get; 
            set;
        }

        public string BirthDate 
        { 
            get; 
            set; 
        }

        public string RetAgeBenefit
        {
            get;
            set;
        }

        public string Cola
        {
            get;
            set;
        }   
     
        #endregion Properties        
    }
}
