﻿using System;
using System.Net.Mail;
using System.Text;
using System.IO;
using System.Net.Mime;
using System.Data;

namespace CalculateDLL
{
    /// <summary>
    /// Mail Helper Class Support Mail Methods.
    /// </summary>
    public class MailHelper
    {
        /// <summary>
        ///  Used to send Email
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <returns> Boolean</returns>        
        public static bool sendMail(string toAddress, string subject, string body, string cid)
        {
            try
            {
                /* Initialize SMTP for sending Email */
                using (var smtp = new System.Net.Mail.SmtpClient())
                {
                    MailMessage mail = new MailMessage();
                    /* Add Subject to mail object */
                    mail.Subject = subject;
                    /* Add Body to mail object */
                    mail.Body = body;
                    /* Add To Recepient Address where we need to send mail */
                    mail.To.Add(new MailAddress(toAddress));
                    mail.IsBodyHtml = true;

                    Customers customer = new Customers();
                    if (!string.IsNullOrWhiteSpace(cid))
                    {
                        DataTable dtPdfDetails = customer.getpdfFile(cid);
                        if (dtPdfDetails.Rows.Count > 0)
                        {
                            Byte[] data = (Byte[])dtPdfDetails.Rows[0]["PdfContent"];
                            using (MemoryStream stream = new MemoryStream(data))
                            {
                                var ct = new ContentType();
                                ct.MediaType = MediaTypeNames.Application.Pdf;
                                ct.Name = "test.pdf";
                                mail.Attachments.Add(new Attachment(stream, "test.pdf", MediaTypeNames.Application.Pdf));
                                smtp.Send(mail);
                            }
                        }
                    }    
                    smtp.Send(mail); 
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("checkSendingDetails - Error - " + ex.ToString());
            }
            return true;
        }
    }
}