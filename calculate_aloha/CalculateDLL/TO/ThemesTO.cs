﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateDLL.TO
{
    public class ThemesTO
    {
        #region Properties
        public string ThemeId { get; set; }
        public string InstitutionId { get; set; }
        public string InstitutionName { get; set; }
        public byte[] InstitutionLogo { get; set; }
        public string BackGroundHeaderColor { get; set; }
        public string BackGroundFooterColor { get; set; }
        public string BackGroundBodyColor { get; set; }
        public string DomainName { get; set; }
        public string URLString { get; set; }
        #endregion
    }
}
