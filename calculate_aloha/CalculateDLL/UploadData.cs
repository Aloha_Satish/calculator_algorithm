﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Reflection;
namespace CalculateDLL
{
    public class UploadData
    {
        /// <summary>
        /// Class costructor
        /// </summary>
        public UploadData()
        {

        }

        #region Methods
        /// <summary>
        /// Used to upload excel file to view records on gridview
        /// </summary>
        /// <param name="strPath">Excel file path</param>
        /// <returns>datatable</returns>
        public DataTable UploadExcel(string strPath)
        {
            string conString = string.Empty;
            string extension = Path.GetExtension(strPath);
            DataTable dtExcelData = new DataTable();
            try
            {
                conString = ConfigurationManager.ConnectionStrings["Excel07+ConString"].ConnectionString;
                //switch (extension)
                //{
                //    case ".xls": //Excel 97-03
                //        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                //        break;
                //    case ".xlsx": //Excel 07 or higher
                //        conString = ConfigurationManager.ConnectionStrings["Excel07+ConString"].ConnectionString;
                //        break;

                //}
                conString = string.Format(conString, strPath);
                using (OleDbConnection excel_con = new OleDbConnection(conString))
                {
                    excel_con.Open();
                    string sheet1 = excel_con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                    using (OleDbDataAdapter oda = new OleDbDataAdapter("SELECT * FROM [" + sheet1 + "]", excel_con))
                    {
                        oda.Fill(dtExcelData);
                    }
                    excel_con.Close();
                    dtExcelData.Columns[0].ColumnName = "Firstname";
                    dtExcelData.Columns[1].ColumnName = "Lastname";
                    dtExcelData.Columns[2].ColumnName = "Email";
                    dtExcelData.Columns[3].ColumnName = "City";
                    dtExcelData.Columns[4].ColumnName = "State";
                    dtExcelData.Columns[5].ColumnName = "Dealer";
                    dtExcelData.AcceptChanges();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", "Upload Data - Error - ", MethodBase.GetCurrentMethod(), ex.ToString()));
            }
            return dtExcelData;
        }

        /// <summary>
        /// Used to retrive data from excel file
        /// </summary>
        /// <param name="strPath">Excel file path in string</param>
        /// <returns>Datatable</returns>
        public DataTable RetriveDataFromCSV(string strPath)
        {
            DataTable dtExcelData = new DataTable();
            try
            {
                dtExcelData.Columns.AddRange(new DataColumn[6] { 
                new DataColumn("Firstname", typeof(string)),
                new DataColumn("Lastname", typeof(string)),
                new DataColumn("Email",typeof(string)),
                new DataColumn("City",typeof(string)),
                new DataColumn("State",typeof(string)),
                new DataColumn("Dealer",typeof(string))});

                using (TextFieldParser parser = new TextFieldParser(strPath))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    while (!parser.EndOfData)
                    {
                        DataRow newDR = dtExcelData.NewRow();
                        //Processing row
                        string[] fields = parser.ReadFields();
                        for (int index = 0; index < fields.Length; index++)
                            newDR[index] = fields[index];
                        dtExcelData.Rows.Add(newDR);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", "Upload Data - Error - ", MethodBase.GetCurrentMethod(), ex.ToString()));
            }
            return dtExcelData;
        }

        /// <summary>
        /// Used to validate selected file based on its extension, it should xls,xlsx or csv
        /// </summary>
        /// <param name="filePath">File path string</param>
        /// <returns>bool</returns>
        public bool ValidateFile(string filePath)
        {
            bool isValidFile = false;
            try
            {
                string[] validFileTypes = { "xls", "xlsx", "csv" };
                string ext = Path.GetExtension(filePath);
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", "Upload Data - Error - ", MethodBase.GetCurrentMethod(), ex.ToString()));
            }
            return isValidFile;
        }
        #endregion Methods
    }
}
