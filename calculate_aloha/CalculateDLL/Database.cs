﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for database
/// </summary>
namespace CalculateDLL
{
    public class Database
    {
        #region Declaration

        private SqlConnection m_Conn;
        private SqlTransaction m_Trans = null;
        private bool m_bNeedsClose = false;
        private static int m_serialNo = 0;
        
        #endregion Declaration

        #region Properties

        private static int serialNo
        {
            get
            {
                m_serialNo++;
                return m_serialNo;
            }
        }

        #endregion Properties

        #region Constructor

        public Database()
        {
            m_Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CalculateDataCon"].ConnectionString);
        }

        #endregion Constructor

        #region Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pQuery"></param>
        /// <returns></returns>
        public object ExecuteScaler(string pQuery)
        {
            SqlCommand cmd = new SqlCommand(pQuery, m_Conn);
            cmd.Transaction = m_Trans;
            object objReturn = null;
            bool bNeedsClose = false;
            try
            {
                if (m_Conn.State != ConnectionState.Open)
                {
                    m_Conn.Open();
                    bNeedsClose = true;
                }
                objReturn = cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("DataBase.cs - Error - " + ex.ToString());
            }
            finally
            {
                if ((m_Conn.State != ConnectionState.Closed) && bNeedsClose)
                    m_Conn.Close();
            }
            return objReturn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pQuery"></param>
        /// <returns></returns>
        public SqlDataReader ExecuteReader(string pQuery)
        {
            SqlCommand cmd = new SqlCommand(pQuery, m_Conn);
            cmd.Transaction = m_Trans;
            bool bNeedsClose = false;
            try
            {
                if (m_Conn.State != ConnectionState.Open)
                {
                    m_Conn.Open();
                    bNeedsClose = true;
                }
                return cmd.ExecuteReader(CommandBehavior.Default);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("DataBase.cs - Error - " + ex.ToString());
                return null;
            }
            finally
            {
                if ((m_Conn.State != ConnectionState.Closed) && bNeedsClose)
                    m_Conn.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pQuery"></param>
        /// <param name="pTableName"></param>
        /// <returns></returns>
        public DataTable ExecuteDataTable(string pQuery, string pTableName)
        {
            SqlDataAdapter daCmd = new SqlDataAdapter(pQuery, m_Conn);
            DataTable dtReturn = new DataTable(pTableName);
           
            try
            {
                daCmd.Fill(dtReturn);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("DataBase.cs - Error - " + ex.ToString());
            }
            
            return dtReturn;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pQuery"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string pQuery)
        {
            SqlCommand cmd = new SqlCommand(pQuery, m_Conn);
            cmd.Transaction = m_Trans;

            int intReturn = -1;
            bool bNeedsClose = false;
            try
            {
                if (m_Conn.State != ConnectionState.Open)
                {
                    m_Conn.Open();
                    bNeedsClose = true;
                }
                intReturn = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("DataBase.cs - Error - " + ex.ToString());
            }
            finally
            {
                if ((m_Conn.State != ConnectionState.Closed) && bNeedsClose)
                    m_Conn.Close();
            }
            return intReturn;
        }

        /// <summary>
        /// RunStoredProcedure for multiple parameters
        /// </summary>
        /// <param name="lstparameters">parameter list</param>
        /// <returns></returns>
        public void RunStoredProcedureParameter(string sProcName, List<QueryParameter> lstparameters)
        {
            SqlCommand cmd = new SqlCommand(sProcName, m_Conn);
            cmd.Transaction = m_Trans;

            int intReturn = -1;
            bool bNeedsClose = false;
            
            try
            {
                if (m_Conn.State != ConnectionState.Open)
                {
                    m_Conn.Open();
                    bNeedsClose = true;
                }

                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paraX = null;

                // Adding parameter to store procedure
                foreach (QueryParameter param in lstparameters)
                {
                    paraX = new SqlParameter();
                    paraX.ParameterName = param.ParameterName;
                    paraX.Value = param.ParameterValue;
                    cmd.Parameters.Add(paraX);
                }

                // Executing store procedure
                intReturn = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("DataBase.cs - Error - " + ex.ToString());
            }
            finally
            {
                if ((m_Conn.State != ConnectionState.Closed) && bNeedsClose)
                    m_Conn.Close();
            }
        }

        /// <summary>
        /// RunStoredProcedure for multiple parameters
        /// </summary>
        /// <param name="lstparameters">parameter list</param>
        /// <returns></returns>
        public void RunStoredProcedureParameterWithByte(string sProcName, string CustomerID, string PdfName, string ContentType, byte[] pdfContent)
        {
            SqlCommand cmd = new SqlCommand(sProcName, m_Conn);
            cmd.Transaction = m_Trans;

            int intReturn = -1;
            bool bNeedsClose = false;

            try
            {
                if (m_Conn.State != ConnectionState.Open)
                {
                    m_Conn.Open();
                    bNeedsClose = true;
                }
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CustomerID", CustomerID);
                cmd.Parameters.AddWithValue("@PdfName", PdfName);
                cmd.Parameters.AddWithValue("@ContentType", ContentType);
                cmd.Parameters.AddWithValue("@PdfContent", pdfContent);
               
                // Executing store procedure
                intReturn = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("DataBase.cs - Error - " + ex.ToString());
            }
            finally
            {
                if ((m_Conn.State != ConnectionState.Closed) && bNeedsClose)
                    m_Conn.Close();
            }
        }

        /// <summary>
        /// RunStoredProcedure for multiple parameters
        /// </summary>
        /// <param name="lstparameters">parameter list</param>
        /// <returns></returns>
        public void RunStoredProcedureMultiParameterWithByte(string sProcName, string NewsLetterId, string FileName, string ContentType, byte[] Content,string FileSize)
        {
            SqlCommand cmd = new SqlCommand(sProcName, m_Conn);
            cmd.Transaction = m_Trans;

            int intReturn = -1;
            bool bNeedsClose = false;

            try
            {
                if (m_Conn.State != ConnectionState.Open)
                {
                    m_Conn.Open();
                    bNeedsClose = true;
                }
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@NewsLetterId", NewsLetterId);
                cmd.Parameters.AddWithValue("@FileName", FileName);
                cmd.Parameters.AddWithValue("@ContentType", ContentType);
                cmd.Parameters.AddWithValue("@Content", Content);
                cmd.Parameters.AddWithValue("@FileSize", FileSize);
                // Executing store procedure
                intReturn = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("DataBase.cs - Error - " + ex.ToString());
            }
            finally
            {
                if ((m_Conn.State != ConnectionState.Closed) && bNeedsClose)
                    m_Conn.Close();
            }
        }

        /// <summary>
        /// RunStoredProcedure for multiple parameters
        /// </summary>
        /// <param name="lstparameters">parameter list</param>
        /// <returns></returns>
        public void RunStoredProcedureMultiParameterWithByteForTheme(string sProcName, string InstitutionId, string InstitutionName, string BackGroundHeaderColor, string BackGroundFooterColor, string BackGroundBodyColor, byte[] InstitutionLogo)
        {
            SqlCommand cmd = new SqlCommand(sProcName, m_Conn);
            cmd.Transaction = m_Trans;

            int intReturn = -1;
            bool bNeedsClose = false;

            try
            {
                if (m_Conn.State != ConnectionState.Open)
                {
                    m_Conn.Open();
                    bNeedsClose = true;
                }
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@InstitutionId", InstitutionId);
                cmd.Parameters.AddWithValue("@InstitutionName", InstitutionName);
                cmd.Parameters.AddWithValue("@BackGroundHeaderColor", BackGroundHeaderColor);
                cmd.Parameters.AddWithValue("@BackGroundFooterColor", BackGroundFooterColor);
                cmd.Parameters.AddWithValue("@BackGroundBodyColor", BackGroundBodyColor);
                cmd.Parameters.AddWithValue("@InstitutionLogo", InstitutionLogo);
                // Executing store procedure
                intReturn = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("DataBase.cs - Error - " + ex.ToString());
            }
            finally
            {
                if ((m_Conn.State != ConnectionState.Closed) && bNeedsClose)
                    m_Conn.Close();
            }
        }

        public void RunStoredProcedureWithByteForTheme(string sProcName, string DomainName, byte[] InstitutionLogo,string URLString )
        {
            SqlCommand cmd = new SqlCommand(sProcName, m_Conn);
            cmd.Transaction = m_Trans;

            int intReturn = -1;
            bool bNeedsClose = false;

            try
            {
                if (m_Conn.State != ConnectionState.Open)
                {
                    m_Conn.Open();
                    bNeedsClose = true;
                }
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DomainName", DomainName);
                cmd.Parameters.AddWithValue("@BannerImage", InstitutionLogo);
                cmd.Parameters.AddWithValue("@URLString", URLString);
                // Executing store procedure
                intReturn = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("DataBase.cs - Error - " + ex.ToString());
            }
            finally
            {
                if ((m_Conn.State != ConnectionState.Closed) && bNeedsClose)
                    m_Conn.Close();
            }
        }
        /// <summary>
        /// RunStoredProcedure for multiple parameters
        /// </summary>
        /// <param name="lstparameters">parameter list</param>
        /// <returns></returns>
        public void RunStoredProcedureMultiParameterWithByteForUpdateTheme(string sProcName, string ThemeId, string InstitutionId, string InstitutionName, string BackGroundHeaderColor, string BackGroundFooterColor, string BackGroundBodyColor, byte[] InstitutionLogo)
        {
            SqlCommand cmd = new SqlCommand(sProcName, m_Conn);
            cmd.Transaction = m_Trans;

            int intReturn = -1;
            bool bNeedsClose = false;

            try
            {
                if (m_Conn.State != ConnectionState.Open)
                {
                    m_Conn.Open();
                    bNeedsClose = true;
                }
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ThemeId", ThemeId);
                cmd.Parameters.AddWithValue("@InstitutionId", InstitutionId);
                cmd.Parameters.AddWithValue("@InstitutionName", InstitutionName);
                cmd.Parameters.AddWithValue("@BackGroundHeaderColor", BackGroundHeaderColor);
                cmd.Parameters.AddWithValue("@BackGroundFooterColor", BackGroundFooterColor);
                cmd.Parameters.AddWithValue("@BackGroundBodyColor", BackGroundBodyColor);
                cmd.Parameters.AddWithValue("@InstitutionLogo", InstitutionLogo);
                // Executing store procedure
                intReturn = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("DataBase.cs - Error - " + ex.ToString());
            }
            finally
            {
                if ((m_Conn.State != ConnectionState.Closed) && bNeedsClose)
                    m_Conn.Close();
            }
        }

        /// <summary>
        /// RunStoredProcedure for multiple parameters
        /// </summary>
        /// <param name="lstparameters">parameter list</param>
        /// <returns></returns>
        public void RunStoredProcedureMultiParameterWithByteForUpdateThemeWithoutLogo(string sProcName, string ThemeId, string InstitutionId, string InstitutionName, string BackGroundHeaderColor, string BackGroundFooterColor, string BackGroundBodyColor)
        {
            SqlCommand cmd = new SqlCommand(sProcName, m_Conn);
            cmd.Transaction = m_Trans;

            int intReturn = -1;
            bool bNeedsClose = false;

            try
            {
                if (m_Conn.State != ConnectionState.Open)
                {
                    m_Conn.Open();
                    bNeedsClose = true;
                }
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ThemeId", ThemeId);
                cmd.Parameters.AddWithValue("@InstitutionId", InstitutionId);
                cmd.Parameters.AddWithValue("@InstitutionName", InstitutionName);
                cmd.Parameters.AddWithValue("@BackGroundHeaderColor", BackGroundHeaderColor);
                cmd.Parameters.AddWithValue("@BackGroundFooterColor", BackGroundFooterColor);
                cmd.Parameters.AddWithValue("@BackGroundBodyColor", BackGroundBodyColor);
                // Executing store procedure
                intReturn = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("DataBase.cs - Error - " + ex.ToString());
            }
            finally
            {
                if ((m_Conn.State != ConnectionState.Closed) && bNeedsClose)
                    m_Conn.Close();
            }
        }

        /// <summary>
        /// RunStoredProcedure for multiple parameters
        /// </summary>
        /// <param name="lstparameters">parameter list</param>
        /// <returns></returns>
        public DataTable RunStoredProcedureSelectParameter(string sProcName, List<QueryParameter> lstparameters)
        {
            SqlCommand cmd = new SqlCommand(sProcName, m_Conn);

            cmd.Transaction = m_Trans;
            SqlDataAdapter dataAdapter;
            DataSet dsReturn = new DataSet();

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter paraX = null;

                if (lstparameters != null)
                {
                    // Adding parameter to store procedure
                    foreach (QueryParameter param in lstparameters)
                    {
                        paraX = new SqlParameter();
                        paraX.ParameterName = param.ParameterName;
                        paraX.Value = param.ParameterValue;
                        cmd.Parameters.Add(paraX);
                    }
                }

                dataAdapter = new SqlDataAdapter(cmd);
                dataAdapter.Fill(dsReturn);
                
                if (dsReturn.Tables.Count > 0)
                    return dsReturn.Tables[0];
               
                return null;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("DataBase.cs - Error - " + ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// RunStoredProcedure for Without parameters
        /// </summary>
        /// <param name="lstparameters">parameter list</param>
        /// <returns></returns>
        public DataTable RunStoredProcedureWithoutParameter(string sProcName)
        {
            SqlCommand cmd = new SqlCommand(sProcName, m_Conn);

            cmd.Transaction = m_Trans;
            SqlDataAdapter dataAdapter;
            DataSet dsReturn = new DataSet();

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                
                dataAdapter = new SqlDataAdapter(cmd);
                dataAdapter.Fill(dsReturn);

                if (dsReturn.Tables.Count > 0)
                    return dsReturn.Tables[0];

                return null;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError("DataBase.cs - Error - " + ex.ToString());
                return null;
            }
        }
        
        public void StartTransaction()
        {
            try
            {
                if (m_Trans == null)
                {
                    if (m_Conn.State != ConnectionState.Open)
                    {
                        m_Conn.Open();
                        m_bNeedsClose = true;
                    }
                    m_Trans = m_Conn.BeginTransaction();
                }
            }
            catch (Exception ex)
            {
               ErrorLog.WriteError("DataBase.cs - Error - " + ex.ToString());
            }
        }

        public void CommitTransaction()
        {
            try
            {
                if (m_Trans != null)
                {
                    m_Trans.Commit();
                    m_Trans.Dispose();
                    m_Trans = null;

                    if ((m_Conn.State != ConnectionState.Closed) && m_bNeedsClose)
                        m_Conn.Close();
                }
            }
            catch (Exception ex)
            {
             ErrorLog.WriteError("DataBase.cs - Error - " + ex.ToString());
            }
        }

        public void RollbackTransaction()
        {
            try
            {
                if (m_Trans != null)
                {
                    m_Trans.Rollback();
                    m_Trans.Dispose();
                    m_Trans = null;

                    if ((m_Conn.State != ConnectionState.Closed) && m_bNeedsClose)
                        m_Conn.Close();
                }
            }
            catch (Exception ex)
            {
               ErrorLog.WriteError("DataBase.cs - Error - " + ex.ToString());
            }
        }

        public static string GenerateGID(string pSeed)
        {
            return pSeed.Substring(0, 1) + DateTime.Now.ToString("yyyyMMddHHmmssffff") + serialNo.ToString("000");
        }

        #endregion Events
    }

    public class QueryParameter
    {
        public QueryParameter(String Name, String ParamValue)
        {
            m_strParameterName = Name;
            m_strParameterValue = ParamValue;
        }

        private String m_strParameterName;
        private String m_strParameterValue;

        /// <summary>
        /// set and get the parameter name
        /// </summary>
        public String ParameterName
        {
            get { return m_strParameterName; }
            set { m_strParameterName = value; }
        }

        /// <summary>
        /// set and get the parameter value
        /// </summary>
        public String ParameterValue
        {
            get { return m_strParameterValue; }
            set { m_strParameterValue = value; }
        }
    }
}