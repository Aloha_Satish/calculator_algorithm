﻿using CalculateDLL;
using CalculateDLL.TO;
using Symbolics;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculate
{
    public partial class MonthlyAdvisor : System.Web.UI.Page
    {
        /// <summary>
        /// Monthly Advisor Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Globals.Instance.strAdvisorPlan = Constants.MonthlyPlan;
        }

        /// <summary>
        /// Get Started Button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GetStarted(object sender, EventArgs e)
        {
            try
            {
                //Add new advisor details
                AddAdvisor();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                throw;
            }
        }

        /// <summary>
        /// code for creating a new advisor
        /// </summary>
        private void AddAdvisor()
        {
            try
            {
                string strQueryStringPlan = string.Empty;
                /* Create object of Customer Class */
                Customers objCustomer = new Customers();
                objCustomer.Email = txtEmailAddress.Text;
                /* Check if email address already exists or not */
                if (!objCustomer.isCustomerExist(objCustomer))
                {
                    /* initialize Instance of classes */
                    AdvisorTO objAdvisorTO = new AdvisorTO();
                    Advisor objAdvisor = new Advisor();
                    TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                    #region set Insert Parameters
                    /* Assign textboxes content into institutional variables */
                    objAdvisorTO.AdvisorID = Database.GenerateGID(Constants.A);
                    objAdvisorTO.InstitutionAdminID = "DI1";
                    objAdvisorTO.AdvisorName = textInfo.ToTitleCase(txtFirstName.Text);
                    objAdvisorTO.Role = Constants.Advisor;
                    objAdvisorTO.Firstname = textInfo.ToTitleCase(txtFirstName.Text);
                    objAdvisorTO.Lastname = string.Empty;
                    objAdvisorTO.Email = txtEmailAddress.Text;
                    objAdvisorTO.Password = Encryption.Encrypt(txtPassword.Text);
                    objAdvisorTO.isInstitutionSubscription = Constants.FlagNo;
                    objAdvisorTO.DelFlag = Constants.FlagNo;
                    objAdvisorTO.PasswordResetFlag = Constants.FlagNo;
                    objAdvisorTO.ChoosePlan = Globals.Instance.strAdvisorPlan;
                    objAdvisorTO.isSubscription = Constants.FlagNo;
                    objAdvisorTO.State = string.Empty;
                    objAdvisorTO.Dealer = string.Empty;
                    objAdvisorTO.City = string.Empty;
                    #endregion set Insert Parameters

                    //InfusionSoft
                    Session["UserEmail"] = objAdvisorTO.Email;

                    /* Insert institution details into Database */
                    objAdvisor.insertAdvisorDetails(objAdvisorTO);
                    clsFlag.Flag = false;
                    /* Reload gridview with updated data */
                    lblUserCreate.Visible = true;
                    lblUserExist.Visible = false;
                    lblUserCreate.Text = Constants.AdvisorAdded;
                    ClearFields();
                    Session[Constants.SessionRole] = Symbolics.Constants.Advisor;
                    Session[Constants.SessionUserId] = objAdvisorTO.AdvisorID;
                    Session[Constants.SessionAdminId] = objAdvisorTO.AdvisorID;

                    //Previously navigating to pop up to select monthly or annual plan
                    //Response.Redirect(Constants.RedirectWelcomeAdmin, false);
                    //Changed to naviage on chargify for payment. changed on 05 oct 2016
                    MailSentPopUp.Show();


                    Session[Constants.SessionRegistered] = Constants.ColaStatusT;
                }
                else
                {
                    lblUserExist.Text = Constants.customerExists;
                    lblUserExist.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorAdvisorRegister + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }


        /// <summary>
        /// Used to navigate on particular plan on the basis of querystring and session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RedirectToChargify(object sender, EventArgs e)
        {
            try
            {
                string strQueryStringPlan = string.Empty;
                if (Request.QueryString[Constants.AdvisorPlan] != null)
                    strQueryStringPlan = Request.QueryString[Constants.AdvisorPlan].ToString();
                if (!string.IsNullOrEmpty(strQueryStringPlan))
                {
                    if (strQueryStringPlan.ToLower().Equals(Constants.MonthlyPlan.ToLower()))
                        Response.Redirect(Constants.MonthlyUseInfusion, true);
                    else if (strQueryStringPlan.ToLower().Equals(Constants.AnnualPlan.ToLower()))
                        Response.Redirect(Constants.AnnualUseInfusion, true);
                }
                else
                {
                    if (Globals.Instance.strAdvisorPlan.Equals(Constants.MonthlyPlan))
                        Response.Redirect(Constants.MonthlyUseInfusion, true);
                    else
                        Response.Redirect(Constants.AnnualUseInfusion, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRegister + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }


        /// <summary>
        /// code to clear the text from the feilds
        /// </summary>
        public void ClearFields()
        {
            try
            {
                txtEmailAddress.Text = string.Empty;
                txtFirstName.Text = string.Empty;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorAdvisorRegister + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }
    }
}