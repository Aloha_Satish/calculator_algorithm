﻿<%@ Page Title="Registration Successful" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="CustomerSuccess.aspx.cs" Inherits="Calculate.CustomerSuccess" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <br />
    <%--<h1 style="text-align: center; color: green">Congratulations ! You have been successfully subscribed to Social Security Calculator.</h1>--%>
    <center>
    <asp:Label runat="server" Font-Size="X-Large" ForeColor="Green" ID="lblSuccessMsg" Text="Congratulations ! You have been successfully subscribed to the Paid to Wait Calculator."></asp:Label>
        </center>
    <br />
    <div style="text-align: center; height: 340px; font-size: large">
        <%--<span>To explore your account please click below link. </span>--%>
        <br />
        <br />
        <%--<asp:LinkButton ID="clickLinkbtn" runat="server" Text="" OnClick="OnClickHere"></asp:LinkButton>--%>

        <asp:Button ID="clickButton" runat="server" CssClass="button_login" OnClick="OnClickHere" />
    </div>
    <script type="text/javascript">

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');

        window.onload = window.history.forward(0);

    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return; n = f.fbq = function () {
                n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            }; if (!f._fbq) f._fbq = n;
            n.push = n; n.loaded = !0; n.version = '2.0'; n.queue = []; t = b.createElement(e); t.async = !0;
            t.src = v; s = b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s)
        }(window,
        document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '622815904574238', {
            em: 'insert_email_variable,'
        });
        fbq('track', 'PageView');
        fbq('track', 'Purchase');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=622815904574238&ev=PageView&noscript=1"/></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->


</asp:Content>
