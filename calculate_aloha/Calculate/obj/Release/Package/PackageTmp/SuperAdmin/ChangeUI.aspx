﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangeUI.aspx.cs" Inherits="Calculate.SuperAdmin.ChangeUI" MasterPageFile="~/AdminMaster.master" Title="Manage UI" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <div class="clearfix">
        <h1>
            <asp:Label runat="server" CssClass="headfontsize" ID="lblModule" Text="Manage UI">
            </asp:Label></h1>
        <asp:Label runat="server" ID="lblSelect" Text="Select Page: " CssClass="font14" Font-Bold="true"></asp:Label>
        <asp:DropDownList runat="server" ID="ddlPages" CssClass="textEntry form-control maxWidthDropDownList minWidthDropdownSearch" OnSelectedIndexChanged="ddlPages_SelectedIndexChanged" AutoPostBack="true" DataTextField="ModuleName" DataValueField="ModuleName">
        </asp:DropDownList>
        <ajaxToolkit:ToolkitScriptManager ID="smCustomer" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:Label ID="lblErrorMessage" runat="server" CssClass="failureErrorNotification margleftcent" Font-Bold="true" Visible="false"></asp:Label>
        <br />
        <div class="clearfix ">
            <textarea class="txtBody form-control width100" id="txtBody" runat="server"></textarea>
        </div>
        <div class="clearfix text-center marginpopbottom ">
            <br />
            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="Save" OnClientClick="return validateData();" CssClass="button_login" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CssClass="button_login" />
        </div>
        <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
    </div>
</asp:Content>

<asp:Content ID="COntent3" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/nice-latest.js"></script>
    <script type="text/javascript">
        bkLib.onDomLoaded(function () {
            new nicEditor({ fullPanel: true, maxHeight: 200 }).panelInstance('ctl00_ctl00_MainContent_BodyContent_txtBody');
        });

        $(document).ready(function () {
            window.scrollTo(0, 400);
        });

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
</asp:Content>
