﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageInstitutions.aspx.cs" Inherits="Calculate.SuperAdmin.ManageInstitutions" MasterPageFile="~/AdminMaster.master" Title="Manage Institutions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <div class="AccountDetails">
        <h1 style="color: #85312f;" class="headfontsize setTopMargin">Manage Institutions
        </h1>
        <ajaxToolkit:ToolkitScriptManager ID="smCustomer" runat="server" ScriptMode="Release">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="upGrid" runat="server">
            <ContentTemplate>
                <div style="min-height: 365px;">
                    <div class="table-responsive">
                        <table class="filterGridView table managegrid">
                            <tbody>
                                <tr>
                                    <td class="searchText invisibletd verticalalign">
                                        <asp:DropDownList Height="27px" CssClass="form-control floatLeft maxWidthDropDownList minWidthDropdownSearch" ID="nameSelection" OnSelectedIndexChanged="nameSelection_TextChanged" AutoPostBack="true" DataTextField="InstitutionName" DataValueField="InstitutionName" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="searchText invisibletd verticalalign">
                                        <asp:TextBox CssClass="form-control textEntry handIcon floatLeft minWidthDropdownSearch" placeholder="Date" ID="dateFilter" runat="server" OnTextChanged="dateFilter_TextChanged" AutoPostBack="true"></asp:TextBox>
                                    </td>
                                    <td class="searchText verticalalign">
                                        <div class="input-append" style="min-width: 170px;">
                                            <asp:TextBox CssClass="form-control handIcon textEntry floatLeft" ID="txtSearch" AutoPostBack="true" runat="server" placeholder="Search" OnTextChanged="txtSearch_TextChanged"></asp:TextBox>
                                            <asp:ImageButton runat="server" ID="searchImage" ClientIDMode="Static" ImageUrl="~/Images/search_1.png" OnClick="txtSearch_TextChanged" CssClass="searchGridView floatLeft" />
                                        </div>
                                    </td>
                                    <td class="verticalalign">
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" Width="55px" OnClick="Reset" CssClass="managegridButton floatLeft" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive">
                        <asp:GridView ID="gvDetails" DataKeyNames="InstitutionAdminID" HeaderStyle-Height="45px" runat="server" AutoGenerateColumns="false"
                            CssClass="mGrid tablegrid table-striped table-hover table-responsive" HeaderStyle-BackColor="#8e8d8a" AllowPaging="true" OnPageIndexChanging="OnPaging"
                            PageSize="10" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" PagerStyle-CssClass="pgr" AllowSorting="true" OnSorting="gvDetails_Sorting"
                            AlternatingRowStyle-CssClass="alt" OnRowDataBound="gvDetails_RowDataBound" PagerStyle-HorizontalAlign="Right" ShowHeaderWhenEmpty="true" EmptyDataText="No Institution Found">
                            <Columns>
                                <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" HeaderStyle-BorderStyle="None" HeaderText="InstitutionAdminID" Visible="false" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInstitutionAdminID" runat="server" Text='<%#Eval("InstitutionAdminID") %>' />
                                        <asp:Label ID="lblUserID" runat="server" Text='<%#Eval("InstitutionAdminID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="Institution Name" SortExpression="InstitutionName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInstitutionName" runat="server" Text='<%#Eval("InstitutionName") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="First Name" ControlStyle-CssClass="ellipsis" SortExpression="Firstname">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFirstname" runat="server" Text='<%#Eval("Firstname") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="Last Name" ControlStyle-CssClass="ellipsis" SortExpression="Lastname">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLastname" runat="server" Text='<%#Eval("Lastname") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" HeaderStyle-BorderStyle="None" HeaderText="Password" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPassword" runat="server" Text='<%#Eval("Password") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" HeaderStyle-BorderStyle="None" HeaderText="Email" SortExpression="Email">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("Email") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" ControlStyle-CssClass="ellipsis" HeaderStyle-BorderStyle="None" HeaderText="Created On" SortExpression="UpdateDate">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("UpdateDate") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="120px" HeaderStyle-BorderStyle="None" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgStatus" runat="server" OnClientClick="return showSuspendConfirm(this);" OnClick="Status" CommandName="Select" />
                                        <asp:ImageButton ID="linkEdit" ToolTip="Edit" runat="server" ImageUrl="~/Images/edit_new.png" OnClick="editGridViewDetails" />
                                        <asp:ImageButton ID="linkDelete" ToolTip="Delete" runat="server" ImageUrl="~/Images/delete_new.png" OnClick="Delete" OnClientClick="return showDeleteConfirm('Institution');" />
                                        <asp:ImageButton ID="linkUpload" ToolTip="Upload Advisors" runat="server" ImageUrl="~/Images/upload.png" OnClick="UploadAdvisors" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="Details" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Button ID="lnkDetails" runat="server" Text="View Advisors" OnClick="View" CssClass="gridButton" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            
                            <%-- <AlternatingRowStyle BackColor="silver" />--%>
                        </asp:GridView>
                    </div>
                    <div class="managePageCount">
                        <span>Showing <%= gvDetails.Rows.Count%> of <%= (lblPageTotalNumber.Text )%> Records &nbsp;</span>
                    </div>
                    <br />
                    <asp:Label ID="lblPageTotalNumber" runat="server" class="displayNone"></asp:Label>
                    <center><asp:Button ID="btnAdd" runat="server" Text="Add New Institution" OnClick="AddInstitution" CssClass="button_login" /></center>
                    <br />
                    <center><asp:Label ID="lblErrorMessage" runat="server" CssClass="failureErrorNotification" Font-Bold="true" Visible="false"></asp:Label></center>
                    <%--<asp:Panel ID="pnlAddEdit" runat="server" CssClass="modalPopup displayNone" ScrollBars="Auto">
                    <div class="h2PopUp" ><asp:Label ForeColor="white" Font-Bold="false" ID="lblHeader" runat="server" Text="Add Institution Details"></asp:Label></div>
                    <br />
                    <form id="formToValidate">
                    <table class="table poptab" >
                        <tbody>
                        <tr>                            
                            <td id="modalTD">
                                <asp:Label ID="lblInstitutionName" ForeColor="#787878"  runat="server" Text="Institution Name"></asp:Label>
                            </td>
                            <td id="modalTD">
                                <asp:Label ID="lblFirstname" ForeColor="#787878" runat="server" Text="First Name"></asp:Label>                                
                            </td>
                            <td id="modalTD"></td>
                        </tr>
                        <tr>                            
                            <td id="modalTD">
                                <asp:TextBox CssClass="textEntry form-control" TabIndex="1" ID="txtDisplayName" MaxLength="20" runat="server" ></asp:TextBox>
                                <asp:TextBox CssClass="textEntry form-control" ID="txtInstitutionID" runat="server" Visible="false"></asp:TextBox>
                            </td>
                            <td id="modalTD">
                                <asp:TextBox CssClass="textEntry form-control" TabIndex="2" ID="txtFirstname" MaxLength="20" runat="server" ></asp:TextBox>
                            </td>
                            <td id="modalTD"></td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:Label ID="lblLastname" ForeColor="#787878" runat="server" Text="Last Name"></asp:Label>
                            </td>
                            <td id="modalTD">
                                <asp:Label ID="lblEmail" ForeColor="#787878" runat="server" Text="Email"></asp:Label> 
                            </td>
                            <td id="modalTD"></td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:TextBox CssClass="textEntry form-control" TabIndex="3" ID="txtLastname" MaxLength="20" runat="server"></asp:TextBox>
                                                               
                            </td>
                            <td id="modalTD">
                                <asp:TextBox CssClass="textEntry form-control" TabIndex="4" ID="txtEmail" MaxLength="50" AutoComplete="off" runat="server"></asp:TextBox>
                            </td>
                            <td id="modalTD"></td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:Label ID="lblpassword" ForeColor="#787878" runat="server" Text="Password"></asp:Label>
                            </td>
                            <td id="modalTD">
                                <asp:Label ID="lblConfirmPassword" ForeColor="#787878" runat="server" Text="Confirm Password"></asp:Label>
                            </td>
                            <td id="modalTD"></td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:TextBox CssClass="textEntry form-control" TabIndex="5" ID="txtPassword" MaxLength="20" AutoComplete="off" TextMode="Password" runat="server"></asp:TextBox>
                                
                            </td>
                            <td id="modalTD">
                                <asp:TextBox CssClass="textEntry form-control" TabIndex="6" ID="txtConfirmPassword" onChange="checkPasswordMatch();" MaxLength="20" TextMode="Password" AutoComplete="off" runat="server"></asp:TextBox>
                                <img id="imgpass" class="img-responsive" runat="server" src="/images/confirmPassword.png" style="margin-left:4px;display:none;" border="0" />
                            </td>
                            <td id="modalTD"></td>
                        </tr>   
                        
                            </tbody>
                    </table>
                        <table class="popupbottom table">
                            <tr>
                            
                            <td id="modalTD">
                                <asp:Button TabIndex="7" ID="btnSave" runat="server" Text="Save" OnClick="Save" OnClientClick="return validateData();" CssClass="button_login marginpopbottom" />&nbsp;
                                <asp:Button TabIndex="8" ID="btnCancel" OnClick="Cancel" runat="server" Text="Cancel" CssClass="button_login" />
                            </td>
                            <td id="modalTD"></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="failureForgotNotification displayNone modalTD" id="errorMessage"></td>
                        </tr>
                        </table>
                    </form>
                </asp:Panel>--%>
                    <asp:Panel ID="pnlAddEdit" runat="server" CssClass="modalInstitutionPopup12 displayNone" ScrollBars="Auto" DefaultButton="btnSave">
                        <div class="modal-dialog modal-md">
                            <div class="h2PopUp modal-header">
                                <asp:Label ForeColor="white" Font-Bold="false" ID="lblHeader" CssClass="marglefthead" runat="server" Text="Add Institution Details"></asp:Label>
                            </div>
                            <br />
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <asp:Label ID="lblInstitutionName" ForeColor="#787878" runat="server" Text="Institution Name"></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control" TabIndex="1" ID="txtDisplayName" MaxLength="20" runat="server" ValidateRequestMode="Inherit" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                        <asp:TextBox CssClass="textEntry form-control" ID="txtInstitutionID" runat="server" Visible="false"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqDisplayName" runat="server" ControlToValidate="txtDisplayName" CssClass="failureErrorNotification" ErrorMessage="Institution name is required." ToolTip="Institution name is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Institution name is required</asp:RequiredFieldValidator>
                                        <%--<asp:RegularExpressionValidator ID="regDisplayName" runat="server" ControlToValidate="txtDisplayName" CssClass="failureErrorNotification" ErrorMessage="Please enter valid Institution name" ValidationExpression="^[A-Za-z][A-Za-z0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid Institution name</asp:RegularExpressionValidator>--%>
                                    </div>
                                    <div class="col-md-6 text-left">
                                        <asp:Label ID="lblFirstname" ForeColor="#787878" runat="server" Text="First Name"></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control" TabIndex="2" ID="txtFirstname" MaxLength="20" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqFirstname" runat="server" ControlToValidate="txtFirstname" CssClass="failureErrorNotification" ErrorMessage="Customer name is required." ToolTip="Customer name is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Customer name is required</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regFirstname" runat="server" ControlToValidate="txtFirstname" CssClass="failureErrorNotification" ErrorMessage="Please enter valid Customer name" ValidationExpression="^[A-Za-z][A-Za-z0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid Customer name</asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <asp:Label ID="lblLastname" ForeColor="#787878" runat="server" Text="Last Name"></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control" TabIndex="3" ID="txtLastname" MaxLength="50" runat="server"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="reqLastname" runat="server" ControlToValidate="txtLastname" CssClass="failureErrorNotification" ErrorMessage="Please enter valid Customer name" ValidationExpression="^[A-Za-z][A-Za-z0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid Customer last name</asp:RegularExpressionValidator>
                                    </div>
                                    <div class="col-md-6 text-left">
                                        <asp:Label ID="lblEmail" ForeColor="#787878" runat="server" Text="Email"></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control" TabIndex="4" ID="txtEmail" MaxLength="50" AutoComplete="off" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqEmail" runat="server" ControlToValidate="txtEmail" CssClass="failureErrorNotification" ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*E-mail is required.</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regEmail" runat="server" ControlToValidate="txtEmail" CssClass="failureErrorNotification" ErrorMessage="Please enter valid E-mail" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid E-mail</asp:RegularExpressionValidator>
                                        <%--<asp:HiddenField ID="HiddenFieldEmail" runat="server" />--%>
                                    </div>
                                </div>
                                <br />
                                <%--<div class="row">
                                    <div class="col-md-6 text-left">
                                        <asp:Label ID="lblpassword" ForeColor="#787878" runat="server" Text="Password"></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control" TabIndex="5" ID="txtPassword" MaxLength="50" onChange="checkPasswordMatch();" AutoComplete="off" TextMode="Password" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqPassword" runat="server" ControlToValidate="txtPassword" CssClass="failureErrorNotification" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Password is required</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regPassword" runat="server" ControlToValidate="txtPassword" CssClass="failureErrorNotification" ErrorMessage="Password length must be in between 6-32" ValidationExpression="^.{6,32}$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Password must be between 6-32 characters</asp:RegularExpressionValidator>
                                    </div>
                                    <div class="col-md-6 text-left">
                                        <asp:Label ID="lblConfirmPassword" ForeColor="#787878" runat="server" Text="Confirm Password"></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control" TabIndex="6" ID="txtConfirmPassword" onChange="checkPasswordMatch();" MaxLength="20" TextMode="Password" AutoComplete="off" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                        <img id="imgpass" class="img-responsive" runat="server" src="/images/confirmPassword.png" style="margin-left: 205px; margin-top: -26px; display: none;" border="0" />
                                        <asp:RequiredFieldValidator ID="reqPasswordRequired" runat="server" ControlToValidate="txtConfirmPassword" CssClass="failureErrorNotification" ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Confirm Password is required</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regCustomerPassword" runat="server" ControlToValidate="txtConfirmPassword" CssClass="failureErrorNotification" ErrorMessage="Confirm Password length must be in between 6-32" ValidationExpression="^.{6,32}$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">* Confirm Password must be between 6-32 characters</asp:RegularExpressionValidator>
                                        <asp:CompareValidator ID="comAdvisorValidator" runat="server" ControlToValidate="txtConfirmPassword" ControlToCompare="txtPassword" CssClass="failureErrorNotification" ErrorMessage="<br/>*Password and Confirm Password must match" ValidationGroup="RegisterUserValidationGroup"></asp:CompareValidator>
                                    </div>
                                </div>--%>
                                <div id="AdvisorDiv" runat="server" class="displayNone">
                                    <div class="row">
                                        <div class="col-md-8 text-left">
                                            <asp:Label ID="lblDefaultAdvisorDetails" Style="display: block;" Font-Bold="true" ForeColor="#000000" runat="server" Text="Add Default Advisor Details:"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <asp:Label ID="lblEmailInstruct" Style="font-size: 11px; padding-left:5px; color: gray;" Text="*This would be the default advisor for your institution,you could use this to create customers directly." runat="server"></asp:Label><br />
                                        <div class="col-md-6 text-left">
                                            <asp:Label ID="lblAdvisorEmail" Style="display: block;" ForeColor="#787878" runat="server" Text="Advisor Email"></asp:Label>
                                            <asp:TextBox CssClass="textEntry form-control" Style="display: block;" AutoComplete="off" TabIndex="7" ID="txtAdvisorEmail" MaxLength="50" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqAdvisorEmail" runat="server" ControlToValidate="txtAdvisorEmail" CssClass="failureErrorNotification" ErrorMessage="Advisor E-mail is required." ToolTip="E-mail is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">* Advisor E-mail is required.</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regAdvisorEmail" runat="server" ControlToValidate="txtAdvisorEmail" CssClass="failureErrorNotification" ErrorMessage="Please enter valid E-mail" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid E-mail</asp:RegularExpressionValidator>
                                        </div>
                                        <div class="col-md-6 text-left">
                                            <asp:Label ID="lblAdvisorPAssword" Style="display: block;" ForeColor="#787878" runat="server" Text="Advisor Password"></asp:Label>
                                            <asp:TextBox CssClass="textEntry form-control" Style="display: block;" TabIndex="8" ID="txtAdvisorPassword" MaxLength="50" TextMode="Password" OnChange="checkAdPasswordMatch();" AutoComplete="off" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqAdvisorPassword" runat="server" ControlToValidate="txtAdvisorPassword" CssClass="failureErrorNotification" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Password is required</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regAdvisorPassword" runat="server" ControlToValidate="txtAdvisorPassword" CssClass="failureErrorNotification" ErrorMessage="Password length must be in between 6-32" ValidationExpression="^.{6,32}$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Password must be between 6-32 characters</asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-6 text-left">
                                            <asp:Label ID="lblAdvisorConfirmPassword" Style="display: block;" ForeColor="#787878" runat="server" Text="Advisor Confirm Password"></asp:Label>
                                            <asp:TextBox CssClass="textEntry form-control" Style="display: block;" TabIndex="9" ID="txtAdvisorConfirmPassword" OnChange="checkAdPasswordMatch();" MaxLength="50" TextMode="Password" AutoComplete="off" runat="server"></asp:TextBox>
                                            <img id="img1" class="img-responsive" runat="server" src="/images/confirmPassword.png" style="display: none;" border="0" />
                                            <asp:RequiredFieldValidator ID="reqAdvisorConfirmPassword" runat="server" ControlToValidate="txtAdvisorConfirmPassword" CssClass="failureErrorNotification" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Confirm Password is required</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regAdvisorConfirmPassword" runat="server" ControlToValidate="txtAdvisorConfirmPassword" CssClass="failureErrorNotification" ErrorMessage="Password length must be in between 6-32" ValidationExpression="^.{6,32}$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Confirm Password must be between 6-32 characters</asp:RegularExpressionValidator>
                                            <asp:CompareValidator ID="comAdvisorConfirmPassword" runat="server" ControlToValidate="txtAdvisorConfirmPassword" CssClass="failureErrorNotification" ControlToCompare="txtAdvisorPassword" ErrorMessage="<br/>*Password and Confirm Password must match" ValidationGroup="RegisterUserValidationGroup"></asp:CompareValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="row popupbottom">
                                    <div class="col-md-12">
                                        <center>
                                    <asp:Button TabIndex="10" ID="btnSave" OnClick="Save" runat="server" Text="Save" CssClass="button_login" ValidationGroup="RegisterUserValidationGroup" />&nbsp;&nbsp;
                            <asp:Button TabIndex="11" ID="btnCancel" OnClick="Cancel" runat="server" Text="Cancel" CssClass="button_login marginpopbottom" />
                                </center>
                                        <asp:Label ID="lblUserExists" runat="server" Visible="false"></asp:Label>
                                    </div>
                                </div>
                                <div class="row popupbottom">
                                    <div class="col-md-12">
                                        <div class="failureForgotNotification widthfull displayNone" id="errorMessage"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
                    <cc1:ModalPopupExtender ID="popup" runat="server" PopupControlID="pnlAddEdit" TargetControlID="lnkFake" BackgroundCssClass="modalBackground">
                    </cc1:ModalPopupExtender>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="gvDetails" />
                <asp:AsyncPostBackTrigger ControlID="btnSave" />
                <asp:AsyncPostBackTrigger ControlID="btnCancel" />
            </Triggers>
        </asp:UpdatePanel>
        
        <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../Scripts/common.js" type="text/javascript"></script>
    <script type="text/javascript">
        function checkAdPasswordMatch() {
            var password = $("[id$=txtAdvisorPassword]").val();
            var confirmPassword = $("[id$=txtAdvisorConfirmPassword]").val();
            if (confirmPassword.match(/\S/) && password.match(/\S/)) {
                if (password == confirmPassword)
                    $("[id$=img1]").css("display", "");
                else
                    $("[id$=img1]").css("display", "none");
            }
            else {
                $("[id$=img1]").css("display", "none");
            }
        }

        //$(document).ready(function () {
        //    window.scrollTo(0, 400);
        //});

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
</asp:Content>
