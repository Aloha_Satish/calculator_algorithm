﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewsLettersAdvisor.aspx.cs" ValidateRequest="false"  Inherits="Calculate.SuperAdmin.NewsLettersAdvisor" MasterPageFile="~/AdminMaster.master" Title="Periodic NewsLetters" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="content3" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/nice-latest.js"></script>
    <script type="text/javascript">
        bkLib.onDomLoaded(function () {
            new nicEditor({ fullPanel: true, maxHeight: 200 }).panelInstance('ctl00_ctl00_MainContent_BodyContent_txtBody');
            $('.nicEdit-main').css('min-height', '150px');
            $('.nicEdit-main').parent().css("width", "");
            $('.nicEdit-panelContain').parent().css("width", "");
        });

        $(function () {
            debugger;
            $("[id*=chkAll]").bind("click", function () {
                if ($(this).is(":checked")) {
                    $("[id*=lstInstitution] input").attr("checked", "checked");
                } else {
                    $("[id*=lstInstitution] input").removeAttr("checked");
                }
            });
            $("[id*=lstInstitution] input").bind("click", function () {
                if ($("[id*=lstInstitution] input:checked").length == $("[id*=lstInstitution] input").length) {
                    $("[id*=chkAll]").attr("checked", "checked");
                } else {
                    $("[id*=chkAll]").removeAttr("checked");
                }
            });
            if (navigator.appName == "Microsoft Internet Explorer") {
                $("#tblNewsletter").addClass("alignleft");
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <div class="AccountDetails">
        <center>            
            <h1 style="color:#85312f;text-align:left;">                
                Manage Periodic NewsLetter
            </h1>
        </center>      
            <ajaxToolkit:ToolkitScriptManager ID="smCustomer" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="upGrid" runat="server">
            <ContentTemplate>
                <div class="table-responsive">
                <asp:GridView ID="gvDetails" DataKeyNames="NewsLetterId" HeaderStyle-Height="45px" runat="server"
                    AutoGenerateColumns="false" CssClass="mGrid table table-striped table-hover table-responsive" HeaderStyle-BackColor="#61A6F8" AllowPaging="true"
                    OnPageIndexChanging="OnPaging" PageSize="10" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" OnRowDataBound="gvDetails_RowDataBound" ShowHeaderWhenEmpty="true" EmptyDataText ="No Record Found">
                    <Columns>
                        <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="ID" Visible="false">
                            <ItemTemplate>                                
                                <asp:Label ID="lblNewsLetterId" runat="server" Text='<%#Eval("NewsLetterId") %>' />
                                <asp:Label ID="lblLetterUpdateBy" runat="server" Text='<%#Eval("LetterUpdateBy") %>' />
                                <asp:Label ID="lblLetterStatus" runat="server" Text='<%#Eval("LetterStatus") %>' />
                                <asp:Label ID="lblLetterBody" runat="server" Text='<%#Eval("LetterBody") %>' />
                                <asp:Label ID="lblAdvisorList" runat="server" Text='<%#Eval("AdvisorList") %>' />
                                <asp:Label ID="lblCustomersFlag" runat="server" Text='<%#Eval("CustomersFlag") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="Subject">
                            <ItemTemplate>
                                <asp:Label ID="lblSubject" runat="server" Text='<%#Eval("LetterSubject") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="Frequency">
                            <ItemTemplate>
                                <asp:Label ID="lblPeriodic" runat="server" Text='<%#Eval("Periodic") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" SortExpression="PublishDate" HeaderStyle-BorderStyle="None" HeaderText="Publish Date">
                            <ItemTemplate>
                                <asp:Label ID="lblLetterPublishDate" runat="server" Text='<%#Eval("PublishDate") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="Next Scheduled Date">
                            <ItemTemplate>
                                <asp:Label ID="lblLetterNextDate" runat="server" Text='<%#(Eval("PublishDate").ToString()!="")?GetNextDate(Eval("PublishDate").ToString(),Eval("Periodic").ToString()):"" %>' />
                            </ItemTemplate>
                        </asp:TemplateField>                        
                        <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderStyle-CssClass="textCenter" ItemStyle-Width="90px" HeaderText="" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgStatus" runat="server" OnClientClick="return showSuspendConfirmNewsletter(this);" OnClick="Status" CommandName="Select" />
                                <asp:ImageButton ID="linkEdit"  runat="server" ImageUrl="~/Images/edit_new.png"  OnClick="editGridViewDetails" />
                               <asp:ImageButton ID="linkDelete" runat="server" ImageUrl="~/Images/delete_new.png"  OnClick="Delete" OnClientClick="return showDeleteConfirm('NewsLetter');" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                   <%-- <AlternatingRowStyle BackColor="silver" />--%>
                </asp:GridView>
                    </div>
                <center><asp:Button ID="btnAdd" runat="server" Text="Add New Periodic NewsLetter" OnClick="AddNewsLetter" CssClass="button_login" /></center>
                <br />
                <center><asp:Label ID="lblErrorMessage" runat="server" CssClass="failureErrorNotification" Font-Bold="true" Visible="false"></asp:Label></center>
                <br />
                <%--<asp:Panel ID="pnlAddEdit" Width="850px" BorderColor="#f3f3f3" BorderStyle="Dashed" runat="server" CssClass="modalPopup modalWhole" ScrollBars="Auto">
                    <asp:Label ForeColor="#85312f" CssClass="floatLeft margleft" Font-Size="20px" ID="lblHeader" runat="server" Text="Add Periodic NewsLetter"></asp:Label>
                    <br />
                    <form id="formToValidate">
                    <table id="tblNewsletter" class="table-responsive table margileft" align="center">
                        <tr>
                            <td id="modalTD">
                                <asp:Label ID="Label1" ForeColor="#787878" runat="server" Text="Publish Date"></asp:Label>
                                                            
                            </td>                           
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:TextBox CssClass="textEntry form-control" ID="datePublish" runat="server" TabIndex="1"></asp:TextBox>
                                <asp:ImageButton ID="ImageButton1" CssClass="manageImageTONewsletter" runat="server" ImageUrl="~/Images/calendar1.png" OnClientClick=" return false;"/>
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="datePublish"></cc1:CalendarExtender>  
                                <asp:HiddenField ID="NewsLetterIDHidden" runat="server" /> 
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:Label ID="Label2" ForeColor="#787878" CssClass="manageNewsletterFrequency" runat="server" Text="Frequency"></asp:Label>
                                                          
                            </td>                           
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:Label ID="lblWeek" runat="server" Font-Bold="true" Text="Weekly"></asp:Label>
                                <asp:Label ID="lblMon" runat="server" Font-Bold="true" Text="Monthly"></asp:Label>
                                <asp:Label ID="lblQuat" runat="server" Font-Bold="true" Text="Quarterly<br />"></asp:Label>
                                <asp:RadioButton ID="rdbWeekly" Width="55px" runat="server" GroupName="Range" Checked="true" TabIndex="2" />
                                <asp:RadioButton ID="rdbMonthly" Width="65px" runat="server" GroupName="Range" TabIndex="3" /> 
                                <asp:RadioButton ID="rdbQuartely" runat="server" GroupName="Range" TabIndex="4" />
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:Label ID="lblLetterID" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lblSubject" ForeColor="#787878" runat="server" Text="Subject"></asp:Label>
                                
                            </td>
                        </tr>  
                        <tr>
                            <td id="modalTD">
                                <asp:TextBox ID="txtSubject" CssClass="form-control newbord" MaxLength="100" placeholder="Enter Subject" Font-Bold="true" runat="server" Width="735px" TabIndex="5" ></asp:TextBox>
                            </td>
                        </tr>                      
                        <tr>
                            <td id="modalTD">
                            <asp:Label ID="lblBody" ForeColor="#787878" runat="server" Text="Body"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                            <textarea class="txtBody" style="width:90%;" id="txtBody" TabIndex="6" runat="server"></textarea> 
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:Label ID="Label3" ForeColor="#787878" CssClass="floatLeft" runat="server" Text="Select Advisors"></asp:Label>                                
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <div class="InstitutionListAdjustment">
                                    <asp:CheckBox ID="chkAll" runat="server" Text="Select All" OnCheckedChanged="lstInstitution_TextChanged" /><br />
                                    <asp:CheckBoxList ID="lstInstitution" runat="server" >  
                                    </asp:CheckBoxList>
                                </div>
                                <asp:CheckBox ID="chkCustomers" runat="server" Text="Customers" CssClass="adjustChkAll" />
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD" style="padding-top:10px;">
                                <asp:FileUpload ID="txtAttachment" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <asp:Table ClientIDMode="Static" runat="server" ID="attached" style="width:50%;text-align:left;">
                                    <asp:TableHeaderRow>
                                        <asp:TableHeaderCell HorizontalAlign="Center" ID="header1" Visible="false" ClientIDMode="Static" CssClass="displayNone" Text="FileName"></asp:TableHeaderCell>
                                        <asp:TableHeaderCell HorizontalAlign="Center" ID="header2" Visible="false" ClientIDMode="Static" CssClass="displayNone" Text="Delete"></asp:TableHeaderCell>
                                    </asp:TableHeaderRow>
                                </asp:Table>
                            </td>
                        </tr>
                        <tr>
                            <td id="modalTD">
                                <center>
                                    <asp:Button  TabIndex="7" ID="btnSave" runat="server" Text="Save" OnClick="Save" OnClientClick="return validateData();" CssClass="button_login" />
                                    <asp:Button TabIndex="8" ID="btnCancel" runat="server" Text="Cancel" OnClick="Cancel"  CssClass="button_login" />
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="failureForgotNotification displayNone modalTD" id="errorMessage"> 
                            </td>
                        </tr>
                    </table>
                    </form>
                </asp:Panel>--%>
                <asp:Panel ID="pnlAddEdit" BorderColor="#f3f3f3" BorderStyle="Dashed" BorderWidth="2px" runat="server" CssClass="modalPopup" ScrollBars="Auto">
                    <div class="modal-md">
                    <asp:Label ForeColor="#85312f" CssClass="floatLeft margleft" Font-Size="20px" ID="lblHeader" runat="server" Text="Add Periodic NewsLetter"></asp:Label>
                    <br /><br />
                    <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Label ID="Label1" ForeColor="#787878" runat="server" Text="Publish Date"></asp:Label><br />
                            <asp:TextBox CssClass="textEntry form-control" ID="datePublish" runat="server" TabIndex="1"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" CssClass="manageImageTONewsletter" runat="server" ImageUrl="~/Images/calendar1.png" OnClientClick=" return false;"/>
                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="datePublish"></cc1:CalendarExtender>  
                            <asp:HiddenField ID="NewsLetterIDHidden" runat="server" />
                        </div>
                    </div><br />
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Label ID="Label2" ForeColor="#787878" CssClass="manageNewsletterFrequency" runat="server" Text="Frequency"></asp:Label><br />
                            <asp:Label ID="lblWeek" runat="server" Font-Bold="true" Text="Weekly"></asp:Label>
                            <asp:Label ID="lblMon" runat="server" Font-Bold="true" Text="Monthly"></asp:Label>
                            <asp:Label ID="lblQuat" runat="server" Font-Bold="true" Text="Quarterly<br />"></asp:Label>
                            <asp:RadioButton ID="rdbWeekly" Width="55px" runat="server" GroupName="Range" Checked="true" TabIndex="2" />
                            <asp:RadioButton ID="rdbMonthly" Width="65px" runat="server" GroupName="Range" TabIndex="3" /> 
                            <asp:RadioButton ID="rdbQuartely" runat="server" GroupName="Range" TabIndex="4" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Label ID="lblLetterID" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblSubject" ForeColor="#787878" runat="server" Text="Subject"></asp:Label><br />
                            <asp:TextBox ID="txtSubject" CssClass="form-control newbord" MaxLength="100" placeholder="Enter Subject" runat="server" TabIndex="5" ></asp:TextBox>
                        </div>
                    </div><br />
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Label ID="lblBody" ForeColor="#787878" runat="server" Text="Body"></asp:Label><br />
                            <textarea class="txtBody form-control" id="txtBody" TabIndex="6" runat="server"></textarea> 
                        </div>
                    </div><br />
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Label ID="Label3" ForeColor="#787878" CssClass="floatLeft" runat="server" Text="Select Advisors"></asp:Label><br />
                            <div class="InstitutionListAdjustment">
                                <asp:CheckBox ID="chkAll" runat="server" Text="Select All" OnCheckedChanged="lstInstitution_TextChanged" /><br />
                                <asp:CheckBoxList ID="lstInstitution" runat="server" >  
                                </asp:CheckBoxList>
                            </div>
                            <div class="width200">
                                <asp:CheckBox ID="chkCustomers" runat="server" Text="Customers" CssClass="adjustChkAll" />
                            </div>
                        </div>
                        <div class="col-md-12">
                    </div><br />
                    <div class="row">
                            <asp:FileUpload ID="txtAttachment" runat="server" />
                        </div>
                    </div><br />
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Table ClientIDMode="Static" runat="server" ID="attached" style="width:60%;text-align:left;">
                                <asp:TableHeaderRow>
                                    <asp:TableHeaderCell HorizontalAlign="Center" ID="header1" Visible="false" ClientIDMode="Static" CssClass="displayNone" Text="FileName"></asp:TableHeaderCell>
                                    <asp:TableHeaderCell HorizontalAlign="Center" ID="header2" Visible="false" ClientIDMode="Static" CssClass="displayNone" Text="Delete"></asp:TableHeaderCell>
                                </asp:TableHeaderRow>
                            </asp:Table>
                        </div>
                    </div><br />
                    <div class="row">
                        <div class="col-md-12">
                            <center>
                                <asp:Button  TabIndex="7" ID="btnSave" runat="server" Text="Save" OnClick="Save" OnClientClick="return validateData();" CssClass="button_login" />
                                <asp:Button TabIndex="8" ID="btnCancel" runat="server" Text="Cancel" OnClick="Cancel"  CssClass="button_login" />
                            </center>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="failureForgotNotification displayNone" id="errorMessage"></div>
                        </div>
                    </div><br />
                    </div>
                    </div>
                </asp:Panel>
                <asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>                
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="gvDetails" />
                <asp:PostBackTrigger ControlID="btnAdd" />
                <asp:PostBackTrigger ControlID="btnSave" />
            </Triggers>
        </asp:UpdatePanel>
        <br />
        <link rel="Stylesheet" type="text/css" href="../css/uploadify.css" />
        <script type="text/javascript" src="../Scripts/jquery-1.3.2.min.js"></script>
        <script type="text/javascript" src="../Scripts/jquery.uploadify.js"></script>
        <script type="text/javascript">
            $(function () {
                $("[id*=txtAttachment]").fileUpload({
                    'uploader': '../Scripts/uploader.swf',
                    'cancelImg': '../Images/cancel.png',
                    'buttonText': 'Attach Files',
                    'script': 'UploadCS.ashx',
                    'folder': 'uploads',
                    'multi': true,
                    'auto': true,
                    'scriptData': { key: '<%=Key %>' },
                    'onSelect': function (event, ID, file) {
                        $("#attachedfiles tr").each(function () {
                            if ($("td", this).eq(0).html() == file.name) {
                                alert(file.name + " already uploaded.");
                                $("[id*=txtAttachment]").fileUploadCancel(ID);
                                return;
                            }
                        });
                    },
                    'onComplete': function (event, ID, file, response, data) {
                        $("#header1").removeClass("displayNone");
                        $("#header2").removeClass("displayNone");
                        $("#attached").append("<tr><td style='text-align:left;'><span>" + file.name + "</span> &nbsp;&nbsp;<img src='../Images/hide.png' style='height:18px;width:18px;cursor:pointer;'/></td></tr>");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $("#attached img").live("click", function () {
                var row = $(this).closest("tr");
                var fileName = $("td span", row).eq(0).html();
                var newsletterid = $("#newsletterIDTable").val();
                if (newsletterid == undefined) {
                    newsletterid = null;
                }
                $.ajax({
                    type: "POST",
                    url: "NewsLettersAdvisor.aspx/RemoveFile",
                    data: '{fileName: "' + fileName + '",id: "' + newsletterid + '", key: "<%=Key %>" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function () { },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
                row.remove();
            });

            //Google Analytics
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-88761127-1', 'auto');
            ga('send', 'pageview');
        </script>
    </div>
</asp:Content>
