﻿<%@ Page Title="Register" Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Calculate.Account.Register" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">


    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <%--<link rel="stylesheet" media="(max-width:600px)" href="/css/smalldevice.css">--%>
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap-theme.css" rel="stylesheet" />
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap-dialog.css" rel="stylesheet" />
    <link href="/css/starter-template.css" rel="stylesheet" />
    <title>User Registration</title>
    <link rel="stylesheet" type="text/css" href="/css/main.css" />
    <link rel="stylesheet" type="text/css" href="/css/forms.css" />
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="/css/interior.css" />
    <link rel="stylesheet" type="text/css" href="/css/jquery-te-1.4.0.css" />
    <link href="/Styles/Calculate.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/css/ui-lightness/jquery-ui-1.10.3.custom.css" />
    <script src="../Scripts/jquery-1.7.2.min.js"></script>
    <script src="../Scripts/hashchange.min.js"></script>
    <script>var $1_7_1 = jQuery.noConflict();</script>


    <script src="../Scripts/common.js" type="text/javascript"></script>
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(".BasicInfo").hide();
        $(".BasicInfoDemo").hide();

        //Processing Dialog
        ijQuery(window).load(function () {
            var notification_loader;

            ijQuery('body').hide().show();

            notification_loader = ijQuery('.notification-loader');
            notification_loader.attr('src', notification_loader.attr('rel'));
        });


        $(document).ready(function () {

            $(".BasicInfo").hide();
            $(".BasicInfoDemo").hide();

            $("#ImgLogin").click(function (e) {
                ShowDialog(true);
                e.preventDefault();
            });

            $("[id$=btnStep1]").click(function () {
                $('input[name="MaritalStatus"]').prop('checked', false);
            });

        });



        $(function () {
            setTimeout(function () {
                $("[id$=displayErrorMessage]").fadeOut(3000);
            }, 5000);
        });
        $(function fade() {
            setTimeout(function () {
                $("[id$=ErrorMessagelable]").fadeOut(1000);
            }, 5000);

        });
        $(function fade() {
            setTimeout(function () {
                $("[id$=lblUserCreate]").fadeOut(500);
            }, 5000);
        });
        $(function fade() {
            setTimeout(function () {
                $("[id$=lblYourError]").fadeOut(3000);
            }, 5000);
        });
        $(function fade() {
            setTimeout(function () {
                $("[id$=lblSpouseError]").fadeOut(3000);
            }, 5000);

        });
        $(document).ready(function () {

            $("#ImgLogin").click(function (e) {
                ShowDialog(true);
                e.preventDefault();
            });

            //window.scrollTo(0, 400);
            $(".spacehide").hide();
            var Browser = {
                IsIe: function () {
                    return navigator.appVersion.indexOf("MSIE") != -1;
                },
                Navigator: navigator.appVersion,
                Version: function () {
                    var version = 999; // we assume a sane browser
                    if (navigator.appVersion.indexOf("MSIE") != -1)
                        // bah, IE again, lets downgrade version number
                        version = parseFloat(navigator.appVersion.split("MSIE")[1]);
                    return version;
                }
            };
            if (Browser.IsIe && Browser.Version() <= 9) {
                $("#fldLogin").addClass("registerIE");
                $("#fldAccount").addClass("registerIE");
                $("[id$=txtCustomerName]").addClass("width67");
                $("[id$=txtEmail]").addClass("width67");
                $("[id$=txtEmailAddress]").addClass("width53");
            }
            <%if (Session["UserID"] == null)
              {%>
            $('#content').attr("style", "border:none;");
                <%}%>
            //$('#ctl00_MainContent_vsLogin').find('li').attr("style", "list-style-image:url(../images/validarrow.png)");
            $('#myTab a:first').tab('show');


        });

        //function EnableDisableSignUp() {
        //    $("[id$=ChkBoxIAgree]").click(function () {
        //        if ($("[id$=ChkBoxIAgree]").prop('checked') == true) {
        //            //do something
        //            $("[id$=btnCreateUser]").prop('disabled', false);
        //        }
        //        else { $("[id$=btnCreateUser]").prop('disabled', true); }
        //    });
        //}

        function checkPasswordMatch() {
            var password = $("[id$=txtCustomerPassword]").val();
            var confirmPassword = $("[id$=txtConfirmCustomerPassword]").val();

            if (password == confirmPassword)
                $("[id$=imgpass]").css("display", "");
            else
                $("[id$=imgpass]").css("display", "none");
        }
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            e.target // activated tab
            e.relatedTarget // previous tab
        });
        $(function () {
            var DatepickerOpts = {
                changeMonth: true,
                changeYear: true,
                yearRange: '1910:2020',
                showOn: 'both',
                buttonImageOnly: true,
                buttonImage: '/Images/calendar1.png',
            };
            $("[id$=txtBirthDate]").datepicker(DatepickerOpts);
            var SpouseDatepickerOpts = {
                changeMonth: true,
                changeYear: true,
                yearRange: '1910:2020',
                showOn: 'both',
                buttonImageOnly: true,
                buttonImage: '/Images/calendar1.png',
            };
            $("[id$=txtSpouseBirthdate]").datepicker(SpouseDatepickerOpts);
            /* Check if id exists in current Page */
            if ($("[id$=txtBirthDate]") != null) {
                $("[id$=txtBirthDate]").keypress(function (e) { e.preventDefault(); });
            }

            /* Check if id exists in current Page */
            if ($("[id$=txtSpouseBirthdate]") != null) {
                $("[id$=txtSpouseBirthdate]").keypress(function (e) { e.preventDefault(); });
            }
        });
        function PopulateDays_Your() {
            var ddlMonth_Your = document.getElementById("<%=ddlMonth_Your.ClientID%>");
            var ddlYear_Your = document.getElementById("<%=ddlYear_Your.ClientID%>");
            var ddlDay_Your = document.getElementById("<%=ddlDay_Your.ClientID%>");
            var y_Your = ddlYear_Your.options[ddlYear_Your.selectedIndex].value;
            var m_Your = ddlMonth_Your.options[ddlMonth_Your.selectedIndex].value != 0;
            var d_Your = ddlDay_Your.options[ddlDay_Your.selectedIndex].value;
            //ddlDay_Your.options.length = 0;
            if (ddlMonth_Your.options[ddlMonth_Your.selectedIndex].value != 0 && ddlYear_Your.options[ddlYear_Your.selectedIndex].value != 0) {
                var dayCount = 32 - new Date(ddlYear_Your.options[ddlYear_Your.selectedIndex].value, ddlMonth_Your.options[ddlMonth_Your.selectedIndex].value - 1, 32).getDate();
                var dayselected = ddlDay_Your.options[ddlDay_Your.selectedIndex].index;
                ddlDay_Your.options.length = 0;
                AddOption_Your(ddlDay_Your, "DD", "0");
                for (var i = 1; i <= 31; i++) {
                    AddOption_Your(ddlDay_Your, i, i);
                }
            }
        }
        function AddOption_Your(ddl, text, value) {
            var opt = document.createElement("OPTION");
            opt.text = text;
            opt.value = value;
            ddl.options.add(opt);
        }
        function Validate_YourDay(sender, args) {
            var ddlDay_Your = document.getElementById("<%=ddlDay_Your.ClientID%>");
            args.IsValid = (ddlDay_Your.selectedIndex != 0)
        }
        function Validate_YourMonth(sender, args) {
            var ddlMonth_Your = document.getElementById("<%=ddlMonth_Your.ClientID%>");
            args.IsValid = (ddlMonth_Your.selectedIndex != 0)
        }
        function Validate_YourYear(sender, args) {
            var ddlYear_Your = document.getElementById("<%=ddlYear_Your.ClientID%>");
            args.IsValid = (ddlYear_Your.selectedIndex != 0)
        }
        function PopulateDays_Spouse() {
            var ddlMonth_Spouse = document.getElementById("<%=ddlMonth_Spouse.ClientID%>");
            var ddlYear_Spouse = document.getElementById("<%=ddlYear_Spouse.ClientID%>");
            var ddlDay_Spouse = document.getElementById("<%=ddlDay_Spouse.ClientID%>");
            var y_Spouse = ddlYear_Spouse.options[ddlYear_Spouse.selectedIndex].value;
            var m_Spouse = ddlMonth_Spouse.options[ddlMonth_Spouse.selectedIndex].value != 0;
            if (ddlMonth_Spouse.options[ddlMonth_Spouse.selectedIndex].value != 0 && ddlYear_Spouse.options[ddlYear_Spouse.selectedIndex].value != 0) {
                var dayCount = 32 - new Date(ddlYear_Spouse.options[ddlYear_Spouse.selectedIndex].value, ddlMonth_Spouse.options[ddlMonth_Spouse.selectedIndex].value - 1, 32).getDate();
                ddlDay_Your.options.length = 0;
                AddOption_Spouse(ddlDay_Spouse, "DD", "0");
                for (var i = 1; i <= 31; i++) {
                    AddOption_Spouse(ddlDay_Spouse, i, i);
                }
            }
        }
        function AddOption_Spouse(ddl, text, value) {
            var opt = document.createElement("OPTION");
            opt.text = text;
            opt.value = value;
            ddl.options.add(opt);
        }
        function Validate_SpouseDay(sender, args) {

            var ddlDay_Spouse = document.getElementById("<%=ddlDay_Spouse.ClientID%>");
            args.IsValid = (ddlDay_Spouse.selectedIndex != 0)
            if ($("[id$=rdbtnWidowed]").prop("checked") || $("[id$=rdbtnSingle]").prop("checked")) {
                args.IsValid = true;
            }
        }
        function Validate_SpouseMonth(sender, args) {
            var ddlMonth_Spouse = document.getElementById("<%=ddlMonth_Spouse.ClientID%>");
            args.IsValid = (ddlMonth_Spouse.selectedIndex != 0);
            if ($("[id$=rdbtnWidowed]").prop("checked") || $("[id$=rdbtnSingle]").prop("checked")) {
                args.IsValid = true;
            }
        }
        function Validate_SpouseYear(sender, args) {
            var ddlYear_Spouse = document.getElementById("<%=ddlYear_Spouse.ClientID%>");
            args.IsValid = (ddlYear_Spouse.selectedIndex != 0)
            if ($("[id$=rdbtnWidowed]").prop("checked") || $("[id$=rdbtnSingle]").prop("checked")) {
                args.IsValid = true;
            }
        }
        function AdvisorUser() {
            $(".hideRow").hide();
            $(".hideRow1").hide();
            $(".Advisor").hide();

        }

        function Validate_DemoWifeBenefit(sender, args) {
            var ddlWifeBenefit = document.getElementById("<%=ddlWifeBenefit.ClientID%>");
            args.IsValid = (ddlWifeBenefit.selectedIndex != 0);
            if ($("[id$=rdbtnSingle]").prop("checked")) {
                args.IsValid = true;
            }
        }

        function Validate_DemoHusbBenefit(sender, args) {
            alert("Hi");
            var ddlHusbandBenefit = document.getElementById("<%=ddlHusbandBenefit.ClientID%>");
            args.IsValid = (ddlHusbandBenefit.selectedIndex != 0);
        }

        function ClearText() {
            var textbox1 = document.getElementById("<%=txtEmail.ClientID%>");
            var textbox3 = document.getElementById("<%=txtCustomerName.ClientID%>");
            textbox1.value = null;
            textbox3.value = null;
        }

        $(function () {

        });

        function HideBasicInfoDiv() {
            $(".BasicInfo").hide();
        }


        function customerSpouseinformation() {

            $(".BasicInfo").hide();
            $(".BasicInfoDemo").hide();

            //$("[id$=lblYourError]").text('');
            //$("[id$=lblSpouseNamerequired]").text('');
            //$("[id$=lblSpouseError]").text('');

            if ($("[id$=rdbtnMarried]").prop("checked")) {

                $("#btnContinueStep2").show();

                $("#btnBackContinueStep2Demo").show();
                $("#dummyDiv").hide();
                $("#dummyDivDemo").hide();
                $(".hideRow").show();
                $(".hideRow1").show();
                $(".DivorceText").hide();
                $(".spacehide").hide();
                $(".BasicInfo").show();
                $(".BasicInfoDemo").show();

                var lblSpouseBirthDate = document.getElementById("<%=lblSpouseBirthDate.ClientID%>");
                if (lblSpouseBirthDate != null) {
                    document.getElementById("<%=lblSpouseBirthDate.ClientID%>").innerHTML = "What is your spouse's date of birth?";
                }
                var Label5 = document.getElementById("<%=Label5.ClientID%>");
                if (Label5 != null) {
                    $("[id$=Label5]").text("What is your spouse's date of birth?");
                }

                //ValidatorEnable(custValidDay, true);
                //ValidatorEnable(custValidYear, true);
                //ValidatorEnable(custValidMonth, true);
                //ValidatorEnable(custValidDaySpouse, true);
                //ValidatorEnable(custValidYearSpouse, true);
                //ValidatorEnable(custValidMonthSpouse, true);

            }
            else if ($("[id$=rdbtnDivorced]").prop("checked")) {
                $("#dummyDiv").hide();
                $("#btnBackContinueStep2Demo").show();
                $(".hideRow").hide();
                $(".hideRow1").show();
                $(".DivorceText").show();
                $("#dummyDivDemo").hide();
                $(".spacehide").hide();
                $(".BasicInfo").show();
                $("#btnContinueStep2").show();
                $(".BasicInfoDemo").show();

                var lblSpouseBirthDate = document.getElementById("<%=lblSpouseBirthDate.ClientID%>");
                if (lblSpouseBirthDate != null) {
                    document.getElementById("<%=lblSpouseBirthDate.ClientID%>").innerHTML = "What is your Ex-spouse's date of birth?";
                }
                var Label5 = document.getElementById("<%=Label5.ClientID%>");
                if (Label5 != null) {
                    $("[id$=Label5]").text("What is your Ex-spouse's date of birth?");
                }

                //ValidatorEnable(custValidDay, true);
                //ValidatorEnable(custValidYear, true);
                //ValidatorEnable(custValidMonth, true);
                //ValidatorEnable(custValidDaySpouse, true);
                //ValidatorEnable(custValidYearSpouse, true);
                //ValidatorEnable(custValidMonthSpouse, true);
            }
            else if ($("[id$=rdbtnWidowed]").prop("checked") || $("[id$=rdbtnSingle]").prop("checked")) {
                $("#dummyDiv").hide();
                $("#btnBackContinueStep2Demo").show();
                $("#dummyDivDemo").hide();
                $(".hideRow").hide();
                $(".hideRow1").hide();
                $(".spacehide").show();
                $(".DivorceText").hide();
                $(".BasicInfo").show();
                $("#btnContinueStep2").show();
                $(".BasicInfoDemo").show();
                //document.getElementById("lblSpouseBirthDate").innerHTML = "What is your Ex-Spouse's date of birth?";



                //ValidatorEnable(custValidDay, true);
                //ValidatorEnable(custValidYear, true);
                //ValidatorEnable(custValidMonth, true);
                //ValidatorEnable(custValidDaySpouse, false);
                //ValidatorEnable(custValidYearSpouse, false);
                //ValidatorEnable(custValidMonthSpouse, false);


            }
    }


    function ValidationForDemoSpouseName() {
        var isValidate = true;
        var alphaNumericRegex = /^[A-Za-z]+$/;



        /* Check if field exists in current form or not */

        var ddlDemoUserBdate = document.getElementById("<%=ddlDemoUserBdate.ClientID%>");
        if (ddlDemoUserBdate.selectedIndex == 0) {
            $("[id$=ddlDemoUserBdate]").addClass("error");
            $("[id$=lbDdlDemoUserBdateError]").text("*Birth date Required");
            isValidate = false;
        }
        else {
            $("[id$=ddlDemoUserBdate]").removeClass("error");
            $("[id$=lbDdlDemoUserBdateError]").text("");
        }
        if ($("[id$=rdbtnMarried]").prop("checked")) {



            if ($("[id$=txtSpouseDemoName]").val() != null) {
                /* Validate First Name Field */
                if ($("[id$=txtSpouseDemoName]").val() == '') {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=txtSpouseDemoName]").addClass("error");
                    $("[id$=lblSpouseNameError]").text("*Spouse's name Required");
                }
                else {
                    /* Check AlphaNumeric String */
                    if (!alphaNumericRegex.test($("[id$=txtSpouseDemoName]").val())) {
                        isValidate = false;
                        /* Add error css class */
                        $("[id$=txtSpouseDemoName]").addClass("error");
                        $("[id$=lblSpouseNameError]").text("*Spouse's name should be character");
                    }
                    else {
                        /* Remove Css Class */
                        $("[id$=txtSpouseDemoName]").removeClass("error");
                        $("[id$=lblSpouseNameError]").text("");
                    }
                }
            }
        }

        if ($("[id$=rdbtnDivorced]").prop("checked") || $("[id$=rdbtnMarried]").prop("checked")) {

            var ddlDemoSpouseBdate = document.getElementById("<%=ddlDemoSpouseBdate.ClientID%>");
            if (ddlDemoSpouseBdate.selectedIndex == 0) {
                isValidate = false;
                $("[id$=ddlDemoSpouseBdate]").addClass("error");
                $("[id$=lblDdlDemoSpouseBdateError]").text("*Birth date Required");
            }
            else {
                $("[id$=ddlDemoSpouseBdate]").removeClass("error");
                $("[id$=lblDdlDemoSpouseBdateError]").text("");
            }

        }
        return isValidate;
    }



    //Check validation for spuse name, user bdate spouse bdate
    function ValidationForSingleUseBdateSpouseName() {
        var isValidate = true;
        var alphaNumericRegex = /^[A-Za-z]+$/;


        /* Check if field exists in current form or not */

        var ddlMonth_Your = document.getElementById("<%=ddlMonth_Your.ClientID%>");
        if (ddlMonth_Your.selectedIndex == 0) {
            $("[id$=ddlMonth_Your]").addClass("error");
            $("[id$=lblMonthYourError]").text("*Month Required");
            isValidate = false;
        }
        else {
            $("[id$=ddlMonth_Your]").removeClass("error");
            $("[id$=lblMonthYourError]").text("");
        }

        var ddlDay_Your = document.getElementById("<%=ddlDay_Your.ClientID%>");
    if (ddlDay_Your.selectedIndex == 0) {
        $("[id$=ddlDay_Your]").addClass("error");
        $("[id$=lblDayYourError]").text("*Day Required");
        isValidate = false;
    }
    else {
        $("[id$=ddlDay_Your]").removeClass("error");
        $("[id$=lblDayYourError]").text("");
    }
    var ddlYear_Your = document.getElementById("<%=ddlYear_Your.ClientID%>");
    if (ddlYear_Your.selectedIndex == 0) {
        $("[id$=ddlYear_Your]").addClass("error");
        $("[id$=lblYearYourError]").text("*Year Required");
        isValidate = false;
    }
    else {
        $("[id$=ddlYear_Your]").removeClass("error");
        $("[id$=lblYearYourError]").text("");
    }


        //Check User age is less than 70
    if (ddlMonth_Your.selectedIndex != 0 && ddlDay_Your.selectedIndex != 0 && ddlYear_Your.selectedIndex != 0) {

        var day = $("#ddlDay_Your").val();
        var year = $("#ddlYear_Your").val();
        var month = $("#ddlMonth_Your").val();
        var age = calculate_age(month, day, year);
        if (age > 70) {
            isValidate = false;
            $("[id$=lblMonthYourError]").text("*Birth date should not exceed 70 years.");
            $("[id$=ddlDay_Your]").addClass("error");
            $("[id$=ddlYear_Your]").addClass("error");
            $("[id$=ddlMonth_Your]").addClass("error");
        }
        else if (age < 18) {
            isValidate = false;
            $("[id$=lblMonthYourError]").text("*Birth date should not less than 18 years.");
            $("[id$=ddlDay_Your]").addClass("error");
            $("[id$=ddlYear_Your]").addClass("error");
            $("[id$=ddlMonth_Your]").addClass("error");
        }
    }


    if ($("[id$=rdbtnMarried]").prop("checked")) {

        if ($("[id$=txtSpouseFirstname]").val() != null) {
            /* Validate First Name Field */
            if ($("[id$=txtSpouseFirstname]").val() == '') {
                isValidate = false;
                /* Add error css class */
                $("[id$=txtSpouseFirstname]").addClass("error");
                $("[id$=lblSpouseNamerequired]").text("*Spouse's name Required");
            }
            else {
                /* Check AlphaNumeric String */
                if (!alphaNumericRegex.test($("[id$=txtSpouseFirstname]").val())) {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=txtSpouseFirstname]").addClass("error");
                    $("[id$=lblSpouseNamerequired]").text("*Spouse's name should be valid");
                }
                else {
                    /* Remove Css Class */
                    $("[id$=txtSpouseFirstname]").removeClass("error");
                    $("[id$=lblSpouseNamerequired]").text("");
                }
            }
        }
    }

    if ($("[id$=rdbtnDivorced]").prop("checked") || $("[id$=rdbtnMarried]").prop("checked")) {

        var ddlMonth_Spouse = document.getElementById("<%=ddlMonth_Spouse.ClientID%>");
            if (ddlMonth_Spouse.selectedIndex == 0) {
                isValidate = false;
                $("[id$=ddlMonth_Spouse]").addClass("error");
                $("[id$=lblMonthSpouseError]").text("*Birth date Required");
            }
            else {
                $("[id$=ddlMonth_Spouse]").removeClass("error");
                $("[id$=lblMonthSpouseError]").text("");
            }


            var ddlDay_Spouse = document.getElementById("<%=ddlDay_Spouse.ClientID%>");
            if (ddlDay_Spouse.selectedIndex == 0) {
                isValidate = false;
                $("[id$=ddlDay_Spouse]").addClass("error");
                $("[id$=lblDaySpouseError]").text("*Birth date Required");
            }
            else {
                $("[id$=ddlDay_Spouse]").removeClass("error");
                $("[id$=lblDaySpouseError]").text("");
            }

            var ddlYear_Spouse = document.getElementById("<%=ddlYear_Spouse.ClientID%>");
            if (ddlYear_Spouse.selectedIndex == 0) {
                isValidate = false;
                $("[id$=ddlYear_Spouse]").addClass("error");
                $("[id$=lblYearSpouseError]").text("*Birth date Required");
            }
            else {
                $("[id$=ddlYear_Spouse]").removeClass("error");
                $("[id$=lblYearSpouseError]").text("");
            }

            //Check spouse age is less than 70
            if (ddlMonth_Spouse.selectedIndex != 0 && ddlDay_Spouse.selectedIndex != 0 && ddlYear_Spouse.selectedIndex != 0) {
                var day = $("#ddlDay_Spouse").val();
                var year = $("#ddlYear_Spouse").val();
                var month = $("#ddlMonth_Spouse").val();
                var age = calculate_age(month, day, year);
                if (age > 70) {
                    isValidate = false;
                    $("[id$=ddlDay_Spouse]").addClass("error");
                    $("[id$=ddlYear_Spouse]").addClass("error");
                    $("[id$=ddlMonth_Spouse]").addClass("error");
                    $("[id$=lblMonthSpouseError]").text("*Birth date should not exceed 70 years.");
                }
                else if (age < 18) {
                    isValidate = false;
                    $("[id$=ddlDay_Spouse]").addClass("error");
                    $("[id$=ddlYear_Spouse]").addClass("error");
                    $("[id$=ddlMonth_Spouse]").addClass("error");
                    $("[id$=lblMonthSpouseError]").text("*Birth date should not less than 18 years.");
                }
            }

        }




        return isValidate;
    }




    function ValidationForDemoUser() {
        var isValidate = true;
        var alphaNumericRegex = /^[A-Za-z]+$/;


        var RegularExpressionValidatortxtEmail = document.getElementById("<%=RegularExpressionValidatortxtEmail.ClientID%>");
        ValidatorEnable(RegularExpressionValidatortxtEmail, false);
        var reqEmail = document.getElementById("<%=reqEmail.ClientID%>");
        ValidatorEnable(reqEmail, false);

        var UserNameRequired = document.getElementById("<%=UserNameRequired.ClientID%>");
        ValidatorEnable(UserNameRequired, false);
        var revCustomerName = document.getElementById("<%=revCustomerName.ClientID%>");
        ValidatorEnable(revCustomerName, false);
        /* Check if field exists in current form or not */
        if ($("[id$=txtCustomerName]").val() != null) {
            /* Validate First Name Field */
            if ($("[id$=txtCustomerName]").val() == '') {
                isValidate = false;
                /* Add error css class */
                $("[id$=txtCustomerName]").addClass("error");
                $("[id$=lblCustNameError]").text("* First Name Required");
            }
            else {
                /* Check AlphaNumeric String */
                if (!alphaNumericRegex.test($("[id$=txtCustomerName]").val())) {
                    isValidate = false;
                    /* Add error css class */
                    $("[id$=txtCustomerName]").addClass("error");
                    $("[id$=lblCustNameError]").text("* First Name should be Character");
                }
                else {
                    /* Remove Css Class */
                    $("[id$=txtCustomerName]").removeClass("error");
                    $("[id$=lblCustNameError]").text("");
                }
            }
        }


        /* Check if field exists in current form or not */
        if ($("[id$=txtEmail]").val() != null) {
            /* Validate Email Field */
            if ($("[id$=txtEmail]").val() == '') {
                isValidate = false;
                /* Add error css class */
                $("[id$=txtEmail]").addClass("error");
                $("[id$=lblEmailError]").text("* Email Address Required");
            }
            else {
                // var emailfilter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                var emailfilter1 = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
                /* Validation Email Address Format */
                if (!emailfilter1.test($("[id$=txtEmail]").val())) {
                    $("[id$=lblEmailError]").text("* Invalid Email Address");
                    /* Add error css class */
                    $("[id$=txtEmail]").addClass("error");
                    $("[id$=RegularExpressionValidatortxtEmail]").text("");

                    isValidate = false;
                }
                else {
                    /* Remove Css Class */
                    $("[id$=txtEmail]").removeClass("error");
                    $("[id$=lblEmailError]").text("");
                }
            }
        }


        return isValidate;
    }


    function ValidateViewDemo() {

        var isValidate = true;

        var ddlHusbandBenefit = document.getElementById("<%=ddlHusbandBenefit.ClientID%>");
        if (ddlHusbandBenefit.selectedIndex == 0) {
            isValidate = false;
            $("[id$=ddlHusbandBenefit]").addClass("error");
            $("[id$=lblDdlHusbandBenefitError]").text("*Benefit Amount Required");
        }
        else {
            $("[id$=ddlHusbandBenefit]").removeClass("error");
            $("[id$=lblDdlHusbandBenefitError]").text("");
        }

        var ddlWifeBenefit = document.getElementById("<%=ddlWifeBenefit.ClientID%>");
        if (ddlWifeBenefit != null) {
            if (ddlWifeBenefit.selectedIndex == 0) {
                isValidate = false;
                $("[id$=ddlWifeBenefit]").addClass("error");
                $("[id$=lblddlWifeBenefitError]").text("*Benefit Amount Required");
            }
            else {
                $("[id$=ddlWifeBenefit]").removeClass("error");
                $("[id$=lblddlWifeBenefitError]").text("");
            }
        }
        return isValidate;
    }

    //function EnableDisableButton() {
    //    if (('#ChkBoxIAgree').prop('checked')) {
    //        alert('checked');
    //        $('#btnCreateUser').removeAttr('disabled');
    //    }
    //    else {
    //        alert('not checked');
    //        $('#btnCreateUser').attr('disabled', 'disabled');
    //    }
    //}

    </script>
</head>
<body>
    <form id="form1" runat="server" style="max-height: 100px; padding: 0px 0%;">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptRegistration" runat="server" ScriptMode="Release">
        </ajaxToolkit:ToolkitScriptManager>

        <div class="ui-content mainbodydiv" style="background-image: url('/Images/laptop-background2.png'); background-size: cover; max-height: calc(100% - 120px); padding: 0px 0%;" data-role="main">
            <div id="color-overlay_singleUse">
            </div>
            <div>
                <div class="col-md-12 ">
                    <div class="login-symbols_register">
                        <table>
                            <tr>
                                <td>
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <img src="/Images/Newlogo2.png" style="height: 50px; width: 50px;" />
                                                </td>
                                                <td>
                                                    <div style="margin-top: 10px;">
                                                        <h1>
                                                            <p style="font-family: 'Times New Roman'; font-size: 24px;">
                                                                <font color="#2f2e2e">THE PAID TO WAIT</font>
                                                            </p>
                                                        </h1>
                                                    </div>
                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                                </td>


                            </tr>

                            <tr>
                                <td>
                                    <div>
                                        <p style="line-height: 45px; font-size: 29px; padding-bottom: 25px; margin-left: 7px;">
                                            <font color="#2f2e2e">SOCIAL SECURITY CALCULATOR</font>
                                        </p>
                                    </div>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>



            <%--Content--%>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <%if (Session["Demo"] == null)
                      {%>
                    <h1 class="fontBold text-center setTopMargin">Create a New Account</h1>
                    <%}
                      else
                      { %>
                    <h1 class="fontBold text-center setTopMargin">Create a Demo Account
        <%} %>
                        <br />
                    </h1>
                    <div class="col-md-4 col-sm-4"></div>
                    <div class="col-md-8 col-sm-8">
                        <div class="col-md-12 col-sm-12">
                            <div class="col-md-12 col-sm-12">
                                <asp:Panel ID="pnlStep1" DefaultButton="btnStep1" runat="server" Visible="false">
                                    <div class="col-md-12 col-sm-12">
                                        <%--<br />--%>
                                        <h5><b>Please confirm the accuracy of the information you entered and then click the “Continue” button below.</b></h5>
                                        <br />
                                        <asp:Label ID="lblCustomerName" runat="server" AssociatedControlID="lblCustomerName">First Name</asp:Label>
                                        <asp:TextBox ID="txtCustomerName" runat="server" CausesValidation="false" CssClass="textEntry form-control" EnableViewState="False" MaxLength="20" onKeyDown="keyPress('txtCustomerName')" TabIndex="2" ValidationGroup="pnl1Validation"></asp:TextBox>
                                        <asp:Label ID="lblCustNameError" runat="server" CssClass="failureErrorNotification" ForeColor="Red"></asp:Label>
                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="txtCustomerName" CssClass="failureErrorNotification" ErrorMessage="Customer name is required." ToolTip="Customer name is required." ValidationGroup="pnl1Validation" Display="Dynamic">*Customer name is required.</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revCustomerName" runat="server" ControlToValidate="txtCustomerName" CssClass="failureErrorNotification" ErrorMessage="Please enter valid customer name" ValidationExpression="^[A-Za-z][A-Za-z0-9]*$" ValidationGroup="pnl1Validation" Display="Dynamic">*Please enter valid Customer name.</asp:RegularExpressionValidator>
                                    </div>

                                    <div class="col-md-12 col-sm-12">
                                        <br />
                                        <asp:Label ID="lblEmail" runat="server" AssociatedControlID="lblEmail">E-mail</asp:Label>
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="textEntry form-control" autocomplete="off" ValidationGroup="pnl1Validation" TabIndex="3" MaxLength="40" CausesValidation="false"></asp:TextBox>
                                        <asp:Label ID="lblEmailInstruct" Style="font-size: 12px; color: gray;" Text="(Please enter your E-mail address so that we can send your Social Security Report.)" runat="server"></asp:Label><br />
                                        <asp:Label ID="lblEmailError" runat="server" CssClass="failureErrorNotification" ForeColor="Red"></asp:Label>
                                        <asp:RequiredFieldValidator ID="reqEmail" runat="server" ControlToValidate="txtEmail" CssClass="failureErrorNotification" ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="pnl1Validation" Display="Dynamic">*E-mail is required.</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatortxtEmail" runat="server" ControlToValidate="txtEmail" CssClass="failureErrorNotification" ErrorMessage="Please enter valid E-mail" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" ValidationGroup="pnl1Validation" Display="Dynamic">*Please enter valid E-mail</asp:RegularExpressionValidator>
                                    </div>

                                    <%if (Session["Demo"] == null)
                                      { %>

                                    <div class="col-md-12 col-sm-12">
                                        <br />
                                        <asp:Label ID="lblCustomerPassword" runat="server" AssociatedControlID="lblCustomerPassword">Password</asp:Label>
                                        <asp:TextBox ID="txtCustomerPassword" runat="server" CssClass="textEntry form-control" CausesValidation="false" autocomplete="off" onChange="checkPasswordMatch();" TextMode="Password" MaxLength="40" ValidationGroup="pnl1Validation" TabIndex="4"></asp:TextBox>
                                        <asp:Label ID="lblpasswordinstruct" Style="font-size: 12px; color: gray;" Text="(Password must be between 6-32 characters)" runat="server"></asp:Label>
                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="txtCustomerPassword" CssClass="failureErrorNotification" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="pnl1Validation" Display="Dynamic"><br />*Password is required.</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revCustomerPassword" runat="server" ControlToValidate="txtCustomerPassword" CssClass="failureErrorNotification" ErrorMessage="Password length must be in between 6-32" ValidationExpression="^.{6,32}$" ValidationGroup="pnl1Validation" Display="Dynamic"><br />*Password must be between 6-32 characters </asp:RegularExpressionValidator>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <br />
                                        <asp:Label ID="lblCustomerConfirmPassword" runat="server" AssociatedControlID="lblCustomerConfirmPassword">Confirm Password</asp:Label>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <asp:TextBox ID="txtConfirmCustomerPassword" runat="server" CausesValidation="false" CssClass="textEntry form-control" onChange="checkPasswordMatch();" TextMode="Password" MaxLength="40" ValidationGroup="pnl1Validation" TabIndex="5"></asp:TextBox>
                                                <asp:Label ID="lblconfirmpasswordinstruct" Style="font-size: 12px; color: gray;" Text="(Password must be between 6-32 characters)" runat="server"></asp:Label>
                                                <asp:RequiredFieldValidator ID="revConfirmPasswordRequired" runat="server" ControlToValidate="txtConfirmCustomerPassword" CssClass="failureErrorNotification" Display="Dynamic" ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required." ValidationGroup="pnl1Validation"><br />*Confirm Password is required.</asp:RequiredFieldValidator>
                                                <asp:CompareValidator ID="cvPasswordCompare" runat="server" ControlToCompare="txtCustomerPassword" ControlToValidate="txtConfirmCustomerPassword" CssClass="failureErrorNotification" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match." ValidationGroup="pnl1Validation"><br />*The Password and Confirm Password must match.</asp:CompareValidator>
                                                <asp:RegularExpressionValidator ID="regtxtConfirmCustomerPassword" runat="server" ControlToValidate="txtConfirmCustomerPassword" CssClass="failureErrorNotification" ErrorMessage="Confirm Password length must be in between 6-32" ValidationExpression="^.{6,32}$" ValidationGroup="pnl1Validation" Display="Dynamic"><br />*Confirm Password must be between 6-32 characters </asp:RegularExpressionValidator><br />
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <img class="img-responsive" id="imgpass" runat="server" src="/images/confirmPassword.png" style="display: none; margin-left: -25%" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <br />
                                        <asp:Button ID="btnStep1" runat="server" Text="Continue" CssClass="button_login1" ValidationGroup="pnl1Validation" OnClick="btnStep1_Click" TabIndex="6" />
                                        <br />
                                        <br />
                                    </div>
                                    <%}
                                      else
                                      {%>
                                    <div class="col-md-12 col-sm-12">
                                        <br />
                                        <asp:Button ID="btnDemoStep1" runat="server" Text="Continue" OnClientClick="return ValidationForDemoUser();" CssClass="button_login1" OnClick="btnStep1Demo_Click" TabIndex="6" />
                                        <br />
                                        <br />
                                    </div>
                                    <%} %>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-2 col-sm-2"></div>
                    <div class="col-md-10 col-sm-10">
                        <asp:Panel ID="pnlStep2" DefaultButton="btnContinueStep2" runat="server" Visible="true">
                            <div class="col-md-12 col-sm-12">
                                <h5><b>Choose your marital status</b></h5>
                            </div>
                            <div class="row text-center">
                                <br />
                                <div class="col-md-2 col-sm-2">
                                    <asp:Label ID="lblMarr" runat="server" Font-Bold="true" Text="Married"></asp:Label><br />
                                    <asp:RadioButton ID="rdbtnMarried" Checked="false" runat="server" GroupName="MaritalStatus" OnClick="customerSpouseinformation();" TabIndex="4" />
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <asp:Label ID="lbSing" runat="server" Font-Bold="true" Text="Single"></asp:Label><br />
                                    <asp:RadioButton ID="rdbtnSingle" Checked="false" onclick="customerSpouseinformation();" runat="server" GroupName="MaritalStatus" TabIndex="5" />
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <asp:Label ID="lblWid" runat="server" Font-Bold="true" Text="Widowed"></asp:Label><br />
                                    <asp:RadioButton ID="rdbtnWidowed" Checked="false" onclick="customerSpouseinformation();" runat="server" GroupName="MaritalStatus" TabIndex="6" />
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <asp:Label ID="lblDiv" runat="server" Font-Bold="true" Text="Divorced" /><br />
                                    <asp:RadioButton ID="rdbtnDivorced" Checked="false" onclick="customerSpouseinformation();" runat="server" GroupName="MaritalStatus" TabIndex="7" />
                                </div>
                                <div class="col-md-4 col-sm-4">
                                </div>
                            </div>

                            <% if (Session["Demo"] == null)
                               {
                                   if (Session["IsValidPage"] != null)
                                   {
                                       if (Session["IsValidPage"].ToString().Equals("True"))
                                       {%>
                            <div id="dummyDiv" style="height: 200px;"></div>
                            <%}
                                   }%>
                            <div class="row">
                                <div class="BasicInfo" style="height: auto; display: none;">
                                    <div class="col-md-12 col-sm-12 DivorceText displayNone" style="font-size: 14px;">
                                        <br />
                                        <p>You must have been married for at least 10 years before your divorce and not currently remarried in order to use your ex-spouse's benefits in your claiming strategy."</p>
                                        <p>If you were married for less than 10 years before your divorce and are not currently remarried, please change your marital status to "Single".</p>
                                        If you are currently remarried, please change your marital status to "Married" and use your current spouse's Social Security Benefit information.
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <br />
                                        <asp:Label ID="lblBirthDate" runat="server" Text="What is your date of birth?"></asp:Label>
                                    </div>
                                    <div class="clearfix">
                                        <div class="col-md-2 col-sm-2">
                                            Month:
                                <br />
                                            <asp:DropDownList ID="ddlMonth_Your" runat="server" Class="Space" TabIndex="1" Width="100px" />
                                            <br />
                                            <asp:Label ID="lblMonthYourError" CssClass="failureErrorNotification" runat="server"></asp:Label>
                                            <%--<asp:CustomValidator ID="Validator_YourMonth" CssClass="failureErrorNotification" ControlToValidate="ddlMonth_Your" ValidationGroup="RegisterUserValidationGroupStep2" runat="server" ErrorMessage="*Month Required"
                                        ClientValidationFunction="Validate_YourMonth" />--%>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            Day:
                             <br />
                                            <asp:DropDownList ID="ddlDay_Your" runat="server" Class="Space" TabIndex="2" Width="100px" />
                                            <br />
                                            <%--<asp:CustomValidator ID="Validator_YourDay" CssClass="failureErrorNotification" ControlToValidate="ddlDay_Your" ValidationGroup="RegisterUserValidationGroupStep2" runat="server" ErrorMessage="*Day Required"
                                        ClientValidationFunction="Validate_YourDay" />--%>
                                            <asp:Label ID="lblDayYourError" CssClass="failureErrorNotification" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            Year:
                                <br />
                                            <asp:DropDownList ID="ddlYear_Your" runat="server" TabIndex="3" Width="100px" />
                                            <br />
                                            <asp:Label ID="lblYearYourError" CssClass="failureErrorNotification" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6"></div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 ">
                                        <asp:Label ID="lblYourError" CssClass="failureErrorNotification" runat="server" Visible="false">
                                        </asp:Label>
                                    </div>
                                    <div class="col-md-12 col-sm-12 hideRow">
                                        <br />
                                        <asp:Label ID="lblSpouseFirstname" runat="server" Text="What is your spouse's first name?"></asp:Label>
                                        <asp:TextBox ID="txtSpouseFirstname" runat="server" CssClass="textEntry form-control" TabIndex="8" MaxLength="20" ValidationGroup="RegisterUserValidationGroupStep2"></asp:TextBox>
                                        <asp:Label ID="lblSpouseNamerequired" runat="server" Text="" Visible="true" CssClass="failureErrorNotification"></asp:Label>
                                        <asp:RegularExpressionValidator ID="regSpouseFirstname" runat="server" ControlToValidate="txtSpouseFirstname" CssClass="failureErrorNotification" ErrorMessage="Please enter valid spouse name" ValidationExpression="^[A-Za-z][A-Za-z0-9]*$" ValidationGroup="RegisterUserValidationGroupStep2" Display="Dynamic">*Please enter valid spouse name.</asp:RegularExpressionValidator>
                                    </div>
                                    <div class="col-md-12 col-sm-12 hideRow1">
                                        <br />
                                        <asp:Label ID="lblSpouseBirthDate" runat="server" Text="What is your spouse's date of birth"></asp:Label>
                                    </div>
                                    <div class="clearfix hideRow1">
                                        <div class="col-md-2 col-sm-2 ">
                                            Month:
                                <br />
                                            <asp:DropDownList ID="ddlMonth_Spouse" runat="server" Class="Space" TabIndex="9" Width="100px" />
                                            <br />
                                            <%-- <asp:CustomValidator ID="cVddlMonth_Spouse" CssClass="failureErrorNotification" ControlToValidate="ddlMonth_Spouse" ValidationGroup="RegisterUserValidationGroupStep2" runat="server" ErrorMessage="*Month Required"
                                        ClientValidationFunction="Validate_SpouseMonth" />--%>
                                            <asp:Label ID="lblMonthSpouseError" CssClass="failureErrorNotification" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-2 col-sm-2 ">
                                            Day:
                            <br />
                                            <asp:DropDownList ID="ddlDay_Spouse" runat="server" Class="Space" TabIndex="10" Width="100px" />
                                            <br />
                                            <%--<asp:CustomValidator ID="compareddlDay_Spouse" CssClass="failureErrorNotification" ControlToValidate="ddlDay_Spouse" ValidationGroup="RegisterUserValidationGroupStep2" runat="server" ErrorMessage="*Day Required"
                                        ClientValidationFunction="Validate_SpouseDay" />--%>
                                            <asp:Label ID="lblDaySpouseError" CssClass="failureErrorNotification" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-2 col-sm-2 ">
                                            Year:
                                <br />
                                            <asp:DropDownList ID="ddlYear_Spouse" runat="server" TabIndex="11" Width="100px" />
                                            <br />
                                            <%--<asp:CustomValidator ID="Validator_Spouse" CssClass="failureErrorNotification" runat="server" ValidationGroup="RegisterUserValidationGroupStep2" ControlToValidate="ddlYear_Spouse" ErrorMessage="*Year Required"
                                        ClientValidationFunction="Validate_SpouseYear" />--%>
                                            <asp:Label ID="lblYearSpouseError" CssClass="failureErrorNotification" runat="server"></asp:Label>
                                        </div>
                                        <div class="col-md-6 col-sm-6 "></div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 hideRow1">
                                        <asp:Label ID="lblSpouseError" CssClass="failureErrorNotification" runat="server" Visible="false">
                                        </asp:Label>
                                    </div>
                                    <div class="spacehide">
                                        <br />
                                    </div>
                                </div>
                            </div>
                            <div class="row " style="text-align: center; padding-bottom: 80px;">
                                <div class="col-md-1"></div>
                                <div class="col-md-7">
                                    <br />
                                    <asp:Button ID="btnBackStep2" runat="server" Text="Back" CssClass="button_login" TabIndex="13" OnClick="btnBackStep2_Click" />
                                    <asp:Button ID="btnContinueStep2" runat="server" OnClientClick="return ValidationForSingleUseBdateSpouseName();" Text="Continue" CssClass="button_login displayNone" TabIndex="14" OnClick="btnContinueStep2_Click" />

                                </div>
                                <div class="col-md-1"></div>
                                <br />
                                <br />
                            </div>
                            <%}
                               else
                               { 
                            %>
                            <div id="dummyDivDemo" style="height: 200px;"></div>
                            <div class="row">
                                <div class="BasicInfoDemo" style="height: auto">
                                    <div class="col-md-12 col-sm-12 DivorceText displayNone" style="font-size: 14px;">
                                        <br />
                                        <p>You must have been married for at least 10 years before your divorce and not currently remarried in order to use your ex-spouse's benefits in your claiming strategy."</p>
                                        <p>If you were married for less than 10 years before your divorce and are not currently remarried, please change your marital status to "Single".</p>
                                        If you are currently remarried, please change your marital status to "Married" and use your current spouse's Social Security Benefit information.
                                    </div>
                                    <div class="col-md-12 col-sm-12" style="padding-top: 8px">

                                        <asp:Label ID="Label1" runat="server" Text="What is your date of birth?"></asp:Label>
                                    </div>
                                    <div class="clearfix">
                                        <div class="col-md-12 col-sm-12" style="padding-top: 8px">
                                            <asp:DropDownList ID="ddlDemoUserBdate" runat="server" Class="Space" TabIndex="1" Width="124px" />
                                            <br />
                                            <asp:Label ID="lbDdlDemoUserBdateError" CssClass="failureErrorNotification" runat="server"></asp:Label>
                                        </div>

                                    </div>
                                    <div class="col-md-12 col-sm-12 ">
                                        <asp:Label ID="Label2" CssClass="failureErrorNotification" runat="server" Visible="false">
                                        </asp:Label>
                                    </div>

                                    <div class="col-md-12 col-sm-12 hideRow" style="padding-top: 8px">
                                        <asp:Label ID="Label3" runat="server" Style="padding-bottom: 8px" Text="What is your spouse's first name?"></asp:Label>
                                        <asp:TextBox ID="txtSpouseDemoName" runat="server" CssClass="textEntry form-control" TabIndex="8" MaxLength="20" ValidationGroup="RegisterUserValidationGroupDemo"></asp:TextBox>
                                        <asp:Label ID="lblSpouseNameError" runat="server" Visible="true" CssClass="failureErrorNotification"></asp:Label>
                                        <%-- <asp:RequiredFieldValidator ID="reqSposueNameDemo" runat="server" ControlToValidate="txtSpouseDemoName" CssClass="failureErrorNotification" ErrorMessage="Spouse name is required." ToolTip="Spouse name is required." ValidationGroup="RegisterUserValidationGroupDemo" Display="Dynamic">*Spouse name is required.</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regSposueNameDemo" runat="server" ControlToValidate="txtSpouseDemoName" CssClass="failureErrorNotification" ErrorMessage="Please enter valid spouse name" ValidationExpression="^[A-Za-z][A-Za-z0-9]*$" ValidationGroup="RegisterUserValidationGroupDemo" Display="Dynamic">*Please enter valid spouse name.</asp:RegularExpressionValidator>--%>
                                    </div>
                                    <div class="col-md-12 col-sm-12 hideRow1" style="padding-top: 8px">

                                        <asp:Label ID="Label5" Text="What is your spouse's date of birth?" runat="server"></asp:Label>
                                    </div>
                                    <div class="clearfix hideRow1">
                                        <div class="col-md-12 col-sm-12 " style="padding-top: 8px">
                                            <asp:DropDownList ID="ddlDemoSpouseBdate" runat="server" Class="Space" TabIndex="9" Width="124px" />
                                            <br />
                                            <asp:Label ID="lblDdlDemoSpouseBdateError" CssClass="failureErrorNotification" runat="server"></asp:Label>
                                        </div>

                                        <div class="col-md-12 col-sm-12 hideRow1">
                                            <asp:Label ID="Label6" CssClass="failureErrorNotification" runat="server" Visible="false">
                                            </asp:Label>
                                        </div>
                                        <div class="spacehide">
                                            <br />
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="row " style="text-align: center; padding-top: 8px; padding-bottom: 80px;">
                                <div class="col-md-1"></div>
                                <div class="col-md-7">
                                    <asp:Button ID="btnBackDemo" runat="server" Visible="true" Text="Back" CssClass="button_login" TabIndex="13" OnClick="btnBackStep2_Click" />
                                    <asp:Button ID="btnBackContinueStep2Demo" runat="server" Text="Continue" CssClass="button_login BasicInfo" TabIndex="14" OnClientClick="return ValidationForDemoSpouseName();" OnClick="btnContinueStep2Demo_Click" />
                                </div>
                                <div class="col-md-1"></div>
                                <br />
                                <br />
                            </div>
                            <%} %>
                        </asp:Panel>

                        <%--        <asp:UpdatePanel ID="pdtpnl3" OnLoad="OnUpdatePanelLoad" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>--%>
                        <asp:Panel ID="pnlStep3" DefaultButton="btnCreateUser" runat="server" Visible="false">

                            <%if (Session["Demo"] == null)
                              {%>
                            <div class="col-md-12 col-sm-12">
                                <p class="Font16 text-justify">
                                    In order to use the calculator, you must provide the amount of your Monthly Social Security benefit at your Full Retirement Age. 
                    If you don't know the amount of your Full Retirement Age benefit, there are two ways you can obtain it. 
                    One, if you have a recent Social Security Benefit statement that you received in the mail, the amount of your Full Retirement Age benefit can be found on that statement. 
                    Or two, you can click on this link, <a href="https://secure.ssa.gov/RIL/SiView.do"><u>https://secure.ssa.gov/RIL/SiView.do</u></a>, and  it will take you to the page on the Social Security web site where you can set up your own account and get your Social Security benefit statement online. 
                    Your online Social Security benefit statement will provide you with the amount of your Full Retirement Age benefit. 
                    It only takes a few minutes to set up your account and you can return to the web site at any time to get your most up to date Social Security benefit information.
                                </p>
                                <h5><b>Please confirm the accuracy of the information you entered and then click the “Continue” button below.</b></h5>
                                <p class="WarningStyle">(*Do not use dollar symbol or commas, round to the nearest whole number)</p>
                                <asp:Label ID="lblHusbandBenefit" runat="server" Text="What is your Monthly Social Security Full Retirement Age Benefit Amount?"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtHusbandBenefit" runat="server" CssClass="textEntry form-control" TabIndex="1" ValidationGroup="RegisterUserValidationGroupStep3"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqBeneHusband" runat="server" ControlToValidate="txtHusbandBenefit" CssClass="failureErrorNotification" ToolTip="Your Full Retirement Age Benefit is required" ValidationGroup="RegisterUserValidationGroupStep3" Display="Dynamic">*Your Full Retirement Age Benefit is required</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regBeneHusband" runat="server" ControlToValidate="txtHusbandBenefit" CssClass="failureErrorNotification" ValidationExpression="^[0-9]*$" ToolTip="Your Full Retirement Age Benefit must be a number" ValidationGroup="RegisterUserValidationGroupStep3" Display="Dynamic">*Your Full Retirement Age Benefit must be a number</asp:RegularExpressionValidator>
                                <br />
                            </div>
                            <%if (Session["MartialStatus"] != "Single")
                              {%>
                            <div class="col-md-12 col-sm-12">
                                <asp:Label ID="lblWifeBenefit" runat="server" Text="What is your spouse's Monthly Social Security Full Retirement Age Benefit Amount?"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtWifeBenefit" runat="server" CssClass="textEntry form-control" TabIndex="2" ValidationGroup="RegisterUserValidationGroupStep3"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqBeneWife" runat="server" ControlToValidate="txtWifeBenefit" CssClass="failureErrorNotification" ToolTip="Spouse's monthly Full Retirement Age Benefit is required." ValidationGroup="RegisterUserValidationGroupStep3" Display="Dynamic">*Spouse's monthly Full Retirement Age Benefit is required</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regBeneWife" runat="server" ControlToValidate="txtWifeBenefit" CssClass="failureErrorNotification" ValidationExpression="^[0-9]*$" ValidationGroup="RegisterUserValidationGroupStep3" ToolTip="Spouse's monthly Full Retirement Age Benefit must be a number" Display="Dynamic">*Spouse's monthly Full Retirement Age Benefit must be a number</asp:RegularExpressionValidator>
                            </div>
                            <%}%>
                            <div class="col-md-12 col-sm-12" style="padding-bottom: 120px;">
                                <br />
                                <asp:CheckBox ID="ChkBoxIAgree" runat="server" />
                                <asp:LinkButton OnClick="ShowAgreementDetails" ID="linkAgreement" ForeColor="Black" Font-Size="Small" runat="server">I agree with terms & conditions</asp:LinkButton>
                                <br />
                                <asp:CustomValidator ID="custCheckBoxAgree" runat="server" OnServerValidate="custCheckBoxAgree_ServerValidate" CssClass="failureErrorNotification" ValidationGroup="RegisterUserValidationGroupStep3" ErrorMessage="*Terms & conditions must be accepted" Display="Dynamic">*Terms & conditions must be accepted</asp:CustomValidator>
                                <br />
                                <asp:Button ID="btnBackStep3" runat="server" TabIndex="3" Text="Back" CssClass="button_login" OnClick="btnBackStep3_Click" />
                                <asp:Button ID="btnCreateUser" runat="server" TabIndex="4" Text="Continue" CssClass="button_login" ValidationGroup="RegisterUserValidationGroupStep3" OnClick="btnCreateUser_Click" />
                                <br />
                                <br />
                                <asp:Label ID="lblUserCreate" CssClass="setGreen" runat="server" Visible="False" Style="text-align: center"></asp:Label>
                            </div>

                            <%}
                              else
                              {%>
                            <div class="col-md-12 col-sm-12">
                                <%--<p class="Font16 text-justify">
                            In order to use the calculator, you must provide the amount of your Monthly Social Security benefit at your Full Retirement Age. 
                    If you don't know the amount of your Full Retirement Age benefit, there are two ways you can obtain it. 
                    One, if you have a recent Social Security Benefit statement that you received in the mail, the amount of your Full Retirement Age benefit can be found on that statement. 
                    Or two, you can click on this link, <a href="https://secure.ssa.gov/RIL/SiView.do"><u>https://secure.ssa.gov/RIL/SiView.do</u></a>, and  it will take you to the page on the Social Security web site where you can set up your own account and get your Social Security benefit statement online. 
                    Your online Social Security benefit statement will provide you with the amount of your Full Retirement Age benefit. 
                    It only takes a few minutes to set up your account and you can return to the web site at any time to get your most up to date Social Security benefit information.
                        </p>
                        <h5><b>Please confirm the accuracy of the information you entered and then click the “Continue” button below.</b></h5>
                        <p class="WarningStyle">(*Do not use dollar symbol or commas, round to the nearest whole number)</p>--%>
                                <asp:Label ID="Label4" runat="server" Text="What is your Monthly Social Security Full Retirement Age Benefit Amount?"></asp:Label>
                                <br />
                                <div class="col-md-3 col-sm-3 " style="padding-top: 8px">
                                    <asp:DropDownList ID="ddlHusbandBenefit" runat="server" Class="Space" TabIndex="9" Width="100px" /><br />
                                    <%--<asp:CustomValidator ID="Validate_DemoHusbBenefit" CssClass="failureErrorNotification" ControlToValidate="ddlHusbandBenefit" ValidationGroup="RegisterUserValidationGroupBenefit" runat="server" ErrorMessage="*Benefit Required"
                                ClientValidationFunction="Validate_DemoHusbBenefit" />--%>
                                    <asp:Label ID="lblDdlHusbandBenefitError" CssClass="failureErrorNotification" runat="server"></asp:Label>
                                </div>
                            </div>
                            <%if (Session["MartialStatus"] != "Single")
                              {%>

                            <div class="col-md-12 col-sm-12" style="padding-top: 8px">
                                <asp:Label ID="Label7" runat="server" Text="What is your spouse's Monthly Social Security Full Retirement Age Benefit Amount?"></asp:Label>
                                <br />
                                <div class="col-md-3 col-sm-3 " style="padding-top: 8px">
                                    <asp:DropDownList ID="ddlWifeBenefit" runat="server" Class="Space" TabIndex="9" Width="100px" />
                                    <br />
                                    <%--<asp:CustomValidator ID="Validate_DemoWifeBenefit" CssClass="failureErrorNotification" ControlToValidate="ddlWifeBenefit" ValidationGroup="RegisterUserValidationGroupBenefit" runat="server" ErrorMessage="*Benefit Required"
                                ClientValidationFunction="Validate_DemoWifeBenefit" />--%>
                                    <asp:Label ID="lblddlWifeBenefitError" CssClass="failureErrorNotification" runat="server"></asp:Label>
                                </div>
                            </div>
                            <%}%>
                            <div class="col-md-12 col-sm-12" style="padding-bottom: 120px;">
                                <br />
                                <asp:Button ID="Button1" runat="server" TabIndex="3" Text="Back" CssClass="button_login" OnClick="btnBackStep3_Click" />
                                <asp:Button ID="Button2" runat="server" TabIndex="4" Text="View Your Strategy" OnClientClick="return ValidateViewDemo();" CssClass="button_login" OnClick="btnCreateDemoUser_Click" />
                                <br />
                                <br />
                                <asp:Label ID="Label8" CssClass="setGreen" runat="server" Visible="False" Style="text-align: center"></asp:Label>
                            </div>
                            <%} %>
                        </asp:Panel>
                        <%--   </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>--%>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12" style="text-align: center">
                    <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
                </div>
                <br />
                <br />
            </div>





            <asp:Panel ID="pnlAgreement" ScrollBars="Auto" runat="server" Style="height: 500px; margin-top: -60px" CssClass="displayNone ExplanationModalPopup">
                <div class="h2PopUp modal-header" style="color: white; font-size: x-large; font: bold;">
                    Consumer Single User Subscription Agreement
                </div>
                <div class="textExplanation plan-contentnew">
                    <div class="bs-example ">
                        <div class="panel-group" id="accordion">
                            <p class="rti-colora1">
                                BY ACCEPTING THIS AGREEMENT, EITHER BY CLICKING A BOX INDICATING YOUR ACCEPTANCE OR BY EXECUTING AN ORDER FORM THAT REFERENCES THIS AGREEMENT, YOU AGREE TO THE TERMS OF THIS AGREEMENT. IF YOU DO NOT AGREE WITH THESE TERMS AND CONDITIONS, YOU MUST NOT ACCEPT THIS AGREEMENT AND MAY NOT USE THE SERVICES.<br />
                                <br />
                                The Social Security Calculator at www.calculatemybenefits.com (hereinafter referred to as the “Web Site”) is comprised of various web pages operated by Filtech for the purpose of providing consumers with analysis and comparisons of Social Security and other retirement strategies. The Web Site is a paid service offered to you conditioned on your acceptance without modification of the terms, conditions, and notices contained herein. Your use of the Web Site constitutes your agreement to all such terms, conditions, and notices.<br />
                                <br />
                                Filtech reserves the right to change the terms, conditions, and notices under which the Web Site is offered, including but not limited to the charges associated with the use of the Web Site.
                        <br />
                                <br />
                            </p>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">License Terms and Restrictions&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <p style="font: 100; font-size: large">
                                            Application License.
                                        </p>
                                        <p class="rti-colora1">
                                            Filtech hereby grants you a non-exclusive, non-transferable, worldwide right to use the Web Site during the term of this Agreement, solely for your own informational purposes, subject to the terms and conditions contained herein. All rights not expressly granted to you are reserved by Filtech and its licensors. You are granted a single use license, however, each additional user must obtain a separate license for their use. You may not access the Web Site if you are a direct competitor of Filtech, except with Filtech’s prior written consent. In addition, you may not access the Web Site for purposes of monitoring its availability, performance or functionality, or for any other benchmarking or competitive purposes.<br />
                                            <br />
                                        </p>

                                        <p style="font: 100; font-size: large">
                                            Additional License Restrictions.
                                        </p>
                                        <p class="rti-colora1">
                                            You shall not (i) license, sublicense, sell, resell, transfer, assign, distribute or otherwise commercially exploit or make available to any third party the Web Site in any way; (ii) modify or make derivative works based upon the Web Site; (iii) create Internet "links" to the Web Site or "frame" or "mirror" any web pages or content on any other server or wireless or Internet-based device; or (iv) reverse engineer or access the Web Site in order to (a) build a competitive product or service, (b) build a product using similar ideas, features, functions or graphics of the Web Site, or (c) copy any ideas, features, functions or graphics of the Web Site. User licenses cannot be shared or used by more than one individual User. You may use the Web Site only for your informational purposes and shall not: (i) knowingly interfere with or disrupt the integrity or performance of the Web Site or the integrity of the data contained therein; or (ii) attempt to gain unauthorized access to the Web Site or its related systems or networks. Your access will be limited to information and portions of databases relating solely to your subscription.<br />
                                            <br />
                                        </p>

                                        <p style="font: 100; font-size: large">
                                            Account Data.
                                        </p>
                                        <p class="rti-colora1">
                                            Filtech does not own any data, information or material that you submit to the Web Site in the course of using the informational tools ("Customer Data"). You, not Filtech, shall have sole responsibility for the accuracy, quality, integrity, legality, reliability, appropriateness, and intellectual property ownership or right to use of all Customer Data, and Filtech shall not be responsible or liable for the deletion, correction, destruction, damage, loss or failure to store any Customer Data. Filtech reserves the right to withhold, remove and/or discard Customer Data without notice for any breach, including, without limitation, your non-payment. Upon termination for cause, your right to access or use Customer Data immediately ceases, and Filtech shall have no obligation to maintain any Customer Data.<br />
                                            <br />
                                        </p>

                                        <p style="font: 100; font-size: large">
                                            Intellectual Property Ownership.
                                        </p>
                                        <p class="rti-colora1">
                                            Filtech alone (and its licensors, where applicable) shall own all right, title and interest, including all related Intellectual Property Rights, in and to the Web Site, reports generated from the Web Site, and any updates or subsequent revisions thereof, and any other intellectual property of Filtech provided to the User, including but not limited to any patents, copyrights, trademarks, service marks, trade names, service names, or logos now owned or that may be owned in the future by Filtech.<br />
                                            <br />
                                        </p>

                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Use of the Web Site&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p style="font: 100; font-size: large">
                                            Filtech Responsibilities.
                                        </p>
                                        <p class="rti-colora1">
                                            We shall: (i) use commercially reasonable efforts to make the Web Site available 24 hours a day, 7 days a week, except for: (a) planned downtime (of which We shall provide prior notice), or (b) any unavailability caused by circumstances beyond our reasonable control, including without limitation, acts of God, acts of government, floods, fires, earthquakes, civil unrest, acts of terror, strikes or other labor problems, Internet service provider failures or delays, or denial of service attacks. Furthermore, we shall maintain appropriate administrative and technical safeguards for protection of the security, confidentiality and integrity of User data.  We shall not (a) modify User data, (b) disclose User data except as compelled by law in accordance with the Compelled Legal Disclosure section below or as expressly permitted in writing by you, or (c) access User data except to provide the services and prevent or address service or technical problems, or at your request in connection with customer support matters.<br />
                                            <br />
                                        </p>

                                        <p style="font: 100; font-size: large">
                                            Compelled Legal Disclosure.
                                        </p>
                                        <p class="rti-colora1">
                                            Filtech reserves the right at all times to disclose any information as necessary to satisfy any applicable law, regulation, legal process or governmental request, or to edit, refuse to post or to remove any information or materials, in whole or in part, in Filtech's sole discretion.<br />
                                            <br />
                                        </p>

                                        <p style="font: 100; font-size: large">
                                            Financial Calculations and Informational Content.
                                        </p>
                                        <p class="rti-colora1">
                                            The Web Site offers financial calculations and informational content to provide consumers with Social Security and retirement planning information. Filtech does not guarantee the accuracy of the calculations, information, reports, or their applicability to your particular situation. Calculations are based upon federal laws/regulations and information you enter, and will change with each use and over time. Filtech accepts no liability for your use or misuse of the site. In no way does Filtech guarantee or represent that you will achieve the projected results illustrated on the Web Site.<br />
                                            <br />
                                        </p>

                                        <p style="font: 100; font-size: large">
                                            Links to Third Party Sites.
                                        </p>
                                        <p class="rti-colora1">
                                            The Web Site may contain links to other web sites ("Linked Sites"). The Linked Sites are not under the control of Filtech and we are not responsible for the contents of any Linked Site, including without limitation any link contained in a Linked Site, or any changes or updates to a Linked Site. Filtech is not responsible for web casting or any other form of transmission received from any Linked Site. Filtech is providing these links to you only as a convenience, and the inclusion of any link does not imply endorsement by us of the site or any association with its operators.<br />
                                            <br />
                                        </p>
                                        <p style="font: 100; font-size: large">
                                            User Responsibilities.
                                        </p>
                                        <p class="rti-colora1">
                                            You shall (i) be responsible for compliance with this Agreement, (ii) be responsible for the accuracy, quality and legality of your data and of the means by which you acquired it, (iii) use commercially reasonable efforts to prevent unauthorized access to or use of the Web Site, and notify Filtech promptly of any such unauthorized access or use, and (iv) use the Web Site only in accordance with the applicable laws and government regulations. You shall not (a) make the Web Site available to anyone other than yourself, (b) sell, resell, rent or lease the Web Site, (c) use the Web Site to store or transmit infringing, libelous, or otherwise unlawful or tortious material, or to store or transmit material in violation of third-party privacy rights, (d) use the Web Site to store or transmit Malicious Code, (e) interfere with or disrupt the integrity or performance of the Web Site or third-party data contained therein, or (f) attempt to gain unauthorized access to the Web Site or their related systems or networks.<br />
                                            <br />
                                        </p>

                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Term and Termination&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p style="font: 100; font-size: large">
                                            Initial Term and Renewal.
                                        </p>
                                        <p class="rti-colora1">
                                            The initial term will commence as of the date you accept this agreement. The Initial Term is for a one-time use. You agree and acknowledge that Filtech has no obligation to retain the Customer Data, and may delete such Customer Data at Filtech’s sole discretion.<br />
                                            <br />
                                        </p>

                                        <p style="font: 100; font-size: large">
                                            Termination for Material Breach. 
                                        </p>
                                        <p class="rti-colora1">
                                            Any breach of your payment obligations or unauthorized use of the Web Site will be deemed a material breach of this Agreement. Filtech, in its sole discretion, may terminate your password, account or use of the Web Site if you breach or otherwise fail to comply with this Agreement.
                                    <br />
                                            <br />
                                        </p>

                                        <p style="font: 100; font-size: large">
                                            Free Trial.
                                        </p>
                                        <p class="rti-colora1">
                                            If you register for a free trial, we will make the Web Site available to you on a trial basis free of charge until the earlier of (a) the end of the free trial period for which you registered or are registering to use the Web Site or (b) the start date of any purchased services ordered by you. Additional trial terms and conditions may appear on the trial registration web page.  Any such additional terms and conditions are incorporated into this Agreement by reference and are legally binding.<br />
                                            <br />
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Fees and Payment Terms&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p style="font: 100; font-size: large">
                                            Web Site Fees.
                                        </p>
                                        <p class="rti-colora1">
                                            Upon execution of this Agreement, Filtech shall invoice subscriber $__________________ for a single-use in consideration for the services provided to subscriber.<br />
                                            <br />
                                        </p>

                                        <p style="font: 100; font-size: large">
                                            Payment Terms.
                                        </p>
                                        <p class="rti-colora1">
                                            All invoices are due electronically at date of use. All payments shall be made in U.S. dollars and shall be non-refundable. If subscriber is delinquent in payments, Filtech may suspend or cancel access to Web Site.<br />
                                            <br />
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Limitations of Liability&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                                    </h4>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p style="font: 100; font-size: large">
                                            Mutual Indemnification.
                                        </p>
                                        <p class="rti-colora1">
                                            You shall indemnify and hold Filtech, its licensors and each such party's parent organizations, subsidiaries, affiliates, officers, directors, employees, attorneys and agents harmless from and against any and all claims, costs, damages, losses, liabilities and expenses (including attorneys' fees and costs) arising out of or in connection with: (i) a claim alleging that use of the Customer Data infringes the rights of, or has caused harm to, a third party; (ii) a claim, which if true, would constitute a violation by you of your representations and warranties; or (iii) a claim arising from the breach by you or your Users of this Agreement, provided in any such case that Filtech (a) gives written notice of the claim promptly to you; (b) gives you sole control of the defense and settlement of the claim (provided that you may not settle or defend any claim unless you unconditionally release Filtech of all liability and such settlement does not affect Filtech's business or Web Site); (c) provides to you all available information and assistance; and (d) has not compromised or settled such claim.<br />
                                            <br />
                                            Filtech shall indemnify and hold you harmless from and against any and all claims, costs, damages, losses, liabilities and expenses (including attorneys' fees and costs) arising out of or in connection with: (i) a claim alleging that the Web Site directly infringes a copyright, a U.S. patent issued as of the Effective Date, or a trademark of a third party; (ii) a claim, which if true, would constitute a violation by Filtech of its representations or warranties; or (iii) a claim arising from breach of this Agreement by Filtech; provided that you (a) promptly give written notice of the claim to Filtech; (b) give Filtech sole control of the defense and settlement of the claim (provided that Filtech may not settle or defend any claim unless it unconditionally releases you of all liability); (c) provide to Filtech all available information and assistance; and (d) have not compromised or settled such claim. Filtech shall have no indemnification obligation, and you shall indemnify Filtech pursuant to this Agreement, for claims arising from any infringement arising from the combination of the Web Site with any of your products, service, hardware or business process(s).<br />
                                            <br />
                                        </p>

                                        <p style="font: 100; font-size: large">
                                            Disclaimer of Warranties.
                                        </p>
                                        <p class="rti-colora1">
                                            FILTECH AND ITS LICENSORS MAKE NO REPRESENTATION, WARRANTY, OR GUARANTY AS TO THE RELIABILITY, TIMELINESS, QUALITY, SUITABILITY, TRUTH, AVAILABILITY, ACCURACY OR COMPLETENESS OF THE WEB SITE OR ANY CONTENT. FILTECH AND ITS LICENSORS DO NOT REPRESENT OR WARRANT THAT (A) THE USE OF THE WEB SITE WILL BE SECURE, TIMELY, UNINTERRUPTED OR ERROR-FREE OR OPERATE IN COMBINATION WITH ANY OTHER HARDWARE, SOFTWARE, SYSTEM OR DATA, (B) THE WEB SITE WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS, (C) ANY STORED DATA WILL BE ACCURATE OR RELIABLE, (D) THE QUALITY OF ANY PRODUCTS, SERVICES, INFORMATION, OR OTHER MATERIAL PURCHASED OR OBTAINED BY YOU THROUGH THE WEB SITE WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS, (E) ERRORS OR DEFECTS WILL BE CORRECTED, OR (F) THE WEB SITE OR THE SERVER(S) THAT MAKE THE WEB SITE AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. THE WEB SITE AND ALL CONTENT IS PROVIDED TO YOU STRICTLY ON AN "AS IS" BASIS. ALL CONDITIONS, REPRESENTATIONS AND WARRANTIES, WHETHER EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT OF THIRD PARTY RIGHTS, ARE HEREBY DISCLAIMED TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW BY FILTECH AND ITS LICENSORS.<br />
                                            <br />
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">General Provisions&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseSix" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                                    </h4>
                                </div>
                                <div id="collapseSix" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p style="font: 100; font-size: large">
                                            Severability.
                                        </p>
                                        <p class="rti-colora1">
                                            If any provision of this Agreement is held by a court of competent jurisdiction to be contrary to law, the provision shall be modified by the court and interpreted so as best to accomplish the objectives of the original provision to the fullest extent permitted by law, and the remaining provisions of this Agreement shall remain in effect.<br />
                                            <br />

                                        </p>

                                        <p style="font: 100; font-size: large">
                                            Governing Law.
                                        </p>
                                        <p class="rti-colora1">
                                            This Agreement will be governed by the laws of the state of New York.<br />
                                            <br />
                                        </p>
                                        <p style="font: 100; font-size: large">
                                            Export Compliance.
                                        </p>
                                        <p class="rti-colora1">
                                            The Web Site and any other technology Filtech makes available, and derivatives thereof may be subject to export laws and regulations of the United States and other jurisdictions. Each party represents that it is not named on any U.S. government denied-party list. You shall not permit users to access or use the Web Site in a U.S.-embargoed country (currently Cuba, Iran, North Korea, Sudan or Syria) or in violation of any U.S. export law or regulation.<br />
                                            <br />
                                        </p>
                                        <p style="font: 100; font-size: large">
                                            Relationship of the Parties.
                                        </p>
                                        <p class="rti-colora1">
                                            The parties are independent contractors. This Agreement does not create a partnership, franchise, joint venture, agency, fiduciary or employment relationship between the parties.<br />
                                            <br />
                                        </p>
                                        <p style="font: 100; font-size: large">
                                            Entire Agreement.
                                        </p>
                                        <p class="rti-colora1">
                                            This Agreement, including all exhibits and addenda hereto and all Order Forms, constitutes the entire agreement between the parties and supersedes all prior and contemporaneous agreements, proposals or representations, written or oral, concerning its subject matter.<br />
                                            <br />
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <br />
                        </div>
                    </div>
                    <p class="text-center fontBold" style="color: #85312F">
                        <asp:Button ID="btnDownload" runat="server" Text="Download" OnClick="DownloadTermsCondition" CssClass="button_login" />
                        <asp:Button ID="btnCancel" runat="server" CssClass="button_login " OnClick="btnCancel_Click" Text="Cancel" />
                        <%--OnClick="exportToPdf" OnClick="imgCancel_Click"--%>
                    </p>
                </div>
            </asp:Panel>
            <asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
            <cc1:ModalPopupExtender ID="popUpAgreementDetails" runat="server" PopupControlID="pnlAgreement" TargetControlID="lnkFake" BackgroundCssClass="modalBackground">
            </cc1:ModalPopupExtender>




            <div class="notification" style="display: none;">
                <div class="notification-overlay"></div>
                <div class="notification-inner" style="left: 325px;">
                    <img rel="//storage.googleapis.com/instapage-app-assets/336/img/loading_circle.svg" src="//storage.googleapis.com/instapage-app-assets/336/img/loading_circle.svg" class="loading notification-loader" alt="" style="display: none;">
                    <span class="message">Processing...</span>
                    <%-- <span class="close-button" onclick="jQuery(this).parent().parent().hide()">Close</span>--%>
                </div>
            </div>
        </div>
        <%--<div class="footerbg" style="padding-top: 0px">
            <div>
                <div class="clearfix">
                    <div class="col-md-12 column removepaddingleftright">
                        <div id="Div1" class="divFoot" runat="server">
                            <table class="table-responsive">
                                <tr class="col-md-12 col-sm-12">
                                    <td class="col-md-2 col-sm-2"></td>
                                    <td class="col-md-3 col-sm-3">
                                        <div id="footer-left" style="line-height: 22.4px; padding-top: 25px; font-size: 16px;">
                                            &copy; 2016 Filtech, LLC. All Rights Reserved.
                                                <br />
                                            3056 New Williamsburg Dr Schenectady NY, 12303
                                            <br />
                                            Website developed by <a style="color: Highlight" href="http://www.alohatechnology.com/"><u>Aloha Technology</u></a>&nbsp;&nbsp;and&nbsp;&nbsp;<a style="color: Highlight" href="http://www.ictusmg.com/"><u>Ictus Marketing Group</u></a>
                                            <br />
                                            <p style="padding-top: 10px;">
                                                <a href="../FrmCancelPolicy.aspx" style="color: white"><u>Cancellation and Refund Policy </u></a>&nbsp; &nbsp; &nbsp; <a href="../FrmPrivacyPolicy.aspx" style="color: white"><u>Privacy Policy </u></a>
                                            </p>
                                            <br />
                                        </div>
                                    </td>
                                    <td class="col-md-2 col-sm-2">
                                        <table style="width: 100px">
                                            <tr>
                                                <td>
                                                    <a href="https://www.linkedin.com/in/brian-doherty-4359386a">
                                                        <img src="/Images/linkedinicon.png" class="img-responsive" />
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="https://www.facebook.com/Brian-Doherty-624778240952341/">
                                                        <img src="/Images/fbicon.png" class="img-responsive" />
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="https://twitter.com/BrianDoherty57">
                                                        <img src="/Images/twittericon.png" class="img-responsive" />
                                                    </a>

                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>--%>

        <div class="footerbg">
            <div>
                <div class="clearfix">
                    <div class="col-md-12 column removepaddingleftright">
                        <div id="ctl00_footer" style="background-color: #333333; height: auto; text-align: center;">
                            <table class="table-responsive">
                                <tbody>
                                    <tr class="col-md-12 col-sm-12">
                                        <td class="col-md-2 col-sm-2"></td>
                                        <td class="col-md-3 col-sm-3">
                                            <div id="footer-left" style="line-height: 22.4px; padding-top: 25px; font-size: 16px;">
                                                &copy; 2016 Filtech, LLC. All Rights Reserved.
                                                <br>
                                                3056 New Williamsburg Dr Schenectady NY, 12303
                                            <br>
                                                Website developed by <a style="color: Highlight" href="http://www.alohatechnology.com/"><u>Aloha Technology</u></a>&nbsp;&nbsp;and&nbsp;&nbsp;<a style="color: Highlight" href="http://www.ictusmg.com/"><u>Ictus Marketing Group</u></a>
                                                <br>
                                                <p style="padding-top: 10px;">
                                                    <%if (Session["UserID"] != null)
                                                      {%>
                                                    <a href="../FrmCancelPolicy.aspx" style="color: white"><u>Cancellation and Refund Policy </u></a>&nbsp; &nbsp; &nbsp; <a href="../FrmPrivacyPolicy.aspx" style="color: white"><u>Privacy Policy </u></a>
                                                    <%}
                                                      else
                                                      { %>
                                                    <a href="../CancelPolicy.aspx" style="color: white"><u>Cancellation and Refund Policy </u></a>&nbsp; &nbsp; &nbsp; <a href="../PrivacyPolicy.aspx" style="color: white"><u>Privacy Policy </u></a>
                                                    <%} %>
                                                </p>
                                                <br>
                                            </div>
                                        </td>
                                        <td class="col-md-2 col-sm-2">
                                            <table style="width: 100px">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <a href="https://www.linkedin.com/in/brian-doherty-4359386a">
                                                                <img src="/Images/linkedinicon.png" class="img-responsive">
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="https://www.facebook.com/Brian-Doherty-624778240952341/">
                                                                <img src="/Images/fbicon.png" class="img-responsive">
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="https://twitter.com/BrianDoherty57">
                                                                <img src="/Images/twittericon.png" class="img-responsive">
                                                            </a>

                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>
</body>
</html>
