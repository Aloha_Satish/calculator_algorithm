﻿<%@ Page Title="About Us" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Calculate.About" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MainContent">
    <script type="text/javascript">
        $(document).ready(function () {
            <%if (Session["UserID"] == null)
              {%>
            $('#content').attr("style", "border:none;");
        });
        <%}%>

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <h1 class="headfontsize divAboutLabelStyle setTopMargin">About us</h1>
    <div id="divAbout" runat="server" class="col-md-12 col-sm-12 divBg"></div>
</asp:Content>

