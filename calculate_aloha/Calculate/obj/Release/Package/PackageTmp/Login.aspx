﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Calculate.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <%--<link rel="stylesheet" media="(max-width:600px)" href="/css/smalldevice.css">--%>
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap-theme.css" rel="stylesheet" />
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap-dialog.css" rel="stylesheet" />
    <link href="/css/starter-template.css" rel="stylesheet" />
    <title>Log in</title>
    <link rel="stylesheet" type="text/css" href="/css/main.css" />
    <link rel="stylesheet" type="text/css" href="/css/forms.css" />
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="/css/interior.css" />
    <link rel="stylesheet" type="text/css" href="/css/jquery-te-1.4.0.css" />
    <link href="/Styles/Calculate.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/css/ui-lightness/jquery-ui-1.10.3.custom.css" />
    <script src="../Scripts/jquery-1.7.2.min.js"></script>
    <script src="../Scripts/hashchange.min.js"></script>
    <script>var $1_7_1 = jQuery.noConflict();</script>
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/blockUI.min.js"></script>
    <script type="text/javascript" src="../Scripts/common.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />

    <%--  <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>--%>

    <!-- Include all compiled plugins (below), or include individual files as needed -->

    <script src="/css/bootstrap-3.2.0-dist/js/bootstrap.min.js"></script>
    <script src="/css/bootstrap-3.2.0-dist/js/bootstrap-dialog.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#loginLink").click(function (e) {
                //$('label[id*="lblEmailError"]').text('');
                //$('label[id*="lblPwdError"]').text('');
                ShowDialog(true);
                e.preventDefault();
            });

            //$('#txtEmailAddress').focusout(function () {
            //    ValidateLoginDetails();
            //});

            $('#txtPassword').focusout(function () {
                ValidateLoginDetails();
            });

        });

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div id="overlay" class="web_dialog_overlay">
                <div id="color-overlay">
                    <div class="login-symbols">
                        <table>
                            <tr>
                                <td>
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <img src="/Images/Calculator-Logo.png" style="height: 50px; width: 50px;" />
                                                </td>
                                                <td>
                                                    <div style="margin-top: 10px;">
                                                        <h1>
                                                            <p style="line-height: 34px; font-family: 'Times New Roman'; font-size: 24px;">
                                                                <font color="#ffffff">THE PAID TO WAIT</font>
                                                            </p>
                                                        </h1>
                                                    </div>
                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                                </td>


                            </tr>

                            <tr>
                                <td>
                                    <div>
                                        <p style="line-height: 45px; font-size: 29px; margin-left: 7px; font-family: 'Times New Roman'">
                                            <font color="#ffffff">SOCIAL SECURITY CALCULATOR</font>
                                        </p>
                                    </div>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>

                <div class="web_dialog" style="display: block;">
                    <asp:Panel ID="pnlLogin" runat="server" DefaultButton="btnHomeLogin">
                        <div style="margin-left: 41px;">
                            <center>
                        <img src="Images/B-Doherty Logo.png" style="height: 42px; width: 196px; margin-right: 44px;" />
                            </center>
                            <div class="contents">
                                <p style="line-height: 22.4px; font-size: 16px; padding-top: 10px;">Login to your account. </p>

                            </div>
                            <asp:TextBox ID="txtEmailAddress" runat="server" MaxLength="40" placeholder="Email" TabIndex="2" CssClass="login-textboxes"></asp:TextBox>

                            <br />
                            <br />
                            <asp:TextBox runat="server" ID="txtPassword" autocomplete="off" TextMode="Password" placeholder="Password" CssClass="login-textboxes" TabIndex="3" MaxLength="32"></asp:TextBox>
                            <br />

                            <br />
                            <div style="opacity: 1">
                                <asp:Button ID="btnHomeLogin" Text="Login Now" runat="server" TabIndex="4" OnClientClick="return ValidateLoginDetails();" OnClick="checkLoginDetails" class="login-buttons" />
                                &nbsp;&nbsp;
                                <br />
                                <br />
                            </div>

                            <a href="ForgotPassword.aspx" style="color: black; line-height: 22.4px; font-size: 16px;">Forgot Your Password?</a>
                            <br />
                            <br />
                            <a href="http://www.gettingpaidtowait.com/calculator" target="_blank" style="color: black; line-height: 22.4px; font-size: 16px;">Need to create an account?</a>
                            <br />
                            <br />
                            <%--<center>
                            <a href="Home.aspx" style="color: black; line-height: 11.4px; color:blue; margin-right: 57px; font-size: 13px;"><u>Back to home screen</u> </a>
                                </center>--%>
                        </div>

                    </asp:Panel>
                    <div>
                        <div id="errordiv" class="email-form-messagebox-wrapper" style="top: 88px; left: 357px; display: none;">
                            <div class="email-form-messagebox-header">Please Fix These Errors</div>
                            <div class="email-form-messagebox" style="background-color: white;">
                                <span>
                                    <asp:Label ID="lblEmailError" ForeColor="Red" Font-Size="12px" runat="server" Visible="true"></asp:Label>
                                </span>
                                <span>
                                    <asp:Label ID="lblPwdError" ForeColor="Red" Font-Size="12px" runat="server" Text="" Visible="true"></asp:Label>
                                </span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="footerbg" style="padding-top: 630px">
                    <div>
                        <div class="clearfix">
                            <div class="col-md-12 column removepaddingleftright">
                                <div id="Div1" class="divFoot" runat="server">
                                    <table class="table-responsive">
                                        <tr class="col-md-12 col-sm-12">
                                            <td class="col-md-2 col-sm-2"></td>
                                            <td class="col-md-3 col-sm-3">
                                                <div id="footer-left" style="line-height: 22.4px; font-size: 16px;">
                                                    &copy; 2017 Filtech, LLC. All Rights Reserved.
                                                <br />
                                                    3056 New Williamsburg Dr Schenectady NY, 12303
                                            <br />
                                                    Website developed by <a style="color: Highlight" href="http://www.alohatechnology.com/"><u>Aloha Technology</u></a>&nbsp;&nbsp;and&nbsp;&nbsp;<a style="color: Highlight" href="http://www.ictusmg.com/"><u>Ictus Marketing Group</u></a>
                                                    <br />
                                                    <p style="padding-top: 10px;">
                                                       <%if (Session["UserID"] != null)
                                                      {%>
                                                    <a href="../FrmCancelPolicy.aspx" style="color: white"><u>Cancellation and Refund Policy </u></a>&nbsp; &nbsp; &nbsp; <a href="../FrmPrivacyPolicy.aspx" style="color: white"><u>Privacy Policy </u></a>
                                                    <%}
                                                      else
                                                      { %>
                                                    <a href="../CancelPolicy.aspx" style="color: white"><u>Cancellation and Refund Policy </u></a>&nbsp; &nbsp; &nbsp; <a href="../PrivacyPolicy.aspx" style="color: white"><u>Privacy Policy </u></a>
                                                    <%} %>
                                                    </p>
                                                </div>
                                            </td>
                                            <td class="col-md-2 col-sm-2">
                                                <table style="width: 100px">
                                                    <tr>
                                                        <td>
                                                            <a href="https://www.linkedin.com/in/brian-doherty-4359386a">
                                                                <img src="/Images/linkedinicon.png" class="img-responsive" />
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="https://www.facebook.com/Brian-Doherty-624778240952341/">
                                                                <img src="/Images/fbicon.png" class="img-responsive" />
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a href="https://twitter.com/BrianDoherty57">
                                                                <img src="/Images/twittericon.png" class="img-responsive" />
                                                            </a>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
</body>
</html>
