﻿<%@ Page Title="Calculator Married" Language="C#" MasterPageFile="~/ClientMaster.master" ValidateRequest="False" AutoEventWireup="true" CodeBehind="Calculate.aspx.cs" Inherits="Calculate._Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="BodyContent">
    <ajaxToolkit:ToolkitScriptManager ID="smCustomer" runat="server" ScriptMode="Release">
    </ajaxToolkit:ToolkitScriptManager>
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <h1 class="headfontsize3 text-center setTopMargin">Calculation and Strategies for Married Couples</h1>

    <asp:Panel ID="PanelEntry" runat="server">
        <div class="col-md-12 col-sm-12 failureForgotNotification displayNone" id="displayErrorMessage">
            <asp:Label ID="ErrorMessage" runat="server"></asp:Label>
        </div>
        <div class="row margleft margiright">
            <div class="col-md-12 col-sm-12">
                <p class="Font16 text-justify">
                    In order to use the calculator, you must provide the amount of your Monthly Social Security benefit at your Full Retirement Age. 
                    If you don't know the amount of your Full Retirement Age benefit, there are two ways you can obtain it. 
                    One, if you have a recent Social Security Benefit statement that you received in the mail, the amount of your Full Retirement Age benefit can be found on that statement. 
                    Or two, you can click on this link, <a href="https://secure.ssa.gov/RIL/SiView.do"><u>https://secure.ssa.gov/RIL/SiView.do</u></a>, and  it will take you to the page on the Social Security web site where you can set up your own account and get your Social Security benefit statement online. 
                    Your online Social Security benefit statement will provide you with the amount of your Full Retirement Age benefit. 
                    It only takes a few minutes to set up your account and you can return to the web site at any time to get your most up to date Social Security benefit information.
                </p>
                <br />
                <asp:Label ID="LabelPlease" runat="server" Font-Bold="True" Font-Names="Arial" Text="Please enter your information:"></asp:Label>
                <p class="WarningStyle">(*Do not use dollar symbol or commas, round to the nearest whole number)</p>
                <asp:Label ID="Label1" runat="server" Text="What is your Full Retirement Age Benefit?"></asp:Label>

                <asp:TextBox ID="BeneHusband" runat="server" CssClass="input-text textEntry form-control" MaxLength="7" TabIndex="1" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqBeneHusband" runat="server" ControlToValidate="BeneHusband" CssClass="failureErrorNotification" ToolTip="Your Full Retirement Age Benefit is required" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Your Full Retirement Age Benefit is required</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regBeneHusband" runat="server" ControlToValidate="BeneHusband" CssClass="failureErrorNotification" ValidationExpression="^[0-9]*$" ToolTip="Your Full Retirement Age Benefit must be a number" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Your Full Retirement Age Benefit must be a number</asp:RegularExpressionValidator>

                <asp:TextBox ID="BirthWifeMM" runat="server" MaxLength="2" Width="20px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="BirthWifeDD" runat="server" MaxLength="2" Width="20px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="birthWifeTempYYYY" runat="server" MaxLength="4" Width="20px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="BirthWifeYYYY" runat="server" MaxLength="4" Width="80px" Visible="false" CssClass="textEntry"></asp:TextBox>
                <asp:TextBox ID="Your_Name" runat="server" Width="80px" Visible="false" CssClass="textEntry"></asp:TextBox>
                <asp:TextBox ID="Spouse_Name" runat="server" Width="80px" Visible="false" CssClass="textEntry"></asp:TextBox>
            </div>
            <div class="col-md-12 col-sm-12">
                <br />
                <asp:Label ID="LabelHusbandBene" runat="server" Text="What is your Spouse's monthly Full Retirement Age Benefit?"></asp:Label>
                <asp:TextBox ID="BeneWife" runat="server" CssClass="input-text textEntry form-control" MaxLength="7" TabIndex="2" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqBeneWife" runat="server" ControlToValidate="BeneWife" CssClass="failureErrorNotification" ToolTip="Spouse's monthly Full Retirement Age Benefit is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Spouse's monthly Full Retirement Age Benefit is required</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regBeneWife" runat="server" ControlToValidate="BeneWife" CssClass="failureErrorNotification" ValidationExpression="^[0-9]*$" ValidationGroup="RegisterUserValidationGroup" ToolTip="Spouse's monthly Full Retirement Age Benefit must be a number" Display="Dynamic">*Spouse's monthly Full Retirement Age Benefit must be a number</asp:RegularExpressionValidator>
                <asp:TextBox Visible="false" ID="BirthHusbandMM" MaxLength="2" runat="server" Width="20px" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="BirthHusbandDD" Visible="false" MaxLength="2" runat="server" Width="20px" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="BirthHusbandTempYYYY" Visible="false" runat="server" Width="20px" CssClass="input-text textEntry"></asp:TextBox>
                <asp:TextBox ID="BirthHusbandYYYY" MaxLength="4" runat="server" Width="80px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
            </div>
            <div class="col-md-12 col-sm-12 displayNone">
                <asp:Label ID="Label2" runat="server" Text="Show Survivor Benefit" Visible="false"></asp:Label>
                <asp:CheckBox ID="CheckBoxSurv" runat="server" Text="" TabIndex="3" Visible="false" />
            </div>
            <div class="col-md-12 col-sm-12 displayNone">
                <asp:Label ID="Label3" runat="server" Text="Include 2.5% COLA"></asp:Label>
                <asp:CheckBox ID="CheckBoxCola" runat="server" Text="" TabIndex="4" />
            </div>
            <div class="col-md-12 col-sm-12">
                <br />
                <%--<asp:Button ID="btnSaveBasicInfoAndProceed" runat="server" Text="Previous - Basic Information" TabIndex="5" OnClick="btnSaveBasicInfoAndProceed_Click" CssClass="button_login" />--%>
                <asp:Button ID="BtnCalculate" ValidationGroup="RegisterUserValidationGroup" CssClass="button_login btnmarginsingle btnmarginmarried" runat="server" Text="Next - View Report" OnClick="Calculate_Click" TabIndex="6" />
                <br />
                <br />
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="PanelParms" runat="server">
        <div class="row">
            <br />
            <div class="col-md-7 col-sm-7 ">
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Customer Name:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerName" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <asp:Label ID="lblCustomerSpouseName" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Date of Birth:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerDOB" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <asp:Label ID="lblCustomerSpouseDOB" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Full Retirement Age:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerFRA" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <asp:Label ID="lblCustomerSpouseFRA" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Full Retirement Age Benefit:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerBenefit" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <asp:Label ID="lblCustomerSpouseBenefit" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold ">
                        Marital Status:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        Married
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Annual Cost of Living Increase:
                    </div>
                    <div class="col-md-4 col-sm-4">
                        2.5%
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-5">
                <br />
                <%if (Session["AdminID"] != null)
                  {%>
                <asp:Button ID="btnManageCustomer" runat="server" CssClass="button_login " OnClick="Backto_Managecustomer" Text="Back to All Clients" />&nbsp;&nbsp;&nbsp;<asp:Button ID="btnDownloadReport" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download Report" />
                <%}
                  else
                  {
                      if (Session["Demo"] == null)
                      {%>
                <asp:Button ID="BtnChange" runat="server" CssClass="button_login " OnClick="Calculate_Click" Text="Edit Information" />
                <%} %>
                <asp:Button ID="btnExplain" CssClass="button_login" runat="server" Width="163px" Text="Explanation of Strategies" OnClick="btnExplain_Click" />
                <asp:Button ID="Button2" runat="server" CssClass="button_login" OnClick="PrintAll_Click" Text="Download Report" />
                <%}%>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="PanelGrids" runat="server">
        <br />
        <br />
        <p class="margileft fontBold text-justify">Please click on the circle below the numbered Strategy to see your benefit numbers for that suggested Strategy.</p>
        <div>
            <br />
            <table class="text-center">
                <tr class="fontLarge">
                    <td runat="server" id="strategy1" class="text-center" style="vertical-align: top">
                        <asp:Label ID="lblStrategy1" Text="Strategy 1" Font-Bold="true" runat="server"></asp:Label>
                        <br />
                        <asp:RadioButton ID="rdbtnStrategy1" GroupName="marriedScenario" runat="server" />
                        <br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber1" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Combined Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome1" runat="server" ForeColor="blue" /><br />
                        <p class="marginTop">
                            <asp:Label runat="server" ID="lblRestrictAppIncomeTitle1" Visible="false">Extra Income from <br /> Restricted Application</asp:Label>
                        </p>
                        <asp:Label runat="server" ID="lblRestrictAppIncomeValue1" ForeColor="Blue" Visible="false"></asp:Label>

                    </td>
                    <td runat="server" id="strategy2" class="text-center" style="vertical-align: top">
                        <asp:Label ID="lblStrategy2" Text="Strategy 2" Font-Bold="true" runat="server"></asp:Label>
                        <br />
                        <asp:RadioButton ID="rdbtnStrategy2" GroupName="marriedScenario" runat="server" />
                        <br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber2" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Combined Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome2" runat="server" ForeColor="blue" /><br />
                        <p class="marginTop">
                            <asp:Label runat="server" ID="lblRestrictAppIncomeTitle2" Visible="false">Extra Income from <br /> Restricted Application</asp:Label>
                        </p>
                        <asp:Label runat="server" ID="lblRestrictAppIncomeValue2" ForeColor="Blue" Visible="false"></asp:Label>
                    </td>
                    <td runat="server" id="strategy3" class="text-center" style="vertical-align: top">
                        <asp:Label ID="lblStrategy3" Text="Strategy 3" Font-Bold="true" runat="server"></asp:Label>
                        <br />
                        <asp:RadioButton ID="rdbtnStrategy3" runat="server" GroupName="marriedScenario" />
                        <br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber3" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Combined Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome3" runat="server" ForeColor="blue" /><br />
                        <p class="marginTop">
                            <asp:Label runat="server" ID="lblRestrictAppIncomeTitle3" Visible="false">Extra Income from <br /> Restricted Application</asp:Label>
                        </p>
                        <asp:Label runat="server" ID="lblRestrictAppIncomeValue3" Visible="false" ForeColor="Blue"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <br />
        </div>
        <!-- Strategies -->
        <div id="strategy-EarlyStrategy" class="strategy-container">
            <asp:Panel ID="EarlySuspend" runat="server">
                <asp:Panel runat="server" ID="pnlText1" CssClass="text-justify margiright">
                    <asp:Label ID="LabelEarlySuspend" runat="server" Text="Early Claim & Suspend" class="displayNone"></asp:Label>
                    <asp:Label ID="lblKeyFigures" runat="server" Font-Bold="true" Font-Names="Arial" CssClass="margileft" ForeColor="Black" Text="Key Benefit Numbers:"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey1" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey2" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyRestrictIncomEarly" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsFigures" Font-Bold="true" runat="server" CssClass="margileft spaceInKeysAndSteps" Font-Names="Arial" ForeColor="Black" Text="Next Steps:"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep8" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="Label5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep9" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep10" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep11" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep12" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                     <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                    <asp:Button ID="btnEarlyDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" />
                </center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridEarly" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblWifeExplanation2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblHusbandExplanation2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center> 
                    <div class="col-md-12 col-sm-12">
                <div class="table-responsive">
                    <asp:Chart ID="ChartEarly" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" ToolTip="Value of X: #VALY Value of Y #VALY" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div>
                    </div>
                </center>
            </asp:Panel>
        </div>
        <!-- end strategy-1 -->
        <div id="strategy-BestSrategy" class="strategy-container">
            <asp:Panel ID="PanelBest" runat="server">
                <asp:Panel ID="pnlText2" runat="server" class="text-justify  margiright">
                    <asp:Label ID="LabelBest" runat="server" Text="Full Retirement & Claim Late" class="displayNone"></asp:Label>
                    <asp:Label ID="lblKeys" runat="server" Text="Key Benefit Numbers:" CssClass="margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyRestrictIncomBest" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblSteps" runat="server" Text="Next Steps:" CssClass="spaceInLblKeysAndSteps margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="Label4" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep13" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep7" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <br />
                <%--<%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                    <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                    <asp:Button ID="btnBestDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" />
                </center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridBest" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife0" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband0" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>


                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblWifeExplanation1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblHusbandExplanation1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center><div class="table-responsive">
                    <asp:Chart ID="ChartBest" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div></center>
            </asp:Panel>
        </div>
        <!-- end strategy-2 -->
        <div id="strategy-SuspendStrategy" class="strategy-container" <%--style="display: block;"--%>>
            <asp:Panel ID="PanelSuspendLater" runat="server">
                <asp:Panel runat="server" ID="pnlText3" class="text-justify  margiright">
                    <asp:Label ID="NoteSuspendLater" Visible="false" runat="server"></asp:Label>
                    <asp:Label ID="LabelSuspendLater" runat="server" Text="Claim and Suspend" class="displayNone"></asp:Label>
                    <asp:Label ID="lblKeySuspend" runat="server" Text="Key Benefit Numbers:" CssClass="margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSuspendKey1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSuspendKey2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSuspendKey3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsSuspend" runat="server" CssClass="spaceInLblKeysAndSteps margileft" Text="Next Steps:" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>

                    <div class="text-justify margin100 margiright">

                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStepsSuspend1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStepsSuspend2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStepsSuspend3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStepsSuspend4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStepsSuspend5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                </asp:Panel>
                <br />
                <%--<%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                    <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                    <asp:Button ID="btnSuspendDownLoad" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" /></center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridSuspendLater" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <center> <div class="table-responsive" >
                    <asp:Chart ID="ChartSuspendLater" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div></center>
            </asp:Panel>
        </div>
        <!-- end strategy-3 -->
        <div id="strategy-FullStrategy" class="strategy-container">
            <asp:Panel ID="PanelBestLater" runat="server">
                <asp:Panel runat="server" ID="ExplainBestLater">
                    <asp:Label ID="LabelBestLater" runat="server" class="displayNone" Text="Claim Early Claim Late "></asp:Label>
                    <asp:Label ID="lblKeyFigures1" runat="server" Text="Key Benefit Numbers:" CssClass="margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey7" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey8" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey9" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyRestrictIncomFull" runat="server" Font-Names="Arial"  ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsFigures1" runat="server" Text="Next Steps:" CssClass="margileft spaceInLblKeysAndSteps" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="stepsage4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps0" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="Label6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps14" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <br />
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center> 
               
                     <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                     <asp:Button ID="btnFullDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" /></center>
                <%} %>--%>
                <div class="table-responsive">
                    <br />
                    <asp:GridView ID="GridBestLater" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblWifeExplanation3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblHusbandExplanation3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center>  <div class="table-responsive" >
                    <asp:Chart ID="ChartBestLater" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div></center>
            </asp:Panel>
        </div>
        <!-- end strategy-4 -->
        <div id="strategy-MaxStrategy" class="strategy-container">
            <asp:Panel ID="PanelMax" runat="server">
                <asp:Panel runat="server" ID="Panel2" CssClass="text-justify margiright">
                    <asp:Label ID="lblKeys4" runat="server" Font-Bold="true" Font-Names="Arial" CssClass="margileft" ForeColor="Black" Text="Key Benefit Numbers:"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeys1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeys2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeys3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyRestrictIncomMax" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblSteps4" Font-Bold="true" runat="server" CssClass="margileft spaceInKeysAndSteps" Font-Names="Arial" ForeColor="Black" Text="Next Steps:"></asp:Label>

                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="stepsage5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStepMax4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                     <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                     <asp:Button ID="btnMaxDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" /></center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridMax" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblWifeExplanation4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblHusbandExplanation4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife7" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband7" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center> 
                    <div class="col-md-12 col-sm-12">
                <div class="table-responsive">
                    <asp:Chart ID="ChartMax" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" ToolTip="Value of X: #VALY Value of Y #VALY" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div>
                    </div>
                </center>
            </asp:Panel>
        </div>
        <%--strategy--zero benefit--%>
        <div id="strategy-ZeroStrategy" class="strategy-container">
            <asp:Panel ID="pnlZero" runat="server">
                <asp:Panel ID="pnlZeroText" runat="server" class="text-justify  margiright">
                    <asp:Label ID="lblZeroStrat" runat="server" Text="Zero Benefit Strategy" class="displayNone"></asp:Label>
                    <asp:Label ID="lblZeroKeys" runat="server" Text="Key Benefit Numbers:" CssClass="margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyZero1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyZero2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyZero3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblZerpNextSteps" runat="server" Text="Next Steps:" CssClass="spaceInLblKeysAndSteps margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblZeroSteps4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblZeroSteps5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblZeroSteps6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <br />
                
                <div class="table-responsive">
                    <asp:GridView ID="GridZero" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblZeroNoteWife0" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblZeroNoteHusb0" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>


                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblZeroWifeExplanation1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblZeroHusbandExplanation1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblZeroNoteWife" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblZeroNoteHusband" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center><div class="table-responsive">
                    <asp:Chart ID="ChartZero" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div></center>
            </asp:Panel>
        </div>



        <div class="strategy-container">
            <asp:Panel ID="PanelNowLater" runat="server">
                <div class="clearfix">
                    <div class="col-md-10 column">
                        <h2>
                            <asp:Label ID="LabelNowLater" runat="server" Text="Claim Early Claim Late"></asp:Label></h2>
                    </div>
                    <div class="col-md-2 column">
                        <asp:Button ID="Button4" runat="server" CssClass="button_login downloadbtn" OnClick="PrintAll_Click" Text="Download" />
                    </div>
                </div>
                <div class="table-responsive">
                    <asp:GridView ID="GridNowLater" runat="server" ForeColor="Black"></asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="ExplainNowLater">
                <p>
                    <asp:Label ID="StrategyNowLater" runat="server">
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="NoteNowLater" runat="server"></asp:Label>
                </p>
            </asp:Panel>
        </div>
        <!-- end -->
        <div class="strategy-container">
            <asp:Panel ID="PanelLater" runat="server">
                <h2>
                    <asp:Label ID="LabelLater" runat="server" Text="Full Retirement & Claim Late"></asp:Label></h2>
                <div class="button-row">
                </div>
                <div class="table-responsive">
                    <asp:GridView ID="GridLater" runat="server" ForeColor="Black"></asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="ExplainLater">
                <p>
                    <asp:Label ID="StrategyLater" runat="server">
          In this strategy the spouse with the lower benefit starts social security at full
          retirement age. The spouse with the higher benefit uses the spousal benefit to get
          some additional income while their work history benefit grows until age 70.
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="NoteLater" runat="server"></asp:Label>
                    <br />
                    <br />
                    If you compare this strategy to the Claim Now, Claim More Later Stratege above,
          you will see that the age 70 benefit is higher while the income prior to age 70
          is lower. This strategy can be better in the long run if you can afford to defer
          the income.
                </p>
            </asp:Panel>
        </div>
        <!-- end -->
        <div class="strategy-container">
            <asp:Panel ID="PanelSuspend" runat="server">
                <h2>
                    <asp:Label ID="LabelSuspend" runat="server" Text="Early Claim and Suspend"></asp:Label></h2>
                <!--<asp:HyperLink ID="HyperLinkSuspend" runat="server">  (Show)</asp:HyperLink>-->
                <div class="button-row">
                </div>
                <div class="table-responsive">
                    <asp:GridView ID="GridSuspend" runat="server" ForeColor="Black"></asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="ExplainSuspend">
                <p>
                    <asp:Label ID="StrategySuspend" runat="server">
            In this strategy the spouse with the lower benefit starts social security as soon as they are eligable. The spouse with the higher benefit claims and suspends so that the spousal benefit can be used while their work history benefit grows until age 70.
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="NoteSuspend" runat="server"></asp:Label>
                </p>
            </asp:Panel>
        </div>
        <!-- end -->
        <div class="strategy-container">
            <asp:Panel ID="PanelStandard" runat="server">
                <h2>
                    <asp:Label ID="LabelStandard" runat="server" Text="Compromise Strategy"></asp:Label></h2>
                <!--<asp:HyperLink ID="HyperLinkStandard" runat="server">  (Show)</asp:HyperLink>-->
                <div class="button-row">
                </div>
                <div class="table-responsive">
                    <asp:GridView ID="GridStandard" runat="server" ForeColor="Black"></asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="ExplainStandard">
                <p>
                    <asp:Label ID="StrategyStandard" runat="server">
          In this strategy both husband and wife start social security at full retirement age.
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="NoteStandard" runat="server"></asp:Label>
                    <br />
                    <br />
                    This strategy uses the standard claim at full retirement age (usually 66). It is
          better than claiming as soon as possible because the age 70 benefit is higher and
          the survivor's benefit is higher. However, the other strategies (Claim Now Claim
          More Later, Claim and Suspend, etc.) offer much better age 70 and survivor benefits.
                </p>
            </asp:Panel>
        </div>
        <!-- end -->
        <div class="strategy-container">
            <asp:Panel ID="PanelLatest" runat="server">
                <h2>
                    <asp:Label ID="LabelLatest" runat="server" Font-Bold="True" Font-Names="Arial" Text="Claim as Late as Possible" ForeColor="Black"></asp:Label></h2>
                <!--<asp:HyperLink ID="HyperLinkLatest" runat="server">  (Show)</asp:HyperLink>-->
                <div class="button-row">
                </div>
                <div class="table-responsive">
                    <asp:GridView ID="GridLatest" runat="server" ForeColor="Black"></asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="ExplainLatest">
                <p>
                    <asp:Label ID="StrategyLatest" runat="server">
          In this strategy both husband and wife start social security at age 70.
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="NoteLatest" runat="server"></asp:Label>
                </p>
            </asp:Panel>
        </div>
        <!-- end -->
        <br />
        <br />

        <a href="../FrmDisclaimer.aspx">Disclaimer </a>
    </asp:Panel>

    <div id="subscription" class="testmodalPopup">
        <center>  <div id="plans" class="plan-contentnew">
          <br />
          <asp:Label ID="MaxCumm"  class="subspopuplabelsColor" runat="server"></asp:Label>
          <br />
          <h2 class="subspopupText">While growing your Social Security Check to the largest amount possible! <br />
               <br />  
              Find out how, for just 39.95 dollars!
                <br />
                <br />
                <asp:Button ID="btnTrial" runat="server" Text="Subscribe" CssClass="button_login" OnClick="btnTrial_Click" />&nbsp;&nbsp;<asp:Button ID="CloseWindow" OnClick="Close_Click" runat="server" CssClass="button_login" Text="Cancel" />
            </h2>
          <br />
        </div></center>
        <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
    </div>

    <asp:Panel ID="pnlExplanation" ScrollBars="Auto" runat="server" CssClass="displayNone ExplanationModalPopup">
        <div class="h2PopUp modal-header" style="color: white; font-size: x-large; font: bold;">
            Explanation of the Strategies
          <asp:Button ID="btnCancelTop" runat="server" CssClass="button_login floatRight" OnClick="imgCancel_Click" Text="Back to the Strategies" />
        </div>
        <div class="textExplanation plan-contentnew">
            <br />
            <div class="bs-example ">
                <div class="panel-group" id="accordion">
                    <center>
                    <div style="font-size: large; font: bold;">
                        Married Couples
                    </div>
                        </center>
                    <br />

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What is the Primary Goal of the Paid to Wait Calculator?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    The primary goal of the Paid to Wait Calculator is to maximize the size of the bigger benefit. Most married couples receive two Social Security benefit checks, one made payable to the husband and the other made payable to the wife.
                                    They should try to maximize the size of the bigger benefit. Not only will that increase their Social Security income when they are both alive but it will also maximize the size of the Survivor Benefit. After the first spouse dies, the surviving spouse will only receive one Social Security benefit check, fortunately they can continue to receive the bigger of the two checks.
                                    Maximizing the size of the bigger benefit will also maximize the size of the Survivor Benefit, which will leave the surviving spouse with the biggest Survivor Benefit check possible for their particular situation.<br />
                                    <br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Many Married Couples Can Get Paid to Wait&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    The Paid to Wait Calculator can help make it easier to maximize the size of the bigger benefit by showing you the ONE claiming strategy that will pay you the most amount of Social Security income while you wait.                                     
                                    In other words, while the spouse with the bigger benefit is waiting to claim it at age 70, so they can maximize the size of it, the Paid to Wait Calculator will show them the one claiming strategy that will pay them the most amount of Social Security income while they wait..<br />
                                    <br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Can All Married Couples Get Paid to Wait?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    No, not all married couples can get Paid to Wait but many do. If you can get Paid to Wait, the Paid to Wait Calculator will show you exactly how to do it.<br />
                                    <br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">How the Paid to Wait Calculator Works?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    Depending on your situation, the Paid to Wait Calculator could show you a maximum of three different suggested claiming strategies, sometimes it may only show you two strategies and in some cases it could only show you one claiming strategy.<br />
                                    <br />
                                    Most married couples can get Paid to Wait. The first suggested claiming strategy shown, or Strategy 1, is usually the ONE strategy that will pay you the most amount of Social Security income while you wait to maximize the size of the bigger benefit. This strategy usually recommends that the spouse with the smaller benefit, claim their benefit as early as possible, even at age 62.
                                    <br />
                                    <br />
                                    Remember the primary goal is to maximize the size of the bigger benefit and claiming the smaller benefit early will pay the married couple some Social Security income while they wait. How much income they receive depends on the size of the smaller benefit. Also, they will probably receive the smaller benefit for the shortest time because once the first spouse dies, the surviving spouse will continue to receive only the bigger benefit, the smaller benefit check will stop.<br />
                                    <br />
                                    If one or both of the spouses were born before January 2, 1954, they may be able to use the Restricted Application claiming strategy. This strategy could pay the married couple additional Social Security income, even up to $60,000 in extra income, while they wait. This strategy revolves around the strategic claiming of the Spousal Benefit. The Paid to Wait Calculator will automatically determine if you are eligible to use the Restricted Application strategy and then show you exactly how it would work in your particular situation.<br />
                                    <br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What if You Continue to Work?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    If the spouse with the smaller benefit is younger than their Full Retirement Age (your Full Retirement Age depends on the year you were born and can vary between age 66 to 67), the Paid to Wait Calculator will show you a second strategy or Strategy 2. In this strategy the spouse with the smaller benefit will wait until their Full Retirement Age before they claim their regular Work History Benefit. It will show this strategy in case the spouse with the smaller benefit continues to work between the ages of 62 and their Full Retirement Age. If they claim their Social Security benefits prior to their Full Retirement Age and continue to work, they could earn too much money and have to give back some or all of their Social Security Benefits. BUT, once they reach their Full Retirement Age, they can receive their Social Security benefits,  continue to work and never have to give back any of their benefits no matter how much money they make from their job. That is why the Paid to Wait Calculator will show this strategy in which the spouse with the smaller benefit waits until their Full Retirement Age to claim it.
                                    <br />
                                    <br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What if You Want to Maximize the Size of Both Benefits?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseSix" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    The last strategy the Paid to wait Calculator will show – is the one that will maximize the size of both spouse’s benefits. When this claiming strategy is complete it will provide the married couple with the largest amount of Combined Annual Social Security Income for the rest of their lives.<br />
                                    <br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What You Can Expect&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    Each suggested claiming strategy will include a chart of your benefit amounts and a simple explanation of what benefit should be claimed and exactly when to claim it. The important numbers on the chart are highlighted and correspond to the explanations in both the “Key Benefit Numbers” section and the “Next Steps” section.
                                    <br />
                                    <br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What are the “Key Benefit Numbers” in the Strategies?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseEight" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    1) Your Paid to Wait number – this is the amount of Social Security income you will receive while you wait to maximize the size of the bigger benefit. Most married couples will have a Paid to Wait number but in some situations they will not.<br />
                                    2) Maxed Out Bigger Benefit – the monthly dollar amount of the maxed out bigger benefit. Another reason to max out the size of the bigger benefit is because eventually that benefit will become the Survivor Benefit after the first spouse dies. The surviving spouse is only going to receive one Social Security check after the first spouse dies, the Survivor Benefit. Maximizing the size of the bigger benefit will also maximize the size of the Survivor Benefit. The amount of this benefit and all the other benefits in the chart will increase every year because of Cost of Living or COLA adjustments. The Paid to Wait Calculator assumes an annual COLA increase of 2.5%.<br />
                                    3) Combined Annual Income – When the claiming strategy is complete, this shows the combined amount of Social Security income you and your spouse will receive every year for the rest of your lives. This combined benefit amount will also increase every year because of COLA adjustments.<br />
                                    <br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What are the “Next Steps” in the Strategies?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseNine" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    This section tells you what benefit to claim and when you should claim it. All the numbered steps in this section correspond to the highlighted Ages and Benefit Amounts in the chart. All of the benefit amounts in all of the strategies increase slightly every year because the Paid to Wait Calculator includes a 2.5% annual Cost of Living or COLA increase.<br />
                                    <br />
                                    We used a 2.5% COLA increase because that has been the approximate average annual COLA increase over the last 20 years. If some of the benefit amounts in our charts don’t match the benefit amounts from your Social Security statements, especially your Full Retirement Age benefit and your benefit at age 70, that is because Social Security does not make any COLA assumptions when calculating your future benefits.<br />
                                    <br />
                                    If you decide on a particular claiming strategy, please follow the explanation in the “Next Steps” section to determine exactly when you should claim a specific benefit. Depending on you situation, the Paid to Wait Calculator may tell you to claim a benefit prior to your birthday. For example, it may tell you to claim your Work History Benefit at age 66 years and 8 months or four months before your 67th birthday.<br />
                                    <br />
                                    On the chart for that particular strategy, which includes your benefit amounts, when a benefit is claimed before a birthday, those benefit payment amounts may be carried over to the next year. For example, if a benefit is claimed at age 66 years and 8 months, you would only receive 4 months of benefit payments before your 67th birthday. On the chart those 4 months of benefit payments could be carried over to the next year and included in the total benefit amount you received at age 67. The chart will indicate this with a # (number sign) or a *  (asterisk) that you receive 16 months of benefit payments at age 67, which includes 4 months of payments at age 66 and 12 months of benefit payments at age 67 (4 + 12 = 16 months of payments). The important thing to remember is to follow the specific instructions in the “Next Steps” section of exactly when you should claim a benefit.<br />


                                    <br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
            <p>
                p.s. – Don’t wait until the last minute to claim your benefits, make an appointment with your local Social Security office at least one month prior to the date you want your benefits to begin.
                <br />
                <br />
            </p>
            <p class="text-center fontBold" style="color: #85312F">
                Good Luck with your claiming strategy!<br />
                <br />
                <asp:Button ID="print" runat="server" Text="Print" CssClass="button_login" OnClick="exportToPdf" />
                <asp:Button ID="btnCancelTopBottom" runat="server" CssClass="button_login " OnClick="imgCancel_Click" Text="Back to the Strategies" />
            </p>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
    <cc1:ModalPopupExtender ID="popup" runat="server" PopupControlID="pnlExplanation" TargetControlID="lnkFake" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
</asp:Content>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/blockUI.min.js"></script>
    <script type="text/javascript" src="../Scripts/showstrategies.js"></script>
    <script type="text/javascript" src="../Scripts/IRR.js"></script>
    <script type="text/javascript">
        var userID = '<%= Session["UserID"].ToString() %> ';
        $(document).ready(function () {
            window.scrollTo(0, 400);
            $('#<%=btnTrial.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
            $('#<%=CloseWindow.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
            <%--$('#<%=btnWithoutTrial.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });--%>
            $('.blockPage').removeAttr('style');
            //$('.blockPage').addClass('subpop');
            // var guess = 0.01;
            //var values = [-19653, 0, 0, 0, 0, 1053, 1053, 1053, 1053, 1053, 1053, 1053, 1053, 1053, 1053, 486.62];
            //var result = IRR(values, guess);
            // console.log("irr="+result);
        });
        $(function () {
            var WifepickerOpts = {
                changeMonth: true,
                changeYear: true,
                yearRange: '1910:2020',
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: '/Images/calendar1.png'

            };
            $("[id$=BirthWifeYYYY]").datepicker(WifepickerOpts);
            var HusbandpickerOpts = {
                changeMonth: true,
                changeYear: true,
                yearRange: '1910:2020',
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: '/Images/calendar1.png'
            };
            $("[id$=BirthHusbandYYYY]").datepicker(HusbandpickerOpts);
            // Control slider with external navigation
            $('div.slider-label').bind('click', function () {
                var newValue = $(this).attr('id').replace('slider-label-', '');
                s.slider('value', newValue);
                s.slider('option', 'stop').call(s, null, {
                    value: newValue
                });
            });
            $('[id$=GridEarly],[id$=GridBest],[id$=GridSuspendLater],[id$=GridBestLater]').addClass('gridborder');
            $('[id$=ChartEarly],[id$=ChartBest],[id$=ChartSuspendLater],[id$=ChartBestLater]').css('height', '');
            $('[id$=ChartEarly],[id$=ChartBest],[id$=ChartSuspendLater],[id$=ChartBestLater]').addClass('img-responsive mobheight');
            if ($('[id$=PanelGrids]').is(':visible')) {
                $('[id$=Img3]').attr('src', '/images/staticslider.png');
                $('[id$=Img2]').attr('src', '/images/upperstaticslider2.png');
            }
            else {
                $('[id$=Img2]').attr('src', '/images/upperstaticslider1.png');
            }
        });

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
</asp:Content>
