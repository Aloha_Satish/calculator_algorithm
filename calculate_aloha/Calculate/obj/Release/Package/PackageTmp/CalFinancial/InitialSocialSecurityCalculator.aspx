﻿<%@ Page Title="Initial Social Security Calculator" Language="C#" MasterPageFile="~/ClientMaster.master" AutoEventWireup="true" CodeBehind="InitialSocialSecurityCalculator.aspx.cs" Inherits="Calculate.InitialSocialSecurityCalculator" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="BodyContent">
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <div class="clearfix">
        <asp:Panel ID="pnlInitialControls" runat="server" Visible="false">
            <center>
        <div class="col-md-12 col-sm-12" >
             <br />   <br /><br /> 
            <h1 class="headfontsize">Create <asp:Label ID="lblWelcome" runat="server"></asp:Label>’s Social Security Report</h1>
        </div>
        <div class="col-md-12 col-sm-12" style="height:300px">
        <asp:Button ID="btnSaveAccountDetails" runat="server" Text="View your Social Security Report" Width="250px" CssClass="button_login" OnClick="redirectToSocialSecurity" TabIndex="1" />        
       <br />  <br />   
        <asp:Button ID="btnDownloadPdf" Visible="false" runat="server" Text=" Download your Social Security Report" Width="250px" CssClass="button_login" OnClick="downloadSocialReport" TabIndex="2" />
        </div>
        </center>
        </asp:Panel>
        <asp:Panel ID="pnlWelcomeControls" runat="server" Visible="false">
            <center style="margin-bottom: 135px">
             <br />
        <br />
        <h1 class="headfontsize">Welcome <asp:Label ID="lblWelcome1"  runat="server"></asp:Label> !</h1>
        <br />
        <div class="fontLarge text-center text-justify">
     Are you ready to find out how much you can get paid to wait?<br />
Before you begin, you will need to find out your projected monthly Social Security benefit at full retirement.<br />
 You may have received this information in a letter from the Social Security Administration (SSA) or you can obtain it by <a href="https://secure.ssa.gov/RIL/SiView.do"><u>visiting the SSA web site</u></a>.
Once you have this information, please click the button below to continue.
        </div>
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Go to My Social Security Analysis" CssClass="button_login btn" OnClick="redirectToSocialSecurity" TabIndex="0" />
             <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
           
    </center>
        </asp:Panel>
        <div class="text-center fontLarge">
            <span id="status" class="statusContent displayNone">Customer Registered Successfully.
            </span>
        </div>
    </div>
    <script type="text/javascript">
        history.pushState(null, null, 'InitialSocialSecurityCalculator.aspx');
        window.addEventListener('popstate', function (event) {
            history.pushState(null, null, 'InitialSocialSecurityCalculator.aspx');
        });
        $(document).ready(function () {
            window.scrollTo(0, 400);
        });

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
</asp:Content>

