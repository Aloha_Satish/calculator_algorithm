﻿using CalculateDLL;
using CalculateDLL.TO;
using Symbolics;
using System;
using System.Data;
using System.Reflection;
using System.Text;
using System.Web.UI;

namespace Calculate
{
    public partial class Store : Page
    {
        #region Events

        /// <summary>
        /// code when page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    Session[Constants.SessionRegistered] = null;
                }
                catch (Exception ex)
                {
                    ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorStore, MethodBase.GetCurrentMethod(), ex.ToString()));
                    ErrorMessagelable.Text = Constants.ErrorStore + MethodBase.GetCurrentMethod() + ex.Message;
                    ErrorMessagelable.Visible = true;
                }
            }
        }

        /// <summary>
        /// Send customer to chargify payment gateway for payments */
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void checkout_Click(object sender, EventArgs e)
        {
            try
            {
                #region code to set the variables
                /* Insert Store Details */
                StoreTO objStoreTO = new StoreTO();
                StoreTransaction objStore = new StoreTransaction();
                objStoreTO.BookName = Constants.Orelly;
                objStoreTO.Price = Constants.Price;
                //Change For Live Server
                //objStoreTO.ComponentID = "67216";
                // Aloha test Server
                objStoreTO.ComponentID = Constants.ComponentID;
                objStoreTO.Unit = unit.Text;
                objStore.insertStoreTransactionDetails(objStoreTO);
                DataTable dtDetails = objStore.getStoreTransactionDetailsLastID();
                #endregion code to set the variables

                #region code redirect to chargify with all parameters
                if (dtDetails.Rows.Count > 0)
                {
                    /* Redirect to Payment Gateway */
                    StringBuilder strForm = new StringBuilder();
                    string formID = "submitChargify";
                    //Change for live server
                    //string url = Constants.LiveStoreURL;
                    //change for testing server
                    string url = Constants.TestStoreURL;
                    strForm.Append("<form id=\"" + formID + "\" name=\"" + formID + "\" action=\"" + url + "\" method=\"POST\">");
                    string quantity = "components[][allocated_quantity]";
                    strForm.Append("<input id='components__component_id' type='hidden' value='" + objStoreTO.ComponentID + "' name='components[][component_id]'>");
                    strForm.Append("<input id='reference' type='hidden' value='" + dtDetails.Rows[dtDetails.Rows.Count - 1]["StoreID"] + "' name='reference'>");
                    strForm.Append("<input type=\"hidden\" name=\"" + quantity + "\" value=\"" + objStoreTO.Unit + "\">");
                    strForm.Append("</form>");
                    //Build the JavaScript which will do the Posting operation.
                    StringBuilder strScript = new StringBuilder();
                    strScript.Append("<script language='javascript'>");
                    strScript.Append("var v" + formID + " = document." +
                                     formID + ";");
                    strScript.Append("v" + formID + ".submit();");
                    strScript.Append("</script>");
                    string strForm1 = strForm.ToString() + strScript.ToString();
                    Page.Controls.Add(new LiteralControl(strForm1));
                }
                #endregion code redirect to chargify with all parameters
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorStore, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorStore + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion
    }
}