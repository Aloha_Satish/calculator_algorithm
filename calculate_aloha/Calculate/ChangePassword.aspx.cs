﻿using CalculateDLL;
using Symbolics;
using System;
using System.Reflection;
using System.Web.UI;

namespace Calculate
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        #region Events

        /// <summary>
        /// code called when page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, "ClearData();", true);
                    txtNewPassword.Focus();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorForgotPassword, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblUserLoginError.Text = Constants.ErrorForgotPassword + MethodBase.GetCurrentMethod() + ex.Message;
                lblUserLoginError.Visible = true;
            }
        }

        #endregion Events

        #region Methods

        /// <summary>
        /// code called when change password button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChangePasswordPushButton_Click(object sender, EventArgs e)
        {
            try
            {
                Customers objCustomer = new Customers();
                objCustomer.Password = Encryption.Encrypt(txtNewPassword.Text);
                objCustomer.PasswordResetFlag = Constants.FlagNo;
                objCustomer.Email = Session[Constants.EmailID].ToString();
                objCustomer.updateCustomerPassword(objCustomer);
                Session.Clear();
                Response.Redirect(Constants.RedirectConfirmPassword, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorForgotPassword, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblUserLoginError.Text = Constants.TryAgain;
                lblUserLoginError.Visible = true;
            }
        }

        /// <summary>
        /// code called whne cancel button is clicked to redirect back to home page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CancelPushButton_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectToLogin, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorForgotPassword, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblUserLoginError.Text = Constants.TryAgain;
                lblUserLoginError.Visible = true;
            }
        }

        protected void GoToLogin(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectToLogin, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorForgotPassword, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblUserLoginError.Text = Constants.TryAgain;
                lblUserLoginError.Visible = true;
            }
        }

        #endregion Methods
    }
}