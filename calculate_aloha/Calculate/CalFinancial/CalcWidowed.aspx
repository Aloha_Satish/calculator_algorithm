﻿<%@ Page Title="Calculator Widowed" Language="C#" MasterPageFile="~/ClientMaster.Master" AutoEventWireup="true" CodeBehind="CalcWidowed.aspx.cs" Inherits="Calculate.WebForm4" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="BodyContent">
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <h1 class="headfontsize3 text-center setTopMargin">Calculation and Strategies for Widows and Widowers</h1>
    <ajaxToolkit:ToolkitScriptManager ID="smCustomer" runat="server" ScriptMode="Release">
    </ajaxToolkit:ToolkitScriptManager>
    <asp:Panel ID="PanelEntry" runat="server">
        <div class="clearfix errorMessageTable failureForgotNotification displayNone" id="displayErrorMessage">
            <asp:Label ID="ErrorMessage" runat="server"></asp:Label>
        </div>
        <div class="col-md-12 col-sm-12">
            <div class="col-md-12 col-sm-12 ">
                <br />
                <p class="Font16 text-justify">
                    In order to use the calculator, you must provide the amount of your Monthly Social Security benefit at your Full Retirement Age. 
                    If you don't know the amount of your Full Retirement Age benefit, there are two ways you can obtain it. 
                    One, if you have a recent Social Security Benefit statement that you received in the mail, the amount of your Full Retirement Age benefit can be found on that statement. 
                    Or two, you can click on this link, <a href="https://secure.ssa.gov/RIL/SiView.do"><u>https://secure.ssa.gov/RIL/SiView.do</u></a>, and  it will take you to the page on the Social Security web site where you can set up your own account and get your Social Security benefit statement online. 
                    Your online Social Security benefit statement will provide you with the amount of your Full Retirement Age benefit. 
                    It only takes a few minutes to set up your account and you can return to the web site at any time to get your most up to date Social Security benefit information.
                </p>
                <br />
                <asp:Label ID="LabelPlease" runat="server" Font-Bold="True" Font-Names="Arial" Text="Please enter your information:"></asp:Label>
                <p class="WarningStyle">(*Do not use dollar symbol or commas, round to the nearest whole number)</p>
                <asp:Label ID="LabelWifeBene" runat="server" Text="What is your monthly Full Retirement Age Benefit?"></asp:Label>
                <asp:TextBox ID="BeneWife" CssClass="textEntry form-control" MaxLength="7" runat="server" TabIndex="1" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqBeneWife" runat="server" ControlToValidate="BeneWife" CssClass="failureErrorNotification" ToolTip="Your monthly Full Retirement Age Benefit is required" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Your monthly Full Retirement Age Benefit is required</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regBeneWife" runat="server" ControlToValidate="BeneWife" CssClass="failureErrorNotification" ToolTip="Your monthly Full Retirement Age Benefit must be a number" ValidationExpression="^[0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Your monthly Full Retirement Age Benefit must be a number</asp:RegularExpressionValidator>
                <asp:TextBox ID="BirthWifeMM" runat="server" MaxLength="2" Width="20px" Visible="false" CssClass="input-text textEntry form-control"></asp:TextBox>
                <asp:TextBox ID="BirthWifeDD" runat="server" MaxLength="2" Width="20px" Visible="false" CssClass="input-text textEntry form-control"></asp:TextBox>
                <asp:TextBox ID="birthWifeTempYYYY" runat="server" MaxLength="4" Width="20px" Visible="false" CssClass="input-text textEntry form-control"></asp:TextBox>
                <asp:TextBox ID="BirthWifeYYYY" Visible="false" runat="server" MaxLength="4" Width="80px" CssClass="textEntry form-control"></asp:TextBox>
            </div>
            <div class="col-md-12 col-sm-12 ">
                <br />
                <asp:Label ID="LabelHusbandBene" runat="server" Text="What is your deceased spouse's monthly Full Retirement Age Benefit?"></asp:Label>
                <asp:TextBox ID="BeneHusband" CssClass="textEntry form-control" ValidationGroup="RegisterUserValidationGroup" MaxLength="7" runat="server" TabIndex="2"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqBeneHusband" runat="server" ControlToValidate="BeneHusband" CssClass="failureErrorNotification" ToolTip="Deceased Spouse's monthly Full Retirement Age Benefit is required" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Deceased Spouse's monthly Full Retirement Age Benefit is required</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regBeneHusband" runat="server" ControlToValidate="BeneHusband" CssClass="failureErrorNotification" ToolTip="Deceased Spouse's monthly Full Retirement Age Benefit must be a number" ValidationExpression="^[0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Deceased Spouse's monthly Full Retirement Age Benefit must be a number</asp:RegularExpressionValidator>
            </div>
            <div class="col-md-12 col-sm-12 ">
                <br />
                <%--<asp:Button ID="btnSaveBasicInfoAndProceed" runat="server" Text="Previous - Basic Information" TabIndex="4" OnClick="btnSaveBasicInfoAndProceed_Click" CssClass="button_login" />--%>
                <asp:Button ID="BtnCalculate" runat="server" Text="Next - View My Report" CssClass="button_login btnmarginwidowed" ValidationGroup="RegisterUserValidationGroup" OnClick="Calculate_Click" TabIndex="5" />
                <br />
                <br />
            </div>
        </div>
        <%-- <asp:Label ID="Label3" runat="server" Font-Names="Arial" CssClass="floatLeft" Text="It is considered that the Spouse expired before taking his benefits."></asp:Label>--%>
    </asp:Panel>

    <asp:Panel ID="PanelParms" runat="server">
        <div class="row">
            <br />
            <div class="col-md-7 col-sm-7 ">
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Customer Name:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerName" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        Deceased Spouse
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Date of Birth:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerDOB" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Full Retirement Age:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerFRA" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Full Retirement Age Benefit:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerBenefit" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <asp:Label ID="lblCustomerSpouseBenefit" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold ">
                        Marital Status:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        Widow
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Annual Cost of Living Increase:
                    </div>
                    <div class="col-md-4 col-sm-4">
                        2.5%
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-5">
                <%if (Session["AdminID"] != null)
                  {%>
                <asp:Button ID="btnManageCustomer" runat="server" CssClass="button_login " OnClick="Backto_Managecustomer" Text="Back to All Clients" />&nbsp;&nbsp;&nbsp;<asp:Button ID="btnDownloadReport" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download Report" />
                <%} %>
                <%else
                  {
                      if (Session["Demo"] == null)
                      {%>

                <asp:Button ID="BtnChange" runat="server" OnClick="Calculate_Click" Text="Edit Information" CssClass="button_login" />
                <%} %>
                <asp:Button ID="btnExplain" CssClass="button_login" runat="server" Width="163px" Text="Explanation of Strategies" OnClick="btnExplain_Click" />
                <asp:Button ID="Button2" runat="server" CssClass="button_login" OnClick="PrintAll_Click" Text="Download Report" />
                <%} %>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="PanelGrids" runat="server">
        <br />
        <br />
        <p class="margileft fontBold text-justify">Please click on the circle below the numbered Strategy to see your benefit numbers for that suggested Strategy.</p>
        <div>
            <br />
            <table>
                <%--<tr>
                    <td align="center">
                        <asp:Image ID="Image1" Style="display: none;" ImageUrl="~\Images\green_Check_mark_img.gif" runat="server" /><asp:Label ID="Label2" Style="display: none; color: green; font-weight: bold; position: static;" Text="Best Plan" runat="server"></asp:Label>
                    </td>
                    <td align="center">
                        <asp:Image ID="Image2" Style="display: none;" ImageUrl="~\Images\green_Check_mark_img.gif" runat="server" /><asp:Label ID="Label1" Style="display: none; color: green; font-weight: bold; position: static;" Text="Best Plan" runat="server"></asp:Label>
                    </td>
                </tr>--%>
                <tr class="fontLarge">
                    <td id="strategy1" runat="server" class="text-center">
                        <asp:Label ID="lblStrategy1" Text="Strategy 1" runat="server" Font-Bold="true" /><br />
                        <asp:RadioButton ID="rdbtnStrategy1" runat="server" GroupName="Wid" /><br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber1" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome1" runat="server" ForeColor="blue" />
                    </td>
                    <td id="strategy2" runat="server" class="text-center">
                        <asp:Label ID="lblStrategy2" Text="Strategy 2" runat="server" Font-Bold="true" /><br />
                        <asp:RadioButton ID="rdbtnStrategy2" runat="server" GroupName="Wid" /><br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber2" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome2" runat="server" ForeColor="blue" />
                    </td>
                    <td id="strategy3" runat="server" class="text-center">
                        <asp:Label ID="lblStrategy3" Text="Strategy 3" runat="server" Font-Bold="true" /><br />
                        <asp:RadioButton ID="rdbtnStrategy3" runat="server" GroupName="Wid" /><br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber3" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome3" runat="server" ForeColor="blue" />
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <br />
        <!-- end slider-container -->
        <!-- Strategies -->
        <div id="strategy-BestSrategy" class="strategy-container">
            <asp:Panel ID="pnlStrategy1" runat="server">
                <asp:Label ID="LabelAsap" runat="server" class="displayNone" Text="Claim Your Work History Benefit as Early as Possible"></asp:Label></h2>
                <asp:Panel runat="server" ID="ExplainAsap" CssClass="text-justify margiright">
                    <asp:Label ID="StrategyAsap" runat="server" Visible="false" Font-Names="Arial" ForeColor="Black">
                    </asp:Label>
                    <asp:Label ID="NoteAsap" runat="server" Visible="false" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <asp:Label ID="lblKeyFigures2" runat="server" Text="Key Benefit Numbers:" Font-Bold="true" CssClass="margileft" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey9" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <div class=" text-justify">
                        <asp:Label ID="lblStepsFigures2" runat="server" Text="Next Steps:" CssClass="spaceInLblKeysAndSteps margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps9" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                </asp:Panel>
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                    <p class="fontLarge fontColorBlack" >Click Here to have the Strategies downloaded to your computer</p>
                    <asp:Button ID="btnAsapDownload" runat="server" CssClass="button_login " Width="125px" OnClick="PrintAll_Click" Text="Download" />
                </center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridAsap" HeaderStyle-Height="45px" runat="server" ForeColor="Black">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margin100 margiright">
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblWifeNote1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblWifeNote5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblWifeNote9" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                </div>
                <center> 
                    <div class="col-md-12 coll-sm-12">
                    <div class="table-responsive"> 
                        <asp:Chart ID="ChartAsap" runat="server" BorderlineDashStyle="Dash">
                    <Titles>
                        <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                    </Titles>
                    <Legends>
                        <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                    </Legends>
                    <Series>
                        <asp:Series Name="Series1" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                    </ChartAreas>
                </asp:Chart>
                    </div>
                         </div>
                </center>
                <br />
                <br />
            </asp:Panel>
        </div>
        <!-- end strategy-1 -->
        <!-- Strategies -->
        <div id="strategy-SuspendStrategy" class="strategy-container">
            <asp:Panel ID="pnlStrategy2" runat="server">
                <asp:Label ID="LabelSpouse" runat="server" class="displayNone" Text="Claim Survivor Benefit to allow Work History Benefit to build to Full Retirement Age"></asp:Label></h2>
                <asp:Panel runat="server" ID="ExplainSpouse" CssClass="text-justify margiright">
                    <asp:Label ID="StrategySpouse" Visible="false" runat="server" Font-Names="Arial" ForeColor="Black">
                    </asp:Label>
                    <asp:Label ID="NoteSpouse" Visible="false" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <asp:Label ID="lblKeyFigures1" runat="server" Font-Bold="true" Text="Key Benefit Numbers:" CssClass="margileft" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey10" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <div class=" text-justify">
                        <asp:Label ID="lblStepsFigures1" runat="server" Font-Bold="true" CssClass="margileft" Text="Next Steps:" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps10" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                </asp:Panel>
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                    <p class="fontLarge fontColorBlack" >Click Here to have the Strategies downloaded to your computer</p>
                    <asp:Button ID="btnSpouseDownLoad" runat="server" CssClass="button_login " Width="125px" OnClick="PrintAll_Click" Text="Download" /></center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridSpouse" HeaderStyle-Height="45px" runat="server" ForeColor="Black">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margin100 margiright">
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblWifeNote2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblWifeNote6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblWifeNote10" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                </div>
                <center> 
                     <div class="col-md-12 col-sm-12">
                    <div class="table-responsive">
                    <asp:Chart ID="ChartSpouse" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                    <br /><br />
                </div>
                         </div>
                </center>
            </asp:Panel>
        </div>
        <!-- end strategy-2 -->
        <!-- Strategies -->
        <div id="strategy-FullStrategy" class="strategy-container">
            <asp:Panel ID="pnlStrategy3" runat="server">
                <asp:Label ID="LabelLatest" runat="server" class="displayNone" Text=" Claim Survivor Benefit to allow Work History Benefit to build to Age 70"></asp:Label></h2>
                <asp:Panel runat="server" ID="ExplainLatest" CssClass="text-justify margiright">
                    <asp:Label ID="StrategyLatest" runat="server" Visible="false" Font-Names="Arial" ForeColor="Black">
                    </asp:Label>
                    <asp:Label ID="NoteLatest" runat="server" Visible="false" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <asp:Label ID="KeyFigures" runat="server" Font-Bold="true" Text="Key Benefit Numbers:" CssClass="margileft" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey11" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsFigures" runat="server" Font-Bold="true" Text="Next Steps:" CssClass="margileft" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps11" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                </asp:Panel>
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                    <p class="fontLarge fontColorBlack" >Click Here to have the Strategies downloaded to your computer</p>
                      <asp:Button ID="btnLatestDownload" runat="server" CssClass="button_login " Width="125px" OnClick="PrintAll_Click" Text="Download" /></center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridLatest" HeaderStyle-Height="45px" runat="server" ForeColor="Black">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margin100 margiright">
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblWifeNote3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblWifeNote7" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                </div>
                <center> 
                    <div class="col-md-12 col-sm-12">
                    <div class="table-responsive">
                    <asp:Chart ID="ChartLatest" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                    <br /><br />
                </div> </div></center>
            </asp:Panel>
        </div>
        <!-- end strategy-3 -->
        <div id="strategy-Claim67Strategy" class="strategy-container">
            <asp:Panel ID="pnlStrategy4" runat="server">
                <asp:Label ID="LabelClaim67" runat="server" class="displayNone" Text="Name to be given"></asp:Label></h2>
                <asp:Panel runat="server" ID="ExplainClaim67" CssClass="text-justify margiright">
                    <asp:Label ID="StrategyClaim67" Visible="false" runat="server" Font-Names="Arial" ForeColor="Black">
                    </asp:Label>
                    <asp:Label ID="NoteClaim67" Visible="false" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <asp:Label ID="lblKeyFigures3" runat="server" Font-Bold="true" Text="Key Benefit Numbers:" CssClass="margileft" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey7" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey8" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey12" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsFigures3" runat="server" Font-Bold="true" Text="Next Steps:" CssClass="margileft" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps7" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps8" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps12" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                </asp:Panel>
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                     <p class="fontLarge fontColorBlack" >Click Here to have the Strategies downloaded to your computer</p>
                     <asp:Button ID="btnClaim67Download" runat="server" CssClass="button_login " Width="125px" OnClick="PrintAll_Click" Text="Download" /></center>
                <%} --%>
                <div class="table-responsive">
                    <asp:GridView ID="GridClaim67" HeaderStyle-Height="45px" runat="server" ForeColor="Black">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margin100 margiright">
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblWifeNote4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify">
                        <asp:Label ID="lblWifeNote8" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                </div>
                <center> 
                     <div class="col-md-12 col-sm-12">
                    <div class="table-responsive">
                    <asp:Chart ID="ChartClaim67" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                    <br /><br />
                </div> </div>
        </center>
            </asp:Panel>
        </div>
        <br />
        <br />
        <a href="../FrmDisclaimer.aspx">Disclaimer </a>
    </asp:Panel>

    <div id="subscription" class="testmodalPopup">
        <center>  <div id="plans" class="plan-contentnew">
          <br />
          <asp:Label ID="MaxCumm"  Style="font-weight: bold; font-size: x-large; color: firebrick" runat="server"></asp:Label>
          <br />
          <h2 class="" style="color: black;">While growing your Social Security Check to the largest amount possible! <br />
               <br />  
              Find out how, for just 39.95 dollars!
                <br />
                <br />
                <asp:Button ID="btnTrial" runat="server" Text="Subscribe" CssClass="button_login" OnClick="btnTrial_Click" />&nbsp;&nbsp;<asp:Button ID="CancelPopup" OnClick="Cancel_Click" runat="server" CssClass="button_login" Text="Cancel" />
            </h2>
        </div></center>
        <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
    </div>

    <asp:Panel ID="pnlExplanation" ScrollBars="Auto" runat="server" CssClass="displayNone ExplanationModalPopup">
        <div class="h2PopUp modal-header" style="color: white; font-size: x-large; font: bold;">
            Explanation of the Strategies
            <asp:Button ID="btnCancelTop" runat="server" CssClass="button_login floatRight" OnClick="imgCancel_Click" Text="Back to the Strategies" />
        </div>
        <div class="textExplanation plan-contentnew">
            <br />
            <div class="bs-example ">
                <div class="panel-group" id="accordion">
                    <center>
                    <div style="font-size: large; font: bold;">
                         Widowed & Widower Spouses
                    </div>
                        </center>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What is the Primary Goal Of the Paid to Wait Calculator?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    The primary goal of the Paid to Wait Calculator is to help you maximize the size of your Social Security benefit. Qualified widowed/widower spouses could have a number of options available to them that could make it easier for them to maximize the amount of their benefit. They could have a choice between claiming a Survivor Benefit or their own Work History Benefit. The Paid to Wait Calculator will automatically determine which benefit you should claim and when you should claim it.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Many Widowed/Widower Spouses Can Get Paid to Wait&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    The Paid to wait Calculator can help make it easier to maximize the size of your benefit by showing you the ONE claiming strategy that will pay you the most amount of Social Security income while you wait. In other words, while you are waiting to claim either your maxed out Work History Benefit, or your maxed out Survivor Benefit, the Paid to Wait Calculator will show you the one claiming strategy that will pay you the most amount of Social Security Income while you wait.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Can All Widowed/Widower Spouses Get Paid to Wait?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    No, not all widowed/widower spouses can get Paid to Wait but many do. If you can get Paid to Wait, the Paid to Wait calculator will show you exactly how to do it.  
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">How the Paid to Wait Calculator Works&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    Depending on your situation, the Paid to Wait Calculator could show you a maximum of three different claiming strategies, sometimes it may show you two strategies and in some cases it could only show you one claiming strategy.<br />
                                    <br />
                                    You could have a choice between claiming your own regular Work History Benefit or claiming a Survivor Benefit. Your deceased spouse’s Social Security benefit is also your Survivor Benefit. You can claim a Survivor Benefit as early as age 60, but the amount of the benefit will be reduced, or you could wait until your Full Retirement Age (your Full Retirement Age depends on the year you were born and can vary between ages 66 and 67) and receive the full benefit amount. The Paid to Wait Calculator will determine which benefit you should try to maximize and then show you exactly how to do that. 
                                    <br />
                                    <br />
                                    In some situations the Paid to Wait Calculator may recommend claiming a particular benefit first and then switching to another benefit later. For example it could tell you to claim a Survivor Benefit at your Full Retirement Age (Once again, your Full Retirement Age depends on the year you were born and can vary between ages 66 to 67) and then later switch to your regular Work History Benefit. In other cases, it may recommend claiming your regular Work History Benefit early and then switching to a Survivor Benefit at your Full Retirement Age. If you qualify to switch from one benefit to another benefit, the Paid to Wait Calculator will show you and tell you exactly what benefit to claim and when to claim it. 
                                    <br />
                                    <br />
                                    Most widowed/widower spouses can get Paid to Wait. The first suggested claiming strategy shown or Strategy 1, is usually the ONE strategy that will pay you the most amount of Social Security income while you wait to maximize the size of your benefit. Many times this strategy will recommend claiming a Survivor Benefit early, even at age 60, or it could recommend claiming a Work History Benefit as early as   age 62. You will collect this benefit for a number of years before the Paid to wait Calculator will tell you when to switch to a bigger benefit either at your Full Retirement Age or at age 70.
                                    <br />
                                    <br />
                                    Remember the primary goal of the Paid to Wait Calculator is to maximize the size of your benefit. Claiming one of these benefits early will make it easier to do that because it could pay you a substantial amount of Social Security income while you wait to maximize the size of the other benefit. If this fits your situation, the Paid to Wait Calculator will show you and tell you exactly how to do it.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What if You Continue to Work?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    If the Paid to Wait Calculator recommends a claiming strategy in which you claim a benefit before your Full Retirement Age, it will also show you a second strategy or Strategy 2. In this strategy it will recommend that you wait until your Full Retirement Age to claim a benefit. It will show you this second strategy in case you decide to continue to work between the ages of 62 and your Full Retirement Age.  If you claim and receive Social Security benefits before your Full Retirement Age and continue to work, you could earn too much money and have to give back some or even all of your benefits. BUT, once you reach your Full Retirement Age, you can receive your Social Security benefits, continue to work and never have to give back any of your benefits no matter how much money you make from your job. That is why the Paid to Wait Calculator will show you this strategy in which you wait until your Full Retirement Age to claim a benefit.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What if I Don’t Want to Wait Too Long?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseSix" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    In many cases in order to receive a maximum benefit you have to wait until age 70 to claim it and the calculator will show you that strategy. If your do not want to wait that long, the Paid to Wait Calculator will also show you one claiming strategy that will be completed when you claim your benefit at your Full Retirement Age, usually sometime between age 66 and 67. The Paid to Wait Calculator will automatically determine which benefit, your Survivor Benefit or your Work History Benefit, will be bigger at your Full Retirement Age. Many times you can even get paid while you are waiting to claim your benefit at your Full Retirement Age.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What You Can Expect&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    Each suggested claiming strategy will include a chart of your benefit amounts and a simple explanation of what benefit should be claimed and exactly when to claim it. The important numbers on the chart are highlighted and correspond to the explanations in both the “Key Benefit  Numbers” section and the “Next Steps” section.
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What are the “Key Benefit Numbers” in the Strategies?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseEight" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    1) Your Paid to Wait number – this is the amount of Social Security income you will receive while you wait to maximize the size of your benefit. Many widowed/widower spouses will have a Paid to Wait number but some will not.
                                    <br />
                                    2) Maxed Out Benefit – the monthly dollar amount of your maxed out benefit.  When the claiming strategy is complete, this is the monthly amount of your Social Security benefit you will receive for the rest of your life. The amount of this benefit and all the other benefits in the chart will increase every year because of Cost of Living or COLA adjustments. The calculator assumes an annual COLA increase of 2.5%.
                                    <br />
                                    3) Annual Income- When the claiming strategy is complete, this shows the amount of Social Security income you will receive every year for the rest of your life. This amount will also increase every year because of COLA adjustments.
                                    <br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What are the “Key Benefit Numbers” in the Strategies?&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseNine" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    1) Your Paid to Wait number – this is the amount of Social Security income you will receive while you wait to maximize the size of your benefit. Many widowed/widower spouses will have a Paid to Wait number but some will not.
                                    <br />
                                    2) Maxed Out Benefit – the monthly dollar amount of your maxed out benefit.  When the claiming strategy is complete, this is the monthly amount of your Social Security benefit you will receive for the rest of your life. The amount of this benefit and all the other benefits in the chart will increase every year because of Cost of Living or COLA adjustments. The calculator assumes an annual COLA increase of 2.5%.
                                    <br />
                                    3) Annual Income- When the claiming strategy is complete, this shows the amount of Social Security income you will receive every year for the rest of your life. This amount will also increase every year because of COLA adjustments.
                                    <br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">What are the “Next Steps” in the Strategies&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseTen" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseTen" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p class="rti-colora1">
                                    This section tells you what benefit to claim and when you should claim it. All numbered steps in this section correspond to the highlighted Ages and Benefit Amounts in the chart. All of the benefit amounts in all the strategies increase slightly every year because the Paid to Wait Calculator includes a 2.5% annual Cost of Living or COLA adjustment.
                                    <br />
                                    <br />
                                    We used a 2.5% COLA increase because that has been the approximate average annual COLA increase over the last 20 years. If some of the benefit amounts in our charts don’t match the benefit amounts from your Social Security statements, especially your Full Retirement Age benefit and your benefit at age 70, that is because Social Security does not make any COLA assumptions when calculating your future benefits.
                                    <br />
                                    <br />
                                    If you decide on a particular claiming strategy, please follow the explanation in the “Next Steps” section to determine exactly when you should claim a specific benefit. Depending on your situation, the Paid to Wait Calculator may tell you to claim a benefit prior to your to your birthday. For example, it may tell you to claim your Work History Benefit at age 66 years and 8 months or four months before your 67th birthday.
                                    <br />
                                    <br />
                                    On the chart for that particular strategy, which includes your benefit amounts, when a benefit is claimed before a birthday, those benefit payment amounts may be carried over to the next year. For example, if a benefit is claimed at age 66 years and 8 months, you would receive only 4 months of benefit payments before your 67th birthday. On the chart, those four months of benefit payments could be carried over to the next year and be included in the total benefit amount you receive at age 67. The chart will indicate this with a # (number sign) or a * (asterisk) that you receive 16 months of payments at age 67, which includes 4 months of payments at age 66 and 12 months of payments at age 67 (4 + 12 = 16 months of payments). The important thing to remember is to follow the specific instructions in the “Next Steps” section of exactly when you should claim a benefit.
                                    <br />
                                    <br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />

                </div>
            </div>
            <p>
                p.s. – Don’t wait until the last minute to claim your benefits, make an appointment with your local Social Security office at least one month prior to the date you want your benefits to begin. 
                <br />
                <br />
            </p>
            <p class="text-center fontBold" style="color: #85312F">
                Good Luck with your claiming strategy!<br />
                <br />
                <asp:Button ID="print" runat="server" Text="Print" CssClass="button_login" OnClick="exportToPdf" />
                <asp:Button ID="btnCancelTopBottom" runat="server" CssClass="button_login " OnClick="imgCancel_Click" Text="Back to the Strategies" />
            </p>
        </div>
    </asp:Panel>
    <asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
    <cc1:ModalPopupExtender ID="popup" runat="server" PopupControlID="pnlExplanation" TargetControlID="lnkFake" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
</asp:Content>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/blockUI.min.js"></script>
    <script type="text/javascript" src="../Scripts/showstrategies.js"></script>
    <script type="text/javascript">
        var userID = '<%= Session["UserID"].ToString() %> ';
        $(document).ready(function () {
            window.scrollTo(0, 400);
            $('#<%=btnTrial.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
            $('#<%=CancelPopup.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
            $('.blockPage').removeAttr('style');
            // $('.blockPage').addClass('subpop');
        });
        $(function () {
            setTimeout(function () {
                $("[id$=ErrorMessagelable]").fadeOut(1500);
            }, 5000);
        });

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
</asp:Content>
