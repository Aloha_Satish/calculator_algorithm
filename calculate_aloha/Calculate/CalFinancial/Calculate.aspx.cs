﻿using CalculateDLL;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Symbolics;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using System.Reflection;
using Calculate.Objects;
using System.Net.Mail;
using Calculate.RestrictAppIncome;

namespace Calculate
{
    public partial class _Default : System.Web.UI.Page
    {
        #region Variable Declaration
        // Input items
        int intAgeWife, intAgeHusb, intAgeWifeMonth, intAgeHusbMonth, intAgeWifeDays, intAgeHusbDays, intCurrentAgeWifeMonth, intCurrentAgeHusbMonth, intCurrentAgeWifeYear, intCurrentAgeHusbYear;
        decimal decBeneWife, decBeneHusb;
        decimal colaAmt = 1;
        int birthYearWife, birthYearHusb;
        decimal halfBenHusb;
        decimal halfbenWife;
        DataTable dtDetails;
        String customerFirstName, strUserFirstName, strUserSpouseFirstName;
        String customerSposeFirstName;

        DateTime dtLimit = new DateTime(1954, 01, 02);
        #endregion

        #region Events
        /// <summary>
        /// btnSaveBasicInfoAndProceed_Click
        /// code to redirect back the user to welcome page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveBasicInfoAndProceed_Click(object sender, EventArgs e)
        {
            try
            {
                Customers customer = new Customers();
                DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                string sIsSubscription = dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription].ToString();
                if (sIsSubscription.Equals(Constants.FlagNo))
                {
                    Response.Redirect(Constants.RedirectWelcomeUser, false);
                }
                else
                {
                    Response.Redirect(Constants.RedirectSocialSecurityCalculator, false);
                    //Response.Redirect("~/CalFinancial/InitialSocialSecurityCalculator.aspx", false);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Function called when Page Load 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ValidateSession();
                if (!IsPostBack)
                {
                    try
                    {
                        if (Session["Demo"] != null)
                        {
                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                            DateTime wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            ValidateAndCalc();

                            if ((Convert.ToDecimal(BeneHusband.Text) < Convert.ToDecimal(BeneWife.Text) && husbandBirthDate < dtLimit && wifeBirthDate >= dtLimit) ||
                             (Convert.ToDecimal(BeneWife.Text) < Convert.ToDecimal(BeneHusband.Text) && wifeBirthDate < dtLimit && husbandBirthDate >= dtLimit))
                            {
                                //Visible button
                                btnHideShowStrat.Visible = true;
                            }

                            //Send mail to the demo user before loading the strategy page
                            Globals.Instance.BoolEmailReport = true;
                            exportAllToPdf();
                            Globals.Instance.BoolEmailReport = false;

                        }
                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                        {
                            ///* Disabled textbox and radio button so that institutional user can only view customer details */
                            ////BtnChange.Text = Constants.BtnChangeText;
                            //BirthWifeYYYY.Enabled = false;
                            //BeneWife.Enabled = false;
                            //BirthHusbandYYYY.Enabled = false;
                            //BeneHusband.Enabled = false;
                            //CheckBoxSurv.Enabled = false;
                            //Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.ClientScriptDatePicker, "disableDatePicker();", true);
                            //// Initialize page variables
                            //ErrorMessage.Text = String.Empty;
                            //PanelGrids.Visible = false;
                            //PanelParms.Visible = false;
                            ////CheckBoxCola.Checked = true;
                            //BirthWifeMM.Focus();
                            //ShowAllInformation();

                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                            DateTime wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            PanelEntry.Visible = false;
                            ValidateAndCalc();

                            if ((Convert.ToDecimal(BeneHusband.Text) < Convert.ToDecimal(BeneWife.Text) && husbandBirthDate < dtLimit && wifeBirthDate >= dtLimit) ||
                             (Convert.ToDecimal(BeneWife.Text) < Convert.ToDecimal(BeneHusband.Text) && wifeBirthDate < dtLimit && husbandBirthDate >= dtLimit))
                            {
                                //Visible button
                                btnHideShowStrat.Visible = true;
                            }

                            if (Globals.Instance.BoolEmailReport || Globals.Instance.BoolAdvisorDownloadReport)
                            {
                                Globals.Instance.BoolAdvisorDownloadReport = false;

                                exportAllToPdf();
                                Response.Redirect(Constants.RedirectManageCustomers);
                            }

                        }
                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                        {
                            ///* Disabled textbox and radio button so that institutional user can only view customer details */
                            ////BtnChange.Text = Constants.BtnChangeText;
                            //BirthWifeYYYY.Enabled = false;
                            //BeneWife.Enabled = false;
                            //BirthHusbandYYYY.Enabled = false;
                            //BeneHusband.Enabled = false;
                            //CheckBoxSurv.Enabled = false;
                            //Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.ClientScriptDatePicker, "disableDatePicker();", true);
                            //// Initialize page variables
                            //ErrorMessage.Text = String.Empty;
                            //PanelGrids.Visible = false;
                            //PanelParms.Visible = false;
                            ////CheckBoxCola.Checked = true;
                            //BirthWifeMM.Focus();
                            //ShowAllInformation();

                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                            DateTime wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            PanelEntry.Visible = false;
                            ValidateAndCalc();

                            if ((Convert.ToDecimal(BeneHusband.Text) < Convert.ToDecimal(BeneWife.Text) && husbandBirthDate < dtLimit && wifeBirthDate >= dtLimit) ||
                             (Convert.ToDecimal(BeneWife.Text) < Convert.ToDecimal(BeneHusband.Text) && wifeBirthDate < dtLimit && husbandBirthDate >= dtLimit))
                            {
                                //Visible button
                                btnHideShowStrat.Visible = true;
                            }

                            if (Globals.Instance.BoolEmailReport || Globals.Instance.BoolAdvisorDownloadReport)
                            {
                                Globals.Instance.BoolAdvisorDownloadReport = false;

                                exportAllToPdf();
                                Response.Redirect(Constants.RedirectManageCustomers);
                            }

                        }
                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor))
                        {
                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                            DateTime wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            ValidateAndCalc();

                            if ((Convert.ToDecimal(BeneHusband.Text) < Convert.ToDecimal(BeneWife.Text) && husbandBirthDate < dtLimit && wifeBirthDate >= dtLimit) ||
                             (Convert.ToDecimal(BeneWife.Text) < Convert.ToDecimal(BeneHusband.Text) && wifeBirthDate < dtLimit && husbandBirthDate >= dtLimit))
                            {
                                //Visible button
                                btnHideShowStrat.Visible = true;
                            }

                            if (Globals.Instance.BoolEmailReport || Globals.Instance.BoolAdvisorDownloadReport)
                            {
                                Globals.Instance.BoolAdvisorDownloadReport = false;

                                exportAllToPdf();
                                Response.Redirect(Constants.RedirectManageCustomers);
                            }

                        }
                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.Customer))
                        {
                            Customers customer = new Customers();

                            DateTime wifeBirthDate, husbandBirthDate;
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());

                            wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            PanelGrids.Visible = false;//Added for new development (on 16/09)
                            PanelEntry.Visible = false;//Added for new development (on 16/09)
                            ValidateAndCalc();
                            //if (wifeBirthDate < dtLimit || husbandBirthDate < dtLimit)
                            //    btnHideShowStrat.Visible = true;

                            if ((Convert.ToDecimal(BeneHusband.Text) < Convert.ToDecimal(BeneWife.Text) && husbandBirthDate < dtLimit && wifeBirthDate >= dtLimit) ||
                              (Convert.ToDecimal(BeneWife.Text) < Convert.ToDecimal(BeneHusband.Text) && wifeBirthDate < dtLimit && husbandBirthDate >= dtLimit))
                            {
                                //Visible button
                                btnHideShowStrat.Visible = true;
                            }

                            if (Convert.ToString(dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription]).Equals(Constants.FlagNo))
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "displaySubscriptionPopUp(false);", true);
                                ValidateAndCalc();
                                //to show the Paid to wait value on the popup for new user
                                if (Session[Constants.MaxCumm] != null)
                                {
                                    decimal amt = decimal.Parse(Session[Constants.MaxCumm].ToString());
                                    MaxCumm.Text = Constants.SubscriptionPopupText + Environment.NewLine + amt.ToString(Constants.AMT_FORMAT);
                                }
                            }
                            //Commented for new development (on 16/09)
                            //else
                            //    ValidateAndCalc();

                        }
                        else if (!string.IsNullOrEmpty(Session["CustAfterTrans"] as string))
                        {
                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                            DateTime wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            ValidateAndCalc();
                        }
                        else
                        {
                            // Initialize page variables
                            ErrorMessage.Text = String.Empty;
                            PanelGrids.Visible = false;
                            PanelParms.Visible = false;
                            //CheckBoxCola.Checked = true;
                            BirthWifeMM.Focus();
                            ShowAllInformation();
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                        ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                        ErrorMessagelable.Visible = true;
                    }
                }
                else
                {
                    //make the first radio button selected by default when page loads
                    rdbtnStrategy1.Checked = false;
                    rdbtnStrategy2.Checked = false;
                    rdbtnStrategy3.Checked = false;

                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Button to proceed to PIA and update the DOB info
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Calculate_Click(object sender, EventArgs e)
        {
            try
            {
                Customers customer = new Customers();
                DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                Button clicked = (Button)sender;
                ResetValues();

                if (clicked.Text == BtnCalculate.Text)
                {
                    if (Convert.ToString(dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription]).Equals(Constants.FlagNo))
                    {
                        if (Session[Constants.SessionRegistered] != null)
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "displaySubscriptionPopUp(true);", true);
                        }
                        else
                        {
                            if (BeneWife.Text != string.Empty && BeneHusband.Text != string.Empty)
                            {
                                //display the Subscription popup for unregistered user
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "displaySubscriptionPopUp(false);", true);
                                ValidateAndCalc();
                                //to show the Paid to wait value on the popup for new user
                                decimal amt = decimal.Parse(Session[Constants.MaxCumm].ToString());
                                MaxCumm.Text = Constants.SubscriptionPopupText + Environment.NewLine + amt.ToString(Constants.AMT_FORMAT);
                            }
                        }
                    }
                    else
                    {
                        //if user is registered validate his inputs
                        ValidateAndCalc();
                    }
                }
                else // if (clicked.Text == BtnChange.Text)
                {
                    if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                    {
                        Response.Redirect(Constants.RedirectSocialSecurityCalculator, false);
                    }
                    else
                    {
                        //varibale to show and hide the cliam and suspend strategy made false
                        Globals.Instance.PDFShowClaimandSuspend = true;
                        BackToParms();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Print Pdf
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PrintAll_Click(object sender, EventArgs e)
        {
            try
            {
                //export the grids and Charts to PDF
                exportAllToPdf();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.DownloadError;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// subscribe user who are in trial period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnTrial_Click(object sender, EventArgs e)
        {
            try
            {
                // Update column with package plan
                Customers customer = new Customers();
                customer.updateCustomerSubscriptionPlan(Session[Constants.SessionUserId].ToString(), Constants.WithoutTrialPeriod);
                //Changes for live Server
                //Response.Redirect(Constants.NewSingleUseURL + Session[Constants.SessionUserId].ToString(), true);

                //Changes for InfusionSoft live Server
                Response.Redirect(Constants.NewSingleUseURLInfusion, true);

                // Redirect to chargify url 
                // Response.Redirect( Constants.TrialTestServerURL+ Session[Constants.SessionUserId].ToString(), true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.TryAgain;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Subscribe user who are not in trial period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnWithoutTrial_Click(object sender, EventArgs e)
        {
            try
            {
                // Update column with package plan
                Customers customer = new Customers();
                customer.updateCustomerSubscriptionPlan(Session[Constants.SessionUserId].ToString(), Constants.TrialPeriod);
                //Changes for live Server
                Response.Redirect(Constants.WithoutTrialLiveServerURL + Session[Constants.SessionUserId].ToString(), true);
                // Redirect to chargify url 
                // Response.Redirect(Constants.WithoutTrialTestServerURL+ Session[Constants.SessionUserId].ToString(), true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.TryAgain;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to cancel the subscription popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Close_Click(object sender, EventArgs e)
        {
            try
            {
                //Previous Redirection
                //redirect on cancel button click on the popup
                //Response.Redirect(Constants.RedirectCalcMarried, false);
                Response.Redirect(Constants.RedirectInitialSocialSecurity, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.TryAgain;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// to close the explanation popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgCancel_Click(object sender, EventArgs e)
        {
            try
            {
                popup.Hide();
                ValidateAndCalc();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to show the explanation popup for all strategies.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExplain_Click(object sender, EventArgs e)
        {
            try
            {
                popup.Show();
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "displayPanelExplanation();", true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion

        #region Validation

        /// <summary>
        /// Validate Session
        /// </summary>
        public void ValidateSession()
        {
            try
            {
                //if user session is expired redirstc him to login again
                Session[Constants.SessionRegistered] = null;
                if (Session[Constants.SessionUserId] == null)
                {
                    Response.Redirect(Constants.RedirectLogin, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Check input values
        /// </summary>
        /// <returns></returns>
        protected bool ValidateInput()
        {
            try
            {
                /* Initialize Variable */
                ErrorMessage.Text = string.Empty;
                bool isValid = true;
                int birthYYYY, birthMM, birthDD;
                DateTime today = DateTime.Now;
                DateTime wifeBD = today;
                Calc calc = new Calc();
                /* Call function to validate numeric range for DOB */
                birthMM = SiteNew.ValidateNumeric(BirthWifeMM, 1, 12, Constants.Text0, ref isValid, ErrorMessage);
                birthDD = SiteNew.ValidateNumeric(BirthWifeDD, 1, 31, Constants.Text1, ref isValid, ErrorMessage);
                birthYYYY = SiteNew.ValidateNumeric(birthWifeTempYYYY, 1900, 2020, Constants.Text2, ref isValid, ErrorMessage);
                if (isValid)
                {
                    birthYearWife = int.Parse(birthWifeTempYYYY.Text);
                    wifeBD = new DateTime(birthYYYY, birthMM, birthDD);
                }
                intAgeWife = intAgeForWife(wifeBD);
                if (wifeBD.Day >= 28 && wifeBD.Day != DateTime.Now.Day)
                {
                    intAgeWifeMonth = wifeBD.Month + 1;
                    intAgeWifeDays = wifeBD.Day;
                }
                else
                {
                    intAgeWifeMonth = wifeBD.Month;
                    intAgeWifeDays = wifeBD.Day;
                }
                DateTime husbBD = today;
                birthMM = SiteNew.ValidateNumeric(BirthHusbandMM, 1, 12, Constants.SpouseText0, ref isValid, ErrorMessage);
                birthDD = SiteNew.ValidateNumeric(BirthHusbandDD, 1, 31, Constants.SpouseText1, ref isValid, ErrorMessage);
                birthYYYY = SiteNew.ValidateNumeric(BirthHusbandTempYYYY, 1900, 2020, Constants.SpouseText2, ref isValid, ErrorMessage);
                if (isValid)
                {
                    birthYearHusb = int.Parse(BirthHusbandTempYYYY.Text);
                    husbBD = new DateTime(birthYYYY, birthMM, birthDD);
                }
                intAgeHusb = intAgeForHusband(husbBD);
                if (husbBD.Day >= 28 && husbBD.Day != DateTime.Now.Day)
                {
                    intAgeHusbMonth = husbBD.Month + 1;
                    intAgeHusbDays = husbBD.Day;
                }
                else
                {
                    intAgeHusbMonth = husbBD.Month;
                    intAgeHusbDays = husbBD.Day;
                }
                //try and parse the benefits for husband and wife
                decimal.TryParse(this.BeneWife.Text, out decBeneWife);
                decimal.TryParse(this.BeneHusband.Text, out decBeneHusb);
                if (isValid)
                {
                    Customers customer = new Customers();
                    PanelGrids.Visible = true;
                    PanelEntry.Visible = false;
                    PanelParms.Visible = true;
                    lblCustomerName.Text = customerFirstName;
                    lblCustomerSpouseName.Text = customerSposeFirstName;
                    lblCustomerDOB.Text = husbBD.ToShortDateString();
                    lblCustomerSpouseDOB.Text = wifeBD.ToShortDateString();
                    lblCustomerFRA.Text = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                    lblCustomerSpouseFRA.Text = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                    lblCustomerBenefit.Text = decBeneHusb.ToString(Constants.AMT_FORMAT);
                    lblCustomerSpouseBenefit.Text = decBeneWife.ToString(Constants.AMT_FORMAT);
                }
                else
                {
                    ErrorMessage.Text += ".";
                }
                if (!String.IsNullOrEmpty(ErrorMessage.Text))
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayErrorMessage();", true);
                else
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "hideErrorMessage();", true);
                return isValid;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                return false;
            }
        }

        /// <summary>
        /// calculate current age of husband
        /// </summary>
        /// <param name="husbBD"></param>
        /// <returns></returns>
        protected int intAgeForHusband(DateTime husbBD)
        {
            try
            {
                TimeSpan Span = DateTime.Now - husbBD;
                DateTime Age = DateTime.MinValue + Span;
                int Years = Age.Year - 1;
                int Months = Age.Month - 1;
                int Days = Age.Day - 1;
                intCurrentAgeHusbYear = Years;
                Globals.Instance.strCurrentAgeHusband = Years.ToString() + " years and " + Months.ToString() + " months ";
                if (Days >= 28)
                    Months += 1;
                if (Months >= 6)
                    Years += 1;
                intCurrentAgeHusbMonth = Months;
                return Years;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                return 0;
            }
        }

        /// <summary>
        /// calculate current age of Wife
        /// </summary>
        /// <param name="husbBD"></param>
        /// <returns></returns>
        protected int intAgeForWife(DateTime wifeBD)
        {
            try
            {
                TimeSpan Span = DateTime.Now - wifeBD;
                DateTime Age = DateTime.MinValue + Span;
                int Years = Age.Year - 1;
                int Months = Age.Month - 1;
                int Days = Age.Day - 1;
                intCurrentAgeWifeYear = Years;
                Globals.Instance.strCurrentAgeWife = Years.ToString() + " years and " + Months.ToString() + " months ";
                if (Days >= 28)
                    Months += 1;
                if (Months >= 6)
                    Years += 1;
                intCurrentAgeWifeMonth = Months;
                return Years;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                return 0;
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
        }

        /// <summary>
        /// Requested calculation validate data entered in the form
        /// </summary>
        protected void ValidateAndCalc()
        {
            try
            {
                if (Session["Demo"] == null)
                {
                    Customers customer = new Customers();
                    if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor) || Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                        dtDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                    else
                        dtDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                    customerFirstName = dtDetails.Rows[0][Constants.CustomerFirstName].ToString();
                    customerSposeFirstName = dtDetails.Rows[0][Constants.CustomerSpouseFirstName].ToString();

                }
                else
                {
                    customerFirstName = Session["DemoHusbandName"].ToString();
                    customerSposeFirstName = Session["DemoWifeName"].ToString();
                }


                string WifeBirth = BirthWifeYYYY.Text.ToString();
                char[] delimiters = new char[] { '/' };
                string[] WifeBirthArray = WifeBirth.Split(delimiters, 3);
                birthWifeTempYYYY.Text = WifeBirthArray[2];
                BirthWifeMM.Text = WifeBirthArray[0];
                BirthWifeDD.Text = WifeBirthArray[1];
                string HusbandBirth = BirthHusbandYYYY.Text.ToString();
                string[] HusbandBirthArray = HusbandBirth.Split(delimiters, 3);
                BirthHusbandTempYYYY.Text = HusbandBirthArray[2];
                BirthHusbandMM.Text = HusbandBirthArray[0];
                BirthHusbandDD.Text = HusbandBirthArray[1];


                if (ValidateInput())
                {
                    BuildPage();
                    // Save All Details to respective tables According to martial status
                    UpdateCustomerFinancialDetails();
                    /* Render GridView Content into Chart */
                    //SiteNew.renderGridViewIntoStackedChart(GridAsap, ChartAsap, LabelAsap, Constants.Married, true);
                    SiteNew.renderGridViewIntoStackedChart(GridBest, ChartBest, LabelBest, Constants.Married, true);
                    SiteNew.renderGridViewIntoStackedChart(GridMax, ChartMax, LabelBest, Constants.Married, true);
                    SiteNew.renderGridViewIntoStackedChart(GridSuspendLater, ChartSuspendLater, LabelSuspendLater, Constants.Married, true);
                    SiteNew.renderGridViewIntoStackedChart(GridBestLater, ChartBestLater, LabelLater, Constants.Married, true);
                    SiteNew.renderGridViewIntoStackedChart(GridEarly, ChartEarly, LabelEarlySuspend, Constants.Married, true);
                    SiteNew.renderGridViewIntoStackedChart(GridZero, ChartZero, lblZeroStrat, Constants.Married, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }

        }

        #endregion

        #region CRUD Operation

        /// <summary>
        /// This method basic information of customer in form
        /// </summary>
        /// Date: 11 June 2014
        private void ShowAllInformation()
        {
            try
            {
                Customers customer = new Customers();
                //DataTable dtCustomerFinancialDetails = customer.getCustomerFinancialDetailsByStatus(Session[Constants.SessionUserId].ToString(), Constants.Married);
                ///* Check if customer have details or not */
                //if (dtCustomerFinancialDetails.Rows.Count > 0)
                //{
                //    /* Check if Wife Birth Date Exists or Not */
                //    if (!String.IsNullOrEmpty(dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoMarriedWifesBirthDate].ToString()))
                //    {
                //        DateTime strWifesBirthDate = (DateTime)dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoMarriedWifesBirthDate];
                //        BirthWifeDD.Text = strWifesBirthDate.Day.ToString();
                //        BirthWifeMM.Text = strWifesBirthDate.Month.ToString();
                //        BirthWifeYYYY.Text = strWifesBirthDate.ToShortDateString();
                //    }
                //    /* Check if Husband Birth Date Exists or Not */
                //    if (!String.IsNullOrEmpty(dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoMarriedHusbandsBirthDate].ToString()))
                //    {
                //        DateTime strBirthHusbandDate = (DateTime)dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoMarriedHusbandsBirthDate];
                //        BirthHusbandDD.Text = strBirthHusbandDate.Day.ToString();
                //        BirthHusbandMM.Text = strBirthHusbandDate.Month.ToString();
                //        BirthHusbandYYYY.Text = strBirthHusbandDate.ToShortDateString();
                //    }

                //    BeneWife.Text = dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoMarriedWifesRetAgeBenefit].ToString();
                //    BeneHusband.Text = dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoMarriedHusbandsRetAgeBenefit].ToString();
                //    Your_Name.Text = dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoMarriedFirstname].ToString();
                //    Spouse_Name.Text = dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoMarriedSpouseFirstname].ToString();
                //    string strSurvivorBenefit = dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoMarriedSurvivorBenefit].ToString();
                //    string strCola = dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoCola].ToString();
                //    if (strSurvivorBenefit.Equals("T"))
                //        CheckBoxSurv.Checked = true;
                //    else
                //        CheckBoxSurv.Checked = false;

                //    //if (strCola.Equals("T"))
                //    //    CheckBoxCola.Checked = true;
                //    //else
                //    //    CheckBoxCola.Checked = false;
                //}
                //else
                //{
                /* Fill Basic info from Customer Table */
                DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                Your_Name.Text = dtCustomerDetails.Rows[0][Constants.SecurityInfoMarriedFirstname].ToString();
                Spouse_Name.Text = dtCustomerDetails.Rows[0][Constants.SecurityInfoMarriedSpouseFirstname].ToString();
                /* Check if Birthdate exists or not */
                if (!String.IsNullOrEmpty(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString()))
                {
                    DateTime strBirthDate = (DateTime)dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate];
                    BirthWifeDD.Text = strBirthDate.Day.ToString();
                    BirthWifeMM.Text = strBirthDate.Month.ToString();
                    BirthWifeYYYY.Text = strBirthDate.ToShortDateString();
                }
                /* Check if spouse birthdate exists or not */
                if (!String.IsNullOrEmpty(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString()))
                {
                    DateTime strSpouseDate = (DateTime)dtCustomerDetails.Rows[0][Constants.CustomerBirthDate];
                    BirthHusbandDD.Text = strSpouseDate.Day.ToString();
                    BirthHusbandMM.Text = strSpouseDate.Month.ToString();
                    BirthHusbandYYYY.Text = strSpouseDate.ToShortDateString();
                }
                //}
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to update the DOB and PIA in DB
        /// </summary>
        public void UpdateCustomerFinancialDetails()
        {
            try
            {
                SecurityInfoMarried securityInfoMarried = new SecurityInfoMarried();
                securityInfoMarried.CustomerID = Session[Constants.SessionUserId].ToString();
                securityInfoMarried.WifesBirthDate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(BirthWifeMM.Text + Constants.BackSlash + BirthWifeDD.Text + Constants.BackSlash + birthWifeTempYYYY.Text));
                securityInfoMarried.HusbandsBirthDate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(BirthHusbandMM.Text + Constants.BackSlash + BirthHusbandDD.Text + Constants.BackSlash + BirthHusbandTempYYYY.Text));
                securityInfoMarried.WifesRetAgeBenefit = BeneWife.Text;
                securityInfoMarried.HusbandsRetAgeBenefit = BeneHusband.Text;
                if (CheckBoxSurv.Checked)
                    securityInfoMarried.SurvivorBenefit = Constants.ColaStatusT;
                else
                    securityInfoMarried.SurvivorBenefit = "F";
                // if (CheckBoxCola.Checked)
                securityInfoMarried.Cola = Constants.ColaStatusT;
                //else
                // securityInfoMarried.Cola = "F";
                Customers customer = new Customers();
                customer.UpdateCustomerFinancialDetailsForMarried(securityInfoMarried);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion

        #region Other Operation

        /// <summary>
        /// code to export the grid and Chart to PDF
        /// </summary>
        public void exportAllToPdf()
        {
            try
            {
                #region code get and set the variables info for PDF
                /* Get Customer Details for showing up in PDF  */
                Customers customer = new Customers();
                DataTable dtCustomerDetails;
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor) || Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                    dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                else
                    dtCustomerDetails = customer.getCustomerFinancialDetailsByStatus(Session[Constants.SessionUserId].ToString(), Constants.Married);
                decimal Benefit, SpouseBenefit;
                string Bene = dtCustomerDetails.Rows[0][Constants.SecurityInfoMarriedHusbandsRetAgeBenefit].ToString();
                decimal.TryParse(Bene, out Benefit);
                string BeneSpouse = dtCustomerDetails.Rows[0][Constants.SecurityInfoMarriedWifesRetAgeBenefit].ToString();
                decimal.TryParse(BeneSpouse, out SpouseBenefit);
                string custFirstName = dtCustomerDetails.Rows[0][Constants.CustomerFirstName].ToString();
                MemoryStream objMemory = new MemoryStream();
                Document pdfDoc = new Document(PageSize.A4, Constants.zero, Constants.zero, -15f, 55f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, objMemory);
                string sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                string sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                decimal halfBenHusb = SpouseBenefit / 2;
                decimal halfBenWife = Benefit / 2;
                string strOwnBenefit = Benefit.ToString(Constants.AMT_FORMAT);
                string strSpousalbenefit = SpouseBenefit.ToString(Constants.AMT_FORMAT);
                string strEmail = string.Empty;
                #endregion code get and set the variables info for PDF

                #region Code to print first page of the PDF
                /* Open the stream */
                pdfDoc.Open();
                writer.PageEvent = new PDFEvent();
                // Front PDF Page
                SiteNew.PdfFrontPage(dtCustomerDetails, ref pdfDoc);


                pdfDoc.NewPage();
                //get a table for 3 colums
                PdfPTable table = new PdfPTable(3);
                //set the column width
                float[] widths = new float[] { 1f, 1f, 1f };
                //set the table width
                table.TotalWidth = 550f;
                //fix the table width
                table.LockedWidth = true;
                //adding space in the table
                table.DefaultCell.Padding = 3f;
                table.SetWidths(widths);
                //remove the table borders
                table.DefaultCell.Border = PdfPCell.NO_BORDER;
                //set the font for the text in the table cells
                BaseFont customfont = BaseFont.CreateFont(HttpContext.Current.Server.MapPath("~") + Constants.PdfRobotoFontStyle, BaseFont.CP1252, BaseFont.EMBEDDED);
                iTextSharp.text.Font font = new iTextSharp.text.Font(customfont, 16);
                //add 1 row to the table as customer name
                table.AddCell(new Phrase(Constants.customerName, font));
                //if (Session["Demo"] != null)
                //{
                //    Benefit = Convert.ToDecimal(Session["DemoHusbandBenefit"]);
                //    SpouseBenefit = Convert.ToDecimal(Session["DemoHusbandBenefit"]);

                //    table.AddCell(new Phrase(Session["DemoHusbandName"].ToString(), font));
                //    table.AddCell(new Phrase(Session["DemoWifeName"].ToString(), font));
                //    //add 2 row to the table as date of births
                //    table.AddCell(new Phrase(Constants.customerDob, font));
                //    table.AddCell(new Phrase(string.Format(Constants.stringDateFormat, Convert.ToDateTime(Session["DemoHusbandBDate"].ToString())), font));
                //    table.AddCell(new Phrase(string.Format(Constants.stringDateFormat, Convert.ToDateTime(Session["DemoWifeBDate"].ToString())), font));

                //}
                //else
                //{
                table.AddCell(new Phrase(dtCustomerDetails.Rows[0][Constants.CustomerFirstName].ToString(), font));
                table.AddCell(new Phrase(dtCustomerDetails.Rows[0][Constants.CustomerSpouseFirstName].ToString(), font));
                //add 2 row to the table as date of births
                table.AddCell(new Phrase(Constants.customerDob, font));
                table.AddCell(new Phrase(string.Format(Constants.stringDateFormat, Convert.ToDateTime(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString())), font));
                table.AddCell(new Phrase(string.Format(Constants.stringDateFormat, Convert.ToDateTime(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString())), font));
                //add 3 row to the table as full retirement ages
                table.AddCell(new Phrase(Constants.customerFullRetirementAge, font));
                table.AddCell(new Phrase(sHusbFRA, font));
                table.AddCell(new Phrase(sWifeFRA, font));
                //add 4 row to the table as PIA's
                table.AddCell(new Phrase(Constants.customerPIA, font));
                table.AddCell(new Phrase(strOwnBenefit, font));
                table.AddCell(new Phrase(strSpousalbenefit, font));
                //add 5 row to the table as maritial status
                table.AddCell(new Phrase(Constants.PdfCustomerMaritalStatus, font));
                table.AddCell(new Phrase(dtCustomerDetails.Rows[0][Constants.CustomerMaritalStatus].ToString(), font));
                table.AddCell(string.Empty);
                //add 6 row to the table as COLA percentage
                table.AddCell(new Phrase(Constants.customerIncludeCOLA, font));
                table.AddCell(new Phrase(Constants.COLAPercentage, font));
                table.AddCell(string.Empty);
                //add table to the document
                pdfDoc.Add(table);
                pdfDoc.Add(SiteNew.generateImage(Constants.pdfHorizontalLine, Constants.pdfImageWidth, 1f));
                pdfDoc.Add(new Paragraph(" "));
                #endregion Code to print first page of the PDF

                #region Code to find best plan and build the PDF content
                Calc calc = new Calc();
                var list = new List<KeyValuePair<string, int>>();
                string sFirstGrid = string.Empty;
                string sSecondGrid = string.Empty;
                string sThirdGrid = string.Empty;
                DateTime HusbDOB = DateTime.Parse(BirthHusbandYYYY.Text.ToString());
                DateTime WifeDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                float w = calc.FRA(int.Parse(birthWifeTempYYYY.Text));
                float h = calc.FRA(int.Parse(BirthHusbandTempYYYY.Text));
                DateTime limit = DateTime.Parse("05/01/1950");
                DateTime limit1 = DateTime.Parse("01/01/1954");
                DateTime StrategyUsageLimit = DateTime.Parse("05/01/2016");
                //TimeSpan ageYearsh = limit.Subtract(HusbDOB);
                //DateTime Age = DateTime.MinValue + ageYearsh;
                //int Years = Age.Year - 1;
                double LPIA, HPIA = 0;
                //code to higher PIA and lower PIA values
                decimal decBenefitHusb = decimal.Parse(BeneHusband.Text.ToString());
                decimal decBenefitWife = decimal.Parse(BeneWife.Text.ToString());
                GridView gFirstGrid = new GridView();
                GridView gSecondGrid = new GridView();
                GridView gThirdGrid = new GridView();
                Chart cFirstChart = new Chart();
                Chart cSecondChart = new Chart();
                Chart cThirdChart = new Chart();
                Label lFirstLable = new Label();
                Label lSecondLabel = new Label();
                Label lThirdLabel = new Label();

                if (decBenefitHusb == 0 || decBenefitWife == 0)
                {

                    if (Globals.Instance.BoolShowSecondZeroStrategy)
                    {
                        list.Add(new KeyValuePair<string, int>(Constants.ZeroStrategy, int.Parse(Session["ZeroCumm"].ToString())));
                        list.Add(new KeyValuePair<string, int>(Constants.GridBestLater, int.Parse(Session[Constants.FullCumm].ToString())));
                        sFirstGrid = list[0].Key.ToString();
                        gFirstGrid = GridZero;
                        cFirstChart = ChartZero;
                        sSecondGrid = list[1].Key.ToString();
                        gSecondGrid = GridBestLater;
                        cSecondChart = ChartBestLater;
                        lFirstLable.Text = Constants.Strategy1;
                        lSecondLabel.Text = Constants.Strategy2;
                    }
                    else
                    {
                        list.Add(new KeyValuePair<string, int>(Constants.ZeroStrategy, int.Parse(Session["ZeroCumm"].ToString())));
                        sFirstGrid = list[0].Key.ToString();
                        gFirstGrid = GridZero;
                        cFirstChart = ChartZero;
                        lFirstLable.Text = Constants.Strategy1;
                    }
                }
                else
                {
                    if (Benefit > SpouseBenefit)
                    {
                        HPIA = double.Parse(BeneHusband.Text.ToString());
                        LPIA = double.Parse(BeneWife.Text.ToString());
                    }
                    else
                    {
                        LPIA = double.Parse(BeneHusband.Text.ToString());
                        HPIA = double.Parse(BeneWife.Text.ToString());
                    }
                    if (HusbDOB < limit)
                    {
                        if (Session[Constants.BestDisplay].Equals(Constants.None))
                            list.Add(new KeyValuePair<string, int>(Constants.GridEarly, int.Parse(Session[Constants.EarlySuspendCumm].ToString())));
                        else
                            list.Add(new KeyValuePair<string, int>(Constants.GridBest, int.Parse(Session[Constants.BestCumm].ToString())));
                    }
                    else
                    {
                        list.Add(new KeyValuePair<string, int>(Constants.GridBest, int.Parse(Session[Constants.BestCumm].ToString())));
                    }
                    ////Added on 24/10/2016
                    //if (int.Parse(Session[Constants.FullCumm].ToString()) > int.Parse(Session[Constants.BestCumm].ToString()) && int.Parse(Session[Constants.valueAtAge70Full].ToString()) > int.Parse(Session[Constants.valueAtAge703].ToString()))
                    list.Add(new KeyValuePair<string, int>(Constants.GridBestLater, int.Parse(Session[Constants.FullCumm].ToString())));

                    if (Convert.ToDecimal(Session[Constants.valueAtAge70Full].ToString()) != Convert.ToDecimal(Session[Constants.valueAtAge70Max].ToString()) && Globals.Instance.BoolShowMaxStrategy)
                        list.Add(new KeyValuePair<string, int>(Constants.GridMax, 0));
                    if ((Convert.ToDecimal(Session[Constants.valueAtAge70Full].ToString()) == Convert.ToDecimal(Session[Constants.valueAtAge703].ToString())) ||
                        (Convert.ToDecimal(Session[Constants.valueAtAge70Full].ToString()) == Convert.ToDecimal(Session[Constants.valueAtAge704].ToString())))
                    {
                        if ((Convert.ToDecimal(Session[Constants.valueAtAge693].ToString()) >= Convert.ToDecimal(Session[Constants.valueAtAge69Full].ToString())) ||
                        (Convert.ToDecimal(Session[Constants.valueAtAge694].ToString()) >= Convert.ToDecimal(Session[Constants.valueAtAge69Full].ToString())))
                        {
                            if (list.Count == 2)
                                list.Remove(list[1]);
                        }
                        else if ((Convert.ToDecimal(Session[Constants.valueAtAge693].ToString()) <= Convert.ToDecimal(Session[Constants.valueAtAge69Full].ToString())) ||
                         (Convert.ToDecimal(Session[Constants.valueAtAge694].ToString()) <= Convert.ToDecimal(Session[Constants.valueAtAge69Full].ToString())))
                        {
                            if (LPIA < (0.4 * HPIA))
                                list.Remove(list[0]);
                        }
                    }
                    else if (Convert.ToInt32(w) <= int.Parse(Session["intAgeHusb"].ToString()) && Convert.ToInt32(h) <= int.Parse(Session["intAgeWife"].ToString()))
                    {
                        if (list.Count == 2)
                            list.Remove(list[1]);

                    }
                    //Added new case on 16/01/2017 to hide 2nd strategy
                    if (Globals.Instance.BoolHideSecondStrat)
                    {
                        if (int.Parse(Session[Constants.BestCumm].ToString()) >= int.Parse(Session[Constants.FullCumm].ToString()))
                            if (list.Count >= 2)
                                list.Remove(list[1]);
                    }


                    if (list.Count == 3)
                    {
                        sFirstGrid = list[0].Key.ToString();
                        sSecondGrid = list[1].Key.ToString();
                        sThirdGrid = list[2].Key.ToString();
                    }
                    else if (list.Count == 2)
                    {
                        sFirstGrid = list[0].Key.ToString();
                        sSecondGrid = list[1].Key.ToString();
                    }
                    else
                    {
                        sFirstGrid = list[0].Key.ToString();
                    }


                    if (sFirstGrid.Equals(Constants.GridEarly))
                    {
                        gFirstGrid = GridEarly;
                        cFirstChart = ChartEarly;
                        lFirstLable.Text = Constants.Strategy1;
                    }
                    if (sFirstGrid.Equals(Constants.GridBest))
                    {
                        gFirstGrid = GridBest;
                        cFirstChart = ChartBest;
                        lFirstLable.Text = Constants.Strategy1;
                    }
                    if (sFirstGrid.Equals(Constants.GridSuspendLater))
                    {
                        gFirstGrid = GridSuspendLater;
                        cFirstChart = ChartSuspendLater;
                        lFirstLable.Text = Constants.Strategy1;
                    }
                    if (sFirstGrid.Equals(Constants.GridBestLater))
                    {
                        gFirstGrid = GridBestLater;
                        cFirstChart = ChartBestLater;
                        lFirstLable.Text = Constants.Strategy1;
                    }
                    if (sFirstGrid.Equals(Constants.GridMax))
                    {
                        gFirstGrid = GridMax;
                        cFirstChart = ChartMax;
                        lFirstLable.Text = Constants.Strategy1;
                    }
                    if (sSecondGrid.Equals(Constants.GridEarly))
                    {
                        gSecondGrid = GridEarly;
                        cSecondChart = ChartEarly;
                        lSecondLabel.Text = Constants.Strategy2;
                    }
                    if (sSecondGrid.Equals(Constants.GridBest))
                    {
                        gSecondGrid = GridBest;
                        cSecondChart = ChartBest;
                        lSecondLabel.Text = Constants.Strategy2;
                    }
                    if (sSecondGrid.Equals(Constants.GridSuspendLater))
                    {
                        gSecondGrid = GridSuspendLater;
                        cSecondChart = ChartSuspendLater;
                        lSecondLabel.Text = Constants.Strategy2;
                    }
                    if (sSecondGrid.Equals(Constants.GridBestLater))
                    {
                        gSecondGrid = GridBestLater;
                        cSecondChart = ChartBestLater;
                        lSecondLabel.Text = Constants.Strategy2;
                    }
                    if (sSecondGrid.Equals(Constants.GridMax))
                    {
                        gSecondGrid = GridMax;
                        cSecondChart = ChartMax;
                        lSecondLabel.Text = Constants.Strategy2;
                    }
                    if (sThirdGrid.Equals(Constants.GridEarly))
                    {
                        gThirdGrid = GridEarly;
                        cThirdChart = ChartEarly;
                        lThirdLabel.Text = Constants.Strategy3;
                    }
                    if (sThirdGrid.Equals(Constants.GridBest))
                    {
                        gThirdGrid = GridBest;
                        cThirdChart = ChartBest;
                        lThirdLabel.Text = Constants.Strategy3;
                    }
                    if (sThirdGrid.Equals(Constants.GridSuspendLater))
                    {
                        gThirdGrid = GridSuspendLater;
                        cThirdChart = ChartSuspendLater;
                        lThirdLabel.Text = Constants.Strategy3;
                    }
                    if (sThirdGrid.Equals(Constants.GridBestLater))
                    {
                        gThirdGrid = GridBestLater;
                        cThirdChart = ChartBestLater;
                        lThirdLabel.Text = Constants.Strategy3;
                    }
                    if (sThirdGrid.Equals(Constants.GridMax))
                    {
                        gThirdGrid = GridMax;
                        cThirdChart = ChartMax;
                        lThirdLabel.Text = Constants.Strategy3;
                    }

                }
                //Add Strategy Details Table in PDF 
                if (list.Count == 1)
                {
                    SiteNew.CreateStrategyDetailsTableForPDF(ref pdfDoc,
                                                                Constants.PaidtoWaitTextForPDF1,
                                                                Calc.listPaidToWaitAmount,
                                                                Constants.AnnualIncomeStratCompleteCombined1,
                                                                Calc.listAnnualAmount,
                                                                list.Count,
                                                                Constants.Married);
                }
                else
                {
                    SiteNew.CreateStrategyDetailsTableForPDF(ref pdfDoc,
                                                                Constants.PaidtoWaitTextForPDF,
                                                                Calc.listPaidToWaitAmount,
                                                                Constants.AnnualIncomeStratCompleteCombined,
                                                                Calc.listAnnualAmount,
                                                                list.Count,
                                                                Constants.Married);
                }

                //Add explanation of strategy in report 
                AddInstructionBeforeSeeStrategy(ref pdfDoc);
                AddExplainationToPDF(ref pdfDoc);
                pdfDoc.NewPage();

                if (list.Count == 3)
                {
                    commonFunctions(gFirstGrid, cFirstChart, lFirstLable, sFirstGrid, custFirstName, ref pdfDoc, Constants.one);
                    commonFunctions(gSecondGrid, cSecondChart, lSecondLabel, sSecondGrid, custFirstName, ref pdfDoc, Constants.two);
                    commonFunctions(gThirdGrid, cThirdChart, lThirdLabel, sThirdGrid, custFirstName, ref pdfDoc, Constants.three);
                }
                else if (list.Count == 2)
                {
                    commonFunctions(gFirstGrid, cFirstChart, lFirstLable, sFirstGrid, custFirstName, ref pdfDoc, Constants.one);
                    commonFunctions(gSecondGrid, cSecondChart, lSecondLabel, sSecondGrid, custFirstName, ref pdfDoc, Constants.two);
                }
                else
                {
                    commonFunctions(gFirstGrid, cFirstChart, lFirstLable, sFirstGrid, custFirstName, ref pdfDoc, Constants.one);
                }

                #endregion Code to find best plan and build the PDF content

                //Add discalimer at the end of report
                SiteNew.AddDisclaimerIntoPDFReport(ref pdfDoc);

                if (Globals.Instance.BoolEmailReport)
                {
                    strEmail = dtCustomerDetails.Rows[0][Constants.UserEmail].ToString();
                    if (!String.IsNullOrEmpty(Globals.Instance.strAdvisorEmail))
                    {
                        if (customer.MailReport(pdfDoc, Globals.Instance.strAdvisorEmail, objMemory, writer, customerFirstName))
                            Globals.Instance.BoolIsMailSent = true;
                        Globals.Instance.strAdvisorEmail = string.Empty;
                    }
                    else
                    {
                        if (customer.MailReport(pdfDoc, strEmail, objMemory, writer, customerFirstName))
                            Globals.Instance.BoolIsMailSent = true;
                    }
                }
                else
                {
                    #region code to close and set type of PDF
                    pdfDoc.Close();
                    Customers objCustomer = new Customers();
                    PdfFilesTO objPdfFile = new PdfFilesTO();
                    /* Assign data into pdf file variable */
                    objPdfFile.PdfContent = objMemory.GetBuffer();
                    objPdfFile.PdfName = Constants.pdfFileName + dtCustomerDetails.Rows[0][Constants.CustomerLastName].ToString() + "_" + DateTime.Now.ToString(Constants.DateFormat) + Constants.pdfFileExtension;
                    objPdfFile.CustomerID = Session[Constants.SessionUserId].ToString();
                    objPdfFile.ContentType = Constants.pdfContentType;
                    objCustomer.insertPdfDetails(objPdfFile);
                    //if (Session["AdvisorCustomer"] != null)
                    //{
                    //    ClsReportDataProperties.objMemoryStream = objMemory;
                    //    ClsReportDataProperties.pdfDocument = pdfDoc;
                    //    ClsReportDataProperties.strUserName = dtCustomerDetails.Rows[0][Constants.CustomerLastName].ToString();
                    //    Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('ViewReport.aspx','_newtab');", true);
                    //}
                    //else
                    //{
                    Response.OutputStream.Write(objMemory.GetBuffer(), 0, objMemory.GetBuffer().Length);
                    Response.ContentType = Constants.pdfContentType;
                    Response.AddHeader(Constants.pdfContentDisposition, Constants.pdfFileName + dtCustomerDetails.Rows[0][Constants.CustomerLastName].ToString() + "_" + DateTime.Now.ToString(Constants.DateFormat) + Constants.pdfFileExtension);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                    Response.End();
                    objMemory.Flush();
                    //}
                    #endregion code to close and set type of PDF
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.DownloadError;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to build the common functions of the PDF
        /// </summary>
        /// <param name="gFirstGrid"></param>
        /// <param name="cFirstChart"></param>
        /// <param name="lFirstLable"></param>
        /// <param name="sGridName"></param>
        /// <param name="custFirstName"></param>
        /// <param name="pdfDoc"></param>
        /// <param name="number"></param>
        public void commonFunctions(GridView gFirstGrid, Chart cFirstChart, Label lFirstLable, string sGridName, string custFirstName, ref Document pdfDoc, string number)
        {
            try
            {
                Customers customer = new Customers();
                string WifeFra = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                string HusbFra = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                if (!number.Equals(Constants.one))
                {
                    pdfDoc.NewPage();
                }
                #region explanation steps after the grid in PDF
                // to show the explanation steps after the grid in PDF
                //if (sGridName.Equals(Constants.GridEarly))
                //{
                //    SiteNew.strWifeExplanation = lblNoteWife0.Text;
                //    SiteNew.strHusbandExplanation = lblNoteHusband0.Text;
                //}
                //else if (sGridName.Equals(Constants.GridBest))
                //{
                //    SiteNew.strHusbandExplanation = lblNoteHusband.Text;
                //    SiteNew.strWifeExplanation = lblNoteWife.Text;
                //}
                //else if (sGridName.Equals(Constants.GridBestLater))
                //{
                //    SiteNew.strWifeExplanation = lblNoteWife1.Text;
                //    SiteNew.strHusbandExplanation = lblNoteHusband1.Text;
                //}
                //else
                //{
                //    SiteNew.strWifeExplanation = lblNoteWife2.Text;
                //    SiteNew.strHusbandExplanation = lblNoteHusband2.Text;
                //}
                #endregion explanation steps after the grid in PDF
                SiteNew.renderGridViewIntoStackedChart(gFirstGrid, cFirstChart, lFirstLable, Constants.Married, true);
                SiteNew.commonPageLayout(lFirstLable, gFirstGrid, ref pdfDoc, custFirstName, Constants.Married, sGridName, WifeFra, HusbFra);
                pdfDoc.NewPage();
                //pdfDoc.Add(SiteNew.generateChartHeading(lFirstLable.Text));
                SiteNew.convertChartToImage(cFirstChart, ref pdfDoc);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.ToString());
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to build the sorting condition for best plan
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int Compare2(KeyValuePair<string, int> a, KeyValuePair<string, int> b)
        {
            try
            {
                return a.Value.CompareTo(b.Value);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                return 0;
            }
        }

        /// <summary>
        /// Build the page - normally once but repeat for testing
        /// </summary>
        protected void BuildPage()
        {
            try
            {
                ResetValues();
                #region get the age in years from DOB to 01/01/2016
                //loop set to get the changes based on date for new change policy  according to Brian
                //get the age in years from DOB to 01/01/2016
                DateTime HusbDOB = DateTime.Parse(BirthHusbandYYYY.Text.ToString());
                DateTime WifeDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                DateTime limit = DateTime.Parse("05/01/1950");
                DateTime limit1 = DateTime.Parse("01/01/1954");
                DateTime StrategyUsageLimit = DateTime.Parse("05/01/2016");
                //TimeSpan ageYearsh = limit.Subtract(HusbDOB);
                //DateTime Age = DateTime.MinValue + ageYearsh;
                //int Years = Age.Year - 1;
                #endregion get the age in years from DOB to 01/01/2016

                #region code to set basic variable for the function
                // Set calculation properties
                Calc Calc = new Calc()
                {
                    intAgeWife = intAgeWife,
                    intAgeHusb = intAgeHusb,
                    decBeneWife = decBeneWife,
                    decBeneHusb = decBeneHusb,
                    birthYearWife = birthYearWife,
                    birthYearHusb = birthYearHusb,
                    intAgeWifeMonth = intAgeWifeMonth,
                    intAgeHusbMonth = intAgeHusbMonth,
                    husbBirthDate = HusbDOB,
                    intAgeHusbDays = intAgeHusbDays,
                    intAgeWifeDays = intAgeWifeDays,
                    intCurrentAgeHusbMonth = intCurrentAgeHusbMonth,
                    intCurrentAgeWifeMonth = intCurrentAgeWifeMonth,
                    intCurrentAgeWifeYear = intCurrentAgeWifeYear,
                    intCurrentAgeHusbYear = intCurrentAgeHusbYear
                };
                Globals.Instance.WifeDateOfBirth = Convert.ToDateTime(BirthWifeYYYY.Text);
                Globals.Instance.HusbDateOfBirth = Convert.ToDateTime(BirthHusbandYYYY.Text);
                halfBenHusb = decBeneHusb / 2;
                halfbenWife = decBeneWife / 2;
                Session["HusbandBenefit"] = BeneHusband.Text;
                Session["WifeBenefit"] = BeneWife.Text;
                Calc.husbBirthDate = Convert.ToDateTime(BirthHusbandYYYY.Text);
                Calc.wifeBirthDate = Convert.ToDateTime(BirthWifeYYYY.Text);
                Calc.Your_Name = Your_Name.Text;
                Calc.Spouse_Name = Spouse_Name.Text;
                //if (CheckBoxCola.Checked)
                //    Calc.colaAmt = 1.025m;
                //else
                //    Calc.colaAmt = 1;
                Calc.colaAmt = 1.025m;
                colaAmt = Calc.colaAmt;
                if (CheckBoxSurv.Checked)
                    Calc.showSurvivor = true;
                else
                    Calc.showSurvivor = false;
                //ParmsText3.Text = Calc.roundNote;
                //if (ParmsText3.Text == String.Empty)
                //    PanelAdj3.Visible = false;
                //else
                //    PanelAdj3.Visible = true;
                // Reset changes from prior calculations
                if (LabelLater.Text.EndsWith(Constants.NOT_REC)) LabelLater.Text = LabelLater.Text.Remove(LabelLater.Text.Length - Constants.NOT_REC.Length);
                if (LabelNowLater.Text.EndsWith(Constants.NOT_REC)) LabelNowLater.Text = LabelNowLater.Text.Remove(LabelNowLater.Text.Length - Constants.NOT_REC.Length);
                if (LabelSuspend.Text.EndsWith(Constants.NOT_REC)) LabelSuspend.Text = LabelSuspend.Text.Remove(LabelSuspend.Text.Length - Constants.NOT_REC.Length);
                if (LabelSuspendLater.Text.EndsWith(Constants.NOT_REC)) LabelSuspendLater.Text = LabelSuspend.Text.Remove(LabelSuspendLater.Text.Length - Constants.NOT_REC.Length);
                PanelLater.Visible = true;
                PanelNowLater.Visible = true;
                PanelSuspend.Visible = true;
                PanelSuspendLater.Visible = true;
                PanelBestLater.Visible = true;
                ExplainBestLater.Visible = true;
                ExplainNowLater.Visible = true;
                ExplainLater.Visible = true;
                ExplainSuspend.Visible = true;
                pnlText3.Visible = true;
                Summ summSuspend;
                // Build all of the alternative grids
                bool bBadCandS = false;
                Customers customer = new Customers();
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor) || Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                    dtDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                else
                    dtDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                strUserFirstName = dtDetails.Rows[0][Constants.CustomerFirstName].ToString();
                strUserSpouseFirstName = dtDetails.Rows[0][Constants.CustomerSpouseFirstName].ToString();
                Calc.Your_Name = strUserSpouseFirstName;
                Calc.Spouse_Name = strUserFirstName;
                CommonVariables.strUserName = strUserFirstName;

                #endregion code to set basic variable for the function

                #region code to build all alternate grids and charts
                var list = new List<KeyValuePair<string, int>>();
                if (decBeneHusb == 0 || decBeneWife == 0)
                {
                    decimal decCombineIncomeFirstStrat = 0, decCombineIncomeSecondStrat = 0;
                    int intWifeFirstPayAge = 0, intHusbFirstPayAge = 0;
                    //First Strategy
                    Calc.BuildGrid(GridZero, Calc.calc_strategy.ZeroBenefit, Calc.calc_start.fra6667, ref NoteNowLater);

                    Calc.Highlight(GridZero, Calculate.Calc.calc_solo.Married, "GridBest");
                    //cummulative value at age 69
                    Session["ZeroCumm"] = Calc.cumm69;
                    //starting value for husb
                    Session[Constants.startingBenefitsValueHusb3] = Calc.startingBenefitsValueHusb;
                    //starting value for wife
                    Session[Constants.startingBenefitsValueWife3] = Calc.startingBenefitsValueWife;
                    //value at age 69 for higher PIA person
                    Session[Constants.valueAtAge693] = Calc.valueAtAge69;
                    //value at age 70 for higher PIA person
                    Session[Constants.valueAtAge703] = Calc.valueAtAge70;
                    decCombineIncomeFirstStrat = Calc.valueAtAge70;
                    //code to add the steps and keys to the grid
                    renderKeysIntoGridZero(Calc.cumm69, Calc.valueAtAge70, Calc.lastBenefitsChangedAge70Value, Calc.startingAgeValueHusb, Calc.startingAgeValueWife, Calc.startingBenefitsValueHusb, Calc.startingBenefitsValueWife, Calc.AgeValueForHigherEarner70, Calc.ValueForHigherEarner70, Calc.ClaimAndSuspendinFullAge, Calc.ClaimAndSuspendinFullValue, Calc.CombinedIncomeAge, Calc.AgeForHusb70, Calc.ValueForHusb70, Calc.AgeForWife70, Calc.ValueForWife70, Calc.AgeValueForSpousalAt70, Calc.ValueForWifeAt70);
                    Calc.cumm69 = 0;
                    ResetAnualIncomeVariables();
                    list.Add(new KeyValuePair<string, int>(Constants.ZeroStrategy, int.Parse(Session["ZeroCumm"].ToString())));
                    lblCombinedIncome1.Text = Calc.valueAtAge70.ToString(Constants.AMT_FORMAT);

                    //Second Strategy
                    int cummLater = Calc.BuildGridSuspend(GridBestLater, Calc.calc_strategy.Spousal, Calc.calc_start.max70, ref NoteLater, "Full");
                    // summLater.description = "Claim Later";//4 on Slidder
                    //highlighting the values in the grid
                    Calc.Highlight(GridBestLater, Calculate.Calc.calc_solo.Married, "Full");
                    //cummulative value at age 69
                    Session[Constants.FullCumm] = Calc.cumm69;
                    //value at age 69 for higher PIA person
                    Session[Constants.valueAtAge69Full] = Calc.valueAtAge69;
                    //value at age 70 for higher PIA person
                    Session[Constants.valueAtAge70Full] = Calc.valueAtAge70;
                    decCombineIncomeSecondStrat = Calc.valueAtAge70;
                    //starting value for husb
                    Session[Constants.startingBenefitsValueHusbFull] = Calc.startingBenefitsValueHusb;
                    //starting value for wife
                    Session[Constants.startingBenefitsValueWifeFull] = Calc.startingBenefitsValueWife;
                    //to show 3 strategies when age sapn is above 10 years
                    Session[Constants.OdlerGreater] = Calc.odlgerGreater;
                    //code to show the keys and steps in the grid
                    renderKeysIntoGridFull(Calc.cumm69, Calc.valueAtAge70, Calc.lastBenefitsChangedAge70Value, Calc.startingAgeValueHusb, Calc.startingAgeValueWife, Calc.startingBenefitsValueHusb, Calc.startingBenefitsValueWife, Calc.AgeValueForHigherEarner70, Calc.ValueForHigherEarner70, Calc.ClaimAndSuspendinFullAge, Calc.ClaimAndSuspendinFullValue, Calc.CombinedIncomeAge, Calc.AgeForHusb70, Calc.ValueForHusb70, Calc.AgeForWife70, Calc.ValueForWife70, Calc.AgeValueForSpousalAt70, Calc.ValueForWifeAt70);
                    Calc.cumm69 = 0;
                    ResetAnualIncomeVariables();
                    lblPaidNumber1.Text = list[0].Value.ToString(Constants.AMT_FORMAT);

                    #region Hide/Show strategy after checking the
                    if (decBeneWife == 0)
                    {
                        intWifeFirstPayAge = Convert.ToInt16(Calc.startingAgeValueWife.Substring(0, 2));
                        if (intWifeFirstPayAge <= Calc.wifeFRA)
                            Globals.Instance.BoolShowSecondZeroStrategy = true;
                        else
                            Globals.Instance.BoolShowSecondZeroStrategy = false;
                    }
                    else
                    {
                        intHusbFirstPayAge = Convert.ToInt16(Calc.startingAgeValueHusb.Substring(0, 2));
                        if (intHusbFirstPayAge <= Calc.husbFRA)
                            Globals.Instance.BoolShowSecondZeroStrategy = true;
                        else
                            Globals.Instance.BoolShowSecondZeroStrategy = false;
                    }
                    if (Globals.Instance.BoolShowSecondZeroStrategy)
                    {
                        list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.FullCumm].ToString())));
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showTwoStrategy('{0}','{1}');", list[0].Key.ToString(), list[1].Key.ToString()), true);
                        lblPaidNumber1.Text = list[0].Value.ToString(Constants.AMT_FORMAT);
                        lblCombinedIncome1.Text = decCombineIncomeFirstStrat.ToString(Constants.AMT_FORMAT);
                        lblPaidNumber3.Text = list[1].Value.ToString(Constants.AMT_FORMAT);
                        lblCombinedIncome3.Text = decCombineIncomeSecondStrat.ToString(Constants.AMT_FORMAT);
                    }
                    else
                    {
                        lblPaidNumber2.Text = list[0].Value.ToString(Constants.AMT_FORMAT);
                        lblCombinedIncome2.Text = decCombineIncomeFirstStrat.ToString(Constants.AMT_FORMAT);
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showOneStrategy('{0}');", list[0].Key.ToString()), true);
                    }

                    #endregion Hide/Show strategy after checking the
                    AddPaidAndAnnualValForPDF(list);
                }
                else
                {

                    int cummNowLater = Calc.BuildGrid(GridBest, Calc.calc_strategy.Spousal, Calc.calc_start.asap62, ref NoteNowLater);
                    //summNowLater.description = "Claim Now, Claim More Later";//2 on Slidder
                    //CheckCasesToHideSecondStrategy();
                    //highlighting the values in the grid
                    Calc.Highlight(GridBest, Calculate.Calc.calc_solo.Married, "GridBest");
                    //cummulative value at age 69
                    Session[Constants.BestCumm] = Calc.cumm69;
                    //starting value for husb
                    Session[Constants.startingBenefitsValueHusb3] = Calc.startingBenefitsValueHusb;
                    //starting value for wife
                    Session[Constants.startingBenefitsValueWife3] = Calc.startingBenefitsValueWife;
                    //value at age 69 for higher PIA person
                    Session[Constants.valueAtAge693] = Calc.valueAtAge69;
                    //value at age 70 for higher PIA person
                    Session[Constants.valueAtAge703] = Calc.valueAtAge70;
                    //code to add the steps and keys to the grid
                    renderKeysIntoGridBest(Calc.cumm69, Calc.valueAtAge70, Calc.lastBenefitsChangedAge70Value, Calc.startingAgeValueHusb, Calc.startingAgeValueWife, Calc.startingBenefitsValueHusb, Calc.startingBenefitsValueWife, Calc.AgeValueForHigherEarner70, Calc.ValueForHigherEarner70, Calc.ClaimAndSuspendinFullAge, Calc.ClaimAndSuspendinFullValue, Calc.CombinedIncomeAge, Calc.AgeForHusb70, Calc.ValueForHusb70, Calc.AgeForWife70, Calc.ValueForWife70, Calc.AgeValueForSpousalAt70, Calc.ValueForWifeAt70);


                    if (Globals.Instance.BoolSpousalAfterWorkHusb)
                    {
                        if (int.Parse(Calc.startingAgeValueHusb.Substring(3, 2)) == Calc.husbFRA)
                            Globals.Instance.BoolHideSecondStrat = true;
                    }
                    else if (Globals.Instance.BoolSpousalAfterWorkWife)
                    {
                        if (int.Parse(Calc.startingAgeValueWife.Substring(0, 2)) == Calc.wifeFRA)
                            Globals.Instance.BoolHideSecondStrat = true;
                    }


                    Calc.cumm69 = 0;
                    ResetAnualIncomeVariables();

                    int maxStrategy = Calc.BuildGridSuspend(GridMax, Calc.calc_strategy.Max, Calc.calc_start.max, ref NoteNowLater, "Max");
                    //summNowLater.description = "Claim Now, Claim More Later";//2 on Slidder
                    //highlighting the values in the grid
                    Calc.Highlight(GridMax, Calculate.Calc.calc_solo.Married, "Max");
                    Session[Constants.valueAtAge70Max] = Calc.valueAtAge70;
                    //cummulative value at age 69
                    Session[Constants.MaxxCumm] = Calc.cumm69;
                    renderKeysIntoGridMax(Calc.cumm69, Calc.valueAtAge70, Calc.lastBenefitsChangedAge70Value, Calc.startingAgeValueHusb, Calc.startingAgeValueWife, Calc.startingBenefitsValueHusb, Calc.startingBenefitsValueWife, Calc.AgeValueForHigherEarner70, Calc.ValueForHigherEarner70, Calc.ValueForWife70, Calc.ValueForHusb70, Calc.AgeForWife70, Calc.AgeForHusb70, Calc.CombinedIncomeAge);
                    Calc.cumm69 = 0;
                    ResetAnualIncomeVariables();

                    int cummLater = Calc.BuildGridSuspend(GridBestLater, Calc.calc_strategy.Spousal, Calc.calc_start.max70, ref NoteLater, "Full");
                    // summLater.description = "Claim Later";//4 on Slidder
                    //highlighting the values in the grid
                    Calc.Highlight(GridBestLater, Calculate.Calc.calc_solo.Married, "Full");
                    //cummulative value at age 69
                    Session[Constants.FullCumm] = Calc.cumm69;
                    //value at age 69 for higher PIA person
                    Session[Constants.valueAtAge69Full] = Calc.valueAtAge69;
                    //value at age 70 for higher PIA person
                    Session[Constants.valueAtAge70Full] = Calc.valueAtAge70;
                    //starting value for husb
                    Session[Constants.startingBenefitsValueHusbFull] = Calc.startingBenefitsValueHusb;
                    //starting value for wife
                    Session[Constants.startingBenefitsValueWifeFull] = Calc.startingBenefitsValueWife;
                    //to show 3 strategies when age sapn is above 10 years
                    Session[Constants.OdlerGreater] = Calc.odlgerGreater;
                    lblCombinedIncome2.Text = Calc.valueAtAge70.ToString(Constants.AMT_FORMAT);
                    //code to show the keys and steps in the grid
                    renderKeysIntoGridFull(Calc.cumm69, Calc.valueAtAge70, Calc.lastBenefitsChangedAge70Value, Calc.startingAgeValueHusb, Calc.startingAgeValueWife, Calc.startingBenefitsValueHusb, Calc.startingBenefitsValueWife, Calc.AgeValueForHigherEarner70, Calc.ValueForHigherEarner70, Calc.ClaimAndSuspendinFullAge, Calc.ClaimAndSuspendinFullValue, Calc.CombinedIncomeAge, Calc.AgeForHusb70, Calc.ValueForHusb70, Calc.AgeForWife70, Calc.ValueForWife70, Calc.AgeValueForSpousalAt70, Calc.ValueForWifeAt70);
                    Calc.cumm69 = 0;
                    ResetAnualIncomeVariables();

                    int cummSuspend = Calc.BuildGrid(GridEarly, Calc.calc_strategy.Suspend, Calc.calc_start.asap62, ref NoteSuspend);
                    //summSuspend.description = "Early Claim and Suspend";
                    //highlighting the values in the grid
                    Calc.Highlight(GridEarly, Calculate.Calc.calc_solo.Married, "GridEarly");
                    //cummulative value at age 69
                    Session[Constants.EarlySuspendCumm] = Calc.cumm69;
                    //starting value for husb
                    Session[Constants.startingBenefitsValueHusb4] = Calc.startingBenefitsValueHusb;
                    //starting value for wife
                    Session[Constants.startingBenefitsValueWife4] = Calc.startingBenefitsValueWife;
                    //value at age 69 for higher PIA person
                    Session[Constants.valueAtAge694] = Calc.valueAtAge69;
                    //value at age 70 for higher PIA person
                    Session[Constants.valueAtAge704] = Calc.valueAtAge70;
                    //code to add the steps and keys to the grid
                    renderKeysIntoGridEarly(Calc.cumm69, Calc.valueAtAge70, Calc.lastBenefitsChangedAge70Value, Calc.startingAgeValueHusb, Calc.startingAgeValueWife, Calc.startingBenefitsValueHusb, Calc.startingBenefitsValueWife, Calc.AgeValueForHigherEarner70, Calc.ValueForHigherEarner70, Calc.ClaimAndSuspendinFullAge, Calc.ClaimAndSuspendinFullValue, Calc.CombinedIncomeAge, Calc.AgeForHusb70, Calc.ValueForHusb70, Calc.AgeForWife70, Calc.ValueForWife70, Calc.AgeValueForSpousalAt70, Calc.ValueForWifeAt70);
                    Calc.cumm69 = 0;
                    ResetAnualIncomeVariables();

                    // double hybrid = Calc.HybridBenefit();
                    //if (Calc.badStrat) bBadCandS = true;
                    //int cummSuspendLater = Calc.BuildGridSuspend(GridSuspendLater, Calc.calc_strategy.Suspend, Calc.calc_start.fra6667, ref NoteSuspendLater, "ClaimandSuspend");
                    //summSuspendLater = new Summ();
                    ////highlighting the values in the grid
                    //Calc.Highlight(GridSuspendLater, Calculate.Calc.calc_solo.Married, Calc.ageNumber);
                    ////summSuspendLater.description = "Claim and Suspend";//3 on Slidder  .BuildGridSuspend
                    ////cummulative value at age 69
                    //Session[Constants.SuspendCumm] = Calc.cumm69;
                    ////value at age 69 for higher PIA person
                    //Session[Constants.valueAtAge69Suspend] = Calc.valueAtAge69;
                    ////value at age 70 for higher PIA person
                    //Session[Constants.valueAtAge70Suspend] = Calc.valueAtAge70;
                    //Session[Constants.OdlerGreater] = Calc.odlgerGreater;
                    ////show 3 strategies when higher earner age 70 value is greater
                    ////Session["higherEarnerValueAtAge70Suspend"] = Calc.higherEarnerValueAtAge70;
                    ////label set to show thw combined annual income
                    ////lblCombinedIncome3.Text = Calc.valueAtAge70.ToString(Constants.AMT_FORMAT);
                    ////code to add the steps and keys to the grid
                    //renderKeysIntoGridSuspend(Calc.cumm69, Calc.valueAtAge70, Calc.lastBenefitsChangedAge70Value, Calc.startingAgeValueHusb, Calc.startingAgeValueWife, Calc.startingBenefitsValueHusb, Calc.startingBenefitsValueWife, Calc.AgeValueForHigherEarner70, Calc.ValueForHigherEarner70, Calc.AgeValueForWifeAt70, Calc.BenefitsValueForWifeAt70, Calc.AgeWhenHigherEarnerClaimandSuspend, Calc.CombinedIncomeAge);

                    #endregion code to build all alternate grids and charts

                    #region Code to show Early Claim and Suspend or Claim Early an Cliam Late
                    double LPIA, HPIA = 0;
                    //code to higher PIA and lower PIA values
                    if (decBeneHusb > decBeneWife)
                    {
                        HPIA = double.Parse(BeneHusband.Text.ToString());
                        LPIA = double.Parse(BeneWife.Text.ToString());
                    }
                    else
                    {
                        LPIA = double.Parse(BeneHusband.Text.ToString());
                        HPIA = double.Parse(BeneWife.Text.ToString());
                    }
                    //loop to check if lower PIA is less tha 40% of Higher PIA
                    if (LPIA < (0.4 * HPIA))
                    {
                        Session[Constants.BestDisplay] = Constants.None;
                    }
                    else
                    {
                        Session[Constants.EarlySuspendDisplay] = Constants.None;
                        Session[Constants.BestDisplay] = Constants.Show;
                    }
                    //Added on 08/08/2016
                    if (Session[Constants.valueAtAge704].ToString().Equals(Session[Constants.valueAtAge703].ToString()))
                    {
                        if (Convert.ToInt32(Session[Constants.valueAtAge694]) > Convert.ToInt32(Session[Constants.valueAtAge693]))
                            Session[Constants.BestDisplay] = Constants.None;
                        else
                            Session[Constants.BestDisplay] = Constants.Show;
                    }

                    #endregion Code to show Early Claim and Suspend or Claim Early an Cliam Late

                    #region Code select Best Plan

                    if (HusbDOB < limit)
                    {
                        if (Session[Constants.BestDisplay].Equals(Constants.None))
                            list.Add(new KeyValuePair<string, int>(Constants.EarlyStrategy, int.Parse(Session[Constants.EarlySuspendCumm].ToString())));
                        else
                            list.Add(new KeyValuePair<string, int>(Constants.BestSrategy, int.Parse(Session[Constants.BestCumm].ToString())));
                        lblPaidNumber1.Text = list[0].Value.ToString(Constants.AMT_FORMAT);
                        //label set to show thw combined annual income
                        //lblCombinedIncome1.Text = decimal.Parse(Session[Constants.valueAtAge703].ToString()).ToString(Constants.AMT_FORMAT);
                    }
                    else
                    {
                        list.Add(new KeyValuePair<string, int>(Constants.BestSrategy, int.Parse(Session[Constants.BestCumm].ToString())));
                        lblPaidNumber1.Text = list[0].Value.ToString(Constants.AMT_FORMAT);
                        //label set to show thw combined annual income
                        //lblCombinedIncome1.Text = decimal.Parse(Session[Constants.valueAtAge704].ToString()).ToString(Constants.AMT_FORMAT);
                    }
                    list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.FullCumm].ToString())));
                    if (Convert.ToDecimal(Session[Constants.valueAtAge70Full].ToString()) != Convert.ToDecimal(Session[Constants.valueAtAge70Max].ToString()) && Globals.Instance.BoolShowMaxStrategy)
                    {
                        list.Add(new KeyValuePair<string, int>(Constants.MaxStrategy, int.Parse(Session[Constants.MaxxCumm].ToString())));
                    }

                    if ((Convert.ToDecimal(Session[Constants.valueAtAge70Full].ToString()) == Convert.ToDecimal(Session[Constants.valueAtAge703].ToString())) ||
                        (Convert.ToDecimal(Session[Constants.valueAtAge70Full].ToString()) == Convert.ToDecimal(Session[Constants.valueAtAge704].ToString())))
                    {
                        if ((Convert.ToDecimal(Session[Constants.valueAtAge693].ToString()) >= Convert.ToDecimal(Session[Constants.valueAtAge69Full].ToString())) ||
                        (Convert.ToDecimal(Session[Constants.valueAtAge694].ToString()) >= Convert.ToDecimal(Session[Constants.valueAtAge69Full].ToString())))
                        {
                            if (list.Count == 2)
                                list.Remove(list[1]);
                        }
                        else if ((Convert.ToDecimal(Session[Constants.valueAtAge693].ToString()) <= Convert.ToDecimal(Session[Constants.valueAtAge69Full].ToString())) ||
                         (Convert.ToDecimal(Session[Constants.valueAtAge694].ToString()) <= Convert.ToDecimal(Session[Constants.valueAtAge69Full].ToString())))
                        {
                            if (LPIA < (0.4 * HPIA))
                                list.Remove(list[0]);
                        }
                    }
                    else if (Calc.wifeFRA <= intAgeWife && Calc.husbFRA <= intAgeHusb)
                    {
                        if (list.Count == 2)
                            list.Remove(list[1]);

                    }
                    if (Globals.Instance.BoolHideSecondStrat)
                    {
                        if (int.Parse(Session[Constants.BestCumm].ToString()) >= int.Parse(Session[Constants.FullCumm].ToString()))
                            if (list.Count >= 2)
                                list.Remove(list[1]);
                    }
                    Session["intAgeWife"] = intAgeWife;
                    Session["intAgeHusb"] = intAgeHusb;
                    if (list.Count == 3)
                    {
                        //list.Sort(Compare2);
                        Session[Constants.MaxCumm] = list[2].Value.ToString();
                        //list.Reverse();
                        lblPaidNumber2.Text = list[1].Value.ToString(Constants.AMT_FORMAT);
                        lblPaidNumber3.Text = list[2].Value.ToString(Constants.AMT_FORMAT);
                        CombineIncomeLabelForThreeStrategies(list);
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showThreeStrategy('{0}','{1}','{2}');", list[0].Key.ToString(), list[1].Key.ToString(), list[2].Key.ToString()), true);
                    }
                    else if (list.Count == 2)
                    {
                        //list.Sort(Compare2);
                        Session[Constants.MaxCumm] = list[1].Value.ToString();
                        //list.Reverse();
                        CombineIncomeLableForTwoStrategies(list);
                        lblPaidNumber3.Text = list[1].Value.ToString(Constants.AMT_FORMAT);
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showTwoStrategy('{0}','{1}');", list[0].Key.ToString(), list[1].Key.ToString()), true);
                    }
                    else
                    {
                        //list.Sort(Compare2);
                        Session[Constants.MaxCumm] = list[0].Value.ToString();
                        //list.Reverse();
                        lblPaidNumber2.Text = list[0].Value.ToString(Constants.AMT_FORMAT);
                        CombineIncomeLabelForOneStrategy(list);
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showOneStrategy('{0}');", list[0].Key.ToString()), true);
                    }

                    //Add Paid to wait and combined annual income values
                    AddPaidAndAnnualValForPDF(list);
                    #endregion Code select Best Plan
                }
                #region code to set the popdup message when HERE link is clicked under the Grids
                LabelBest.Text = LabelNowLater.Text;
                LabelBestLater.Text = LabelLater.Text;
                //StrategyBest.Text = StrategyNowLater.Text;
                //StrategyBestLater.Text = StrategyLater.Text;
                PanelNowLater.Visible = false;
                PanelLater.Visible = false;
                ExplainNowLater.Visible = false;
                ExplainLater.Visible = false;
                if (bBadCandS)
                {
                    // When Claim and suspend is bad, hide it
                    PanelSuspend.Visible = false;
                    ExplainSuspend.Visible = false;
                    summSuspend.description = string.Empty;
                }
                //Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, "HideEmptyTextLabel();",true);


                #endregion code to set the popdup message when HERE link is clicked under the Grids
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }


        //private void CheckCasesToHideSecondStrategy()
        //{

        //    try
        //    {
        //        if (Globals.Instance.WifeStartingBenefitName == Constants.Working && Globals.Instance.BoolSpousalAfterWorkWife && Globals.Instance.BoolSpousalAtFRAAgeCliamedHusb && Globals.Instance.BoolWHBHusbat70)
        //        {
        //            Globals.Instance.BoolHideSecondStrat = true;
        //        }
        //        else if (Globals.Instance.HusbStartingBenefitName == Constants.Working && Globals.Instance.BoolSpousalAfterWorkHusb && Globals.Instance.BoolSpousalAtFRAAgeCliamedWife && Globals.Instance.BoolWHBWifeat70)
        //        {
        //            Globals.Instance.BoolHideSecondStrat = true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        throw;
        //    }
        //}


        /// <summary>
        /// to reset the annual Income variables after each startegy
        /// </summary>
        public void ResetAnualIncomeVariables()
        {
            try
            {
                Globals.Instance.BoolWHBSingleSwitchedWife = false;
                Globals.Instance.BoolHusbandAgeAdjusted = false;
                Globals.Instance.BoolWifeAgeAdjusted = false;
                Globals.Instance.HusbandNumberofMonthsinNote = 0;
                Globals.Instance.WifeNumberofMonthinNote = 0;
                Globals.Instance.WifeNumberofMonthsinSteps = 0;
                Globals.Instance.HusbandNumberofMonthsinSteps = 0;
                Globals.Instance.HusbNumberofMonthsAboveGrid = 0;
                Globals.Instance.WifeNumberofMonthsAboveGrid = 0;
                Globals.Instance.HusbandNumberofYears = 0;
                Globals.Instance.WifeNumberofYears = 0;
                Globals.Instance.BoolWHBSwitchedHusb = false;
                Globals.Instance.BoolWHBSwitchedWife = false;
                Globals.Instance.HusbNumberofMonthsBelowGrid = 0;
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                Globals.Instance.WifeOriginalAnnualIncome = 0;
                Globals.Instance.HusbandOriginalAnnualIncome = 0;
                Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior = 0;
                Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = 0;
                Globals.Instance.BoolSpousalAfterWorkWife = false;
                Globals.Instance.BoolSpousalAfterWorkHusb = false;
                Globals.Instance.BoolWHBSingleSwitchedHusb = false;
                Globals.Instance.HusbNumberofYearsAbove66 = 0;
                Globals.Instance.WifeNumberofYearsAbove66 = 0;
                Globals.Instance.BoolSpousalAtFRAAgeCliamedWife = false;
                Globals.Instance.BoolSpousalWHBat70 = false;
                Globals.Instance.HusbSpousalBenMonthAt69 = 0;
                Globals.Instance.HusbAnnualIcomeAt69 = 0;
                Globals.Instance.HusbAnnualIcomeAt70 = 0;
                Globals.Instance.HusbSpousalBenefitWhenWife66 = 0;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// to reset the all common variables after each startegy
        /// </summary>
        public void ResetCommonVariables()
        {
            try
            {
                CommonVariables.strKey1 = string.Empty;
                CommonVariables.strKey2 = string.Empty;
                CommonVariables.strKey3 = string.Empty;
                CommonVariables.strStep1 = string.Empty;
                CommonVariables.strStep2 = string.Empty;
                CommonVariables.strStep3 = string.Empty;
                CommonVariables.strStep4 = string.Empty;
                CommonVariables.strNoteHusband0 = string.Empty;
                CommonVariables.strNoteWife0 = string.Empty;
                CommonVariables.strNoteHusband = string.Empty;
                CommonVariables.strNoteWife = string.Empty;
                CommonVariables.strHusbExplain1 = string.Empty;
                CommonVariables.strWifeExplain1 = string.Empty;
                CommonVariables.strKey4 = string.Empty;
                CommonVariables.strKey5 = string.Empty;
                CommonVariables.strKey6 = string.Empty;
                CommonVariables.strStep5 = string.Empty;
                CommonVariables.strStep6 = string.Empty;
                CommonVariables.strStep7 = string.Empty;
                CommonVariables.strStep8 = string.Empty;
                CommonVariables.strNoteHusband6 = string.Empty;
                CommonVariables.strNoteWife6 = string.Empty;
                CommonVariables.strNoteHusband7 = string.Empty;
                CommonVariables.strNoteWife7 = string.Empty; ;
                CommonVariables.strHusbExplain4 = string.Empty;
                CommonVariables.strWifeExplain4 = string.Empty;
                CommonVariables.strKey7 = string.Empty;
                CommonVariables.strKey8 = string.Empty;
                CommonVariables.strKey9 = string.Empty;
                CommonVariables.strStep9 = string.Empty;
                CommonVariables.strStep10 = string.Empty;
                CommonVariables.strStep11 = string.Empty;
                CommonVariables.strStep12 = string.Empty;
                CommonVariables.strStep13 = string.Empty;
                CommonVariables.strNoteHusband5 = string.Empty;
                CommonVariables.strNoteWife5 = string.Empty;
                CommonVariables.strNoteHusband4 = string.Empty;
                CommonVariables.strNoteWife4 = string.Empty;
                CommonVariables.strHusbExplain3 = string.Empty;
                CommonVariables.strWifeExplain3 = string.Empty;
                CommonVariables.strKey10 = string.Empty;
                CommonVariables.strKey11 = string.Empty;
                CommonVariables.strKey12 = string.Empty;
                CommonVariables.strStep14 = string.Empty;
                CommonVariables.strStep15 = string.Empty;
                CommonVariables.strStep16 = string.Empty;
                CommonVariables.strStep17 = string.Empty;
                CommonVariables.strStep18 = string.Empty;
                CommonVariables.strNoteHusband1 = string.Empty;
                CommonVariables.strNoteWife1 = string.Empty;
                CommonVariables.strNoteHusband3 = string.Empty;
                CommonVariables.strNoteWife3 = string.Empty;
                CommonVariables.strHusbExplain2 = string.Empty;
                CommonVariables.strWifeExplain2 = string.Empty;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCommonVariable, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCommonVariable + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// redirect advisor back to the managecustomer page
        /// </summary>
        /// <param name="strGridName"></param>
        protected void Backto_Managecustomer(object sender, EventArgs e)
        {
            try
            {
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor))
                    Response.Redirect(Constants.RedirectManageCustomers, true);
                else if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                    Response.Redirect(Constants.RedirectManageCustomersWithAdvisorID + Session[Constants.TempAdvisorID].ToString(), false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCommonVariable + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Used to add Paid to wait amount and annual income amount in lists
        /// </summary>
        /// <param name="listPaidToWait">list paid to wait amount</param>
        public void AddPaidAndAnnualValForPDF(List<KeyValuePair<string, int>> listPaidToWait)
        {
            try
            {
                //Add Paid To Wait Amount
                Calc.listPaidToWaitAmount.Clear();
                for (int index = 0; index < listPaidToWait.Count; index++)
                {
                    if (!string.IsNullOrEmpty(listPaidToWait[index].Value.ToString()))
                        Calc.listPaidToWaitAmount.Add(listPaidToWait[index].Value.ToString(Constants.AMT_FORMAT));
                }

                //Add Combined Amount
                Calc.listAnnualAmount.Clear();
                if (!string.IsNullOrEmpty(lblCombinedIncome1.Text))
                    Calc.listAnnualAmount.Add(lblCombinedIncome1.Text);
                if (!string.IsNullOrEmpty(lblCombinedIncome2.Text))
                    Calc.listAnnualAmount.Add(lblCombinedIncome2.Text);
                if (!string.IsNullOrEmpty(lblCombinedIncome3.Text))
                    Calc.listAnnualAmount.Add(lblCombinedIncome3.Text);

                //Add Restricted Application Amount
                Calc.listRestrictAppIcome.Clear();
                Calc.listRestrictAppIcome.Add(lblRestrictAppIncomeValue1.Text);
                Calc.listRestrictAppIcome.Add(lblRestrictAppIncomeValue2.Text);
                Calc.listRestrictAppIcome.Add(lblRestrictAppIncomeValue3.Text);

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// set the combined income for 1 strategy
        /// </summary>
        /// <param name="list"></param>
        private void CombineIncomeLabelForOneStrategy(List<KeyValuePair<string, int>> list)
        {
            try
            {

                if (list[0].Key == Constants.FullStrategy)
                {
                    lblCombinedIncome2.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Full]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForFull != 0)
                    {
                        lblRestrictAppIncomeValue1.Text = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue1.Visible = true;
                        lblRestrictAppIncomeTitle1.Visible = true;
                    }
                }
                else if (list[0].Key == Constants.SuspendStrategy)
                    lblCombinedIncome2.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Suspend]).ToString(Constants.AMT_FORMAT);
                else if (list[0].Key == Constants.BestSrategy)
                {
                    lblCombinedIncome2.Text = Convert.ToDecimal(Session[Constants.valueAtAge704]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForBest != 0)
                    {
                        lblRestrictAppIncomeValue2.Text = Globals.Instance.IntShowRestrictIncomeForBest.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue2.Visible = true;
                        lblRestrictAppIncomeTitle2.Visible = true;
                    }
                }
                else if (list[0].Key == Constants.MaxStrategy)
                {
                    lblCombinedIncome2.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Max]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForMax != 0)
                    {
                        lblRestrictAppIncomeValue2.Text = Globals.Instance.IntShowRestrictIncomeForMax.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue2.Visible = true;
                        lblRestrictAppIncomeTitle2.Visible = true;
                    }
                }
                else
                {
                    lblCombinedIncome2.Text = Convert.ToDecimal(Session[Constants.valueAtAge703]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForEarly != 0)
                    {
                        lblRestrictAppIncomeValue2.Text = Globals.Instance.IntShowRestrictIncomeForEarly.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue2.Visible = true;
                        lblRestrictAppIncomeTitle2.Visible = true;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// set the combined income for 2 strategy
        /// </summary>
        /// <param name="list"></param>
        private void CombineIncomeLableForTwoStrategies(List<KeyValuePair<string, int>> list)
        {
            try
            {
                if (list[0].Key == Constants.FullStrategy)
                {
                    lblCombinedIncome1.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Full]).ToString(Constants.AMT_FORMAT);

                    if (Globals.Instance.IntShowRestrictIncomeForFull != 0)
                    {
                        lblRestrictAppIncomeValue1.Text = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue1.Visible = true;
                        lblRestrictAppIncomeTitle1.Visible = true;
                    }

                }
                else if (list[0].Key == Constants.SuspendStrategy)
                    lblCombinedIncome1.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Suspend]).ToString(Constants.AMT_FORMAT);
                else if (list[0].Key == Constants.BestSrategy)
                {
                    lblCombinedIncome1.Text = Convert.ToDecimal(Session[Constants.valueAtAge703]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForBest != 0)
                    {
                        lblRestrictAppIncomeValue1.Text = Globals.Instance.IntShowRestrictIncomeForBest.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue1.Visible = true;
                        lblRestrictAppIncomeTitle1.Visible = true;
                    }
                }
                else
                {
                    lblCombinedIncome1.Text = Convert.ToDecimal(Session[Constants.valueAtAge704]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForEarly != 0)
                    {
                        lblRestrictAppIncomeValue1.Text = Globals.Instance.IntShowRestrictIncomeForEarly.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue1.Visible = true;
                        lblRestrictAppIncomeTitle1.Visible = true;
                    }
                }

                if (list[1].Key == Constants.MaxStrategy)
                {
                    lblCombinedIncome3.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Max]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForMax != 0)
                    {
                        lblRestrictAppIncomeValue3.Text = Globals.Instance.IntShowRestrictIncomeForMax.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue3.Visible = true;
                        lblRestrictAppIncomeTitle3.Visible = true;
                    }
                }
                else if (list[1].Key == Constants.SuspendStrategy)
                    lblCombinedIncome3.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Suspend]).ToString(Constants.AMT_FORMAT);
                else
                {
                    lblCombinedIncome3.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Full]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForFull != 0)
                    {
                        lblRestrictAppIncomeValue3.Text = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue3.Visible = true;
                        lblRestrictAppIncomeTitle3.Visible = true;
                    }

                }
                lblCombinedIncome2.Text = string.Empty;

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// set the combined income for 3 strategy
        /// </summary>
        /// <param name="list"></param>
        private void CombineIncomeLabelForThreeStrategies(List<KeyValuePair<string, int>> list)
        {
            try
            {
                if (list[0].Key == Constants.FullStrategy)
                {
                    lblCombinedIncome1.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Full]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForFull != 0)
                    {
                        lblRestrictAppIncomeValue1.Text = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue1.Visible = true;
                        lblRestrictAppIncomeTitle1.Visible = true;
                    }
                }
                else if (list[0].Key == Constants.SuspendStrategy)
                    lblCombinedIncome1.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Suspend]).ToString(Constants.AMT_FORMAT);
                else if (list[0].Key == Constants.BestSrategy)
                {
                    lblCombinedIncome1.Text = Convert.ToDecimal(Session[Constants.valueAtAge704]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForBest != 0)
                    {
                        lblRestrictAppIncomeValue1.Text = Globals.Instance.IntShowRestrictIncomeForBest.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue1.Visible = true;
                        lblRestrictAppIncomeTitle1.Visible = true;
                    }
                }
                else if (list[0].Key == Constants.MaxStrategy)
                {
                    lblCombinedIncome1.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Max]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForMax != 0)
                    {
                        lblRestrictAppIncomeValue1.Text = Globals.Instance.IntShowRestrictIncomeForMax.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue1.Visible = true;
                        lblRestrictAppIncomeTitle1.Visible = true;
                    }
                }
                else
                {
                    lblCombinedIncome1.Text = Convert.ToDecimal(Session[Constants.valueAtAge703]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForEarly != 0)
                    {
                        lblRestrictAppIncomeValue1.Text = Globals.Instance.IntShowRestrictIncomeForEarly.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue1.Visible = true;
                        lblRestrictAppIncomeTitle1.Visible = true;
                    }

                }


                if (list[1].Key == Constants.FullStrategy)
                {
                    lblCombinedIncome2.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Full]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForFull != 0)
                    {
                        lblRestrictAppIncomeValue2.Text = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue2.Visible = true;
                        lblRestrictAppIncomeTitle2.Visible = true;
                    }
                }
                else if (list[1].Key == Constants.SuspendStrategy)
                    lblCombinedIncome2.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Suspend]).ToString(Constants.AMT_FORMAT);
                else if (list[1].Key == Constants.BestSrategy)
                {
                    lblCombinedIncome2.Text = Convert.ToDecimal(Session[Constants.valueAtAge704]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForBest != 0)
                    {
                        lblRestrictAppIncomeValue2.Text = Globals.Instance.IntShowRestrictIncomeForBest.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue2.Visible = true;
                        lblRestrictAppIncomeTitle2.Visible = true;
                    }
                }
                else
                {
                    lblCombinedIncome2.Text = Convert.ToDecimal(Session[Constants.valueAtAge703]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForEarly != 0)
                    {
                        lblRestrictAppIncomeValue2.Text = Globals.Instance.IntShowRestrictIncomeForEarly.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue2.Visible = true;
                        lblRestrictAppIncomeTitle2.Visible = true;
                    }

                }

                if (list[2].Key == Constants.MaxStrategy)
                {
                    lblCombinedIncome3.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Max]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForMax != 0)
                    {
                        lblRestrictAppIncomeValue3.Text = Globals.Instance.IntShowRestrictIncomeForMax.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue3.Visible = true;
                        lblRestrictAppIncomeTitle3.Visible = true;
                    }
                }
                else if (list[2].Key == Constants.SuspendStrategy)
                    lblCombinedIncome3.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Suspend]).ToString(Constants.AMT_FORMAT);
                else
                {
                    lblCombinedIncome3.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Full]).ToString(Constants.AMT_FORMAT);
                    if (Globals.Instance.IntShowRestrictIncomeForFull != 0)
                    {
                        lblRestrictAppIncomeValue3.Text = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT);
                        lblRestrictAppIncomeValue3.Visible = true;
                        lblRestrictAppIncomeTitle3.Visible = true;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to print the keys and steps to the grid best strategy1 ie Claim Now, Claim More Later
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        public void renderKeysIntoGridBest(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, string ClaimAndSuspendinFullAge, decimal ClaimAndSuspendinFullValue, string CombinedIncomeAge, string AgeForHusb70, decimal ValueForHusb70, string AgeForWife70, decimal ValueForWife70, decimal AgeValueForSpousalAt70, decimal ValueForWifeAt70)
        {
            try
            {
                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName, strColumnNameHusb = string.Empty;
                string strColumnName = string.Empty, strStartAgeForUnClaimed = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                string sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                string sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strDoubleAsteriskIncludes = string.Empty;
                bool boolRestrictIncomeWife = false;
                bool boolRestrictIncomeHusb = false;

                //FRA age in months
                int intWifeFraInMonth = calc.CalculateFRAAgeInMonth(Convert.ToInt32(birthWifeTempYYYY.Text));
                int intHusbFraInMonth = calc.CalculateFRAAgeInMonth(Convert.ToInt32(BirthHusbandTempYYYY.Text));

                DateTime HusbDOB = DateTime.Parse(BirthHusbandYYYY.Text.ToString());
                DateTime WifeDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());

                //Current age in months
                int intAgeWifeInMonth = calc.CalculateAgeinTotalMonth(WifeDOB);
                int intAgeHusbInMonth = calc.CalculateAgeinTotalMonth(HusbDOB);
                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    strName = strUserSpouseFirstName;
                    //strColumnNameWife = Constants.keyFigures14;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnNameHusb = Constants.keyFigures13;
                    strColumnName = Constants.keyFigures13;
                }

                if (Globals.Instance.WifeNumberofMonthinNote > 0)
                {
                    strageWife = Globals.Instance.WifeNumberofYearsAbove66 + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                }
                else
                {
                    strageWife = ageWife.Substring(0, 2);
                }


                if (Globals.Instance.HusbandNumberofMonthsinNote > 0)
                {
                    strageHusb = Globals.Instance.HusbNumberofYearsAbove66 + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                }
                else
                {
                    strageHusb = ageHusb.Substring(3, 2);
                }
                //to check if he cummulative income is not zero
                if (cumm69 != 0)
                {
                    lblKey4.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + combineBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
                    lblKey4.Visible = true;
                }
                else
                {
                    lblKey4.Visible = false;
                }
                if (combineBenefits != 0)
                    lblKey5.Text = string.Format(combineBenefits.ToString(Constants.AMT_FORMAT) + strColumnName + Constants.steps7 + strName + Constants.keyFigures8);
                else
                    lblKey5.Text = string.Empty;
                if (valueAtAge70 != 0)
                    lblKey6.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);
                else
                    lblKey6.Text = string.Empty;

                if (Globals.Instance.BoolHusbandAgeAdjusted && Globals.Instance.HusbandNumberofMonthsinNote > 0)
                    lblNoteHusband0.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                if (Globals.Instance.BoolWifeAgeAdjusted && Globals.Instance.WifeNumberofMonthinNote > 0)
                    lblNoteWife0.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                    lblHusbandExplanation1.Text = Constants.includes + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                    lblWifeExplanation1.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //notes for husband and wife
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblHusbandExplanation1.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblNoteHusband.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                }
                else if (Globals.Instance.BoolWHBSwitchedWife)
                {
                    lblWifeExplanation1.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                    lblNoteWife.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                }

                //to check whos value is greater husband or wife
                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    #region Commented Code
                    //if (Globals.Instance.ShowAsterikForTwoStrategies)
                    //{

                    //Label4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ClaimAndSuspendinFullAge + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.CliamAndSuspendWife + Constants.ClaimandSuspendWarning + strUserSpouseFirstName + Constants.CliamAndSuspend1 + strUserFirstName + Constants.CliamAndSuspend2);
                    //lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ClaimAndSuspendinFullAge + Constants.when + strUserFirstName + Constants.steps8 + ClaimAndSuspendinFullAge.Substring(3, 2) + Constants.steps1 + ClaimAndSuspendinFullValue.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.MarriedSpousalText);
                    //if (startingValueHusb != ValueForHusb70)
                    //{
                    //    lblStep4.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps18 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.Age70Text);
                    //    lblStep6.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + Constants.steps9 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    //    lblStep7.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    //    Label4.Visible = true;
                    //    lblStep4.Visible = true;
                    //    lblStep5.Visible = true;
                    //    lblStep6.Visible = true;
                    //    lblStep7.Visible = true;
                    //}
                    //else
                    //{
                    //    lblStep4.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + Constants.steps9 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    //    lblStep6.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    //    Label4.Visible = true;
                    //    lblStep4.Visible = true;
                    //    lblStep5.Visible = true;
                    //    lblStep6.Visible = true;
                    //    lblStep7.Visible = false;
                    //}
                    //Globals.Instance.CurrHusValue = false;
                    //Globals.Instance.CurrHusValuePDF = true;
                    //Globals.Instance.ShowAsterikForTwoStrategies = false;

                    //}
                    //else
                    //{
                    #endregion Commented Code

                    if (sHusbFRA.Substring(0, 2).Equals((intHusbFRA.ToString())) && int.Parse(sHusbFRA.Substring(0, 2)) > int.Parse(strageHusb.Substring(0, 2)))
                    {
                        lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.ReducedWorkBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                    }
                    else
                    {
                        //if (sHusbFRA.Substring(0, 2).Equals((intHusbFRA.ToString())))
                        if (intAgeHusbInMonth == intHusbFraInMonth)
                            lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.FRAText + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                        //else if (int.Parse(sHusbFRA.Substring(0, 2)) > int.Parse(intHusbFRA.ToString()))
                        else if (intHusbFraInMonth > intAgeHusbInMonth)
                            lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.ReducedWorkBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                        else
                            lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                    }
                    if (startingValueWife != ValueForWife70)
                    {
                        //if (intWifeFRA.ToString().Equals(sWifeFRA.Substring(0, 2)))
                        if (intAgeWifeInMonth == intWifeFraInMonth)
                        {
                            strStartAgeForUnClaimed = sWifeFRA;
                            lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                        }
                        else
                        {
                            strStartAgeForUnClaimed = strageWife;
                            lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                        }
                        boolRestrictIncomeWife = true;



                        if (Globals.Instance.BoolSpousalChangeAtAge70)
                        {
                            if (ValueForWifeAt70 != 0)
                            {
                                if (Globals.Instance.BoolWHBSwitchedHusb)
                                    lblStep13.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months + ", " + strUserFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                else
                                    lblStep13.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + AgeValueForSpousalAt70 + ", " + strUserFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                lblStep13.Visible = true;
                            }
                            else
                            {
                                lblStep13.Visible = false;
                            }
                            lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserSpouseFirstName + Constants.steps18 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                            lblStep7.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            lblStep4.Visible = true;
                            lblStep5.Visible = true;
                            lblStep6.Visible = true;
                            lblStep7.Visible = true;
                        }
                        else
                        {
                            lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserSpouseFirstName + Constants.steps18 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                            lblStep7.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            lblStep4.Visible = true;
                            lblStep5.Visible = true;
                            lblStep6.Visible = true;
                            lblStep7.Visible = true;
                        }
                    }
                    else
                    {
                        if (Globals.Instance.BoolSpousalChangeAtAge70 && ValueForWifeAt70 != 0)
                        {
                            if (Globals.Instance.BoolWHBSwitchedHusb)
                                lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months + ", " + strUserFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                            else
                                lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + AgeValueForSpousalAt70 + ", " + strUserFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                            lblStep7.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            lblStep6.Visible = true;
                        }
                        else
                        {
                            lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            lblStep6.Visible = true;
                        }
                        if (intWifeFRA.ToString() == Constants.Number70)
                            lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                        else
                            lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                        lblStep5.Visible = true;
                        lblStep7.Visible = true;
                    }
                    //}
                }
                else
                {
                    #region Commented Code
                    //if (Globals.Instance.ShowAsterikForTwoStrategies)
                    //{
                    //    Label4.Text = string.Format(Constants.Line2 + Constants.Age + " " + ClaimAndSuspendinFullAge + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + Constants.CliamAndSuspendHusb + Constants.ClaimandSuspendWarning + strUserFirstName + Constants.CliamAndSuspend1 + strUserSpouseFirstName + Constants.CliamAndSuspend2);
                    //    lblStep5.Text = string.Format(Constants.Line3 + Constants.Age + " " + ClaimAndSuspendinFullAge + Constants.when + strUserSpouseFirstName + Constants.steps8 + ClaimAndSuspendinFullAge.Substring(0, 2) + Constants.steps1 + ClaimAndSuspendinFullValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.MarriedSpousalText);
                    //    if (startingValueWife != ValueForWife70)
                    //    {
                    //        lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps18 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                    //        lblStep6.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps9 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    //        lblStep7.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    //        Label4.Visible = true;
                    //        lblStep5.Visible = true;
                    //        lblStep4.Visible = true;
                    //        lblStep6.Visible = true;
                    //        lblStep7.Visible = true;
                    //    }
                    //    else
                    //    {
                    //        lblStep6.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps9 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    //        lblStep7.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    //        Label4.Visible = true;
                    //        lblStep5.Visible = true;
                    //        lblStep4.Visible = true;
                    //        lblStep6.Visible = true;
                    //        lblStep7.Visible = true;
                    //    }
                    //    Globals.Instance.CurrWifeValue = false;
                    //    Globals.Instance.CurrWifeValuePDF = true;
                    //    Globals.Instance.ShowAsterikForTwoStrategies = false;
                    //}
                    //else
                    //{
                    #endregion Commented Code
                    if (strageHusb != Constants.Number70)
                    {
                        //if (sHusbFRA.Substring(0, 2).Equals((strageHusb.Substring(0, 2))))
                        lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                        boolRestrictIncomeHusb = true;
                        strStartAgeForUnClaimed = strageHusb;
                        //else
                        // lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                    }
                    else
                    {
                        if (Globals.Instance.BoolSpousalCliamedWifeSt1)
                            lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        else
                            lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    }
                    //to check if the starting benefits for husb is same to his age 70 value
                    if (startingValueHusb != ValueForHusb70)
                    {
                        if (intAgeWifeInMonth < intWifeFraInMonth)
                            lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.ReducedWorkBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        else if (intAgeWifeInMonth == intWifeFraInMonth)
                            lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        else
                            //lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        if (Globals.Instance.BoolSpousalChangeAtAge70)
                        {
                            if (ValueForWifeAt70 != 0)
                            {

                                lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps18 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                                if (Globals.Instance.BoolWHBSwitchedWife)
                                    lblStep13.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months + ", " + strUserSpouseFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                else
                                    lblStep13.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + AgeValueForSpousalAt70 + ", " + strUserSpouseFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                lblStep7.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStep13.Visible = true;
                            }
                            else
                            {
                                lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps18 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                                lblStep7.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStep13.Visible = false;
                            }
                            lblStep5.Visible = true;
                            lblStep4.Visible = true;
                            lblStep6.Visible = true;
                            lblStep7.Visible = true;
                        }
                        else
                        {
                            lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps18 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                            lblStep7.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            lblStep5.Visible = true;
                            lblStep4.Visible = true;
                            lblStep6.Visible = true;
                            lblStep7.Visible = true;
                        }
                    }
                    else
                    {
                        if (Globals.Instance.BoolSpousalChangeAtAge70 && ValueForWifeAt70 != 0)
                        {
                            if (Globals.Instance.BoolWHBSwitchedWife)
                                lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months + ", " + strUserSpouseFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            else
                                lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + AgeValueForSpousalAt70 + ", " + strUserSpouseFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            lblStep7.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                        else
                        {
                            lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                        if (intWifeFRA.ToString() == Constants.Number70)
                        {
                            lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps18 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        }
                        else
                        {
                            if (AgeValueForSpousalAt70.ToString() != strageWife)
                            {
                                if (Globals.Instance.BoolSpousalCliamedWifeSt1)
                                {
                                    if (intAgeWifeInMonth < intWifeFraInMonth)
                                        lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.ReducedSpousalBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                    else if (intAgeWifeInMonth == intWifeFraInMonth)
                                        lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                    else
                                        lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                }
                                else
                                {
                                    if (intAgeWifeInMonth < intWifeFraInMonth)
                                        //lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.ReducedWorkBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                        lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.ReducedWorkBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                    else if (intAgeWifeInMonth == intWifeFraInMonth)
                                        lblStep4.Text
                                            = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                    else
                                        lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                }
                            }
                            else
                            {
                                if (Globals.Instance.BoolWHBSwitchedWife)
                                    lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months + ", " + strUserSpouseFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                else
                                    lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + AgeValueForSpousalAt70 + ", " + strUserSpouseFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                                lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStep7.Text = "";

                            }
                        }
                        lblStep5.Visible = true;
                        lblStep4.Visible = true;
                        lblStep6.Visible = true;
                        lblStep7.Visible = true;
                    }
                    //}
                }

                if (boolRestrictIncomeWife)
                {
                    Globals.Instance.IntShowRestrictIncomeForBest = Globals.Instance.intRestrictAppIncomeBestWife;
                    lblKeyRestrictIncomBest.Text = Globals.Instance.intRestrictAppIncomeBestWife.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;

                    lblKeyRestrictIncomBest1.Text = Constants.RestrictExplainNew1 + strUserSpouseFirstName + Constants.RestrictExplainNew2 + strUserSpouseFirstName + Constants.RestrictExplainNew3 + strStartAgeForUnClaimed + Constants.RestrictExplainNew4 + Globals.Instance.intRestrictAppIncomeFullWife.ToString(Constants.AMT_FORMAT) + Constants.RestrictExplainNew5;

                    calc.DesignGreeBoxForRestrictStrategy(GridBest, strStartAgeForUnClaimed, "Wife");
                }
                else if (boolRestrictIncomeHusb)
                {
                    Globals.Instance.IntShowRestrictIncomeForBest = Globals.Instance.intRestrictAppIncomeBestHusb;
                    lblKeyRestrictIncomBest.Text = Globals.Instance.intRestrictAppIncomeBestHusb.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                    lblKeyRestrictIncomBest1.Text = Constants.RestrictExplainNew1 + strUserFirstName + Constants.RestrictExplainNew2 + strUserFirstName + Constants.RestrictExplainNew3 + strStartAgeForUnClaimed + Constants.RestrictExplainNew4 + Globals.Instance.intRestrictAppIncomeBestHusb.ToString(Constants.AMT_FORMAT) + Constants.RestrictExplainNew5;

                    calc.DesignGreeBoxForRestrictStrategy(GridBest, strStartAgeForUnClaimed, "Husband");
                }
                //Set Keys and Steps values
                SetKeyAndStepsValuesForPDF("GridBest");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }



        public void renderKeysIntoGridZero(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, string ClaimAndSuspendinFullAge, decimal ClaimAndSuspendinFullValue, string CombinedIncomeAge, string AgeForHusb70, decimal ValueForHusb70, string AgeForWife70, decimal ValueForWife70, decimal AgeValueForSpousalAt70, decimal ValueForWifeAt70)
        {
            try
            {

                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName, strColumnNameHusb = string.Empty;
                string strColumnName = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                string sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                string sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strDoubleAsteriskIncludes = string.Empty;
                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    strName = strUserSpouseFirstName;
                    //strColumnNameWife = Constants.keyFigures14;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnNameHusb = Constants.keyFigures13;
                    strColumnName = Constants.keyFigures13;
                }

                if (Globals.Instance.WifeNumberofMonthsBelowGridAgePrior > 0)
                    strageWife = Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                else
                    strageWife = ageWife.Substring(0, 2);

                if (Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior > 0)
                    strageHusb = Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                else
                    strageHusb = ageHusb.Substring(3, 2);

                //to check if he cummulative income is not zero
                if (cumm69 != 0)
                {
                    lblKeyZero1.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + combineBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
                    lblKeyZero1.Visible = true;
                }
                else
                {
                    lblKeyZero1.Visible = false;
                }
                if (combineBenefits != 0)
                    lblKeyZero2.Text = string.Format(combineBenefits.ToString(Constants.AMT_FORMAT) + strColumnName + Constants.steps7 + strName + Constants.keyFigures8);
                else
                    lblKeyZero2.Text = string.Empty;
                if (valueAtAge70 != 0)
                    lblKeyZero3.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);
                else
                    lblKeyZero3.Text = string.Empty;

                if (Globals.Instance.BoolHusbandAgeAdjusted && Globals.Instance.HusbandNumberofMonthsinNote > 0)
                    lblZeroNoteHusb0.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                if (Globals.Instance.BoolWifeAgeAdjusted && Globals.Instance.WifeNumberofMonthinNote > 0)
                    lblZeroNoteWife0.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                    lblZeroHusbandExplanation1.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                    lblZeroWifeExplanation1.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //notes for husband and wife
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    //lblZeroHusbandExplanation1.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblZeroNoteHusband.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                }
                else if (Globals.Instance.BoolWHBSwitchedWife)
                {
                    //lblZeroWifeExplanation1.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                    lblZeroNoteWife.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                }



                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    if (Globals.Instance.BoolSpousalChangeAtAge70)
                    {
                        if (ValueForWifeAt70 != 0)
                        {
                            if (Globals.Instance.BoolWHBSwitchedHusb)
                                lblZeroSteps5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months + ", " + strUserFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                            else
                                lblZeroSteps5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + AgeValueForSpousalAt70 + ", " + strUserFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                            lblZeroSteps5.Visible = true;
                        }
                        else
                        {
                            lblZeroSteps5.Visible = false;
                        }
                        lblZeroSteps4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserSpouseFirstName + Constants.steps6WHB + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                        lblZeroSteps6.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);

                        lblZeroSteps4.Visible = true;
                        lblZeroSteps6.Visible = true;
                    }
                    else
                    {
                        lblZeroSteps4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserSpouseFirstName + Constants.steps6WHB + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        lblZeroSteps6.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);

                        lblZeroSteps4.Visible = true;
                        lblZeroSteps6.Visible = true;
                    }

                }
                else
                {
                    if (Globals.Instance.BoolSpousalChangeAtAge70)
                    {
                        if (ValueForWifeAt70 != 0)
                        {

                            lblZeroSteps4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps6WHB + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                            if (Globals.Instance.BoolWHBSwitchedWife)
                                lblZeroSteps5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months + ", " + strUserSpouseFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            else
                                lblZeroSteps5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            lblZeroSteps6.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            lblZeroSteps5.Visible = true;
                        }
                        else
                        {
                            lblZeroSteps4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps6WHB + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                            lblZeroSteps6.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            lblZeroSteps5.Visible = false;
                        }

                        lblZeroSteps4.Visible = true;
                        lblZeroSteps6.Visible = true;
                    }
                    else
                    {
                        lblZeroSteps4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps6WHB + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        lblZeroSteps6.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);

                        lblZeroSteps4.Visible = true;
                        lblZeroSteps6.Visible = true;
                    }

                }

                //Set Keys and Steps values
                SetKeyAndStepsValuesForPDF("ZeroStrategy");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }
        /// <summary>
        /// Used to set key and steps values to display on PDF 
        /// </summary>
        /// <param name="strGridName">Grid Name</param>
        public void SetKeyAndStepsValuesForPDF(string strGridName)
        {
            try
            {
                if (strGridName.Equals(Constants.GridBest))
                {
                    CommonVariables.strRestrictKeyBest = string.Empty;
                    CommonVariables.strKey1 = lblKey4.Text;
                    CommonVariables.strKey2 = lblKey5.Text;
                    CommonVariables.strKey3 = lblKey6.Text;
                    if (Globals.Instance.IntShowRestrictIncomeForBest != 0)
                        CommonVariables.strRestrictKeyBest = Globals.Instance.IntShowRestrictIncomeForBest.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                    CommonVariables.strStep1 = lblStep4.Text;
                    CommonVariables.strStep2 = Label4.Text;
                    CommonVariables.strStep3 = lblStep5.Text;
                    CommonVariables.strStep4 = lblStep6.Text;
                    CommonVariables.strStep5 = lblStep13.Text;
                    CommonVariables.strStep6 = lblStep7.Text;
                    CommonVariables.strNoteHusband0 = lblNoteHusband0.Text;
                    CommonVariables.strNoteWife0 = lblNoteWife0.Text;
                    CommonVariables.strNoteHusband = lblNoteHusband.Text;
                    CommonVariables.strNoteWife = lblNoteWife.Text;
                    CommonVariables.strHusbExplain1 = lblHusbandExplanation1.Text;
                    CommonVariables.strWifeExplain1 = lblWifeExplanation1.Text;
                }
                else if (strGridName.Equals("Max"))
                {
                    CommonVariables.strRestrictKeyMax = string.Empty;
                    CommonVariables.strKey4 = lblKeys1.Text;
                    CommonVariables.strKey5 = lblKeys2.Text;
                    CommonVariables.strKey6 = lblKeys3.Text;
                    if (Globals.Instance.IntShowRestrictIncomeForMax != 0)
                        CommonVariables.strRestrictKeyMax = Globals.Instance.IntShowRestrictIncomeForMax.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                    CommonVariables.strStep7 = lblStep1.Text;
                    CommonVariables.strStep8 = lblStep2.Text;
                    CommonVariables.strStep9 = lblStep3.Text;
                    CommonVariables.strStep10 = lblStepMax4.Text;
                    CommonVariables.strNoteHusband6 = lblNoteHusband6.Text;
                    CommonVariables.strNoteWife6 = lblNoteWife6.Text;
                    CommonVariables.strNoteHusband7 = lblNoteHusband7.Text;
                    CommonVariables.strNoteWife7 = lblNoteWife7.Text;
                    CommonVariables.strHusbExplain4 = lblHusbandExplanation4.Text;
                    CommonVariables.strWifeExplain4 = lblWifeExplanation4.Text;
                }
                else if (strGridName.Equals("Full"))
                {
                    CommonVariables.strRestrictKeyFull = string.Empty;
                    CommonVariables.strKey7 = lblKey7.Text;
                    CommonVariables.strKey8 = lblKey8.Text;
                    CommonVariables.strKey9 = lblKey9.Text;
                    if (Globals.Instance.IntShowRestrictIncomeForFull != 0)
                        CommonVariables.strRestrictKeyFull = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                    CommonVariables.strStep11 = lblSteps0.Text;
                    CommonVariables.strStep12 = Label6.Text;
                    CommonVariables.strStep13 = lblSteps1.Text;
                    CommonVariables.strStep14 = lblSteps2.Text;
                    CommonVariables.strStep15 = lblSteps3.Text;
                    CommonVariables.strNoteHusband5 = lblNoteHusband5.Text;
                    CommonVariables.strNoteWife5 = lblNoteWife5.Text;
                    CommonVariables.strNoteHusband4 = lblNoteHusband4.Text;
                    CommonVariables.strNoteWife4 = lblNoteWife4.Text;
                    CommonVariables.strHusbExplain3 = lblHusbandExplanation3.Text;
                    CommonVariables.strWifeExplain3 = lblWifeExplanation3.Text;
                }
                else if (strGridName.Equals(Constants.GridEarly))
                {
                    CommonVariables.strRestrictKeyEarly = string.Empty;
                    CommonVariables.strKey10 = lblKey1.Text;
                    CommonVariables.strKey11 = lblKey2.Text;
                    CommonVariables.strKey12 = lblKey3.Text;
                    if (Globals.Instance.IntShowRestrictIncomeForEarly != 0)
                        CommonVariables.strRestrictKeyEarly = Globals.Instance.IntShowRestrictIncomeForEarly.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                    CommonVariables.strStep16 = lblStep8.Text;
                    CommonVariables.strStep17 = lblStep9.Text;
                    CommonVariables.strStep18 = lblStep10.Text;
                    CommonVariables.strStep19 = lblStep11.Text;
                    CommonVariables.strStep20 = lblStep12.Text;
                    CommonVariables.strNoteHusband1 = lblNoteHusband1.Text;
                    CommonVariables.strNoteWife1 = lblNoteWife1.Text;
                    CommonVariables.strNoteHusband3 = lblNoteHusband3.Text;
                    CommonVariables.strNoteWife3 = lblNoteWife3.Text;
                    CommonVariables.strHusbExplain2 = lblHusbandExplanation2.Text;
                    CommonVariables.strWifeExplain2 = lblWifeExplanation2.Text;
                }
                else if (strGridName.Equals(Constants.GridZero))
                {
                    CommonVariables.strKey13 = lblKeyZero1.Text;
                    CommonVariables.strKey14 = lblKeyZero2.Text;
                    CommonVariables.strKey15 = lblKeyZero3.Text;
                    CommonVariables.strStep21 = lblZeroSteps4.Text;
                    CommonVariables.strStep22 = lblZeroSteps5.Text;
                    CommonVariables.strStep23 = lblZeroSteps6.Text;
                    CommonVariables.strNoteWife11 = lblZeroNoteWife0.Text;
                    CommonVariables.strNoteHusband8 = lblZeroNoteHusb0.Text;
                    CommonVariables.strNoteWife12 = lblZeroWifeExplanation1.Text;
                    CommonVariables.strNoteHusband9 = lblZeroHusbandExplanation1.Text;
                    CommonVariables.strNoteWife13 = lblZeroNoteWife.Text;
                    CommonVariables.strNoteHusband10 = lblZeroNoteHusband.Text;

                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to print the keys and steps to the grid max strategy5 ie claim at age 70
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        public void renderKeysIntoGridMax(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, decimal ValueForWife70, decimal ValueForHusb70, string AgeForWife70, string AgeForHusb70, string CombinedIncomeAge)
        {
            try
            {
                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName = string.Empty;
                string strColumnName = string.Empty, strStartAgeForUnClaimed = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                string sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                string sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strWifeSurvivorText, strHusbSurvivorText;
                string strDoubleAsteriskIncludes = string.Empty;
                bool boolRestrictIncomHusb = false, boolRestrictIncomWife = false;
                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    strName = strUserSpouseFirstName;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnName = Constants.keyFigures13;
                }
                if (ValueForHusb70 > ValueForWife70)
                {
                    strHusbSurvivorText = Constants.steps15;
                    strWifeSurvivorText = string.Empty;
                    lblKeys2.Text = string.Format(ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures13 + Constants.steps7 + strUserFirstName + Constants.keyFigures8);
                }
                else
                {
                    strWifeSurvivorText = Constants.steps15;
                    strHusbSurvivorText = string.Empty;
                    lblKeys2.Text = string.Format(ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps7 + strUserSpouseFirstName + Constants.keyFigures8);
                }
                if (Globals.Instance.WifeNumberofMonthinNote > 0)
                {
                    strageWife = Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                }
                else
                {
                    strageWife = ageWife.Substring(0, 2);
                }

                if (Globals.Instance.HusbandNumberofMonthsinNote > 0)
                {
                    strageHusb = Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                }
                else
                {
                    strageHusb = ageHusb.Substring(3, 2);
                }
                if (cumm69 != 0)
                    lblKeys1.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + combineBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);

                lblKeys3.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);

                //notes for husband and wife
                if (Globals.Instance.BoolHusbandAgeAdjusted)
                    lblNoteHusband7.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                if (Globals.Instance.BoolWifeAgeAdjusted)
                    lblNoteWife7.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                    lblHusbandExplanation4.Text = Constants.includes + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                    lblWifeExplanation4.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //notes for husband and wife
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblHusbandExplanation4.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblNoteHusband6.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                }
                else if (Globals.Instance.BoolWHBSwitchedWife)
                {
                    lblWifeExplanation4.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                    lblNoteWife6.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                }

                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    if (intHusbFRA.ToString().Equals(Constants.Number70) && intWifeFRA.ToString().Equals(Constants.Number70))
                    {
                        if (int.Parse(ageWife.Substring(3, 2)) < intWifeFRA)
                        {
                            lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                            lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserSpouseFirstName + Constants.steps23 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                        }
                        else
                        {
                            lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserSpouseFirstName + Constants.steps23 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                            lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                        }
                        lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        lblStep1.Visible = true;
                        lblStep2.Visible = true;
                        lblStep3.Visible = true;
                    }
                    else
                    {
                        if (intWifeFRA > intHusbFRA)
                        {
                            if (Globals.Instance.BoolMaxStrategyAt70Husb)
                            {
                                if (sHusbFRA.Substring(0, 2).Equals(intHusbFRA.ToString()))
                                {
                                    strStartAgeForUnClaimed = sHusbFRA;
                                    lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                                }
                                else
                                {
                                    strStartAgeForUnClaimed = strageHusb;
                                    lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                                }
                                boolRestrictIncomHusb = true;
                                
                                lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserSpouseFirstName + Constants.steps23 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                                lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + AgeForHusb70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps18 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                                lblStepMax4.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStepMax4.Visible = true;
                                lblStep1.Visible = true;
                                lblStep2.Visible = true;
                                lblStep3.Visible = true;
                            }
                            else
                            {
                                lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                                lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStepMax4.Visible = false;
                                lblStep1.Visible = true;
                                lblStep2.Visible = true;
                                lblStep3.Visible = true;
                            }
                        }
                        else
                        {
                            if (Globals.Instance.BoolMaxStrategyAt70Wife)
                            {
                                if (sWifeFRA.Substring(0, 2).Equals(intWifeFRA.ToString()))
                                {
                                    strStartAgeForUnClaimed = sWifeFRA;
                                    lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                                }
                                else
                                {
                                    strStartAgeForUnClaimed = strageWife;
                                    lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                                }
                                boolRestrictIncomWife = true;
                                lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                                lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + AgeForWife70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserSpouseFirstName + Constants.steps18 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                                lblStepMax4.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStepMax4.Visible = true;
                                lblStep1.Visible = true;
                                lblStep2.Visible = true;
                                lblStep3.Visible = true;
                            }
                            else
                            {
                                lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                                lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStepMax4.Visible = false;
                                lblStep1.Visible = true;
                                lblStep2.Visible = true;
                                lblStep3.Visible = true;
                            }
                        }
                    }
                }
                else
                {
                    if (intHusbFRA.ToString().Equals(Constants.Number70) && intWifeFRA.ToString().Equals(Constants.Number70))
                    {
                        if (int.Parse(ageHusb.Substring(0, 2)) < intHusbFRA)
                        {
                            lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                            lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserSpouseFirstName + Constants.steps23 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                        }
                        else
                        {
                            lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                            lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserSpouseFirstName + Constants.steps23 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                        }
                        lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        lblStep1.Visible = true;
                        lblStep2.Visible = true;
                        lblStep3.Visible = true;
                        lblStepMax4.Visible = false;
                    }
                    else
                    {

                        if (intWifeFRA > intHusbFRA)
                        {
                            if (Globals.Instance.BoolMaxStrategyAt70Husb)
                            {
                                if (sHusbFRA.Substring(0, 2).Equals(intHusbFRA.ToString()))
                                {
                                    strStartAgeForUnClaimed = sHusbFRA;
                                    lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                                }
                                else
                                {
                                    strStartAgeForUnClaimed = strageHusb;
                                    lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                                }
                                boolRestrictIncomHusb = true;
                                lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + AgeForHusb70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps18 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                                lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserSpouseFirstName + Constants.steps23 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                                lblStepMax4.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStep1.Visible = true;
                                lblStep2.Visible = true;
                                lblStep3.Visible = true;
                                lblStepMax4.Visible = true;
                            }
                            else
                            {
                                lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                                lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStep1.Visible = true;
                                lblStep2.Visible = true;
                                lblStep3.Visible = true;
                                lblStepMax4.Visible = false;
                            }
                        }
                        else
                        {
                            if (Globals.Instance.BoolMaxStrategyAt70Wife)
                            {
                                if (sWifeFRA.Substring(0, 2).Equals(intWifeFRA.ToString()))
                                {
                                    strStartAgeForUnClaimed = sWifeFRA;
                                    lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                                }
                                else
                                {
                                    strStartAgeForUnClaimed = strageWife;
                                    lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                                }
                                boolRestrictIncomWife = true;
                                lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                                lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + AgeForWife70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserSpouseFirstName + Constants.steps18 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                                lblStepMax4.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStep1.Visible = true;
                                lblStep2.Visible = true;
                                lblStep3.Visible = true;
                                lblStepMax4.Visible = true;
                            }
                            else
                            {
                                lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                                lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStep1.Visible = true;
                                lblStep2.Visible = true;
                                lblStep3.Visible = true;
                                lblStepMax4.Visible = false;
                            }
                        }
                    }
                }

                if (boolRestrictIncomWife)
                {
                    Globals.Instance.IntShowRestrictIncomeForMax = Globals.Instance.intRestrictAppIncomeMaxWife;
                    lblKeyRestrictIncomMax.Text = Globals.Instance.intRestrictAppIncomeMaxWife.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                    lblKeyRestrictIncomMax1.Text = Constants.RestrictExplainNew1 + strUserFirstName + Constants.RestrictExplainNew2 + strUserSpouseFirstName + Constants.RestrictExplainNew3 + strUserSpouseFirstName + Constants.RestrictExplainNew4 + Globals.Instance.intRestrictAppIncomeMaxWife.ToString(Constants.AMT_FORMAT) + Constants.RestrictExplainNew5;

                    calc.DesignGreeBoxForRestrictStrategy(GridMax, strStartAgeForUnClaimed, "Wife");
                }
                else if (boolRestrictIncomHusb)
                {
                    Globals.Instance.IntShowRestrictIncomeForMax = Globals.Instance.intRestrictAppIncomeMaxHusb;
                    lblKeyRestrictIncomMax.Text = Globals.Instance.intRestrictAppIncomeMaxHusb.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                    lblKeyRestrictIncomMax1.Text = Constants.RestrictExplainNew1 + strUserFirstName + Constants.RestrictExplainNew2 + strUserFirstName + Constants.RestrictExplainNew3 + strStartAgeForUnClaimed + Constants.RestrictExplainNew4 + Globals.Instance.intRestrictAppIncomeMaxHusb.ToString(Constants.AMT_FORMAT) + Constants.RestrictExplainNew5;
                    calc.DesignGreeBoxForRestrictStrategy(GridMax, strStartAgeForUnClaimed, "Husband");
                }


                //Set Keys and Steps values
                SetKeyAndStepsValuesForPDF("Max");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        ///  code to print the keys and steps to the grid Early ie strategy Early Claim and Suspend
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        public void renderKeysIntoGridEarly(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, string ClaimAndSuspendinFullAge, decimal ClaimAndSuspendinFullValue, string CombinedIncomeAge, string AgeForHusb70, decimal ValueForHusb70, string AgeForWife70, decimal ValueForWife70, decimal AgeValueForSpousalAt70, decimal ValueForWifeAt70)
        {
            try
            {
                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName = string.Empty;
                string strColumnName = string.Empty, strStartAgeForUnClaimed = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                string sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                string sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strDoubleAsteriskIncludes = string.Empty;
                bool boolRestrictIncomWife = false, boolRestrictIncomHusb = false;
                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    strName = strUserSpouseFirstName;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnName = Constants.keyFigures13;
                }

                if (Globals.Instance.WifeNumberofMonthinNote > 0)
                    strageWife = Globals.Instance.WifeNumberofYearsAbove66 + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                else
                    strageWife = ageWife.Substring(0, 2);
                if (Globals.Instance.HusbandNumberofMonthsinNote > 0)
                    strageHusb = Globals.Instance.HusbNumberofYearsAbove66 + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                else
                    strageHusb = ageHusb.Substring(3, 2);
                //to check if he cummulative income is not zero
                if (cumm69 != 0)
                {
                    lblKey1.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + combineBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
                    lblKey1.Visible = true;
                }
                else
                {
                    lblKey1.Visible = false;
                }
                if (combineBenefits != 0)
                    lblKey2.Text = string.Format(combineBenefits.ToString(Constants.AMT_FORMAT) + strColumnName + Constants.steps7 + strName + Constants.keyFigures8);
                else
                    lblKey2.Text = string.Empty;
                if (valueAtAge70 != 0)
                    lblKey3.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);
                else
                    lblKey3.Text = string.Empty;

                if (Globals.Instance.BoolHusbandAgeAdjusted)
                    lblNoteHusband1.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                if (Globals.Instance.BoolWifeAgeAdjusted)
                    lblNoteWife1.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                    lblHusbandExplanation2.Text = Constants.includes + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                    lblWifeExplanation2.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //notes for husband and wife
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblHusbandExplanation2.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblNoteHusband3.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                }
                else if (Globals.Instance.BoolWHBSwitchedWife)
                {
                    lblWifeExplanation2.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                    lblNoteWife3.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                }
                //to check whos value is greater husband or wife
                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    //if (Globals.Instance.ShowAsterikForTwoStrategies)
                    //{
                    //    Label5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ClaimAndSuspendinFullAge + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.CliamAndSuspendWife + Constants.ClaimandSuspendWarning + strUserSpouseFirstName + Constants.CliamAndSuspend1 + strUserFirstName + Constants.CliamAndSuspend2);
                    //    lblStep8.Text = string.Format(Constants.Line2 + Constants.Age + " " + ClaimAndSuspendinFullAge + Constants.when + strUserFirstName + Constants.steps8 + ClaimAndSuspendinFullAge.Substring(3, 2) + Constants.steps1 + ClaimAndSuspendinFullValue.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.MarriedSpousalText);
                    //    if (startingValueHusb != ValueForHusb70)
                    //    {
                    //        lblStep9.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + AgeForHusb70 + Constants.steps18 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                    //        lblStep10.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + Constants.steps9 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    //        lblStep11.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    //        Label5.Visible = true;
                    //        lblStep8.Visible = true;
                    //        lblStep9.Visible = true;
                    //        lblStep10.Visible = true;
                    //        lblStep11.Visible = true;
                    //    }
                    //    else
                    //    {
                    //        lblStep9.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + Constants.steps9 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    //        lblStep10.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    //        Label5.Visible = true;
                    //        lblStep8.Visible = true;
                    //        lblStep9.Visible = true;
                    //        lblStep10.Visible = true;
                    //        lblStep11.Visible = false;
                    //    }
                    //    Globals.Instance.CurrHusValue = false;
                    //    Globals.Instance.CurrHusValuePDF = true;
                    //    Globals.Instance.ShowAsterikForTwoStrategies = false;
                    //}
                    //else
                    //{
                    if (intHusbFRA.ToString() != Constants.Number70)
                    {
                        if (sHusbFRA.Substring(0, 2).Equals((intHusbFRA.ToString())) && int.Parse(sHusbFRA.Substring(0, 2)) > int.Parse(strageHusb.Substring(0, 2)))
                        {
                            lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                        }
                        else
                        {
                            if (sHusbFRA.Substring(0, 2).Equals((intHusbFRA.ToString())))
                                lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                            else if (intHusbFRA < int.Parse(sHusbFRA.Substring(0, 2)))
                                lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                            else
                                lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                        }
                    }
                    else
                    {
                        lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                    }
                    if (startingValueWife != ValueForWife70)
                    {
                        if (intWifeFRA.ToString().Equals(sWifeFRA.Substring(0, 2)))
                        {
                            strStartAgeForUnClaimed = sWifeFRA;
                            lblStep9.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                        }
                        else
                        {
                            strStartAgeForUnClaimed = strageWife;
                            lblStep9.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                        }
                        boolRestrictIncomWife = true;
                        if (Globals.Instance.BoolSpousalChangeAtAge70)
                        {
                            if (ValueForWifeAt70 != 0)
                            {
                                if (Globals.Instance.BoolWHBSwitchedHusb)
                                    lblStep11.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months + ", " + strUserFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                else
                                    lblStep11.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + AgeValueForSpousalAt70 + ", " + strUserFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            }
                            lblStep10.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserSpouseFirstName + Constants.steps18 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                            lblStep12.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            Label5.Visible = false;
                            lblStep8.Visible = true;
                            lblStep9.Visible = true;
                            lblStep10.Visible = true;
                            lblStep11.Visible = true;
                            lblStep12.Visible = true;
                        }
                        else
                        {
                            lblStep10.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserSpouseFirstName + Constants.steps18 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                            lblStep11.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            Label5.Visible = false;
                            lblStep8.Visible = true;
                            lblStep9.Visible = true;
                            lblStep10.Visible = true;
                            lblStep11.Visible = true;
                            lblStep12.Visible = false;
                        }
                    }
                    else
                    {
                        if (Globals.Instance.BoolSpousalChangeAtAge70 && ValueForWifeAt70 != 0)
                        {
                            if (Globals.Instance.BoolWHBSwitchedHusb)
                                lblStep10.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months + ", " + strUserFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            else
                                lblStep10.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + AgeValueForSpousalAt70 + ", " + strUserFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            lblStep11.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            Label5.Visible = false;
                            lblStep8.Visible = true;
                            lblStep9.Visible = true;
                            lblStep10.Visible = true;
                            lblStep11.Visible = true;
                            lblStep12.Visible = false;
                        }
                        else
                        {
                            lblStep10.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            Label5.Visible = false;
                            lblStep8.Visible = true;
                            lblStep9.Visible = true;
                            lblStep10.Visible = true;
                            lblStep11.Visible = false;
                            lblStep12.Visible = false;
                        }
                        if (intWifeFRA.ToString() == Constants.Number70)
                            lblStep9.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        else
                            lblStep9.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.Age70Text);
                    }
                    // }
                }
                else
                {
                    //if (Globals.Instance.ShowAsterikForTwoStrategies)
                    //{
                    //    Label5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ClaimAndSuspendinFullAge + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + Constants.CliamAndSuspendHusb + Constants.ClaimandSuspendWarning + strUserFirstName + Constants.CliamAndSuspend1 + strUserSpouseFirstName + Constants.CliamAndSuspend2);
                    //    lblStep8.Text = string.Format(Constants.Line2 + Constants.Age + " " + ClaimAndSuspendinFullAge + Constants.when + strUserSpouseFirstName + Constants.steps8 + ClaimAndSuspendinFullAge.Substring(0, 2) + Constants.steps1 + ClaimAndSuspendinFullValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.MarriedSpousalText);
                    //    if (startingValueWife != ValueForWife70)
                    //    {
                    //        lblStep9.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + AgeForWife70 + Constants.steps18 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                    //        lblStep10.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps9 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    //        lblStep11.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    //        lblStep8.Visible = true;
                    //        Label5.Visible = true;
                    //        lblStep9.Visible = true;
                    //        lblStep10.Visible = true;
                    //        lblStep11.Visible = true;
                    //    }
                    //    else
                    //    {
                    //        lblStep9.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps9 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    //        lblStep10.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    //        Label5.Visible = true;
                    //        lblStep9.Visible = true;
                    //        lblStep10.Visible = true;
                    //        lblStep11.Visible = false;
                    //    }
                    //    Globals.Instance.CurrWifeValue = false;
                    //    Globals.Instance.CurrWifeValuePDF = true;
                    //    Globals.Instance.ShowAsterikForTwoStrategies = false;
                    //}
                    //else
                    //{
                    if (intHusbFRA.ToString() != Constants.Number70)
                    {
                        if (sHusbFRA.Substring(0, 2).Equals((intHusbFRA.ToString())))
                        {
                            strStartAgeForUnClaimed = sHusbFRA;
                            lblStep9.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                        }
                        else
                        {
                            strStartAgeForUnClaimed = strageHusb;
                            lblStep9.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                        }
                        boolRestrictIncomHusb = true;
                    }
                    else
                    {
                        if (Globals.Instance.BoolSpousalCliamedWifeSt1)
                            lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        else
                            lblStep9.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    }

                    if (startingValueHusb != ValueForHusb70)
                    {
                        if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                            lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.ReducedWorkBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                            lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        else
                            lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        if (Globals.Instance.BoolSpousalChangeAtAge70)
                        {
                            if (ValueForWifeAt70 != 0)
                            {
                                if (Globals.Instance.BoolWHBSwitchedWife)
                                    lblStep11.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months + ", " + strUserSpouseFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                else
                                    lblStep11.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + AgeValueForSpousalAt70 + ", " + strUserSpouseFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            }
                            lblStep10.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps18 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                            lblStep12.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                        else
                        {
                            lblStep10.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps18 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                            lblStep11.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                    }
                    else
                    {
                        if (Globals.Instance.BoolSpousalChangeAtAge70 && ValueForWifeAt70 != 0)
                        {
                            if (Globals.Instance.BoolWHBSwitchedHusb)
                                lblStep10.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months + ", " + strUserFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            else
                                lblStep10.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + AgeValueForSpousalAt70 + ", " + strUserFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            //lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps18 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                            lblStep11.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                        else
                        {
                            lblStep10.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                        if (intWifeFRA.ToString() == Constants.Number70)
                        {
                            lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps18 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        }
                        else
                        {
                            if (AgeValueForSpousalAt70.ToString() != intWifeFRA.ToString())
                            {
                                if (Globals.Instance.BoolSpousalCliamedWifeSt1)
                                {
                                    if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                        lblStep9.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.ReducedSpousalBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                    else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                        lblStep9.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                    else
                                        lblStep9.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                }
                                else
                                {
                                    if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                        lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.ReducedWorkBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                    else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                        lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                    else
                                        lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                }
                            }
                            else
                            {

                                if (Globals.Instance.BoolWHBSwitchedWife)
                                    lblStep9.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months + ", " + strUserSpouseFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                else
                                    lblStep9.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + AgeValueForSpousalAt70 + ", " + strUserSpouseFirstName + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                                lblStep10.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStep11.Text = string.Empty;

                            }
                        }
                        //}
                        Label5.Visible = false;
                        lblStep8.Visible = true;
                        lblStep9.Visible = true;
                        lblStep10.Visible = true;
                        lblStep11.Visible = true;
                        lblStep12.Visible = true;
                    }
                }
                //Decide Restricted Aplliacation Income Value 
                if (boolRestrictIncomWife)
                {
                    Globals.Instance.IntShowRestrictIncomeForEarly = Globals.Instance.intRestrictAppIncomeEarlyWife;
                    lblKeyRestrictIncomEarly.Text = Globals.Instance.intRestrictAppIncomeEarlyWife.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                    lblKeyRestrictIncomEarly1.Text = Constants.RestrictExplainNew1 + strUserSpouseFirstName + Constants.RestrictExplainNew2 + strUserSpouseFirstName + Constants.RestrictExplainNew3 + strStartAgeForUnClaimed + Constants.RestrictExplainNew4 + Globals.Instance.intRestrictAppIncomeEarlyWife.ToString(Constants.AMT_FORMAT) + Constants.RestrictExplainNew5;
                    calc.DesignGreeBoxForRestrictStrategy(GridEarly, strStartAgeForUnClaimed, "Wife");
                }
                else if (boolRestrictIncomHusb)
                {
                    Globals.Instance.IntShowRestrictIncomeForEarly = Globals.Instance.intRestrictAppIncomeEarlyHusb;
                    lblKeyRestrictIncomEarly.Text = Globals.Instance.intRestrictAppIncomeEarlyHusb.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                    lblKeyRestrictIncomEarly1.Text = Constants.RestrictExplainNew1 + strUserFirstName + Constants.RestrictExplainNew2 + strUserFirstName + Constants.RestrictExplainNew3 + strStartAgeForUnClaimed + Constants.RestrictExplainNew4 + Globals.Instance.intRestrictAppIncomeEarlyHusb.ToString(Constants.AMT_FORMAT) + Constants.RestrictExplainNew5;
                    calc.DesignGreeBoxForRestrictStrategy(GridEarly, strStartAgeForUnClaimed, "Husband");
                }


                //Set Keys and Steps values
                SetKeyAndStepsValuesForPDF("GridEarly");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to print the keys and steps to the grid bestlater ie strategy Full retirement
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        public void renderKeysIntoGridFull(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, string ClaimAndSuspendinFullAge, decimal ClaimAndSuspendinFullValue, string CombinedIncomeAge, string AgeForHusb70, decimal ValueForHusb70, string AgeForWife70, decimal ValueForWife70, decimal AgeValueForSpousalAt70, decimal ValueForWifeAt70)
        {
            try
            {
                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName, sWifeFRA, sHusbFRA = string.Empty;
                string strColumnName = string.Empty, strStartAgeForUnClaimed = string.Empty;
                string strHusbStep = string.Empty, strWifeStep = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                DateTime HusbDOB = DateTime.Parse(BirthHusbandYYYY.Text.ToString());
                DateTime WifeDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                DateTime limit = DateTime.Parse("05/01/1950");
                DateTime limit1 = DateTime.Parse("01/01/1954");
                sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strDoubleAsteriskIncludes = string.Empty;
                bool boolRestrictIcomeWife = false, boolRestrictIncomHusb = false;
                if (Globals.Instance.WifeNumberofMonthinNote > 0)
                    strageWife = Globals.Instance.WifeNumberofYearsAbove66 + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                else
                    strageWife = ageWife.Substring(0, 2);
                if (Globals.Instance.HusbandNumberofMonthsinNote > 0)
                    strageHusb = Globals.Instance.HusbNumberofYearsAbove66 + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                else
                    strageHusb = ageHusb.Substring(3, 2);
                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    strName = strUserSpouseFirstName;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnName = Constants.keyFigures13;
                }
                //to check if he cummulative income is not zero
                if (cumm69 != 0)
                {
                    lblKey7.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + combineBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
                    lblKey7.Visible = true;
                }
                else
                {
                    lblKey7.Visible = false;
                }
                if (combineBenefits != 0)
                    lblKey8.Text = string.Format(combineBenefits.ToString(Constants.AMT_FORMAT) + strColumnName + Constants.steps7 + strName + Constants.keyFigures8);
                else
                    lblKey8.Text = string.Empty;
                if (valueAtAge70 != 0)
                    lblKey9.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);
                else
                    lblKey9.Text = string.Empty;
                //notes for husband and wife
                if (Globals.Instance.BoolHusbandAgeAdjusted)
                {
                    if (Globals.Instance.BoolSpousalWHBat70 && Globals.Instance.HusbAnnualIcomeAt70 != 0 && Globals.Instance.HusbAnnualIcomeAt69 != 0)//husband age is smaller than 70 when spouse is age 66 (FRA) // Added on 29/09/2016
                        lblNoteHusband5.Text = "# includes " + Globals.Instance.HusbSpousalBenMonthAt69 + Constants.strSpousal_WHB1 + Globals.Instance.HusbAnnualIcomeAt69.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB2 + Globals.Instance.HusbAnnualIcomeAt70.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB3 + strUserFirstName;
                    else
                        lblNoteHusband5.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                }
                if (Globals.Instance.BoolWifeAgeAdjusted)
                    lblNoteWife5.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                    lblHusbandExplanation3.Text = Constants.includes + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                    lblWifeExplanation3.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //notes for husband and wife
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblHusbandExplanation3.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblNoteHusband4.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                }
                else if (Globals.Instance.BoolWHBSwitchedWife)
                {
                    lblWifeExplanation3.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                    lblNoteWife4.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                }

                //Update the explanation below grid if user taking his/her both the benefits in same year where annual income value attached
                //with "#*" updated on (17/01/2017)
                if (Globals.Instance.BoolContainsHashStar)
                {
                    if (Globals.Instance.BoolWHBSwitchedWife)
                    {
                        lblNoteWife5.Text = string.Empty;
                        lblWifeExplanation3.Text = "# includes " + (Globals.Instance.WifeNumberofMonthinNote - (Globals.Instance.WifeNumberofMonthsBelowGridAgePrior - 12)) + Constants.strWHBNote + strUserSpouseFirstName; ;

                    }
                    else if (Globals.Instance.BoolWHBSwitchedHusb)
                    {
                        lblNoteHusband5.Text = string.Empty;
                        lblHusbandExplanation3.Text = "# includes " + (Globals.Instance.HusbandNumberofMonthsinNote - (Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior - 12)) + Constants.strWHBNote + strUserFirstName; ;

                    }
                    Globals.Instance.BoolContainsHashStar = false;
                }

                #region when husband cliams WHB and then spousal benefits
                if (Globals.Instance.BoolSpousalCliamedHusb && Globals.Instance.HusbStartingBenefitName.Equals(Constants.Working))
                {
                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.Working))
                    {
                        if (intHusbFRA.ToString().Equals(Constants.Number70))
                        {
                            lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        }
                        else
                        {
                            if (intHusbFRA < int.Parse(sHusbFRA.Substring(0, 2)))
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.ReducedWorkBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                            else if (intHusbFRA == int.Parse(sHusbFRA.Substring(0, 2)))
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                            else
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                        }
                    }
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal))
                    {
                        if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife && WifeDOB < limit1)
                        {
                            Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                            boolRestrictIcomeWife = true;
                            strStartAgeForUnClaimed = strageWife;
                        }
                        else
                        {
                            if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal))
                            {
                                if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.ReducedSpousalBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                else
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            }
                        }
                    }
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife)
                    {
                        lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps18 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeWife)
                            lblSteps1.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps23 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    }
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife && Globals.Instance.BoolSpousalCliamedHusb)
                    {
                        if (Globals.Instance.BoolWHBSwitchedHusb)
                            lblSteps2.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months + Constants.steps1 + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                        else
                            lblSteps2.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + AgeValueForSpousalAt70 + ", " + strUserFirstName + Constants.steps1 + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                        lblSteps3.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    }
                    else
                    {
                        if (Globals.Instance.BoolSpousalCliamedHusb)
                        {
                            if (Globals.Instance.BoolWHBSwitchedHusb)
                                lblSteps2.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months + Constants.steps1 + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                            else
                                lblSteps2.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + AgeValueForSpousalAt70 + ", " + strUserFirstName + Constants.steps1 + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                        }
                        lblSteps3.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    }
                }
                #endregion when husband cliams WHB and then spousal benefits

                #region when wife cliams WHB and then spousal benefits
                else if (Globals.Instance.BoolSpousalCliamedWife && Globals.Instance.WifeStartingBenefitName.Equals(Constants.Working))
                {
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.Working))
                    {
                        if (intWifeFRA.ToString().Equals(Constants.Number70))
                        {
                            lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                        }
                        else
                        {
                            if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.ReducedWorkBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            else
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        }
                    }
                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal))
                    {
                        if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb && HusbDOB < limit1)
                        {
                            Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                            boolRestrictIncomHusb = true;
                            strStartAgeForUnClaimed = strageHusb;
                        }
                        else
                        {
                            if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal))
                            {
                                if (intHusbFRA < int.Parse(sHusbFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.ReducedSpousalBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                else if (intHusbFRA == int.Parse(sHusbFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                else
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                            }
                        }
                    }
                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb)
                    {
                        lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + ", " + strUserFirstName + Constants.steps18 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeHusb)
                            lblSteps1.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + ", " + strUserFirstName + Constants.steps23 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);

                    }
                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb && Globals.Instance.BoolSpousalCliamedWife)
                    {
                        if (Globals.Instance.BoolWHBSwitchedWife)
                            lblSteps2.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months + ", " + strUserSpouseFirstName + Constants.steps1 + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                        else
                            lblSteps2.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + AgeValueForSpousalAt70 + ", " + strUserSpouseFirstName + Constants.steps1 + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                        lblSteps3.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    }
                    else
                    {
                        if (Globals.Instance.BoolSpousalCliamedWife)
                        {
                            if (Globals.Instance.BoolWHBSwitchedWife)
                                lblSteps2.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months + ", " + strUserSpouseFirstName + Constants.steps1 + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            else
                                lblSteps2.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + AgeValueForSpousalAt70 + ", " + strUserSpouseFirstName + Constants.steps1 + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                        }
                        lblSteps3.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    }
                }
                #endregion when wife cliams WHB and then spousal benefits

                #region when husband cliams and suspends
                else if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb && !strageHusb.Equals(Constants.Number70))
                {
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.Working))
                    {
                        if (intWifeFRA.ToString().Equals(Constants.Number70))
                        {
                            lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                        }
                        else
                        {
                            if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.ReducedWorkBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            else
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        }
                    }
                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal))
                    {
                        if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb && HusbDOB < limit1)
                        {
                            Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                            boolRestrictIncomHusb = true;
                            strStartAgeForUnClaimed = strageHusb;
                        }
                        else
                        {
                            if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal))
                            {
                                if (intHusbFRA < int.Parse(sHusbFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.ReducedSpousalBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                else if (intHusbFRA == int.Parse(sHusbFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                else
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                            }
                        }
                    }
                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb)
                    {
                        lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + ", " + strUserFirstName + Constants.steps18 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeHusb)
                            lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + ", " + strUserFirstName + Constants.steps23 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    }
                    lblSteps2.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                #endregion when husband cliams and suspends

                #region when wife cliams and suspends
                else if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife && !strageWife.Equals(Constants.Number70))
                {
                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.Working))
                    {
                        if (intHusbFRA.ToString().Equals(Constants.Number70))
                        {
                            lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        }
                        else
                        {
                            if (intHusbFRA < int.Parse(sHusbFRA.Substring(0, 2)))
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.ReducedWorkBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                            else if (intHusbFRA == int.Parse(sHusbFRA.Substring(0, 2)))
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                            else
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                        }
                    }
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal))
                    {
                        if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife && WifeDOB < limit1)
                        {
                            Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                            boolRestrictIcomeWife = true;
                            strStartAgeForUnClaimed = strageWife;
                        }
                        else
                        {
                            if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal))
                            {
                                if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.ReducedSpousalBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                else
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            }
                        }
                    }
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife)
                    {
                        lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps18 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeWife)
                            lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps23 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    }
                    lblSteps2.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                #endregion when wife cliams and suspends

                #region when wife cliams sposual and husband working at age 70
                else if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && !Globals.Instance.BoolAge70ChangeWife && Globals.Instance.BoolAge70ChangeHusb)
                {
                    if (!ageForHigherEarnerAt70.Substring(3, 2).Equals(strageHusb) && Globals.Instance.BoolAge70ChangeHusb)
                    {
                        lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + ", " + strUserFirstName + Constants.steps18 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeHusb)
                            lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + ", " + strUserFirstName + Constants.steps23 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    }
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal))
                    {
                        if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife && WifeDOB < limit1)
                        {
                            Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                        }
                        else
                        {
                            if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal))
                            {
                                if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.ReducedSpousalBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);

                                else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                {
                                    if (Globals.Instance.WifeNumberofMonthinNote > 0)
                                        Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                    else
                                        Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                }
                                else
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            }
                        }
                    }
                    lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                #endregion when wife cliams sposual abd husband working at age 70

                #region when husb cliams sposual and wife working at age 70
                else if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && !Globals.Instance.BoolAge70ChangeHusb && Globals.Instance.BoolAge70ChangeWife)
                {
                    if (!ageForHigherEarnerAt70.Substring(0, 2).Equals(strageWife) && Globals.Instance.BoolAge70ChangeWife)
                    {
                        lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps18 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeWife)
                            lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps23 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    }

                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal))
                    {
                        if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb && HusbDOB < limit1)
                        {
                            Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                            boolRestrictIncomHusb = true;
                            strStartAgeForUnClaimed = strageHusb;

                        }
                        else
                        {
                            if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal))
                            {
                                if (intHusbFRA < int.Parse(sHusbFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.ReducedSpousalBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                else if (intHusbFRA == int.Parse(sHusbFRA.Substring(0, 2)))
                                {
                                    if (Globals.Instance.HusbandNumberofMonthsinNote > 0)
                                        Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                    else
                                        Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                }
                                else
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                            }
                        }
                    }
                    lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                #endregion when husb cliams sposual abd wife working at age 70

                #region when wife cliams working abd husb working at age 70
                else if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.Working) && !Globals.Instance.BoolAge70ChangeWife && Globals.Instance.BoolAge70ChangeHusb)
                {
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.Working))
                    {
                        if (intWifeFRA.ToString().Equals(Constants.Number70))
                        {
                            strWifeStep = string.Format(Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                        }
                        else
                        {
                            if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                strWifeStep = string.Format(Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.ReducedWorkBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                strWifeStep = string.Format(Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            else
                                strWifeStep = string.Format(Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        }
                    }
                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb)
                    {
                        if (ageForHigherEarnerAt70.Substring(3, 2).Equals(Constants.Number70))
                        {
                            strHusbStep = string.Format(Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        }
                        else
                        {
                            strHusbStep = string.Format(Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + ", " + strUserFirstName + Constants.steps18 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        }
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeHusb)
                            strHusbStep = string.Format(Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + ", " + strUserFirstName + Constants.steps23 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    }

                    //Set step sequence on the basis of age
                    if (intWifeFRA < int.Parse(ageForHigherEarnerAt70.Substring(0, 2)))
                    {
                        lblSteps0.Text = string.Format(Constants.Line1 + strWifeStep);
                        Label6.Text = string.Format(Constants.Line2 + strHusbStep);
                    }
                    else
                    {
                        lblSteps0.Text = string.Format(Constants.Line1 + strHusbStep);
                        Label6.Text = string.Format(Constants.Line2 + strWifeStep);
                    }
                    lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                #endregion when wife cliams working abd husb working at age 70

                #region when husband cliams working abd wife working at age 70
                else if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.Working) && !Globals.Instance.BoolAge70ChangeHusb && Globals.Instance.BoolAge70ChangeWife)
                {

                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.Working))
                    {
                        if (intHusbFRA.ToString().Equals(Constants.Number70))
                        {
                            strHusbStep = Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text;
                        }
                        else
                        {
                            if (intHusbFRA < int.Parse(sHusbFRA.Substring(0, 2)))
                                strHusbStep = Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.ReducedWorkBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12;
                            else if (intHusbFRA == int.Parse(sHusbFRA.Substring(0, 2)))
                                strHusbStep = Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12;
                            else
                                strHusbStep = Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12;
                        }
                    }
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife)
                    {
                        strWifeStep = Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps18 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text;
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeWife)
                            strWifeStep = Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps23 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text;
                    }

                    //Set step sequence on the basis of age
                    if (int.Parse(ageHusb.Substring(0, 2)) < int.Parse(ageForHigherEarnerAt70.Substring(0, 2)))
                    {
                        lblSteps0.Text = string.Format(Constants.Line1 + strHusbStep);
                        Label6.Text = string.Format(Constants.Line2 + strWifeStep);
                    }
                    else
                    {
                        lblSteps0.Text = string.Format(Constants.Line1 + strWifeStep);
                        Label6.Text = string.Format(Constants.Line2 + strHusbStep);
                    }
                    lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                #endregion when husband cliams working and wife working at age 70

                #region when husband and wife working at age 70
                else if (Globals.Instance.BoolAge70StartingAgeWife)
                {
                    if (Globals.Instance.BoolAge70StartingAgeHusb)
                    {

                        if (intWifeFRA > intHusbFRA)
                        {
                            if (ValueForHusb70 > ValueForWife70)
                            {
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                                Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                            }
                            else
                            {
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.Age70Text);
                                Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                            }
                        }
                        else
                        {
                            if (ValueForHusb70 > ValueForWife70)
                            {
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                                Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                            }
                            else
                            {
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                                Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.Age70Text);
                            }

                        }
                        lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeWife)
                        {
                            lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                        }
                        if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb && HusbDOB < limit1)
                        {
                            Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                            boolRestrictIncomHusb = true;
                            strStartAgeForUnClaimed = strageHusb;
                        }
                        else
                        {
                            if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal))
                            {
                                if (intHusbFRA < int.Parse(sHusbFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.ReducedSpousalBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                else if (intHusbFRA == int.Parse(sHusbFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                else
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + ", " + strUserFirstName + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                            }
                        }
                        if (Globals.Instance.BoolAge70ChangeHusb)
                        {
                            lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                            lblSteps2.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                        else
                        {
                            lblSteps2.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                    }
                }

                else if (Globals.Instance.BoolAge70StartingAgeHusb)
                {
                    if (Globals.Instance.BoolAge70StartingAgeWife)
                    {
                        if (intWifeFRA > intHusbFRA)
                        {
                            if (ValueForHusb70 > ValueForWife70)
                            {
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                                Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                            }
                            else
                            {
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.Age70Text);
                                Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                            }
                        }
                        else
                        {
                            if (ValueForHusb70 > ValueForWife70)
                            {
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                                Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                            }
                            else
                            {
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                                Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.Age70Text);
                            }

                        }
                        lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    }
                    else
                    {

                        if (Globals.Instance.BoolAge70ChangeHusb)
                        {
                            lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + ", " + strUserFirstName + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        }
                        if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal))
                        {
                            if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife && WifeDOB < limit1)
                            {
                                lblSteps1.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                                boolRestrictIcomeWife = true;
                                strStartAgeForUnClaimed = strageWife;
                            }
                            else
                            {
                                if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal))
                                {
                                    if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                        lblSteps1.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.ReducedSpousalBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                    else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                        lblSteps1.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserSpouseFirstName + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                    else
                                        lblSteps1.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + ", " + strUserSpouseFirstName + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                }
                            }
                        }
                        if (Globals.Instance.BoolAge70ChangeWife)
                        {
                            lblSteps2.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + ", " + strUserSpouseFirstName + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                            lblSteps3.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                        else
                        {
                            lblSteps3.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                    }
                }
                #endregion when husband and wife working at age 70

                if (boolRestrictIcomeWife)
                {
                    Globals.Instance.IntShowRestrictIncomeForFull = Globals.Instance.intRestrictAppIncomeFullWife;
                    lblKeyRestrictIncomFull.Text = Globals.Instance.intRestrictAppIncomeFullWife.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                    lblKeyRestrictIncomFull1.Text = Constants.RestrictExplainNew1 + strUserSpouseFirstName + Constants.RestrictExplainNew2 + strUserSpouseFirstName + Constants.RestrictExplainNew3 + strStartAgeForUnClaimed + Constants.RestrictExplainNew4 + Globals.Instance.intRestrictAppIncomeFullWife.ToString(Constants.AMT_FORMAT) + Constants.RestrictExplainNew5;

                    calc.DesignGreeBoxForRestrictStrategy(GridBestLater, strStartAgeForUnClaimed, "Wife");
                }
                else if (boolRestrictIncomHusb)
                {
                    Globals.Instance.IntShowRestrictIncomeForFull = Globals.Instance.intRestrictAppIncomeFullHusb;
                    lblKeyRestrictIncomFull.Text = Globals.Instance.intRestrictAppIncomeFullHusb.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                    lblKeyRestrictIncomFull1.Text = Constants.RestrictExplainNew1 + strUserFirstName + Constants.RestrictExplainNew2 + strUserFirstName + Constants.RestrictExplainNew3 + strStartAgeForUnClaimed + Constants.RestrictExplainNew4 + Globals.Instance.intRestrictAppIncomeFullHusb.ToString(Constants.AMT_FORMAT) + Constants.RestrictExplainNew5;

                    calc.DesignGreeBoxForRestrictStrategy(GridBestLater, strStartAgeForUnClaimed, "Husband");

                }
                //Set Keys and Steps values
                SetKeyAndStepsValuesForPDF("Full");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to print the keys and steps to the grid suspendLater ie strategy Claim and Suspend
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        /// <param name="AgeValueForWifeAt70"></param>
        /// <param name="BenefitsValueForWifeAt70"></param>
        /// <param name="AgeWhenHigherEarnerClaimandSuspend"></param>
        //public void renderKeysIntoGridSuspend(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, string AgeValueForWifeAt70, decimal BenefitsValueForWifeAt70, string AgeWhenHigherEarnerClaimandSuspend, string CombinedIncomeAge)
        //{
        //    try
        //    {
        //        Calc calc = new Calc();
        //        Customers customer = new Customers();
        //        string strName = string.Empty;
        //        string strColumnName = string.Empty;
        //        string strageHusb = ageHusb.Substring(3, 2);
        //        string strageWife = ageWife.Substring(0, 2);
        //        string sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
        //        string sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
        //        int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
        //        int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
        //        //to set the name and column name of the higher earner in the keys
        //        if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
        //        {
        //            strName = strUserSpouseFirstName;
        //            strColumnName = Constants.keyFigures14;
        //        }
        //        else
        //        {
        //            strName = strUserFirstName;
        //            strColumnName = Constants.keyFigures13;
        //        }
        //        //to check if the cumulative value at age 69 of higher earner is zero or not
        //        if (cumm69 != 0)
        //        {
        //            lblSuspendKey1.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + combineBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
        //            lblSuspendKey1.Visible = true;
        //        }
        //        else
        //        {
        //            lblSuspendKey1.Visible = false;
        //        }
        //        //to set the value for higher earners age 70 value
        //        if (combineBenefits != 0)
        //            lblSuspendKey2.Text = string.Format(combineBenefits.ToString(Constants.AMT_FORMAT) + strColumnName + Constants.steps7 + strName + Constants.keyFigures8);
        //        else
        //            lblSuspendKey2.Text = string.Empty;
        //        //to set the commbined income value in keys
        //        if (valueAtAge70 != 0)
        //            lblSuspendKey3.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);
        //        else
        //            lblSuspendKey3.Text = string.Empty;
        //        //to set the steps according to the benefits entered
        //        #region code when husband benefits are greater
        //        if (Convert.ToDecimal(BeneHusband.Text) > Convert.ToDecimal(BeneWife.Text))
        //        {
        //            //to check if the higher earner is claiming and suspending or not
        //            if (!(AgeWhenHigherEarnerClaimandSuspend.Equals(string.Empty)))
        //            {
        //                //to set the claim and suspend age of higher earner
        //                lblStepsSuspend1.Text = string.Format(Constants.Line1 + Constants.Age + " " + AgeWhenHigherEarnerClaimandSuspend + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + Constants.CliamAndSuspendHusb + Constants.ClaimandSuspendWarning + Constants.CliamAndSuspend3 + strUserFirstName + Constants.CliamAndSuspend1 + strUserFirstName + Constants.CliamAndSuspend4 + Constants.CliamAndSuspend5 + strUserSpouseFirstName + Constants.CliamAndSuspend2);
        //                //to get the sposual benfits for wife at the same age when the higher earner claim and suspend
        //                if (Globals.Instance.Age70ChangeForWife)
        //                {
        //                    if (!ageWife.Substring(0, 2).Equals(Constants.Number70))
        //                    {
        //                        if (ageWife.Substring(0, 2).Equals(intWifeFRA.ToString()))
        //                            lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps21 + Constants.While + strUserSpouseFirstName + Constants.CliamAndSuspend6 + strUserSpouseFirstName + Constants.CliamAndSuspend7);
        //                        else
        //                            lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps21 + Constants.While + strUserSpouseFirstName + Constants.CliamAndSuspend6 + strUserSpouseFirstName + Constants.CliamAndSuspend7);
        //                    }
        //                    else
        //                    {
        //                        lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
        //                    }
        //                }
        //                else
        //                {
        //                    if (!ageWife.Substring(0, 2).Equals(Constants.Number70))
        //                    {
        //                        if (ageWife.Substring(0, 2).Equals(intWifeFRA.ToString()))
        //                            lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.MarriedSpousalText + Constants.While + strUserSpouseFirstName + Constants.CliamAndSuspend6 + strUserSpouseFirstName + Constants.CliamAndSuspend7);
        //                        else
        //                            lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.MarriedSpousalText + Constants.While + strUserSpouseFirstName + Constants.CliamAndSuspend6 + strUserSpouseFirstName + Constants.CliamAndSuspend7);
        //                    }
        //                }
        //                //to set the working benefits value for husband at age 70
        //                lblStepsSuspend3.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps23 + combineBenefits.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
        //                //to check if starting value for wife is same to the age 70 value
        //                if (Globals.Instance.Age70ChangeForWife)
        //                {
        //                    //if not same set the ae 70 value
        //                    lblStepsSuspend4.Text = string.Format(Constants.Line4 + Constants.Age + " " + AgeValueForWifeAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + AgeValueForWifeAt70.Substring(0, 2) + Constants.steps18 + BenefitsValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
        //                    lblStepsSuspend4.Visible = true;
        //                    lblStepsSuspend5.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
        //                }
        //                else
        //                {
        //                    lblStepsSuspend5.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
        //                    lblStepsSuspend4.Visible = false;
        //                }
        //            }
        //            else
        //            {
        //                //to check if the starting age of wife is same to the age 70
        //                if (ageWife.Substring(0, 2) != Constants.Number70)
        //                {
        //                    //if there is no suspending then we show starting value for wife
        //                    if (ageWife.Substring(0, 2).Equals(sWifeFRA.ToString()))
        //                    {
        //                        if (Globals.Instance.Age70ChangeForWife)
        //                            lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps21);
        //                        else
        //                            lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
        //                    }
        //                    else
        //                    {
        //                        if (Globals.Instance.Age70ChangeForWife)
        //                            lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps21);
        //                        else
        //                            lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
        //                    }
        //                }
        //                else
        //                {
        //                    lblStepsSuspend1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps23 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
        //                }
        //                //to check if the starting age of husband is same to the age 70
        //                if (ageHusb.Substring(3, 2) != Constants.Number70)
        //                {
        //                    //if there is no suspending then we show starting value for husb
        //                    if (ageHusb.Substring(3, 2).Equals(intHusbFRA.ToString()))
        //                        lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps21);
        //                    else
        //                        lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps21);
        //                }
        //                else
        //                {
        //                    lblStepsSuspend1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
        //                }
        //                if (intAgeHusb < intAgeWife)
        //                {
        //                    //if the starting value for wife is same as that of the age 70 value then sow the value
        //                    if (valueforHIgherEarnerAt70 != 0 && valueforHIgherEarnerAt70 != startingValueHusb)
        //                    {
        //                        lblStepsSuspend3.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + Constants.steps18 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12);
        //                        lblStepsSuspend5.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
        //                        lblStepsSuspend3.Visible = true;
        //                        lblStepsSuspend4.Visible = false;
        //                    }
        //                    else
        //                    {
        //                        lblStepsSuspend5.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
        //                        lblStepsSuspend3.Visible = false;
        //                        lblStepsSuspend4.Visible = false;
        //                    }
        //                }
        //                else
        //                {
        //                    if (valueforHIgherEarnerAt70 != 0 && valueforHIgherEarnerAt70 != startingValueWife)
        //                    {
        //                        if (BenefitsValueForWifeAt70 != 0)
        //                        {
        //                            lblStepsSuspend3.Text = string.Format(Constants.Line3 + Constants.Age + " " + AgeValueForWifeAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + AgeValueForWifeAt70.Substring(0, 2) + Constants.steps18 + BenefitsValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
        //                            lblStepsSuspend5.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
        //                        }
        //                        else
        //                        {
        //                            lblStepsSuspend5.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
        //                        }
        //                        lblStepsSuspend3.Visible = true;
        //                        lblStepsSuspend4.Visible = false;
        //                    }
        //                    else
        //                    {
        //                        lblStepsSuspend5.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
        //                        lblStepsSuspend3.Visible = false;
        //                        lblStepsSuspend4.Visible = false;
        //                    }
        //                }
        //            }
        //        }
        //        #endregion code when husband benefits are greater

        //        #region code when wife benefits are greater
        //        else
        //        {
        //            //to check if the higher earner is claiming and suspending or not
        //            if (!(AgeWhenHigherEarnerClaimandSuspend.Equals(string.Empty)))
        //            {
        //                //to set the claim and suspend age of higher earner          
        //                lblStepsSuspend1.Text = string.Format(Constants.Line1 + Constants.Age + " " + AgeWhenHigherEarnerClaimandSuspend + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.CliamAndSuspendWife + Constants.ClaimandSuspendWarning + Constants.CliamAndSuspend3 + strUserSpouseFirstName + Constants.CliamAndSuspend1 + strUserSpouseFirstName + Constants.CliamAndSuspend4 + Constants.CliamAndSuspend5 + strUserFirstName + Constants.CliamAndSuspend2);
        //                //to get the sposual benfits for husb at the same age when the higher earner claim and suspend
        //                if (!ageHusb.Substring(3, 2).Equals(Constants.Number70))
        //                {
        //                    if (Globals.Instance.Age70ChangeForHusb)
        //                    {
        //                        if (ageHusb.Substring(3, 2).Equals(intHusbFRA.ToString()))
        //                            lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps21 + Constants.While + strUserFirstName + Constants.CliamAndSuspend6 + strUserFirstName + Constants.CliamAndSuspend7);
        //                        else
        //                            lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps21 + Constants.While + strUserFirstName + Constants.CliamAndSuspend6 + strUserFirstName + Constants.CliamAndSuspend7);
        //                    }
        //                    else
        //                    {
        //                        if (ageHusb.Substring(3, 2).Equals(intHusbFRA.ToString()))
        //                            lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.MarriedSpousalText + Constants.While + strUserFirstName + Constants.CliamAndSuspend6 + strUserFirstName + Constants.CliamAndSuspend7);
        //                        else
        //                            lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.MarriedSpousalText + Constants.While + strUserFirstName + Constants.CliamAndSuspend6 + strUserFirstName + Constants.CliamAndSuspend7);
        //                    }
        //                }
        //                else
        //                {
        //                    lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
        //                }
        //                //to check if starting value for wife is same to the age 70 value
        //                if (Globals.Instance.Age70ChangeForHusb && BenefitsValueForWifeAt70 != startingValueHusb)
        //                {
        //                    //if not same set the ae 70 value
        //                    lblStepsSuspend3.Text = string.Format(Constants.Line3 + Constants.Age + " " + AgeValueForWifeAt70 + Constants.when + strUserFirstName + Constants.steps8 + AgeValueForWifeAt70.Substring(3, 2) + Constants.steps23 + BenefitsValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12);
        //                    lblStepsSuspend3.Visible = true;
        //                    //to set the working benefits value for husband at age 70
        //                    lblStepsSuspend4.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.ToString().Substring(0, 2) + Constants.steps9 + combineBenefits.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
        //                    lblStepsSuspend4.Visible = true;
        //                    lblStepsSuspend5.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
        //                }
        //                else
        //                {//to set the working benefits value for husband at age 70
        //                    lblStepsSuspend4.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.ToString().Substring(0, 2) + Constants.steps9 + combineBenefits.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
        //                    lblStepsSuspend4.Visible = true;
        //                    lblStepsSuspend5.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
        //                    lblStepsSuspend5.Visible = true;
        //                    lblStepsSuspend3.Visible = false;
        //                }
        //            }
        //            else
        //            {
        //                //to check if the starting age of wife is same to the age 70
        //                if (ageWife.Substring(0, 2) != Constants.Number70)
        //                {
        //                    //if there is no suspending then we show starting value for wife
        //                    if (ageWife.Substring(0, 2).Equals(intWifeFRA.ToString()))
        //                    {
        //                        if (Globals.Instance.Age70ChangeForWife)
        //                            lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps21);
        //                        else
        //                            lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
        //                    }
        //                    else
        //                    {
        //                        if (Globals.Instance.Age70ChangeForWife)
        //                            lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps21);
        //                        else
        //                            lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
        //                    }
        //                }
        //                else
        //                {
        //                    lblStepsSuspend1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps23 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
        //                }
        //                if (ageHusb.Substring(3, 2) != Constants.Number70)
        //                {   //if there is no suspending then we show starting value for husb
        //                    if (ageHusb.Substring(3, 2).Equals(intHusbFRA.ToString()))
        //                        lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps21);
        //                    else
        //                        lblStepsSuspend2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps21);
        //                }
        //                else
        //                {
        //                    lblStepsSuspend1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.Age70Text);
        //                }
        //                if (intAgeWife > intAgeHusb)
        //                {
        //                    //if the starting value for Wife is same as that of the age 70 value then show the value
        //                    if (valueforHIgherEarnerAt70 != 0 && valueforHIgherEarnerAt70 != startingValueHusb)
        //                    {
        //                        if (BenefitsValueForWifeAt70 != 0)
        //                        {
        //                            lblStepsSuspend3.Text = string.Format(Constants.Line3 + Constants.Age + " " + AgeValueForWifeAt70 + Constants.when + strUserFirstName + Constants.steps8 + AgeValueForWifeAt70.Substring(3, 2) + Constants.steps18 + BenefitsValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
        //                            lblStepsSuspend5.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);

        //                        }
        //                        else
        //                        {
        //                            lblStepsSuspend5.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
        //                        }
        //                        lblStepsSuspend3.Visible = true;
        //                        lblStepsSuspend4.Visible = false;
        //                    }
        //                    else
        //                    {
        //                        lblStepsSuspend5.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
        //                        lblStepsSuspend3.Visible = false;
        //                        lblStepsSuspend4.Visible = false;
        //                    }
        //                }
        //                else
        //                {
        //                    if (valueforHIgherEarnerAt70 != 0 && valueforHIgherEarnerAt70 != startingValueWife)
        //                    {
        //                        lblStepsSuspend3.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + Constants.steps18 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
        //                        lblStepsSuspend5.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
        //                        lblStepsSuspend3.Visible = true;
        //                        lblStepsSuspend4.Visible = false;
        //                    }
        //                    else
        //                    {
        //                        lblStepsSuspend5.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
        //                        lblStepsSuspend3.Visible = false;
        //                        lblStepsSuspend4.Visible = false;
        //                    }
        //                }
        //            }
        //        }
        //        #endregion code when wife benefits are greater
        //        //to set the combined income value for claim and suspend
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
        //        ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
        //        ErrorMessagelable.Visible = true;
        //    }
        //}

        /// <summary>
        /// code to export the explanation text pop up to PDF
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void exportToPdf(object sender, EventArgs e)
        {
            try
            {
                Response.ContentType = Constants.pdfContentType;
                Response.AddHeader(Constants.pdfContentDisposition, "attachment;filename=Explanation_For_Married_Strategies" + "_" + DateTime.Now.ToString(Constants.DateFormat) + Constants.pdfFileExtension);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Document pdfDoc = new Document(PageSize.A4, Constants.zero, Constants.zero, -15f, 55f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                writer.PageEvent = new PDFEvent();
                pdfDoc.Open();

                //Add strategy explanation content
                AddExplainationToPDF(ref pdfDoc);

                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Used to add strategy explaination to PDF
        /// </summary>
        /// <param name="pdfDoc">PDF Document</param>
        private void AddExplainationToPDF(ref Document pdfDoc)
        {
            try
            {
                #region Add titles
                pdfDoc.Add(SiteNew.generateParagraph(Constants.StrategyExpanationTitle, false, true, true));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.MarriedCouples, false, true, true));
                #endregion Add titles

                #region What is the Primary Goal of the Paid to Wait Calculator?
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion1, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion1Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What is the Primary Goal of the Paid to Wait Calculator?

                #region Many Married Couples Can Get Paid to Wait
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion2, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion2Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion Many Married Couples Can Get Paid to Wait

                #region Can All Married Couples Get Paid to Wait?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion3, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion3Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));

                #endregion Can All Married Couples Get Paid to Wait?

                #region How the Paid to Wait Calculator Works
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion4, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion4Answer1, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion4Answer2, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion4Answer3, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion4Answer4, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion How the Paid to Wait Calculator Works

                #region What if You Continue to Work?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion5, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion5Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What if You Continue to Work?

                #region What if You Want to Maximize the Size of Both Benefits?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion6, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion6Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What if You Want to Maximize the Size of Both Benefits?

                #region What You Can Expect
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion7, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion7Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What You Can Expect

                #region What are the “Key Benefit Numbers” in the Strategies
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion8, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion8Answer1, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion8Answer2, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion8Answer3, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What are the “Key Benefit Numbers” in the Strategies?";

                #region What are the “Next Steps” in the Strategies?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion9, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion9Answer1, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion9Answer2, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion9Answer3, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationMarriedQuestion9Answer4, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What are the “Next Steps” in the Strategies?

                #region Add Note for explanation
                pdfDoc.Add(SiteNew.generateParagraph(Constants.StrategyExplanationNote, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                #endregion Add Note for explanation

                #region Add Good Luck Text
                pdfDoc.Add(SiteNew.generateParagraph(Constants.GoodLuckText, false, true, true));
                #endregion Add Good Luck Text
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }

        }

        /// <summary>
        /// Used to add strategy instructions in pdf
        /// </summary>
        /// <param name="pdfDoc"></param>
        private void AddInstructionBeforeSeeStrategy(ref Document pdfDoc)
        {
            try
            {
                #region Add Instructions
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.StrategyInstruction, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion Add Instructions
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }
        struct Summ
        {
            public string description;
        }

        /// <summary>
        ///   Requested assumptions change
        /// </summary>
        protected void BackToParms()
        {
            try
            {
                PanelGrids.Visible = false;
                PanelEntry.Visible = true;
                PanelParms.Visible = false;
                BirthWifeMM.Focus();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// restting the values to null for the gloabal variables and lable texts
        /// </summary>
        public void ResetValues()
        {
            try
            {
                #region reset values
                Globals.Instance.BoolSpousalChange = false;
                Globals.Instance.BoolShowMaxStrategy = false;
                Globals.Instance.StringSpousalChangeAgeWife = false;
                Globals.Instance.WifeStartingBenefitName = string.Empty;
                Globals.Instance.HusbStartingBenefitName = string.Empty;
                Globals.Instance.BoolSpousalCliamedHusb = false;
                Globals.Instance.BoolSpousalCliamedWife = false;
                Globals.Instance.BoolAge70ChangeHusb = false;
                Globals.Instance.BoolAge70ChangeWife = false;
                Globals.Instance.BoolAge70StartingAgeHusb = false;
                Globals.Instance.BoolAge70StartingAgeWife = false;
                Globals.Instance.BoolHighlightCummAtWife69Max = false;
                Globals.Instance.BoolHusbandAgeAdjusted = false;
                Globals.Instance.BoolWifeAgeAdjusted = false;
                Globals.Instance.BoolWHBSwitchedHusb = false;
                Globals.Instance.BoolSpousalAfterWorkWife = false;
                Globals.Instance.BoolSpousalAfterWorkHusb = false;
                Globals.Instance.BoolWHBSwitchedWife = false;
                Globals.Instance.intRestrictAppIncomeBestHusb = 0;
                Globals.Instance.intRestrictAppIncomeBestWife = 0;
                Globals.Instance.intRestrictAppIncomeMaxWife = 0;
                Globals.Instance.intRestrictAppIncomeMaxHusb = 0;
                Globals.Instance.intRestrictAppIncomeFullWife = 0;
                Globals.Instance.intRestrictAppIncomeFullHusb = 0;
                Globals.Instance.intRestrictAppIncomeEarlyWife = 0;
                Globals.Instance.intRestrictAppIncomeEarlyHusb = 0;
                Globals.Instance.IntShowRestrictIncomeForFull = 0;
                Globals.Instance.IntShowRestrictIncomeForMax = 0;
                Globals.Instance.IntShowRestrictIncomeForBest = 0;
                Globals.Instance.IntShowRestrictIncomeForEarly = 0;
                Globals.Instance.CurrHusValue = false;
                Globals.Instance.BoolHideSecondStrat = false;
                Globals.Instance.BoolWHBWifeat70 = false;
                Globals.Instance.BoolWHBHusbat70 = true;

                //Make keys empty
                lblKey1.Text = string.Empty;
                lblKey2.Text = string.Empty;
                lblKey3.Text = string.Empty;
                lblKey4.Text = string.Empty;
                lblKey5.Text = string.Empty;
                lblKey6.Text = string.Empty;
                lblKey7.Text = string.Empty;
                lblKey8.Text = string.Empty;
                lblKey9.Text = string.Empty;
                lblKeys1.Text = string.Empty;
                lblKeys2.Text = string.Empty;
                lblKeys3.Text = string.Empty;
                //Make steps empty
                lblSteps0.Text = string.Empty;
                lblSteps1.Text = string.Empty;
                lblSteps2.Text = string.Empty;
                lblSteps3.Text = string.Empty;
                lblStep10.Text = string.Empty;
                lblStep11.Text = string.Empty;
                lblStep12.Text = string.Empty;
                lblStep13.Text = string.Empty;
                lblSteps14.Text = string.Empty;
                lblSteps1.Text = string.Empty;
                lblStep1.Text = string.Empty;
                lblStep2.Text = string.Empty;
                lblStep3.Text = string.Empty;
                lblStep4.Text = string.Empty;
                lblStep6.Text = string.Empty;
                lblStep7.Text = string.Empty;
                lblStep8.Text = string.Empty;
                lblStep9.Text = string.Empty;
                lblStep10.Text = string.Empty;
                lblStep11.Text = string.Empty;
                lblStep12.Text = string.Empty;
                lblStep13.Text = string.Empty;
                lblStepsSuspend1.Text = string.Empty;
                lblStepsSuspend2.Text = string.Empty;
                lblStepsSuspend3.Text = string.Empty;
                lblStepsSuspend4.Text = string.Empty;
                lblStepsSuspend5.Text = string.Empty;
                lblSuspendKey1.Text = string.Empty;
                lblSuspendKey2.Text = string.Empty;
                lblSuspendKey3.Text = string.Empty;

                #endregion  reset values
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }

        }


        /// <summary>
        /// Naviaget to property page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NavigateToReportPage(object sender, EventArgs e)
        {
            try
            {

                decimal deciHusb = 0, deciWife = 0;

                RestrictAppIncomeProp.Instance.BoolRestrictNewCase = false;

                Globals.Instance.BoolIsShowRestrictFull = false;
                RestrictAppIncomeProp.Instance.intHusbandClaimAge = 0;
                RestrictAppIncomeProp.Instance.intWifeClaimAge = 0;
                RestrictAppIncomeProp.Instance.intHusbClaimBenefit = 0;
                RestrictAppIncomeProp.Instance.intWifeClaimBenefit = 0;
                DateTime dtLimit = new DateTime(1954, 01, 02);
                //Decimal decBeneHusb = Convert.ToDecimal(txtHusbCurrBen.Text);
                //Decimal decBeneWife = Convert.ToDecimal(txtWifeCurrBen.Text);
                //Session["FBUserBDate"] = SelectedDate_Your.ToString();
                //Session["FBSpouseDate"] = SelectedDate_Spouse.ToString();
                //Session[Constants.SessionRole] = "FBUser";
                //Session["FBUsername"] = txtFirstname.Text;
                //Session["FBSpousename"] = txtSpouseFirstname.Text;

                //if (rdbtnMarried.Checked)
                //    Session["FBUserBenefit"] = txtHusbCurrBen.Text;
                //else
                //    Session["FBUserBenefit"] = txtUserBenefit.Text;

                //if (rdbtnMarried.Checked)
                //    Session["FBSpouseBenefit"] = txtWifeCurrBen.Text;
                //else
                //    Session["FBSpouseBenefit"] = txtSpouseBenefit.Text;

                //Session["FBUserClaimedAge"] =  ddlHusbClaimAge.Text;
                //Session["FBSpouseClaimedAge"] = ddlWifeClaimAge.Text;
                //Session["FBUserClaimedBenefit"] = txtHusbCliamBenefit.Text;
                //Session["FBSpouseClaimedBenefit"] = txtWifeCliamBenefit.Text;


                //DateTime UserDob = Convert.ToDateTime(SelectedDate_Your.ToString());
                //DateTime SpouseDob = Convert.ToDateTime(SelectedDate_Spouse.ToString());

                Customers customer = new Customers();
                DateTime wifeBirthDate, husbandBirthDate;
                DataTable dtCustomerDetails = new DataTable();
                if (Session["AdvisorCustomer"] != null)
                    dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                else
                    dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());

                wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                //BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                //BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                //birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                //BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                //BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                //BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                //BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                //BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                string WifeBene = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                string HusbBene = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();

                deciHusb = Convert.ToDecimal(HusbBene);
                deciWife = Convert.ToDecimal(WifeBene);

                Session["FBSpouseBenefit"] = WifeBene;
                Session["FBUserBenefit"] = HusbBene;
                Session["FBSpouseDate"] = wifeBirthDate.ToString();
                Session["FBUserBDate"] = husbandBirthDate.ToString();



                DateTime UserDob = husbandBirthDate;// new DateTime(birthYearHusb, int.Parse(BirthHusbandMM.Text), int.Parse(BirthHusbandDD.Text));
                DateTime SpouseDob = wifeBirthDate;//new DateTime(birthYearWife, int.Parse(BirthWifeMM.Text), int.Parse(BirthWifeDD.Text));

                //if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                //{
                //    if (!string.IsNullOrEmpty(HusbBene))
                //        RestrictAppIncomeProp.Instance.intHusbCurrentBenefit = Convert.ToInt32(HusbBene);
                //    RestrictAppIncomeProp.Instance.intHusbandClaimAge = Convert.ToInt32(Session["FBUserClaimedAge"]);
                //    if (Session["FBUserClaimedBenefit"] != null)
                //        RestrictAppIncomeProp.Instance.intHusbClaimBenefit = Convert.ToInt32(Session["FBUserClaimedBenefit"]);
                //}
                //if (Globals.Instance.BoolIsWifeClaimedBenefit)
                //{
                //    if (!string.IsNullOrEmpty(WifeBene))
                //        RestrictAppIncomeProp.Instance.intWifeCurrentBenefit = Convert.ToInt32(WifeBene);
                //    RestrictAppIncomeProp.Instance.intWifeClaimAge = Convert.ToInt32(Session["FBSpouseClaimedAge"]);
                //    if (Session["FBSpouseClaimedBenefit"] != null)
                //        RestrictAppIncomeProp.Instance.intWifeClaimBenefit = Convert.ToInt32(Session["FBSpouseClaimedBenefit"]);
                //}

                //Determine show/hide full strategy
                if (Globals.Instance.BoolIsHusbandClaimedBenefit || Globals.Instance.BoolIsWifeClaimedBenefit)
                {
                    if (!Globals.Instance.BoolIsHusbandClaimedBenefit)
                    {
                        if (UserDob < dtLimit)
                            Globals.Instance.BoolIsShowRestrictFull = true;
                    }
                    else if (!Globals.Instance.BoolIsWifeClaimedBenefit)
                    {
                        if (SpouseDob < dtLimit)
                            Globals.Instance.BoolIsShowRestrictFull = true;
                    }
                    RestrictAppIncomeProp.Instance.BoolRestrictSpecialCase = false;
                    Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                }
                else
                {

                    if (((deciHusb >= deciWife && UserDob > dtLimit && SpouseDob < dtLimit) ||
                        (deciHusb < deciWife && SpouseDob > dtLimit && UserDob < dtLimit)))
                    {
                        RestrictAppIncomeProp.Instance.BoolRestrictSpecialCase = true;
                        Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                    }
                    else
                    {
                        //Redirect to new strategies of 62 and 66 mentioned on 24/02/2017
                        //Strategy Explaination
                        //Strategy-1 : Lower Benefit spouse will claim WHB at his age 62.. Higher benefit spouse will claim Maxed out WHB at 70.. Lower benefit will switches to spousal benefit when Higher claims Maxed Out WHB at 70
                        //Strategy-2 : Lower Benefit spouse will claim WHB at his age 66.. Higher benefit spouse will claim Maxed out WHB at 70.. Lower benefit will switches to spousal benefit when Higher claims Maxed Out WHB at 70



                        RestrictAppIncomeProp.Instance.BoolRestrictNewCase = true;
                        Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRestrictAppUserInfo, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRestrictAppUserInfo + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;

            }
        }


        #endregion
    }
}
