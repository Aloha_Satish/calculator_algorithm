﻿using CalculateDLL;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Symbolics;
using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Reflection;
using System.Configuration;
using System.Collections.Generic;
using CalculateDLL.TO;
using System.Linq;
using Calculate.RestrictAppIncome;

namespace Calculate
{
    public partial class InitialSocialSecurityCalculator : System.Web.UI.Page
    {
        #region Events
        /// <summary>
        /// code when page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Customers customer = new Customers();
                DataTable dtUserDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                Session[Constants.SessionRegistered] = null;
                if (Session[Constants.SessionUserId] == null)
                {
                    Response.Redirect(Constants.RedirectRegister, true);
                }
                /*  Dev :- Aloha Date:-02/12/2014 */

                if (Request.QueryString[Constants.reference] != null)
                {
                    customer.updateCustomerDetails(Request.QueryString[Constants.reference].ToString());
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "SubscribeStatus();", true);
                }
                if (dtUserDetails.Rows[Constants.zero][Constants.IsSubscription].ToString().Equals(Constants.FlagYes))
                {
                    pnlInitialControls.Visible = true;
                    pnlWelcomeControls.Visible = false;
                }
                else
                {
                    pnlInitialControls.Visible = false;
                    pnlWelcomeControls.Visible = true;
                }
                if (!IsPostBack)
                {
                    getCustomerDetails();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ExceptionInitialSocialSecurity, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ExceptionInitialSocialSecurity + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        ///  Redirect to Social Security
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void redirectToSocialSecurity(object sender, EventArgs e)
        {
            try
            {
                //Previously
                // Response.Redirect(Constants.RedirectSocialSecurityCalculator, false);



                //code for redirecting as per the marital status changed on 04/08/2014
                Customers calc = new Customers();
                DataTable dtInfo = calc.getCustomerDetails(Session["UserId"].ToString());
                String MaritalStatus = dtInfo.Rows[0]["MartialStatus"].ToString();
                if (MaritalStatus.Equals("Widowed"))
                    Response.Redirect(Constants.RedirectCalcWidowed, false);
                else if (MaritalStatus.Equals("Married"))
                {
                    //Response.Redirect(Constants.RedirectCalcMarried, false);

                    //Restricted
                    RedirectionOfMarriedStretegy(dtInfo);
                }
                else if (MaritalStatus.Equals("Divorced"))
                    Response.Redirect(Constants.RedirectCalcDivorced, false);
                else
                    Response.Redirect(Constants.RedirectCalcSingle, false);

                Session[Constants.SessionRole] = Constants.Customer;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ExceptionInitialSocialSecurity, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ExceptionInitialSocialSecurity + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }


        /// <summary>
        ///  Download Social Security Report as PDF Format
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void downloadSocialReportTest(object sender, EventArgs e)
        {
            try
            {
                Customers customer = new Customers();
                /* Get Pdf File Information based on User Id */
                DataTable dtPdfDetails = customer.getpdfFile(Session[Constants.SessionUserId].ToString());
                /* Check if user contain downloaded pdf file or not */
                if (dtPdfDetails.Rows.Count > Constants.zero)
                {
                    btnDownloadPdf.Visible = true;
                    /* Get Customer Details */
                    DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                    Document pdfDoc = new Document(PageSize.A4, Constants.zero, Constants.zero, -15f, Constants.zero);
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    /* Open the stream */
                    pdfDoc.Open();
                    writer.PageEvent = new PDFEvent();
                    // Front PDF Page
                    SiteNew.PdfFrontPage(dtCustomerDetails, ref pdfDoc);
                    pdfDoc.Close();
                    /* Response Setting */
                    Response.ContentType = Constants.pdfContentType;
                    Response.AddHeader(Constants.pdfContentDisposition, Constants.pdfFileName + dtCustomerDetails.Rows[0][Constants.CustomerFirstName].ToString() + "_" + DateTime.Now.ToString(Constants.DateFormat) + Constants.pdfFileExtension);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ExceptionInitialSocialSecurity, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ExceptionInitialSocialSecurity + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        ///  Download Social Security Report as PDF Format
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void downloadSocialReport(object sender, EventArgs e)
        {
            try
            {
                Customers customer = new Customers();
                /* Get Pdf File Information based on User Id */
                DataTable dtPdfDetails = customer.getpdfFile(Session[Constants.SessionUserId].ToString());
                /* Check if user contain downloaded pdf file or not */
                if (dtPdfDetails.Rows.Count > Constants.zero)
                {
                    //btnDownloadPdf.Visible = true;
                    /* Convert Pdf file content into Byte for writing data into pdf format */
                    Byte[] data = (Byte[])dtPdfDetails.Rows[Constants.zero][Constants.PdfFilesContent];
                    // Send the file to the browser
                    Response.AddHeader(Constants.pdfContentType, dtPdfDetails.Rows[Constants.zero][Constants.PdfFilesContentType].ToString());
                    Response.AddHeader(Constants.pdfContentDisposition, dtPdfDetails.Rows[Constants.zero][Constants.PdfFilesContent].ToString() + dtPdfDetails.Rows[Constants.zero][Constants.PdfFilesName].ToString());
                    Response.BinaryWrite(data);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Flush();
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ExceptionInitialSocialSecurity, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ExceptionInitialSocialSecurity + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion Events

        #region Methods
        public void validateSession()
        {
            try
            {
                Session[Constants.SessionRegistered] = null;
                if (Session[Constants.SessionUserId] == null)
                {
                    Response.Redirect(Constants.RedirectLogin, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ExceptionInitialSocialSecurity + "validateSession() - " + ex.ToString());
                ErrorMessagelable.Text = Constants.ExceptionInitialSocialSecurity + "validateSession() - " + ex.ToString();
                ErrorMessagelable.Visible = true;
            }
        }
        /// <summary>
        /// get Customer Details to display user name
        /// </summary>
        private void getCustomerDetails()
        {
            try
            {
                Customers customer = new Customers();
                DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                /* Get Pdf File Information based on User Id */
                DataTable dtPdfDetails = customer.getpdfFile(Session[Constants.SessionUserId].ToString());
                lblWelcome.Text = " " + dtCustomerDetails.Rows[Constants.zero][Constants.CustomerFirstName].ToString();
                lblWelcome1.Text = " " + dtCustomerDetails.Rows[Constants.zero][Constants.CustomerFirstName].ToString().ToUpper();
                //DataTable dtPdfDetails = customer.getpdfFile(Session[Constants.SessionUserId].ToString());
                /* Check if user contain downloaded pdf file or not */
                //if (dtPdfDetails.Rows.Count > Constants.zero)
                //{
                /* Before Displaying Download Button Check User Subscription Dev:- Aloha  Date:- 02/12/2014 */
                if (!Convert.ToString(dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription]).Equals(Constants.FlagNo))
                {

                    /* Check if user contain downloaded pdf file or not */
                    if (dtPdfDetails.Rows.Count > Constants.zero)
                    {
                        btnDownloadPdf.Visible = true;
                    }
                }
                else
                {
                    btnDownloadPdf.Visible = false;
                }
                /* Check if customer visits first time or not */
                if (!String.IsNullOrEmpty(dtCustomerDetails.Rows[Constants.zero][Constants.CustomerBirthDate].ToString()) && dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString() != Constants.DefaultDate)
                {
                    //btnSaveAccountDetails.Text = Constants.BtnSocialSecurity;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ExceptionInitialSocialSecurity, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ExceptionInitialSocialSecurity + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Decides Redirection of Married Strategy
        /// </summary>
        /// <param name="dtDetails"></param>
        private void RedirectionOfMarriedStretegy(DataTable dtDetails)
        {
            try
            {
                #region Restricted App Change
                DateTime UserDob = Convert.ToDateTime(dtDetails.Rows[0]["Birthdate"].ToString());
                DateTime SpouseDob = Convert.ToDateTime(dtDetails.Rows[0]["SpouseBirthdate"].ToString());
                DateTime dtLimit = new DateTime(1954, 01, 02);
                string strClaimedPerson = dtDetails.Rows[0]["ClaimedPerson"].ToString();
                string ClaimedBenefit = string.Empty, ClaimedAge = string.Empty, CurrentBenefit = string.Empty;
                if (!strClaimedPerson.Equals("NONE"))
                {
                    if (!strClaimedPerson.Equals(""))
                    {
                        ClaimedBenefit = dtDetails.Rows[0]["ClaimedBenefit"].ToString();
                        ClaimedAge = dtDetails.Rows[0]["ClaimedAge"].ToString();
                        if (strClaimedPerson.Equals("HUSBAND"))
                        {
                            Globals.Instance.BoolIsHusbandClaimedBenefit = true;
                            CurrentBenefit = dtDetails.Rows[0]["HusbandsRetAgeBenefit"].ToString();
                        }
                        else if (strClaimedPerson.Equals("WIFE"))
                        {
                            Globals.Instance.BoolIsWifeClaimedBenefit = true;
                            CurrentBenefit = dtDetails.Rows[0]["WifesRetAgeBenefit"].ToString();
                        }
                    }
                    else
                        Response.Redirect(Constants.RedirectCalcMarried);
                }
                else
                {
                    Response.Redirect(Constants.RedirectCalcMarried);
                }
                if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                {
                    if (!string.IsNullOrEmpty(CurrentBenefit))
                        RestrictAppIncomeProp.Instance.intHusbCurrentBenefit = Convert.ToInt32(CurrentBenefit);
                    RestrictAppIncomeProp.Instance.intHusbandClaimAge = Convert.ToInt32(ClaimedAge);
                    if (!string.IsNullOrEmpty(ClaimedBenefit))
                        RestrictAppIncomeProp.Instance.intHusbClaimBenefit = Convert.ToInt32(ClaimedBenefit);
                }
                if (Globals.Instance.BoolIsWifeClaimedBenefit)
                {
                    if (!string.IsNullOrEmpty(CurrentBenefit))
                        RestrictAppIncomeProp.Instance.intWifeCurrentBenefit = Convert.ToInt32(CurrentBenefit);
                    RestrictAppIncomeProp.Instance.intWifeClaimAge = Convert.ToInt32(ClaimedAge);
                    if (!string.IsNullOrEmpty(ClaimedBenefit))
                        RestrictAppIncomeProp.Instance.intWifeClaimBenefit = Convert.ToInt32(ClaimedBenefit);
                }
                //Determine show/hide full strategy
                if (Globals.Instance.BoolIsHusbandClaimedBenefit || Globals.Instance.BoolIsWifeClaimedBenefit)
                {
                    if (!Globals.Instance.BoolIsHusbandClaimedBenefit)
                    {
                        if (UserDob < dtLimit)
                        {
                            Globals.Instance.BoolIsShowRestrictFull = true;
                            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                        }
                        else
                            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                    }
                    else if (!Globals.Instance.BoolIsWifeClaimedBenefit)
                    {
                        if (SpouseDob < dtLimit)
                        {
                            Globals.Instance.BoolIsShowRestrictFull = true;
                            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                        }
                        else
                            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                    }
                    else
                        Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);

                }


                #endregion Restricted App Change
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCustomerSuccess, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }


        #endregion Methods
    }
}