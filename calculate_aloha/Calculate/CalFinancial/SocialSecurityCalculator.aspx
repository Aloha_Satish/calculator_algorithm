﻿<%@ Page Title="Social Security Calculator" Language="C#" MasterPageFile="~/ClientMaster.master" EnableEventValidation="true"  AutoEventWireup="true" CodeBehind="SocialSecurityCalculator.aspx.cs" Inherits="Calculate.SocialSecurityCalculator" %>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <div class="SecurityCal">
        <h1 class="headfontsize3 text-center setTopMargin">Basic Information For Social Security Calculation
        </h1>
        <%-- <asp:Panel ID="Panel1" runat="server">
            <div class="slider-single-label slider-label-selected" id="Div3">
                Basic Information
            </div>
            <div class="slider-single-label" id="Div1">
                Social Security Information
            </div>
            <div class="slider-single-label" id="Div2">
                Your Result
            </div>
            <div class="clearfloats"></div>
            <br />
            <center><div ><img id="Img1" runat="server" src="/images/statslider.png" style="height:auto;position:relative;margin-right:-1px;"/><img runat="server" src="/images/upperstaticslider.png" class="imgupslide" border="0" /><img id="Img2" runat="server" src="/images/statslider.png" style="position:static;margin-left:-5px"/>
            <div><img id="Img3" runat="server" src="/images/statslider.png" style="margin-top:-48PX" /></div>
      </div>
        </center>
        </asp:Panel>--%>
        <asp:Button ID="BtnChange" Visible="false" runat="server" CssClass="button_login floatLeft" Text="&laquo; Go back to Manage Customers" OnClick="BtnChange_Click" />
        <table class="SocaialCalculatorTable">
            <tr>
                <td>
                    <p class="text-center">
                        <b>Please confirm the accuracy of the information you entered and then click the “Save & Next” button below.
                        </b>
                    </p>
                </td>
            </tr>
            <tr>
                <td class="failureForgotNotification displayNone" colspan="2" id="displayErrorMessage">
                    <asp:Label ID="errorMessage" Visible="false" runat="server" ClientIDMode="Static"></asp:Label>
                    <br />
                    <asp:Label ID="errorMessage0" Visible="false" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Label ID="lblMartialStatus" runat="server" Text="What is your current marital status? "></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <%--<span style="font-weight:bold;"><label for="rdbtnMarried">Married</label><br />
                    <input id="rdbtnMarried" type="radio" name="MaritalStatus" value="rdbtnMarried" onclick="showHideSpouseInformation();" tabindex="4"></span>
                <span style="font-weight:bold;"><label for="rdbtnSingle">Single</label><br />
                    <input id="rdbtnSingle" type="radio" name="MaritalStatus" value="rdbtnSingle" onclick="showHideSpouseInformation();" tabindex="5"></span>
                <span style="font-weight:bold;"><label for="rdbtnWidowed">Widowed</label><br />
                    <input id="rdbtnWidowed" type="radio" name="MaritalStatus" value="rdbtnWidowed" onclick="showHideSpouseInformation();" tabindex="6"></span>
                <span style="font-weight:bold;"><label for="rdbtnDivorced">Divorced</label><br />
                    <input id="rdbtnDivorced" type="radio" name="MaritalStatus" value="rdbtnDivorced" checked="checked" onclick="showHideSpouseInformation();" tabindex="7"></span>--%>
                    <table>
                        <tr>
                            <td class="style2" style="text-align: center; width: 38px;">
                                <asp:Label ID="lblMarr" runat="server" Font-Bold="true" Text="Married" Class="Space"></asp:Label></td>
                            <td style="text-align: center; width: 44px;">
                                <asp:Label ID="lbSing" runat="server" Font-Bold="true" Text="Single" Class="Space"></asp:Label></td>
                            <td style="text-align: center; width: 16px;">
                                <asp:Label ID="lblWid" runat="server" Font-Bold="true" Text="Widowed" Class="Space"></asp:Label></td>
                            <td class="style5" style="width: 98px; text-align: center">
                                <asp:Label ID="lblDiv" runat="server" Font-Bold="true" Text="Divorced" Class="Space" /></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="style2" style="text-align: center; width: 38px;">
                                <asp:RadioButton ID="rdbtnMarried" onclick="customerSpouseinformation();" runat="server" Width="60px" GroupName="MaritalStatus" TabIndex="4" /></td>
                            <%--</td><td>--%>
                            <td style="text-align: center; width: 44px;">
                                <asp:RadioButton ID="rdbtnSingle" onclick="customerSpouseinformation();" runat="server" Width="50px" GroupName="MaritalStatus" TabIndex="5" /></td>
                            <%--</td><td>--%>
                            <td style="text-align: center; width: 20px;">
                                <asp:RadioButton ID="rdbtnWidowed" onclick="customerSpouseinformation();" runat="server" Width="65px" GroupName="MaritalStatus" TabIndex="6" /></td>
                            <%--</td><td>--%>
                            <td class="style5" style="width: 98px; text-align: center;">
                                <asp:RadioButton ID="rdbtnDivorced" onclick="customerSpouseinformation();" runat="server" Width="65px" GroupName="MaritalStatus" TabIndex="7" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="DivorceText displayNone" style="font-size: 14px;">
                <td>
                    <br />
                    <p>You must have been married for at least 10 years before your divorce and not currently remarried in order to use your ex-spouse's benefits in your claiming strategy."</p>
                    <p>If you were married for less than 10 years before your divorce and are not currently remarried, please change your marital status to "Single".</p>
                    <p>If you are currently remarried, please change your marital status to "Married" and use your current spouse's Social Security Benefit information.</p>
                </td>
            </tr>
            <tr>
                <td>

                    <br />
                    <asp:Label ID="lblFirstname" runat="server" Text="What is your first name?"></asp:Label></td>
            </tr>
            <tr>
                <td class="SocialCalculatorFirstColumn">
                    <asp:TextBox ID="txtFirstname" runat="server" MaxLength="20" CssClass="textEntry form-control" placeholder="Your First Name" TabIndex="1"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="display: none">
            </tr>
            <tr>
                <td style="display: none"></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="height: 26px">
                    <asp:Label ID="lblBirthDate" runat="server" Text="What is your date of birth?"></asp:Label></td>
            </tr>
            <tr>
                <td class="searchText SocialCalculatorFirstColumn">
                    <%--<asp:TextBox ID="txtBirthDate" runat="server" CssClass="textEntry" placeholder="Your Birth Date" TabIndex="3" MaxLength="20"></asp:TextBox>--%>
                    
                    Month:
                    <asp:DropDownList ID="ddlMonth_Your" runat="server" Class="Space" onchange="PopulateDays_Your()" />
                    Day:
                    <asp:DropDownList ID="ddlDay_Your" runat="server" Class="Space" />
                    Year:
                    <asp:DropDownList ID="ddlYear_Your" runat="server" onchange="PopulateDays_Your()" />
                    <br />
                    <asp:CustomValidator ID="Validator_Your" runat="server" ErrorMessage="* Required"
                        ClientValidationFunction="Validate_Your" />
                    <br />
                </td>
            </tr>
            <tr class="hideRow">
                <td>
                    <asp:Label ID="lblSpouseFirstname" runat="server" Text="What is your spouse's first name?"></asp:Label></td>
            </tr>
            <tr class="hideRow">
                <td>
                    <asp:TextBox ID="txtSpouseFirstname" runat="server" CssClass="textEntry form-control" placeholder="Spouse First Name" TabIndex="8" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr class="hideRow">
                <td style="display: none">
            </tr>
            <tr class="hideRow">
                <td style="display: none"></td>
            </tr>
            <tr class="hideRow">
                <td>&nbsp;</td>
            </tr>
            <tr class="hideRow1">
                <td>
                    <asp:Label ID="lblSpouseBirthDate" runat="server" Text="What is your spouse's birth date?"></asp:Label></td>
            </tr>
            <tr class="hideRow1">
                <td class="searchText SocialCalculatorFirstColumn">
                    <%--<asp:TextBox ID="txtSpouseBirthdate" CssClass="textEntry" runat="server" placeholder="Spouse Birth Date" TabIndex="10" MaxLength="20"></asp:TextBox>--%>
                    Month:
                    <asp:DropDownList ID="ddlMonth_Spouse" runat="server" onchange="PopulateDays_Spouse()" Class="Space" />
                    Day:
                    <asp:DropDownList ID="ddlDay_Spouse" runat="server" Class="Space" />
                    Year:
                    <asp:DropDownList ID="ddlYear_Spouse" runat="server" onchange="PopulateDays_Spouse()" />
                    <br />
                    <asp:CustomValidator ID="Validator_Spouse" runat="server" ErrorMessage="* Required"
                        ClientValidationFunction="Validate_Spouse" />
                </td>
            </tr>
            <tr class="hideRow">
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnSaveBasicInfoAndProceed" runat="server" Text="Save & Next - Social Security Information" CssClass="button_login btn" OnClientClick="return validateData();" OnClick="btnSaveBasicInfoAndProceed_Click" TabIndex="11" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>

</asp:Content>

<%--JAVA SCRIPTS--%>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            window.scrollTo(0, 400);
        });
        $(function () {
            setTimeout(function () {
                $("[id$=errorMessage0]").fadeOut(3000);
            }, 5000);
        });
        $(function () {
            var DatepickerOpts = {
                changeMonth: true,
                changeYear: true,
                yearRange: '1910:2020',
                showOn: 'both',
                buttonImageOnly: true,
                buttonImage: '/Images/calendar1.png',
            };
            $("[id$=txtBirthDate]").datepicker(DatepickerOpts);
            var SpouseDatepickerOpts = {
                changeMonth: true,
                changeYear: true,
                yearRange: '1910:2020',
                showOn: 'both',
                buttonImageOnly: true,
                buttonImage: '/Images/calendar1.png',
            };
            $("[id$=txtSpouseBirthdate]").datepicker(SpouseDatepickerOpts);
            function showLabelSSC() {
                if ($('[id$=rdbtnSingle]').is(':checked')) {
                    $('#Div3').html("Social Security Information");
                    $('#Div1').html("Your Results");
                }
                else if ($('[id$=rdbtnMarried]').is(':checked')) {
                    $('#Div1').html("Social Security Information");
                    $('#Div2').html("Your Results");
                }
                else if ($('[id$=rdbtnWidowed]').is(':checked')) {
                    $('#Div3').html("Social Security Information");
                    $('#Div1').html("Your Results");
                }
                else if ($('[id$=rdbtnDivorced]').is(':checked')) {
                    $('#Div3').html("Social Security Information");
                    $('#Div1').html("Your Results");
                }
            }

            $('[id$=Img1]').attr('src', '/images/staticslider.png');
            $('input[type=radio][name=$MaritalStatus]').change(function () {

            });
            /* Check if id exists in current Page */
            if ($("[id$=txtBirthDate]") != null) {
                $("[id$=txtBirthDate]").keypress(function (e) { e.preventDefault(); });
            }

            /* Check if id exists in current Page */
            if ($("[id$=txtSpouseBirthdate]") != null) {
                $("[id$=txtSpouseBirthdate]").keypress(function (e) { e.preventDefault(); });
            }
        });
        function PopulateDays_Your() {
            var ddlMonth_Your = document.getElementById("<%=ddlMonth_Your.ClientID%>");
            var ddlYear_Your = document.getElementById("<%=ddlYear_Your.ClientID%>");
            var ddlDay_Your = document.getElementById("<%=ddlDay_Your.ClientID%>");
            var y_Your = ddlYear_Your.options[ddlYear_Your.selectedIndex].value;
            var m_Your = ddlMonth_Your.options[ddlMonth_Your.selectedIndex].value != 0;
            if (ddlMonth_Your.options[ddlMonth_Your.selectedIndex].value != 0 && ddlYear_Your.options[ddlYear_Your.selectedIndex].value != 0) {
                var dayCount = 32 - new Date(ddlYear_Your.options[ddlYear_Your.selectedIndex].value, ddlMonth_Your.options[ddlMonth_Your.selectedIndex].value - 1, 32).getDate();
                // ddlDay_Your.options.length = 0;
                AddOption_Your(ddlDay_Your, "", "0");
                for (var i = 1; i <= dayCount; i++) {
                    AddOption_Your(ddlDay_Your, i, i);
                }
            }
        }

        function AddOption_Your(ddl, text, value) {
            var opt = document.createElement("OPTION");
            opt.text = text;
            opt.value = value;
            ddl.options.add(opt);

        }

        function Validate_Your(sender, args) {
            var ddlMonth_Your = document.getElementById("<%=ddlMonth_Your.ClientID%>");
            var ddlYear_Your = document.getElementById("<%=ddlYear_Your.ClientID%>");
            var ddlDay_Your = document.getElementById("<%=ddlDay_Your.ClientID%>");
            args.IsValid = (ddlDay_Your.selectedIndex != 0 && ddlMonth_Your.selectedIndex != 0 && ddlYear_Your.selectedIndex != 0)
        }


        function PopulateDays_Spouse() {

            var ddlMonth_Spouse = document.getElementById("<%=ddlMonth_Spouse.ClientID%>");
            var ddlYear_Spouse = document.getElementById("<%=ddlYear_Spouse.ClientID%>");
            var ddlDay_Spouse = document.getElementById("<%=ddlDay_Spouse.ClientID%>");
            var y_Spouse = ddlYear_Spouse.options[ddlYear_Spouse.selectedIndex].value;
            var m_Spouse = ddlMonth_Spouse.options[ddlMonth_Spouse.selectedIndex].value != 0;
            if (ddlMonth_Spouse.options[ddlMonth_Spouse.selectedIndex].value != 0 && ddlYear_Spouse.options[ddlYear_Spouse.selectedIndex].value != 0) {
                var dayCount = 32 - new Date(ddlYear_Spouse.options[ddlYear_Spouse.selectedIndex].value, ddlMonth_Spouse.options[ddlMonth_Spouse.selectedIndex].value - 1, 32).getDate();
                //ddlDay_Spouse.options.length = 0;
                AddOption_Spouse(ddlDay_Spouse, "", "0");
                for (var i = 1; i <= dayCount; i++) {
                    AddOption_Spouse(ddlDay_Spouse, i, i);
                }
            }
        }

        function AddOption_Spouse(ddl, text, value) {
            var opt = document.createElement("OPTION");
            opt.text = text;
            opt.value = value;
            ddl.options.add(opt);
        }

        function Validate_Spouse(sender, args) {
            var ddlMonth_Spouse = document.getElementById("<%=ddlMonth_Spouse.ClientID%>");
            var ddlYear_Spouse = document.getElementById("<%=ddlYear_Spouse.ClientID%>");
            var ddlDay_Spouse = document.getElementById("<%=ddlDay_Spouse.ClientID%>");
            args.IsValid = (ddlDay_Spouse.selectedIndex != 0 && ddlMonth_Spouse.selectedIndex != 0 && ddlYear_Spouse.selectedIndex != 0)
        }

        function customerSpouseinformation() {

            if ($("[id$=rdbtnMarried]").prop("checked")) {
                $(".hideRow").show();
                $(".hideRow1").show();
                $(".DivorceText").hide();
                $(".spacehide").show();
                document.getElementById("<%=lblSpouseBirthDate.ClientID%>").innerHTML = "What is your spouse's date of birth?";
            }
            else if ($("[id$=rdbtnDivorced]").prop("checked")) {
                $(".hideRow").hide();
                $(".hideRow1").show();
                $(".DivorceText").show();
                $(".spacehide").show();
                document.getElementById("<%=lblSpouseBirthDate.ClientID%>").innerHTML = "What is your Ex-spouse's date of birth?";
            }
            else {
                $(".hideRow").hide();
                $(".hideRow1").hide();
                $(".spacehide").show();
                $(".DivorceText").hide();
                document.getElementById("lblSpouseBirthDate").innerHTML = "What is your Ex-Spouse's date of birth?";
            }
        }

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
</asp:Content>
<%--END FO JAVA SCRIPTS--%>