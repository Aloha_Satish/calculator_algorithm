﻿using CalculateDLL;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Symbolics;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using System.Reflection;
using Calculate.Objects;

namespace Calculate
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        #region Variable Declaration
        // Input items
        int intAgeWife, intAgeHusb, intAgeWifeMonth, intAgeHusbMonth, intAgeWifeDays, intCurrentAgeWifeMonth, intCurrentAgeWifeYear;
        decimal decBeneWife, decBeneHusb;
        decimal colaAmt = 1;
        int birthYearWife = 0, birthYearHusb = 0;
        DataTable dtDetails;
        String customerFirstName = string.Empty;
        String customerSposeFirstName = string.Empty;
        Customers customer = new Customers();
        DateTime limit = DateTime.Parse("01/01/1954");
        #endregion

        #region Events
        /// <summary>
        /// btnSaveBasicInfoAndProceed_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveBasicInfoAndProceed_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                string sIsSubscription = dtCustomerDetails.Rows[0][Constants.IsSubscription].ToString();
                if (sIsSubscription.Equals(Constants.FlagNo))
                {
                    Response.Redirect(Constants.RedirectWelcomeUser, false);
                }
                else
                {
                    Response.Redirect(Constants.RedirectSocialSecurityCalculator, false);
                    //Response.Redirect("~/CalFinancial/InitialSocialSecurityCalculator.aspx", false);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.TryAgain;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// code to show the explanation popup for all strategies.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExplain_Click(object sender, EventArgs e)
        {
            try
            {
                popup.Show();
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "displayPanelExplanation();", true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Function called when Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                validateSession();
                if (!IsPostBack)
                {
                    try
                    {
                        HttpContext.Current.Session[Constants.SpousalBenefitValue] = null;
                        if (Session["Demo"] != null)
                        {
                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                            DateTime wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            ValidateAndCalc();

                            //Send mail to the demo user before loading the strategy page
                            Globals.Instance.BoolEmailReport = true;
                            exportAllToPdf();
                            Globals.Instance.BoolEmailReport = false;
                        }
                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                        {
                            ////BtnChange.Text = Constants.BtnChangeText;
                            ///* Disabled textbox and radio button so that institutional user can only view customer details */
                            //BirthWifeYYYY.Enabled = false;
                            //BeneWife.Enabled = false;
                            //BirthHusbandYYYY.Enabled = false;
                            //BeneHusband.Enabled = false;
                            ////CheckBoxCola.Enabled = false;
                            //// Initialize page variables                
                            //ErrorMessage.Text = String.Empty;
                            //PanelGrids.Visible = false;
                            //PanelParms.Visible = false;
                            ////CheckBoxCola.Checked = true;
                            //BirthWifeMM.Focus();
                            //// On Page load bind All the values to the respective grid
                            //ShowAllInformation();
                            //Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.ClientScriptDatePickerName, "disableDatePicker();", true);

                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                            DateTime wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            PanelEntry.Visible = false;
                            ValidateAndCalc();
                            if (Globals.Instance.BoolEmailReport || Globals.Instance.BoolAdvisorDownloadReport)
                            {
                                Globals.Instance.BoolAdvisorDownloadReport = false;
                                exportAllToPdf();
                                Response.Redirect(Constants.RedirectManageCustomers);
                            }
                        }

                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                        {
                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                            DateTime wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            PanelEntry.Visible = false;
                            ValidateAndCalc();
                            if (Globals.Instance.BoolEmailReport || Globals.Instance.BoolAdvisorDownloadReport)
                            {
                                Globals.Instance.BoolAdvisorDownloadReport = false;
                                exportAllToPdf();
                                Response.Redirect(Constants.RedirectManageCustomers);
                            }
                        }
                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor))
                        {
                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                            DateTime wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            ValidateAndCalc();
                            if (Globals.Instance.BoolEmailReport || Globals.Instance.BoolAdvisorDownloadReport)
                            {
                                Globals.Instance.BoolAdvisorDownloadReport = false;
                                exportAllToPdf();
                                Response.Redirect(Constants.RedirectManageCustomers);
                            }
                        }
                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.Customer))
                        {
                            Customers customer = new Customers();
                            DateTime wifeBirthDate, husbandBirthDate;
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());

                            wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;

                            PanelGrids.Visible = false;//Added for new development (on 16/09)
                            PanelEntry.Visible = false;//Added for new development (on 16/09)
                            ValidateAndCalc();
                            if (Convert.ToString(dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription]).Equals(Constants.FlagNo))
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "displaySubscriptionPopUp(false);", true);
                                ValidateAndCalc();
                                //to show the Paid to wait value on the popup for new user
                                decimal amt = decimal.Parse(Session[Constants.DMaxCumm].ToString());
                                MaxCumm.Text = Constants.SubscriptionPopupText + Environment.NewLine + amt.ToString(Constants.AMT_FORMAT);
                            }
                            //Commented for new development (on 16/09)
                            //else
                            //    ValidateAndCalc();

                        }
                        else if (!string.IsNullOrEmpty(Session["CustAfterTrans"] as string))
                        {
                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                            DateTime wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            ValidateAndCalc();
                        }
                        else
                        {
                            // Initialize page variables                
                            ErrorMessage.Text = String.Empty;
                            PanelGrids.Visible = false;
                            PanelParms.Visible = false;
                            //CheckBoxCola.Checked = true;
                            BirthWifeMM.Focus();
                            // On Page load bind All the values to the respective grid
                            ShowAllInformation();
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                        ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                        ErrorMessageLabel.Visible = true;
                    }
                }
                else
                {
                    //make first radio button selected when page loads
                    rdbtnStrategy1.Checked = false;
                    rdbtnStrategy2.Checked = false;
                    rdbtnStrategy3.Checked = false;
                }
            }
            catch (Exception ex)
            {

                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Function called when click over calculate or next button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Calculate_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                Button clicked = (Button)sender;


                if (clicked.Text == BtnCalculate.Text)
                {
                    if (Convert.ToString(dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription]).Equals(Constants.FlagNo))
                    {
                        if (Session[Constants.SessionRegistered] != null)
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displaySubscriptionPopUp(true);", true);
                        }
                        else
                        {
                            if (BeneWife.Text != string.Empty && BeneHusband.Text != string.Empty)
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displaySubscriptionPopUp(false);", true);
                                ValidateAndCalc();
                                //to show the paidt to wait number on the subscription popup
                                decimal amt = decimal.Parse(Session[Constants.DMaxCumm].ToString());
                                MaxCumm.Text = Constants.SubscriptionPopupText + amt.ToString(Constants.AMT_FORMAT);
                            }
                        }
                    }
                    else
                    {
                        ValidateAndCalc();
                    }
                }
                else //if (clicked.Text == BtnChange.Text)
                {
                    if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                    {
                        Response.Redirect(Constants.RedirectSocialSecurityCalculator, false);
                    }
                    else
                    {
                        BackToParms();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.TryAgain;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Function called when print data into pdf
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PrintAll_Click(object sender, EventArgs e)
        {
            try
            {
                //print all grids and Charts to PDF
                exportAllToPdf();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.DownloadError;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Button click to send user to paypal for Trial period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnTrial_Click(object sender, EventArgs e)
        {
            try
            {
                // Update column with package plan
                customer.updateCustomerSubscriptionPlan(Session[Constants.SessionUserId].ToString(), Constants.WithoutTrialPeriod);
                //Changes for live Server
                //Response.Redirect(Constants.TrialLiveServerURL + Session[Constants.SessionUserId].ToString(), true);
                //Response.Redirect(Constants.NewSingleUseURL + Session[Constants.SessionUserId].ToString(), true);
                //Changes for InfusionSoft live Server
                Response.Redirect(Constants.NewSingleUseURLInfusion, true);
                
                // Redirect to chargify url 
                //Response.Redirect(Constants.TrialTestServerURL + Session[Constants.SessionUserId].ToString(), true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.TryAgain;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Button click to send user to paypal for not in Trial period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnWithoutTrial_Click(object sender, EventArgs e)
        {
            try
            {
                // Update column with package plan
                customer.updateCustomerSubscriptionPlan(Session[Constants.SessionUserId].ToString(), Constants.TrialPeriod);
                //Changes for live Server
                Response.Redirect(Constants.WithoutTrialLiveServerURL + Session[Constants.SessionUserId].ToString(), true);
                // Redirect to chargify url 
                //Response.Redirect(Constants.WithoutTrialTestServerURL + Session[Constants.SessionUserId].ToString(), true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.TryAgain;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// to close the explanation popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgCancel_Click(object sender, EventArgs e)
        {
            try
            {
                popup.Hide();
                ValidateAndCalc();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }
        #endregion

        #region Validation

        /// <summary>
        /// Validate Session
        /// </summary>
        public void validateSession()
        {
            try
            {
                Session[Constants.SessionRegistered] = null;
                if (Session[Constants.SessionUserId] == null)
                {
                    //redirected to login when the session ends
                    Response.Redirect(Constants.RedirectLogin, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + "validateSession() - " + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Check input values
        /// </summary>
        /// <returns></returns>
        protected bool ValidateInput()
        {
            try
            {
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor) || Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                    dtDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                else
                    dtDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                customerFirstName = dtDetails.Rows[0][Constants.CustomerFirstName].ToString();
                customerSposeFirstName = dtDetails.Rows[0][Constants.CustomerSpouseFirstName].ToString();
                ErrorMessage.Text = string.Empty;
                bool isValid = true;
                int birthYYYY, birthMM, birthDD;
                DateTime today = DateTime.Now;
                DateTime wifeBD = today;
                birthMM = SiteNew.ValidateNumeric(BirthWifeMM, 1, 12, Constants.Text0, ref isValid, ErrorMessage);
                birthDD = SiteNew.ValidateNumeric(BirthWifeDD, 1, 31, Constants.Text1, ref isValid, ErrorMessage);
                birthYYYY = SiteNew.ValidateNumeric(birthWifeTempYYYY, 1900, 2020, Constants.Text2, ref isValid, ErrorMessage);
                if (isValid)
                {
                    birthYearWife = int.Parse(birthWifeTempYYYY.Text);
                    wifeBD = new DateTime(birthYYYY, birthMM, birthDD);
                }
                TimeSpan Span = DateTime.Now - wifeBD;
                DateTime Age = DateTime.MinValue + Span;
                int Years = Age.Year - 1;
                int Months = Age.Month - 1;
                int Days = Age.Day - 1;
                if (Days >= 28)
                    Months += 1;
                if (Months >= 10)
                    Years += 1;
                intAgeWife = Years;
                intAgeForWife(wifeBD);
                // intAgeWife = today.Year - wifeBD.Year;
                // if (today.Month > wifeBD.Month
                //|| (today.Month == wifeBD.Month)
                //&& (today.Day < wifeBD.Day))
                //     intAgeWife -= 1;
                //loop to get the numbers of months and if days greater thna 28 than considered as complete month
                if (wifeBD.Day >= 28 && wifeBD.Day != DateTime.Now.Day)
                {
                    intAgeWifeMonth = wifeBD.Month + 1;
                    intAgeWifeDays = wifeBD.Day;
                }
                else
                {
                    intAgeWifeMonth = wifeBD.Month;
                    intAgeWifeDays = wifeBD.Day;
                }
                DateTime husbBD = today;
                birthMM = SiteNew.ValidateNumeric(BirthHusbandMM, 1, 12, Constants.SpouseText0, ref isValid, ErrorMessage);
                birthDD = SiteNew.ValidateNumeric(BirthHusbandDD, 1, 31, Constants.SpouseText1, ref isValid, ErrorMessage);
                birthYYYY = SiteNew.ValidateNumeric(BirthHusbandTempYYYY, 1900, 2020, Constants.SpouseText2, ref isValid, ErrorMessage);
                if (isValid)
                {
                    birthYearHusb = int.Parse(BirthHusbandTempYYYY.Text);
                    husbBD = new DateTime(birthYYYY, birthMM, birthDD);
                }
                //intAgeHusb = today.Year - husbBD.Year;
                //if (today.Month < husbBD.Month || (today.Month == husbBD.Month) && (today.Day < husbBD.Day))
                //    intAgeHusb -= 1;
                Span = DateTime.Now - husbBD;
                Age = DateTime.MinValue + Span;
                Years = Age.Year - 1;
                Months = Age.Month - 1;
                Days = Age.Day - 1;
                if (Days >= 28)
                    Months += 1;
                if (Months >= 10)
                    Years += 1;
                intAgeHusb = Years;
                if (husbBD.Day >= 28 && husbBD.Day != DateTime.Now.Day)
                    intAgeHusbMonth = husbBD.Month + 1;
                else
                    intAgeHusbMonth = husbBD.Month;
                decimal.TryParse(this.BeneWife.Text, out decBeneWife);
                decimal.TryParse(this.BeneHusband.Text, out decBeneHusb);
                if (isValid)
                {
                    PanelGrids.Visible = true;
                    PanelEntry.Visible = false;
                    PanelParms.Visible = true;
                    lblCustomerName.Text = customerFirstName;
                    lblCustomerDOB.Text = wifeBD.ToShortDateString();
                    lblCustomerSpouseDOB.Text = husbBD.ToShortDateString();
                    lblCustomerFRA.Text = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                    lblCustomerSpouseFRA.Text = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                    lblCustomerBenefit.Text = decBeneWife.ToString(Constants.AMT_FORMAT);
                    lblCustomerSpouseBenefit.Text = decBeneHusb.ToString(Constants.AMT_FORMAT);
                }
                else
                    ErrorMessage.Text += ".";
                if (!String.IsNullOrEmpty(ErrorMessage.Text))
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayErrorMessage();", true);
                else
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "hideErrorMessage();", true);
                return isValid;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
                return false;
            }
        }

        #endregion Validation

        #region CRUD Operation

        /// <summary>
        /// This method basic information of customer in form
        /// </summary>
        /// Date: 11 June 2014
        private void ShowAllInformation()
        {
            try
            {
                DataTable dtCustomerFinancialDetails = customer.getCustomerFinancialDetailsByStatus(Session[Constants.SessionUserId].ToString(), Constants.Divorced);
                if (dtCustomerFinancialDetails.Rows.Count != 0)
                {
                    if (!String.IsNullOrEmpty(dtCustomerFinancialDetails.Rows[0][Constants.CustomerBirthDate].ToString()))
                    {
                        DateTime strWifesBirthDate = (DateTime)dtCustomerFinancialDetails.Rows[0][Constants.CustomerBirthDate];
                        BirthWifeDD.Text = strWifesBirthDate.Day.ToString();
                        BirthWifeMM.Text = strWifesBirthDate.Month.ToString();
                        BirthWifeYYYY.Text = strWifesBirthDate.ToShortDateString();
                    }
                    if (!String.IsNullOrEmpty(dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoExSpousesBirthDate].ToString()))
                    {
                        DateTime strBirthHusbandDate = (DateTime)dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoExSpousesBirthDate];
                        BirthHusbandDD.Text = strBirthHusbandDate.Day.ToString();
                        BirthHusbandMM.Text = strBirthHusbandDate.Month.ToString();
                        BirthHusbandYYYY.Text = strBirthHusbandDate.ToShortDateString();
                    }
                    BeneWife.Text = dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoRetAgeBenefit].ToString();
                    BeneHusband.Text = dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoSlimyExsRetAgeBenefit].ToString();
                    //code commented to remove the COLA Checkbox
                    //string strCola = dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoCola].ToString();
                    //if (strCola.Equals("T"))
                    //    CheckBoxCola.Checked = true;
                    //else
                    //    CheckBoxCola.Checked = false;
                }
                else
                {
                    DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                    /* Check if Birthdate exists or not */
                    if (!String.IsNullOrEmpty(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString()))
                    {
                        DateTime strBirthDate = (DateTime)dtCustomerDetails.Rows[0][Constants.CustomerBirthDate];
                        BirthWifeDD.Text = strBirthDate.Day.ToString();
                        BirthWifeMM.Text = strBirthDate.Month.ToString();
                        BirthWifeYYYY.Text = strBirthDate.ToShortDateString();
                    }
                    /* Check if spouse birthdate exists or not */
                    if (!String.IsNullOrEmpty(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString()))
                    {
                        DateTime strSpouseDate = (DateTime)dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate];
                        BirthHusbandDD.Text = strSpouseDate.Day.ToString();
                        BirthHusbandMM.Text = strSpouseDate.Month.ToString();
                        BirthHusbandYYYY.Text = strSpouseDate.ToShortDateString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Update Customer Financial Details
        /// </summary>
        public void UpdateCustomerFinancialDetails()
        {
            try
            {
                SecurityInfoDivorced divorced = new SecurityInfoDivorced();
                divorced.BirthDate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(BirthWifeMM.Text + Constants.BackSlash + BirthWifeDD.Text + Constants.BackSlash + birthWifeTempYYYY.Text));
                divorced.ExSpousesBirthDate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(BirthHusbandMM.Text + Constants.BackSlash + BirthHusbandDD.Text + Constants.BackSlash + BirthHusbandTempYYYY.Text));
                divorced.RetAgeBenefit = BeneWife.Text;
                divorced.SlimyExsRetAgeBenefit = BeneHusband.Text;
                divorced.CustomerID = Session[Constants.SessionUserId].ToString();
                //if (CheckBoxCola.Checked)
                divorced.Cola = Constants.ColaStatusT;
                //else
                //    divorced.Cola = "F";
                Customers customer = new Customers();
                customer.UpdateCustomerFinancialDetailsForDivorced(divorced);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        #endregion

        #region Other Operations

        /// <summary>
        /// Requested calculation
        /// </summary>
        protected void ValidateAndCalc()
        {
            try
            {
                ResetValues();
                string WifeBirth = BirthWifeYYYY.Text.ToString();
                char[] delimiters = new char[] { '/' };
                string[] WifeBirthArray = WifeBirth.Split(delimiters, 3);
                birthWifeTempYYYY.Text = WifeBirthArray[2];
                BirthWifeMM.Text = WifeBirthArray[0];
                BirthWifeDD.Text = WifeBirthArray[1];
                string HusbandBirth = BirthHusbandYYYY.Text.ToString();
                string[] HusbandBirthArray = HusbandBirth.Split(delimiters, 3);
                BirthHusbandTempYYYY.Text = HusbandBirthArray[2];
                BirthHusbandMM.Text = HusbandBirthArray[0];
                BirthHusbandDD.Text = HusbandBirthArray[1];
                if (ValidateInput())
                {
                    BuildPage();
                    // Save All Details to respective tables According to martial status
                    UpdateCustomerFinancialDetails();
                    SiteNew.renderGridViewIntoStackedChart(GridAsap, ChartAsap, LabelAsap, Constants.Divorced, false);
                    SiteNew.renderGridViewIntoStackedChart(GridStandard, ChartStandard, LabelStandard, Constants.Divorced, false);
                    SiteNew.renderGridViewIntoStackedChart(GridSpouse, ChartSpouse, LabelSpouse, Constants.Divorced, false);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// code generate the sort experession for list acending order
        /// </summary>
        /// <param name="a">strategy Name</param>
        /// <param name="b">value of age 70</param>
        /// <returns></returns>
        public static int Compare2(KeyValuePair<string, int> a, KeyValuePair<string, int> b)
        {
            try
            {
                return a.Value.CompareTo(b.Value);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// Build the page - code to build the Grids/Charts and find the best grid and display them
        /// </summary>
        protected void BuildPage()
        {
            try
            {

                ResetValues();

                #region get the age in years from DOB to 01/01/2016
                //loop set to get the changes based on date for new change policy  according to Brian
                //get the age in years from DOB to 01/01/2016
                DateTime HusbDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                //TimeSpan ageYearsh = limit.Subtract(HusbDOB);
                //DateTime Age = DateTime.MinValue + ageYearsh;
                //int Years = Age.Year - 1;
                #endregion get the age in years from DOB to 01/01/2016

                #region Code set properties and set variable values
                // Set calculation properties
                Calc Calc = new Calc()
                {
                    intAgeWife = intAgeWife,
                    decBeneWife = decBeneWife,
                    birthYearWife = birthYearWife,
                    intAgeHusb = intAgeHusb,
                    decBeneHusb = decBeneHusb,
                    birthYearHusb = birthYearHusb,
                    intAgeWifeMonth = intAgeWifeMonth,
                    intAgeWifeDays = intAgeWifeDays,
                    intAgeHusbMonth = intAgeHusbMonth,
                    HusbDataofBirth = HusbDOB,
                    intCurrentAgeWifeMonth = intCurrentAgeWifeMonth,
                    intCurrentAgeWifeYear = intCurrentAgeWifeYear
                };
                //code commented to set COLA 2.5% by default
                //if (CheckBoxCola.Checked)
                //    Calc.colaAmt = 1.025m;
                //else
                //    Calc.colaAmt = 1;
                Calc.colaAmt = 1.025m;
                colaAmt = Calc.colaAmt;
                Customers customer = new Customers();
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor) || Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                    dtDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                else
                    dtDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                customerFirstName = dtDetails.Rows[0][Constants.CustomerFirstName].ToString();
                Globals.Instance.WifeDateOfBirth = Convert.ToDateTime(BirthWifeYYYY.Text);
                Globals.Instance.HusbDateOfBirth = Convert.ToDateTime(BirthHusbandYYYY.Text);
                Calc.husbBirthDate = Convert.ToDateTime(BirthHusbandYYYY.Text);
                Calc.wifeBirthDate = Convert.ToDateTime(BirthWifeYYYY.Text);
                Calc.Your_Name = customerFirstName;
                CommonVariables.strUserName = customerFirstName;
                #endregion Code set properties and set variable values

                #region Code build all visible Grids for Divorced case
                //build the strategy as per the PIA and DOB
                int cummAsap = Calc.BuildSingle(GridAsap, Calc.calc_strategy.Standard, Calc.calc_solo.Divorced, 0, ref NoteAsap, Constants.DAsap);
                //int[] asapBeAmts = Calc.breakEvenAmt;
                //highlight the numbers in the grid
                Calc.Highlight(GridAsap, Calculate.Calc.calc_solo.Divorced, Constants.GridAsap);
                //cummulative income at age 69
                Session[Constants.DivorceAasapCumm] = Calc.DCummValue;
                //lable to shpw the annual income value at paid to wait age
                Session[Constants.DivorceAnualAsap] = Calc.DivorceAnnualValue.ToString(Constants.AMT_FORMAT);
                //show keys and steps in the grid for grid asap
                renderDataIntoGridStartegy3(Calc.DCummValue, Calc.SValueStarting, Calc.SAgeValue, Calc.DivorceSpousalBenefits, Calc.DivorceSpousalAge, customerFirstName, Calc.AgeAtLastChange, Calc.DivorceAnnualValue);
                Globals.Instance.strDivorceStarting = string.Empty;
                Globals.Instance.WifeNumberofMonthinNote = 0;
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                Globals.Instance.BoolWHBSwitchedHusb = false;
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                Calc.DivorceSpousalBenefits = 0;

                //build the strategy as per the PIA and DOB
                int cummStd = Calc.BuildSingle(GridStandard, Calc.calc_strategy.Suspend, Calc.calc_solo.Divorced, 0, ref NoteStandard, Constants.DStandard);
                //int stdAge70Combined = Calc.age70CombBene;
                //highlight the numbers in the grid
                Calc.Highlight(GridStandard, Calculate.Calc.calc_solo.Divorced, Constants.GridStandard);
                //cummulative income at age 69
                Session[Constants.DivorceStandardCumm] = Calc.DCummValue;
                //lable to shpw the annual income value at paid to wait age
                Session[Constants.DivorceAnualStandard] = Calc.DivorceAnnualValue.ToString(Constants.AMT_FORMAT);
                renderDataIntoGridStartegy2(Calc.DCummValue, Calc.SValueStarting, Calc.SAgeValue, Calc.DivorceSpousalBenefits, Calc.DivorceSpousalAge, customerFirstName, Calc.AgeAtLastChange, Calc.DivorceAnnualValue);
                Globals.Instance.WifeNumberofMonthinNote = 0;
                Globals.Instance.strDivorceStarting = string.Empty;
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                Globals.Instance.BoolWHBSwitchedHusb = false;
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;

                //build the strategy as per the PIA and DOB
                int cummSpouse = Calc.BuildSingle(GridSpouse, Calc.calc_strategy.Spousal, Calc.calc_solo.Divorced, Calc.calc_start.fra6667, ref NoteSpouse, Constants.DSpouse);
                //int spouseAge70Combined = Calc.age70CombBene;
                //highlight the numbers in the grid
                Calc.Highlight(GridSpouse, Calculate.Calc.calc_solo.Divorced, Constants.GridSpouse);
                //cummulative income at age 69
                Session[Constants.DivorceSpouseCumm] = Calc.DCummValue;
                //lable to shpw the annual income value at paid to wait age
                Session[Constants.DivorceAnualSpouse] = Calc.DivorceAnnualValue.ToString(Constants.AMT_FORMAT);
                //code to show the keys and steps on the Grid
                renderDataIntoGridStartegy1(Calc.DCummValue, Calc.SValueStarting, Calc.SAgeValue, Calc.DivorceSpousalBenefits, Calc.DivorceSpousalAge, Calc.AgeValueAt70, customerFirstName, Calc.AgeAtLastChange, Calc.DivorceAnnualValue);
                Globals.Instance.WifeNumberofMonthinNote = 0;
                Globals.Instance.strDivorceStarting = string.Empty;
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                Globals.Instance.BoolWHBSwitchedHusb = false;
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                //Calc.BuildSingle(GridLatest, Calc.calc_strategy.Standard, Calc.calc_solo.Divorced, Calc.calc_start.max70, ref NoteLatest);
                //int maxAge70Combined = Calc.age70CombBene; 
                #endregion Code build all visible Grids for Divorced case

                #region code to get highest and second highest grid
                var list = new List<KeyValuePair<string, int>>();
                //if there is change of spousal benefits when husb is above age 62 than we show the second strategy 
                //strategy starting benefits from wife current age and switching to spousal benefits when husb is older than age 62
                DateTime WifeDOB = DateTime.Parse(BirthHusbandYYYY.Text);
                int intHusbTotalMonths = Calc.CalculateAgeinTotalMonth(HusbDOB);
                int intWifeTotalMonths = Calc.CalculateAgeinTotalMonth(WifeDOB);

                if (HusbDOB > limit)
                {
                    if (Globals.Instance.BoolShowSecondDivorce)
                    {
                        if (intAgeHusb <= intAgeWife)
                        {
                            if (Globals.Instance.BoolSpousalGreaterAtFRAandAge70)
                            {
                                //if (intAgeHusb == intAgeWife)  // added on 17/10/2016.. compared total no. of months to show two strategies if husband is greater than ex-spouse..
                                if (intWifeTotalMonths == intHusbTotalMonths)
                                {
                                    list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                                    Globals.Instance.BoolShowSecondDivorce = false;
                                    Globals.Instance.BoolShowSecondDivorcePDFLimit = true;
                                    Globals.Instance.BoolShowSecondDivorcePDF = true;
                                }
                                else
                                {
                                    if (intAgeWife < 62)
                                    {
                                        if (intWifeTotalMonths > intHusbTotalMonths)
                                        {
                                            list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                                            Globals.Instance.BoolShowSecondDivorce = false;
                                            Globals.Instance.BoolShowSecondDivorcePDFLimit = true;
                                            Globals.Instance.BoolShowSecondDivorcePDF = true;
                                        }
                                        else
                                        {
                                            list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                                            list.Add(new KeyValuePair<string, int>(Constants.BestStrategy, int.Parse(Session[Constants.DivorceAasapCumm].ToString())));
                                            Globals.Instance.BoolShowSecondDivorce = false;
                                            Globals.Instance.BoolShowSecondDivorcePDFLimit = true;
                                            Globals.Instance.BoolShowSecondDivorcePDF = true;
                                        }
                                    }
                                    else
                                    {
                                        list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                                        Globals.Instance.BoolShowSecondDivorce = false;
                                        Globals.Instance.BoolShowSecondDivorcePDFLimit = true;
                                        Globals.Instance.BoolShowSecondDivorcePDF = true;
                                    }
                                }
                            }
                            else
                            {
                                if (intAgeHusb == intAgeWife)
                                {
                                    list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                                    Globals.Instance.BoolShowSecondDivorce = false;
                                    Globals.Instance.BoolShowSecondDivorcePDFLimit = true;
                                    Globals.Instance.BoolShowSecondDivorcePDF = true;
                                }
                                else
                                {
                                    list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                                    list.Add(new KeyValuePair<string, int>(Constants.BestStrategy, int.Parse(Session[Constants.DivorceAasapCumm].ToString())));
                                    Globals.Instance.BoolShowSecondDivorce = false;
                                    Globals.Instance.BoolShowSecondDivorcePDFLimit = true;
                                    Globals.Instance.BoolShowSecondDivorcePDF = true;
                                }
                            }

                        }
                        else
                        {
                            list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                            Globals.Instance.BoolShowSecondDivorcePDF = false;
                            Globals.Instance.BoolShowSecondDivorce = false;
                            Globals.Instance.BoolShowSecondDivorcePDFLimit = true;
                        }
                    }
                    else
                    {


                        //if (Globals.Instance.BoolShowThirdDivorceStrategy)
                        //    list.Add(new KeyValuePair<string, int>(Constants.BestStrategy, int.Parse(Session[Constants.DivorceAasapCumm].ToString())));
                        //list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                        //list.Add(new KeyValuePair<string, int>(Constants.SuspendStrategy, int.Parse(Session[Constants.DivorceStandardCumm].ToString())));
                        //Globals.Instance.BoolShowSecondDivorcePDFLimit = false;
                        //Globals.Instance.BoolShowSecondDivorcePDF = false;

                        if (Globals.Instance.BoolShowThirdDivorceStrategy)
                        {
                            list.Add(new KeyValuePair<string, int>(Constants.BestStrategy, int.Parse(Session[Constants.DivorceAasapCumm].ToString())));
                            list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                            list.Add(new KeyValuePair<string, int>(Constants.SuspendStrategy, int.Parse(Session[Constants.DivorceStandardCumm].ToString())));
                        }
                        else
                        {
                            list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                            if (Session[Constants.DivorceSpouseCumm].ToString().Equals(Session[Constants.DivorceStandardCumm].ToString()))
                                list.Add(new KeyValuePair<string, int>(Constants.BestStrategy, int.Parse(Session[Constants.DivorceAasapCumm].ToString())));
                            else
                                list.Add(new KeyValuePair<string, int>(Constants.SuspendStrategy, int.Parse(Session[Constants.DivorceStandardCumm].ToString())));
                        }
                        Globals.Instance.BoolShowSecondDivorcePDFLimit = false;
                        Globals.Instance.BoolShowSecondDivorcePDF = false;

                    }
                }
                else
                {
                    if (Globals.Instance.BoolShowSecondConditionDivorce)
                    {
                        if (!Globals.Instance.BoolSpousalGreaterAtFRAandAge70)
                            list.Add(new KeyValuePair<string, int>(Constants.BestStrategy, int.Parse(Session[Constants.DivorceAasapCumm].ToString())));
                        list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                        Globals.Instance.BoolShowSecondDivorce = false;
                        Globals.Instance.BoolShowSecondDivorcePDFLimit = false;
                        Globals.Instance.BoolShowSecondDivorcePDF = true;
                    }
                    else
                    {
                        if (Globals.Instance.BoolSpGrternWrkgFRADivorce)
                        {
                            list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                            list.Add(new KeyValuePair<string, int>(Constants.SuspendStrategy, int.Parse(Session[Constants.DivorceStandardCumm].ToString())));
                            Globals.Instance.BoolShowSecondDivorcePDF = false;
                            Globals.Instance.BoolShowSecondDivorcePDFLimit = false;
                        }
                        else
                        {
                            list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                            Globals.Instance.BoolShowSecondDivorcePDF = false;
                            Globals.Instance.BoolShowSecondDivorce = false;
                            Globals.Instance.BoolShowSecondDivorcePDFLimit = true;
                        }
                    }
                }
                if (Globals.Instance.BoolHideAsapStrategyDivorce)
                {
                    if (list[0].Key.ToString().Equals(Constants.BestStrategy))
                    {
                        list.Remove(list[0]);
                        Globals.Instance.BoolShowSecondDivorcePDF = false;
                        Globals.Instance.BoolShowSecondDivorce = false;
                        Globals.Instance.BoolShowSecondDivorcePDFLimit = true;
                    }
                    else if (list[1].Key.ToString().Equals(Constants.BestStrategy))
                    {
                        list.Remove(list[1]);
                        Globals.Instance.BoolShowSecondDivorcePDF = false;
                        Globals.Instance.BoolShowSecondDivorce = false;
                        Globals.Instance.BoolShowSecondDivorcePDFLimit = true;
                    }
                }
                if (list.Count == 2)
                {
                    list.Sort(Compare2);
                    list.Reverse();
                }
                if (list.Count == 2)
                {
                    if (list[0].Key.ToString().Equals(Constants.FullStrategy))
                    {
                        lblCombinedIncome1.Text = Session[Constants.DivorceAnualSpouse].ToString();
                        if (Globals.Instance.BoolShowDivRestrictIncom)
                        {
                            lblRestrictAppIncomeTitle1.Visible = true;
                            lblRestrictAppIncomeValue1.Visible = true;
                            lblRestrictAppIncomeValue1.Text = Globals.Instance.IntShowRestrictIncomeForDivorce.ToString(Constants.AMT_FORMAT);
                        }
                        if (list[1].Key.ToString().Equals(Constants.BestStrategy))
                            lblCombinedIncome3.Text = Session[Constants.DivorceAnualAsap].ToString();
                        else if (list[1].Key.ToString().Equals(Constants.SuspendStrategy))
                            lblCombinedIncome3.Text = Session[Constants.DivorceAnualStandard].ToString();
                    }
                    else
                    {
                        if (list[0].Key.ToString().Equals(Constants.BestStrategy))
                            lblCombinedIncome1.Text = Session[Constants.DivorceAnualAsap].ToString();
                        else if (list[0].Key.ToString().Equals(Constants.SuspendStrategy))
                            lblCombinedIncome1.Text = Session[Constants.DivorceAnualStandard].ToString();
                        lblCombinedIncome3.Text = Session[Constants.DivorceAnualSpouse].ToString();
                        if (Globals.Instance.BoolShowDivRestrictIncom)
                        {
                            lblRestrictAppIncomeTitle3.Visible = true;
                            lblRestrictAppIncomeValue3.Visible = true;
                            lblRestrictAppIncomeValue3.Text = Globals.Instance.IntShowRestrictIncomeForDivorce.ToString(Constants.AMT_FORMAT);
                        }
                    }
                    lblPaidNumber1.Text = list[0].Value.ToString(Constants.AMT_FORMAT);
                    lblPaidNumber3.Text = list[1].Value.ToString(Constants.AMT_FORMAT);
                    Session[Constants.DMaxCumm] = list[0].Value.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showTwoStrategy('{0}','{1}');", list[0].Key.ToString(), list[1].Key.ToString()), true);
                }
                else if (list.Count == 3)
                {
                    lblCombinedIncome1.Text = Session[Constants.DivorceAnualAsap].ToString();
                    lblCombinedIncome2.Text = Session[Constants.DivorceAnualSpouse].ToString();
                    lblCombinedIncome3.Text = Session[Constants.DivorceAnualStandard].ToString();

                    if (Globals.Instance.BoolShowDivRestrictIncom)
                    {
                        lblRestrictAppIncomeTitle2.Visible = true;
                        lblRestrictAppIncomeValue2.Visible = true;
                        lblRestrictAppIncomeValue2.Text = Globals.Instance.IntShowRestrictIncomeForDivorce.ToString(Constants.AMT_FORMAT);
                    }


                    lblPaidNumber1.Text = list[0].Value.ToString(Constants.AMT_FORMAT);
                    lblPaidNumber2.Text = list[1].Value.ToString(Constants.AMT_FORMAT);
                    lblPaidNumber3.Text = list[2].Value.ToString(Constants.AMT_FORMAT);
                    Session[Constants.DMaxCumm] = list[0].Value.ToString();
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showThreeStrategy('{0}','{1}','{2}');", list[0].Key.ToString(), list[1].Key.ToString(), list[2].Key.ToString()), true);
                }
                else
                {
                    Session[Constants.DMaxCumm] = list[0].Value.ToString();
                    lblPaidNumber2.Text = list[0].Value.ToString(Constants.AMT_FORMAT);
                    lblCombinedIncome2.Text = Session[Constants.DivorceAnualSpouse].ToString();
                    if (Globals.Instance.BoolShowDivRestrictIncom)
                    {
                        lblRestrictAppIncomeTitle2.Visible = true;
                        lblRestrictAppIncomeValue2.Visible = true;
                        lblRestrictAppIncomeValue2.Text = Globals.Instance.IntShowRestrictIncomeForDivorce.ToString(Constants.AMT_FORMAT);
                    }
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showOneStrategy('{0}');", list[0].Key.ToString()), true);
                }
                //Add Paid To Wait Amount and Annual Income Amount in list
                AddPaidAndAnnualValForPDF();
                #endregion code to get highest and second highest grid

                #region Code show popup and hide and show Panel
                // Hide/show script
                //StringBuilder sb = new StringBuilder();
                //sb.Append("<script language=javascript>");
                //sb.Append("function HideShow(theLabel) { ");
                //sb.Append("var explanation;");
                //sb.Append("if (theLabel.innerText == '(Show)') {");
                //sb.Append("explanation = document.getElementById(theLabel.id.substr(0,18) + 'Explain' + theLabel.id.substr(27));");
                //sb.Append("explanation.className = 'styleShow';");
                //sb.Append("theLabel.innerText = '(Hide)'; ");
                //sb.Append("$(theLabel).siblings().removeClass('styleHide'); ");
                //sb.Append("$(theLabel).siblings().children().removeClass('styleHide'); ");
                //sb.Append("$(theLabel).siblings().addClass('styleShow'); }");
                //sb.Append("else {");
                //sb.Append("explanation = document.getElementById(theLabel.id.substr(0,18) + 'Explain' + theLabel.id.substr(27));");
                //sb.Append("explanation.className = 'styleHide';");
                //sb.Append("theLabel.innerText = '(Show)'; ");
                //sb.Append("$(theLabel).siblings().removeClass('styleShow'); ");
                //sb.Append("$(theLabel).siblings().addClass('styleHide'); ");
                //sb.Append("$(theLabel).prev().removeClass('styleHide'); ");
                //sb.Append("$(theLabel).prev().addClass('styleShow'); ");
                //sb.Append("$(theLabel).prev().prev().removeClass('styleHide'); ");
                //sb.Append("$(theLabel).prev().prev().addClass('styleShow'); }");
                //sb.Append("}");
                //sb.Append("</script>");
                //ClientScript.RegisterStartupScript(typeof(string), "MyScript", sb.ToString());
                //NoteStandard.CssClass = "styleShow";
                PanelSpouse.Visible = true;
                ExplainSpouse.Visible = true;
                //  If spousal doesn't work, hide it
                //if (spouseAge70Combined > stdAge70Combined && cummStd == cummSpouse)
                //{
                //    PanelStandard.Visible = false;
                //    ExplainStandard.Visible = false;
                //}
                //else
                //{
                //    PanelStandard.Visible = true;
                //    ExplainStandard.Visible = true;
                //}
                //  Show the grids
                PanelGrids.Visible = true;
                #endregion Code show popup and hide and show Panel
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Used to set key and steps values to display on PDF 
        /// </summary>
        /// <param name="strGridName">Grid Name</param>
        public void SetKeyAndStepsValuesForPDF(string strGridName)
        {
            try
            {
                if (strGridName.Equals(Constants.GridAsap))
                {
                    CommonVariables.strKey1 = lblKey5.Text;
                    CommonVariables.strKey2 = lblKey6.Text;
                    CommonVariables.strKey3 = lblKey7.Text;
                    CommonVariables.strStep1 = lblSteps5.Text;
                    CommonVariables.strStep2 = lblSteps6.Text;
                    CommonVariables.strStep3 = lblSteps7.Text;

                    CommonVariables.strNoteWife0 = lblNoteWife0.Text;
                    CommonVariables.strNoteWife3 = lblNoteWife3.Text;
                    CommonVariables.strNoteWife6 = lblNoteWife6.Text;
                }
                else if (strGridName.Equals(Constants.GridStandard))
                {
                    CommonVariables.strKey4 = lblNewKey.Text;
                    CommonVariables.strKey5 = lblKey3.Text;
                    CommonVariables.strKey6 = lblKey4.Text;
                    CommonVariables.strKey7 = lblKey8.Text;
                    CommonVariables.strStep4 = lblSteps3.Text;
                    CommonVariables.strStep5 = lblSteps4.Text;
                    CommonVariables.strStep6 = lblSteps8.Text;

                    CommonVariables.strNoteWife1 = lblNoteWife1.Text;
                    CommonVariables.strNoteWife4 = lblNoteWife4.Text;
                    CommonVariables.strNoteWife7 = lblNoteWife7.Text;
                }
                else if (strGridName.Equals(Constants.GridSpouse))
                {
                    CommonVariables.strKeyDivorceRestrictIncome = string.Empty;
                    CommonVariables.strKey8 = lblKey1.Text;
                    CommonVariables.strKey9 = lblKey2.Text;
                    CommonVariables.strKey10 = lblKey9.Text;
                    CommonVariables.strKeyDivorceRestrictIncome = lblRestrictIcomeKey.Text;
                    CommonVariables.strStep7 = lblSteps1.Text;
                    CommonVariables.strStep8 = lblSteps2.Text;
                    CommonVariables.strStep9 = lblSteps9.Text;

                    CommonVariables.strNoteWife2 = lblNoteWife2.Text;
                    CommonVariables.strNoteWife5 = lblNoteWife5.Text;
                    CommonVariables.strNoteWife8 = lblNoteWife8.Text;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// redirect advisor back to the managecustomer page
        /// </summary>
        /// <param name="strGridName"></param>
        protected void Backto_Managecustomer(object sender, EventArgs e)
        {
            try
            {
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor))
                    Response.Redirect(Constants.RedirectManageCustomers, true);
                else if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                    Response.Redirect(Constants.RedirectManageCustomersWithAdvisorID + Session[Constants.TempAdvisorID].ToString(), false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// add the annual income and paid to wait numbers in a list to be displayed
        /// </summary>
        private void AddPaidAndAnnualValForPDF()
        {

            try
            {
                //Add Annual income values to show in PDF
                Calc.listAnnualAmount.Clear();
                if (!string.IsNullOrEmpty(lblCombinedIncome1.Text))
                    Calc.listAnnualAmount.Add(lblCombinedIncome1.Text);
                if (!string.IsNullOrEmpty(lblCombinedIncome2.Text))
                    Calc.listAnnualAmount.Add(lblCombinedIncome2.Text);
                if (!string.IsNullOrEmpty(lblCombinedIncome3.Text))
                    Calc.listAnnualAmount.Add(lblCombinedIncome3.Text);
                //Calc.listAnnualAmount.Add(lblCombinedIncome2.Text);

                //Add Paid to Wait values to show in PDF
                Calc.listPaidToWaitAmount.Clear();
                if (!string.IsNullOrEmpty(lblPaidNumber1.Text))
                    Calc.listPaidToWaitAmount.Add(lblPaidNumber1.Text);
                if (!string.IsNullOrEmpty(lblPaidNumber2.Text))
                    Calc.listPaidToWaitAmount.Add(lblPaidNumber2.Text);
                if (!string.IsNullOrEmpty(lblPaidNumber3.Text))
                    Calc.listPaidToWaitAmount.Add(lblPaidNumber3.Text);

                //Add Restricted Application Amount
                Calc.listRestrictAppIcome.Clear();
                Calc.listRestrictAppIcome.Add(lblRestrictAppIncomeValue1.Text);
                Calc.listRestrictAppIcome.Add(lblRestrictAppIncomeValue2.Text);
                Calc.listRestrictAppIcome.Add(lblRestrictAppIncomeValue3.Text);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Export Grid View Content into PDF
        /// </summary>
        /// <param name="gridView"></param>
        public void exportAllToPdf()
        {
            try
            {
                #region Code to set values to show in PDF

                DataTable dtCustomerDetails;
                decimal SpouseBenefit, Benefit;
                string Bene = string.Empty, BeneSpouse = string.Empty;
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor) || Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                {
                    dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                    Bene = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                    BeneSpouse = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                }
                else
                {
                    dtCustomerDetails = customer.getCustomerFinancialDetailsByStatus(Session[Constants.SessionUserId].ToString(), Constants.Divorced);
                    Bene = dtCustomerDetails.Rows[0][Constants.SecurityInfoMarrDivorceRetAgeBenefit].ToString();
                    BeneSpouse = dtCustomerDetails.Rows[0][Constants.SecurityInfoSlimyExsRetAgeBenefit].ToString();
                }
                customerFirstName = dtCustomerDetails.Rows[0][Constants.CustomerFirstName].ToString();
                decimal.TryParse(Bene, out Benefit);
                decimal.TryParse(BeneSpouse, out SpouseBenefit);
                SiteNew.renderGridViewIntoStackedChart(GridAsap, ChartAsap, LabelAsap, Constants.Divorced, false);
                /* Create the PDF Document */
                MemoryStream objMemory = new MemoryStream();
                Document pdfDoc = new Document(PageSize.A4, Constants.zero, Constants.zero, -15f, 55f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, objMemory);
                string sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                string sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                string strOwnBenefit = Benefit.ToString(Constants.AMT_FORMAT);
                string strSpousalbenefit = SpouseBenefit.ToString(Constants.AMT_FORMAT);
                string strEmail = string.Empty;
                #endregion Code to set values to show in PDF

                #region code to set First page of the PDF
                /* Open the stream */
                pdfDoc.Open();
                writer.PageEvent = new PDFEvent();
                // First PDF Page
                SiteNew.PdfFrontPage(dtCustomerDetails, ref pdfDoc);
                //Add explanation of strategy in report while sending it to client by email
                pdfDoc.NewPage();
                //  SiteNew.convertGridToImage(GridAsap, ref pdfDoc);
                // Second PDF Page
                //get a table for 3 colums
                PdfPTable table = new PdfPTable(3);
                //set the column width
                float[] widths = new float[] { 1f, 1f, 1f };
                //set the table width
                table.TotalWidth = 550f;
                //fix the table width
                table.LockedWidth = true;
                //adding space in the table
                table.DefaultCell.Padding = 3f;
                table.SetWidths(widths);
                //remove the table borders
                table.DefaultCell.Border = PdfPCell.NO_BORDER;
                //set the font for the text in the table cells
                BaseFont customfont = BaseFont.CreateFont(HttpContext.Current.Server.MapPath("~") + Constants.PdfRobotoFontStyle, BaseFont.CP1252, BaseFont.EMBEDDED);
                iTextSharp.text.Font font = new iTextSharp.text.Font(customfont, 16);
                //add 1 row to the table as customer name
                table.AddCell(new Phrase(Constants.customerName, font));
                table.AddCell(new Phrase(dtCustomerDetails.Rows[0][Constants.CustomerFirstName].ToString(), font));
                table.AddCell(new Phrase(Constants.ExSpouseName, font));
                //add 2 row to the table as date of births
                table.AddCell(new Phrase(Constants.customerDob, font));
                table.AddCell(new Phrase(string.Format(Constants.stringDateFormat, Convert.ToDateTime(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString())), font));
                table.AddCell(new Phrase(string.Format(Constants.stringDateFormat, Convert.ToDateTime(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString())), font));
                //add 3 row to the table as full retirement ages
                table.AddCell(new Phrase(Constants.customerFullRetirementAge, font));
                table.AddCell(new Phrase(sWifeFRA, font));
                table.AddCell(new Phrase(sHusbFRA, font));
                //add 4 row to the table as PIA's
                table.AddCell(new Phrase(Constants.customerPIA, font));
                table.AddCell(new Phrase(strOwnBenefit, font));
                table.AddCell(new Phrase(strSpousalbenefit, font));
                //add 5 row to the table as maritial status
                table.AddCell(new Phrase(Constants.PdfCustomerMaritalStatus, font));
                table.AddCell(new Phrase(dtCustomerDetails.Rows[0][Constants.CustomerMaritalStatus].ToString(), font));
                table.AddCell(string.Empty);
                //add 6 row to the table as COLA percentage
                table.AddCell(new Phrase(Constants.customerIncludeCOLA, font));
                table.AddCell(new Phrase(Constants.COLAPercentage, font));
                table.AddCell(string.Empty);


                //add 7 row to the table as COLA percentage
                //table.AddCell(new Phrase("Stratergy1", font));
                //table.AddCell(new Phrase("Stratergy2", font));
                //table.AddCell(new Phrase("Stratergy3", font));
                //table.AddCell(string.Empty);



                //add table to the document
                pdfDoc.Add(table);
                pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(string.Format(Constants.customerName + dtCustomerDetails.Rows[0][Constants.CustomerFirstName].ToString()), true, false, false));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.customerDob + string.Format(Constants.stringDateFormat, Convert.ToDateTime(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString())) + "   " + string.Format(Constants.stringDateFormat, Convert.ToDateTime(dtCustomerDetails.Rows[0][Constants.SecurityInfoExSpousesBirthDate].ToString())), true, false, false));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.customerFullRetirementAge + string.Format(sWifeFRA + "         " + sHusbFRA), true, false, false));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.customerPIA + Benefit.ToString(Constants.AMT_FORMAT) + "             " + SpouseBenefit.ToString(Constants.AMT_FORMAT), true, false, false));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.PdfCustomerMaritalStatus + dtCustomerDetails.Rows[0][Constants.CustomerMaritalStatus].ToString(), true, false, false));
                //if (dtCustomerDetails.Rows[0][Constants.SecurityInfoCola].ToString() == "T")
                //    pdfDoc.Add(SiteNew.generateParagraph(Constants.customerIncludeCOLA, true, false, false));
                //else
                //    pdfDoc.Add(SiteNew.generateParagraph(Constants.customerIncludeCOLA, true, false, false));
                pdfDoc.Add(SiteNew.generateImage(Constants.pdfHorizontalLine, Constants.pdfImageWidth, 1f));
                pdfDoc.Add(new Paragraph(" "));
                #endregion code to set First page of the PDF

                #region code to find the best plan and print accordingly on the PDF
                /* Get Customer Details for showing up in PDF  */
                var list = new List<KeyValuePair<string, int>>();
                string sFirstGrid = string.Empty;
                string sSecondGrid = string.Empty;
                string sThirdGrid = string.Empty;
                Calc calc = new Calc();
                DateTime HusbDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                DateTime WifeDOB = DateTime.Parse(BirthHusbandYYYY.Text);
                int intHusbTotalMonths = calc.CalculateAgeinTotalMonth(HusbDOB);
                int intWifeTotalMonths = calc.CalculateAgeinTotalMonth(WifeDOB);

                if (HusbDOB > limit)
                {
                    if (Globals.Instance.BoolShowSecondDivorcePDFLimit)
                    {
                        //list.Add(new KeyValuePair<string, int>(Constants.GridSpouse, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                        if (intAgeHusb <= intAgeWife)
                        {
                            if (Globals.Instance.BoolSpousalGreaterAtFRAandAge70)
                            {
                                //if (intAgeWife == intAgeHusb) // added on 17/10/2016.. compared total no. of months to show two strategies if husband is greater than ex-spouse..
                                if (intWifeTotalMonths == intHusbTotalMonths)
                                {
                                    list.Add(new KeyValuePair<string, int>(Constants.GridSpouse, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                                }
                                else
                                {
                                    if (intAgeWife < 62)
                                    {
                                        if (intWifeTotalMonths > intHusbTotalMonths)
                                        {
                                            list.Add(new KeyValuePair<string, int>(Constants.GridSpouse, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                                        }
                                        else
                                        {
                                            list.Add(new KeyValuePair<string, int>(Constants.GridSpouse, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                                            list.Add(new KeyValuePair<string, int>(Constants.GridAsap, int.Parse(Session[Constants.DivorceAasapCumm].ToString())));
                                        }
                                    }
                                    else
                                    {
                                        list.Add(new KeyValuePair<string, int>(Constants.GridSpouse, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                                    }
                                }
                            }
                            else
                            {
                                if (intAgeWife == intAgeHusb)
                                {
                                    list.Add(new KeyValuePair<string, int>(Constants.GridSpouse, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                                }
                                else
                                {
                                    list.Add(new KeyValuePair<string, int>(Constants.GridSpouse, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                                    list.Add(new KeyValuePair<string, int>(Constants.GridAsap, int.Parse(Session[Constants.DivorceAasapCumm].ToString())));
                                }
                            }
                        }
                        else
                        {
                            list.Add(new KeyValuePair<string, int>(Constants.GridSpouse, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                        }
                    }
                    else
                    {
                        if (Globals.Instance.BoolShowThirdDivorceStrategy)
                        {
                            list.Add(new KeyValuePair<string, int>(Constants.GridAsap, int.Parse(Session[Constants.DivorceAasapCumm].ToString())));
                            list.Add(new KeyValuePair<string, int>(Constants.GridSpouse, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                            list.Add(new KeyValuePair<string, int>(Constants.GridStandard, int.Parse(Session[Constants.DivorceStandardCumm].ToString())));
                        }
                        else
                        {
                            list.Add(new KeyValuePair<string, int>(Constants.GridSpouse, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                            if (Session[Constants.DivorceSpouseCumm].ToString().Equals(Session[Constants.DivorceStandardCumm].ToString()))
                                list.Add(new KeyValuePair<string, int>(Constants.GridAsap, int.Parse(Session[Constants.DivorceAasapCumm].ToString())));
                            else
                                list.Add(new KeyValuePair<string, int>(Constants.GridStandard, int.Parse(Session[Constants.DivorceStandardCumm].ToString())));
                        }
                    }
                }
                else
                {
                    if (Globals.Instance.BoolShowSecondDivorcePDF)
                    {
                        if (!Globals.Instance.BoolSpousalGreaterAtFRAandAge70)
                            list.Add(new KeyValuePair<string, int>(Constants.GridAsap, int.Parse(Session[Constants.DivorceAasapCumm].ToString())));
                        list.Add(new KeyValuePair<string, int>(Constants.GridSpouse, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                    }
                    else
                    {

                        if (Globals.Instance.BoolSpGrternWrkgFRADivorce)
                        {
                            list.Add(new KeyValuePair<string, int>(Constants.GridSpouse, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                            list.Add(new KeyValuePair<string, int>(Constants.GridStandard, int.Parse(Session[Constants.DivorceStandardCumm].ToString())));
                        }
                        else
                        {
                            list.Add(new KeyValuePair<string, int>(Constants.GridSpouse, int.Parse(Session[Constants.DivorceSpouseCumm].ToString())));
                            //list.Add(new KeyValuePair<string, int>(Constants.GridStandard, int.Parse(Session[Constants.DivorceStandardCumm].ToString())));
                        }
                    }

                }
                if (Globals.Instance.BoolHideAsapStrategyDivorce)
                {
                    if (list.Count == 2)
                    {
                        if (list[0].Key.ToString().Equals(Constants.GridAsap))
                            list.Remove(list[0]);
                        else if (list[1].Key.ToString().Equals(Constants.GridAsap))
                            list.Remove(list[1]);
                    }
                }
                if (list.Count == 2)
                {
                    list.Sort(Compare2);
                    list.Reverse();
                    sFirstGrid = list[0].Key.ToString();
                    sSecondGrid = list[1].Key.ToString();
                }
                else if (list.Count == 3)
                {
                    sFirstGrid = list[0].Key.ToString();
                    sSecondGrid = list[1].Key.ToString();
                    sThirdGrid = list[2].Key.ToString();
                }
                else
                {
                    sFirstGrid = list[0].Key.ToString();
                }
                GridView gFirstGrid = new GridView();
                GridView gSecondGrid = new GridView();
                GridView gThirdGrid = new GridView();
                Chart cFirstChart = new Chart();
                Chart cSecondChart = new Chart();
                Chart cThirdChart = new Chart();
                Label lFirstLable = new Label();
                Label lSecondLabel = new Label();
                Label lThirdLabel = new Label();
                if (sFirstGrid.Equals(Constants.GridAsap))
                {
                    gFirstGrid = GridAsap;
                    cFirstChart = ChartAsap;
                    lFirstLable.Text = Constants.Strategy1;
                }
                else if (sFirstGrid.Equals(Constants.GridStandard))
                {
                    gFirstGrid = GridStandard;
                    cFirstChart = ChartStandard;
                    lFirstLable.Text = Constants.Strategy1;
                }
                if (sFirstGrid.Equals(Constants.GridSpouse))
                {
                    gFirstGrid = GridSpouse;
                    cFirstChart = ChartSpouse;
                    lFirstLable.Text = Constants.Strategy1;
                }

                if (sSecondGrid.Equals(Constants.GridAsap))
                {
                    gSecondGrid = GridAsap;
                    cSecondChart = ChartAsap;
                    lSecondLabel.Text = Constants.Strategy2;
                }
                if (sSecondGrid.Equals(Constants.GridStandard))
                {
                    gSecondGrid = GridStandard;
                    cSecondChart = ChartStandard;
                    lSecondLabel.Text = Constants.Strategy2;
                }
                if (sSecondGrid.Equals(Constants.GridSpouse))
                {
                    gSecondGrid = GridSpouse;
                    cSecondChart = ChartSpouse;
                    lSecondLabel.Text = Constants.Strategy2;
                }
                if (sThirdGrid.Equals(Constants.GridAsap))
                {
                    gThirdGrid = GridAsap;
                    cThirdChart = ChartAsap;
                    lThirdLabel.Text = Constants.Strategy3;
                }
                if (sThirdGrid.Equals(Constants.GridStandard))
                {
                    gThirdGrid = GridStandard;
                    cThirdChart = ChartStandard;
                    lThirdLabel.Text = Constants.Strategy3;
                }
                if (sThirdGrid.Equals(Constants.GridSpouse))
                {
                    gThirdGrid = GridSpouse;
                    cThirdChart = ChartSpouse;
                    lThirdLabel.Text = Constants.Strategy3;
                }


                //Add Strategy Details Table in PDF 
                if (Globals.Instance.BoolShowSecondDivorcePDFLimit)
                {
                    SiteNew.CreateStrategyDetailsTableForPDF(ref pdfDoc,
                                                                Constants.PaidtoWaitTextForPDF1,
                                                                Calc.listPaidToWaitAmount,
                                                                Constants.AnnualIncomeStratComplete1,
                                                                Calc.listAnnualAmount,
                                                                Calc.listAnnualAmount.Count,
                                                                Constants.Divorced);
                }
                else
                {
                    SiteNew.CreateStrategyDetailsTableForPDF(ref pdfDoc,
                                                                Constants.PaidtoWaitTextForPDF,
                                                                Calc.listPaidToWaitAmount,
                                                                Constants.AnnualIncomeStratComplete,
                                                                Calc.listAnnualAmount,
                                                                Calc.listAnnualAmount.Count,
                                                                Constants.Divorced);
                }


                //Add explanation of strategy in report 
                AddInstructionBeforeSeeStrategy(ref pdfDoc);
                AddExplainationToPDF(ref pdfDoc);
                pdfDoc.NewPage();

                if (list.Count == 2)
                {
                    commonFunctions(gFirstGrid, cFirstChart, lFirstLable, sFirstGrid, customerFirstName, ref pdfDoc, Constants.one);
                    commonFunctions(gSecondGrid, cSecondChart, lSecondLabel, sSecondGrid, customerFirstName, ref pdfDoc, Constants.two);
                }
                else if (list.Count == 3)
                {
                    commonFunctions(gFirstGrid, cFirstChart, lFirstLable, sFirstGrid, customerFirstName, ref pdfDoc, Constants.one);
                    commonFunctions(gSecondGrid, cSecondChart, lSecondLabel, sSecondGrid, customerFirstName, ref pdfDoc, Constants.two);
                    commonFunctions(gThirdGrid, cThirdChart, lThirdLabel, sThirdGrid, customerFirstName, ref pdfDoc, Constants.three);
                }
                else
                {
                    commonFunctions(gFirstGrid, cFirstChart, lFirstLable, sFirstGrid, customerFirstName, ref pdfDoc, Constants.one);
                }
                #endregion code to find the best plan and print accordingly on the PDF

                //Add discalimer at the end of report
                SiteNew.AddDisclaimerIntoPDFReport(ref pdfDoc);


                #region Email Report to Client
                if (Globals.Instance.BoolEmailReport)
                {
                    strEmail = dtCustomerDetails.Rows[0][Constants.UserEmail].ToString();
                    if (!String.IsNullOrEmpty(Globals.Instance.strAdvisorEmail))
                    {
                        if (customer.MailReport(pdfDoc, Globals.Instance.strAdvisorEmail, objMemory, writer, customerFirstName))
                            Globals.Instance.BoolIsMailSent = true;
                        Globals.Instance.strAdvisorEmail = string.Empty;
                    }
                    else
                    {
                        if (customer.MailReport(pdfDoc, strEmail, objMemory, writer, customerFirstName))
                            Globals.Instance.BoolIsMailSent = true;
                    }
                }

                #endregion Email Report to Client

                #region code to set the other PDF values and close the PDF
                else
                {
                    pdfDoc.Close();
                    /* Create object for Customer and Pdf File */
                    PdfFilesTO objPdfFile = new PdfFilesTO();
                    /* Assign data into pdf file variable */
                    objPdfFile.PdfContent = objMemory.GetBuffer();
                    objPdfFile.PdfName = Constants.pdfFileName + dtCustomerDetails.Rows[0][Constants.CustomerLastName].ToString() + "_" + DateTime.Now.ToString(Constants.DateFormat) + Constants.pdfFileExtension;
                    objPdfFile.CustomerID = Session[Constants.SessionUserId].ToString();
                    objPdfFile.ContentType = Constants.pdfContentType;
                    customer.insertPdfDetails(objPdfFile);
                    //Download report into PDF
                    Response.OutputStream.Write(objMemory.GetBuffer(), 0, objMemory.GetBuffer().Length);
                    Response.ContentType = Constants.pdfContentType;
                    Response.AddHeader(Constants.pdfContentDisposition, Constants.pdfFileName + dtCustomerDetails.Rows[0][Constants.CustomerLastName].ToString() + "_" + DateTime.Now.ToString(Constants.DateFormat) + Constants.pdfFileExtension);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                    Response.End();

                }
                #endregion code to set the other PDF values and close the PDF

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// commonFunctions
        /// code to set the common structure of teh PDF pages
        /// </summary>
        /// <param name="gFirstGrid"></param>
        /// <param name="cFirstChart"></param>
        /// <param name="lFirstLable"></param>
        /// <param name="sGridName"></param>
        /// <param name="customerFirstName"></param>
        /// <param name="pdfDoc"></param>
        /// <param name="number"></param>
        public void commonFunctions(GridView gFirstGrid, Chart cFirstChart, Label lFirstLable, string sGridName, string customerFirstName, ref Document pdfDoc, string number)
        {
            try
            {
                Customers customer = new Customers();
                string HusbFra = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                string WifeFra = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                if (!number.Equals(Constants.one))
                {
                    pdfDoc.NewPage();
                }
                #region explanation steps after the grid in PDF
                // to show the explanation steps after the grid in PDF
                //if (sGridName.Equals(Constants.GridAsap))
                //    SiteNew.strWifeExplanation = lblNoteWife0.Text;
                //else if (sGridName.Equals(Constants.GridStandard))
                //    SiteNew.strWifeExplanation = lblNoteWife1.Text;
                //else
                //    SiteNew.strWifeExplanation = lblNoteWife2.Text;
                #endregion explanation steps after the grid in PDF
                SiteNew.renderGridViewIntoStackedChart(gFirstGrid, cFirstChart, lFirstLable, Constants.Divorced, false);
                SiteNew.commonPageLayout(lFirstLable, gFirstGrid, ref pdfDoc, customerFirstName, Constants.Divorced, sGridName, WifeFra, HusbFra.ToString());
                /* Add new page in pdf */
                pdfDoc.NewPage();
                /* Print chart into image in pdf */
                // pdfDoc.Add(SiteNew.generateChartHeading(lFirstLable.Text));
                SiteNew.convertChartToImage(cFirstChart, ref pdfDoc);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// code to close the pop up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                //Previous Redirection
                //redirect to divorce page when the cancel button is clicked on the subscription popup
                //Response.Redirect(Constants.RedirectCalcDivorced, false);
                Response.Redirect(Constants.RedirectInitialSocialSecurity, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.TryAgain;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Requested assumptions change
        /// </summary>
        protected void BackToParms()
        {
            try
            {
                PanelGrids.Visible = false;
                PanelEntry.Visible = true;
                PanelParms.Visible = false;
                BirthWifeMM.Focus();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// code to get the keys and steps for the first grid ie GridSpouse on the website below the Grid in the strategy
        /// </summary>
        /// <param name="cumm"></param>
        /// <param name="startingValue"></param>
        /// <param name="ageWife"></param>
        /// <param name="Valueat70"></param>
        /// <param name="strUserFirstName"></param>
        public void renderDataIntoGridStartegy1(decimal cumm, decimal startingValue, string ageWife, decimal spousalBenefits, string spousalAge, decimal Valueat70, string strUserFirstName, string AgeAtLastChange, decimal AnualIncome)
        {
            try
            {
                DateTime HusbDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                Customers customer = new Customers();
                string sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                string ageWifeStarting = ageWife;
                string strWifeFRA = sWifeFRA.Substring(0, 2);
                bool IsContainRestrictIncome = false;
                if (spousalBenefits != 0 && Valueat70 == 0)
                {
                    lblKey2.Text = string.Format(spousalBenefits.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps25 + strUserFirstName + Constants.stepsNewSpousal);
                }
                else if (Valueat70 != 0)
                {
                    lblKey2.Text = string.Format(Valueat70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps25 + strUserFirstName + Constants.WorkBenefitsKey);
                }
                else if (spousalBenefits == 0 && Valueat70 == 0)
                {
                    lblKey2.Text = string.Format(startingValue.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps25 + strUserFirstName + Constants.stepsNewWorking);
                }
                lblKey2.Visible = true;

                if (Globals.Instance.WifeNumberofMonthinNote != 0)
                {
                    lblNoteWife2.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserFirstName;
                    ageWifeStarting = Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + " years and " + Globals.Instance.WifeNumberofMonthsinSteps + " months ";
                }

                if (Globals.Instance.WifeNumberofMonthsBelowGrid != 0)
                {
                    lblNoteWife8.Text = "** includes " + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    sWifeFRA = Globals.Instance.WifeNumberofYears + " years and " + Globals.Instance.WifeNumberofMonthsAboveGrid + " months ";
                }
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblNoteWife5.Text = "* includes " + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName; ;
                }

                if (HusbDOB > limit)
                {
                    if (Globals.Instance.BoolWorkingBenefitsShown)
                    {
                        if (cumm != 0)
                        {
                            if (spousalBenefits == 0)
                                lblKey1.Text = string.Format(cumm.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + strUserFirstName + Constants.keyFigures7 + Constants.CummuText1s + strUserFirstName + Constants.CummuText2Work + startingValue.ToString(Constants.AMT_FORMAT) + Constants.CummuText3s + ageWife + ", " + strUserFirstName + Constants.CummuText4s + cumm.ToString(Constants.AMT_FORMAT) + Constants.CummuText5s);
                            else
                                lblKey1.Text = string.Format(cumm.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + strUserFirstName + Constants.keyFigures7 + Constants.CummuText1s + strUserFirstName + Constants.CummuText2s + startingValue.ToString(Constants.AMT_FORMAT) + Constants.CummuText3s + ageWife + ", " + strUserFirstName + Constants.CummuText4s + cumm.ToString(Constants.AMT_FORMAT) + Constants.CummuText5s);
                            lblKey1.Visible = true;
                        }
                        else
                        {
                            lblKey1.Visible = false;
                        }
                        if (spousalBenefits == 0)
                        {
                            if (ageWife == sWifeFRA.Substring(0, 2))
                                lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            else
                                lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWife + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        }
                        else
                        {
                            lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps1 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.spousaltextdivorce);
                        }
                        lblSteps9.Text = string.Format(Constants.Line2 + Constants.Age + " " + Calc.AnnualIncomeAge + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                        lblSteps2.Visible = false;
                    }
                    else
                    {
                        if (cumm != 0)
                        {
                            if (spousalBenefits == 0)
                                lblKey1.Text = string.Format(cumm.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + strUserFirstName + Constants.keyFigures7 + Constants.CummuText1s + strUserFirstName + Constants.CummuText2Work + startingValue.ToString(Constants.AMT_FORMAT) + Constants.CummuText3s + ageWife + ", " + strUserFirstName + Constants.CummuText4s + cumm.ToString(Constants.AMT_FORMAT) + Constants.CummuText5s);
                            else
                                lblKey1.Text = string.Format(cumm.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + strUserFirstName + Constants.keyFigures7 + Constants.CummuText1s + strUserFirstName + Constants.CummuText2s + startingValue.ToString(Constants.AMT_FORMAT) + Constants.CummuText3s + ageWife + ", " + strUserFirstName + Constants.CummuText4s + cumm.ToString(Constants.AMT_FORMAT) + Constants.CummuText5s);
                            lblKey1.Visible = true;
                        }
                        else
                        {
                            lblKey1.Visible = false;
                        }
                        //check if the value at age 70 is 0
                        if (Valueat70 != 0)
                        {
                            //addd the step 2 for age 70 of the user
                            lblSteps2.Text = string.Format(Constants.Line2 + Constants.Age + "  " + Constants.Number70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps18 + Valueat70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                            lblSteps2.Visible = true;
                            lblSteps9.Text = string.Format(Constants.Line3 + Constants.Age + " " + Calc.AnnualIncomeAge + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                        }
                        else
                        {
                            lblSteps2.Visible = false;
                            lblSteps9.Text = string.Format(Constants.Line2 + Constants.Age + " " + Calc.AnnualIncomeAge + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                        }
                        //show step 1 for the starting value for user
                        if (Convert.ToInt32(ageWife) <= Convert.ToInt32(sWifeFRA.Substring(0, 2)))
                        {
                            if (Valueat70 == 0)
                                lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps1 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.spousaltextdivorce);
                            else
                            {    //lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.DivorceSurvior + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserFirstName + Constants.DivorceSpouse + Constants.DivorceSpouse1 + strUserFirstName + Constants.DivorceSpouse2 + strUserFirstName + Constants.DivorceSpouse3);
                                lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserFirstName + Constants.DivorceSurvior + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.strClaimRestrictSpousal1 + strUserFirstName + Constants.strClaimRestrictSpousal2 + strUserFirstName + Constants.strClaimRestrictSpousal3 + strUserFirstName + Constants.strClaimRestrictSpousal4, true, false, false);
                                IsContainRestrictIncome = true;
                            }
                        }
                        else
                        {
                            if (Valueat70 == 0)
                            {
                                //added on 02/08/2016
                                if (ageWife.Equals(sWifeFRA))
                                    lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps1 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.spousaltextdivorce);
                                else
                                    lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.steps1 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.spousaltextdivorce);
                            }
                            else
                            {
                                //lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWife + Constants.DivorceSurvior + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserFirstName + Constants.DivorceSpouse + Constants.DivorceSpouse1 + strUserFirstName + Constants.DivorceSpouse2 + strUserFirstName + Constants.DivorceSpouse3);
                                lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.DivorceSurvior + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.strClaimRestrictSpousal1 + strUserFirstName + Constants.strClaimRestrictSpousal2 + strUserFirstName + Constants.strClaimRestrictSpousal3 + strUserFirstName + Constants.strClaimRestrictSpousal4, true, false, false);
                                IsContainRestrictIncome = true;
                            }
                        }
                    }
                    lblKey9.Text = string.Format(AnualIncome.ToString(Constants.AMT_FORMAT) + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey);
                }
                else
                {
                    if (cumm != 0)
                    {
                        if (!AgeAtLastChange.Equals(Constants.Number70))
                            lblKey1.Text = string.Format(cumm.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + strUserFirstName + Constants.keyFigures7 + Constants.CummuText1s + strUserFirstName + Constants.CummuText2s + spousalBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3s + ageWife + Constants.CommaandSpace + strUserFirstName + Constants.CummuText4s + cumm.ToString(Constants.AMT_FORMAT) + Constants.CummuText5s);
                        else
                            lblKey1.Text = string.Format(cumm.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + strUserFirstName + Constants.keyFigures7 + Constants.CummuText1s + strUserFirstName + Constants.CummuText2sWork + Valueat70.ToString(Constants.AMT_FORMAT) + Constants.CummuText3s + Constants.Number70 + Constants.CommaandSpace + strUserFirstName + Constants.CummuText4s + cumm.ToString(Constants.AMT_FORMAT) + Constants.CummuText5s);
                        lblKey1.Visible = true;
                    }
                    else
                    {
                        lblKey1.Visible = false;
                    }
                    if (startingValue != 0 && startingValue != Valueat70)
                    {
                        if (!AgeAtLastChange.Equals(Constants.Number70))
                        {
                            if (startingValue != spousalBenefits && startingValue != 0 && spousalBenefits != 0)
                            {
                                if (int.Parse(strWifeFRA) > int.Parse(ageWife))
                                    lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.ReducedWorkBenefit + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                else if (int.Parse(strWifeFRA) == int.Parse(ageWife))
                                    lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + Constants.FRAText + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                else
                                    lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + sWifeFRA + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            }
                            else
                            {
                                if (int.Parse(strWifeFRA) > int.Parse(ageWife))
                                    lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.ReducedWorkBenefit + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                else if (int.Parse(strWifeFRA) == int.Parse(ageWife))
                                    lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + Constants.FRAText + ", " + strUserFirstName + Constants.steps1 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                else
                                    lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.steps1 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            }
                        }
                        else
                        {
                            //lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWife + Constants.DivorceSurvior + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserFirstName + Constants.DivorceSpouse + Constants.DivorceSpouse1 + strUserFirstName + Constants.DivorceSpouse2 + strUserFirstName + Constants.DivorceSpouse3);
                            lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.DivorceSurvior + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.strClaimRestrictSpousal1 + strUserFirstName + Constants.strClaimRestrictSpousal2 + strUserFirstName + Constants.strClaimRestrictSpousal3 + strUserFirstName + Constants.strClaimRestrictSpousal4, true, false, false);
                            IsContainRestrictIncome = true;
                            lblSteps1.Visible = true;

                        }
                    }
                    else
                    {
                        lblSteps1.Visible = false;
                    }
                    if (startingValue != spousalBenefits && spousalBenefits != 0 && !AgeAtLastChange.Equals(Constants.Number70))
                    {
                        if (spousalBenefits != 0)
                        {
                            lblSteps2.Text = string.Format(Constants.Line2 + Constants.Age + " " + spousalAge + Constants.when + strUserFirstName + Constants.steps8 + sWifeFRA + ", " + strUserFirstName + Constants.spousalswitch + spousalBenefits.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.spousaltextdivorce);
                        }
                    }
                    else
                    {
                        if (Valueat70 != 0 && Valueat70 != startingValue && AgeAtLastChange.Equals(Constants.Number70))
                        {
                            lblSteps2.Text = string.Format(Constants.Line2 + Constants.Age + " " + Constants.Number70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps18 + Valueat70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                        }
                        else
                        {
                            if (AgeAtLastChange.Equals(Constants.Number70))
                            {
                                lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + Constants.Number70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + ", " + strUserFirstName + Constants.steps23 + Valueat70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                                lblSteps1.Visible = true;
                            }
                        }
                    }
                    if (AnualIncome != 0)
                    {
                        lblKey9.Text = string.Format(AnualIncome.ToString(Constants.AMT_FORMAT) + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey);
                        if ((Valueat70 != 0 && Valueat70 != startingValue) || (spousalBenefits != 0 && Valueat70 != 0))
                            lblSteps9.Text = string.Format(Constants.Line3 + Constants.Age + " " + Constants.Number70 + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                        else
                            lblSteps9.Text = string.Format(Constants.Line2 + Constants.Age + " " + Calc.AnnualIncomeAge + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                        lblKey9.Visible = true;
                        lblSteps9.Visible = true;
                    }
                    else
                    {
                        lblKey9.Visible = false;
                        lblSteps9.Visible = false;
                    }
                }
                //Add Restrict Income Value to label
                if (IsContainRestrictIncome)
                {
                    Globals.Instance.BoolShowDivRestrictIncom = true;
                    lblRestrictIcomeKey.Text = Globals.Instance.IntShowRestrictIncomeForDivorce.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                }
                else
                    lblRestrictIcomeKey.Visible = false;
                //Set key and steps values
                SetKeyAndStepsValuesForPDF(Constants.GridSpouse);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// code to get the keys and steps for the second grid ie GridAsap on the website below the Grid in the strategy
        /// </summary>
        /// <param name="cumm"></param>
        /// <param name="startingValue"></param>
        /// <param name="ageWife"></param>
        /// <param name="Valueat70"></param>
        /// <param name="strUserFirstName"></param>
        public void renderDataIntoGridStartegy2(decimal cumm, decimal startingValue, string ageWife, decimal spousalBenefits, string spousalAge, string strUserFirstName, string AgeAtLastChange, decimal AnualIncome)
        {
            try
            {
                Customers customer = new Customers();
                string sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                string ageWifeStarting = ageWife;
                //key 1 for the starting value ie working benefits
                if (cumm != 0)
                {
                    if (spousalBenefits == 0)
                        lblNewKey.Text = string.Format(cumm.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + strUserFirstName + Constants.keyFigures7 + Constants.CummuText1s + strUserFirstName + Constants.CummuText2sWork + startingValue.ToString(Constants.AMT_FORMAT) + Constants.CummuText3s + spousalAge + Constants.CommaandSpace + strUserFirstName + Constants.CummuText4s + cumm.ToString(Constants.AMT_FORMAT) + Constants.CummuText5s);
                    else
                        lblNewKey.Text = string.Format(cumm.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + strUserFirstName + Constants.keyFigures7 + Constants.CummuText1s + strUserFirstName + Constants.CummuText2sWork + spousalBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3s + spousalAge + Constants.CommaandSpace + strUserFirstName + Constants.CummuText4s + cumm.ToString(Constants.AMT_FORMAT) + Constants.CummuText5s);
                }
                else
                    lblNewKey.Visible = false;
                //show key 2 for spousal benefits 
                if (spousalBenefits != 0)
                    lblKey3.Text = string.Format(spousalBenefits.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps25 + strUserFirstName + Constants.SpousalBenefitsKey);
                else
                    lblKey3.Text = string.Format(startingValue.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps25 + strUserFirstName + Constants.WHBenefitsKey);

                if (Globals.Instance.WifeNumberofMonthinNote != 0)
                {
                    lblNoteWife1.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserFirstName;
                    ageWifeStarting = Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + " years and " + Globals.Instance.WifeNumberofMonthsinSteps + " months ";
                }

                if (Globals.Instance.WifeNumberofMonthsBelowGrid != 0)
                {
                    lblNoteWife7.Text = "** includes " + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    sWifeFRA = Globals.Instance.WifeNumberofYears + " years and " + Globals.Instance.WifeNumberofMonthsAboveGrid + " months ";
                }
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblNoteWife4.Text = "* includes " + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName; ;
                }

                if (startingValue != spousalBenefits && spousalBenefits != 0)
                {
                    lblSteps3.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    lblSteps4.Text = string.Format(Constants.Line2 + Constants.Age + "  " + spousalAge + Constants.when + strUserFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserFirstName + Constants.spousalswitch + spousalBenefits.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.spousaltextdivorce);
                    lblSteps8.Text = string.Format(Constants.Line3 + Constants.Age + " " + Calc.AnnualIncomeAge + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                }
                else
                {
                    lblSteps3.Visible = false;
                    lblKey4.Visible = false;
                    lblSteps4.Visible = false;
                    lblSteps8.Text = string.Format(Constants.Line2 + Constants.Age + " " + Calc.AnnualIncomeAge + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                }
                if (startingValue != 0 && startingValue != spousalBenefits)
                {
                    lblSteps3.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    lblSteps3.Visible = true;
                    lblKey4.Visible = true;
                }
                lblKey8.Text = string.Format(AnualIncome.ToString(Constants.AMT_FORMAT) + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey);
                //step 1 for the starting value ie working benefits
                //Set key and steps values
                SetKeyAndStepsValuesForPDF(Constants.GridStandard);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// code to get the keys and steps for the third grid ie GridStandard on the website below the Grid in the strategy
        /// </summary>
        /// <param name="cumm"></param>
        /// <param name="startingValue"></param>
        /// <param name="ageWife"></param>
        /// <param name="Valueat70"></param>
        /// <param name="strUserFirstName"></param>
        public void renderDataIntoGridStartegy3(decimal cumm, decimal startingValue, string ageWife, decimal spousalBenefits, string spousalAge, string strUserFirstName, string AgeAtLastChange, decimal AnualIncome)
        {
            try
            {
                string sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                string sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                string strWifeFRA = sWifeFRA.Substring(0, 2);
                string strHusbFRA = sHusbFRA.Substring(0, 2);
                string ageWifeStarting = ageWife;
                if (spousalBenefits != 0)
                    lblKey6.Text = string.Format(spousalBenefits.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps25 + strUserFirstName + Constants.stepsNewSpousal);
                else if (AgeAtLastChange.Equals(Constants.Number70))
                    lblKey6.Text = string.Format(startingValue.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps25 + strUserFirstName + Constants.MaxedOutBenefits);
                else
                    lblKey6.Text = string.Format(startingValue.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps25 + strUserFirstName + Constants.stepsNewWorking);

                DateTime HusbDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());


                if (Globals.Instance.WifeNumberofMonthinNote != 0)
                {
                    lblNoteWife0.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserFirstName;
                    ageWifeStarting = Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + " years and " + Globals.Instance.WifeNumberofMonthsinSteps + " months ";
                }

                if (Globals.Instance.WifeNumberofMonthsBelowGrid != 0)
                {
                    lblNoteWife6.Text = "** includes " + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    sWifeFRA = Globals.Instance.WifeNumberofYears + " years and " + Globals.Instance.WifeNumberofMonthsAboveGrid + " months ";
                    spousalAge = sWifeFRA;
                }
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblNoteWife3.Text = "* includes " + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName; ;
                }
                if (HusbDOB > limit)
                {
                    if (intAgeHusb > intAgeWife)
                    {
                        if (startingValue != 0)
                        {
                            lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWife + ", " + strUserFirstName + Constants.steps23 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                            lblKey5.Visible = true;
                            lblSteps5.Visible = true;
                        }
                        else
                        {
                            lblKey5.Visible = false;
                            lblSteps5.Visible = false;
                        }
                        lblSteps6.Visible = false;
                        lblKey6.Visible = true;
                        lblKey7.Text = string.Format(AnualIncome.ToString(Constants.AMT_FORMAT) + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey);
                        lblSteps7.Text = string.Format(Constants.Line2 + Constants.Age + " " + Calc.AnnualIncomeAge + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                        Globals.Instance.strDivorceStarting = Constants.Working;
                    }
                    else
                    {
                        if (cumm != 0)
                        {
                            lblKey5.Text = string.Format(cumm.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + strUserFirstName + Constants.keyFigures7 + Constants.CummuText1s + strUserFirstName + Constants.CummuText2s + spousalBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3s + spousalAge + Constants.CommaandSpace + strUserFirstName + Constants.CummuText4s + cumm.ToString(Constants.AMT_FORMAT) + Constants.CummuText5s); //string.Format(AnualIncome.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.AnualIncomeKey);
                            lblKey5.Visible = true;
                        }
                        else
                        {
                            lblKey5.Visible = false;
                        }
                        if (ageWife.Equals(Constants.Number70))
                        {
                            lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWife + ", " + strUserFirstName + Constants.steps23 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        }
                        else
                        {
                            if (startingValue != 0 && startingValue != spousalBenefits)
                            {
                                //added on 02/08/2016

                                if (int.Parse(strWifeFRA) > int.Parse(ageWife))
                                    lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.ReducedWorkBenefit + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                else if (int.Parse(strWifeFRA) == int.Parse(ageWife))
                                    lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                else
                                    lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + sWifeFRA + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                lblSteps5.Visible = true;

                            }
                            else
                            {
                                if (int.Parse(strWifeFRA) > int.Parse(ageWife))
                                    lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.ReducedSpousalBenefit + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                else if (int.Parse(strWifeFRA) == int.Parse(ageWife))
                                    lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps1 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.spousaltextdivorce);
                                else
                                    lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + sWifeFRA + ", " + strUserFirstName + Constants.steps1 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.spousaltextdivorce);
                                lblSteps5.Visible = true;
                                Globals.Instance.strDivorceStarting = Constants.spousal;
                            }
                        }//show key 2 for spousal benefits 
                        if (startingValue != spousalBenefits && spousalBenefits != 0)
                        {
                            lblSteps6.Text = string.Format(Constants.Line2 + Constants.Age + "  " + spousalAge.Substring(0, 2) + Constants.when + strUserFirstName + Constants.steps8 + spousalAge + ", " + strUserFirstName + Constants.spousalswitch + spousalBenefits.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.spousaltextdivorce);
                            lblSteps7.Text = string.Format(Constants.Line3 + Constants.Age + " " + Calc.AnnualIncomeAge + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                            lblKey6.Visible = true;
                            lblSteps6.Visible = true;
                        }
                        else
                        {
                            lblSteps6.Visible = false;
                            lblSteps7.Text = string.Format(Constants.Line2 + Constants.Age + " " + Calc.AnnualIncomeAge + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                        }
                        lblKey7.Text = string.Format(AnualIncome.ToString(Constants.AMT_FORMAT) + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey);
                    }
                }
                else
                {
                    if (cumm != 0)
                    {
                        if (spousalBenefits == 0)
                            lblKey5.Text = string.Format(cumm.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + strUserFirstName + Constants.keyFigures7 + Constants.CummuText1s + strUserFirstName + Constants.CummuText2s + startingValue.ToString(Constants.AMT_FORMAT) + Constants.CummuText3s + spousalAge + Constants.CommaandSpace + strUserFirstName + Constants.CummuText4s + cumm.ToString(Constants.AMT_FORMAT) + Constants.CummuText5s); //string.Format(AnualIncome.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.AnualIncomeKey);
                        else
                            lblKey5.Text = string.Format(cumm.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + strUserFirstName + Constants.keyFigures7 + Constants.CummuText1s + strUserFirstName + Constants.CummuText2s + spousalBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3s + spousalAge + Constants.CommaandSpace + strUserFirstName + Constants.CummuText4s + cumm.ToString(Constants.AMT_FORMAT) + Constants.CummuText5s); //string.Format(AnualIncome.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.AnualIncomeKey);

                        lblKey5.Visible = true;
                    }
                    else
                    {
                        lblKey5.Visible = false;
                    }
                    if (startingValue != 0 && startingValue != spousalBenefits)
                    {
                        if (int.Parse(strWifeFRA) > int.Parse(ageWife))
                            lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.ReducedWorkBenefit + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        else if (int.Parse(strWifeFRA) == int.Parse(ageWife))
                            lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + Constants.FRAText + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        else
                            lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        lblSteps5.Visible = true;
                    }
                    else
                    {
                        if (int.Parse(strWifeFRA) > int.Parse(ageWife))
                            lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.ReducedSpousalBenefit + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        else if (int.Parse(strWifeFRA) == int.Parse(ageWife))
                            lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps1 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.spousaltextdivorce);
                        else
                            lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + sWifeFRA + ", " + strUserFirstName + Constants.steps1 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.spousaltextdivorce);
                        lblSteps5.Visible = true;
                        Globals.Instance.strDivorceStarting = Constants.spousal;
                    }//show key 2 for spousal benefits 
                    if (startingValue != spousalBenefits && spousalBenefits != 0)
                    {
                        lblSteps6.Text = string.Format(Constants.Line2 + Constants.Age + "  " + spousalAge.Substring(0, 2) + Constants.when + strUserFirstName + Constants.steps8 + sWifeFRA + ", " + strUserFirstName + Constants.spousalswitch + spousalBenefits.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.spousaltextdivorce);
                        lblSteps7.Text = string.Format(Constants.Line3 + Constants.Age + " " + Calc.AnnualIncomeAge + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                        lblKey6.Visible = true;
                        lblSteps6.Visible = true;
                    }
                    else
                    {
                        lblSteps6.Visible = false;
                        lblSteps7.Text = string.Format(Constants.Line2 + Constants.Age + " " + Calc.AnnualIncomeAge + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                    }
                    lblKey7.Text = string.Format(AnualIncome.ToString(Constants.AMT_FORMAT) + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey);
                }

                //Set key and steps values
                SetKeyAndStepsValuesForPDF(Constants.GridAsap);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// code to export the explanation text pop up to PDF
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void exportToPdf(object sender, EventArgs e)
        {
            try
            {
                DataTable dtCustomerDetails = customer.getCustomerFinancialDetailsByStatus(Session[Constants.SessionUserId].ToString(), Constants.Divorced);
                customerFirstName = dtCustomerDetails.Rows[0][Constants.CustomerFirstName].ToString();
                Response.ContentType = Constants.pdfContentType;
                Response.AddHeader(Constants.pdfContentDisposition, "attachment;filename=Explanation_For_Divorce_Strategies" + dtCustomerDetails.Rows[0][Constants.CustomerLastName].ToString() + "_" + DateTime.Now.ToString(Constants.DateFormat) + Constants.pdfFileExtension);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Document pdfDoc = new Document(PageSize.A4, Constants.zero, Constants.zero, -15f, 55f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                writer.PageEvent = new PDFEvent();
                pdfDoc.Open();

                #region Old strategy explanation text
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.StrategyExpanationTitle, false, true, true));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(new Paragraph(" "));
                //#region What is the Primary Goal of Social Security Calculator?
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion1, false, false, true));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationTitleDivorce1, false, false, false));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationTitleText, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //#endregion What is the Primary Goal of Social Security Calculator?
                //#region How the Social Security Calculator Works?
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion2, false, false, true));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWorkingDivorce1, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWorkingDivorce2, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //#endregion How the Social Security Calculator Works?
                //#region What are the Key Benefit Numbers in the Strategies?
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWorkingDivorce3, false, false, false));
                //pdfDoc.NewPage();
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWorkingDivorce4, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion3, false, false, true));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationKeys1, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationKeys2, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationKeys3, false, false, false));
                //pdfDoc.NewPage();
                //#endregion What are the Key Benefit Numbers in the Strategies?
                //#region What are the Next Steps in the Strategies?
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion4, false, false, true));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSteps1, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSteps2, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.GoodLuckText, false, true, true));
                //#endregion What are the Next Steps in the Strategies?
                #endregion Old strategy explanation text

                //Add strategy explanation content
                AddExplainationToPDF(ref pdfDoc);


                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }


        /// <summary>
        /// Used to add strategy explaination to PDF
        /// </summary>
        /// <param name="pdfDoc">PDF Document</param>
        private void AddExplainationToPDF(ref Document pdfDoc)
        {
            try
            {
                #region Add titles
                pdfDoc.Add(SiteNew.generateParagraph(Constants.StrategyExpanationTitle, false, true, true));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.DivorcedSpouses, false, true, true));
                #endregion Add titles

                #region What is the Primary Goal Of the Paid to Wait Calculator?
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion1, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion1Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What is the Primary Goal of the Paid to Wait Calculator?

                #region Many Married Couples Can Get Paid to Wait
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion12, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion2Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion Many Married Couples Can Get Paid to Wait

                #region Can All Qualified Divorced Spouses Get Paid to Wait?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion11, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion3Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion Can All Qualified Divorced Spouses Get Paid to Wait?

                #region How do I Know if I am Qualified?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion10, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion4Answer1, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion4Answer2, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion4Answer3, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion How do I Know if I am Qualified?

                #region How the Paid to Wait Calculator Works
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion4, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion5Answer1, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion5Answer2, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion5Answer3, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion5Answer4, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion5Answer5, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion How the Paid to Wait Calculator Works

                #region What if You Continue to Work?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion5, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion6Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What if You Continue to Work?

                #region What You Can Expect
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion7, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion7Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What You Can Expect

                #region What are the “Key Benefit Numbers” in the Strategies?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion8, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion8Answer1, false, false, false));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion8Answer2, false, false, false));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion8Answer3, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What are the “Key Benefit Numbers” in the Strategies?

                #region What are the “Next Steps” in the Strategies?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion9, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion9Answer1, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion9Answer2, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion9Answer3, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationDivorcedQuestion9Answer4, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What are the “Next Steps” in the Strategies?

                #region Add Note for explanation
                pdfDoc.Add(SiteNew.generateParagraph(Constants.StrategyExplanationNote, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                #endregion Add Note for explanation

                #region Add Good Luck Text
                pdfDoc.Add(SiteNew.generateParagraph(Constants.GoodLuckText, false, true, true));
                #endregion Add Good Luck Text
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }


        /// <summary>
        /// Used to add strategy instructions in pdf
        /// </summary>
        /// <param name="pdfDoc"></param>
        private void AddInstructionBeforeSeeStrategy(ref Document pdfDoc)
        {
            try
            {
                #region Add Instructions
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.StrategyInstruction, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion Add Instructions
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }
        /// <summary>
        /// restting the values to null for the gloabal variables and lable texts
        /// </summary>
        public void ResetValues()
        {
            try
            {
                #region reset the values
                Globals.Instance.BoolSpousalGreaterAtFRAandAge70 = false;
                Globals.Instance.BoolWorkingBenefitsShown = false;
                Globals.Instance.BoolHideAsapStrategyDivorce = false;
                Globals.Instance.strDivorceStarting = string.Empty;
                Globals.Instance.strDivorceStartingSpouse = string.Empty;
                Globals.Instance.BoolHideSpouseStrategyDivorce = false;
                Globals.Instance.WifeNumberofMonthinNote = 0;
                Globals.Instance.strDivorceStarting = string.Empty;
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                Globals.Instance.BoolWHBSwitchedHusb = false;
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                Globals.Instance.BoolShowThirdDivorceStrategy = false;
                Globals.Instance.BoolShowSecondStrat = false;
                Globals.Instance.IntShowRestrictIncomeForDivorce = 0;
                Globals.Instance.BoolShowDivRestrictIncom = false;

                lblKey1.Text = string.Empty;
                lblKey2.Text = string.Empty;
                lblKey3.Text = string.Empty;
                lblKey4.Text = string.Empty;
                lblKey5.Text = string.Empty;
                lblKey6.Text = string.Empty;
                lblKey7.Text = string.Empty;
                lblKey8.Text = string.Empty;
                lblKey9.Text = string.Empty;
                lblSteps1.Text = string.Empty;
                lblSteps2.Text = string.Empty;
                lblSteps3.Text = string.Empty;
                lblSteps4.Text = string.Empty;
                lblSteps5.Text = string.Empty;
                lblSteps6.Text = string.Empty;
                lblSteps7.Text = string.Empty;
                lblSteps8.Text = string.Empty;
                lblSteps9.Text = string.Empty;
                lblNoteWife0.Text = string.Empty;
                lblNoteWife1.Text = string.Empty;
                lblNoteWife2.Text = string.Empty;
                lblNoteWife3.Text = string.Empty;
                lblNoteWife4.Text = string.Empty;
                lblNoteWife5.Text = string.Empty;
                lblNoteWife6.Text = string.Empty;
                lblNoteWife7.Text = string.Empty;
                lblNoteWife8.Text = string.Empty;
                #endregion reset the values
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// calculate current age of Wife
        /// </summary>
        /// <param name="husbBD"></param>
        /// <returns></returns>
        protected void intAgeForWife(DateTime wifeBD)
        {
            try
            {
                TimeSpan Span = DateTime.Now - wifeBD;
                DateTime Age = DateTime.MinValue + Span;
                int Years = Age.Year - 1;
                int Months = Age.Month - 1;
                int Days = Age.Day - 1;
                intCurrentAgeWifeYear = Years;
                Globals.Instance.strCurrentAgeWife = Years.ToString() + " years and " + Months.ToString() + " months ";
                if (Days >= 28)
                    Months += 1;
                if (Months >= 6)
                    Years += 1;
                intCurrentAgeWifeMonth = Months;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }
        #endregion
    }
}