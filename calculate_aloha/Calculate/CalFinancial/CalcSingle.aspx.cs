﻿using CalculateDLL;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Symbolics;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using System.Reflection;
using Calculate.Objects;

namespace Calculate
{
    public partial class WebForm2 : System.Web.UI.Page
    {

        #region Variable Declaration
        // Input items
        int intAgeWife, intAgeWifeMonth, intAgeWifeDays, intCurrentAgeWifeMonth, intCurrentAgeWifeYear;
        decimal decBeneWife;
        decimal colaAmt = 1;
        int birthYearWife = 0;
        DataTable dtDetails;
        String custFirstName = string.Empty;
        Customers customer = new Customers();

        Calc calc = new Calc();
        #endregion Variable Declaration

        #region Events

        /// <summary>
        /// btnSaveBasicInfoAndProceed_Click
        /// code to redirect according to subscription
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveBasicInfoAndProceed_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                string sIsSubscription = dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription].ToString();
                if (sIsSubscription.Equals(Constants.FlagNo))
                {
                    Response.Redirect(Constants.RedirectWelcomeUser, false);
                }
                else
                {
                    Response.Redirect(Constants.RedirectSocialSecurityCalculator, false);
                    //Response.Redirect("~/CalFinancial/InitialSocialSecurityCalculator.aspx", false);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// code when page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //user session validation
                validateSession();
                if (!IsPostBack)
                {
                    try
                    {
                        if (Session["Demo"] != null)
                        {
                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = husbandBirthDate.Month.ToString();
                            BirthWifeDD.Text = husbandBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            ValidateAndCalc();

                            //Send mail to the demo user before loading the strategy page
                            Globals.Instance.BoolEmailReport = true;
                            exportAllToPdf();
                            Globals.Instance.BoolEmailReport = false;
                        }
                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                        {
                            ///* Disabled textbox and radio button so that institutional user can only view customer details */
                            ////BtnChange.Text = Constants.BtnChangeText;
                            //BirthWifeYYYY.Enabled = false;
                            //BeneWife.Enabled = false;
                            ////CheckBoxCola.Enabled = false;
                            //// Initialize page variables
                            //ErrorMessage.Text = String.Empty;
                            //PanelGrids.Visible = false;
                            //PanelParms.Visible = false;
                            //// CheckBoxCola.Checked = true;
                            //BirthWifeMM.Focus();
                            //ShowAllInformation();
                            //Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.ClientScriptDatePickerName, "disableDatePicker();", true);

                            //It will work Same Like as Advisor 
                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = husbandBirthDate.Month.ToString();
                            BirthWifeDD.Text = husbandBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            PanelEntry.Visible = false;
                            ValidateAndCalc();
                            rdbtnStrategy1.Checked = false;
                            rdbtnStrategy2.Checked = false;
                            rdbtnStrategy3.Checked = false;
                            if (Globals.Instance.BoolEmailReport || Globals.Instance.BoolAdvisorDownloadReport)
                            {
                                Globals.Instance.BoolAdvisorDownloadReport = false;

                                exportAllToPdf();
                                Response.Redirect(Constants.RedirectManageCustomers);
                            }

                        }
                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                        {
                            //It will work Same Like as Advisor 
                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = husbandBirthDate.Month.ToString();
                            BirthWifeDD.Text = husbandBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            PanelEntry.Visible = false;
                            ValidateAndCalc();
                            rdbtnStrategy1.Checked = false;
                            rdbtnStrategy2.Checked = false;
                            rdbtnStrategy3.Checked = false;
                            if (Globals.Instance.BoolEmailReport || Globals.Instance.BoolAdvisorDownloadReport)
                            {
                                Globals.Instance.BoolAdvisorDownloadReport = false;

                                exportAllToPdf();
                                Response.Redirect(Constants.RedirectManageCustomers);
                            }
                        }
                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor))
                        {
                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = husbandBirthDate.Month.ToString();
                            BirthWifeDD.Text = husbandBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            ValidateAndCalc();
                            rdbtnStrategy1.Checked = false;
                            rdbtnStrategy2.Checked = false;
                            rdbtnStrategy3.Checked = false;
                            if (Globals.Instance.BoolEmailReport || Globals.Instance.BoolAdvisorDownloadReport)
                            {
                                Globals.Instance.BoolAdvisorDownloadReport = false;

                                exportAllToPdf();
                                Response.Redirect(Constants.RedirectManageCustomers);
                            }
                        }
                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.Customer))
                        {
                            Customers customer = new Customers();
                            DateTime husbandBirthDate;
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                            husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = husbandBirthDate.Month.ToString();
                            BirthWifeDD.Text = husbandBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;

                            PanelGrids.Visible = false;//Added for new development (on 16/09)
                            PanelEntry.Visible = false;//Added for new development (on 16/09)
                            ValidateAndCalc();
                            if (Convert.ToString(dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription]).Equals(Constants.FlagNo))
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "displaySubscriptionPopUp(false);", true);
                                ValidateAndCalc();
                                //to show the Paid to wait value on the popup for new user
                                //decimal amt = decimal.Parse(Session[Constants.MaxCumm].ToString());
                                //MaxCumm.Text = Constants.SubscriptionPopupText + Environment.NewLine + amt.ToString(Constants.AMT_FORMAT);
                            }
                            //Commented for new development (on 16/09)
                            //else
                            //    ValidateAndCalc();
                        }
                        else if (!string.IsNullOrEmpty(Session["CustAfterTrans"] as string))
                        {
                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = husbandBirthDate.Month.ToString();
                            BirthWifeDD.Text = husbandBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            ValidateAndCalc();

                        }
                        else
                        {
                            // Initialize page variables
                            ErrorMessage.Text = String.Empty;
                            PanelGrids.Visible = false;
                            PanelParms.Visible = false;
                            // CheckBoxCola.Checked = true;
                            BirthWifeMM.Focus();
                            ShowAllInformation();
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                        ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                        ErrorMessageLabel.Visible = true;
                    }
                }
                else
                {
                    rdbtnStrategy1.Checked = false;
                    rdbtnStrategy2.Checked = false;
                    rdbtnStrategy3.Checked = false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// code to close the pop up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                //Previous Redirection
                //Response.Redirect(Constants.RedirectCalcSingle, false);
                Response.Redirect(Constants.RedirectInitialSocialSecurity, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.TryAgain;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// code when the next to proceed button is clicked to calculate the strategies
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Calculate_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                Button clicked = (Button)sender;
                ResetValues();

                if (clicked.Text == BtnCalculate.Text)
                {
                    if (Convert.ToString(dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription]).Equals(Constants.FlagNo))
                    {
                        if (Session[Constants.SessionRegistered] != null)
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displaySubscriptionPopUp(true);", true);
                        }
                        else
                        {
                            if (BeneWife.Text != string.Empty)
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displaySubscriptionPopUp(false);", true);
                                ValidateAndCalc();
                                //to show pait to wait number on the subscription poup
                                decimal amt = decimal.Parse(Session[Constants.SMaxCumm].ToString());
                                MaxCumm.Text = Constants.SubscriptionPopupText + amt.ToString(Constants.AMT_FORMAT);
                            }
                        }
                    }
                    else
                    {
                        ValidateAndCalc();
                    }
                }
                else //if (clicked.Text == BtnChange.Text)
                {
                    if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                    {
                        Response.Redirect(Constants.RedirectSocialSecurityCalculator, false);
                    }
                    else
                    {
                        BackToParms();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        ///  restting the values to null for the gloabal variables and lable texts
        /// </summary>
        public void ResetValues()
        {
            try
            {
                #region reset the value
                Globals.Instance.BoolHideSpouseStrategySingle = false;
                //Make keys empty
                lblKey1.Text = string.Empty;
                lblKey2.Text = string.Empty;
                lblKey3.Text = string.Empty;
                lblKey4.Text = string.Empty;
                lblKey5.Text = string.Empty;
                lblKey6.Text = string.Empty;
                lblKey7.Text = string.Empty;
                lblKey8.Text = string.Empty;
                lblKey9.Text = string.Empty;

                //Make steps empty
                lblSteps1.Text = string.Empty;
                lblSteps2.Text = string.Empty;
                lblSteps3.Text = string.Empty;
                lblSteps4.Text = string.Empty;
                lblSteps5.Text = string.Empty;
                lblSteps6.Text = string.Empty;
                lblSteps7.Text = string.Empty;
                lblSteps8.Text = string.Empty;
                lblSteps9.Text = string.Empty;
                #endregion reset the value
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// code when print button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PrintAll_Click(object sender, EventArgs e)
        {
            try
            {
                //export grids and chart to PDF
                exportAllToPdf();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.DownloadError;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// subscribe user who are in trial period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnTrial_Click(object sender, EventArgs e)
        {
            try
            {
                // Update column with package plan
                customer.updateCustomerSubscriptionPlan(Session[Constants.SessionUserId].ToString(), Constants.WithoutTrialPeriod);
                //Changes for live Server
                //Response.Redirect(Constants.TrialLiveServerURL + Session[Constants.SessionUserId].ToString(), true);
                //Response.Redirect(Constants.NewSingleUseURL + Session[Constants.SessionUserId].ToString(), true);
                //Changes for InfusionSoft live Server
                Response.Redirect(Constants.NewSingleUseURLInfusion, true);
                // Redirect to chargify url 
                //Response.Redirect(Constants.TrialTestServerURL + Session[Constants.SessionUserId].ToString(), true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.TryAgain;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Subscribe user who are not in trial period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnWithoutTrial_Click(object sender, EventArgs e)
        {
            try
            {
                // Update column with package plan
                customer.updateCustomerSubscriptionPlan(Session[Constants.SessionUserId].ToString(), Constants.TrialPeriod);
                //Changes for live Server
                Response.Redirect(Constants.WithoutTrialLiveServerURL + Session[Constants.SessionUserId].ToString(), true);
                // Redirect to chargify url 
                //Response.Redirect(Constants.WithoutTrialTestServerURL + Session[Constants.SessionUserId].ToString(), true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.TryAgain;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// code to show the explanation popup for all strategies.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExplain_Click(object sender, EventArgs e)
        {
            try
            {
                popup.Show();
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "displayPanelExplanation();", true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.TryAgain;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// to close the explanation popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgCancel_Click(object sender, EventArgs e)
        {
            try
            {
                popup.Hide();
                ValidateAndCalc();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.TryAgain;
                ErrorMessageLabel.Visible = true;
            }
        }
        /// <summary>
        /// code to export the explanation text pop up to PDF
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void exportToPdf(object sender, EventArgs e)
        {
            try
            {
                Response.ContentType = Constants.pdfContentType;
                Response.AddHeader(Constants.pdfContentDisposition, "attachment;filename=Explanation_For_Single_Strategies" + "_" + DateTime.Now.ToString(Constants.DateFormat) + Constants.pdfFileExtension);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Document pdfDoc = new Document(PageSize.A4, Constants.zero, Constants.zero, -15f, 55f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                writer.PageEvent = new PDFEvent();
                pdfDoc.Open();

                //Add strategy explanation content
                AddExplainationToPDF(ref pdfDoc);

                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.TryAgain;
                ErrorMessageLabel.Visible = true;
            }
        }

        #endregion Events

        #region Validation

        /// <summary>
        /// code to validate the session
        /// </summary>
        public void validateSession()
        {
            try
            {
                Session[Constants.SessionRegistered] = null;
                if (Session[Constants.SessionUserId] == null)
                {
                    Response.Redirect(Constants.RedirectLogin, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// code to check the inputs on the form
        /// </summary>
        /// <returns></returns>
        protected bool ValidateInput()
        {
            try
            {
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor) || Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                    dtDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                else
                    dtDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                custFirstName = dtDetails.Rows[0][Constants.CustomerFirstName].ToString();
                bool isValid = true;
                ErrorMessage.Text = string.Empty;
                int birthYYYY, birthMM, birthDD;
                DateTime today = DateTime.Now;
                DateTime wifeBD = today;
                birthMM = SiteNew.ValidateNumeric(BirthWifeMM, 1, 12, Constants.Text0, ref isValid, ErrorMessage);
                birthDD = SiteNew.ValidateNumeric(BirthWifeDD, 1, 31, Constants.Text1, ref isValid, ErrorMessage);
                birthYYYY = SiteNew.ValidateNumeric(birthWifeTempYYYY, 1900, 2020, Constants.Text2, ref isValid, ErrorMessage);
                if (isValid)
                {
                    birthYearWife = int.Parse(birthWifeTempYYYY.Text);
                    wifeBD = new DateTime(birthYYYY, birthMM, birthDD);
                }
                TimeSpan Span = DateTime.Now - wifeBD;
                DateTime Age = DateTime.MinValue + Span;
                int Years = Age.Year - 1;
                int Months = Age.Month - 1;
                int Days = Age.Day - 1;
                if (Days >= 28)
                    Months += 1;
                if (Months >= 10)
                    Years += 1;
                intAgeWife = Years;
                intAgeForWife(wifeBD);
                //intAgeWife = today.Year - wifeBD.Year;
                //if (today.Month < wifeBD.Month
                //|| (today.Month == wifeBD.Month)
                //&& (today.Day < wifeBD.Day))
                //{
                //intAgeWife -= 1;
                //to get number of months and if no of days are more than 28 full complete month is considered.
                if (wifeBD.Day >= 28 && wifeBD.Day != DateTime.Now.Day)
                {
                    intAgeWifeMonth = wifeBD.Month + 1;
                    intAgeWifeDays = wifeBD.Day;
                }
                else
                {
                    intAgeWifeMonth = wifeBD.Month;
                    intAgeWifeDays = wifeBD.Day;
                }
                //}
                decimal.TryParse(this.BeneWife.Text, out decBeneWife);
                if (isValid)
                {
                    PanelGrids.Visible = true;
                    PanelEntry.Visible = false;
                    PanelParms.Visible = true;
                    lblCustomerName.Text = custFirstName;
                    lblCustomerDOB.Text = wifeBD.ToShortDateString();
                    lblCustomerFRA.Text = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                    lblCustomerBenefit.Text = decBeneWife.ToString(Constants.AMT_FORMAT);
                }
                else
                    ErrorMessage.Text += ".";
                if (!String.IsNullOrEmpty(ErrorMessage.Text))
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayErrorMessage();", true);
                else
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "hideErrorMessage();", true);
                return isValid;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
                return false;
            }
        }

        /// <summary>
        ///  Run standard tests if requested, otherwise build page
        /// </summary>
        protected void ValidateAndCalc()
        {
            try
            {
                string WifeBirth = BirthWifeYYYY.Text.ToString();
                char[] delimiters = new char[] { '/' };
                string[] WifeBirthArray = WifeBirth.Split(delimiters, 3);
                birthWifeTempYYYY.Text = WifeBirthArray[2];
                BirthWifeMM.Text = WifeBirthArray[0];
                BirthWifeDD.Text = WifeBirthArray[1];
                if (ValidateInput())
                {
                    BuildPage();
                    // Save All Details to respective tables According to martial status
                    UpdateCustomerFinancialDetails();
                    SiteNew.renderGridViewIntoStackedChart(GridAsap, ChartAsap, LabelAsap, Constants.Single, false);
                    SiteNew.renderGridViewIntoStackedChart(GridStandard, ChartStandard, LabelStandard, Constants.Single, false);
                    SiteNew.renderGridViewIntoStackedChart(GridLatest, ChartLatest, LabelLatest, Constants.Single, false);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        #endregion Validation

        #region CRUD Operation

        /// <summary>
        /// This method basic information of customer in form
        /// </summary>
        private void ShowAllInformation()
        {
            try
            {
                DataTable dtCustomerFinancialDetails = customer.getCustomerFinancialDetailsByStatus(Session[Constants.SessionUserId].ToString(), Constants.Single);
                if (dtCustomerFinancialDetails.Rows.Count != 0)
                {
                    if (dtCustomerFinancialDetails.Rows[0][Constants.CustomerBirthDate].ToString() != null && dtCustomerFinancialDetails.Rows[0][Constants.CustomerBirthDate].ToString() != string.Empty)
                    {
                        DateTime strBirthDate = (DateTime)dtCustomerFinancialDetails.Rows[0][Constants.CustomerBirthDate];
                        BirthWifeDD.Text = strBirthDate.Day.ToString();
                        BirthWifeMM.Text = strBirthDate.Month.ToString();
                        BirthWifeYYYY.Text = strBirthDate.ToShortDateString();
                    }
                    BeneWife.Text = dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoRetAgeBenefit].ToString();
                    //string strCola = dtCustomerFinancialDetails.Rows[0]["Cola"].ToString();
                    // if (strCola.Equals("T"))
                    //    CheckBoxCola.Checked = true;
                    //else
                    //    CheckBoxCola.Checked = false;
                }
                else
                {
                    DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                    /* Check if Birthdate exists or not */
                    if (!String.IsNullOrEmpty(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString()))
                    {
                        DateTime strBirthDate = (DateTime)dtCustomerDetails.Rows[0][Constants.CustomerBirthDate];
                        BirthWifeDD.Text = strBirthDate.Day.ToString();
                        BirthWifeMM.Text = strBirthDate.Month.ToString();
                        BirthWifeYYYY.Text = strBirthDate.ToShortDateString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Update Customer financial Details
        /// </summary>
        public void UpdateCustomerFinancialDetails()
        {
            try
            {
                SecurityInfoSingle securitySingleInfo = new SecurityInfoSingle();
                securitySingleInfo.BirthDate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(BirthWifeMM.Text + Constants.BackSlash + BirthWifeDD.Text + Constants.BackSlash + birthWifeTempYYYY.Text));
                securitySingleInfo.RetAgeBenefit = BeneWife.Text;
                //if (CheckBoxCola.Checked)
                securitySingleInfo.Cola = Constants.ColaStatusT;
                //else
                //    securitySingleInfo.Cola = "F";
                securitySingleInfo.CustomerID = Session[Constants.SessionUserId].ToString();
                customer.UpdateCustomerFinancialDetailsForSingle(securitySingleInfo);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }
        #endregion CRUD Operation

        #region Other Operations

        /// <summary>
        /// code to set the sort experession in ascending order
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int Compare2(KeyValuePair<string, int> a, KeyValuePair<string, int> b)
        {
            try
            {
                return a.Value.CompareTo(b.Value);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// code to buld all alternative strategies based on the inputs
        /// </summary>
        protected void BuildPage()
        {
            try
            {
                ResetValues();
                #region code to set variable values
                // Set calculation properties
                Calc Calc = new Calc()
                {
                    intAgeWife = intAgeWife,
                    decBeneWife = decBeneWife,
                    birthYearWife = birthYearWife,
                    intAgeWifeMonth = intAgeWifeMonth,
                    intAgeWifeDays = intAgeWifeDays,
                    intCurrentAgeWifeMonth = intCurrentAgeWifeMonth,
                    intCurrentAgeWifeYear = intCurrentAgeWifeYear
                };
                //if (CheckBoxCola.Checked)
                //    Calc.colaAmt = 1.025m;
                //else
                //    Calc.colaAmt = 1;
                Calc.colaAmt = 1.025m;
                colaAmt = Calc.colaAmt;
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor) || Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                    dtDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                else
                    dtDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                string strUserFirstName = dtDetails.Rows[0][Constants.CustomerFirstName].ToString();
                Calc.Your_Name = strUserFirstName;
                CommonVariables.strUserName = strUserFirstName;
                string wifeFra = customer.FRA(birthYearWife);
                int intWifeFra = int.Parse(wifeFra.Substring(0, 2));
                Globals.Instance.intWifeAge = intAgeWife;
                #endregion code to set variable values

                #region code to build all the visible Grids and chart
                // Build all of the alternative grids
                int cummAsap = Calc.BuildSingle(GridAsap, Calc.calc_strategy.Spousal, Calc.calc_solo.Single, 0, ref NoteAsap, Constants.SAsap);
                int[] asapBeAmts = Calc.breakEvenAmt;
                int asapAge70Combined = Calc.age70CombBene;
                //to highlight numbers in the  grid
                Calc.Highlight(GridAsap, Calculate.Calc.calc_solo.Single, Constants.GridAsap);
                //cummulative value at age 69
                Session[Constants.SingleAasapCumm] = Calc.SCummValue;
                //label to show the anual value
                lblCombinedIncome3.Text = Calc.SingleAnualValue.ToString(Constants.AMT_FORMAT);
                //render steps and keys for the asap
                renderDataIntoGridStartegy3(Calc.SCummValue, Calc.SValueStarting, Calc.AgeValueAt70, Calc.SAgeValue, strUserFirstName, Calc.SingleAnualValue);
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                Globals.Instance.WifeNumberofMonthsinSteps = 0;

                Calc.BuildSingle(GridStandard, Calc.calc_strategy.Standard, Calc.calc_solo.Single, Calc.calc_start.fra6667, ref NoteStandard, Constants.SStandard);
                int standardAge70Combined = Calc.age70CombBene;
                //to highlight numbers in the  grid
                Calc.Highlight(GridStandard, Calculate.Calc.calc_solo.Single, Constants.GridStandard);
                //cummulative value at age 69
                Session[Constants.SingleStandardCumm] = Calc.SCummValue;
                //label to show the anual value
                if ((intAgeWife > intWifeFra))
                    lblCombinedIncome2.Text = string.Empty;
                else
                    lblCombinedIncome2.Text = Calc.SingleAnualValue.ToString(Constants.AMT_FORMAT);

                renderDataIntoGridStartegy2(Calc.SCummValue, Calc.SValueStarting, Calc.AgeValueAt70, Calc.SAgeValue, strUserFirstName, Calc.SingleAnualValue, Calc.AnnualIncomeAge);
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                Globals.Instance.WifeNumberofMonthsinSteps = 0;

                Calc.BuildSingle(GridLatest, Calc.calc_strategy.Suspend, Calc.calc_solo.Single, Calc.calc_start.max70, ref NoteLatest, Constants.SLatest);
                int latestAge70Combined = Calc.age70CombBene;
                //to highlight numbers in the  grid
                Calc.Highlight(GridLatest, Calculate.Calc.calc_solo.Single, Constants.GridLatest);
                //cummulative value at age 69
                Session[Constants.SingleLatestCumm] = Calc.SCummValue;
                //label to show the anual value
                lblCombinedIncome1.Text = Calc.SingleAnualValue.ToString(Constants.AMT_FORMAT);
                renderDataIntoGridStartegy1(Calc.SCummValue, Calc.SValueStarting, Calc.AgeValueAt70, Calc.SAgeValue, strUserFirstName, Calc.SingleAnualValue);
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                Globals.Instance.WifeNumberofMonthsinSteps = 0;

                var list = new List<KeyValuePair<string, int>>();
                list.Add(new KeyValuePair<string, int>(Constants.BestStrategy, int.Parse(Session[Constants.SingleLatestCumm].ToString())));
                //if(!intAgeForWife>)
                if (!(intAgeWife > intWifeFra))
                    list.Add(new KeyValuePair<string, int>(Constants.SuspendStrategy, int.Parse(Session[Constants.SingleStandardCumm].ToString())));
                if (!Globals.Instance.BoolHideSpouseStrategySingle)
                    list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.SingleAasapCumm].ToString())));
                //Used to set the total strategies count
                Calc.intStrategyCount = list.Count;
                //Used to add strategy details in pdf    
                AddPaidAndAnnualValForPDF();
                Session[Constants.SMaxCumm] = Session[Constants.SingleLatestCumm];
                if (list.Count == 3)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showThreeStrategy('{0}','{1}','{2}');", list[0].Key.ToString(), list[1].Key.ToString(), list[2].Key.ToString()), true);
                else if (list.Count == 2)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showTwoStrategy('{0}','{1}');", list[0].Key.ToString(), list[1].Key.ToString()), true);
                #endregion code to build all the visible Grids and chart

                #region code to show thw popups where HERE link is clicked
                PanelGrids.Visible = true;
                #endregion code to show thw popups where HERE link is clicked
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Used to set key and steps values to display on PDF 
        /// </summary>
        /// <param name="strGridName">Grid Name</param>
        public void SetKeyAndStepsValuesForPDF(string strGridName)
        {
            try
            {
                if (strGridName.Equals(Constants.GridAsap))
                {
                    CommonVariables.strKey1 = lblKey5.Text;
                    CommonVariables.strKey2 = lblKey6.Text;
                    CommonVariables.strStep1 = lblSteps5.Text;
                    CommonVariables.strStep2 = lblSteps6.Text;
                    CommonVariables.strNoteWife1 = lblNoteWife1.Text;
                }
                else if (strGridName.Equals(Constants.GridStandard))
                {
                    CommonVariables.strKey3 = lblKey3.Text;
                    CommonVariables.strKey4 = lblKey4.Text;
                    CommonVariables.strStep3 = lblSteps3.Text;
                    CommonVariables.strStep4 = lblSteps4.Text;

                    CommonVariables.strNoteWife0 = lblNoteWife0.Text;
                }
                else if (strGridName.Equals(Constants.GridLatest))
                {
                    CommonVariables.strKey5 = lblKey1.Text;
                    CommonVariables.strKey6 = lblKey2.Text;
                    CommonVariables.strStep5 = lblSteps1.Text;
                    CommonVariables.strStep6 = lblSteps2.Text;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.DownloadError;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// redirect advisor back to the managecustomer page
        /// </summary>
        /// <param name="strGridName"></param>
        protected void Backto_Managecustomer(object sender, EventArgs e)
        {
            try
            {
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor))
                    Response.Redirect(Constants.RedirectManageCustomers, true);
                else if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                    Response.Redirect(Constants.RedirectManageCustomersWithAdvisorID + Session[Constants.TempAdvisorID].ToString(), false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.DownloadError;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Export Grid View Content into PDF
        /// </summary>
        /// <param name="gridView"></param>
        public void exportAllToPdf()
        {
            try
            {
                #region code to set the PDf variables
                /* Get Customer Details for showing up in PDF  */
                DataTable dtCustomerDetails;
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor) || Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                    dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                else
                    dtCustomerDetails = customer.getCustomerFinancialDetailsByStatus(Session["UserId"].ToString(), Constants.Single);
                string custFirstName = dtCustomerDetails.Rows[0][Constants.CustomerFirstName].ToString();
                decimal Benefit;
                string Bene = dtCustomerDetails.Rows[0][Constants.SecurityInfoMarriedHusbandsRetAgeBenefit].ToString();
                decimal.TryParse(Bene, out Benefit);
                MemoryStream objMemory = new MemoryStream();
                Document pdfDoc = new Document(PageSize.A4, Constants.zero, Constants.zero, -15f, 55f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, objMemory);
                string sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                string strOwnBenefit = Benefit.ToString(Constants.AMT_FORMAT);
                string strEmail = string.Empty;
                #endregion code to set the PDf variables

                #region code to set the first page of the PDF
                /* Open the stream */
                pdfDoc.Open();
                writer.PageEvent = new PDFEvent();
                // First PDF Page
                SiteNew.PdfFrontPage(dtCustomerDetails, ref pdfDoc);
                pdfDoc.NewPage();
                // Second PDF Page
                //get a table for 3 colums
                PdfPTable table = new PdfPTable(3);
                //set the column width
                float[] widths = new float[] { 1f, 1f, 1f };
                //set the table width
                table.TotalWidth = 550f;
                //fix the table width
                table.LockedWidth = true;
                //adding space in the table
                table.DefaultCell.Padding = 3f;
                table.SetWidths(widths);
                //remove the table borders
                table.DefaultCell.Border = PdfPCell.NO_BORDER;
                //set the font for the text in the table cells
                BaseFont customfont = BaseFont.CreateFont(HttpContext.Current.Server.MapPath("~") + Constants.PdfRobotoFontStyle, BaseFont.CP1252, BaseFont.EMBEDDED);
                iTextSharp.text.Font font = new iTextSharp.text.Font(customfont, 16);
                //add 1 row to the table as customer name
                table.AddCell(new Phrase(Constants.customerName, font));
                table.AddCell(new Phrase(dtCustomerDetails.Rows[0][Constants.CustomerFirstName].ToString(), font));
                table.AddCell(string.Empty);
                //add 2 row to the table as date of births
                table.AddCell(new Phrase(Constants.customerDob, font));
                table.AddCell(new Phrase(string.Format(Constants.stringDateFormat, Convert.ToDateTime(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString())), font));
                table.AddCell(new Phrase(string.Empty));
                //add 3 row to the table as full retirement ages
                table.AddCell(new Phrase(Constants.customerFullRetirementAge, font));
                table.AddCell(new Phrase(sWifeFRA, font));
                table.AddCell(string.Empty);
                //add 4 row to the table as PIA's
                table.AddCell(new Phrase(Constants.customerPIA, font));
                table.AddCell(new Phrase(strOwnBenefit, font));
                table.AddCell(string.Empty);
                //add 5 row to the table as maritial status
                table.AddCell(new Phrase(Constants.PdfCustomerMaritalStatus, font));
                table.AddCell(new Phrase(dtCustomerDetails.Rows[0][Constants.CustomerMaritalStatus].ToString(), font));
                table.AddCell(string.Empty);
                //add 6 row to the table as COLA percentage
                table.AddCell(new Phrase(Constants.customerIncludeCOLA, font));
                table.AddCell(new Phrase(Constants.COLAPercentage, font));
                table.AddCell(string.Empty);
                //add table to the document
                pdfDoc.Add(table);
                pdfDoc.Add(SiteNew.generateImage(Constants.pdfHorizontalLine, Constants.pdfImageWidth, 1f));
                pdfDoc.Add(new Paragraph(" "));
                #endregion code to set the first page of the PDF

                #region Code to build the gridTables for PDF and print them
                /* print gridview and some guideline into pdf page */
                var list = new List<KeyValuePair<string, int>>();
                string wifeFra = customer.FRA(int.Parse(birthWifeTempYYYY.Text));
                int intWifeFra = int.Parse(wifeFra.Substring(0, 2));
                string sThirdGrid = string.Empty;
                string sSecondGrid = string.Empty;
                list.Add(new KeyValuePair<string, int>(Constants.GridLatest, int.Parse(Session[Constants.SingleLatestCumm].ToString())));
                string sFirstGrid = Constants.GridLatest;
                if (!(Globals.Instance.intWifeAge > intWifeFra))
                {
                    list.Add(new KeyValuePair<string, int>(Constants.GridStandard, int.Parse(Session[Constants.SingleStandardCumm].ToString())));
                    sSecondGrid = list[1].Key.ToString();
                }
                if (!Globals.Instance.BoolHideSpouseStrategySingle)
                {
                    list.Add(new KeyValuePair<string, int>(Constants.GridAsap, int.Parse(Session[Constants.SingleAasapCumm].ToString())));
                    if ((Globals.Instance.intWifeAge > intWifeFra))
                        sSecondGrid = list[1].Key.ToString();
                    else
                        sThirdGrid = list[2].Key.ToString();
                }
                GridView gFirstGrid = new GridView();
                GridView gSecondGrid = new GridView();
                GridView gThirdGrid = new GridView();
                Chart cFirstChart = new Chart();
                Chart cSecondChart = new Chart();
                Chart cThirdChart = new Chart();
                Label lFirstLable = new Label();
                Label lSecondLabel = new Label();
                Label lThirdLabel = new Label();
                gFirstGrid = GridLatest;
                cFirstChart = ChartLatest;
                lFirstLable.Text = Constants.Strategy1;
                if ((Globals.Instance.intWifeAge > intWifeFra))
                {
                    if (!Globals.Instance.BoolHideSpouseStrategySingle)
                    {
                        gSecondGrid = GridAsap;
                        cSecondChart = ChartAsap;
                        lSecondLabel.Text = Constants.Strategy2;
                    }
                }
                else
                {
                    gSecondGrid = GridStandard;
                    cSecondChart = ChartStandard;
                    lSecondLabel.Text = Constants.Strategy2;
                    if (!Globals.Instance.BoolHideSpouseStrategySingle)
                    {
                        gThirdGrid = GridAsap;
                        cThirdChart = ChartAsap;
                        lThirdLabel.Text = Constants.Strategy3;
                    }
                }
                //Append strategy details to label to display in pdf    
                SiteNew.CreateStrategyDetailsTableForPDF(ref pdfDoc,
                                                            Constants.PaidtoWaitTextForPDF,
                                                            Calc.listPaidToWaitAmount,
                                                            Constants.AnnualIncomeStratComplete,
                                                            Calc.listAnnualAmount,
                                                            list.Count,
                                                            Constants.Single);

                //Add explanation of strategy in report while sending it to client by email
                AddInstructionBeforeSeeStrategy(ref pdfDoc);
                AddExplainationToPDF(ref pdfDoc);
                pdfDoc.NewPage();

                if (list.Count == 3)
                {
                    commonFunctions(gFirstGrid, cFirstChart, lFirstLable, sFirstGrid, custFirstName, ref pdfDoc, Constants.one);
                    commonFunctions(gSecondGrid, cSecondChart, lSecondLabel, sSecondGrid, custFirstName, ref pdfDoc, Constants.two);
                    commonFunctions(gThirdGrid, cThirdChart, lThirdLabel, sThirdGrid, custFirstName, ref pdfDoc, Constants.three);
                }
                else if (list.Count == 2)
                {
                    commonFunctions(gFirstGrid, cFirstChart, lFirstLable, sFirstGrid, custFirstName, ref pdfDoc, Constants.one);
                    commonFunctions(gSecondGrid, cSecondChart, lSecondLabel, sSecondGrid, custFirstName, ref pdfDoc, Constants.two);
                }

                #endregion Code to build the gridTables for PDF and print them

                //Add discalimer at the end of report
                SiteNew.AddDisclaimerIntoPDFReport(ref pdfDoc);

                #region Email Report to Client
                if (Globals.Instance.BoolEmailReport)
                {
                    strEmail = dtCustomerDetails.Rows[0][Constants.UserEmail].ToString();
                    if (!String.IsNullOrEmpty(Globals.Instance.strAdvisorEmail))
                    {
                        if (customer.MailReport(pdfDoc, Globals.Instance.strAdvisorEmail, objMemory, writer, custFirstName))
                            Globals.Instance.BoolIsMailSent = true;
                        Globals.Instance.strAdvisorEmail = string.Empty;
                    }
                    else
                    {
                        if (customer.MailReport(pdfDoc, strEmail, objMemory, writer, custFirstName))
                            Globals.Instance.BoolIsMailSent = true;
                    }
                }
                #endregion Email Report to Client

                #region code to set other PDF variables and close PDF
                else
                {
                    pdfDoc.Close();
                    /* Create object for Customer and Pdf File */
                    PdfFilesTO objPdfFile = new PdfFilesTO();
                    /* Assign data into pdf file variable */
                    objPdfFile.PdfContent = objMemory.GetBuffer();
                    objPdfFile.PdfName = Constants.pdfFileName + dtCustomerDetails.Rows[0][Constants.CustomerLastName].ToString() + "_" + DateTime.Now.ToString(Constants.DateFormat) + ".pdf";
                    objPdfFile.CustomerID = Session[Constants.SessionUserId].ToString();
                    objPdfFile.ContentType = Constants.pdfContentType;
                    /* Insert pdf content into database */
                    customer.insertPdfDetails(objPdfFile);
                    /* Print memory stream data into pdf */
                    Response.OutputStream.Write(objMemory.GetBuffer(), 0, objMemory.GetBuffer().Length);
                    /* Response Setting */
                    Response.ContentType = Constants.pdfContentType;
                    Response.AddHeader(Constants.pdfContentDisposition, Constants.pdfFileName + dtCustomerDetails.Rows[0][Constants.CustomerLastName].ToString() + "_" + DateTime.Now.ToString(Constants.DateFormat) + Constants.pdfFileExtension);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                    Response.End();
                }
                #endregion code to set other PDF variables and close PDF

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.DownloadError;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Used to strore Paid to Wait Amount and Annual Income values
        /// </summary>
        private void AddPaidAndAnnualValForPDF()
        {
            try
            {
                //Add Annual income values to show in PDF
                Calc.listAnnualAmount.Clear();
                if (!string.IsNullOrEmpty(lblCombinedIncome1.Text))
                    Calc.listAnnualAmount.Add(lblCombinedIncome1.Text);
                if (!string.IsNullOrEmpty(lblCombinedIncome2.Text))
                    Calc.listAnnualAmount.Add(lblCombinedIncome2.Text);
                if (!string.IsNullOrEmpty(lblCombinedIncome3.Text))
                    Calc.listAnnualAmount.Add(lblCombinedIncome3.Text);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.DownloadError;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// code to build common sturctture of the PDF
        /// </summary>
        /// <param name="gFirstGrid"></param>
        /// <param name="cFirstChart"></param>
        /// <param name="lFirstLable"></param>
        /// <param name="sGridName"></param>
        /// <param name="custFirstName"></param>
        /// <param name="pdfDoc"></param>
        /// <param name="number"></param>
        public void commonFunctions(GridView gFirstGrid, Chart cFirstChart, Label lFirstLable, string sGridName, string custFirstName, ref Document pdfDoc, string number)
        {
            try
            {
                Customers customer = new Customers();
                string WifeFra = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                if (!number.Equals(Constants.one))
                {
                    pdfDoc.NewPage();
                }
                SiteNew.renderGridViewIntoStackedChart(gFirstGrid, cFirstChart, lFirstLable, Constants.Single, false);
                SiteNew.commonPageLayout(lFirstLable, gFirstGrid, ref pdfDoc, custFirstName, Constants.Single, sGridName, WifeFra, string.Empty);
                /* Add new page in pdf */
                pdfDoc.NewPage();
                /* Print chart into image in pdf */
                // pdfDoc.Add(SiteNew.generateChartHeading(lFirstLable.Text));
                SiteNew.convertChartToImage(cFirstChart, ref pdfDoc);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        ///  Requested assumptions change
        /// </summary>
        protected void BackToParms()
        {
            try
            {
                PanelGrids.Visible = false;
                PanelEntry.Visible = true;
                PanelParms.Visible = false;
                BirthWifeMM.Focus();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        protected void SetStdTest(int iWifeAge, int iHusbAge, int iWifeBene, int iHusbBene)
        {
            try
            {
                DateTime today = DateTime.Now;
                BirthWifeMM.Text = today.Month.ToString();
                BirthWifeDD.Text = today.Day.ToString();
                BirthWifeYYYY.Text = (today.Year - iWifeAge).ToString();
                BeneWife.Text = iWifeBene.ToString();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        ///show the keys and steps below the grid for grid GridLatest
        /// </summary>
        /// <param name="cumm"></param>
        /// <param name="startingValue"></param>
        /// <param name="valueAT70"></param>
        /// <param name="ageWife"></param>
        /// <param name="strUserFirstName"></param>
        public void renderDataIntoGridStartegy1(decimal cumm, decimal startingValue, decimal valueAT70, string ageWife, string strUserFirstName, decimal AnualIncome)
        {
            try
            {
                Customers customer = new Customers();
                string sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                sWifeFRA = sWifeFRA.Substring(0, 2);
                if (startingValue != 0)
                {
                    if (ageWife.Equals(Constants.Number70))
                    {
                        lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWife + ", " + strUserFirstName + Constants.steps23 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                        lblKey1.Text = string.Format(startingValue.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps7 + strUserFirstName + Constants.steps6WHB);
                    }
                    else
                    {
                        if (int.Parse(ageWife) < int.Parse(sWifeFRA))
                            lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWife + ", " + strUserFirstName + Constants.ReducedWorkBenefit + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        else if (int.Parse(ageWife) == int.Parse(sWifeFRA))
                            lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWife + Constants.FRAText + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        else
                            lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWife + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        lblKey1.Text = string.Format(startingValue.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps7 + strUserFirstName + Constants.steps6);
                    }
                }
                lblSteps2.Text = string.Format(Constants.Line2 + Constants.Age + " " + Constants.Number70 + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                lblKey2.Text = string.Format(AnualIncome.ToString(Constants.AMT_FORMAT) + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey);

                //Set values for Keys and steps to dispay on pdf
                SetKeyAndStepsValuesForPDF(Constants.GridLatest);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// show the keys and steps below the grid for grid GridStandard
        /// </summary>
        /// <param name="cumm"></param>
        /// <param name="startingValue"></param>
        /// <param name="valueAT70"></param>
        /// <param name="ageWife"></param>
        /// <param name="strUserFirstName"></param>
        public void renderDataIntoGridStartegy2(decimal cumm, decimal startingValue, decimal valueAT70, string ageWife, string strUserFirstName, decimal AnualIncome, string strAnnualIncomeAge)
        {
            try
            {
                Customers customer = new Customers();
                string sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                sWifeFRA = sWifeFRA.Substring(0, 2);
                string ageWifeStarting = ageWife;

                if (Globals.Instance.WifeNumberofMonthsBelowGrid > 0)
                {
                    lblNoteWife0.Text = "# includes " + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    ageWifeStarting = Globals.Instance.WifeNumberofYears + " years and " + Globals.Instance.WifeNumberofMonthsAboveGrid + " months ";
                }
                if (Globals.Instance.WifeNumberofMonthsinSteps > 0)
                {
                    lblNoteWife0.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserFirstName;
                    ageWifeStarting = Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + " years and " + Globals.Instance.WifeNumberofMonthsinSteps + " months ";
                }
                if (startingValue != 0)
                {
                    if (ageWife.Equals(Constants.Number70))
                    {
                        lblSteps3.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWife + ", " + strUserFirstName + Constants.steps23 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                        lblKey3.Text = string.Format(startingValue.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps7 + strUserFirstName + Constants.steps6WHB);
                    }
                    else
                    {
                        if (int.Parse(ageWife) < int.Parse(sWifeFRA))
                            lblSteps3.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.ReducedWorkBenefit + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        else if (int.Parse(ageWife) == int.Parse(sWifeFRA))
                            lblSteps3.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + Constants.FRAText + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        else
                            lblSteps3.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWife + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        lblKey3.Text = string.Format(startingValue.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps7 + strUserFirstName + Constants.steps6);
                    }
                }
                lblSteps4.Text = string.Format(Constants.Line2 + Constants.Age + " " + strAnnualIncomeAge + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                lblKey4.Text = string.Format(AnualIncome.ToString(Constants.AMT_FORMAT) + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey);

                //Set values for Keys and steps to dispay on pdf
                SetKeyAndStepsValuesForPDF(Constants.GridStandard);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// show the keys and steps below the grid for grid GridAsap
        /// </summary>
        /// <param name="cumm"></param>
        /// <param name="startingValue"></param>
        /// <param name="valueAT70"></param>
        /// <param name="ageWife"></param>
        /// <param name="strUserFirstName"></param>
        public void renderDataIntoGridStartegy3(decimal cumm, decimal startingValue, decimal valueAT70, string ageWife, string strUserFirstName, decimal AnualIncome)
        {
            try
            {
                Customers customer = new Customers();
                string sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                sWifeFRA = sWifeFRA.Substring(0, 2);
                string ageWifeStarting = ageWife;
                if (Globals.Instance.WifeNumberofMonthsinSteps > 0)
                {
                    lblNoteWife1.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserFirstName;
                    ageWifeStarting = Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + " years and " + Globals.Instance.WifeNumberofMonthsinSteps + " months ";
                }
                if (Globals.Instance.WifeNumberofMonthsBelowGrid > 0)
                {
                    lblNoteWife1.Text = "# includes " + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    ageWifeStarting = Globals.Instance.WifeNumberofYears + " years and " + Globals.Instance.WifeNumberofMonthsAboveGrid + " months ";
                }
                if (startingValue != 0)
                {
                    if (ageWife.Equals(Constants.Number70))
                    {
                        lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWife + ", " + strUserFirstName + Constants.steps23 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                        lblKey5.Text = string.Format(startingValue.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps7 + strUserFirstName + Constants.steps6WHB);
                    }
                    else
                    {
                        if (int.Parse(ageWife) < int.Parse(sWifeFRA))
                            lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.ReducedWorkBenefit + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        else if (int.Parse(ageWife) == int.Parse(sWifeFRA))
                            lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + Constants.FRAText + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        else
                            lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        lblKey5.Text = string.Format(startingValue.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps7 + strUserFirstName + Constants.steps6);
                    }
                }
                lblSteps6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                lblKey6.Text = string.Format(AnualIncome.ToString(Constants.AMT_FORMAT) + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey);

                //Set values for Keys and steps to dispay on pdf
                SetKeyAndStepsValuesForPDF(Constants.GridAsap);


            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcSingle, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcSingle + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// calculate current age of Wife
        /// </summary>
        /// <param name="husbBD"></param>
        /// <returns></returns>
        protected void intAgeForWife(DateTime wifeBD)
        {
            try
            {
                TimeSpan Span = DateTime.Now - wifeBD;
                DateTime Age = DateTime.MinValue + Span;
                int Years = Age.Year - 1;
                int Months = Age.Month - 1;
                int Days = Age.Day - 1;
                intCurrentAgeWifeYear = Years;
                Globals.Instance.strCurrentAgeWife = Years.ToString() + " years and " + Months.ToString() + " months ";
                if (Days >= 28)
                    Months += 1;
                if (Months >= 6)
                    Years += 1;
                intCurrentAgeWifeMonth = Months;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Used to add strategy explaination to PDF
        /// </summary>
        /// <param name="pdfDoc">PDF Document</param>
        private void AddExplainationToPDF(ref Document pdfDoc)
        {
            try
            {
                #region Add titles
                pdfDoc.Add(SiteNew.generateParagraph(Constants.StrategyExpanationTitle, false, true, true));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.SinglePeople, false, true, true));
                #endregion Add titles

                #region What is the Primary Goal Of the Paid to Wait Calculator?
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion1, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSingleQuestion1Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What is the Primary Goal of the Paid to Wait Calculator?

                #region How the Paid to Wait Calculator Works
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion4, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSingleQuestion2Answer1, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSingleQuestion2Answer2, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSingleQuestion2Answer3, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSingleQuestion2Answer4, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSingleQuestion2Answer5, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion How the Paid to Wait Calculator Works

                #region What if You Continue to Work?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion5, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSingleQuestion3Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What if You Continue to Work?


                #region What You Can Expect
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion7, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSingleQuestion4Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What You Can Expect




                #region What are the “Key Benefit Numbers” in the Strategies?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion8, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSingleQuestion5Answer1, false, false, false));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSingleQuestion5Answer2, false, false, false));
                pdfDoc.Add(new Paragraph(" "));

                #endregion What are the “Key Benefit Numbers” in the Strategies?



                #region What are the “Next Steps” in the Strategies?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion9, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSingleQuestion6Answer1, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSingleQuestion6Answer2, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSingleQuestion6Answer3, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSingleQuestion6Answer4, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What are the “Next Steps” in the Strategies?

                #region Add Note for explanation
                pdfDoc.Add(SiteNew.generateParagraph(Constants.StrategyExplanationNote, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                #endregion Add Note for explanation

                #region Add Good Luck Text
                pdfDoc.Add(SiteNew.generateParagraph(Constants.GoodLuckText, false, true, true));
                #endregion Add Good Luck Text
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }

        /// <summary>
        /// Used to add strategy instructions in pdf
        /// </summary>
        /// <param name="pdfDoc"></param>
        private void AddInstructionBeforeSeeStrategy(ref Document pdfDoc)
        {
            try
            {
                #region Add Instructions
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.StrategyInstruction, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion Add Instructions
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessageLabel.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessageLabel.Visible = true;
            }
        }
        #endregion Other Operations
    }
}