using CalculateDLL;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Symbolics;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using System.Reflection;
using Calculate.Objects;

namespace Calculate
{
    public partial class WebForm4 : System.Web.UI.Page
    {

        #region Variable Declaration
        // Input items
        bool hasAmtFormatValue1 = false, hasAmtFormatValue2 = false, hasAmtFormatValue3 = false;
        int intAgeWife, intAgeWifeMonth, intAgeWifeDays, intCurrentAgeWifeMonth, intCurrentAgeWifeYear, intFRAage;
        decimal decBeneWife, decBeneHusb;
        decimal colaAmt = 1;
        int birthYearWife = 0;
        DataTable dtDetails;
        String customerFirstName;
        String CSpouseFirstname;
        Customers customer = new Customers();
        #endregion

        #region Events
        /// <summary>
        /// code called when page loads
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                validateSession();
                if (!IsPostBack)
                {
                    if (Session["Demo"] != null)
                    {
                        Customers customer = new Customers();
                        DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                        DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                        BirthWifeMM.Text = husbandBirthDate.Month.ToString();
                        BirthWifeDD.Text = husbandBirthDate.Day.ToString();
                        birthWifeTempYYYY.Text = husbandBirthDate.Year.ToString();
                        BirthWifeYYYY.Text = husbandBirthDate.ToShortDateString();
                        BeneWife.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                        BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                        PanelParms.Visible = false;
                        ValidateAndCalc();

                        //Send mail to the demo user before loading the strategy page
                        Globals.Instance.BoolEmailReport = true;
                        exportAllToPdf();
                        Globals.Instance.BoolEmailReport = false;
                    }
                    else if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                    {
                        ////BtnChange.Text = Constants.BtnChangeText;
                        ///* Disabled textbox and radio button so that institutional user can only view customer details */
                        //BirthWifeYYYY.Enabled = false;
                        //BeneWife.Enabled = false;
                        //BeneHusband.Enabled = false;
                        ////CheckBoxCola.Enabled = false;
                        //// Initialize page variables                
                        //ErrorMessage.Text = String.Empty;
                        //PanelGrids.Visible = false;
                        //PanelParms.Visible = false;
                        ////CheckBoxCola.Checked = true;
                        //BirthWifeMM.Focus();
                        ///* Show Customer Widowed related Financial information */
                        //ShowAllInformation();
                        //Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.ClientScriptDatePicker, "disableDatePicker();", true);

                        Customers customer = new Customers();
                        DataTable dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                        DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                        BirthWifeMM.Text = husbandBirthDate.Month.ToString();
                        BirthWifeDD.Text = husbandBirthDate.Day.ToString();
                        birthWifeTempYYYY.Text = husbandBirthDate.Year.ToString();
                        BirthWifeYYYY.Text = husbandBirthDate.ToShortDateString();
                        BeneWife.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                        BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                        PanelParms.Visible = false;
                        PanelEntry.Visible = false;
                        ValidateAndCalc();
                        rdbtnStrategy1.Checked = false;
                        rdbtnStrategy2.Checked = false;
                        rdbtnStrategy3.Checked = false;
                        if (Globals.Instance.BoolEmailReport || Globals.Instance.BoolAdvisorDownloadReport)
                        {
                            Globals.Instance.BoolAdvisorDownloadReport = false;
                            exportAllToPdf();
                            Response.Redirect(Constants.RedirectManageCustomers);
                        }
                    }
                    else if (Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                    {
                        Customers customer = new Customers();
                        DataTable dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                        DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                        BirthWifeMM.Text = husbandBirthDate.Month.ToString();
                        BirthWifeDD.Text = husbandBirthDate.Day.ToString();
                        birthWifeTempYYYY.Text = husbandBirthDate.Year.ToString();
                        BirthWifeYYYY.Text = husbandBirthDate.ToShortDateString();
                        BeneWife.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                        BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                        PanelParms.Visible = false;
                        PanelEntry.Visible = false;
                        ValidateAndCalc();
                        rdbtnStrategy1.Checked = false;
                        rdbtnStrategy2.Checked = false;
                        rdbtnStrategy3.Checked = false;
                        if (Globals.Instance.BoolEmailReport || Globals.Instance.BoolAdvisorDownloadReport)
                        {
                            Globals.Instance.BoolAdvisorDownloadReport = false;
                            exportAllToPdf();
                            Response.Redirect(Constants.RedirectManageCustomers);
                        }
                    }
                    else if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor))
                    {
                        Customers customer = new Customers();
                        DataTable dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                        DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                        BirthWifeMM.Text = husbandBirthDate.Month.ToString();
                        BirthWifeDD.Text = husbandBirthDate.Day.ToString();
                        birthWifeTempYYYY.Text = husbandBirthDate.Year.ToString();
                        BirthWifeYYYY.Text = husbandBirthDate.ToShortDateString();
                        BeneWife.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                        BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                        PanelParms.Visible = false;
                        ValidateAndCalc();
                        rdbtnStrategy1.Checked = false;
                        rdbtnStrategy2.Checked = false;
                        rdbtnStrategy3.Checked = false;
                        if (Globals.Instance.BoolEmailReport || Globals.Instance.BoolAdvisorDownloadReport)
                        {
                            Globals.Instance.BoolAdvisorDownloadReport = false;
                            exportAllToPdf();
                            Response.Redirect(Constants.RedirectManageCustomers);
                        }
                    }
                    else if (Session[Constants.SessionRole].ToString().Equals(Constants.Customer))
                    {
                        Customers customer = new Customers();
                        DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                        DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                        BirthWifeMM.Text = husbandBirthDate.Month.ToString();
                        BirthWifeDD.Text = husbandBirthDate.Day.ToString();
                        birthWifeTempYYYY.Text = husbandBirthDate.Year.ToString();
                        BirthWifeYYYY.Text = husbandBirthDate.ToShortDateString();
                        BeneWife.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                        BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                        PanelParms.Visible = false;

                        PanelGrids.Visible = false;//Added for new development (on 16/09)
                        PanelEntry.Visible = false;//Added for new development (on 16/09)
                        ValidateAndCalc();

                        if (Convert.ToString(dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription]).Equals(Constants.FlagNo))
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "displaySubscriptionPopUp(false);", true);
                            ValidateAndCalc();
                            //to show the Paid to wait value on the popup for new user
                            decimal amt = decimal.Parse(Session[Constants.WMaxCumm].ToString());
                            MaxCumm.Text = Constants.SubscriptionPopupText + Environment.NewLine + amt.ToString(Constants.AMT_FORMAT);
                        }
                        //Commented for new development (on 16/09)
                        //else
                        //    ValidateAndCalc();
                    }
                    else if (!string.IsNullOrEmpty(Session["CustAfterTrans"] as string))
                    {
                        Customers customer = new Customers();
                        DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                        DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                        BirthWifeMM.Text = husbandBirthDate.Month.ToString();
                        BirthWifeDD.Text = husbandBirthDate.Day.ToString();
                        birthWifeTempYYYY.Text = husbandBirthDate.Year.ToString();
                        BirthWifeYYYY.Text = husbandBirthDate.ToShortDateString();
                        BeneWife.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                        BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                        PanelParms.Visible = false;
                        ValidateAndCalc();
                    }
                    else
                    {
                        // Initialize page variables                
                        ErrorMessage.Text = String.Empty;
                        PanelGrids.Visible = false;
                        PanelParms.Visible = false;
                        //CheckBoxCola.Checked = true;
                        BirthWifeMM.Focus();
                        /* Show Customer Widowed related Financial information */
                        ShowAllInformation();
                    }
                }
                else
                {
                    rdbtnStrategy1.Checked = false;
                    rdbtnStrategy2.Checked = false;
                    rdbtnStrategy3.Checked = false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Click event for Calculate Button,to build the grids
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Calculate_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                Button clicked = (Button)sender;
                ResetValues();

                if (clicked.Text == BtnCalculate.Text)
                {
                    if (Convert.ToString(dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription]).Equals(Constants.FlagNo))
                    {
                        if (Session[Constants.SessionRegistered] != null)
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displaySubscriptionPopUp(true);", true);
                        }
                        else
                        {
                            if (BeneWife.Text != string.Empty && BeneHusband.Text != string.Empty)
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displaySubscriptionPopUp(false);", true);
                                ValidateAndCalc();
                                //to show paid to wait number on subscription popup
                                decimal amt = decimal.Parse(Session[Constants.WMaxCumm].ToString());
                                MaxCumm.Text = Constants.SubscriptionPopupText + amt.ToString(Constants.AMT_FORMAT);
                            }
                        }
                    }
                    else
                    {
                        ValidateAndCalc();
                    }
                }
                else //if (clicked.Text == BtnChange.Text)
                {
                    if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                    {
                        Response.Redirect(Constants.RedirectSocialSecurityCalculator, false);
                    }
                    else
                    {
                        BackToParms();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to close the pop up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            try
            {
                //Previous redirection
                //Response.Redirect(Constants.RedirectCalcWidowed, false);
                Response.Redirect(Constants.RedirectInitialSocialSecurity, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Print Pdf File
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PrintAll_Click(object sender, EventArgs e)
        {
            try
            {
                //export all grid and Charts to PDF
                exportAllToPdf();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.DownloadError;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to take user to welcome page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveBasicInfoAndProceed_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                string sIsSubscription = dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription].ToString();
                if (sIsSubscription.Equals(Constants.FlagNo))
                {
                    Response.Redirect(Constants.RedirectWelcomeUser, false);
                }
                else
                {
                    Response.Redirect(Constants.RedirectSocialSecurityCalculator, false);
                    //Response.Redirect("~/CalFinancial/InitialSocialSecurityCalculator.aspx", false);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// subscribe user who are in trial period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnTrial_Click(object sender, EventArgs e)
        {
            try
            {
                // Update column with package plan
                customer.updateCustomerSubscriptionPlan(Session[Constants.SessionUserId].ToString(), Constants.WithoutTrialPeriod);
                //Changes for live Server
                //Response.Redirect(Constants.TrialLiveServerURL + Session[Constants.SessionUserId].ToString(), true);
                //Response.Redirect(Constants.NewSingleUseURL + Session[Constants.SessionUserId].ToString(), true);
                //Changes for InfusionSoft live Server
                Response.Redirect(Constants.NewSingleUseURLInfusion, true);
                // Redirect to chargify url 
                //Response.Redirect(Constants.TrialTestServerURL + Session[Constants.SessionUserId].ToString(), true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Subscribe user who are not in trial period
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnWithoutTrial_Click(object sender, EventArgs e)
        {
            try
            {
                // Update column with package plan
                customer.updateCustomerSubscriptionPlan(Session[Constants.SessionUserId].ToString(), Constants.TrialPeriod);
                //Changes for live Server
                Response.Redirect(Constants.WithoutTrialLiveServerURL + Session[Constants.SessionUserId].ToString(), true);
                // Redirect to chargify url 
                //Response.Redirect(Constants.WithoutTrialTestServerURL + Session[Constants.SessionUserId].ToString(), true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// to close the explanation popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgCancel_Click(object sender, EventArgs e)
        {
            try
            {
                popup.Hide();
                ValidateAndCalc();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to show the explanation popup for all strategies.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExplain_Click(object sender, EventArgs e)
        {
            try
            {
                popup.Show();
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "displayPanelExplanation();", true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }
        #endregion

        #region Validation
        /// <summary>
        /// code to validate the session
        /// </summary>
        public void validateSession()
        {
            try
            {
                Session[Constants.SessionRegistered] = null;
                if (Session[Constants.SessionUserId] == null)
                {
                    Response.Redirect(Constants.RedirectLogin, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Check input values
        /// </summary>
        /// <returns></returns>
        protected bool ValidateInput()
        {
            try
            {
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor) || Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                    dtDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                else
                    dtDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                customerFirstName = dtDetails.Rows[0][Constants.CustomerFirstName].ToString();
                CSpouseFirstname = dtDetails.Rows[0][Constants.CustomerSpouseFirstName].ToString();
                ErrorMessage.Text = string.Empty;
                bool isValid = true;
                int birthYYYY, birthMM, birthDD;
                DateTime today = DateTime.Now;
                DateTime wifeBD = today;
                birthMM = SiteNew.ValidateNumeric(BirthWifeMM, 1, 12, Constants.Text0, ref isValid, ErrorMessage);
                birthDD = SiteNew.ValidateNumeric(BirthWifeDD, 1, 31, Constants.Text1, ref isValid, ErrorMessage);
                birthYYYY = SiteNew.ValidateNumeric(birthWifeTempYYYY, 1900, 2020, Constants.Text2, ref isValid, ErrorMessage);
                if (isValid)
                {
                    birthYearWife = int.Parse(birthWifeTempYYYY.Text);
                    wifeBD = new DateTime(birthYYYY, birthMM, birthDD);
                }
                TimeSpan Span = DateTime.Now - wifeBD;
                DateTime Age = DateTime.MinValue + Span;
                int Years = Age.Year - 1;
                int Months = Age.Month - 1;
                int Days = Age.Day - 1;
                if (Days >= 28)
                    Months += 1;
                if (Months >= 6)
                    Years += 1;
                intAgeWife = Years;
                intAgeForWife(wifeBD);
                if (wifeBD.Day >= 28 && wifeBD.Day != DateTime.Now.Day)
                {
                    intAgeWifeMonth = wifeBD.Month + 1;
                    intAgeWifeDays = wifeBD.Day;
                }
                else
                {
                    intAgeWifeMonth = wifeBD.Month;
                    intAgeWifeDays = wifeBD.Day;
                }
                // Parse benefits
                decimal.TryParse(this.BeneWife.Text, out decBeneWife);
                decimal.TryParse(this.BeneHusband.Text, out decBeneHusb);
                if (isValid)
                {
                    PanelGrids.Visible = true;
                    PanelEntry.Visible = false;
                    PanelParms.Visible = true;
                    lblCustomerName.Text = customerFirstName;
                    lblCustomerDOB.Text = wifeBD.ToShortDateString();
                    lblCustomerFRA.Text = customer.WidowFRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                    lblCustomerBenefit.Text = decBeneWife.ToString(Constants.AMT_FORMAT);
                    lblCustomerSpouseBenefit.Text = decBeneHusb.ToString(Constants.AMT_FORMAT);
                }
                else
                {
                    ErrorMessage.Text += ".";
                }
                if (!String.IsNullOrEmpty(ErrorMessage.Text))
                {
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayErrorMessage();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "hideErrorMessage();", true);
                }
                return isValid;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                return false;
            }
        }

        /// <summary>
        /// Valdate Input and calculate retirement plan
        /// </summary>
        protected void ValidateAndCalc()
        {
            try
            {
                string WifeBirth = BirthWifeYYYY.Text.ToString();
                char[] delimiters = new char[] { '/' };
                string[] WifeBirthArray = WifeBirth.Split(delimiters, 3);
                birthWifeTempYYYY.Text = WifeBirthArray[2];
                BirthWifeMM.Text = WifeBirthArray[0];
                BirthWifeDD.Text = WifeBirthArray[1];
                if (ValidateInput())
                {
                    BuildPage();
                    // Save All Details to respective tables According to martial status
                    UpdateCustomerFinancialDetails();
                    SiteNew.renderGridViewIntoStackedChart(GridAsap, ChartAsap, LabelAsap, Constants.Widowed, false);
                    SiteNew.renderGridViewIntoStackedChart(GridLatest, ChartLatest, LabelLatest, Constants.Widowed, false);
                    SiteNew.renderGridViewIntoStackedChart(GridSpouse, ChartSpouse, LabelSpouse, Constants.Widowed, false);
                    SiteNew.renderGridViewIntoStackedChart(GridClaim67, ChartClaim67, LabelClaim67, Constants.Widowed, false);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion

        #region CRUD Operation

        /// <summary>
        /// This method basic information of customer in form
        /// </summary>
        private void ShowAllInformation()
        {
            try
            {
                DataTable dtCustomerFinancialDetails = customer.getCustomerFinancialDetailsByStatus(Session[Constants.SessionUserId].ToString(), Constants.Widowed);
                if (dtCustomerFinancialDetails.Rows.Count != 0)
                {
                    if (!String.IsNullOrEmpty(dtCustomerFinancialDetails.Rows[0][Constants.CustomerBirthDate].ToString()))
                    {
                        DateTime strBirthDate = (DateTime)dtCustomerFinancialDetails.Rows[0][Constants.CustomerBirthDate];
                        BirthWifeDD.Text = strBirthDate.Day.ToString();
                        BirthWifeMM.Text = strBirthDate.Month.ToString();
                        BirthWifeYYYY.Text = strBirthDate.ToShortDateString();
                    }
                    BeneWife.Text = dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoRetAgeBenefit].ToString();
                    BeneHusband.Text = dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoWidowedSpousesRetAgeBenefit].ToString();
                    // string strCola = dtCustomerFinancialDetails.Rows[0][Constants.SecurityInfoCola].ToString();
                    //if (strCola.Equals("T"))
                    //    CheckBoxCola.Checked = true;
                    //else
                    //    CheckBoxCola.Checked = false;
                }
                else
                {
                    DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
                    /* Check if Birthdate exists or not */
                    if (!String.IsNullOrEmpty(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString()))
                    {
                        DateTime strBirthDate = (DateTime)dtCustomerDetails.Rows[0][Constants.CustomerBirthDate];
                        BirthWifeDD.Text = strBirthDate.Day.ToString();
                        BirthWifeMM.Text = strBirthDate.Month.ToString();
                        BirthWifeYYYY.Text = strBirthDate.ToShortDateString();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// UpdateCustomerFinancialDetails
        /// </summary>
        public void UpdateCustomerFinancialDetails()
        {
            try
            {
                SecurityInfoWidowed securityInfoWidowed = new SecurityInfoWidowed();
                securityInfoWidowed.BirthDate = string.Format(Constants.stringDateFormat, Convert.ToDateTime(BirthWifeMM.Text + Constants.BackSlash + BirthWifeDD.Text + Constants.BackSlash + birthWifeTempYYYY.Text));
                securityInfoWidowed.RetAgeBenefit = BeneWife.Text;
                securityInfoWidowed.SpousesRetAgeBenefit = BeneHusband.Text;
                securityInfoWidowed.CustomerID = Session[Constants.SessionUserId].ToString();
                //if (CheckBoxCola.Checked)
                securityInfoWidowed.Cola = Constants.ColaStatusT;
                //else
                //    securityInfoWidowed.Cola = "F";
                customer.UpdateCustomerFinancialDetailsForWidowed(securityInfoWidowed);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion

        #region Other Operations

        /// <summary>
        /// code buid the sort experession to determine the best strategy
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int Compare2(KeyValuePair<string, int> a, KeyValuePair<string, int> b)
        {
            try
            {
                return a.Value.CompareTo(b.Value);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                return 0;
            }
        }

        /// <summary>
        /// Build all the alternative strategies on the page
        /// </summary>
        protected void BuildPage()
        {
            try
            {
                ResetValues();
                #region Setting all basic properties
                // Set calculation properties
                Calc Calc = new Calc()
                {
                    intAgeWife = intAgeWife,
                    decBeneWife = decBeneWife,
                    birthYearWife = birthYearWife,
                    decBeneHusb = decBeneHusb,
                    intAgeWifeMonth = intAgeWifeMonth,
                    intAgeWifeDays = intAgeWifeDays,
                    intCurrentAgeWifeMonth = intCurrentAgeWifeMonth,
                    intCurrentAgeWifeYear = intCurrentAgeWifeYear
                };
                Calc.colaAmt = 1.025m;
                colaAmt = Calc.colaAmt;
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor) || Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                    dtDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                else
                    dtDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                string strUserFirstName = dtDetails.Rows[0][Constants.CustomerFirstName].ToString();
                Calc.Your_Name = strUserFirstName;
                CommonVariables.strUserName = strUserFirstName;
                string wifeFra = customer.FRA(birthYearWife);
                int intWifeFra = int.Parse(wifeFra.Substring(0, 2));
                Globals.Instance.intWifeFRAAge = intWifeFra;
                Globals.Instance.intWifeAge = intAgeWife;
                // Build all of the alternative grids
                #endregion Setting all basic properties

                #region Code build all alternate grids
                //Strategy hidden as per client request on 13/05/2015
                //As Early as possible strategy-Strategy 2 on the grid.
                int cummAsap = Calc.BuildSingle(GridAsap, Calc.calc_strategy.WidowedSelf, Calc.calc_solo.Widowed, 0, ref NoteAsap, Constants.WAsap);
                int[] asapBeAmts = Calc.breakEvenAmt;
                //highlight the numbers on grid
                Calc.Highlight(GridAsap, Calculate.Calc.calc_solo.Widowed, Constants.GridAsap);
                int asapAge70Combined = Calc.age70CombBene;
                bool badAsap = Calc.badStrat;
                //cummulative value at age 69
                Session[Constants.WidowAasapCumm] = Calc.WCummValue;
                //show the annual income value at apid to wait age
                Session[Constants.WidowAnnualAsap] = Calc.WidowAnnualIncomeValue.ToString(Constants.AMT_FORMAT);
                //render keys ans dteps in the grid
                renderDataIntoGridStartegy3(Calc.WCummValue, Calc.SValueStarting, Calc.ValueAtLastChange, Calc.SAgeValue, strUserFirstName, Calc.AgeAtLastChange, Calc.BoolAboveFRAageStep2, Calc.WidowAnnualIncomeValue);
                Globals.Instance.strWidowStarting = string.Empty;
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                Globals.Instance.WifeNumberofMonthinNote = 0;
                Globals.Instance.BoolWHBTakenWidowAt66 = false;
                ResetAnualIncomeVariables();

                int Claim67 = Calc.BuildSingleWidow(GridClaim67, Calc.calc_strategy.Claim67, Calc.calc_solo.Widowed, 0, ref NoteAsap, Constants.WClaim67);
                //highlight the numbers on grid
                Calc.Highlight(GridClaim67, Calculate.Calc.calc_solo.Widowed, Constants.GridClaim67);
                Session[Constants.WidowClaim67Cumm] = Calc.WCummValue;
                //show the annual income value when the paid to  number is shown
                Session[Constants.WidowAnnualClaim67] = Calc.WidowAnnualIncomeValue.ToString(Constants.AMT_FORMAT);
                renderDataIntoGridStartegy4(Calc.WCummValue, Calc.SValueStarting, Calc.AgeValueAt70, Calc.SAgeValue, strUserFirstName, Calc.AgeAtLastChange, Calc.WidowAnnualIncomeValue);
                Globals.Instance.strWidowStarting = string.Empty;
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                Globals.Instance.WifeNumberofMonthinNote = 0;
                Globals.Instance.BoolWHBTakenWidowAt66 = false;
                ResetAnualIncomeVariables();

                int cumm66 = Calc.BuildSingle(GridSpouse, Calc.calc_strategy.Widowed66, Calc.calc_solo.Widowed, Calc.calc_start.asap60, ref NoteSpouse, Constants.WSpouse);
                int spouseAge70Combined = Calc.age70CombBene;
                bool badStrat66 = Calc.badStrat;
                //highlight the numbers on grid
                Calc.Highlight(GridSpouse, Calculate.Calc.calc_solo.Widowed, Constants.GridSpouse);
                //cummulative value at age 69
                Session[Constants.WidowSpouseCumm] = Calc.WCummValue;
                //show the annual income value when the paid to  number is shown
                Session[Constants.WidowAnnualSpouse] = Calc.WidowAnnualIncomeValue.ToString(Constants.AMT_FORMAT);
                renderDataIntoGridStartegy2(Calc.WCummValue, Calc.SValueStarting, Calc.AgeValueAt70, Calc.SAgeValue, strUserFirstName, Calc.AgeAtLastChange, Calc.BoolAboveFRAageStep2, Calc.WidowAnnualIncomeValue);
                Globals.Instance.strWidowStarting = string.Empty;
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                Globals.Instance.WifeNumberofMonthinNote = 0;
                Globals.Instance.BoolWHBTakenWidowAt66 = false;
                ResetAnualIncomeVariables();

                int cumm70 = Calc.BuildSingle(GridLatest, Calc.calc_strategy.Widowed70, Calc.calc_solo.Widowed, Calc.calc_start.asap60, ref NoteLatest, Constants.WLatest);
                int maxAge70Combined = Calc.age70CombBene;
                //highlight the numbers on grid
                Calc.Highlight(GridLatest, Calculate.Calc.calc_solo.Widowed, Constants.GridLatest);
                bool badStrat70 = Calc.badStrat;
                //cummulative value at age 69
                Session[Constants.WidowLatestCumm] = Calc.WCummValue;
                //to show anual income at last change of benefit
                Session[Constants.WidowAnnualLatest] = Calc.WidowAnnualIncomeValue.ToString(Constants.AMT_FORMAT);
                renderDataIntoGridStartegy1(Calc.WCummValue, Calc.SValueStarting, Calc.AgeValueAt70, Calc.SAgeValue, strUserFirstName, Calc.AgeAtLastChange, Calc.WidowAnnualIncomeValue);
                Globals.Instance.strWidowStarting = string.Empty;
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                Globals.Instance.WifeNumberofMonthinNote = 0;

                //wife and husb benefits to compare in case they are same
                Session["HusbandBenefits"] = Calc.decBeneHusb;
                Session["WifeBenefits"] = Calc.decBeneWife;
                //to compare the working and surviour value at age 66 and 70 to show the strategies
                Session[Constants.surviourValueAtAge66] = Calc.surviourValueAtAge66;
                Session[Constants.surviourValueAtAge70] = Calc.surviourValueAtAge70;
                Session[Constants.workingValueAtAge66] = Calc.workingValueAtAge66;
                Session[Constants.workingValueAtAge70] = Calc.workingValueAtAge70;
                #endregion Code build all alternate grids

                #region to find best and second highest plan
                var list = new List<KeyValuePair<string, int>>();
                Calculate.Calc objCalc = new Calculate.Calc();
                intFRAage = Convert.ToInt32(objCalc.FRA(int.Parse(birthWifeTempYYYY.Text)));
                //if the benfits are same than show only 2 strategies
                if (Session["HusbandBenefits"].Equals(Session["WifeBenefits"]))
                {
                    list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.WidowLatestCumm].ToString())));
                    //list.Add(new KeyValuePair<string, int>("BestSrategy", int.Parse(Session["WidowAasapCumm"].ToString())));
                    list.Add(new KeyValuePair<string, int>(Constants.SuspendStrategy, int.Parse(Session[Constants.WidowSpouseCumm].ToString())));
                    lblCombinedIncome1.Text = Session[Constants.WidowAnnualLatest].ToString();
                    lblCombinedIncome3.Text = Session[Constants.WidowAnnualSpouse].ToString();
                }
                //check if the survivor value are greater both at age 66 and age 70
                //if yes than show only one strategy
                else if (Convert.ToInt32(Session[Constants.surviourValueAtAge66]) > Convert.ToInt32(Session[Constants.workingValueAtAge66]) && Convert.ToInt32(Session[Constants.surviourValueAtAge70]) > Convert.ToInt32(Session[Constants.workingValueAtAge70]))
                {
                    list.Add(new KeyValuePair<string, int>(Constants.BestSrategy, int.Parse(Session[Constants.WidowAasapCumm].ToString())));
                    lblCombinedIncome2.Text = Session[Constants.WidowAnnualAsap].ToString();
                }
                //check if the working value at age 70 is greater than surviour value at age 70
                else if (Calc.workingValueAtAge70 > Calc.surviourValueAtAge70)//
                {
                    list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.WidowLatestCumm].ToString())));
                    if (intAgeWife < intFRAage)
                        list.Add(new KeyValuePair<string, int>(Constants.Claim67Strategy, int.Parse(Session[Constants.WidowClaim67Cumm].ToString())));
                    if (Calc.workingValueAtAge66 > Calc.surviourValueAtAge66)
                    {
                        list.Add(new KeyValuePair<string, int>(Constants.SuspendStrategy, int.Parse(Session[Constants.WidowSpouseCumm].ToString())));
                        lblCombinedIncome3.Text = Session[Constants.WidowAnnualSpouse].ToString();
                    }
                    else
                    {
                        if ((intAgeWife < intFRAage) && (Calc.workingValueAtAge70 > Calc.surviourValueAtAge70))
                            list.Add(new KeyValuePair<string, int>(Constants.BestSrategy, int.Parse(Session[Constants.WidowAasapCumm].ToString())));
                        lblCombinedIncome3.Text = Session[Constants.WidowAnnualAsap].ToString();
                    }
                    lblCombinedIncome1.Text = Session[Constants.WidowAnnualLatest].ToString();
                    lblCombinedIncome2.Text = Session[Constants.WidowAnnualClaim67].ToString();
                }
                else
                {
                    list.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.WidowLatestCumm].ToString())));
                    if (Calc.workingValueAtAge66 > Calc.surviourValueAtAge66)
                    {
                        list.Add(new KeyValuePair<string, int>(Constants.SuspendStrategy, int.Parse(Session[Constants.WidowSpouseCumm].ToString())));
                        lblCombinedIncome3.Text = Session[Constants.WidowAnnualSpouse].ToString();
                    }
                    else
                    {
                        list.Add(new KeyValuePair<string, int>(Constants.BestSrategy, int.Parse(Session[Constants.WidowAasapCumm].ToString())));
                        lblCombinedIncome3.Text = Session[Constants.WidowAnnualSpouse].ToString();
                    }
                    lblCombinedIncome1.Text = Session[Constants.WidowAnnualLatest].ToString();
                }
                if (list.Count == 3)
                {
                    //list.Sort(Compare2);
                    Session[Constants.WMaxCumm] = list[0].Value.ToString();
                    // list.Reverse();
                    lblPaidNumber1.Text = list[0].Value.ToString(Constants.AMT_FORMAT);
                    lblPaidNumber2.Text = list[1].Value.ToString(Constants.AMT_FORMAT);
                    lblPaidNumber3.Text = list[2].Value.ToString(Constants.AMT_FORMAT);
                    hasAmtFormatValue1 = true;
                    hasAmtFormatValue2 = true;
                    hasAmtFormatValue3 = true;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showThreeStrategy('{0}','{1}','{2}');", list[0].Key.ToString(), list[1].Key.ToString(), list[2].Key.ToString()), true);
                }
                else if (list.Count == 2)
                {
                    // list.Sort(Compare2);
                    Session[Constants.WMaxCumm] = list[0].Value.ToString();
                    //list.Reverse();
                    lblPaidNumber1.Text = list[0].Value.ToString(Constants.AMT_FORMAT);
                    lblPaidNumber3.Text = list[1].Value.ToString(Constants.AMT_FORMAT);
                    lblCombinedIncome2.Text = string.Empty;
                    hasAmtFormatValue1 = true;
                    hasAmtFormatValue3 = true;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showTwoStrategy('{0}','{1}');", list[0].Key.ToString(), list[1].Key.ToString()), true);
                }
                else
                {
                    // list.Sort(Compare2);
                    Session[Constants.WMaxCumm] = list[0].Value.ToString();
                    //list.Reverse();
                    lblPaidNumber2.Text = list[0].Value.ToString(Constants.AMT_FORMAT);
                    lblCombinedIncome1.Text = string.Empty;
                    lblCombinedIncome3.Text = string.Empty;
                    hasAmtFormatValue2 = true;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showOneStrategy('{0}');", list[0].Key.ToString()), true);
                }
                AddPaidAndAnnualValForPDF();

                #endregion to find best and second highest plan

                #region Code show the popup when HERE link is clicked
                PanelGrids.Visible = true;
                #endregion Code show the popup when HERE link is clicked
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// redirect advisor back to the managecustomer page
        /// </summary>
        /// <param name="strGridName"></param>
        protected void Backto_Managecustomer(object sender, EventArgs e)
        {
            try
            {
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor))
                    Response.Redirect(Constants.RedirectManageCustomers, true);
                else if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                    Response.Redirect(Constants.RedirectManageCustomersWithAdvisorID + Session[Constants.TempAdvisorID].ToString(), false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Used to set key and steps values to display on PDF 
        /// </summary>
        /// <param name="strGridName">Grid Name</param>
        public void SetKeyAndStepsValuesForPDF(string strGridName)
        {
            try
            {
                if (strGridName.Equals(Constants.GridAsap))
                {
                    CommonVariables.strKey1 = lblKey5.Text;
                    CommonVariables.strKey2 = lblKey6.Text;
                    CommonVariables.strKey3 = lblKey9.Text;
                    CommonVariables.strStep1 = lblSteps5.Text;
                    CommonVariables.strStep2 = lblSteps6.Text;
                    CommonVariables.strStep3 = lblSteps9.Text;

                    CommonVariables.strNoteWife1 = lblWifeNote1.Text;
                    CommonVariables.strNoteWife9 = lblWifeNote9.Text;
                    CommonVariables.strNoteWife5 = lblWifeNote5.Text;

                }
                else if (strGridName.Equals(Constants.GridClaim67))
                {
                    CommonVariables.strKey4 = lblKey7.Text;
                    CommonVariables.strKey5 = lblKey8.Text;
                    CommonVariables.strKey6 = lblKey12.Text;
                    CommonVariables.strStep4 = lblSteps7.Text;
                    CommonVariables.strStep5 = lblSteps8.Text;
                    CommonVariables.strStep6 = lblSteps12.Text;

                    CommonVariables.strNoteWife4 = lblWifeNote4.Text;
                    CommonVariables.strNoteWife8 = lblWifeNote8.Text;
                }
                else if (strGridName.Equals(Constants.GridSpouse))
                {
                    CommonVariables.strKey7 = lblKey3.Text;
                    CommonVariables.strKey8 = lblKey4.Text;
                    CommonVariables.strKey9 = lblKey10.Text;
                    CommonVariables.strStep7 = lblSteps3.Text;
                    CommonVariables.strStep8 = lblSteps4.Text;
                    CommonVariables.strStep9 = lblSteps10.Text;

                    CommonVariables.strNoteWife2 = lblWifeNote2.Text;
                    CommonVariables.strNoteWife6 = lblWifeNote6.Text;
                    CommonVariables.strNoteWife10 = lblWifeNote10.Text;
                }
                else if (strGridName.Equals(Constants.GridLatest))
                {
                    CommonVariables.strKey10 = lblKey1.Text;
                    CommonVariables.strKey11 = lblKey2.Text;
                    CommonVariables.strKey12 = lblKey11.Text;
                    CommonVariables.strStep10 = lblSteps1.Text;
                    CommonVariables.strStep11 = lblSteps2.Text;
                    CommonVariables.strStep12 = lblSteps11.Text;

                    CommonVariables.strNoteWife3 = lblWifeNote3.Text;
                    CommonVariables.strNoteWife7 = lblWifeNote7.Text;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Used to strore Paid to Wait Amount and Annual Income values
        /// </summary>
        private void AddPaidAndAnnualValForPDF()
        {
            try
            {
                //Add Annual income values to show in PDF
                Calc.listAnnualAmount.Clear();
                if (!string.IsNullOrEmpty(lblCombinedIncome1.Text))
                    Calc.listAnnualAmount.Add(lblCombinedIncome1.Text);
                if (!string.IsNullOrEmpty(lblCombinedIncome2.Text))
                    Calc.listAnnualAmount.Add(lblCombinedIncome2.Text);
                if (!string.IsNullOrEmpty(lblCombinedIncome3.Text))
                    Calc.listAnnualAmount.Add(lblCombinedIncome3.Text);
                //Calc.listAnnualAmount.Add(lblCombinedIncome2.Text);

                //Add Paid to Wait values to show in PDF
                Calc.listPaidToWaitAmount.Clear();
                if (!string.IsNullOrEmpty(lblPaidNumber1.Text) && hasAmtFormatValue1)
                    Calc.listPaidToWaitAmount.Add(lblPaidNumber1.Text);
                if (!string.IsNullOrEmpty(lblPaidNumber2.Text) && hasAmtFormatValue2)
                    Calc.listPaidToWaitAmount.Add(lblPaidNumber2.Text);
                if (!string.IsNullOrEmpty(lblPaidNumber3.Text) && hasAmtFormatValue3)
                    Calc.listPaidToWaitAmount.Add(lblPaidNumber3.Text);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Export Grid View Content into PDF
        /// </summary>
        /// <param name="gridView"></param>
        public void exportAllToPdf()
        {
            try
            {
                #region Setting the PDF variables
                Calc Calc = new Calc();
                string strBene = string.Empty, strBeneSpouse = string.Empty;
                decimal decBenefit, decBenefitSpouse;
                /* Get Customer Details for showing up in PDF  */
                DataTable dtCustomerDetails;
                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor) || Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                {
                    dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                    strBene = dtCustomerDetails.Rows[0][Constants.SecurityInfoMarriedHusbandsRetAgeBenefit].ToString();
                    strBeneSpouse = dtCustomerDetails.Rows[0][Constants.SecurityInfoMarriedWifesRetAgeBenefit].ToString();
                }
                else
                {
                    dtCustomerDetails = customer.getCustomerFinancialDetailsByStatus(Session[Constants.SessionUserId].ToString(), Constants.Widowed);
                    strBene = dtCustomerDetails.Rows[0][Constants.SecurityInfoRetAgeBenefit].ToString();
                    strBeneSpouse = dtCustomerDetails.Rows[0][Constants.SecurityInfoWidowedSpousesRetAgeBenefit].ToString();
                }
                string custFirstName = dtCustomerDetails.Rows[0][Constants.CustomerFirstName].ToString();
                decimal.TryParse(strBene, out decBenefit);
                decimal.TryParse(strBeneSpouse, out decBenefitSpouse);
                SiteNew.renderGridViewIntoStackedChart(GridAsap, ChartAsap, LabelAsap, Constants.Widowed, false);
                /* Create the PDF Document */
                MemoryStream objMemory = new MemoryStream();
                Document pdfDoc = new Document(PageSize.A4, Constants.zero, Constants.zero, -15f, 55f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, objMemory);
                /* Open the stream */
                pdfDoc.Open();
                writer.PageEvent = new PDFEvent();
                // First PDF Page
                SiteNew.PdfFrontPage(dtCustomerDetails, ref pdfDoc);

                string sWifeFRA = customer.WidowFRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                string strOwnBenefit = decBenefit.ToString(Constants.AMT_FORMAT);
                string strSpousalbenefit = decBenefitSpouse.ToString(Constants.AMT_FORMAT);
                string strEmail = string.Empty;
                #endregion Setting the PDF variables

                #region First Page of the PDF
                pdfDoc.NewPage();
                // Second PDF Page
                //get a table for 3 colums
                PdfPTable table = new PdfPTable(3);
                //set the column width
                float[] widths = new float[] { 1f, 1f, 1f };
                //set the table width
                table.TotalWidth = 550f;
                //fix the table width
                table.LockedWidth = true;
                //adding space in the table
                table.DefaultCell.Padding = 3f;
                table.SetWidths(widths);
                //remove the table borders
                table.DefaultCell.Border = PdfPCell.NO_BORDER;
                //set the font for the text in the table cells
                BaseFont customfont = BaseFont.CreateFont(HttpContext.Current.Server.MapPath("~") + Constants.PdfRobotoFontStyle, BaseFont.CP1252, BaseFont.EMBEDDED);
                iTextSharp.text.Font font = new iTextSharp.text.Font(customfont, 16);
                //add 1 row to the table as customer name
                table.AddCell(new Phrase(Constants.customerName, font));
                table.AddCell(new Phrase(dtCustomerDetails.Rows[0][Constants.CustomerFirstName].ToString(), font));
                table.AddCell(string.Empty);
                //add 2 row to the table as date of births
                table.AddCell(new Phrase(Constants.customerDob, font));
                table.AddCell(new Phrase(string.Format(Constants.stringDateFormat, Convert.ToDateTime(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString())), font));
                table.AddCell(string.Empty);
                //add 3 row to the table as full retirement ages
                table.AddCell(new Phrase(Constants.customerFullRetirementAge, font));
                table.AddCell(new Phrase(sWifeFRA, font));
                table.AddCell(string.Empty);
                //add 4 row to the table as PIA's
                table.AddCell(new Phrase(Constants.customerPIA, font));
                table.AddCell(new Phrase(strOwnBenefit, font));
                table.AddCell(new Phrase(strSpousalbenefit, font));
                //add 5 row to the table as maritial status
                table.AddCell(new Phrase(Constants.PdfCustomerMaritalStatus, font));
                table.AddCell(new Phrase(dtCustomerDetails.Rows[0][Constants.CustomerMaritalStatus].ToString(), font));
                table.AddCell(string.Empty);
                //add 6 row to the table as COLA percentage
                table.AddCell(new Phrase(Constants.customerIncludeCOLA, font));
                table.AddCell(new Phrase(Constants.COLAPercentage, font));
                table.AddCell(string.Empty);
                //add table to the document
                pdfDoc.Add(table);
                //pdfDoc.Add(SiteNew.generateParagraph(string.Format(Constants.customerName + dtCustomerDetails.Rows[0][Constants.CustomerFirstName].ToString() + "               " + dtCustomerDetails.Rows[0][Constants.CustomerSpouseFirstName].ToString()), true, false, false));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.customerDob + string.Format(Constants.stringDateFormat, Convert.ToDateTime(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString())), true, false, false));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.customerFullRetirementAge + string.Format(sWifeFRA), true, false, false));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.customerPIA + Benefit.ToString(Constants.AMT_FORMAT) + "             " + BenefitSpouse.ToString(Constants.AMT_FORMAT), true, false, false));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.PdfCustomerMaritalStatus + dtCustomerDetails.Rows[0][Constants.CustomerMaritalStatus].ToString(), true, false, false));
                //if (dtCustomerDetails.Rows[0][Constants.SecurityInfoCola].ToString() == "T")
                //    pdfDoc.Add(SiteNew.generateParagraph(Constants.customerIncludeCOLA, true, false, false));
                //else
                //    pdfDoc.Add(SiteNew.generateParagraph(Constants.customerIncludeCOLA, true, false, false));
                pdfDoc.Add(SiteNew.generateImage(Constants.pdfHorizontalLine, Constants.pdfImageWidth, 1f));
                pdfDoc.Add(new Paragraph(" "));
                #endregion First Page of the PDF

                #region Deciding the best plan and building and rest
                var list = new List<KeyValuePair<string, int>>();
                string sFirstGrid = string.Empty;
                string sSecondGrid = string.Empty;
                string sThirdGrid = string.Empty;
                if (Session["HusbandBenefits"].Equals(Session["WifeBenefits"]))
                {
                    list.Add(new KeyValuePair<string, int>(Constants.GridLatest, int.Parse(Session[Constants.WidowLatestCumm].ToString())));
                    list.Add(new KeyValuePair<string, int>(Constants.GridSpouse, int.Parse(Session[Constants.WidowSpouseCumm].ToString())));
                }
                else if (Convert.ToDecimal(Session[Constants.surviourValueAtAge66]) > Convert.ToDecimal(Session[Constants.workingValueAtAge66]) && Convert.ToDecimal(Session[Constants.surviourValueAtAge70]) > Convert.ToDecimal(Session[Constants.workingValueAtAge70]))
                    list.Add(new KeyValuePair<string, int>(Constants.GridAsap, int.Parse(Session[Constants.WidowAasapCumm].ToString())));
                else if (Convert.ToDecimal(Session[Constants.workingValueAtAge70]) > Convert.ToDecimal(Session[Constants.surviourValueAtAge70]))
                {
                    list.Add(new KeyValuePair<string, int>(Constants.GridLatest, int.Parse(Session[Constants.WidowLatestCumm].ToString())));
                    if (Globals.Instance.intWifeAge < Globals.Instance.intWifeFRAAge)
                        list.Add(new KeyValuePair<string, int>(Constants.GridClaim67, int.Parse(Session[Constants.WidowClaim67Cumm].ToString())));
                    if (Convert.ToDecimal(Session[Constants.workingValueAtAge66]) > Convert.ToDecimal(Session[Constants.surviourValueAtAge66]))
                        list.Add(new KeyValuePair<string, int>(Constants.GridSpouse, int.Parse(Session[Constants.WidowSpouseCumm].ToString())));
                    else if ((Globals.Instance.intWifeAge < Globals.Instance.intWifeFRAAge) && (Convert.ToDecimal(Session[Constants.workingValueAtAge70]) > Convert.ToDecimal(Session[Constants.surviourValueAtAge70])))
                        list.Add(new KeyValuePair<string, int>(Constants.GridAsap, int.Parse(Session[Constants.WidowAasapCumm].ToString())));
                }
                else
                {
                    list.Add(new KeyValuePair<string, int>(Constants.GridLatest, int.Parse(Session[Constants.WidowLatestCumm].ToString())));
                    if (Convert.ToDecimal(Session[Constants.workingValueAtAge66]) > Convert.ToDecimal(Session[Constants.surviourValueAtAge66]))
                        list.Add(new KeyValuePair<string, int>(Constants.GridSpouse, int.Parse(Session[Constants.WidowSpouseCumm].ToString())));
                    else
                        list.Add(new KeyValuePair<string, int>(Constants.GridAsap, int.Parse(Session[Constants.WidowAasapCumm].ToString())));
                }
                if (list.Count == 3)
                {
                    sFirstGrid = list[0].Key.ToString();
                    sSecondGrid = list[1].Key.ToString();
                    sThirdGrid = list[2].Key.ToString();
                }
                else if (list.Count == 2)
                {
                    sFirstGrid = list[0].Key.ToString();
                    sSecondGrid = list[1].Key.ToString();
                }
                else
                {
                    sFirstGrid = list[0].Key.ToString();
                }
                GridView gFirstGrid = new GridView();
                GridView gSecondGrid = new GridView();
                GridView gThirdGrid = new GridView();
                Chart cFirstChart = new Chart();
                Chart cSecondChart = new Chart();
                Chart cThirdChart = new Chart();
                Label lFirstLable = new Label();
                Label lSecondLabel = new Label();
                Label lThirdLabel = new Label();
                if (sFirstGrid.Equals(Constants.GridAsap))
                {
                    gFirstGrid = GridAsap;
                    cFirstChart = ChartAsap;
                    lFirstLable.Text = Constants.Strategy1;
                }
                if (sFirstGrid.Equals(Constants.GridLatest))
                {
                    gFirstGrid = GridLatest;
                    cFirstChart = ChartLatest;
                    lFirstLable.Text = Constants.Strategy1;
                }
                if (sFirstGrid.Equals(Constants.GridSpouse))
                {
                    gFirstGrid = GridSpouse;
                    cFirstChart = ChartSpouse;
                    lFirstLable.Text = Constants.Strategy1;
                }
                if (sFirstGrid.Equals(Constants.GridClaim67))
                {
                    gFirstGrid = GridClaim67;
                    cFirstChart = ChartClaim67;
                    lFirstLable.Text = Constants.Strategy1;
                }
                if (sSecondGrid.Equals(Constants.GridAsap))
                {
                    gSecondGrid = GridAsap;
                    cSecondChart = ChartAsap;
                    lSecondLabel.Text = Constants.Strategy2;
                }
                if (sSecondGrid.Equals(Constants.GridLatest))
                {
                    gSecondGrid = GridLatest;
                    cSecondChart = ChartLatest;
                    lSecondLabel.Text = Constants.Strategy2;
                }
                if (sSecondGrid.Equals(Constants.GridSpouse))
                {
                    gSecondGrid = GridSpouse;
                    cSecondChart = ChartSpouse;
                    lSecondLabel.Text = Constants.Strategy2;
                }
                if (sSecondGrid.Equals(Constants.GridClaim67))
                {
                    gSecondGrid = GridClaim67;
                    cSecondChart = ChartClaim67;
                    lSecondLabel.Text = Constants.Strategy2;
                }
                if (sThirdGrid.Equals(Constants.GridClaim67))
                {
                    gThirdGrid = GridClaim67;
                    cThirdChart = ChartClaim67;
                    lThirdLabel.Text = Constants.Strategy3;
                }
                if (sThirdGrid.Equals(Constants.GridLatest))
                {
                    gThirdGrid = GridLatest;
                    cThirdChart = ChartLatest;
                    lThirdLabel.Text = Constants.Strategy3;
                }
                if (sThirdGrid.Equals(Constants.GridSpouse))
                {
                    gThirdGrid = GridSpouse;
                    cThirdChart = ChartSpouse;
                    lThirdLabel.Text = Constants.Strategy3;
                }
                if (sThirdGrid.Equals(Constants.GridAsap))
                {
                    gThirdGrid = GridAsap;
                    cThirdChart = ChartAsap;
                    lThirdLabel.Text = Constants.Strategy3;
                }
                //Add Strategy Details Table in PDF 
                if (list.Count == 1)
                {
                    SiteNew.CreateStrategyDetailsTableForPDF(ref pdfDoc,
                                                                Constants.PaidtoWaitTextForPDF1,
                                                                Calc.listPaidToWaitAmount,
                                                                Constants.AnnualIncomeStratComplete1,
                                                                Calc.listAnnualAmount,
                                                                list.Count,
                                                                Constants.Widowed);
                }
                else
                {
                    SiteNew.CreateStrategyDetailsTableForPDF(ref pdfDoc,
                                                    Constants.PaidtoWaitTextForPDF,
                                                    Calc.listPaidToWaitAmount,
                                                    Constants.AnnualIncomeStratComplete,
                                                    Calc.listAnnualAmount,
                                                    list.Count,
                                                    Constants.Widowed);
                }

                //Add explanation of strategy in report while sending it to client by email
                AddInstructionBeforeSeeStrategy(ref pdfDoc);
                AddExplainationToPDF(ref pdfDoc);
                pdfDoc.NewPage();

                if (list.Count == 3)
                {
                    commonFunctions(gFirstGrid, cFirstChart, lFirstLable, sFirstGrid, custFirstName, ref pdfDoc, Constants.one);
                    commonFunctions(gSecondGrid, cSecondChart, lSecondLabel, sSecondGrid, custFirstName, ref pdfDoc, Constants.two);
                    commonFunctions(gThirdGrid, cThirdChart, lThirdLabel, sThirdGrid, custFirstName, ref pdfDoc, Constants.three);
                }
                else if (list.Count == 2)
                {
                    commonFunctions(gFirstGrid, cFirstChart, lFirstLable, sFirstGrid, custFirstName, ref pdfDoc, Constants.one);
                    commonFunctions(gSecondGrid, cSecondChart, lSecondLabel, sSecondGrid, custFirstName, ref pdfDoc, Constants.two);
                }
                else
                {
                    commonFunctions(gFirstGrid, cFirstChart, lFirstLable, sFirstGrid, custFirstName, ref pdfDoc, Constants.one);
                }
                #endregion Deciding the best plan and building and rest

                //Add discalimer at the end of report
                SiteNew.AddDisclaimerIntoPDFReport(ref pdfDoc);

                #region Email Report to Client
                if (Globals.Instance.BoolEmailReport)
                {
                    strEmail = dtCustomerDetails.Rows[0][Constants.UserEmail].ToString();
                    if (!String.IsNullOrEmpty(Globals.Instance.strAdvisorEmail))
                    {
                        if (customer.MailReport(pdfDoc, Globals.Instance.strAdvisorEmail, objMemory, writer, custFirstName))
                            Globals.Instance.BoolIsMailSent = true;
                        Globals.Instance.strAdvisorEmail = string.Empty;
                    }
                    else
                    {
                        if (customer.MailReport(pdfDoc, strEmail, objMemory, writer, custFirstName))
                            Globals.Instance.BoolIsMailSent = true;
                    }
                }
                #endregion Email Report to Client

                #region code to close the PDF and set the MIME type
                else
                {
                    /* Close the stream */
                    pdfDoc.Close();
                    /* Create object for Customer and Pdf File */
                    PdfFilesTO objPdfFile = new PdfFilesTO();
                    /* Assign data into pdf file variable */
                    objPdfFile.PdfContent = objMemory.GetBuffer();
                    objPdfFile.PdfName = Constants.pdfFileName + dtCustomerDetails.Rows[0][Constants.CustomerLastName].ToString() + "_" + DateTime.Now.ToString(Constants.DateFormat) + Constants.pdfFileExtension;
                    objPdfFile.CustomerID = Session[Constants.SessionUserId].ToString();
                    objPdfFile.ContentType = Constants.pdfContentType;
                    customer.insertPdfDetails(objPdfFile);
                    //Used to open report in new tab
                    //if (Session["AdvisorCustomer"] != null)
                    //{
                    //    ClsReportDataProperties.objMemoryStream = objMemory;
                    //    ClsReportDataProperties.pdfDocument = pdfDoc;
                    //    ClsReportDataProperties.strUserName = dtCustomerDetails.Rows[0][Constants.CustomerLastName].ToString();
                    //    Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('ViewReport.aspx','_newtab');", true);
                    //}
                    //else
                    //{
                    Response.OutputStream.Write(objMemory.GetBuffer(), 0, objMemory.GetBuffer().Length);
                    //Response Setting 
                    Response.ContentType = Constants.pdfContentType;
                    //Response.AddHeader(Constants.pdfContentDisposition, Constants.pdfFileName + dtCustomerDetails.Rows[0][Constants.CustomerLastName].ToString() + "_" + DateTime.Now.ToString(Constants.DateFormat) + Constants.pdfFileExtension);
                    Response.AddHeader(Constants.pdfContentDisposition, Constants.pdfFileName + dtCustomerDetails.Rows[0][Constants.CustomerLastName].ToString() + "_" + DateTime.Now.ToString(Constants.DateFormat) + Constants.pdfFileExtension);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Write(pdfDoc);
                    Response.End();
                    //}
                }
                #endregion code to close the PDF and set the MIME type

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code for the common functions to build the PDF for alll strategies
        /// </summary>
        /// <param name="gFirstGrid"></param>
        /// <param name="cFirstChart"></param>
        /// <param name="lFirstLable"></param>
        /// <param name="sGridName"></param>
        /// <param name="custFirstName"></param>
        /// <param name="pdfDoc"></param>
        /// <param name="number"></param>
        public void commonFunctions(GridView gFirstGrid, Chart cFirstChart, Label lFirstLable, string sGridName, string custFirstName, ref Document pdfDoc, string number)
        {
            try
            {
                Customers customer = new Customers();
                string WifeFra = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                string HusbFRA = customer.WidowFRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                if (!number.Equals(Constants.one))
                {
                    pdfDoc.NewPage();
                }
                SiteNew.renderGridViewIntoStackedChart(gFirstGrid, cFirstChart, lFirstLable, Constants.Widowed, false);
                SiteNew.commonPageLayout(lFirstLable, gFirstGrid, ref pdfDoc, custFirstName, Constants.Widowed, sGridName, WifeFra, HusbFRA);
                /* Add new page in pdf */
                pdfDoc.NewPage();
                /* Print chart into image in pdf */
                // pdfDoc.Add(SiteNew.generateChartHeading(lFirstLable.Text));
                SiteNew.convertChartToImage(cFirstChart, ref pdfDoc);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Back to Parameters
        /// </summary>
        protected void BackToParms()
        {
            try
            {
                PanelGrids.Visible = false;
                PanelEntry.Visible = true;
                PanelParms.Visible = false;
                BirthWifeMM.Focus();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to add steps adn keys after the grid on website for GridLatest
        /// </summary>
        /// <param name="cumm"></param>
        /// <param name="startingValue"></param>
        /// <param name="valueAT70"></param>
        /// <param name="ageWife"></param>
        /// <param name="strUserFirstName"></param>
        /// <param name="ageAtLastChange"></param>
        public void renderDataIntoGridStartegy1(decimal cumm, decimal startingValue, decimal valueAT70, string ageWife, string strUserFirstName, string ageAtLastChange, decimal AnualIncome)
        {
            try
            {
                Customers Customer = new Customers();
                string WifraAtFra = Convert.ToString(Customer.WidowFRA(Convert.ToInt32(birthWifeTempYYYY.Text))).Substring(0, 2);
                string ageWifeStarting = ageWife;
                string strStandardTextForWidow = string.Empty;
                if (int.Parse(ageWife) < int.Parse(WifraAtFra.Substring(0, 2)))
                    strStandardTextForWidow = Constants.WidowSurviourStep1 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.WidowSurviourStep2 + strUserFirstName + Constants.WidowSurviourStep3;

                if (Globals.Instance.WifeNumberofMonthsBelowGrid != 0)
                {
                    lblWifeNote3.Text = "# " + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    WifraAtFra = Globals.Instance.WifeNumberofYears + " years and " + Globals.Instance.WifeNumberofMonthsAboveGrid + " months ";
                }
                if (Globals.Instance.WifeNumberofMonthinNote != 0)
                {
                    lblWifeNote7.Text = "# " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserFirstName;
                    ageWifeStarting = Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + " years and " + Globals.Instance.WifeNumberofMonthsinSteps + " months ";
                }
                //check cumulative value for 0
                if (cumm != 0)
                {
                    //show key 1 for cumulative value
                    if (Globals.Instance.WidowChangeAt70)
                        lblKey1.Text = string.Format(cumm.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + strUserFirstName + Constants.keyFigures7 + Constants.CummuText1s + strUserFirstName + Constants.CummuText2sWork + valueAT70.ToString(Constants.AMT_FORMAT) + Constants.CummuText3 + strUserFirstName + Constants.CummuText4s + cumm.ToString(Constants.AMT_FORMAT) + Constants.CummuText5s);
                    else
                        lblKey1.Text = string.Format(cumm.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + strUserFirstName + Constants.keyFigures7 + Constants.CummuText1s + strUserFirstName + Constants.CummuText2sWidowSurvivor + valueAT70.ToString(Constants.AMT_FORMAT) + Constants.CummuText3 + strUserFirstName + Constants.CummuText4s + cumm.ToString(Constants.AMT_FORMAT) + Constants.CummuText5s);
                    lblKey1.Visible = true;
                }
                else
                {
                    //hide key 1
                    lblKey1.Visible = false;
                }
                //check last change age for age 70
                if (ageAtLastChange.Equals(Constants.Number70) && valueAT70 != 0)
                {
                    //if last change is at age 70
                    lblKey2.Text = string.Format(valueAT70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps25 + strUserFirstName + Constants.WorkBenefitsKey);
                }
                else
                {
                    //if last change is not at age 70
                    if (valueAT70 != 0)
                        lblKey2.Text = string.Format(valueAT70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps25 + strUserFirstName + Constants.keyFigures16);
                }
                //chow step 1 for starting value
                if (startingValue != 0)
                {
                    if (int.Parse(ageWife) < int.Parse(WifraAtFra.Substring(0, 2)))
                        lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.WidowReducedWHB + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strStandardTextForWidow);
                    else if (int.Parse(ageWife) == int.Parse(WifraAtFra.Substring(0, 2)))
                        lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + Constants.FRAText + Constants.steps24 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strStandardTextForWidow);
                    else
                        lblSteps1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + Constants.steps24 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strStandardTextForWidow);
                }
                //check age 70 value for 0 in step 2
                if (valueAT70 != 0 && ageAtLastChange.Equals(Constants.Number70))
                {
                    //check lat change age is at age 70
                    if (ageAtLastChange.Equals(Constants.Number70))
                    {
                        //if last change is at age 70
                        lblSteps2.Text = string.Format(Constants.Line2 + Constants.Age + "  " + ageAtLastChange + Constants.when + strUserFirstName + Constants.steps8 + ageAtLastChange + ", " + strUserFirstName + Constants.steps18 + valueAT70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                    }
                    else
                    {
                        //if last change is not at age 70
                        lblSteps2.Text = string.Format(Constants.Line2 + Constants.Age + "  " + ageAtLastChange + Constants.when + strUserFirstName + Constants.steps8 + WifraAtFra + Constants.FRAText + ", " + strUserFirstName + Constants.steps22 + valueAT70.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    }
                    lblSteps2.Visible = true;
                    lblSteps11.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageAtLastChange + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                }
                else
                {
                    //hide step 2
                    lblSteps2.Visible = false;
                    lblSteps11.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageAtLastChange + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                }
                lblKey11.Text = string.Format(AnualIncome.ToString(Constants.AMT_FORMAT) + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey);

                //Set values for Keys and steps to display on pdf
                SetKeyAndStepsValuesForPDF(Constants.GridLatest);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to add steps adn keys after the grid on website for GridSpouse
        /// </summary>
        /// <param name="cumm"></param>
        /// <param name="startingValue"></param>
        /// <param name="valueAT70"></param>
        /// <param name="ageWife"></param>
        /// <param name="strUserFirstName"></param>
        /// <param name="ageAtLastChange"></param>
        public void renderDataIntoGridStartegy2(decimal cumm, decimal startingValue, decimal valueAT70, string ageWife, string strUserFirstName, string ageAtLastChange, bool BoolAboveFRAageStep2, decimal AnualIncome)
        {
            try
            {
                Customers Customer = new Customers();
                string WifraAtFra = Convert.ToString(Customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text)));
                WifraAtFra = WifraAtFra.Substring(0, 2);
                string ageWifeStarting = ageWife;
                string strStandardTextForWidow = string.Empty;
                if (int.Parse(ageWife) < int.Parse(WifraAtFra.Substring(0, 2)))
                    strStandardTextForWidow = Constants.WidowSurviourStep1 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.WidowSurviourStep2 + strUserFirstName + Constants.WidowSurviourStep3;
                if (Globals.Instance.WifeNumberofMonthsBelowGrid != 0)
                {
                    lblWifeNote2.Text = "* " + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblWifeNote6.Text = "** " + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    WifraAtFra = Globals.Instance.WifeNumberofYears + " years and " + Globals.Instance.WifeNumberofMonthsAboveGrid + " months ";
                }
                if (Globals.Instance.WifeNumberofMonthinNote != 0)
                {
                    lblWifeNote10.Text = "# " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserFirstName;
                    ageWifeStarting = Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + " years and " + Globals.Instance.WifeNumberofMonthsinSteps + " months ";
                }
                //check for cumulative value equals to 0
                if (cumm != 0)
                {
                    //show key 2 for cumulative income
                    lblKey3.Text = string.Format(cumm.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + strUserFirstName + Constants.keyFigures7 + Constants.CummuText1s + strUserFirstName + Constants.CummuText2Work + valueAT70.ToString(Constants.AMT_FORMAT) + Constants.CummuText3s + ageAtLastChange + ", " + strUserFirstName + Constants.CummuText4s + cumm.ToString(Constants.AMT_FORMAT) + Constants.CummuText5s);
                    lblKey3.Visible = true;
                }
                else
                {
                    //hide key 1
                    lblKey3.Visible = false;
                }
                if (ageAtLastChange.Equals(Constants.Number70))
                    lblKey4.Text = string.Format(valueAT70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps25 + strUserFirstName + Constants.WorkBenefitsKey);
                else
                    lblKey4.Text = string.Format(valueAT70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps25 + strUserFirstName + Constants.WHBenefitsKey);
                //show step 1 for starting value
                if (startingValue != 0)
                {
                    if (int.Parse(ageWife) < int.Parse(WifraAtFra.Substring(0, 2)))
                        lblSteps3.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.WidowReducedWHB + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strStandardTextForWidow);
                    else if (int.Parse(ageWife) == int.Parse(WifraAtFra.Substring(0, 2)))
                        lblSteps3.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + Constants.FRAText + Constants.steps24 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strStandardTextForWidow);
                    else if (Globals.Instance.strWidowStarting.Equals(Constants.Working))
                        lblSteps3.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strStandardTextForWidow);
                    else
                        lblSteps3.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + Constants.steps24 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strStandardTextForWidow);
                }
                //check age 70 value for 0 for step 2
                if (BoolAboveFRAageStep2)
                {
                    if (int.Parse(ageAtLastChange) == int.Parse(WifraAtFra.Substring(0, 2)))
                        lblSteps4.Text = string.Format(Constants.Line2 + Constants.Age + "  " + ageAtLastChange + Constants.when + strUserFirstName + Constants.steps8 + WifraAtFra + Constants.FRAText + ", " + strUserFirstName + Constants.steps22 + valueAT70.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    else
                        lblSteps4.Text = string.Format(Constants.Line2 + Constants.Age + "  " + ageAtLastChange + Constants.when + strUserFirstName + Constants.steps8 + WifraAtFra + ", " + strUserFirstName + Constants.steps22 + valueAT70.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    lblSteps4.Visible = true;
                    lblSteps10.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageAtLastChange + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                }
                else
                {
                    //hide step 2
                    lblSteps4.Visible = false;
                    lblSteps10.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageAtLastChange + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                }
                lblKey10.Text = string.Format(AnualIncome.ToString(Constants.AMT_FORMAT) + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey);

                //Set values for Keys and steps to dispay on pdf
                SetKeyAndStepsValuesForPDF(Constants.GridSpouse);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to add steps adn keys after the grid on website for GridAsap
        /// </summary>
        /// <param name="cumm"></param>
        /// <param name="startingValue"></param>
        /// <param name="valueAT70"></param>
        /// <param name="ageWife"></param>
        /// <param name="strUserFirstName"></param>
        /// <param name="ageAtLastChange"></param>
        public void renderDataIntoGridStartegy3(decimal cumm, decimal startingValue, decimal valueAT70, string ageWife, string strUserFirstName, string ageAtLastChange, bool BoolAboveFRAageStep2, decimal AnualIncome)
        {
            try
            {
                Calc calc = new Calc();
                Customers Customer = new Customers();
                string WifraAtFra = Convert.ToString(Customer.WidowFRA(Convert.ToInt32(birthWifeTempYYYY.Text)));
                int wifeFRAage = Convert.ToInt32(WifraAtFra.Substring(0, 2));
                string ageWifeStarting = ageWife;
                string strTempFRA = customer.WidowFRA(birthYearWife);
                int intTempFRA = Convert.ToInt32(strTempFRA.Substring(0, 2));
                int intWifeFRA = Convert.ToInt32(calc.WidowFRA(birthYearWife));
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    if (WifraAtFra.Contains("Months") && intTempFRA < intWifeFRA)
                    {
                        lblWifeNote1.Text = "* " + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.strNote1 + strUserFirstName;
                        lblWifeNote9.Text = "** " + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                        //WifraAtFra = Globals.Instance.WifeNumberofYears + " years and " + Globals.Instance.WifeNumberofMonthsAboveGrid + " months ";
                    }
                    else
                    {
                        lblWifeNote1.Text = "* " + (12 + Globals.Instance.WifeNumberofMonthsAboveGrid) + Constants.strNote1 + strUserFirstName;
                        lblWifeNote9.Text = "** " + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                        WifraAtFra = Globals.Instance.WifeNumberofYears + " years and " + Globals.Instance.WifeNumberofMonthsAboveGrid + " months ";
                    }
                }
                else if (Globals.Instance.WifeNumberofMonthsBelowGrid != 0)
                {
                    lblWifeNote1.Text = "# " + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    WifraAtFra = Globals.Instance.WifeNumberofYears + " years and " + Globals.Instance.WifeNumberofMonthsAboveGrid + " months ";
                }
                if (Globals.Instance.WifeNumberofMonthinNote != 0)
                {
                    lblWifeNote5.Text = "# " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserFirstName;
                    ageWifeStarting = Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + " years and " + Globals.Instance.WifeNumberofMonthsinSteps + " months ";
                }
                //check the cumulative income for 0
                if (cumm != 0)
                {
                    //show key 1 for cumulative income
                    lblKey5.Text = string.Format(cumm.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + strUserFirstName + Constants.keyFigures7 + Constants.CummuText1s + strUserFirstName + Constants.CummuText2sWidowSurvivor + valueAT70.ToString(Constants.AMT_FORMAT) + Constants.CummuText3s + ageAtLastChange + ", " + strUserFirstName + Constants.CummuText4s + cumm.ToString(Constants.AMT_FORMAT) + Constants.CummuText5s);
                    lblKey5.Visible = true;
                }
                else
                {
                    //hide key 1
                    lblKey5.Visible = false;
                }
                //check the last change for age 70
                //if last change if not at age 70
                if (valueAT70 != 0)
                    lblKey6.Text = string.Format(valueAT70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps25 + strUserFirstName + Constants.SurvivorBenefitsKey);
                else
                    lblKey6.Text = string.Empty;
                //show step 1 for starting the value
                if (startingValue != 0)
                {
                    //Added on 08/08/2016
                    if (Globals.Instance.strWidowStarting.Equals(Constants.spousal))
                        lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + Constants.steps24 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    else if (int.Parse(WifraAtFra.Substring(0, 2)) <= Convert.ToInt32(ageWife))
                        lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.steps9 + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    else
                        lblSteps5.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWifeStarting + ", " + strUserFirstName + Constants.ReducedWorkBenefit + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                }
                else
                    lblSteps5.Text = string.Empty;
                //check for age 70 value for 0 and show step 2
                if (BoolAboveFRAageStep2)
                {
                    //check if last chnage is not on age 70
                    lblSteps6.Text = string.Format(Constants.Line2 + Constants.Age + "  " + ageAtLastChange + Constants.when + strUserFirstName + Constants.steps8 + WifraAtFra + Constants.FRATextSurviour + " " + strUserFirstName + Constants.SurvoirBenefitSwitch + valueAT70.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    lblSteps6.Visible = true;
                    lblSteps9.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageAtLastChange + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                }
                else
                {
                    //hide step 2
                    lblSteps6.Visible = false;
                    lblSteps9.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageAtLastChange + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                }
                lblKey9.Text = string.Format(AnualIncome.ToString(Constants.AMT_FORMAT) + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey);

                //Set values for Keys and steps to dispay on pdf
                SetKeyAndStepsValuesForPDF(Constants.GridAsap);

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to show steps and keys for grid GridClaim67
        /// </summary>
        /// <param name="cumm"></param>
        /// <param name="startingValue"></param>
        /// <param name="valueAT70"></param>
        /// <param name="ageWife"></param>
        /// <param name="strUserFirstName"></param>
        /// <param name="ageAtLastChange"></param>
        public void renderDataIntoGridStartegy4(decimal cumm, decimal startingValue, decimal valueAT70, string ageWife, string strUserFirstName, string ageAtLastChange, decimal AnualIncome)
        {
            try
            {
                Customers customer = new Customers();
                string WifeFRA = customer.WidowFRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                string ageWifeStarting = ageWife;
                if (Globals.Instance.WifeNumberofMonthsBelowGrid != 0)
                {
                    lblWifeNote4.Text = "# " + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    WifeFRA = Globals.Instance.WifeNumberofYears + " years and " + Globals.Instance.WifeNumberofMonthsAboveGrid + " months ";
                }
                if (Globals.Instance.WifeNumberofMonthinNote != 0)
                {
                    lblWifeNote8.Text = "# " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserFirstName;
                    ageWifeStarting = Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + " years and " + Globals.Instance.WifeNumberofMonthsinSteps + " months ";
                }
                //check for cumulative value at age 69 for 0
                if (cumm != 0)
                {
                    //show key 1 for cumulative value
                    lblKey7.Text = string.Format(cumm.ToString(Constants.AMT_FORMAT) + Constants.keyFigures15 + Constants.steps25 + Constants.keyFigures6 + strUserFirstName + Constants.keyFigures7 + Constants.CummuText1s + strUserFirstName + Constants.CummuText2sWork + valueAT70.ToString(Constants.AMT_FORMAT) + Constants.CummuText3 + strUserFirstName + Constants.CummuText4s + cumm.ToString(Constants.AMT_FORMAT) + Constants.CummuText5s);
                    lblKey7.Visible = true;
                }
                else
                {
                    //hide key 1
                    lblKey7.Visible = false;
                }
                //show key2 for last change and if age is 70 then text is switiching to maxed out benefits
                if (ageAtLastChange.Equals(Constants.Number70) && valueAT70 != 0)
                {
                    lblKey8.Text = string.Format(valueAT70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps25 + strUserFirstName + Constants.WorkBenefitsKey);
                }
                else
                {
                    //if last change is not age 70
                    lblKey8.Text = string.Format(valueAT70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps25 + strUserFirstName + Constants.keyFigures16);
                }
                //step 1 for starting value
                if (startingValue != 0)
                    lblSteps7.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + WifeFRA + Constants.FRATextSurviour + strUserFirstName + Constants.steps24s + startingValue.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                else
                    lblSteps7.Text = string.Empty;
                //check age 70 value for 0
                if (valueAT70 != 0)
                {
                    //if last change is age 70 then wording will be switch to maxed out benfits
                    if (ageAtLastChange.Equals(Constants.Number70))
                    {
                        lblSteps8.Text = string.Format(Constants.Line2 + Constants.Age + "  " + ageAtLastChange + Constants.when + strUserFirstName + Constants.steps8 + ageAtLastChange + ", " + strUserFirstName + Constants.steps18 + valueAT70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                    }
                    else
                    {
                        //if last change not at age 70
                        lblSteps8.Text = string.Format(Constants.Line2 + Constants.Age + "  " + ageAtLastChange + Constants.when + strUserFirstName + Constants.steps8 + WifeFRA + Constants.FRAText + ", " + strUserFirstName + Constants.steps22 + valueAT70.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    }
                    lblSteps8.Visible = true;
                    lblSteps12.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageAtLastChange + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                }
                else
                {
                    //hide step 2
                    lblSteps8.Visible = false;
                    lblSteps12.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageAtLastChange + Constants.steps7 + Constants.AnualIncomeStep + AnualIncome.ToString(Constants.AMT_FORMAT));
                }
                lblKey12.Text = string.Format(AnualIncome.ToString(Constants.AMT_FORMAT) + Constants.keyFigures13 + Constants.steps25 + Constants.AnualIncomeKey);

                //Set values for Keys and steps to dispay on pdf
                SetKeyAndStepsValuesForPDF(Constants.GridClaim67);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to export the explanation text pop up to PDF
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void exportToPdf(object sender, EventArgs e)
        {
            try
            {
                Response.ContentType = Constants.pdfContentType;
                Response.AddHeader(Constants.pdfContentDisposition, "attachment;filename=Explanation_For_Widowed_Strategies" + "_" + DateTime.Now.ToString(Constants.DateFormat) + Constants.pdfFileExtension);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Document pdfDoc = new Document(PageSize.A4, Constants.zero, Constants.zero, -15f, 55f);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                writer.PageEvent = new PDFEvent();
                pdfDoc.Open();

                #region Old strategy explanation
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.WidowPopup, false, true, true));
                //#region What is the Primary Goal of Social Security Calculator?
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion1, false, false, true));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationTitleWidow1, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationTitleWidow2, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationTitleWidow3, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationTitleText, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //#endregion What is the Primary Goal of Social Security Calculator?
                //#region How the Social Security Calculator Works?
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion2, false, false, true));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWorkingWidow1, false, false, false));
                //pdfDoc.NewPage();
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWorkingWidow2, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWorkingWidow3, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWorkingWidow4, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWorkingWidow5, false, false, false));
                //#endregion How the Social Security Calculator Works?
                //#region What are the Key Benefit Numbers in the Strategies?
                //pdfDoc.NewPage();
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion3, false, false, true));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationKeys1, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationKeys2, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationKeys3, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //#endregion What are the Key Benefit Numbers in the Strategies?
                //#region What are the Next Steps in the Strategies?
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion4, false, false, true));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSteps1, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationSteps2, false, false, false));
                //pdfDoc.Add(new Paragraph(" "));
                //pdfDoc.Add(SiteNew.generateParagraph(Constants.GoodLuckText, false, true, true));
                //#endregion What are the Next Steps in the Strategies?
                #endregion Old strategy explanation

                //Add strategy explanation content
                AddExplainationToPDF(ref pdfDoc);

                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        ///  restting the values to null for the gloabal variables and lable texts
        /// </summary>
        public void ResetValues()
        {
            try
            {
                #region reset values
                //Make keys empty
                lblKey1.Text = string.Empty;
                lblKey2.Text = string.Empty;
                lblKey3.Text = string.Empty;
                lblKey4.Text = string.Empty;
                lblKey5.Text = string.Empty;
                lblKey6.Text = string.Empty;
                lblKey7.Text = string.Empty;
                lblKey8.Text = string.Empty;
                lblKey9.Text = string.Empty;

                //Make steps empty
                lblSteps1.Text = string.Empty;
                lblSteps2.Text = string.Empty;
                lblSteps3.Text = string.Empty;
                lblSteps4.Text = string.Empty;
                lblSteps5.Text = string.Empty;
                lblSteps6.Text = string.Empty;
                lblSteps7.Text = string.Empty;
                lblSteps8.Text = string.Empty;
                lblSteps9.Text = string.Empty;
                #endregion
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// calculate current age of Wife
        /// </summary>
        /// <param name="husbBD"></param>
        /// <returns></returns>
        protected void intAgeForWife(DateTime wifeBD)
        {
            try
            {
                TimeSpan Span = DateTime.Now - wifeBD;
                DateTime Age = DateTime.MinValue + Span;
                int Years = Age.Year - 1;
                int Months = Age.Month - 1;
                int Days = Age.Day - 1;
                intCurrentAgeWifeYear = Years;
                Globals.Instance.strCurrentAgeWife = Years.ToString() + " years and " + Months.ToString() + " months ";
                if (Days >= 28)
                    Months += 1;
                if (Months >= 6)
                    Years += 1;
                intCurrentAgeWifeMonth = Months;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcDivorced, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcDivorced + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// to reset the annual Income variables after each startegy
        /// </summary>
        public void ResetAnualIncomeVariables()
        {
            try
            {
                Globals.Instance.BoolWHBSingleSwitchedWife = false;
                Globals.Instance.BoolHusbandAgeAdjusted = false;
                Globals.Instance.BoolWifeAgeAdjusted = false;
                Globals.Instance.HusbandNumberofMonthsinNote = 0;
                Globals.Instance.WifeNumberofMonthinNote = 0;
                Globals.Instance.WifeNumberofMonthsinSteps = 0;
                Globals.Instance.HusbandNumberofMonthsinSteps = 0;
                Globals.Instance.HusbNumberofMonthsAboveGrid = 0;
                Globals.Instance.WifeNumberofMonthsAboveGrid = 0;
                Globals.Instance.HusbandNumberofYears = 0;
                Globals.Instance.WifeNumberofYears = 0;
                Globals.Instance.BoolWHBSwitchedHusb = false;
                Globals.Instance.BoolWHBSwitchedWife = false;
                Globals.Instance.HusbNumberofMonthsBelowGrid = 0;
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                Globals.Instance.WifeOriginalAnnualIncome = 0;
                Globals.Instance.HusbandOriginalAnnualIncome = 0;
                Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior = 0;
                Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = 0;
                Globals.Instance.BoolSpousalAfterWorkWife = false;
                Globals.Instance.BoolSpousalAfterWorkHusb = false;
                Globals.Instance.BoolWHBSingleSwitchedHusb = false;
                Globals.Instance.HusbNumberofYearsAbove66 = 0;
                Globals.Instance.WifeNumberofYearsAbove66 = 0;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Used to add strategy explaination to PDF
        /// </summary>
        /// <param name="pdfDoc">PDF Document</param>
        private void AddExplainationToPDF(ref Document pdfDoc)
        {
            try
            {
                #region Add titles
                pdfDoc.Add(SiteNew.generateParagraph(Constants.StrategyExpanationTitle, false, true, true));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.WidowedSpouses, false, true, true));
                #endregion Add titles

                #region What is the Primary Goal Of the Paid to Wait Calculator?
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion1, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion1Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What is the Primary Goal of the Paid to Wait Calculator?

                #region Many Married Couples Can Get Paid to Wait
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion13, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion2Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion Many Married Couples Can Get Paid to Wait

                #region Can All Qualified Divorced Spouses Get Paid to Wait?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion14, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion3Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion Can All Qualified Divorced Spouses Get Paid to Wait?

                #region How do I Know if I am Qualified?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion4, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion4Answer1, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion4Answer2, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion4Answer3, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion4Answer4, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion4Answer5, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion How do I Know if I am Qualified?

                #region How the Paid to Wait Calculator Works
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion5, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion5Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion How the Paid to Wait Calculator Works

                #region What if You Continue to Work?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion15, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion6Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What if You Continue to Work?

                #region What You Can Expect
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion7, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion7Answer, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What You Can Expect

                #region What are the �Key Benefit Numbers� in the Strategies?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion8, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion8Answer1, false, false, false));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion8Answer2, false, false, false));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion8Answer3, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What are the �Key Benefit Numbers� in the Strategies?

                #region What are the �Next Steps� in the Strategies?
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationQuestion9, false, false, true));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion9Answer1, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion9Answer2, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion9Answer3, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.ExplanationWidowedQuestion9Answer4, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion What are the �Next Steps� in the Strategies?

                #region Add Note for explanation
                pdfDoc.Add(SiteNew.generateParagraph(Constants.StrategyExplanationNote, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                #endregion Add Note for explanation

                #region Add Good Luck Text
                pdfDoc.Add(SiteNew.generateParagraph(Constants.GoodLuckText, false, true, true));
                #endregion Add Good Luck Text
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }


        /// <summary>
        /// Used to add strategy instructions in pdf
        /// </summary>
        /// <param name="pdfDoc"></param>
        private void AddInstructionBeforeSeeStrategy(ref Document pdfDoc)
        {
            try
            {
                #region Add Instructions
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(SiteNew.generateParagraph(Constants.StrategyInstruction, false, false, false));
                pdfDoc.Add(new Paragraph(" "));
                #endregion Add Instructions
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalcWidowed, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalcWidowed + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }
        #endregion
    }
}
