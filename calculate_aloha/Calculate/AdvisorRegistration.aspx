﻿<%@ Page Title="Advisor Registration" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="AdvisorRegistration.aspx.cs" Inherits="Calculate.AdvisorRegistration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="ScriptRegistration" EnablePageMethods="true" runat="server" ScriptMode="Release">
    </ajaxToolkit:ToolkitScriptManager>
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <div class="clearfix ">
        <h1 class="fontBold text-center">Create a New Account</h1>
        <p class="text-center">Step 1 of 2</p>
        <div class="col-md-12 col-sm-12">
            <div class="col-md-4 col-sm-4">
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-12 col-sm-12">
                        <asp:Panel DefaultButton="btnCreateUser" runat="server">
                            <div class="col-md-12 col-sm-12">
                                <%--<asp:LinkButton ID="CustomerLink" Class="Highlight" OnClick="CustomerLink_Click" runat="server" TabIndex="1" Text="Click here if you are a Customer"></asp:LinkButton>--%>
                                <%--    <br />--%>
                                <br />
                                <asp:Label ID="lblCustomerName" runat="server" AssociatedControlID="lblCustomerName"> First Name</asp:Label>
                                <asp:TextBox ID="txtCustomerName" runat="server" onChange="CheckFirstNameValidOrNot();" CausesValidation="false" CssClass="textEntry form-control" EnableViewState="False" onKeyDown="keyPress('txtCustomerName')" TabIndex="1" MaxLength="100" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="txtCustomerName" CssClass="failureErrorNotificationSignUp" ErrorMessage="Advisor name is required." ToolTip="Advisor name is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Advisor name is required.</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revCustomerName" runat="server" ControlToValidate="txtCustomerName" CssClass="failureErrorNotificationSignUp" ErrorMessage="Please enter valid Advisor name" ValidationExpression="^[A-Za-z][A-Za-z0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid Advisor name</asp:RegularExpressionValidator>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <br />
                                <asp:Label ID="lblEmail" runat="server" AssociatedControlID="lblEmail">E-mail</asp:Label>
                                <asp:TextBox ID="txtEmail" runat="server" onChange="CheckEmailValidOrNot();" CssClass="textEntry form-control" autocomplete="off" ValidationGroup="failureErrorNotificationSignUp" TabIndex="3" MaxLength="40" CausesValidation="false"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqEmail" runat="server" ControlToValidate="txtEmail" CssClass="failureErrorNotificationSignUp" ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*E-mail is required.</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatortxtEmail" runat="server" ControlToValidate="txtEmail" CssClass="failureErrorNotificationSignUp" ErrorMessage="Please enter valid E-mail" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid E-mail</asp:RegularExpressionValidator>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <br />
                                <asp:Label ID="lblCustomerPassword" runat="server" AssociatedControlID="lblCustomerPassword">Password</asp:Label>
                                <asp:TextBox ID="txtCustomerPassword" onChange="checkPasswordMatch();" runat="server" CssClass="textEntry form-control" CausesValidation="false" autocomplete="off" TextMode="Password" MaxLength="40" ValidationGroup="RegisterUserValidationGroup" TabIndex="4"></asp:TextBox>
                                <asp:Label ID="lblpasswordinstruct" Style="font-size: 12px; color: gray;" Text="(Password must be between 6-32 characters)" runat="server"></asp:Label>
                                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="txtCustomerPassword" CssClass="failureErrorNotificationSignUp" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic"><br />*Password is required.</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revCustomerPassword" runat="server" ControlToValidate="txtCustomerPassword" CssClass="failureErrorNotificationSignUp" ErrorMessage="Password length must be in between 6-32" ValidationExpression="^.{6,32}$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic"><br />*Password length must be in between 6-32.</asp:RegularExpressionValidator>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <br />
                                    <div class="col-md-6 col-sm-6">
                                        <asp:Label ID="lblCustomerConfirmPassword" runat="server" AssociatedControlID="lblCustomerConfirmPassword">Confirm Password</asp:Label>
                                        <asp:TextBox ID="txtConfirmCustomerPassword" runat="server" CausesValidation="false" CssClass="textEntry form-control" onChange="checkPasswordMatch();" TextMode="Password" MaxLength="40" ValidationGroup="RegisterUserValidationGroup" TabIndex="5"></asp:TextBox>
                                        <img class="img-responsive" id="imgpass" runat="server" src="/images/confirmPassword.png" style="display: none; position: static; float: right; margin-top: -25px;" />
                                        <asp:Label ID="lblconfirmpasswordinstruct" Style="font-size: 12px; color: gray;" Text="(Password must be between 6-32 characters)" runat="server"></asp:Label>
                                        <asp:RequiredFieldValidator ID="revConfirmPasswordRequired" runat="server" ControlToValidate="txtConfirmCustomerPassword" CssClass="failureErrorNotificationSignUp" Display="Dynamic" ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required." ValidationGroup="RegisterUserValidationGroup"><br />*Confirm Password is required.</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regtxtConfirmCustomerPassword" runat="server" ControlToValidate="txtConfirmCustomerPassword" CssClass="failureErrorNotificationSignUp" ErrorMessage="Confirm Password length must be in between 6-32" ValidationExpression="^.{6,32}$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic"><br />*Confirm Password must be between 6-32 characters </asp:RegularExpressionValidator>
                                        <asp:CompareValidator ID="cvPasswordCompare" runat="server" ControlToCompare="txtCustomerPassword" ControlToValidate="txtConfirmCustomerPassword" CssClass="failureErrorNotificationSignUp" Display="Dynamic" ErrorMessage="The Password and Confirm Password must match." ValidationGroup="RegisterUserValidationGroup"><br />*The Password and Confirm Password must match.</asp:CompareValidator>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 marginpopbottom">
                                <asp:CheckBox ID="ChkBoxIAgree" runat="server" />
                                <asp:LinkButton OnClick="ShowAgreementDetails" ID="linkAgreement" ForeColor="Black" Font-Size="Small" runat="server">I agree with terms & conditions</asp:LinkButton>
                                <br />
                                <asp:CustomValidator ID="custCheckBoxAgree" runat="server" OnServerValidate="custCheckBoxIAgree_ServerValidate" CssClass="failureErrorNotificationSignUp" ValidationGroup="RegisterUserValidationGroup" ErrorMessage="*Terms & conditions must be accepted" Display="Dynamic">*Terms & conditions must be accepted</asp:CustomValidator>
                                <br />
                                <asp:Button ID="btnCreateUser" runat="server" Text="Sign up" OnClientClick="CheckMailFirstName();" CssClass="button_login1" ValidationGroup="RegisterUserValidationGroup" OnClick="btnCreateUser_Click" TabIndex="6" />
                                <br />
                                <br />
                                <p style="font-size: 12px; color: gray;">On the next page, you will be asked for your credit card and billing information. You will not be charged anything until you submit your credit card information.</p>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 text-center">
                    <asp:Label ID="lblUserExist" runat="server" CssClass="failureErrorNotification" Font-Size="1em" ForeColor="Red" Visible="False"></asp:Label>
                    <asp:Label ID="lblUserCreate" CssClass="setGreen" runat="server" Visible="False"></asp:Label>
                </div>
                <br />
                <asp:ValidationSummary ID="vsRegisterUser" runat="server" CssClass="failureForgotNotification" />
                <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
                <br />
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlAgreement" ScrollBars="Auto" runat="server" Style="height: 500px; margin-top: -60px" CssClass="displayNone ExplanationModalPopup">
        <div class="h2PopUp modal-header" style="color: white; font-size: x-large; font: bold;">
            Advisor Single User Subscription Agreement
        </div>

        <div class="textExplanation plan-contentnew">
            <div class="bs-example ">
                <div class="panel-group" id="accordion">
                    <p class="rti-colora1">
                        BY ACCEPTING THIS AGREEMENT, EITHER BY CLICKING A BOX INDICATING YOUR ACCEPTANCE OR BY EXECUTING AN ORDER FORM THAT REFERENCES THIS AGREEMENT, YOU AGREE TO THE TERMS OF THIS AGREEMENT. IF YOU ARE ENTERING INTO THIS AGREEMENT ON BEHALF OF A COMPANY OR OTHER LEGAL ENTITY, YOU REPRESENT THAT 
YOU HAVE THE AUTHORITY TO BIND SUCH ENTITY AND ITS AFFILIATES TO THESE TERMS AND CONDITIONS, IN WHICH CASE THE TERMS "YOU" OR "YOUR" SHALL REFER TO SUCH ENTITY AND ITS AFFILIATES. IF YOU DO NOT HAVE SUCH AUTHORITY, OR IF YOU DO NOT AGREE WITH THESE TERMS AND CONDITIONS, YOU MUST NOT ACCEPT THIS AGREEMENT AND MAY NOT USE THE SERVICES.<br />
                        <br />
                        The Social Security Calculator at www.calculatemybenefits.com (hereinafter referred to as the “Web Site”) is comprised of various web pages operated by Filtech for the purpose of providing financial professionals with analysis and comparisons of Social Security and other retirement strategies. The Web Site is a subscription-based service offered to you (the subscriber) conditioned on your acceptance without modification of the terms, conditions, and notices contained herein. Your use of the Web Site constitutes your agreement to all such terms, conditions, and notices.<br />
                        <br />
                        Filtech reserves the right to change the terms, conditions, and notices under which the Web Site is offered, including but not limited to the charges associated with the use of the Web Site.
                        <br />
                        <br />
                    </p>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">License Terms and Restrictions&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p style="font: 100; font-size: large">
                                    Application License.
                                </p>
                                <p class="rti-colora1">
                                    Filtech hereby grants you a non-exclusive, non-transferable, worldwide right to use the Web Site during the term of this Agreement, solely for your own internal business purposes, subject to the terms and conditions contained herein. All rights not expressly granted to you are reserved by Filtech and its licensors. You are granted a single license and may use the Web Site for your professional practice, however, each additional user must obtain a separate license for their use. You may not access the Web Site if you are a direct competitor of Filtech, except with Filtech’s prior written consent. In addition, you may not access the Web Site for purposes of monitoring its availability, performance or functionality, or for any other benchmarking or competitive purposes.<br />
                                    <br />
                                </p>

                                <p style="font: 100; font-size: large">
                                    Additional License Restrictions.
                                </p>
                                <p class="rti-colora1">
                                    You shall not (i) license, sublicense, sell, resell, transfer, assign, distribute or otherwise commercially exploit or make available to any third party the Web Site in any way; (ii) modify or make derivative works based upon the Web Site; (iii) create Internet "links" to the Web Site (except we may agree to provide you with a single sign-on link or account creation and login links) or "frame" or "mirror" any web pages or content on any other server or wireless or Internet-based device; or (iv) reverse engineer or access the Web Site in order to (a) build a competitive product or service, (b) build a product using similar ideas, features, functions or graphics of the Web Site, or (c) copy any ideas, features, functions or graphics of the Web Site. User licenses cannot be shared or used by more than one individual User but may be reassigned from time to time to new Users who are replacing former Users who have terminated employment or otherwise changed job status or function and no longer use the Web Site. You may use the Web Site only for your internal business purposes and shall not: (i) knowingly interfere with or disrupt the integrity or performance of the Web Site or the integrity of the data contained therein; or (ii) attempt to gain unauthorized access to the Web Site or its related systems or networks. Your access will be limited to information and portions of databases relating solely to your subscription.<br />
                                    <br />
                                </p>

                                <p style="font: 100; font-size: large">
                                    Account Data.
                                </p>
                                <p class="rti-colora1">
                                    Filtech does not own any data, information or material that you submit to the Web Site in the course of using the planning tools ("Customer Data"). You, not Filtech, shall have sole responsibility for the accuracy, quality, integrity, legality, reliability, appropriateness, and intellectual property ownership or right to use of all Customer Data, and Filtech shall not be responsible or liable for the deletion, correction, destruction, damage, loss or failure to store any Customer Data. In the event this Agreement is terminated, Filtech will make available to you a file of the Customer Data within 30 days of termination if you so request at the time of termination. Filtech reserves the right to withhold, remove and/or discard Customer Data without notice for any breach, including, without limitation, your non-payment. Upon termination for cause, your right to access or use Customer Data immediately ceases, and Filtech shall have no obligation to maintain any Customer Data.<br />
                                    <br />
                                </p>

                                <p style="font: 100; font-size: large">
                                    Intellectual Property Ownership.
                                </p>
                                <p class="rti-colora1">
                                    Filtech alone (and its licensors, where applicable) shall own all right, title and interest, including all related Intellectual Property Rights, in and to the Web Site, reports generated from the Web Site, and any updates or subsequent revisions thereof, and any other intellectual property of Filtech provided to the User, including but not limited to any patents, copyrights, trademarks, service marks, trade names, service names, or logos now owned or that may be owned in the future by Filtech.<br />
                                    <br />
                                </p>

                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Use of the Web Site&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p style="font: 100; font-size: large">
                                    Filtech Responsibilities.
                                </p>
                                <p class="rti-colora1">
                                    We shall: (i) use commercially reasonable efforts to make the Web Site available 24 hours a day, 7 days a week, except for: (a) planned downtime (of which We shall provide prior notice), or (b) any unavailability caused by circumstances beyond our reasonable control, including without limitation, acts of God, acts of government, floods, fires, earthquakes, civil unrest, acts of terror, strikes or other labor problems, Internet service provider failures or delays, or denial of service attacks. Furthermore, we shall maintain appropriate administrative and technical safeguards for protection of the security, confidentiality and integrity of User data.  We shall not (a) modify User data, (b) disclose User data except as compelled by law in accordance with the Compelled Legal Disclosure section below or as expressly permitted in writing by you, or (c) access User data except to provide the services and prevent or address service or technical problems, or at your request in connection with customer support matters.<br />
                                    <br />
                                </p>

                                <p style="font: 100; font-size: large">
                                    Compelled Legal Disclosure.
                                </p>
                                <p class="rti-colora1">
                                    Filtech reserves the right at all times to disclose any information as necessary to satisfy any applicable law, regulation, legal process or governmental request, or to edit, refuse to post or to remove any information or materials, in whole or in part, in Filtech's sole discretion.<br />
                                    <br />
                                </p>

                                <p style="font: 100; font-size: large">
                                    Financial Calculations and Informational Content.
                                </p>
                                <p class="rti-colora1">
                                    The Web Site offers financial calculations and informational content to assist financial professionals with Social Security and retirement planning. Filtech does not guarantee the accuracy of the calculations, information, reports, or their applicability to your client’s particular situation. Calculations are based upon federal laws/regulations and information you enter, and will change with each use and over time. Filtech accepts no liability for your use or misuse of the site. In no way does Filtech guarantee or represent that you will achieve the projected results illustrated on the Web Site.<br />
                                    <br />
                                </p>

                                <p style="font: 100; font-size: large">
                                    Links to Third Party Sites.
                                </p>
                                <p class="rti-colora1">
                                    The Web Site may contain links to other web sites ("Linked Sites"). The Linked Sites are not under the control of Filtech and we are not responsible for the contents of any Linked Site, including without limitation any link contained in a Linked Site, or any changes or updates to a Linked Site. Filtech is not responsible for web casting or any other form of transmission received from any Linked Site. Filtech is providing these links to you only as a convenience, and the inclusion of any link does not imply endorsement by us of the site or any association with its operators.<br />
                                    <br />
                                </p>
                                <p style="font: 100; font-size: large">
                                    User Responsibilities.
                                </p>
                                <p class="rti-colora1">
                                    You shall (i) be responsible for compliance with this Agreement, (ii) be responsible for the accuracy, quality and legality of your data and of the means by which you acquired it, (iii) use commercially reasonable efforts to prevent unauthorized access to or use of the Web Site, and notify Filtech promptly of any such unauthorized access or use, and (iv) use the Web Site only in accordance with the applicable laws and government regulations. You shall not (a) make the Web Site available to anyone other than authorized Users, (b) sell, resell, rent or lease the Web Site, (c) use the Web Site to store or transmit infringing, libelous, or otherwise unlawful or tortious material, or to store or transmit material in violation of third-party privacy rights, (d) use the Web Site to store or transmit Malicious Code, (e) interfere with or disrupt the integrity or performance of the Web Site or third-party data contained therein, or (f) attempt to gain unauthorized access to the Web Site or their related systems or networks.<br />
                                    <br />
                                </p>

                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Term and Termination&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p style="font: 100; font-size: large">
                                    Initial Term and Renewal.
                                </p>
                                <p class="rti-colora1">
                                    The initial term will commence as of the date you accept this agreement. The Initial Term is one year payable monthly or annually. Upon the expiration of the Initial Term, this Agreement will automatically renew for successive renewal terms equal in duration to the Initial Term at Filtech's then current fees. Either party may terminate this Agreement, by notifying the other party in writing at least sixty (60) days prior to termination. In the case of free trials, notifications provided through the Web Site indicating the remaining number of days in the free trial shall constitute notice of termination. In the event this Agreement is terminated, Filtech will make available to you a file of the Customer Data within 30 days of termination if you so request at the time of termination. You agree and acknowledge that Filtech has no obligation to retain the Customer Data, and may delete such Customer Data, more than 30 days after termination.<br />
                                    <br />
                                </p>

                                <p style="font: 100; font-size: large">
                                    Termination for Material Breach. 
                                </p>
                                <p class="rti-colora1">
                                    Any breach of your payment obligations or unauthorized use of the Web Site will be deemed a material breach of this Agreement. Filtech, in its sole discretion, may terminate your password, account or use of the Web Site if you breach or otherwise fail to comply with this Agreement. You agree and acknowledge that Filtech has no obligation to retain the Customer Data, and may delete such Customer Data more than 30 days after termination, if you have materially breached this Agreement, including but not limited to failure to pay outstanding fees, and such breach has not been cured within 30 days of notice of such breach. In the event this Agreement is terminated for cause, Filtech will make available to you a file of the Customer Data within 30 days of termination if you so request at the time of termination.<br />
                                    <br />
                                </p>

                                <p style="font: 100; font-size: large">
                                    Free Trial.
                                </p>
                                <p class="rti-colora1">
                                    If you register for a free trial, we will make the Web Site available to you on a trial basis free of charge until the earlier of (a) the end of the free trial period for which you registered or are registering to use the Web Site or (b) the start date of any purchased services ordered by you. Additional trial terms and conditions may appear on the trial registration web page.  Any such additional terms and conditions are incorporated into this Agreement by reference and are legally binding.<br />
                                    <br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Fees and Payment Terms&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p style="font: 100; font-size: large">
                                    Web Site Fees.
                                </p>
                                <p class="rti-colora1">
                                    Upon execution of this Agreement, Filtech shall invoice subscriber $40 monthly or $395 annually (the “Initial Subscription Fee”) in consideration for the services provided to subscriber during the initial term. All other services shall be invoiced in accordance with the following:<br />
                                    <br />
                                    Filtech will notify subscriber of any changes in its fee schedule, provided that any such changes will not apply to the Initial Term or the then-current renewal term.<br />
                                    <br />
                                </p>

                                <p style="font: 100; font-size: large">
                                    Payment Terms.
                                </p>
                                <p class="rti-colora1">
                                    All invoices are due electronically at date of invoice subject to a thirty (30) day grace period from date of invoice if there is a technical difficulty (e.g., expired or invalid credit card). All payments shall be made in U.S. dollars and shall be non-refundable. If subscriber is delinquent in payments, Filtech may suspend or cancel access to Web Site.<br />
                                    <br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Limitations of Liability&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p style="font: 100; font-size: large">
                                    Mutual Indemnification.
                                </p>
                                <p class="rti-colora1">
                                    You shall indemnify and hold Filtech, its licensors and each such party's parent organizations, subsidiaries, affiliates, officers, directors, employees, attorneys and agents harmless from and against any and all claims, costs, damages, losses, liabilities and expenses (including attorneys' fees and costs) arising out of or in connection with: (i) a claim alleging that use of the Customer Data infringes the rights of, or has caused harm to, a third party; (ii) a claim, which if true, would constitute a violation by you of your representations and warranties; or (iii) a claim arising from the breach by you or your Users of this Agreement, provided in any such case that Filtech (a) gives written notice of the claim promptly to you; (b) gives you sole control of the defense and settlement of the claim (provided that you may not settle or defend any claim unless you unconditionally release Filtech of all liability and such settlement does not affect Filtech's business or Web Site); (c) provides to you all available information and assistance; and (d) has not compromised or settled such claim.<br />
                                    <br />
                                    Filtech shall indemnify and hold you and your parent organizations, subsidiaries, affiliates, officers, directors, employees, attorneys and agents harmless from and against any and all claims, costs, damages, losses, liabilities and expenses (including attorneys' fees and costs) arising out of or in connection with: (i) a claim alleging that the Web Site directly infringes a copyright, a U.S. patent issued as of the Effective Date, or a trademark of a third party; (ii) a claim, which if true, would constitute a violation by Filtech of its representations or warranties; or (iii) a claim arising from breach of this Agreement by Filtech; provided that you (a) promptly give written notice of the claim to Filtech; (b) give Filtech sole control of the defense and settlement of the claim (provided that Filtech may not settle or defend any claim unless it unconditionally releases you of all liability); (c) provide to Filtech all available information and assistance; and (d) have not compromised or settled such claim. Filtech shall have no indemnification obligation, and you shall indemnify Filtech pursuant to this Agreement, for claims arising from any infringement arising from the combination of the Web Site with any of your products, service, hardware or business process(s).<br />
                                    <br />
                                </p>

                                <p style="font: 100; font-size: large">
                                    Disclaimer of Warranties.
                                </p>
                                <p class="rti-colora1">
                                    FILTECH AND ITS LICENSORS MAKE NO REPRESENTATION, WARRANTY, OR GUARANTY AS TO THE RELIABILITY, TIMELINESS, QUALITY, SUITABILITY, TRUTH, AVAILABILITY, ACCURACY OR COMPLETENESS OF THE WEB SITE OR ANY CONTENT. FILTECH AND ITS LICENSORS DO NOT REPRESENT OR WARRANT THAT (A) THE USE OF THE WEB SITE WILL BE SECURE, TIMELY, UNINTERRUPTED OR ERROR-FREE OR OPERATE IN COMBINATION WITH ANY OTHER HARDWARE, SOFTWARE, SYSTEM OR DATA, (B) THE WEB SITE WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS, (C) ANY STORED DATA WILL BE ACCURATE OR RELIABLE, (D) THE QUALITY OF ANY PRODUCTS, SERVICES, INFORMATION, OR OTHER MATERIAL PURCHASED OR OBTAINED BY YOU THROUGH THE WEB SITE WILL MEET YOUR REQUIREMENTS OR EXPECTATIONS, (E) ERRORS OR DEFECTS WILL BE CORRECTED, OR (F) THE WEB SITE OR THE SERVER(S) THAT MAKE THE WEB SITE AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. THE WEB SITE AND ALL CONTENT IS PROVIDED TO YOU STRICTLY ON AN "AS IS" BASIS. ALL CONDITIONS, REPRESENTATIONS AND WARRANTIES, WHETHER EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT OF THIRD PARTY RIGHTS, ARE HEREBY DISCLAIMED TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW BY FILTECH AND ITS LICENSORS.<br />
                                    <br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">General Provisions&nbsp;&nbsp;&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseSix" class="hrefColor" style="font-size: smaller">[Click to View Answer]</a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p style="font: 100; font-size: large">
                                    Severability.
                                </p>
                                <p class="rti-colora1">
                                    If any provision of this Agreement is held by a court of competent jurisdiction to be contrary to law, the provision shall be modified by the court and interpreted so as best to accomplish the objectives of the original provision to the fullest extent permitted by law, and the remaining provisions of this Agreement shall remain in effect.<br />
                                    <br />

                                </p>

                                <p style="font: 100; font-size: large">
                                    Governing Law.
                                </p>
                                <p class="rti-colora1">
                                    This Agreement will be governed by the laws of the state of New York.<br />
                                    <br />
                                </p>
                                <p style="font: 100; font-size: large">
                                    Export Compliance.
                                </p>
                                <p class="rti-colora1">
                                    The Web Site and any other technology Filtech makes available, and derivatives thereof may be subject to export laws and regulations of the United States and other jurisdictions. Each party represents that it is not named on any U.S. government denied-party list. You shall not permit users to access or use the Web Site in a U.S.-embargoed country (currently Cuba, Iran, North Korea, Sudan or Syria) or in violation of any U.S. export law or regulation.<br />
                                    <br />
                                </p>
                                <p style="font: 100; font-size: large">
                                    Relationship of the Parties.
                                </p>
                                <p class="rti-colora1">
                                    The parties are independent contractors. This Agreement does not create a partnership, franchise, joint venture, agency, fiduciary or employment relationship between the parties.<br />
                                    <br />
                                </p>
                                <p style="font: 100; font-size: large">
                                    Entire Agreement.
                                </p>
                                <p class="rti-colora1">
                                    This Agreement, including all exhibits and addenda hereto and all Order Forms, constitutes the entire agreement between the parties and supersedes all prior and contemporaneous agreements, proposals or representations, written or oral, concerning its subject matter.<br />
                                    <br />
                                </p>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
            <p class="text-center fontBold" style="color: #85312F">
                <asp:Button ID="btnDownload" runat="server" Text="Download" OnClick="DownloadTermsCondition" CssClass="button_login" />
                <asp:Button ID="btnCancel" runat="server" CssClass="button_login " OnClick="btnCancel_Click" Text="Cancel" />
                <%--OnClick="exportToPdf" OnClick="imgCancel_Click"--%>
            </p>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlShowMsg" CssClass="MailSentPopUpModal" runat="server">
        <div class="modal-body">
            <br />
            <asp:Label runat="server" Text="Your account has been created successfully!" ID="lblMessage"></asp:Label>
            <br />
            <br />
            <asp:Button runat="server" ID="btnOk" OnClick="RedirectToChargify" CssClass="button_ok_poopup" Text="OK" />
            <br />
        </div>
    </asp:Panel>

    <asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
    <cc1:ModalPopupExtender ID="popUpAgreementDetails" runat="server" PopupControlID="pnlAgreement" TargetControlID="lnkFake" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
    <asp:LinkButton ID="lnkFake1" runat="server"></asp:LinkButton>
    <cc1:ModalPopupExtender ID="MailSentPopUp" runat="server" BehaviorID="MsgPopUp" PopupControlID="pnlShowMsg" TargetControlID="lnkFake1" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>

     <script src="../Scripts/common.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        function ClearHistory() {
            var backlen = history.length;
            history.go(-backlen);
            window.location.href = loggedOutPageUrl
        }
        $(function () {
            setTimeout(function () {
                $("[id$=lblUserExist]").fadeOut(3000);
            }, 5000);
        });
        function ClearText() {
            var textbox = document.getElementById("<%=txtCustomerName.ClientID%>");
            textbox.value = "";
        }
        $(function fade() {
            setTimeout(function () {
                $("[id$=lblUserLoginError]").fadeOut(1000);
            }, 5000);

        });
        $(function fade() {
            setTimeout(function () {
                $("[id$=ErrorMessagelable]").fadeOut(1000);
            }, 5000);
        });
        $(document).ready(function () {


            $("#ImgLogin").click(function (e) {
                ShowDialog(true);
                e.preventDefault();
            });
            //window.scrollTo(0, 400);
            var Browser = {
                IsIe: function () {
                    return navigator.appVersion.indexOf("MSIE") != -1;
                },
                Navigator: navigator.appVersion,
                Version: function () {
                    var version = 999; // we assume a sane browser
                    if (navigator.appVersion.indexOf("MSIE") != -1)
                        // bah, IE again, lets downgrade version number
                        version = parseFloat(navigator.appVersion.split("MSIE")[1]);
                    return version;
                }
            };
            if (Browser.IsIe && Browser.Version() <= 9) {
                $("#fldLogin").addClass("registerIE");
                $("#fldAccount").addClass("registerIE");
                $("[id$=txtCustomerName]").addClass("width67");
                $("[id$=txtEmail]").addClass("width67");
            }
            <%if (Session["UserID"] == null)
              {%>
            $('#content').attr("style", "border:none;");
                <%}%>
            //$('#ctl00_MainContent_vsLogin').find('li').attr("style", "list-style-image:url(../images/validarrow.png)");
            $('#myTab a:first').tab('show');


        });
        function checkPasswordMatch() {
            var password = $("[id$=txtCustomerPassword]").val();
            var confirmPassword = $("[id$=txtConfirmCustomerPassword]").val();
            if (password != null && confirmPassword != null) {
                if (password != '' && confirmPassword != '') {

                    if (password.length >= 6 && confirmPassword.length >= 6) {
                        if (password == confirmPassword) {
                            $("[id$=imgpass]").css("display", "");

                            $("[id$=txtCustomerPassword]").removeClass("errorSignUp");
                            $("[id$=txtConfirmCustomerPassword]").removeClass("errorSignUp");
                        }
                        else {
                            $("[id$=imgpass]").css("display", "none");
                            $("[id$=txtCustomerPassword]").addClass("errorSignUp");
                            $("[id$=txtConfirmCustomerPassword]").addClass("errorSignUp");
                            $("[id$=regtxtConfirmCustomerPassword]").removeClass("errorSignUp");

                        }
                    }
                    else {
                        $("[id$=imgpass]").css("display", "none");
                        $("[id$=txtCustomerPassword]").addClass("errorSignUp");
                        $("[id$=txtConfirmCustomerPassword]").addClass("errorSignUp");
                        $("[id$=regtxtConfirmCustomerPassword]").removeClass("errorSignUp");


                    }
                }
            }
        }


        function CheckPasswordEmptyOrNot() {
            var password = $("[id$=txtCustomerPassword]").val();
            var confirmPassword = $("[id$=txtConfirmCustomerPassword]").val();
            if (password != null) {
                if (password != '') {
                    if (password.length >= 6) {
                        $("[id$=txtCustomerPassword]").removeClass("errorSignUp");
                    }
                    else {
                        $("[id$=txtCustomerPassword]").addClass("errorSignUp");
                    }
                }
                else {
                    $("[id$=txtCustomerPassword]").addClass("errorSignUp");


                }
            }
            if (confirmPassword != null) {
                if (confirmPassword != '') {
                    if (confirmPassword.length >= 6) {
                        $("[id$=txtConfirmCustomerPassword]").removeClass("errorSignUp");
                    }
                    else {
                        $("[id$=txtConfirmCustomerPassword]").addClass("errorSignUp");
                        $("[id$=regtxtConfirmCustomerPassword]").removeClass("errorSignUp");
                    }
                }
                else {
                    $("[id$=txtConfirmCustomerPassword]").addClass("errorSignUp");
                    $("[id$=regtxtConfirmCustomerPassword]").removeClass("errorSignUp");
                }
            }

        }

        function CheckEmailValidOrNot() {

            /* Check if field exists in current form or not */
            if ($("[id$=txtEmail]").val() != null) {
                /* Validate Email Field */
                if ($("[id$=txtEmail]").val() == '') {
                    /* Add error css class */
                    $("[id$=txtEmail]").addClass("errorSignUp");
                    $("[id$=RegularExpressionValidatortxtEmail]").removeClass("errorSignUp");
                }
                else {

                    //var emailfilter1 = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
                    var emailfilter1 = /^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$/;
                    /* Validation Email Address Format */
                    if (!emailfilter1.test($("[id$=txtEmail]").val())) {
                        /* Add error css class */
                        $("[id$=txtEmail]").addClass("errorSignUp");

                        $("[id$=RegularExpressionValidatortxtEmail]").removeClass("errorSignUp");
                    }
                    else {
                        /* Remove Css Class */
                        $("[id$=txtEmail]").removeClass("errorSignUp");
                    }
                }
            }
        }

        function CheckMailFirstName() {
            CheckFirstNameValidOrNot();
            CheckEmailValidOrNot();
            CheckPasswordEmptyOrNot();
            checkPasswordMatch();

        }


        function CheckFirstNameValidOrNot() {

            /* Check if field exists in current form or not */
            if ($("[id$=txtCustomerName]").val() != null) {
                /* Validate Email Field */
                if ($("[id$=txtCustomerName]").val() == '') {
                    /* Add error css class */
                    $("[id$=txtCustomerName]").addClass("errorSignUp");
                }
                else {
                    var nameFilter = /^[A-Za-z][A-Za-z0-9]*$/;
                    /* Validation Email Address Format */
                    if (!nameFilter.test($("[id$=txtCustomerName]").val())) {
                        /* Add error css class */
                        $("[id$=txtCustomerName]").addClass("errorSignUp");
                    }
                    else {
                        /* Remove Css Class */
                        $("[id$=txtCustomerName]").removeClass("errorSignUp");
                    }
                }
            }
        }

        function HideModalPopupForMessage() {
            $find("MsgPopUp").hide();
            PageMethods.RedirectToChargify();
            return false;
        }

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
</asp:Content>


