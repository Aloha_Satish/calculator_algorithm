﻿using CalculateDLL;
using Symbolics;
using System;
using System.Data;
using System.Reflection;

namespace Calculate
{
    public partial class Media : System.Web.UI.Page
    {
        #region Events
        /// <summary>
        /// Function call when page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Session[Constants.SessionRegistered] = null;
                this.getModuleDetails();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorMedia, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        #endregion Events

        #region Methods

        /// <summary>
        /// Get Description of page
        /// </summary>
        public void getModuleDetails()
        {
            try
            {
                ModuleData moduleData = new ModuleData();
                DataTable dtModuleData = moduleData.getModuleDataDescription(Constants.Media); ;
                divMedia.InnerHtml = dtModuleData.Rows[0][Constants.ModuleDataDescription].ToString();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorMedia, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        #endregion Methods
    }
}