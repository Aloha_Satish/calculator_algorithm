﻿<%@ Page Title="Pricing" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="Pricing.aspx.cs" Inherits="Calculate.Pricing" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-md-12 col-sm-12">
        <br />
        <br />
        <div class="col-md-4 col-sm-4 text-justify" style="border-right: 1px solid;">
            <h1 style="text-align: center; font-size: xx-large; font-weight: bold; color: black;">Single Use</h1>
            <p style="text-align: center">
                Best For Retirees and  Near Retirees
            </p>
            <h1 style="text-align: center; font-size: xx-large; font-weight: bold;">$39
            </h1>
            <p style="text-align: center">
                One Time Payment<br />
            </p>
            <div class="line-horizontal" style="width: auto; height: 10px; border-bottom: 2px solid #000000;"></div>
            <br />
            <div style="text-align: center;">
                <a href="/SingleUse.aspx" class="dynamic-button shadow btn" style="text-decoration: none;">Get Started</a>
            </div>
            <br />
            <div>
                <ul style="line-height: 22.4px; font-size: medium">
                    <li><span style="">Single time use of Social Security calculator</span></li>
                </ul>
                <ul style="line-height: 22.4px; font-size: medium">
                    <li>One PDF-Report with recommended Social Security Claiming Strategies</li>
                </ul>
                <ul style="line-height: 22.4px; font-size: medium; visibility: hidden;">
                    <li>Create unlimited client accounts, store their basic information and strategies online</li>
                </ul>
                <ul style="line-height: 22.4px; font-size: medium; visibility: hidden;">
                    <li>Create unlimited client accounts</li>
                </ul>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 text-justify" style="border-right: 1px solid; text-align: justify;">
            <h1 style="text-align: center; font-size: xx-large; font-weight: bold; color: black;">Professional </h1>
            <p style="text-align: center">
                Ideal For Financial Professionals
            </p>
            <h1 style="text-align: center; font-size: xx-large; font-weight: bold;">$40 &nbsp; &nbsp;&nbsp; $395   
            </h1>
            <p style="text-align: center; margin-left: -15px;">
                Monthly &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;Annual
            </p>
            <div class="line-horizontal" style="width: auto; height: 10px; border-bottom: 2px solid #000000;"></div>
            <br />
            <div style="text-align: center;">
                <%--<a href="AdvisorRegistration.aspx" class="dynamic-button shadow btn" style="text-decoration: none;">Get Started</a>--%>
                <asp:LinkButton ID="lnkBtnMonthly" runat="server" class="dynamic-button shadow btn" Text="Monthly" OnClick="Monthly_Plan_Click" Style="text-decoration: none; width: 90px"></asp:LinkButton>&nbsp;&nbsp;
                <asp:LinkButton ID="lnkBtnAnnual" runat="server" class="dynamic-button shadow btn" OnClick="Annual_Plan_Click" Text="Annual" Style="text-decoration: none; width: 90px"></asp:LinkButton>&nbsp;&nbsp;
            </div>
            <br />
            <div>
                <ul style="line-height: 22.4px; font-size: medium;">
                    <li><span style="">Unlimited use of Social Security calculator</span></li>
                </ul>
                <ul style="line-height: 22.4px; font-size: medium;">
                    <li>Unlimited PDF-Reports with recommended Social Security Claiming Strategies</li>
                </ul>
                <ul style="line-height: 22.4px; font-size: medium;">
                    <li>Create unlimited client accounts, store their basic information and strategies online</li>
                </ul>
                <ul style="line-height: 22.4px; font-size: medium;">
                    <li>Send PDF Social Security reports directly to clients</li>
                </ul>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 text-justify">
            <h1 style="text-align: center; font-size: xx-large; font-weight: bold; color: black;">Enterprise </h1>
            <p style="text-align: center">
                Made For Financial Organizations 
            </p>
            <h1 style="text-align: center; font-size: xx-large; font-weight: bold;">Contact for Details
            </h1>

            <p style="visibility: hidden;">
                Socila Security calculator
            </p>
            <div class="line-horizontal" style="width: auto; height: 10px; border-bottom: 2px solid #000000;"></div>
            <br />
            <div style="text-align: center;">
                <asp:Button runat="server" Text="Get Started" CssClass="dynamic-button shadow" Enabled="false" />
            </div>
            <br />
            <div>
                <ul style="line-height: 22.4px; font-size: medium;">
                    <li><span style="">Unlimited use of Social Security calculator</span></li>
                </ul>
                <ul style="line-height: 22.4px; font-size: medium;">
                    <li>Unlimited PDF-Reports with recommended Social Security Claiming Strategies</li>
                </ul>
                <ul style="line-height: 22.4px; font-size: medium;">
                    <li>Create unlimited client accounts and send PDF reports directly to them</li>
                </ul>
                <ul style="line-height: 22.4px; font-size: medium;">
                    <li>Create new user accounts for financial professionals and team members</li>
                </ul>
            </div>
        </div>
    </div>
    <div style="visibility: hidden">
        Social Security Calculator<br />
        <br />
    </div>

   <%-- <script type="text/javascript">
        $(document).ready(function () {
            window.scrollTo(0, 400);
        })
    </script>--%>

      <script type="text/javascript">
          //Google Analytics
          (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments)
              }, i[r].l = 1 * new Date(); a = s.createElement(o),
              m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
          })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
          ga('create', 'UA-88761127-1', 'auto');
          ga('send', 'pageview');

    </script>
</asp:Content>
