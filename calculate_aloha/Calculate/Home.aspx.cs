﻿using CalculateDLL;
using Symbolics;
using System;
using System.Data;
using System.Web.UI.WebControls;

namespace Calculate
{
    public partial class Home : System.Web.UI.Page
    {
        #region Events
        /// <summary>
        /// Function call when page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string strReference = string.Empty;
                if (Request.QueryString[Constants.reference] != null)
                    strReference = Request.QueryString[Constants.reference].ToString();
                if (strReference.ToLower().Equals("login"))
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, "ShowDialog(true);", true);
                }
                else
                {

                    setBanner();
                    Session[Constants.SessionRegistered] = null;
                    this.getModuleDetails();
                }

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorHome + "Page_Load()" + ex.ToString());
            }
        }

        private void setBanner()
        {
            //string strUrlName = Request.Url.Host.ToLower();
            //Theme themes = new Theme();
            //DataTable dtTheme = themes.GetBannerDetailsByDomainName(strUrlName);
            //if (Request.QueryString[Constants.URL] != null)
            //{
            //    if (dtTheme.Rows.Count > 0)
            //    {
            //        if (Request.QueryString[Constants.URL].ToString().Equals(dtTheme.Rows[Constants.zero][Constants.URLString]))
            //        {
            //            Session[Constants.BannerImage] = Constants.SessionBanner;
            //            byte[] imageBytes = (byte[])dtTheme.Rows[Constants.zero][Constants.BannerImage];
            //            Globals.Instance.ChangeBanner = Constants.ImgUrl + Convert.ToBase64String(imageBytes);
            //        }
            //        else
            //        {
            //            Globals.Instance.ChangeBanner = Constants.DefaultBanner;
            //        }
            //    }
            //    else
            //    {
            //        Globals.Instance.ChangeBanner = Constants.DefaultBanner;
            //    }
            //}
            //else
            //{
            //    if (dtTheme.Rows.Count > 0)
            //    {
            //        byte[] imageBytes = (byte[])dtTheme.Rows[Constants.zero][Constants.BannerImage];
            //        Globals.Instance.ChangeBanner = Constants.ImgUrl + Convert.ToBase64String(imageBytes);
            //        Session[Constants.BannerImage] = Constants.SessionBanner;
            //    }
            //    else
            //    {
            //        Globals.Instance.ChangeBanner = Constants.DefaultBanner;
            //    }
            //}
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "JsFunc", String.Format("ChangeBanner('{0}');", Globals.Instance.ChangeBanner), true);


            if (Request.QueryString[Constants.URL] != null && Request.QueryString[Constants.URL].ToString().Equals("grand"))
            {
                Session[Constants.BannerImage] = Constants.FlagYes;
                Globals.Instance.ChangeBanner = "/Images/BannerGrand.jpg";
                //byte[] imageBytes = (byte[])dtTheme.Rows[Constants.zero][Constants.BannerImage];
                //Globals.Instance.ChangeBanner = Constants.ImgUrl + Convert.ToBase64String(imageBytes);
            }
            else
            {
                Session[Constants.BannerImage] = Constants.FlagNo;
                Globals.Instance.ChangeBanner = Constants.DefaultBanner;
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), "JsFunc", String.Format("ChangeBanner('{0}');", Globals.Instance.ChangeBanner), true);


        }

        #endregion Events

        #region Methods

        /// <summary>
        /// Get Description of page
        /// </summary>
        public void getModuleDetails()
        {
            try
            {
                ModuleData moduleData = new ModuleData();
                DataTable dtModuleData = moduleData.getModuleDataDescription(Constants.Home); ;
                //divHome.InnerHtml = dtModuleData.Rows[0][Constants.ModuleDataDescription].ToString();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorHome + "getModuleDetails()" + ex.ToString());
            }
        }

        /// <summary>
        /// Code for login of a user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSignUp_Click(object sender, EventArgs e)
        {
            Response.Redirect(Constants.RedirectSignUp, false);
        }

        /// <summary>
        /// function to check login details for a user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void checkLoginDetails(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        /* Create object of User Class */
        //        Users objUsers = new Users();
        //        Customers objCustomer = new Customers();
        //        /* Fetched data for user based on email ID */
        //        DataTable dtUserDetails = objUsers.getUserDetails(txtEmailAddress.Text);
        //        Session[Constants.CustomerId] = dtUserDetails.Rows[0][Constants.SessionUserId];
        //        /* Check if email exists in table or not */
        //        if (dtUserDetails.Rows.Count == Constants.zero)
        //        {
        //            /* Display error message when user email id not exists in table */
        //            lblUserLoginError.Visible = true;
        //            lblUserLoginError.Text = Constants.invalidEmail;
        //        }
        //        else
        //        {
        //            if (dtUserDetails.Rows[Constants.zero][Constants.UserDeleteFlag].ToString().Equals(Constants.FlagYes))
        //            {
        //                /* Display Error Message in UI */
        //                lblUserLoginError.Text = Constants.accountDeleted;
        //                lblUserLoginError.Visible = true;
        //            }
        //            else
        //            {
        //                if (dtUserDetails.Rows[Constants.zero][Constants.PasswordResetFlag].ToString().Equals(Constants.FlagYes) && (txtPassword.Text.Trim()).Equals(dtUserDetails.Rows[Constants.zero][Constants.Password].ToString()))
        //                {
        //                    /* Assign User details into Session */
        //                    //Session[Constants.SessionRole] = dtUserDetails.Rows[Constants.zero][Constants.UserRole].ToString();
        //                    //Session[Constants.SessionUserId] = dtUserDetails.Rows[Constants.zero][Constants.UserId].ToString();
        //                    Session[Constants.EmailID] = txtEmailAddress.Text.Trim();
        //                    /* Redirect to User in UI based on its role */
        //                    Response.Redirect(Constants.RedirectChangePassword, false);
        //                }
        //                else
        //                {
        //                    /* Check if Passowrd matched or not */
        //                    if (Encryption.Decrypt(dtUserDetails.Rows[Constants.zero][Constants.UserPassword].ToString()).Equals(txtPassword.Text))
        //                    {
        //                        /* Check if user activated or not */
        //                        if (Convert.ToString(dtUserDetails.Rows[Constants.zero][Constants.UserActiveFlag]).Equals(Constants.FlagNo))
        //                        {
        //                            /* Display Error Message in UI */
        //                            lblUserLoginError.Text = Constants.accountSuspend;
        //                            lblUserLoginError.Visible = true;
        //                        }
        //                        else
        //                        {
        //                            /* Assign User details into Session */
        //                            Session[Constants.SessionRole] = dtUserDetails.Rows[Constants.zero][Constants.UserRole].ToString();
        //                            Session[Constants.SessionUserId] = dtUserDetails.Rows[Constants.zero][Constants.UserId].ToString();
        //                            DataTable dtCustomerDetails = objCustomer.getCustomerDetails(Session[Constants.SessionUserId].ToString());
        //                            /* Redirect to User in UI based on its role */
        //                            if (dtUserDetails.Rows[Constants.zero][Constants.UserRole].ToString().Equals(Constants.Customer))
        //                            {
        //                                if (dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription].ToString().Equals(Constants.FlagYes))
        //                                {
        //                                    Session[Constants.UserRegistered] = Constants.value;
        //                                    Response.Redirect(Constants.RedirectInitialSocialSecurity, false);
        //                                }
        //                                else
        //                                {
        //                                    Response.Redirect(Constants.RedirectInitialSocialSecurity, false);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                /* Redirect to Admin Welcome Page based on User Role */
        //                                Session[Constants.SessionAdminId] = dtUserDetails.Rows[Constants.zero][Constants.UserId].ToString();
        //                                if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor))
        //                                    Response.Redirect(Constants.RedirectManageCustomers, false);
        //                                else
        //                                    Response.Redirect(Constants.RedirectWelcomeAdmin, false);
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        /* Display Error message when invalid password entered by User */
        //                        lblUserLoginError.Visible = true;
        //                        lblUserLoginError.Text = Constants.invalidPassword;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog.WriteError(Constants.ErrorHome + "checkLoginDetails()" + ex.ToString());
        //        lblUserLoginError.Text = Constants.InvalidDetails;
        //        lblUserLoginError.Visible = true;
        //    }
        //}

        /// <summary>
        /// code to redirect the user to the Registration page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClickHere_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectPricing);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorHome + "btnClickHere_Click()" + ex.ToString());
            }
        }

        /// <summary>
        /// code to redirect the user to the Registration page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDemo_Click(object sender, EventArgs e)
        {
            try
            {
                Session["Demo"] = "Demo";
                Response.Redirect(Constants.RedirectDemoUser);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorHome + "btnDemo_Click()" + ex.ToString());
            }
        }

        #endregion Methods
    }
}