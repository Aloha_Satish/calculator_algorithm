﻿using CalculateDLL;
using Symbolics;
using System;

namespace Calculate
{
    public partial class ClientMasterDemo : System.Web.UI.MasterPage
    {
        #region Events

        /// <summary>
        /// code is called when page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    ValidateSession();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorClientMaster + "Page_Load()-" + ex.ToString());
            }
        }

        /// <summary>
        /// code to validate the user session
        /// </summary>
        public void ValidateSession()
        {
            if (Session["UserID"] == null)
            {
                try
                {
                    Response.Redirect(Constants.RedirectLogin, false);
                }
                catch (Exception ex)
                {
                    ErrorLog.WriteError(Constants.ErrorClientMaster + "ValidateSession()" + ex.ToString());
                }
            }
        }

        /// <summary>
        /// code to redirect to welcome page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgbtnWelcome_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectWelcomeUser, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorClientMaster + "imgbtnWelcome_Click()" + ex.ToString());
            }
        }

        /// <summary>
        /// redirect to SocialSecurityCalculator.aspx
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgbtnSocialSecurity_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectSocialSecurityCalculator, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorClientMaster + "imgbtnSocialSecurity_Click()" + ex.ToString());
            }
        }

        /// <summary>
        /// redirect to my accounts page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgbtnMyAccount_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectMyAccount, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorClientMaster + "imgbtnMyAccount_Click()" + ex.ToString());
            }
        }

        #endregion Events
    }
}