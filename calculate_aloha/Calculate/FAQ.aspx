﻿<%@ Page Title="FAQ" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="FAQ.aspx.cs" Inherits="Calculate.FAQ" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            <%if (Session["UserID"] == null)
              {%>
            $('#content').attr("style", "border:none;");
        });
        <%}%>

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    
    <div runat="server" id="divFAQ" class="col-md-12 col-sm-12 divBg setTopMargin" >
        
    </div>
</asp:Content>
        