﻿<%@ Page Title="Disclaimer" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="FrmDisclaimer.aspx.cs" Inherits="Calculate.FrmDisclaimer" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row" style="margin-left: 20px; margin-right:20px; font-family: Arial">
        <h1>Disclaimer</h1>
        <p>
            This report and the strategies contained herein is intended to be an introduction to some of the Social Security claiming strategies that are most likely to play a role in your retirement planning. It is not intended to be a substitute for personalized advice from a professional advisor. The information presented in this report and on the web site is for educational and illustrative purposes only; it should not be relied upon in your specific circumstances. In addition, our intent is accuracy based on current law. Please keep in mind that Congress may change the law governing benefit amounts at any time. 
        </p>

        <p>
            Every effort has been made to ensure this report and web site is as accurate and complete as possible. However, no representations or warranties are made with respect to the accuracy or completeness of the report or web site. It is not our intent to provide professional tax, investment, or legal advice. The strategies contained herein may not be suitable for your situation. You should consult with a professional where appropriate. This report should be used only as a general guideline and not as the ultimate source of information about Social Security claiming strategies. 
        </p>
        <p>
            The content of this report is intended to be informational and educational and DOES NOT constitute financial advice. We strongly recommend that you seek the advice of a tax, legal, and financial services professional before making decisions related to any investment or other financial decisions. Neither Filtech nor its development partners is responsible for the consequences of any decisions or actions taken in reliance upon or as a result of the information provided in this report or web site.
        </p>
        <p>
            The text includes information about some of the tax consequences of Social Security benefits. While nothing contained within should be constructed as tax advice, any tax-related information is not intended or written to be used, and cannot be used, for the purpose of avoiding penalties under the Internal Revenue Code or promoting, marketing, or recommending to another party any transaction or matter addressed in this report.
        </p>
        <p>
            Neither Filtech nor its development partners can be held responsible for any direct or incidental loss resulting from applying any of the information provided herein, or from any other source mentioned. The information provided does not constitute any legal, tax, or accounting advice.
        </p>
    </div>
     

     <script type="text/javascript">
         //Google Analytics
         (function (i, s, o, g, r, a, m) {
             i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                 (i[r].q = i[r].q || []).push(arguments)
             }, i[r].l = 1 * new Date(); a = s.createElement(o),
             m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
         })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
         ga('create', 'UA-88761127-1', 'auto');
         ga('send', 'pageview');
    </script>
</asp:Content>
