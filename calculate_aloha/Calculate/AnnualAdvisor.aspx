﻿<%@ Page Language="C#" Title="Annual Calculator" AutoEventWireup="true" CodeBehind="AnnualAdvisor.aspx.cs" Inherits="Calculate.AnnualAdvisor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <%--<link rel="stylesheet" media="(max-width:600px)" href="/css/smalldevice.css">--%>
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap-theme.css" rel="stylesheet" />
    <link href="/css/bootstrap-3.2.0-dist/css/bootstrap-dialog.css" rel="stylesheet" />
    <link href="/css/starter-template.css" rel="stylesheet" />
    <title>Log in</title>
    <link rel="stylesheet" type="text/css" href="/css/main.css" />
    <link rel="stylesheet" type="text/css" href="/css/forms.css" />
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="/css/interior.css" />
    <link rel="stylesheet" type="text/css" href="/css/jquery-te-1.4.0.css" />
    <link href="/Styles/Calculate.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/css/ui-lightness/jquery-ui-1.10.3.custom.css" />
    <script src="../Scripts/jquery-1.7.2.min.js"></script>
    <script src="../Scripts/hashchange.min.js"></script>
    <script>var $1_7_1 = jQuery.noConflict();</script>
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/blockUI.min.js"></script>
    <script type="text/javascript" src="../Scripts/common.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />

    <%--  <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>--%>

    <!-- Include all compiled plugins (below), or include individual files as needed -->

    <script src="/css/bootstrap-3.2.0-dist/js/bootstrap.min.js"></script>
    <script src="/css/bootstrap-3.2.0-dist/js/bootstrap-dialog.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#loginLink").click(function (e) {
                //$('label[id*="lblEmailError"]').text('');
                //$('label[id*="lblPwdError"]').text('');
                ShowDialog(true);
                e.preventDefault();
            });

        });


        //Processing Window
        ijQuery(window).load(function () {
            var notification_loader;

            ijQuery('body').hide().show();

            notification_loader = ijQuery('.notification-loader');
            notification_loader.attr('src', notification_loader.attr('rel'));
        });

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ScriptRegistration" EnablePageMethods="true" runat="server" ScriptMode="Release">
        </ajaxToolkit:ToolkitScriptManager>
        <div>
            <div id="overlay" class="web_dialog_overlay_Pricing">
                <div class="web_dialog_Single_Use" style="display: block;">
                    <div class="row">
                        <div id="color-overlay_singleUse">
                        </div>
                        <div>
                            <div class="col-md-12 ">
                                <div class="login-symbols_register">
                                    <table>
                                        <tr>
                                            <td>
                                                <div>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <img src="/Images/Newlogo2.png" style="height: 50px; width: 50px;" />
                                                            </td>
                                                            <td>
                                                                <div style="margin-top: 10px;">
                                                                    <h1>
                                                                        <p style="font-family: 'Times New Roman'; font-size: 24px;">
                                                                            <font color="#2f2e2e">THE PAID TO WAIT</font>
                                                                        </p>
                                                                    </h1>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </div>
                                            </td>


                                        </tr>

                                        <tr>
                                            <td>
                                                <div>
                                                    <p style="line-height: 45px; padding-bottom: 90px; font-size: 29px; margin-left: 7px;">
                                                        <font color="#2f2e2e">SOCIAL SECURITY CALCULATOR</font>
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-2">
                        </div>

                        <div>
                            <div class="col-md-3 Annual_Monthly_Advisor_div1">
                                <div style="width: 355px; height: 750px; background-color: #cac5c5; border-radius: 0px 0px 0px 0px; opacity: 1; filter: alpha(opacity=100); background-repeat: repeat; background-size: cover; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-position: top left;">

                                    <div class="contents">
                                        <h1>
                                            <p style="text-align: center; line-height: 50.4px; font-size: 36px;">PROFESSIONAL ANNUAL PLAN</p>
                                        </h1>
                                    </div>

                                    <div class="contents">
                                        <p style="text-align: center; line-height: 22.4px; font-size: 16px;">Ideal For Financial Professionals </p>
                                        <p style="text-align: center; line-height: 0px; font-size: 16px;">$395 per year</p>
                                    </div>

                                    <div style="height: 12px; padding-left: 136px; padding-top: 5px; padding-bottom: 30px; width: 200px;">
                                        <div class="line-horizontal" style="width: auto; height: 10px; border-bottom: 2px solid #ffffff;"></div>
                                    </div>

                                    <div class="contents">
                                        <ul style="line-height: 22.4px; font-size: 16px;">
                                            <li>Single time use of Social Security calculator.</li>
                                        </ul>
                                        <ul style="line-height: 22.4px; font-size: 16px;">
                                            <li>Generate PDF-Reports for clients with easy to understand step by step instructions telling them what benefits to claim and exactly when to claim them.</li>
                                        </ul>
                                        <ul style="line-height: 22.4px; font-size: 16px;">
                                            <li>Create unlimited client accounts, store their basic information and strategies online.</li>
                                        </ul>
                                        <ul style="line-height: 22.4px; font-size: 16px;">
                                            <li>Send PDF Social Security reports directly to clients.</li>
                                        </ul>
                                    </div>

                                    <div class="contents">
                                        <img src="//storage.googleapis.com/instapage-user-media/2e6aa360/8158688-0-Screenshot-2016-09-2.png" style="height: 116px; width: 82px; margin-top: 23px; margin-left: 39px">
                                    </div>

                                    <div class="contents">
                                        <p style="line-height: 22.4px; font-size: 16px; padding-top: 29px; margin-left: 39px">Download Sample report</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-1">
                            <div class="contents">
                                <img src="/Images/arrow-pen.png" style="margin-left: 30px; margin-top: 134px;" alt="">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <img src="/Images/B-Doherty Logo.png" style="height: 42px; width: 196px; margin-right: 44px;">
                            <div class="contents">
                                <p style="line-height: 22.4px; font-size: 16px;">Step 1 of 2. Fill out the form to create your account. </p>
                            </div>
                            <div>
                                <asp:TextBox ID="txtFirstName" placeholder="First Name" runat="server" CssClass="textBoxSingleUse"></asp:TextBox>
                                <br />
                                <asp:TextBox ID="txtEmailAddress" placeholder="Email" runat="server" CssClass="textBoxSingleUse"></asp:TextBox>
                                <br />
                                <asp:TextBox ID="txtPassword" TextMode="Password" placeholder="Password" runat="server" CssClass="textBoxSingleUse"></asp:TextBox>
                                <br />
                                <asp:Label ID="lblconfirmpasswordinstruct" Style="font-size: 11px; color: inactivecaptiontext;" Text="(Password must be between 6-32 characters)" runat="server"></asp:Label>

                                <br />
                                <asp:TextBox ID="txtConfirmPassword" TextMode="Password" placeholder="Confirm Password" runat="server" CssClass="textBoxSingleUse"></asp:TextBox>

                                <br />
                                <br />
                                <asp:Button runat="server" OnClientClick="return ValidateRgistrationDetails();" CssClass="btnSingleUse" ValidationGroup="pnl1Validation" OnClick="GetStarted" Text="Get Started" />
                                <br />
                                <asp:Label ID="lblUserExist" runat="server" CssClass="failureErrorNotification" Font-Size="1em" ForeColor="Red" Visible="False"></asp:Label>
                                <br />
                                <asp:Label ID="lblUserCreate" CssClass="setGreen" runat="server" Visible="False"></asp:Label>
                                <div class="contents">
                                    <a href="/Login.aspx" style="line-height: 22.4px; font-size: 16px; padding-top: 10px"><u>Already have an account?</u> </a>
                                </div>
                            </div>
                        </div>

                        <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>

                        <div class="col-md-2">
                            <div class="contents">
                                <div id="errordiv" class="RegistrationErrorWindow" style="display: none; margin-top: 104px !important">
                                    <div class="email-form-messagebox-header">Please Fix These Errors</div>
                                    <div class="email-form-messagebox" style="background-color: white; border-radius: 5px;">
                                        <span>
                                            <asp:Label ID="lblFirstNameError" ForeColor="Red" Font-Size="12px" runat="server" Visible="true"></asp:Label>
                                        </span>
                                        <span>
                                            <asp:Label ID="lblEmailError" ForeColor="Red" Font-Size="12px" runat="server" Visible="true"></asp:Label>
                                        </span>
                                        <span>
                                            <asp:Label ID="lblPwdError" ForeColor="Red" Font-Size="12px" runat="server" Text="" Visible="true"></asp:Label>
                                        </span>
                                        <span>
                                            <asp:Label ID="lblConfirmPwdError" ForeColor="Red" Font-Size="12px" runat="server" Text="" Visible="true"></asp:Label>
                                        </span>
                                        <asp:RegularExpressionValidator ID="revCustomerPassword" ForeColor="Red" Font-Size="12px" runat="server" ControlToValidate="txtPassword" CssClass="failureErrorNotification" ErrorMessage="Password length must be in between 6-32" ValidationExpression="^.{6,32}$" ValidationGroup="pnl1Validation" Display="Dynamic">- Password must be between 6-32 characters </asp:RegularExpressionValidator>
                                        <asp:RegularExpressionValidator ID="regtxtConfirmCustomerPassword" ForeColor="Red" Font-Size="12px" runat="server" ControlToValidate="txtConfirmPassword" CssClass="failureErrorNotification" ErrorMessage="Confirm Password length must be in between 6-32" ValidationExpression="^.{6,32}$" ValidationGroup="pnl1Validation" Display="Dynamic"><br />- Confirm Password must be between 6-32 characters </asp:RegularExpressionValidator><br />

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="notification" style="display: none;">
                    <div class="notification-overlay"></div>
                    <div class="notification-inner" style="left: 325px;">
                        <img rel="//storage.googleapis.com/instapage-app-assets/336/img/loading_circle.svg" src="//storage.googleapis.com/instapage-app-assets/336/img/loading_circle.svg" class="loading notification-loader" alt="" style="display: none;">
                        <span class="message">Processing...</span>
                        <%-- <span class="close-button" onclick="jQuery(this).parent().parent().hide()">Close</span>--%>
                    </div>
                </div>
            </div>

            <asp:Panel ID="pnlShowMsg" CssClass="MailSentPopUpModal" runat="server">
                <div class="modal-body">
                    <br />
                    <asp:Label runat="server" Text="Your account has been created successfully!" ID="lblMessage"></asp:Label>
                    <br />
                    <br />
                    <asp:Button runat="server" ID="btnOk" OnClick="RedirectToChargify" CssClass="button_ok_poopup" Text="OK" />
                    <br />
                </div>
            </asp:Panel>

            <asp:LinkButton ID="lnkFake1" runat="server"></asp:LinkButton>
            <cc1:ModalPopupExtender ID="MailSentPopUp" runat="server" BehaviorID="MsgPopUp" PopupControlID="pnlShowMsg" TargetControlID="lnkFake1" BackgroundCssClass="modalBackground">
            </cc1:ModalPopupExtender>


            <div class="footerbg" style="padding-top: 0px">
                <div>
                    <div class="clearfix">
                        <div class="col-md-12 column removepaddingleftright">
                            <div id="Div1" class="divFoot" runat="server">
                                <table class="table-responsive">
                                    <tr class="col-md-12 col-sm-12">
                                        <td class="col-md-2 col-sm-2"></td>
                                        <td class="col-md-3 col-sm-3">
                                            <div id="footer-left" style="line-height: 22.4px; padding-top: 25px; font-size: 16px;">
                                                &copy; 2017 Filtech, LLC. All Rights Reserved.
                                                <br />
                                                3056 New Williamsburg Dr Schenectady NY, 12303
                                            <br />
                                                Website developed by <a style="color: Highlight" href="http://www.alohatechnology.com/"><u>Aloha Technology</u></a>&nbsp;&nbsp;and&nbsp;&nbsp;<a style="color: Highlight" href="http://www.ictusmg.com/"><u>Ictus Marketing Group</u></a>
                                                <br />
                                                <p style="padding-top: 10px;">
                                                    <%if (Session["UserID"] != null)
                                                      {%>
                                                    <a href="../FrmCancelPolicy.aspx" style="color: white"><u>Cancellation and Refund Policy </u></a>&nbsp; &nbsp; &nbsp; <a href="../FrmPrivacyPolicy.aspx" style="color: white"><u>Privacy Policy </u></a>
                                                    <%}
                                                      else
                                                      { %>
                                                    <a href="../CancelPolicy.aspx" style="color: white"><u>Cancellation and Refund Policy </u></a>&nbsp; &nbsp; &nbsp; <a href="../PrivacyPolicy.aspx" style="color: white"><u>Privacy Policy </u></a>
                                                    <%} %>
                                                </p>
                                                <br />
                                            </div>
                                        </td>
                                        <td class="col-md-2 col-sm-2">
                                            <table style="width: 100px">
                                                <tr>
                                                    <td>
                                                        <a href="https://www.linkedin.com/in/brian-doherty-4359386a">
                                                            <img src="/Images/linkedinicon.png" class="img-responsive" />
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a href="https://www.facebook.com/Brian-Doherty-624778240952341/">
                                                            <img src="/Images/fbicon.png" class="img-responsive" />
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <a href="https://twitter.com/BrianDoherty57">
                                                            <img src="/Images/twittericon.png" class="img-responsive" />
                                                        </a>

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>

                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div>
    </form>
</body>
</html>
