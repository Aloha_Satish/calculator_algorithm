﻿using Calculate.RestrictAppIncome;
using CalculateDLL;
using Symbolics;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

namespace Calculate
{
    public class Calc
    {
        #region VARIABLE declaration
        static readonly Color LOW_GREY = Color.Gray;
        static readonly Color Back_Color = Color.Brown;
        DateTime limit = DateTime.Parse("05/01/1950");
        DateTime limit1 = DateTime.Parse("01/01/1954");
        TimeSpan spanWife, spanHusb;
        int intHusbAge, intWifeAge, intMonthsWife, intMonthsHusb = 0;
        static readonly string HIGHLIGHT_CLASS = Constants.HIGHLIGHT_70;
        const int FONT_SIZE = 10;
        decimal result, halfBenHusb, halfbenWife = 0;
        decimal multiply, tempbenefit;
        bool at70, buildGridAt70, FbuildGridAt70, fBuildGridat70, fBuildGridat70SpousalWife, fBuildGridat70SpousalHusb, combinedBenefts, dat70, wat66, swat66, BenefitAt70Change, BenefitAt66Change = false;
        string workingBenefitsForHusb = string.Empty, workingBenefitsForWife = string.Empty, DivorceSpousalBenefitChange = string.Empty;
        bool showWorkingBenefitsForHusb = false, showWorkingBenefitsForWife = false, isHusbSpouse = false, isWifeSpouse = false;
        bool BoolHusbandAgePDF = false, BoolWifeAgePDF = false, BoolWifeDoubleAsterisk = false, BoolHusbDoubleAsterisk = false, BoolWifeSingleAsterisk = false, BoolHusbSingleAsterisk = false;// BoolWifeProirAge = false, BoolHusbProirAge = false;
        int SpousalBenefitsDivorce = 0, intWifeYearsMax = 0, intHusbandYearsMax = 0;
        string newCombinedOncomeAgeDivorce = string.Empty;
        #endregion VARIABLE declaration

        #region Input Properties
        //current age of Husband
        public int intAgeHusb { get; set; }
        //date of birth for Husband
        public DateTime HusbDataofBirth { get; set; }
        //current age of wife
        public int intAgeWife { get; set; }
        //number of months from DOB of husband
        public int intAgeHusbMonth { get; set; }
        //number of months from DOB of wife
        public int intAgeWifeMonth { get; set; }
        //number of months from DOB of husband
        public int intCurrentAgeHusbMonth { get; set; }
        //number of months from DOB of wife
        public int intCurrentAgeWifeMonth { get; set; }
        //number of months from DOB of husband
        public int intCurrentAgeHusbYear { get; set; }
        //number of months from DOB of wife
        public int intCurrentAgeWifeYear { get; set; }
        //number of days from DOB of wife
        public int intAgeWifeDays { get; set; }
        //number of days from DOB of husb
        public int intAgeHusbDays { get; set; }
        //to save value at age 70 so taht it could be increases by 5 yrs in next itteration for wife
        public decimal currBeneWifeLocal { get; set; }
        //to save value at age 70 so taht it could be increases by 5 yrs in next itteration for husband
        public decimal currBeneHusbLocal { get; set; }
        //PIA entered for wife
        public decimal decBeneWife { get; set; }
        //PIA entered for HUsband
        public decimal decBeneHusb { get; set; }
        //current benefit of husband
        public decimal currbenehus { get; set; }
        //age when husband gets the first benefit
        public int husFirstPay { get; set; }
        //age when wife gets the first benefit
        public int wifeFirstPay { get; set; }
        //current benefits of wife
        public decimal currbenewife { get; set; }
        //COLA amt to increse the next benefit-by default it is 2.5%
        public decimal colaAmt { get; set; }
        public bool showSurvivor { get; set; }
        public bool badStrat { get; set; }
        //This is to set the wife's FRA year
        public int wifeFRA { get; set; }
        //This is to set the husband's FRA year
        public int husbFRA { get; set; }
        //This is to set the Firstname of user
        public string Your_Name { get; set; }
        //value for the age of the combined benefits when last change of the benefits takes place
        public string ageNumber { get; set; }
        //value for the combined benefits when last change of the benefits takes place
        public decimal lastBenefitsChangedAge70Value { get; set; }
        //This is to set the Firstname of user's Spouse
        public string Spouse_Name { get; set; }
        //This Method is for to set retirement year for Wife
        public int wifeFRAYear { get; set; }
        //This Method is for to set retirement year for Husband
        public int husbFRAYear { get; set; }
        //This Method is for to set Birth Date year for Husband
        public DateTime husbBirthDate { get; set; }
        //This Method is for to set Birth Date year for Wife
        public DateTime wifeBirthDate { get; set; }
        public string roundNote { get; set; }
        //To set the strategy count to be display
        public int intStrategyCount { get; set; }
        public static List<string> listPaidToWaitAmount = new List<string>();
        public static List<string> listAnnualAmount = new List<string>();
        public static List<string> listRestrictAppIcome = new List<string>();
        private static string _strStrategyToPdf1;
        public string strStrategyToPdf1
        {
            get
            {
                return _strStrategyToPdf1;
            }
            set
            {
                _strStrategyToPdf1 = value;
            }
        }
        private static string _strStrategyToPdf2;
        public string strStrategyToPdf2
        {
            get
            {
                return _strStrategyToPdf2;
            }
            set
            {
                _strStrategyToPdf2 = value;
            }
        }
        private static string _strStrategyToPdf3;
        public string strStrategyToPdf3
        {
            get
            {
                return _strStrategyToPdf3;
            }
            set
            {
                _strStrategyToPdf3 = value;
            }
        }

        private int mBirthYearHusb;
        /// <summary>
        /// Code to set the FRA year for husband for all calculations
        /// </summary>
        public int birthYearHusb
        {
            get { return mBirthYearHusb; }
            set
            {
                mBirthYearHusb = value; if (mBirthYearHusb < 1958 && mBirthYearHusb > 1937) husbFRA = 66; else husbFRA = 67;
                if (mBirthYearHusb == 1955) roundNote += "Husband's Full Retirement Age is 66 years 2 months; rounded down to 66.  ";
                if (mBirthYearHusb == 1956) roundNote += "Husband's Full Retirement Age is 66 years 4 months; rounded down to 66.  ";
                if (mBirthYearHusb == 1957) roundNote += "Husband's Full Retirement Age is 66 years 6 months; rounded down to 66.  ";
                if (mBirthYearHusb == 1958) roundNote += "Husband's Full Retirement Age is 66 years 8 months; rounded up to 67.  ";
                if (mBirthYearHusb == 1959) roundNote += "Husband's Full Retirement Age is 66 years 10 months; rounded up to 67.  ";
                roundNote = roundNote.Replace(".  Husband", ".\r\nHusband");
            }
        }

        private int mBirthYearWife;
        /// <summary>
        /// code to set the FRA year of Wife for all calcualtions
        /// </summary>
        public int birthYearWife
        {
            get { return mBirthYearWife; }
            set
            {
                mBirthYearWife = value; if (mBirthYearWife < 1958 && mBirthYearWife > 1937) wifeFRA = 66; else wifeFRA = 67;
                if (mBirthYearWife == 1955) roundNote += "Wife's Full Retirement Age is 66 years 2 months; rounded down to 66.  ";
                if (mBirthYearWife == 1956) roundNote += "Wife's Full Retirement Age is 66 years 4 months; rounded down to 66.  ";
                if (mBirthYearWife == 1957) roundNote += "Wife's Full Retirement Age is 66 years 6 months; rounded down to 66.  ";
                if (mBirthYearWife == 1958) roundNote += "Wife's Full Retirement Age is 66 years 8 months; rounded up to 67.  ";
                if (mBirthYearWife == 1959) roundNote += "Wife's Full Retirement Age is 66 years 10 months; rounded up to 67.  ";
            }
        }

        /// <summary>
        /// To Calculate FRA as per Client New formulae given
        /// </summary>
        /// <param name="BirthYear"></param>
        /// <returns></returns>
        public float FRA(int BirthYear)
        {
            float FRA_Age;
            if (BirthYear >= 1900 && BirthYear <= 1937)
                BirthYear = 1937;
            if (BirthYear >= 1943 && BirthYear <= 1954)
                BirthYear = 1954;
            if (BirthYear >= 1960 && BirthYear <= 2000)
                BirthYear = 2000;
            switch (BirthYear)
            {
                case 1937:
                    FRA_Age = 65;
                    break;
                case 1938:
                    FRA_Age = 65.17f;
                    break;
                case 1939:
                    FRA_Age = 65.33f;
                    break;
                case 1940:
                    FRA_Age = 65.5f;
                    break;
                case 1941:
                    FRA_Age = 65.67f;
                    break;
                case 1942:
                    FRA_Age = 65.83f;
                    break;
                case 1954:
                    FRA_Age = 66;
                    break;
                case 1955:
                    FRA_Age = 66.17f;
                    break;
                case 1956:
                    FRA_Age = 66.33f;
                    break;
                case 1957:
                    FRA_Age = 66.50f;
                    break;
                case 1958:
                    FRA_Age = 66.67f;
                    break;
                case 1959:
                    FRA_Age = 66.83f;
                    break;
                case 2000:
                    FRA_Age = 67;
                    break;
                default:
                    FRA_Age = 0;
                    break;
            }
            return FRA_Age;
        }

        #endregion Input Properties

        #region Output Propertise
        public int cumm65 { get; set; }
        public int odlgerGreater { get; set; }
        //cummulative value at age 69
        public decimal cumm69 { get; set; }
        //value at the age 70
        public decimal valueAtAge70 { get; set; }
        public decimal ClaimAndSuspendinFullValue { get; set; }
        public string ClaimAndSuspendinFullAge { get; set; }
        public string CombinedIncomeAge { get; set; }
        public string AgeValueForHigherEarner70 { get; set; }
        public decimal ValueForHigherEarner70 { get; set; }
        public decimal ValueForWife70 { get; set; }
        public decimal ValueForHusb70 { get; set; }
        public string AgeForWife70 { get; set; }
        public string AgeForHusb70 { get; set; }
        public string AgeValueForWifeAt70 { get; set; }
        public decimal AgeValueForSpousalAt70 { get; set; }
        public decimal ValueForWifeAt70 { get; set; }
        public decimal BenefitsValueForWifeAt70 { get; set; }
        //starting benefit value for husband
        public decimal startingBenefitsValueHusb { get; set; }
        public string startingAgeValueHusb { get; set; }
        public string startingAgeValueWife { get; set; }
        //starting benefit value for wife
        public decimal startingBenefitsValueWife { get; set; }
        //value at the age 69
        public decimal valueAtAge69 { get; set; }
        //show 3 strategies when higher earner age 70 value is greater
        public decimal higherEarnerValueAtAge70 { get; set; }
        public bool BoolAboveFRAageStep2 { get; set; }
        //surviour benefits value at age 66
        public decimal surviourValueAtAge66 { get; set; }
        //working benefits value at age 66
        public decimal workingValueAtAge66 { get; set; }
        public string AgeWhenHigherEarnerClaimandSuspend { get; set; }
        //surviour benefits value at age 70
        public decimal surviourValueAtAge70 { get; set; }
        //working benefits value at age 66
        public decimal workingValueAtAge70 { get; set; }
        //cummulative value for single at age 69
        public decimal SCummValue { get; set; }
        //value to get the anula income for single at paid to wait age
        public decimal SingleAnualValue { get; set; }
        public string SAgeValue { get; set; }
        public decimal AgeValueAt70 { get; set; }
        public string AgeAtLastChange { get; set; }
        public decimal ValueAtLastChange { get; set; }
        public static string AnnualIncomeAge { get; set; }
        public decimal SValueStarting { get; set; }
        //cummulative value for widow at age 69
        public decimal WCummValue { get; set; }
        public decimal WidowAnnualIncomeValue { get; set; }
        public decimal DivorceSpousalBenefits { get; set; }
        public string DivorceSpousalAge { get; set; }
        //cummulative value for Divorce at age 69
        public decimal DCummValue { get; set; }
        //anual income value for divorce at paid to wait age
        public decimal DivorceAnnualValue { get; set; }
        public int cumm80 { get; set; }
        public int annual70 { get; set; }
        public string age70SurvBeneAge { get; set; }
        public decimal age70SurvBene { get; set; }
        public int age70HighBene { get; set; }
        public int age70CombBene { get; set; }
        public int age80CombBene { get; set; }
        public int[] breakEvenAmt { get; set; }
        public string[] breakEvenAge { get; set; }
        public int gageDiff { get; set; }
        public static int intSpousalBenefitAge { get; set; }

        public string strRestriCummValue = string.Empty, strRestriCombValue = string.Empty;
        #endregion Output Propertise

        #region Enums

        //  Display columns

        // Date : 20112014
        // Description : Add Extra Column "YEAR" in Grid.
        // old Commented 
        //enum disp_col { Age, WifeInc, HusbInc, CombInc, AnnInc, CummInc, SurvInc };
        enum disp_col { Age, WifeInc, HusbInc, CombInc, AnnInc, CummInc, SurvInc };// ,Year

        // Date : 20112014
        // Description : Add Extra Column "YEAR" in Grid.
        // old Commented 
        // enum disp_cols { Age, WifeInc, AnnInc, CummInc };
        enum disp_cols { Age, WifeInc, AnnInc, CummInc };

        /// <summary>
        /// Different charts use different strategies 
        /// </summary>
        public enum calc_strategy { Standard, Spousal, Suspend, WidowedSelf, Widowed66, Widowed70, Claim67, Max, ZeroBenefit };

        /// <summary>
        /// Restricted Application Strategy depends on spouse starting his/her benefit at specific age
        /// </summary>
        public enum calc_restrict_strategy { SingleStrategy, RestrOptionAtCurrent, At70, AtFra, At62, Additional1, Additional2, NoClaim62, NoClaim66 };

        /// <summary>
        /// 4 kids of Solo calcs
        /// </summary>
        public enum calc_solo { Married, Single, Divorced, Widowed };

        /// <summary>
        /// Startpoint can 62, 66/67, or 70
        /// </summary>
        public enum calc_start { asap60, asap62, fra6667, max70, max };

        /// <summary>
        /// Benefit recieved could be based own own earnings or spouse's
        /// </summary>
        protected enum benefit_source { Self, Spouse, None, SpousalBenefit };

        #endregion  Enums

        #region Methods

        public Calc() { roundNote = string.Empty; }

        /// <summary>
        /// This routine builds Claim & Suspend For Marrried only   Date:31/12/2014
        /// </summary>
        /// <param name="Grid1"></param>
        /// <param name="strategy"></param>
        /// <param name="startAge"></param>
        /// <param name="gridNote"></param>
        /// <returns></returns>
        public int BuildGridSuspend(GridView Grid1, calc_strategy strategy, calc_start startAge, ref System.Web.UI.WebControls.Label gridNote, String gridname)
        {
            try
            {
                #region VARIABLE DECLARATION
                cumm65 = 0;
                cumm80 = 0;
                int husbFirstPayAge = 0, wifeFirstPayAge = 0;
                badStrat = false;
                int baseAge = 62;
                AgeWhenHigherEarnerClaimandSuspend = string.Empty;
                odlgerGreater = 0;
                decimal workingBenefitsat70Husb = 0; decimal workingBenefitsat70Wife = 0;
                Globals.Instance.intRestrictAppIncomeFullHusb = 0;
                Globals.Instance.intRestrictAppIncomeMaxHusb = 0;
                Globals.Instance.intRestrictAppIncomeFullWife = 0;
                Globals.Instance.intRestrictAppIncomeMaxWife = 0;
                #endregion

                #region CODE TO CALCULATE THE AGE AND SET THE FRA YEAR AND FIRST PAY YEAR
                //if date of birth of husb or wife is 1958 or 1959
                //changed due to starting values were showing at wrong age
                if ((birthYearWife == 1959 || birthYearWife == 1958) && (birthYearHusb == 1959 || birthYearHusb == 1958))
                {
                    wifeFRA = 66;
                    husbFRA = 66;
                }
                else if (birthYearWife == 1959 || birthYearWife == 1958)
                {
                    husbFRA = Convert.ToInt32(FRA(birthYearHusb));
                    wifeFRA = 66;
                }
                else if (birthYearHusb == 1959 || birthYearHusb == 1958)
                {
                    wifeFRA = Convert.ToInt32(FRA(birthYearWife));
                    husbFRA = 66;
                }
                else
                {
                    husbFRA = Convert.ToInt32(FRA(birthYearHusb));
                    wifeFRA = Convert.ToInt32(FRA(birthYearWife));
                }
                //to set the first pay age ie when the benefits will start
                switch (startAge)
                {
                    case calc_start.fra6667:
                        if (decBeneHusb == decBeneWife)
                        {
                            husbFirstPayAge = 70;
                            wifeFirstPayAge = 70;
                            baseAge = 70;
                            wifeFirstPay = 70;
                            husFirstPay = 70;
                        }
                        else if (decBeneHusb > decBeneWife)
                        {
                            husbFirstPayAge = 70;
                            wifeFirstPayAge = wifeFRA;
                            baseAge = wifeFRA;
                            wifeFirstPay = wifeFRA;
                            husFirstPay = 70;
                        }
                        else
                        {
                            husbFirstPayAge = husbFRA;
                            wifeFirstPayAge = 70;
                            baseAge = husbFRA;
                            wifeFirstPay = 70;
                            husFirstPay = husbFRA;
                        }
                        break;
                    case calc_start.max70:
                        baseAge = Math.Min(husbFRA, wifeFRA);
                        husbFirstPayAge = husbFRA;
                        wifeFirstPayAge = wifeFRA;
                        wifeFirstPay = wifeFRA;
                        husFirstPay = husbFRA;
                        break;
                }
                #endregion

                #region TO BUILD THE COLUMN NAMES FOR THE GRID
                disp_col dispCol;
                gridNote.Text = string.Empty;
                // Build dataset
                System.Data.DataSet incomeSet = new System.Data.DataSet();
                System.Data.DataTable incomeTab = incomeSet.Tables.Add("incomeTab");
                // Initialize grid
                Grid1.Columns.Clear();
                Grid1.AutoGenerateColumns = false;
                // Build Display Columns and Dataset Columns
                var values = (disp_col[])Enum.GetValues(typeof(disp_col));
                //to show initila of husband and wife in the grid's age columns
                string sWifeInitials = Your_Name.Substring(0, 1);
                string sHusbInitials = Spouse_Name.Substring(0, 1);
                for (int dcIx = 0; dcIx < values.Length; dcIx++)
                {
                    dispCol = (disp_col)dcIx;
                    incomeTab.Columns.Add(dispCol.ToString());
                    BoundField bf = new BoundField();
                    bf.HtmlEncode = false;
                    TableItemStyle tis = Grid1.HeaderStyle;
                    //to build the header names for grids
                    switch (dispCol)
                    {
                        //case disp_col.Year:
                        //        bf.HeaderText = "Year";
                        //    break;
                        case disp_col.Age:
                            //if (intAgeWife == intAgeHusb)
                            //    bf.HeaderText = "Age";
                            //else
                            bf.HeaderText = Constants.HeaderA + Constants.HeaderBreak + Constants.Age + Environment.NewLine + Constants.OpeningBracket + sWifeInitials + Constants.BackSlash + sHusbInitials + Constants.CloseingBracket;
                            break;
                        case disp_col.WifeInc:
                            bf.HeaderText = Constants.HeaderB + Constants.HeaderBreak + Your_Name + Constants.MonthlyIncome;
                            break;
                        case disp_col.HusbInc:
                            bf.HeaderText = Constants.HeaderC + Constants.HeaderBreak + Spouse_Name + Constants.MonthlyIncome;
                            break;
                        case disp_col.CombInc:
                            bf.HeaderText = Constants.HeaderD + Constants.HeaderBreak + Constants.CombinedMonthlyIncome;
                            break;
                        case disp_col.AnnInc:
                            bf.HeaderText = Constants.HeaderE + Constants.HeaderBreak + Constants.CombinedAnnualIncome;
                            break;
                        case disp_col.CummInc:
                            bf.HeaderText = Constants.HeaderF + Constants.HeaderBreak + Constants.CumulativeAnnualIncome;
                            break;
                        case disp_col.SurvInc:
                            bf.HeaderText = Constants.SurvivorMonthlyIncome;
                            break;
                    }
                    // Column details
                    bf.DataField = dispCol.ToString();
                    bf.ReadOnly = true;
                    // Skip the survivor column if not requested
                    if ((showSurvivor) || (dcIx != values.Length - 1))
                    {
                        Grid1.Columns.Add(bf);
                    }
                }
                incomeTab.Columns.Add("Note");

                #endregion

                #region CODE TO POPULATE THE ROWS AND ADDING AGES TO THE GRID
                //  Populate rows
                // Date : 20112014
                // Description : Add Extra Column "YEAR" in Grid.
                #region local variables
                int year = 0;
                int HigherPIA_Age62yr = 0, LowerPIA_FRA_Year = 0;
                int ageHusb = 0, ageWife = 0, ageDiff = 0;
                age70SurvBene = 0;
                age70SurvBeneAge = string.Empty;
                decimal currBene = 0, currBeneAnnual = 0, currAnnualBenefitsWife = 0, currAnnualBenefitsHusb = 0;
                decimal currBeneWife = 0, currBeneHusb = 0, cummBeneWife = 0, cummBeneHusb = 0, cummBene = 0;
                decimal spousalBene = 0;
                bool IsHusbLess70AtSpouse66 = false;

                DateTime datediffHusb66 = wifeBirthDate.AddYears(66);
                TimeSpan difftimespanHusb66 = datediffHusb66.Subtract(husbBirthDate);
                int yearHusb66 = (int)Math.Floor((double)difftimespanHusb66.Days / 365.2425);
                int monthsHusb66 = Convert.ToInt32(((double)difftimespanHusb66.Days / 30.436875));
                int multiplyHusb66 = monthsHusb66 - yearHusb66 * 12;
                if (yearHusb66 < 70)
                    IsHusbLess70AtSpouse66 = true;
                //bool needAsterisk = false;
                //bool needAsteriskForFullRetirement = false;



                benefit_source wifeBenefitSource = benefit_source.None;
                benefit_source husbBenefitSource = benefit_source.None;
                int youngest = Math.Min(intAgeHusb, intAgeWife);
                int oldest = Math.Max(intAgeHusb, intAgeWife);
                int ageSpan = oldest - youngest;
                #endregion local variables
                if (youngest == intAgeWife && decBeneHusb > decBeneWife)
                    ageDiff = ageSpan; // most common case
                else
                    ageDiff = 0 - ageSpan; // inverted
                breakEvenAmt = new int[100];
                breakEvenAge = new string[100];
                int highBE = 0;
                //To print Age According to New Calculation    Date:12/29/2014
                if (intAgeHusb > intAgeWife)
                {
                    year = birthYearHusb + 62;
                    youngest = intAgeWife;
                    oldest = intAgeHusb;
                }
                else
                {
                    year = birthYearWife + 62;
                    youngest = intAgeHusb;
                    oldest = intAgeWife;
                }

                if (decBeneWife > decBeneHusb)
                {
                    HigherPIA_Age62yr = birthYearWife + 62;
                    LowerPIA_FRA_Year = FRAYear(husbBirthDate);
                }
                else
                {
                    HigherPIA_Age62yr = birthYearHusb + 62;
                    LowerPIA_FRA_Year = FRAYear(wifeBirthDate);
                }
                youngest = 62 - ageSpan;
                //  Build the data rows and set age of wife and husband
                for (int aIx = youngest; aIx <= 90;)//int aIx = 62 - ageSpan; aIx < 99; aIx++)
                {
                    ageWife = aIx;
                    ageHusb = aIx;
                    if (intAgeWife < intAgeHusb)
                        ageHusb = aIx + intAgeHusb - intAgeWife;
                    else
                        ageWife = aIx + intAgeWife - intAgeHusb;

                    #endregion

                    #region CODE FOR FULL RETIREMENT AND CLAIM LATE
                    //  Spousal strategy for Full Retirement & Claim Late strategy
                    if (strategy == calc_strategy.Spousal)
                    {
                        #region code to switch from working benefits to spousal benefist if spousal are greater
                        if ((ageHusb > ageWife) && (decBeneWife > decBeneHusb) && wifeBirthDate < limit1)
                        {
                            //if age  of wife equals to FRA age of wife
                            if (ageWife == wifeFRA)
                            {
                                //code to check if age greater than FRA
                                if (intAgeHusb > husbFRA)
                                {
                                    //code to get the FRA value for Husb
                                    tempbenefit = AgeAbove68YearsOfHusb();
                                    //code to amt for delayed creits
                                    multiply = 8 * (ageHusb - husbFRA);
                                    multiply = multiply / 100;
                                    multiply = 1 + multiply;
                                    //at 70 value for husb
                                    currBeneHusb = tempbenefit * multiply;
                                    currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                    Globals.Instance.HusbStartingBenefitName = Constants.Working;
                                    //spousal benefits at husb age70 and wife at FRA
                                    if (wifeBenefitSource != benefit_source.Spouse)
                                    {
                                        currBeneWife = ColaUpSpousal(ageWife, tempbenefit, birthYearWife, intAgeWife, wifeFRA);
                                        currBeneWife = currBeneWife / 2;
                                        Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                        wifeBenefitSource = benefit_source.Spouse;
                                        wifeFirstPayAge = ageWife;
                                    }
                                    husbFirstPayAge = ageHusb;
                                    //changeToSpousalBenefist = false;
                                    husbBenefitSource = benefit_source.Self;
                                }
                                else
                                {
                                    //code to switch from working benefits to sposual at another spouse reaching FRA
                                    spousalBene = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA); // Change if COLA is YES 
                                    spousalBene /= 2;
                                    decimal ownSpousalBenefits = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                    ownSpousalBenefits = ownSpousalBenefits / 2;
                                    if (currBeneWife < spousalBene)
                                    {
                                        decimal intdiff = spousalBene - currBeneHusb;
                                        if (ownSpousalBenefits > intdiff)
                                        {
                                            //if own spousal benefits are greater assign the spousal benefits
                                            currBeneWife = ownSpousalBenefits;

                                            //Globals.Instance.ShowClaimAndSuspend = false;
                                            Globals.Instance.CurrHusValue = true;
                                            //needAsteriskForFullRetirement = true;
                                            //changeToSpousalBenefist = false;
                                            wifeFirstPayAge = ageWife;
                                            wifeBenefitSource = benefit_source.Spouse;
                                            husbBenefitSource = benefit_source.Self;
                                            Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                        }
                                        //code to change from working benefits to spousal benefits if they are greater at current age is equal to FRA age
                                        else
                                        {
                                            //currBeneHusb = spousalBene;
                                            ////if change is made we hide spousal benfits
                                            //Globals.Instance.ShowClaimAndSuspend = false;
                                            ////to highlight the changes number in the grid
                                            //Globals.Instance.CurrHusValue = true;
                                            ////show asterisk at FRA age of wife
                                            //needAsteriskForFullRetirement = true;
                                            //if (currBeneWife == 0)
                                            //    Globals.Instance.ShowAsterikForTwoStrategies = true;
                                            //changeToSpousalBenefist = false;
                                            //husbFirstPayAge = ageHusb;
                                            //husbBenefitSource = benefit_source.Spouse;
                                        }

                                    }
                                    else
                                    {
                                        //else continue with the own benefits
                                        Globals.Instance.HusbStartingBenefitName = Constants.Working;
                                        husbFirstPayAge = ageHusb;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if ((ageWife >= ageHusb) && (decBeneWife >= decBeneHusb) && wifeBirthDate < limit1 && (ageHusb == husbFRA))
                            {
                                spousalBene = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA); // Change if COLA is YES 
                                spousalBene /= 2;
                                if (currBeneHusb == 0 && wifeBenefitSource != benefit_source.Spouse)
                                {
                                    currBeneWife = spousalBene;
                                    Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                    wifeBenefitSource = benefit_source.Spouse;
                                    wifeFirstPayAge = ageWife;
                                }
                            }
                        }
                        if ((ageHusb < ageWife) && (decBeneWife < decBeneHusb) && husbBirthDate < limit1)
                        {
                            //if age  of husb equals to FRA age of husb
                            if (ageHusb == husbFRA)
                            {
                                //code to check if age greater than FRA
                                if (intAgeWife > wifeFRA)
                                {
                                    //code to get the FRA value for wife
                                    tempbenefit = AgeAbove68YearsOfWife();
                                    //code to amt for delayed creits
                                    multiply = 8 * (ageWife - wifeFRA);
                                    multiply = multiply / 100;
                                    multiply = 1 + multiply;
                                    //at 70 value for wife
                                    currBeneWife = tempbenefit * multiply;
                                    currBeneWife = ColaUpSpousal(ageHusb, currBeneWife, birthYearHusb, intAgeHusb, husbFRA);
                                    Globals.Instance.WifeStartingBenefitName = Constants.Working;
                                    //spousal benefits at wife age70 and husb at FRA
                                    if (husbBenefitSource != benefit_source.Spouse)
                                    {
                                        currBeneHusb = ColaUpSpousal(ageWife, tempbenefit, birthYearWife, intAgeWife, wifeFRA);
                                        currBeneHusb = currBeneHusb / 2;
                                        Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                        husbBenefitSource = benefit_source.Spouse;
                                        husbFirstPayAge = ageHusb;
                                    }
                                    wifeFirstPayAge = ageWife;
                                    //changeToSpousalBenefist = false;
                                    wifeBenefitSource = benefit_source.Self;
                                }
                                else
                                {
                                    //code to switch from working benefits to sposual at another spouse reaching FRA
                                    spousalBene = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA); // Change if COLA is YES 
                                    spousalBene /= 2;
                                    decimal ownSpousalBenefits = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA); // Change if COLA is YES 
                                    ownSpousalBenefits /= 2;
                                    if (currBeneHusb < spousalBene)
                                    {
                                        decimal intdiff = spousalBene - currBeneWife;
                                        if (ownSpousalBenefits > intdiff)
                                        {
                                            //if own spousal benefits are greater assign the spousal benefits
                                            currBeneHusb = ownSpousalBenefits;
                                            //highlight the changed value
                                            Globals.Instance.CurrWifeValue = true;
                                            //changeToSpousalBenefist = false;
                                            wifeBenefitSource = benefit_source.Self;
                                            husbBenefitSource = benefit_source.Spouse;
                                            wifeFirstPayAge = ageWife;
                                            husbFirstPayAge = ageHusb;
                                        }
                                        else
                                        {
                                            //currBeneWife = spousalBene;
                                            ////if change is made we hide spousal benfits
                                            //Globals.Instance.ShowClaimAndSuspend = false;
                                            ////highlight the changed value
                                            //Globals.Instance.CurrWifeValue = true;
                                            ////show asterik at FRA of husb
                                            //needAsteriskForFullRetirement = true;
                                            //changeToSpousalBenefist = false;
                                            //if (currBeneHusb == 0)
                                            //    Globals.Instance.ShowAsterikForTwoStrategies = true;
                                            //wifeBenefitSource = benefit_source.Spouse;
                                            //wifeFirstPayAge = ageWife;
                                        }
                                    }
                                    else
                                    {
                                        //else continue with the working benefits
                                        Globals.Instance.WifeStartingBenefitName = Constants.Working;
                                        wifeFirstPayAge = ageWife;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if ((ageHusb >= ageWife) && (decBeneWife <= decBeneHusb) && husbBirthDate < limit1 && (ageWife == wifeFRA))
                            {
                                spousalBene = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA); // Change if COLA is YES 
                                spousalBene /= 2;
                                if (currBeneHusb == 0 && husbBenefitSource != benefit_source.Spouse)
                                {
                                    currBeneHusb = spousalBene;
                                    Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                    husbBenefitSource = benefit_source.Spouse;
                                    husbFirstPayAge = ageHusb;
                                }
                            }
                        }
                        #endregion code to switch from working benefits to spousal benefist if spousal are greater

                        #region code to get the Full REtirement strategy benefits
                        if (decBeneWife <= decBeneHusb)
                        {
                            if ((ageHusb >= husbFirstPayAge) && (ageHusb < 70) && (ageWife >= wifeFRA) && (husbBenefitSource == benefit_source.None) && wifeBirthDate < limit1)
                            {
                                //code to get spousal benefits for husband
                                if (intAgeHusb >= 62)
                                {
                                    //code to check if age greater than FRA
                                    if (intAgeWife > wifeFRA)
                                    {
                                        //code to get FRA value for wife
                                        tempbenefit = AgeAbove68YearsOfWife();
                                        //including the COLA amt for spousal benefits for husb
                                        spousalBene = ColaUpSpousal(ageWife, tempbenefit, birthYearWife, intAgeWife, wifeFRA);
                                        spousalBene = spousalBene / 2;
                                        decimal workingBenefitsat70 = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                                        workingBenefitsat70 = ColaUpSpousal(70, workingBenefitsat70, birthYearHusb, intAgeHusb, husbFRA);
                                        int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                        decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                        spousalBenefits = spousalBenefits / 2;
                                        if (workingBenefitsat70 < spousalBenefits)
                                        {
                                            if (spousalBene > currBeneHusb)
                                            {
                                                currBeneHusb = spousalBene;
                                                Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                husbFirstPayAge = ageHusb;
                                                husbBenefitSource = benefit_source.Spouse;
                                            }
                                        }
                                        else
                                        {
                                            if (husbBirthDate < limit1)
                                            {
                                                if (spousalBene > currBeneHusb)
                                                {
                                                    currBeneHusb = spousalBene;
                                                    Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                    husbFirstPayAge = ageHusb;
                                                    husbBenefitSource = benefit_source.Spouse;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //spousal benefits if age less than FRA for husb
                                        spousalBene = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);//Change if COLA is YES 
                                        spousalBene = spousalBene / 2;
                                        decimal workingBenefitsat70 = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                                        workingBenefitsat70 = ColaUpSpousal(70, workingBenefitsat70, birthYearHusb, intAgeHusb, husbFRA);
                                        int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                        decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                        spousalBenefits = spousalBenefits / 2;
                                        if (workingBenefitsat70 < spousalBenefits)
                                        {
                                            if (spousalBene > currBeneHusb)
                                            {
                                                currBeneHusb = spousalBene;
                                                Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                husbFirstPayAge = ageHusb;
                                                husbBenefitSource = benefit_source.Spouse;
                                            }
                                        }
                                        else
                                        {
                                            if (husbBirthDate < limit1)
                                            {
                                                if (spousalBene > currBeneHusb)
                                                {
                                                    currBeneHusb = spousalBene;
                                                    Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                    husbFirstPayAge = ageHusb;
                                                    husbBenefitSource = benefit_source.Spouse;
                                                }
                                            }
                                        }
                                    }
                                }
                                wifeFRAYear = FRAYear(wifeBirthDate);
                            }
                            //code to get benefits for wife
                            if ((ageWife >= intAgeWife) && (ageWife == wifeFirstPayAge))
                            {
                                //code to check if age greater than FRA
                                if (intAgeWife > wifeFRA && wifeBenefitSource != benefit_source.Self)
                                {
                                    //give the PIA as benefits
                                    currBeneWife = decBeneWife;
                                    wifeFirstPayAge = ageWife;
                                    wifeBenefitSource = benefit_source.Self;
                                    Globals.Instance.WifeStartingBenefitName = Constants.Working;
                                }
                                else
                                {
                                    decimal temp, spousalbenefitatage;
                                    decimal tempbene = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                    if (intAgeHusb > husbFRA)
                                    {
                                        temp = AgeAbove68YearsOfHusb();
                                        spousalbenefitatage = ColaUpSpousal(ageHusb, temp, birthYearHusb, intAgeHusb, husbFRA);
                                    }
                                    else
                                    {
                                        spousalbenefitatage = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                    }
                                    spousalbenefitatage = spousalbenefitatage / 2;
                                    decimal workingBenefitsat70 = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                                    workingBenefitsat70 = ColaUpSpousal(70, workingBenefitsat70, birthYearWife, intAgeWife, wifeFRA);
                                    int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                    decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                    spousalBenefits = spousalBenefits / 2;
                                    if (husbBenefitSource == benefit_source.Self && spousalbenefitatage > tempbene && wifeBenefitSource != benefit_source.Spouse)
                                    {
                                        if (workingBenefitsat70 < spousalBenefits)
                                        {
                                            if (spousalbenefitatage > currBeneWife)
                                            {
                                                currBeneWife = spousalbenefitatage;
                                                wifeFirstPayAge = ageWife;
                                                wifeBenefitSource = benefit_source.Spouse;
                                                Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                            }
                                        }
                                        else
                                        {
                                            if (wifeBirthDate < limit1)
                                            {
                                                if (spousalbenefitatage > currBeneWife)
                                                {
                                                    currBeneWife = spousalbenefitatage;
                                                    wifeFirstPayAge = ageWife;
                                                    wifeBenefitSource = benefit_source.Spouse;
                                                    Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (currBeneWife < tempbene && wifeBenefitSource != benefit_source.Self)
                                        {
                                            currBeneWife = tempbene;
                                            wifeFirstPayAge = ageWife;
                                            wifeBenefitSource = benefit_source.Self;
                                            Globals.Instance.WifeStartingBenefitName = Constants.Working;

                                            //Calculate spousal benefit for husband at spouse age 66
                                            decimal decHusbSpousalBene = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA); // Change if COLA is YES 
                                            decHusbSpousalBene /= 2;
                                            Globals.Instance.HusbSpousalBenefitWhenWife66 = decHusbSpousalBene;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if ((ageWife >= wifeFirstPayAge) && (ageWife < 70) && (ageHusb >= husbFRA) && (wifeBenefitSource == benefit_source.None) && husbBirthDate < limit1)
                            {
                                //code to get spousal benefits for wife
                                if (intAgeWife >= 62)
                                {
                                    //code to check if age greater than FRA
                                    if (intAgeHusb > husbFRA)
                                    {
                                        //code to get value at FRA for husb                                 
                                        tempbenefit = AgeAbove68YearsOfHusb();
                                        spousalBene = ColaUpSpousal(ageHusb, tempbenefit, birthYearHusb, intAgeHusb, husbFRA); // Change if COLA is YES 
                                        spousalBene = spousalBene / 2;
                                        decimal workingBenefitsat70 = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                                        workingBenefitsat70 = ColaUpSpousal(70, workingBenefitsat70, birthYearWife, intAgeWife, wifeFRA);
                                        int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                        decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                        spousalBenefits = spousalBenefits / 2;
                                        if (spousalBenefits > workingBenefitsat70)
                                        {
                                            if (currBeneWife < spousalBene)
                                            {
                                                currBeneWife = spousalBene;
                                                wifeFirstPayAge = ageWife;
                                                wifeBenefitSource = benefit_source.Spouse;
                                                Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                            }
                                        }
                                        else
                                        {
                                            if (wifeBirthDate < limit1)
                                            {
                                                if (currBeneWife < spousalBene)
                                                {
                                                    currBeneWife = spousalBene;
                                                    wifeFirstPayAge = ageWife;
                                                    wifeBenefitSource = benefit_source.Spouse;
                                                    Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        spousalBene = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA); // Change if COLA is YES 
                                        spousalBene = spousalBene / 2;
                                        decimal workingBenefitsat70 = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                                        workingBenefitsat70 = ColaUpSpousal(70, workingBenefitsat70, birthYearWife, intAgeWife, wifeFRA);
                                        int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                        decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                        spousalBenefits = spousalBenefits / 2;
                                        if (spousalBenefits > workingBenefitsat70)
                                        {
                                            if (currBeneWife < spousalBene)
                                            {
                                                currBeneWife = spousalBene;
                                                wifeFirstPayAge = ageWife;
                                                wifeBenefitSource = benefit_source.Spouse;
                                                Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                            }
                                        }
                                        else
                                        {
                                            if (wifeBirthDate < limit1)
                                            {
                                                if (currBeneWife < spousalBene)
                                                {
                                                    currBeneWife = spousalBene;
                                                    wifeFirstPayAge = ageWife;
                                                    Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                    wifeFirstPayAge = ageWife;
                                                    wifeBenefitSource = benefit_source.Spouse;
                                                }
                                            }
                                        }
                                    }
                                }
                                husbFRAYear = FRAYear(husbBirthDate);
                            }
                            //code to get benefits for husband
                            if ((ageHusb >= intAgeHusb) && (ageHusb == husbFirstPayAge))
                            {
                                //code to check if age greater than FRA
                                if (intAgeHusb > husbFRA)
                                {
                                    //give husb PIA as the value
                                    currBeneHusb = decBeneHusb;
                                    husbBenefitSource = benefit_source.Self;
                                    husbFirstPayAge = ageHusb;
                                    Globals.Instance.HusbStartingBenefitName = Constants.Working;
                                }
                                else
                                {
                                    decimal tempBenefits = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                    if (currBeneHusb < tempBenefits && husbBenefitSource != benefit_source.Self)
                                    {
                                        currBeneHusb = tempBenefits;
                                        husbBenefitSource = benefit_source.Self;
                                        husbFirstPayAge = ageHusb;
                                        Globals.Instance.HusbStartingBenefitName = Constants.Working;
                                    }
                                }
                            }
                        }
                        #endregion code to get the Full REtirement benefits
                    }
                    #endregion

                    #region CODE FOR MAXIMIZE BENEFITS AT AGE 66
                    if (strategy == calc_strategy.Max)
                    {
                        if (ageHusb >= husbFRA)
                        {
                            decimal workingBenefitsat70 = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                            workingBenefitsat70 = ColaUpSpousal(ageHusb, workingBenefitsat70, birthYearHusb, intAgeHusb, husbFRA);
                            int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                            decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                            spousalBenefits = spousalBenefits / 2;
                            if (currBeneHusb == 0 && spousalBenefits > workingBenefitsat70 && husbBirthDate < limit1 && wifeBenefitSource == benefit_source.Self)
                            {
                                currBeneHusb = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                currBeneHusb = currBeneHusb / 2;
                                husbFirstPayAge = ageHusb;
                                husbBenefitSource = benefit_source.Spouse;
                                Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                            }
                        }
                        if (ageWife >= wifeFRA)
                        {
                            decimal workingBenefitsat70 = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                            workingBenefitsat70 = ColaUpSpousal(ageWife, workingBenefitsat70, birthYearWife, intAgeWife, wifeFRA);
                            int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                            decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                            spousalBenefits = spousalBenefits / 2;
                            if (currBeneWife == 0 && spousalBenefits > workingBenefitsat70 && wifeBirthDate < limit1 && husbBenefitSource == benefit_source.Self)
                            {
                                currBeneWife = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                currBeneWife = currBeneWife / 2;
                                wifeFirstPayAge = ageWife;
                                wifeBenefitSource = benefit_source.Spouse;
                                Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                            }
                        }
                        if (decBeneHusb > decBeneWife)
                        {
                            if (ageWife == 70)
                            {
                                decimal WHBWife = 0;
                                if (intAgeWife > wifeFRA)
                                {
                                    multiply = AgeAbove68YearsOfWife();
                                    WHBWife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                }
                                else
                                {
                                    WHBWife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                }
                                WHBWife = ColaUpSpousal(ageWife, WHBWife, birthYearWife, intWifeAge, wifeFRA);
                                decimal spousalBenefits = 0;
                                int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                if (intAgeHusb > husbFRA)
                                {
                                    multiply = AgeAbove68YearsOfHusb();
                                    spousalBenefits = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                                }
                                else
                                {
                                    spousalBenefits = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                }
                                spousalBenefits = spousalBenefits / 2;
                                if (spousalBenefits < WHBWife)
                                    Globals.Instance.BoolShowMaxStrategy = true;
                            }
                        }
                        else
                        {
                            if (ageHusb == 70)
                            {
                                decimal WHBHusb = 0;
                                if (intAgeHusb > husbFRA)
                                {
                                    multiply = AgeAbove68YearsOfHusb();
                                    WHBHusb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                }
                                else
                                {
                                    WHBHusb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                }
                                WHBHusb = ColaUpSpousal(ageHusb, WHBHusb, birthYearHusb, intAgeHusb, husbFRA);
                                decimal spousalBenefits = 0;
                                int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                if (intAgeWife > wifeFRA)
                                {
                                    multiply = AgeAbove68YearsOfWife();
                                    spousalBenefits = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                                }
                                else
                                {
                                    spousalBenefits = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                }
                                spousalBenefits = spousalBenefits / 2;
                                if (spousalBenefits < WHBHusb)
                                    Globals.Instance.BoolShowMaxStrategy = true;
                            }
                        }
                    }
                    #endregion CODE FOR MAXIMIZE BENEFITS AT AGE 66

                    #region CODE FOR CAL OF STRATEGY FOR SPOUSE AND HUS WHEN AGE 70

                    #region code when age difference between0-4
                    //  //Spousal and Suspend strategy changes at age 70 (strategy == calc_strategy.Spousal)
                    spanHusb = DateTime.Now - husbBirthDate;
                    intHusbAge = Convert.ToInt32(spanHusb.TotalDays / 365.2425);

                    spanWife = DateTime.Now - wifeBirthDate;
                    intWifeAge = Convert.ToInt32(spanWife.TotalDays / 365.2425);

                    #region Code for Max strategy at the age of 70
                    if (strategy == calc_strategy.Max)
                    {
                        GetSpousalBenefitForIntAge(ageHusb, ageWife, ref currBeneWife, ref currBeneHusb, ref wifeBenefitSource, ref husbBenefitSource, ref husbFirstPayAge, ref wifeFirstPayAge);
                        if (ageHusb == 70)
                        {
                            decimal workingBenefitsat70;
                            if (intAgeHusb > husbFRA)
                            {
                                multiply = AgeAbove68YearsOfHusb();
                                workingBenefitsat70 = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                            }
                            else
                            {
                                workingBenefitsat70 = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                            }
                            workingBenefitsat70 = ColaUpSpousal(ageHusb, workingBenefitsat70, birthYearHusb, intHusbAge, husbFRA);
                            if (currBeneHusb < workingBenefitsat70)
                            {
                                if (currBeneHusb == 0)
                                    husbFirstPayAge = ageHusb;
                                currBeneHusb = workingBenefitsat70;
                                husbBenefitSource = benefit_source.Self;
                                Globals.Instance.BoolMaxStrategyAt70Husb = true;
                                decimal WHBWife = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                                WHBWife = ColaUpSpousal(ageWife, WHBWife, birthYearWife, intAgeWife, wifeFRA);
                                int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                spousalBenefits = spousalBenefits / 2;

                                decimal temBenefits = 0;
                                if (intAgeHusb > husbFRA)
                                {
                                    multiply = AgeAbove68YearsOfHusb();
                                    temBenefits = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                                }
                                else
                                {
                                    temBenefits = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                }
                                decimal WHBWife70 = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                                WHBWife70 = ColaUpSpousal(70, WHBWife70, birthYearWife, intAgeWife, wifeFRA);
                                int intAgeAt70 = GetAgeofHusbWhenWifeAge70();
                                decimal spousalBenefitWife = ColaUpSpousal(intAgeAt70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                spousalBenefitWife = spousalBenefitWife / 2;

                                if (WHBWife70 > spousalBenefitWife)
                                {
                                    if (currBeneWife == 0 && ageWife >= wifeFRA && wifeBenefitSource != benefit_source.Spouse && wifeBirthDate < limit1 && (spousalBenefits < WHBWife))
                                    {
                                        currBeneWife = temBenefits / 2;
                                        wifeFirstPayAge = ageWife;
                                    }
                                    else
                                    {
                                        Globals.Instance.BoolSpousalWifeAfter70 = true;
                                        Globals.Instance.WifeSpousalBenefitAfter70 = temBenefits;
                                    }
                                }
                                else
                                {
                                    if (currBeneWife == 0 && wifeBenefitSource != benefit_source.Spouse && wifeBirthDate < limit1 && (spousalBenefits < WHBWife))
                                    {
                                        currBeneWife = temBenefits / 2;
                                        wifeFirstPayAge = ageWife;
                                    }

                                }
                            }
                            else
                            { Globals.Instance.BoolMaxStrategyAt70Husb = false; }

                        }
                        if (ageWife == 70)
                        {
                            decimal workingBenefitsat70;
                            if (intAgeWife > wifeFRA)
                            {
                                multiply = AgeAbove68YearsOfWife();
                                workingBenefitsat70 = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                            }
                            else
                            {
                                workingBenefitsat70 = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                            }
                            workingBenefitsat70 = ColaUpSpousal(ageWife, workingBenefitsat70, birthYearWife, intWifeAge, wifeFRA);
                            if (currBeneWife < workingBenefitsat70)
                            {
                                if (currBeneWife == 0)
                                    wifeFirstPayAge = ageWife;
                                currBeneWife = workingBenefitsat70;
                                Globals.Instance.BoolMaxStrategyAt70Wife = true;
                                int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                decimal temBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneWife, birthYearWife, intWifeAge, wifeFRA);
                                temBenefits = temBenefits / 2;
                                decimal WHBHusb = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                                WHBHusb = ColaUpSpousal(ageHusb, WHBHusb, birthYearHusb, intAgeHusb, husbFRA);
                                decimal spousalBenefits = 0;
                                if (intAgeWife > wifeFRA)
                                {
                                    multiply = AgeAbove68YearsOfWife();
                                    spousalBenefits = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                                }
                                else
                                {
                                    spousalBenefits = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                }
                                spousalBenefits = spousalBenefits / 2;
                                decimal WHBHusband70 = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                                WHBHusband70 = ColaUpSpousal(70, WHBHusband70, birthYearHusb, intAgeHusb, husbFRA);
                                int intAgeAt70 = GetAgeofHusbWhenWifeAge70();
                                decimal spousalBenefitHusb = ColaUpSpousal(intAgeAt70, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                spousalBenefitHusb = spousalBenefitHusb / 2;

                                if (WHBHusband70 > spousalBenefitHusb)
                                {
                                    if (currBeneHusb == 0 && ageWife >= wifeFRA && husbBenefitSource != benefit_source.Spouse && husbBirthDate < limit1 && (temBenefits < WHBHusb))
                                    {
                                        currBeneHusb = spousalBenefits;
                                        husbFirstPayAge = ageHusb;
                                    }
                                    else
                                    {
                                        Globals.Instance.BoolSpousalHubsAfter70 = true;
                                    }
                                }
                                else
                                {
                                    if (currBeneHusb == 0 && husbBenefitSource != benefit_source.Spouse && husbBirthDate < limit1 && (temBenefits < WHBHusb))
                                    {
                                        currBeneHusb = spousalBenefits;
                                        husbFirstPayAge = ageHusb;
                                    }

                                }
                            }
                            else
                            { Globals.Instance.BoolMaxStrategyAt70Wife = false; }
                        }
                    }
                    #endregion Code for Max strategy at the age of 70

                    #region Suspend strategy code at the age 70
                    //if (strategy == calc_strategy.Suspend)
                    //{
                    //    #region code to the older person with higher benefits
                    //    bool WifeOlderGreater = false;
                    //    bool HusbOlderGreater = false;
                    //    if (intAgeHusb > intAgeWife)
                    //    {
                    //        if (decBeneHusb > decBeneWife)
                    //        {
                    //            HusbOlderGreater = true;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (decBeneHusb < decBeneWife)
                    //        {
                    //            WifeOlderGreater = true;
                    //        }
                    //    }
                    //    halfBenHusb = decBeneHusb / 2;
                    //    halfbenWife = decBeneWife / 2;

                    //    #endregion code to the older person with higher benefits

                    //    #region code when one benefits is less than half of another
                    //    if (decBeneHusb > decBeneWife)
                    //    {
                    //        #region code when husb benefits are greater
                    //        if (decBeneWife <= halfBenHusb)
                    //        {
                    //            if (ageHusb == 70)
                    //            {
                    //                //code to check if age greater than FRA
                    //                if (intAgeHusb > husbFRA)
                    //                {
                    //                    //vlaue for husb at FRA age
                    //                    multiply = AgeAbove68YearsOfHusb();
                    //                    currBeneHusb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                    //                    //spousal benefits at same age for wife
                    //                    decimal temBenefits = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                    //                    temBenefits = temBenefits / 2;
                    //                    if (ageSpan >= 10)
                    //                    {
                    //                        if (HusbOlderGreater)
                    //                            odlgerGreater = 1;
                    //                    }
                    //                    else
                    //                    {
                    //                        if (temBenefits > currBeneHusb && husbBenefitSource != benefit_source.Spouse)
                    //                            currBeneHusb = temBenefits;
                    //                    }
                    //                    //currBeneWife = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                    //                    //currBeneWife = currBeneWife / 2;
                    //                }
                    //                else
                    //                {
                    //                    currBeneHusb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                    //                }
                    //                currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intHusbAge, husbFRA);
                    //                decimal spousalWifebene = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                    //                spousalWifebene = spousalWifebene / 2;
                    //                if (currBeneWife < spousalWifebene && (ageWife < 70) && wifeBenefitSource != benefit_source.Spouse)
                    //                {
                    //                    currBeneWife = spousalWifebene;
                    //                    wifeBenefitSource = benefit_source.Spouse;
                    //                }
                    //                at70 = true;
                    //            }
                    //            if (ageWife == 70)
                    //            {
                    //                decimal workingBenefitsat70 = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                    //                workingBenefitsat70 = ColaUpSpousal(ageWife, workingBenefitsat70, birthYearWife, intWifeAge, wifeFRA);
                    //                if (Convert.ToInt32(workingBenefitsat70) > Convert.ToInt32(currBeneWife))
                    //                {
                    //                    currBeneWife = workingBenefitsat70;
                    //                    FbuildGridAt70 = true;
                    //                    //needAsterisk = true;
                    //                    Globals.Instance.Age70ChangeForWife = true;
                    //                }
                    //            }
                    //        }
                    //        else
                    //        {
                    //            if (ageWife == 70)
                    //            {
                    //                //code to check if age greater than FRA
                    //                if (intAgeWife > wifeFRA)
                    //                {
                    //                    //FRA value for wife when age greater than FRA
                    //                    multiply = AgeAbove68YearsOfWife();
                    //                    currBeneWife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                    //                    //spousal benefits at same age for husb
                    //                    decimal temBenefits = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                    //                    temBenefits = temBenefits / 2;

                    //                    if (ageSpan >= 10)
                    //                    {
                    //                        if (HusbOlderGreater)
                    //                            odlgerGreater = 1;
                    //                    }
                    //                    else
                    //                    {
                    //                        if (temBenefits > currBeneHusb && husbBenefitSource != benefit_source.Spouse)
                    //                            currBeneHusb = temBenefits;
                    //                    }
                    //                    //currBeneHusb = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                    //                    //currBeneHusb = currBeneHusb / 2;
                    //                }
                    //                else
                    //                {
                    //                    currBeneWife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                    //                }
                    //                currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intWifeAge, wifeFRA);
                    //                // wifeBenefitSource = benefit_source.Self;
                    //                //gridNote.Text += "  Wife switched to his own work history benefit at age " + ageWife.ToString() + ".";
                    //                FbuildGridAt70 = true;
                    //                Globals.Instance.Age70ChangeForWife = true;
                    //            }

                    //            if (ageHusb == 70)
                    //            {
                    //                //code to check if age greater than FRA
                    //                if (intAgeHusb > husbFRA)
                    //                {
                    //                    //FRA value for husb
                    //                    multiply = AgeAbove68YearsOfHusb();
                    //                    currBeneHusb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                    //                    //spousal benefits for wife at same age
                    //                    decimal temBenefits = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                    //                    temBenefits = temBenefits / 2;
                    //                    if (ageSpan >= 10)
                    //                    {
                    //                        if (HusbOlderGreater)
                    //                            odlgerGreater = 1;
                    //                    }
                    //                    else
                    //                    {
                    //                        if (temBenefits > currBeneWife && wifeBenefitSource != benefit_source.Spouse)
                    //                        {
                    //                            currBeneWife = temBenefits;
                    //                            Globals.Instance.Age70ChangeForWife = true;
                    //                        }
                    //                    }
                    //                    //currBeneWife = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                    //                    //currBeneWife = currBeneWife / 2;
                    //                }
                    //                else
                    //                {
                    //                    currBeneHusb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                    //                }
                    //                currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intHusbAge, husbFRA);
                    //                //husbBenefitSource = benefit_source.Self;
                    //                //gridNote.Text += "  Husband switched to his own work history benefit at age " + ageHusb.ToString() + ".";
                    //                at70 = true;
                    //            }
                    //        }
                    //        #endregion code when husb benefits are greater
                    //    }
                    //    else
                    //    {
                    //        #region Code when wife benefits are greater
                    //        if (decBeneHusb <= halfbenWife)
                    //        {
                    //            if (ageWife == 70)
                    //            {
                    //                //code to check if age greater than FRA
                    //                if (intAgeWife > wifeFRA)
                    //                {
                    //                    //value at FRA for Wife
                    //                    multiply = AgeAbove68YearsOfWife();
                    //                    currBeneWife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                    //                    //spousal benefits for husb at same age
                    //                    decimal temBenefits = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                    //                    temBenefits = temBenefits / 2;
                    //                    if (ageSpan >= 10)
                    //                    {
                    //                        if (WifeOlderGreater)
                    //                            odlgerGreater = 1;
                    //                    }
                    //                    else
                    //                    {
                    //                        if (temBenefits > currBeneHusb && husbBenefitSource != benefit_source.Spouse)
                    //                        {
                    //                            currBeneHusb = temBenefits;
                    //                            Globals.Instance.Age70ChangeForHusb = true;
                    //                        }
                    //                    }
                    //                    //currBeneHusb = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                    //                    //currBeneHusb = currBeneHusb / 2;
                    //                }
                    //                else
                    //                {
                    //                    currBeneWife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                    //                }
                    //                currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intWifeAge, wifeFRA);
                    //                decimal spousalhusbbene;
                    //                //decimal spousalhusbbene = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                    //                //spousalhusbbene = Convert.ToInt32(spousalhusbbene / 2);
                    //                if (ageHusb < husbFRA)
                    //                {
                    //                    spousalhusbbene = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                    //                }
                    //                else
                    //                {
                    //                    spousalhusbbene = AgeAbove68YearsOfWife();
                    //                    spousalhusbbene = ColaUpSpousal(ageWife, spousalhusbbene, birthYearWife, intAgeWife, wifeFRA);
                    //                    spousalhusbbene = spousalhusbbene / 2;
                    //                }
                    //                if (spousalhusbbene > currBeneHusb && husbBenefitSource != benefit_source.Spouse)
                    //                {
                    //                    currBeneHusb = spousalhusbbene;
                    //                    workingBenefitsForHusb = ageHusb.ToString();
                    //                    showWorkingBenefitsForHusb = true;
                    //                    Globals.Instance.Age70ChangeForHusb = true;

                    //                }
                    //                // wifeBenefitSource = benefit_source.Self;
                    //                //gridNote.Text += "  Wife switched to his own work history benefit at age " + ageWife.ToString() + ".";
                    //                FbuildGridAt70 = true;
                    //            }
                    //            if (ageHusb == 70)
                    //            {
                    //                decimal workingBenefitsat70 = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                    //                workingBenefitsat70 = ColaUpSpousal(ageHusb, workingBenefitsat70, birthYearHusb, intHusbAge, husbFRA);
                    //                if (Convert.ToInt32(workingBenefitsat70) > currBeneHusb && husbBenefitSource != benefit_source.Spouse)
                    //                {
                    //                    currBeneHusb = workingBenefitsat70;
                    //                    at70 = true;
                    //                    //needAsterisk = true;
                    //                    Globals.Instance.Age70ChangeForHusb = true;
                    //                }
                    //            }
                    //        }
                    //        else
                    //        {
                    //            if (ageWife == 70)
                    //            {
                    //                //code to check if age greater than FRA
                    //                if (intAgeWife > wifeFRA)
                    //                {
                    //                    //value at FRA for wife
                    //                    multiply = AgeAbove68YearsOfWife();
                    //                    currBeneWife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                    //                    //spousal benefits for husb at same age
                    //                    decimal temBenefits = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                    //                    temBenefits = Convert.ToInt32(temBenefits / 2);
                    //                    if (ageSpan >= 10)
                    //                    {
                    //                        if (WifeOlderGreater)
                    //                            odlgerGreater = 1;
                    //                    }
                    //                    else
                    //                    {
                    //                        if (temBenefits > currBeneHusb && husbBenefitSource != benefit_source.Spouse)
                    //                        {
                    //                            currBeneHusb = temBenefits;
                    //                            Globals.Instance.Age70ChangeForHusb = true;
                    //                        }
                    //                    }
                    //                    //currBeneHusb = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                    //                    //currBeneHusb = currBeneHusb / 2;
                    //                }
                    //                else
                    //                {
                    //                    currBeneWife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                    //                }
                    //                currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intWifeAge, wifeFRA);
                    //                //wifeBenefitSource = benefit_source.Self;
                    //                //gridNote.Text += "  Wife switched to his own work history benefit at age " + ageWife.ToString() + ".";
                    //                FbuildGridAt70 = true;
                    //            }

                    //            if (ageHusb == 70)
                    //            {
                    //                //code to check if age greater than FRA
                    //                if (intAgeHusb > husbFRA)
                    //                {
                    //                    //value at FRA for husb
                    //                    multiply = AgeAbove68YearsOfHusb();
                    //                    currBeneHusb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                    //                    //spousal benefits for wife at same age
                    //                    decimal temBenefits = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                    //                    temBenefits = Convert.ToInt32(temBenefits / 2);
                    //                    if (ageSpan >= 10)
                    //                    {
                    //                        if (WifeOlderGreater)
                    //                            odlgerGreater = 1;
                    //                    }
                    //                    else
                    //                    {
                    //                        if (temBenefits > currBeneWife && wifeBenefitSource != benefit_source.Spouse)
                    //                        {
                    //                            currBeneWife = temBenefits;
                    //                        }
                    //                    }    //currBeneWife = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                    //                    //currBeneWife = currBeneWife / 2;
                    //                }
                    //                else
                    //                {
                    //                    currBeneHusb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                    //                }
                    //                currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intHusbAge, husbFRA);
                    //                // husbBenefitSource = benefit_source.Self;
                    //                //gridNote.Text += "  Husband switched to his own work history benefit at age " + ageHusb.ToString() + ".";
                    //                at70 = true;
                    //            }
                    //        }

                    //        #endregion Code when wife benefits are greater
                    //    }
                    //    #endregion code when one benefits is less than half of another

                    //    #region code when one benefits is greater than half of another
                    //    if (decBeneHusb > decBeneWife && decBeneWife > halfBenHusb)
                    //    {
                    //        if (ageHusb == 70)
                    //        {
                    //            //code to check if age greater than FRA
                    //            if (intAgeHusb > husbFRA)
                    //            {
                    //                //value at FRA for husb
                    //                multiply = AgeAbove68YearsOfHusb();
                    //                currBeneHusb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                    //                //spousal benefits for wife at same age
                    //                decimal temBenefits = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                    //                temBenefits = Convert.ToInt32(temBenefits / 2);
                    //                if (ageSpan >= 10)
                    //                {
                    //                    if (WifeOlderGreater)
                    //                        odlgerGreater = 1;
                    //                }
                    //                else
                    //                {
                    //                    if (temBenefits > currBeneWife && wifeBenefitSource != benefit_source.Spouse)
                    //                    {
                    //                        currBeneWife = temBenefits;
                    //                        Globals.Instance.Age70ChangeForWife = true;
                    //                    }
                    //                }
                    //            }
                    //            else
                    //            {
                    //                currBeneHusb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                    //            }
                    //            currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intHusbAge, husbFRA);
                    //            // gridNote.Text += "  Husband switched to his own work history benefit at age " + ageHusb.ToString() + ".";
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (decBeneHusb < decBeneWife && decBeneHusb > halfbenWife)
                    //        {
                    //            if (ageWife == 70)
                    //            {
                    //                //code to check if age greater than FRA
                    //                if (intAgeWife > wifeFRA)
                    //                {//value at FRA for wife
                    //                    multiply = AgeAbove68YearsOfWife();
                    //                    currBeneWife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                    //                    //spousal benefits for husb at same age
                    //                    decimal temBenefits = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                    //                    temBenefits = Convert.ToInt32(temBenefits / 2);
                    //                    if (ageSpan >= 10)
                    //                    {
                    //                        if (HusbOlderGreater)
                    //                            odlgerGreater = 1;
                    //                    }
                    //                    else
                    //                    {
                    //                        if (temBenefits > currBeneHusb && husbBenefitSource != benefit_source.Spouse)
                    //                        {
                    //                            currBeneHusb = temBenefits;
                    //                        }
                    //                    }
                    //                    //currBeneHusb = currBeneHusb / 2;
                    //                    // wifeBenefitSource = benefit_source.SpousalBenefit;
                    //                }
                    //                else
                    //                {
                    //                    currBeneWife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                    //                }
                    //                currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intWifeAge, wifeFRA);// (currBeneWife) * Convert.ToDecimal(Math.Pow(1.025, (70 - intAgeWife)));
                    //                if (strategy == calc_strategy.Suspend && husbBenefitSource != benefit_source.Spouse)
                    //                    Globals.Instance.Age70ChangeForHusb = true;
                    //                // gridNote.Text += "  Wife switched to his own work history benefit at age " + ageWife.ToString() + ".";
                    //            }
                    //        }
                    //    }
                    //    #endregion code when one benefits is greater than half of another
                    //}
                    #endregion Suspend strategy code at the age 70

                    #region Spousal Stratgey code at the age 70
                    if (strategy == calc_strategy.Spousal)
                    {
                        if (wifeBirthDate < limit1)
                        {
                            #region code to the older person with higher benefits
                            bool WifeOlderGreater = false;
                            bool HusbOlderGreater = false;
                            if (intAgeHusb > intAgeWife)
                            {
                                if (decBeneHusb > decBeneWife)
                                {
                                    HusbOlderGreater = true;
                                }
                            }
                            else
                            {
                                if (decBeneHusb < decBeneWife)
                                {
                                    WifeOlderGreater = true;
                                }
                            }
                            halfBenHusb = decBeneHusb / 2;
                            halfbenWife = decBeneWife / 2;
                            #endregion code to the older person with higher benefits

                            #region code when one benefits is less than half of another
                            if (decBeneHusb >= decBeneWife)
                            {
                                #region code when husb benefits are greater
                                if (decBeneWife <= halfBenHusb)
                                {
                                    if (husbBenefitSource == benefit_source.Spouse)
                                    {
                                        if (husbBirthDate < limit1)
                                        {
                                            if (ageHusb == 70)
                                            {
                                                //code to check if age greater than FRA
                                                if (intAgeHusb > husbFRA)
                                                {
                                                    //vlaue for husb at FRA age
                                                    multiply = AgeAbove68YearsOfHusb();
                                                    if (currBeneHusb == 0)
                                                    {
                                                        Globals.Instance.HusbStartingBenefitName = Constants.Working;
                                                        Globals.Instance.BoolAge70StartingAgeHusb = true;
                                                    }
                                                    currBeneHusb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                                    //spousal benefits at same age for wife
                                                    decimal temBenefits = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                                                    temBenefits = temBenefits / 2;
                                                    if (ageSpan >= 10)
                                                    {
                                                        if (HusbOlderGreater)
                                                            odlgerGreater = 1;
                                                    }
                                                    else
                                                    {
                                                        decimal WHBWife70 = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                                                        WHBWife70 = ColaUpSpousal(70, WHBWife70, birthYearWife, intAgeWife, wifeFRA);
                                                        int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                        decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                                        spousalBenefits = spousalBenefits / 2;

                                                        if (WHBWife70 > spousalBenefits)
                                                        {
                                                            if (temBenefits > currBeneWife && ageWife >= wifeFRA)
                                                            {
                                                                if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                                    Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                                currBeneWife = temBenefits;
                                                                Globals.Instance.BoolSpousalChange = true;
                                                                Globals.Instance.StringSpousalChangeAgeWife = true;
                                                                Globals.Instance.BoolSpousalCliamedWife = true;
                                                                AgeValueForSpousalAt70 = ageWife;
                                                                ValueForWifeAt70 = currBeneWife;
                                                            }
                                                            else
                                                            {
                                                                Globals.Instance.BoolSpousalWifeAfter70 = true;
                                                                Globals.Instance.WifeSpousalBenefitAfter70 = temBenefits;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (temBenefits > currBeneWife)
                                                            {
                                                                if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                                    Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                                currBeneWife = temBenefits;
                                                                Globals.Instance.BoolSpousalChange = true;
                                                                Globals.Instance.StringSpousalChangeAgeWife = true;
                                                                Globals.Instance.BoolSpousalCliamedWife = true;
                                                                AgeValueForSpousalAt70 = ageWife;
                                                                ValueForWifeAt70 = currBeneWife;
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    currBeneHusb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                                }
                                                currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intHusbAge, husbFRA);
                                                Globals.Instance.BoolAge70ChangeHusb = true;
                                                decimal spousalWifebene;
                                                if (wifeBenefitSource == benefit_source.None && currBeneWife == 0)
                                                {
                                                    if (intAgeHusb > husbFRA)
                                                    {
                                                        multiply = AgeAbove68YearsOfHusb();
                                                        spousalWifebene = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                                                    }
                                                    else
                                                    {
                                                        spousalWifebene = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                                    }
                                                    spousalWifebene = spousalWifebene / 2;
                                                    if (currBeneWife == 0)
                                                        Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                    currBeneWife = spousalWifebene;
                                                    wifeBenefitSource = benefit_source.Spouse;
                                                }
                                                if (intAgeHusb > husbFRA)
                                                {
                                                    if (ageWife >= wifeFRA)
                                                    {
                                                        spousalWifebene = AgeAbove68YearsOfHusb();
                                                        spousalWifebene = ColaUpSpousal(ageHusb, spousalWifebene, birthYearHusb, intAgeHusb, husbFRA);
                                                        spousalWifebene = spousalWifebene / 2;
                                                    }
                                                    else
                                                    {
                                                        spousalWifebene = SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                                    }
                                                }
                                                else
                                                {
                                                    if (ageWife < wifeFRA)
                                                    {
                                                        spousalWifebene = SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                                    }
                                                    else
                                                    {
                                                        spousalWifebene = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                                        spousalWifebene = spousalWifebene / 2;
                                                    }
                                                }
                                                if (spousalWifebene > currBeneWife && ageWife >= wifeFRA && wifeBenefitSource == benefit_source.Self)
                                                {
                                                    decimal WHBWife70 = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                                                    WHBWife70 = ColaUpSpousal(70, WHBWife70, birthYearWife, intAgeWife, wifeFRA);
                                                    int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                    decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                                    spousalBenefits = spousalBenefits / 2;
                                                    if (WHBWife70 > spousalBenefits)
                                                    {
                                                        if (ageWife >= wifeFRA)
                                                        {
                                                            if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                                Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                            currBeneWife = spousalWifebene;
                                                            Globals.Instance.BoolSpousalChange = true;
                                                            AgeValueForSpousalAt70 = ageWife;
                                                            ValueForWifeAt70 = currBeneWife;
                                                            Globals.Instance.BoolSpousalCliamedWife = true;
                                                            wifeBenefitSource = benefit_source.SpousalBenefit;
                                                            Globals.Instance.StringSpousalChangeAgeWife = true;
                                                            workingBenefitsForWife = ageWife.ToString();
                                                            showWorkingBenefitsForWife = true;
                                                        }
                                                        else
                                                        {
                                                            Globals.Instance.BoolSpousalWifeAfter70 = true;
                                                            Globals.Instance.WifeSpousalBenefitAfter70 = spousalWifebene;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                            Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                        currBeneWife = spousalWifebene;
                                                        Globals.Instance.BoolSpousalChange = true;
                                                        AgeValueForSpousalAt70 = ageWife;
                                                        ValueForWifeAt70 = currBeneWife;
                                                        Globals.Instance.BoolSpousalCliamedWife = true;
                                                        wifeBenefitSource = benefit_source.SpousalBenefit;
                                                        Globals.Instance.StringSpousalChangeAgeWife = true;
                                                        workingBenefitsForWife = ageWife.ToString();
                                                        showWorkingBenefitsForWife = true;
                                                    }
                                                }
                                                at70 = true;
                                            }
                                        }
                                    }
                                    else if (ageHusb == 70 && husbBenefitSource != benefit_source.Self)
                                    {
                                        //code to check if age greater than FRA
                                        if (intAgeHusb > husbFRA)
                                        {
                                            //vlaue for husb at FRA age
                                            multiply = AgeAbove68YearsOfHusb();
                                            if (currBeneHusb == 0)
                                            {
                                                Globals.Instance.HusbStartingBenefitName = Constants.Working;
                                                Globals.Instance.BoolAge70StartingAgeHusb = true;
                                            }
                                            currBeneHusb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                            //spousal benefits at same age for wife
                                            decimal temBenefits = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                                            temBenefits = temBenefits / 2;
                                            if (ageSpan >= 10)
                                            {
                                                if (HusbOlderGreater)
                                                    odlgerGreater = 1;
                                            }
                                            else
                                            {
                                                decimal WHBWife70 = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                                                WHBWife70 = ColaUpSpousal(70, WHBWife70, birthYearWife, intAgeWife, wifeFRA);
                                                int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                                spousalBenefits = spousalBenefits / 2;

                                                if (WHBWife70 > spousalBenefits)
                                                {
                                                    if (temBenefits > currBeneWife && ageWife >= wifeFRA)
                                                    {
                                                        if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                            Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                        currBeneWife = temBenefits;
                                                        Globals.Instance.BoolSpousalChange = true;
                                                        Globals.Instance.StringSpousalChangeAgeWife = true;
                                                        Globals.Instance.BoolSpousalCliamedWife = true;
                                                        AgeValueForSpousalAt70 = ageWife;
                                                        ValueForWifeAt70 = currBeneWife;
                                                    }
                                                    else
                                                    {
                                                        Globals.Instance.BoolSpousalWifeAfter70 = true;
                                                        Globals.Instance.WifeSpousalBenefitAfter70 = temBenefits;
                                                    }
                                                }
                                                else
                                                {
                                                    if (temBenefits > currBeneWife)
                                                    {
                                                        if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                            Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                        currBeneWife = temBenefits;
                                                        Globals.Instance.BoolSpousalChange = true;
                                                        Globals.Instance.StringSpousalChangeAgeWife = true;
                                                        Globals.Instance.BoolSpousalCliamedWife = true;
                                                        AgeValueForSpousalAt70 = ageWife;
                                                        ValueForWifeAt70 = currBeneWife;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            currBeneHusb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                        }
                                        currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intHusbAge, husbFRA);
                                        Globals.Instance.BoolAge70ChangeHusb = true;
                                        decimal spousalWifebene;
                                        if (wifeBenefitSource == benefit_source.None && currBeneWife == 0)
                                        {
                                            if (intAgeHusb > husbFRA)
                                            {
                                                multiply = AgeAbove68YearsOfHusb();
                                                spousalWifebene = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                                            }
                                            else
                                            {
                                                spousalWifebene = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                            }
                                            spousalWifebene = spousalWifebene / 2;
                                            if (currBeneWife == 0)
                                                Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                            currBeneWife = spousalWifebene;
                                            wifeBenefitSource = benefit_source.Spouse;
                                        }

                                        if (intAgeHusb > husbFRA)
                                        {
                                            if (ageWife >= wifeFRA)
                                            {
                                                spousalWifebene = AgeAbove68YearsOfHusb();
                                                spousalWifebene = ColaUpSpousal(ageHusb, spousalWifebene, birthYearHusb, intAgeHusb, husbFRA);
                                                spousalWifebene = spousalWifebene / 2;
                                            }
                                            else
                                            {
                                                spousalWifebene = SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                            }
                                        }
                                        else
                                        {
                                            if (ageWife < wifeFRA)
                                            {
                                                spousalWifebene = SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                            }
                                            else
                                            {
                                                spousalWifebene = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                                spousalWifebene = spousalWifebene / 2;
                                            }
                                        }

                                        if (spousalWifebene > currBeneWife && ageWife > wifeFRA && wifeBenefitSource == benefit_source.Self)
                                        {
                                            decimal WHBWife70 = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                                            WHBWife70 = ColaUpSpousal(70, WHBWife70, birthYearWife, intAgeWife, wifeFRA);
                                            int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                            decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                            spousalBenefits = spousalBenefits / 2;

                                            if (WHBWife70 > spousalBenefits)
                                            {
                                                if (ageWife >= wifeFRA)
                                                {
                                                    if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                        Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                    currBeneWife = spousalWifebene;
                                                    Globals.Instance.BoolSpousalChange = true;
                                                    AgeValueForSpousalAt70 = ageWife;
                                                    ValueForWifeAt70 = currBeneWife;
                                                    Globals.Instance.BoolSpousalCliamedWife = true;
                                                    wifeBenefitSource = benefit_source.SpousalBenefit;
                                                    Globals.Instance.StringSpousalChangeAgeWife = true;
                                                    workingBenefitsForWife = ageWife.ToString();
                                                    showWorkingBenefitsForWife = true;
                                                }
                                                else
                                                {
                                                    Globals.Instance.WifeSpousalBenefitAfter70 = spousalWifebene;
                                                    Globals.Instance.BoolSpousalWifeAfter70 = true;
                                                }
                                            }
                                            else
                                            {
                                                if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                    Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                currBeneWife = spousalWifebene;
                                                Globals.Instance.BoolSpousalChange = true;
                                                AgeValueForSpousalAt70 = ageWife;
                                                ValueForWifeAt70 = currBeneWife;
                                                Globals.Instance.BoolSpousalCliamedWife = true;
                                                wifeBenefitSource = benefit_source.SpousalBenefit;
                                                Globals.Instance.StringSpousalChangeAgeWife = true;
                                                workingBenefitsForWife = ageWife.ToString();
                                                showWorkingBenefitsForWife = true;
                                            }
                                        }
                                        at70 = true;
                                    }
                                    if (wifeBenefitSource == benefit_source.Spouse)
                                    {
                                        if (wifeBirthDate < limit1)
                                        {
                                            if (ageWife == 70 && wifeBenefitSource != benefit_source.SpousalBenefit && wifeBenefitSource != benefit_source.Self && decBeneWife != 0)
                                            {
                                                //code to check if age greater than FRA
                                                if (intAgeWife > wifeFRA)
                                                {
                                                    multiply = AgeAbove68YearsOfWife();
                                                    if (currBeneWife == 0)
                                                    {
                                                        Globals.Instance.WifeStartingBenefitName = Constants.Working;
                                                        Globals.Instance.BoolAge70StartingAgeWife = true;
                                                    }
                                                    currBeneWife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                                    decimal temBenefits = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                                                    if (ageSpan >= 10)
                                                    {
                                                        if (WifeOlderGreater)
                                                            odlgerGreater = 1;
                                                    }
                                                    else
                                                    {
                                                        decimal WHBHusband70 = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                                                        WHBHusband70 = ColaUpSpousal(70, WHBHusband70, birthYearHusb, intAgeHusb, husbFRA);
                                                        int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                        decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                                        spousalBenefits = spousalBenefits / 2;

                                                        if (WHBHusband70 > spousalBenefits)
                                                        {
                                                            if (temBenefits > currBeneHusb && ageHusb >= husbFRA && husbBenefitSource != benefit_source.Spouse)
                                                            {
                                                                if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                                    Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                                currBeneHusb = temBenefits;
                                                                Globals.Instance.BoolSpousalChange = true;
                                                                Globals.Instance.BoolSpousalCliamedHusb = true;
                                                                Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                                AgeValueForSpousalAt70 = ageHusb;
                                                                ValueForWifeAt70 = currBeneHusb;
                                                                husbBenefitSource = benefit_source.Spouse;
                                                            }
                                                            else
                                                            {
                                                                Globals.Instance.BoolSpousalHubsAfter70 = true;
                                                                Globals.Instance.HusbSpousalBenefitAfter70 = temBenefits;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (temBenefits > currBeneHusb && husbBenefitSource != benefit_source.Spouse)
                                                            {
                                                                if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                                    Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                                currBeneHusb = temBenefits;
                                                                Globals.Instance.BoolSpousalChange = true;
                                                                Globals.Instance.BoolSpousalCliamedHusb = true;
                                                                Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                                AgeValueForSpousalAt70 = ageHusb;
                                                                ValueForWifeAt70 = currBeneHusb;
                                                                husbBenefitSource = benefit_source.Spouse;
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    currBeneWife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                                }
                                                currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intWifeAge, wifeFRA);
                                                Globals.Instance.BoolAge70ChangeWife = true;
                                                FbuildGridAt70 = true;
                                            }
                                        }
                                    }
                                    else if (ageWife == 70 && wifeBenefitSource != benefit_source.SpousalBenefit && wifeBenefitSource != benefit_source.Self && decBeneWife != 0)
                                    {
                                        //code to check if age greater than FRA
                                        if (intAgeWife > wifeFRA)
                                        {
                                            multiply = AgeAbove68YearsOfWife();
                                            if (currBeneWife == 0)
                                            {
                                                Globals.Instance.WifeStartingBenefitName = Constants.Working;
                                                Globals.Instance.BoolAge70StartingAgeWife = true;
                                            }
                                            currBeneWife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                            decimal temBenefits = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                                            if (ageSpan >= 10)
                                            {
                                                if (WifeOlderGreater)
                                                    odlgerGreater = 1;
                                            }
                                            else
                                            {
                                                decimal WHBHusband70 = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                                                WHBHusband70 = ColaUpSpousal(70, WHBHusband70, birthYearHusb, intAgeHusb, husbFRA);
                                                int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                                spousalBenefits = spousalBenefits / 2;

                                                if (WHBHusband70 > spousalBenefits)
                                                {
                                                    if (temBenefits > currBeneHusb && ageHusb >= husbFRA && husbBenefitSource != benefit_source.Spouse)
                                                    {
                                                        if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                            Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                        currBeneHusb = temBenefits;
                                                        Globals.Instance.BoolSpousalChange = true;
                                                        Globals.Instance.BoolSpousalCliamedHusb = true;
                                                        Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                        AgeValueForSpousalAt70 = ageHusb;
                                                        ValueForWifeAt70 = currBeneHusb;
                                                        husbBenefitSource = benefit_source.Spouse;
                                                    }
                                                    else
                                                    {
                                                        Globals.Instance.BoolSpousalHubsAfter70 = true;
                                                        Globals.Instance.HusbSpousalBenefitAfter70 = temBenefits;
                                                    }
                                                }
                                                else
                                                {
                                                    if (temBenefits > currBeneHusb && husbBenefitSource != benefit_source.Spouse)
                                                    {
                                                        if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                            Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                        currBeneHusb = temBenefits;
                                                        Globals.Instance.BoolSpousalChange = true;
                                                        Globals.Instance.BoolSpousalCliamedHusb = true;
                                                        Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                        AgeValueForSpousalAt70 = ageHusb;
                                                        ValueForWifeAt70 = currBeneHusb;
                                                        husbBenefitSource = benefit_source.Spouse;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            currBeneWife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                        }
                                        currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intWifeAge, wifeFRA);
                                        Globals.Instance.BoolAge70ChangeWife = true;
                                        FbuildGridAt70 = true;
                                    }
                                }
                                else
                                {
                                    if (husbBenefitSource == benefit_source.Spouse)
                                    {
                                        if (husbBirthDate < limit1)
                                        {
                                            if (ageHusb == 70 && decBeneHusb != 0)
                                            {
                                                //code to check if age greater than FRA
                                                if (intAgeHusb > husbFRA)
                                                {
                                                    //FRA value for husb
                                                    multiply = AgeAbove68YearsOfHusb();
                                                    if (currBeneHusb == 0)
                                                    {
                                                        Globals.Instance.HusbStartingBenefitName = Constants.Working;
                                                        Globals.Instance.BoolAge70StartingAgeHusb = true;
                                                    }
                                                    currBeneHusb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                                    //spousal benefits for wife at same age
                                                    decimal temBenefits = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                                                    temBenefits = temBenefits / 2;
                                                    if (ageSpan >= 10)
                                                    {
                                                        if (WifeOlderGreater)
                                                            odlgerGreater = 1;
                                                    }
                                                    else
                                                    {
                                                        decimal WHBWife70 = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                                                        WHBWife70 = ColaUpSpousal(70, WHBWife70, birthYearWife, intAgeWife, wifeFRA);
                                                        int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                        decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                                        spousalBenefits = spousalBenefits / 2;

                                                        if (WHBWife70 > spousalBenefits)
                                                        {
                                                            if (temBenefits > currBeneWife && ageWife >= wifeFRA && (wifeBenefitSource == benefit_source.Self || wifeBenefitSource == benefit_source.None))
                                                            {
                                                                if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                                    Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                                currBeneWife = temBenefits;
                                                                Globals.Instance.BoolSpousalChange = true;
                                                                Globals.Instance.BoolSpousalCliamedWife = true;
                                                                Globals.Instance.StringSpousalChangeAgeWife = true;
                                                                AgeValueForSpousalAt70 = ageWife;
                                                                ValueForWifeAt70 = currBeneWife;
                                                                wifeBenefitSource = benefit_source.Spouse;
                                                            }
                                                            else
                                                            {
                                                                Globals.Instance.BoolSpousalWifeAfter70 = true;
                                                                Globals.Instance.WifeSpousalBenefitAfter70 = temBenefits;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (temBenefits > currBeneWife && (wifeBenefitSource == benefit_source.Self || wifeBenefitSource == benefit_source.None))
                                                            {
                                                                if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                                    Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                                currBeneWife = temBenefits;
                                                                Globals.Instance.BoolSpousalChange = true;
                                                                Globals.Instance.BoolSpousalCliamedWife = true;
                                                                Globals.Instance.StringSpousalChangeAgeWife = true;
                                                                AgeValueForSpousalAt70 = ageWife;
                                                                ValueForWifeAt70 = currBeneWife;
                                                                wifeBenefitSource = benefit_source.Spouse;
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    currBeneHusb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                                }
                                                currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intHusbAge, husbFRA);
                                                Globals.Instance.BoolAge70ChangeHusb = true;
                                                at70 = true;
                                            }
                                        }
                                    }
                                    else if (ageHusb == 70 && husbBenefitSource != benefit_source.Self && decBeneHusb != 0)
                                    {
                                        //code to check if age greater than FRA
                                        if (intAgeHusb > husbFRA)
                                        {
                                            //FRA value for husb
                                            multiply = AgeAbove68YearsOfHusb();
                                            if (currBeneHusb == 0)
                                            {
                                                Globals.Instance.HusbStartingBenefitName = Constants.Working;
                                                Globals.Instance.BoolAge70StartingAgeHusb = true;
                                            }
                                            currBeneHusb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                            //spousal benefits for wife at same age
                                            decimal temBenefits = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                                            temBenefits = temBenefits / 2;
                                            if (ageSpan >= 10)
                                            {
                                                if (WifeOlderGreater)
                                                    odlgerGreater = 1;
                                            }
                                            else
                                            {
                                                decimal WHBWife70 = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                                                WHBWife70 = ColaUpSpousal(70, WHBWife70, birthYearWife, intAgeWife, wifeFRA);
                                                int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                                spousalBenefits = spousalBenefits / 2;

                                                if (WHBWife70 > spousalBenefits)
                                                {
                                                    if (temBenefits > currBeneWife && ageWife >= wifeFRA && (wifeBenefitSource == benefit_source.Self || wifeBenefitSource == benefit_source.None))
                                                    {
                                                        if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                            Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                        currBeneWife = temBenefits;
                                                        Globals.Instance.BoolSpousalChange = true;
                                                        Globals.Instance.BoolSpousalCliamedWife = true;
                                                        Globals.Instance.StringSpousalChangeAgeWife = true;
                                                        AgeValueForSpousalAt70 = ageWife;
                                                        ValueForWifeAt70 = currBeneWife;
                                                        wifeBenefitSource = benefit_source.Spouse;
                                                    }
                                                    else
                                                    {
                                                        Globals.Instance.BoolSpousalWifeAfter70 = true;
                                                        Globals.Instance.WifeSpousalBenefitAfter70 = temBenefits;
                                                    }
                                                }
                                                else
                                                {
                                                    if (temBenefits > currBeneWife && (wifeBenefitSource == benefit_source.Self || wifeBenefitSource == benefit_source.None))
                                                    {
                                                        if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                            Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                        currBeneWife = temBenefits;
                                                        Globals.Instance.BoolSpousalChange = true;
                                                        Globals.Instance.BoolSpousalCliamedWife = true;
                                                        Globals.Instance.StringSpousalChangeAgeWife = true;
                                                        AgeValueForSpousalAt70 = ageWife;
                                                        ValueForWifeAt70 = currBeneWife;
                                                        wifeBenefitSource = benefit_source.Spouse;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            currBeneHusb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                        }
                                        currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intHusbAge, husbFRA);
                                        Globals.Instance.BoolAge70ChangeHusb = true;
                                        at70 = true;
                                    }
                                }
                                #endregion code when husb benefits are greater
                            }
                            else
                            {
                                #region Code when wife benefits are greater
                                if (decBeneHusb <= halfbenWife)
                                {
                                    if (wifeBenefitSource == benefit_source.Spouse)
                                    {
                                        if (wifeBirthDate < limit1)
                                        {
                                            if (ageWife == 70)
                                            {
                                                //code to check if age greater than FRA
                                                if (intAgeWife > wifeFRA)
                                                {
                                                    //value at FRA for Wife
                                                    multiply = AgeAbove68YearsOfWife();
                                                    if (currBeneWife == 0)
                                                    {
                                                        Globals.Instance.WifeStartingBenefitName = Constants.Working;
                                                        Globals.Instance.BoolAge70StartingAgeWife = true;
                                                    }
                                                    currBeneWife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);

                                                    decimal temBenefits = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                                                    if (ageSpan >= 10)
                                                    {
                                                        if (WifeOlderGreater)
                                                            odlgerGreater = 1;
                                                    }
                                                    else
                                                    {
                                                        decimal WHBHusband70 = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                                                        WHBHusband70 = ColaUpSpousal(70, WHBHusband70, birthYearHusb, intAgeHusb, husbFRA);
                                                        int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                        decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                                        spousalBenefits = spousalBenefits / 2;

                                                        if (WHBHusband70 > spousalBenefits)
                                                        {
                                                            if (temBenefits > currBeneHusb && ageHusb >= husbFRA && (husbBenefitSource == benefit_source.Self || husbBenefitSource == benefit_source.None))
                                                            {
                                                                if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                                    Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                                currBeneHusb = temBenefits;
                                                                Globals.Instance.BoolSpousalChange = true;
                                                                Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                                Globals.Instance.BoolSpousalCliamedHusb = true;
                                                                AgeValueForSpousalAt70 = ageHusb;
                                                                ValueForWifeAt70 = currBeneHusb;
                                                                husbBenefitSource = benefit_source.Spouse;
                                                            }
                                                            else
                                                            {
                                                                Globals.Instance.BoolSpousalHubsAfter70 = true;
                                                                Globals.Instance.HusbSpousalBenefitAfter70 = temBenefits;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (temBenefits > currBeneHusb && (husbBenefitSource == benefit_source.Self || husbBenefitSource == benefit_source.None))
                                                            {
                                                                if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                                    Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                                currBeneHusb = temBenefits;
                                                                Globals.Instance.BoolSpousalChange = true;
                                                                Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                                Globals.Instance.BoolSpousalCliamedHusb = true;
                                                                AgeValueForSpousalAt70 = ageHusb;
                                                                ValueForWifeAt70 = currBeneHusb;
                                                                husbBenefitSource = benefit_source.Spouse;
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    currBeneWife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                                }
                                                currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intWifeAge, wifeFRA);
                                                Globals.Instance.BoolAge70ChangeWife = true;
                                                wifeBenefitSource = benefit_source.Self;

                                                decimal spousalhusbbene;
                                                if (husbBenefitSource == benefit_source.None && currBeneHusb == 0)
                                                {

                                                    if (intAgeHusb > husbFRA)
                                                    {
                                                        spousalhusbbene = AgeAbove68YearsOfWife();
                                                        spousalhusbbene = ColaUpSpousal(ageWife, spousalhusbbene, birthYearWife, intAgeWife, wifeFRA);
                                                    }
                                                    else
                                                    {
                                                        spousalhusbbene = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                                    }
                                                    spousalhusbbene = spousalhusbbene / 2;
                                                    if (currBeneHusb == 0)
                                                        Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                    currBeneHusb = spousalhusbbene;
                                                    husbBenefitSource = benefit_source.Spouse;
                                                }
                                                if (intAgeWife > wifeFRA)
                                                {
                                                    if (ageHusb < husbFRA)
                                                    {
                                                        spousalhusbbene = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                                                    }
                                                    else
                                                    {
                                                        spousalhusbbene = AgeAbove68YearsOfWife();
                                                        spousalhusbbene = ColaUpSpousal(ageWife, spousalhusbbene, birthYearWife, intAgeWife, wifeFRA);
                                                        spousalhusbbene = spousalhusbbene / 2;
                                                    }
                                                }
                                                else
                                                {
                                                    if (ageHusb < husbFRA)
                                                    {
                                                        spousalhusbbene = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                                                    }
                                                    else
                                                    {
                                                        spousalhusbbene = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                                        spousalhusbbene = spousalhusbbene / 2;
                                                    }
                                                }
                                                if (spousalhusbbene > currBeneHusb && (husbBenefitSource == benefit_source.Self || husbBenefitSource == benefit_source.None))
                                                {
                                                    decimal WHBHusband70 = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                                                    WHBHusband70 = ColaUpSpousal(70, WHBHusband70, birthYearHusb, intAgeHusb, husbFRA);
                                                    int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                    decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                                    spousalBenefits = spousalBenefits / 2;

                                                    if (WHBHusband70 > spousalBenefits)
                                                    {
                                                        if (ageHusb >= husbFRA)
                                                        {
                                                            if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                                Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                            currBeneHusb = spousalhusbbene;
                                                            husbBenefitSource = benefit_source.SpousalBenefit;
                                                            Globals.Instance.BoolSpousalChange = true;
                                                            Globals.Instance.BoolSpousalCliamedHusb = true;
                                                            Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                            AgeValueForSpousalAt70 = ageHusb;
                                                            ValueForWifeAt70 = currBeneHusb;
                                                            workingBenefitsForHusb = ageHusb.ToString();
                                                            showWorkingBenefitsForHusb = true;
                                                        }
                                                        else
                                                        {
                                                            Globals.Instance.BoolSpousalHubsAfter70 = true;
                                                            Globals.Instance.HusbSpousalBenefitAfter70 = spousalhusbbene;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                            Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                        currBeneHusb = spousalhusbbene;
                                                        husbBenefitSource = benefit_source.SpousalBenefit;
                                                        Globals.Instance.BoolSpousalChange = true;
                                                        Globals.Instance.BoolSpousalCliamedHusb = true;
                                                        Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                        AgeValueForSpousalAt70 = ageHusb;
                                                        ValueForWifeAt70 = currBeneHusb;
                                                        workingBenefitsForHusb = ageHusb.ToString();
                                                        showWorkingBenefitsForHusb = true;
                                                    }
                                                }
                                                FbuildGridAt70 = true;
                                            }
                                        }

                                    }
                                    else if (ageWife == 70 && wifeBenefitSource != benefit_source.Self)
                                    {
                                        //code to check if age greater than FRA
                                        if (intAgeWife > wifeFRA)
                                        {
                                            //value at FRA for Wife
                                            multiply = AgeAbove68YearsOfWife();
                                            if (currBeneWife == 0)
                                            {
                                                Globals.Instance.WifeStartingBenefitName = Constants.Working;
                                                Globals.Instance.BoolAge70StartingAgeWife = true;
                                            }
                                            currBeneWife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);

                                            decimal temBenefits = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                                            if (ageSpan >= 10)
                                            {
                                                if (WifeOlderGreater)
                                                    odlgerGreater = 1;
                                            }
                                            else
                                            {
                                                decimal WHBHusband70 = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                                                WHBHusband70 = ColaUpSpousal(70, WHBHusband70, birthYearHusb, intAgeHusb, husbFRA);
                                                int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                                spousalBenefits = spousalBenefits / 2;

                                                if (WHBHusband70 > spousalBenefits)
                                                {
                                                    if (temBenefits > currBeneHusb && ageHusb >= husbFRA && (husbBenefitSource == benefit_source.Self || husbBenefitSource == benefit_source.None))
                                                    {
                                                        if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                            Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                        currBeneHusb = temBenefits;
                                                        Globals.Instance.BoolSpousalChange = true;
                                                        Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                        Globals.Instance.BoolSpousalCliamedHusb = true;
                                                        AgeValueForSpousalAt70 = ageHusb;
                                                        ValueForWifeAt70 = currBeneHusb;
                                                        husbBenefitSource = benefit_source.Spouse;
                                                    }
                                                    else
                                                    {
                                                        Globals.Instance.BoolSpousalHubsAfter70 = true;
                                                        Globals.Instance.HusbSpousalBenefitAfter70 = temBenefits;
                                                    }
                                                }
                                                else
                                                {
                                                    if (temBenefits > currBeneHusb && (husbBenefitSource == benefit_source.Self || husbBenefitSource == benefit_source.None))
                                                    {
                                                        if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                            Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                        currBeneHusb = temBenefits;
                                                        Globals.Instance.BoolSpousalChange = true;
                                                        Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                        Globals.Instance.BoolSpousalCliamedHusb = true;
                                                        AgeValueForSpousalAt70 = ageHusb;
                                                        ValueForWifeAt70 = currBeneHusb;
                                                        husbBenefitSource = benefit_source.Spouse;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            currBeneWife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                        }
                                        currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intWifeAge, wifeFRA);
                                        Globals.Instance.BoolAge70ChangeWife = true;
                                        wifeBenefitSource = benefit_source.Self;

                                        decimal spousalhusbbene;
                                        if (husbBenefitSource == benefit_source.None && currBeneHusb == 0)
                                        {

                                            if (intAgeHusb > husbFRA)
                                            {
                                                spousalhusbbene = AgeAbove68YearsOfWife();
                                                spousalhusbbene = ColaUpSpousal(ageWife, spousalhusbbene, birthYearWife, intAgeWife, wifeFRA);
                                            }
                                            else
                                            {
                                                spousalhusbbene = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                            }
                                            spousalhusbbene = spousalhusbbene / 2;
                                            if (currBeneHusb == 0)
                                                Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                            currBeneHusb = spousalhusbbene;
                                            husbBenefitSource = benefit_source.Spouse;
                                        }
                                        if (intAgeWife > wifeFRA)
                                        {
                                            if (ageHusb < husbFRA)
                                            {
                                                spousalhusbbene = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                                            }
                                            else
                                            {
                                                spousalhusbbene = AgeAbove68YearsOfWife();
                                                spousalhusbbene = ColaUpSpousal(ageWife, spousalhusbbene, birthYearWife, intAgeWife, wifeFRA);
                                                spousalhusbbene = spousalhusbbene / 2;
                                            }
                                        }
                                        else
                                        {
                                            if (ageHusb < husbFRA)
                                            {
                                                spousalhusbbene = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                                            }
                                            else
                                            {
                                                spousalhusbbene = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                                spousalhusbbene = spousalhusbbene / 2;
                                            }
                                        }
                                        if (spousalhusbbene > currBeneHusb && (husbBenefitSource == benefit_source.Self || husbBenefitSource == benefit_source.None))
                                        {
                                            decimal WHBHusband70 = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                                            WHBHusband70 = ColaUpSpousal(70, WHBHusband70, birthYearHusb, intAgeHusb, husbFRA);
                                            int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                            decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                            spousalBenefits = spousalBenefits / 2;

                                            if (WHBHusband70 > spousalBenefits)
                                            {
                                                if (ageHusb >= husbFRA)
                                                {
                                                    if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                        Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                    currBeneHusb = spousalhusbbene;
                                                    husbBenefitSource = benefit_source.SpousalBenefit;
                                                    Globals.Instance.BoolSpousalChange = true;
                                                    Globals.Instance.BoolSpousalCliamedHusb = true;
                                                    Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                    AgeValueForSpousalAt70 = ageHusb;
                                                    ValueForWifeAt70 = currBeneHusb;
                                                    workingBenefitsForHusb = ageHusb.ToString();
                                                    showWorkingBenefitsForHusb = true;
                                                }
                                                else
                                                {
                                                    Globals.Instance.BoolSpousalHubsAfter70 = true;
                                                    Globals.Instance.HusbSpousalBenefitAfter70 = spousalhusbbene;
                                                }
                                            }
                                            else
                                            {
                                                if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                    Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                currBeneHusb = spousalhusbbene;
                                                husbBenefitSource = benefit_source.SpousalBenefit;
                                                Globals.Instance.BoolSpousalChange = true;
                                                Globals.Instance.BoolSpousalCliamedHusb = true;
                                                Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                AgeValueForSpousalAt70 = ageHusb;
                                                ValueForWifeAt70 = currBeneHusb;
                                                workingBenefitsForHusb = ageHusb.ToString();
                                                showWorkingBenefitsForHusb = true;

                                            }

                                        }
                                        FbuildGridAt70 = true;
                                    }
                                    if (husbBenefitSource == benefit_source.Spouse)
                                    {
                                        if (husbBirthDate < limit1)
                                        {
                                            if (ageHusb == 70 && husbBenefitSource != benefit_source.SpousalBenefit && husbBenefitSource != benefit_source.Self)
                                            {
                                                if (intAgeHusb > husbFRA)
                                                {
                                                    //FRA value for husb
                                                    multiply = AgeAbove68YearsOfHusb();
                                                    if (currBeneHusb == 0)
                                                    {
                                                        Globals.Instance.HusbStartingBenefitName = Constants.Working;
                                                        Globals.Instance.BoolAge70StartingAgeHusb = true;
                                                    }
                                                    currBeneHusb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                                    //spousal benefits for wife at same age
                                                    decimal temBenefits = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                                                    temBenefits = temBenefits / 2;
                                                    if (ageSpan >= 10)
                                                    {
                                                        if (WifeOlderGreater)
                                                            odlgerGreater = 1;
                                                    }
                                                    else
                                                    {
                                                        decimal WHBWife70 = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                                                        WHBWife70 = ColaUpSpousal(70, WHBWife70, birthYearWife, intAgeWife, wifeFRA);
                                                        int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                        decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                                        spousalBenefits = spousalBenefits / 2;

                                                        if (WHBWife70 > spousalBenefits)
                                                        {
                                                            if (temBenefits > currBeneWife && ageWife >= wifeFRA && (wifeBenefitSource == benefit_source.Self && wifeBenefitSource == benefit_source.None))
                                                            {
                                                                if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                                    Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                                currBeneWife = temBenefits;
                                                                Globals.Instance.BoolSpousalChange = true;
                                                                Globals.Instance.BoolSpousalCliamedWife = true;
                                                                Globals.Instance.StringSpousalChangeAgeWife = true;
                                                                AgeValueForSpousalAt70 = ageWife;
                                                                ValueForWifeAt70 = currBeneWife;
                                                                wifeBenefitSource = benefit_source.Spouse;
                                                            }
                                                            else
                                                            {
                                                                Globals.Instance.BoolSpousalWifeAfter70 = true;
                                                                Globals.Instance.WifeSpousalBenefitAfter70 = temBenefits;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (temBenefits > currBeneWife && (wifeBenefitSource == benefit_source.Self && wifeBenefitSource == benefit_source.None))
                                                            {
                                                                if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                                    Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                                currBeneWife = temBenefits;
                                                                Globals.Instance.BoolSpousalChange = true;
                                                                Globals.Instance.BoolSpousalCliamedWife = true;
                                                                Globals.Instance.StringSpousalChangeAgeWife = true;
                                                                AgeValueForSpousalAt70 = ageWife;
                                                                ValueForWifeAt70 = currBeneWife;
                                                                wifeBenefitSource = benefit_source.Spouse;
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    currBeneHusb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                                }
                                                currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intHusbAge, husbFRA);
                                                Globals.Instance.BoolAge70ChangeHusb = true;
                                                at70 = true;
                                            }
                                        }
                                    }
                                    else if (ageHusb == 70 && husbBenefitSource != benefit_source.SpousalBenefit && husbBenefitSource != benefit_source.Self)
                                    {
                                        if (intAgeHusb > husbFRA)
                                        {
                                            //FRA value for husb
                                            multiply = AgeAbove68YearsOfHusb();
                                            if (currBeneHusb == 0)
                                            {
                                                Globals.Instance.HusbStartingBenefitName = Constants.Working;
                                                Globals.Instance.BoolAge70StartingAgeHusb = true;
                                            }
                                            currBeneHusb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                            //spousal benefits for wife at same age
                                            decimal temBenefits = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                                            temBenefits = temBenefits / 2;
                                            if (ageSpan >= 10)
                                            {
                                                if (WifeOlderGreater)
                                                    odlgerGreater = 1;
                                            }
                                            else
                                            {
                                                decimal WHBWife70 = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                                                WHBWife70 = ColaUpSpousal(70, WHBWife70, birthYearWife, intAgeWife, wifeFRA);
                                                int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                                spousalBenefits = spousalBenefits / 2;

                                                if (WHBWife70 > spousalBenefits)
                                                {
                                                    if (temBenefits > currBeneWife && ageWife >= wifeFRA && (wifeBenefitSource == benefit_source.Self && wifeBenefitSource == benefit_source.None))
                                                    {
                                                        if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                            Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                        currBeneWife = temBenefits;
                                                        Globals.Instance.BoolSpousalChange = true;
                                                        Globals.Instance.BoolSpousalCliamedWife = true;
                                                        Globals.Instance.StringSpousalChangeAgeWife = true;
                                                        AgeValueForSpousalAt70 = ageWife;
                                                        ValueForWifeAt70 = currBeneWife;
                                                        wifeBenefitSource = benefit_source.Spouse;
                                                    }
                                                    else
                                                    {
                                                        Globals.Instance.BoolSpousalWifeAfter70 = true;
                                                        Globals.Instance.WifeSpousalBenefitAfter70 = temBenefits;
                                                    }
                                                }
                                                else
                                                {
                                                    if (temBenefits > currBeneWife && (wifeBenefitSource == benefit_source.Self && wifeBenefitSource == benefit_source.None))
                                                    {
                                                        if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                            Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                        currBeneWife = temBenefits;
                                                        Globals.Instance.BoolSpousalChange = true;
                                                        Globals.Instance.BoolSpousalCliamedWife = true;
                                                        Globals.Instance.StringSpousalChangeAgeWife = true;
                                                        AgeValueForSpousalAt70 = ageWife;
                                                        ValueForWifeAt70 = currBeneWife;
                                                        wifeBenefitSource = benefit_source.Spouse;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            currBeneHusb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                        }
                                        currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intHusbAge, husbFRA);
                                        Globals.Instance.BoolAge70ChangeHusb = true;
                                        at70 = true;
                                    }
                                }
                                else
                                {
                                    if (wifeBenefitSource == benefit_source.Spouse)
                                    {
                                        if (wifeBirthDate < limit1)
                                        {
                                            if (ageWife == 70)
                                            {
                                                //code to check if age greater than FRA
                                                if (intAgeWife > wifeFRA)
                                                {
                                                    //value at FRA for wife
                                                    multiply = AgeAbove68YearsOfWife();
                                                    if (currBeneWife == 0)
                                                    {
                                                        Globals.Instance.WifeStartingBenefitName = Constants.Working;
                                                        Globals.Instance.BoolAge70StartingAgeWife = true;
                                                    }
                                                    currBeneWife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                                    //spousal benefits for husb at same age
                                                    decimal temBenefits = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                                                    temBenefits = Convert.ToInt32(temBenefits / 2);
                                                    if (ageSpan >= 10)
                                                    {
                                                        if (HusbOlderGreater)
                                                            odlgerGreater = 1;
                                                    }
                                                    else
                                                    {
                                                        decimal WHBHusband70 = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                                                        WHBHusband70 = ColaUpSpousal(70, WHBHusband70, birthYearHusb, intAgeHusb, husbFRA);
                                                        int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                        decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                                        spousalBenefits = spousalBenefits / 2;

                                                        if (WHBHusband70 > spousalBenefits)
                                                        {
                                                            if (temBenefits > currBeneHusb && ageHusb >= husbFRA && (husbBenefitSource != benefit_source.Spouse || husbBenefitSource != benefit_source.SpousalBenefit))
                                                            {
                                                                if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                                    Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                                currBeneHusb = temBenefits;
                                                                Globals.Instance.BoolSpousalChange = true;
                                                                Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                                Globals.Instance.BoolSpousalCliamedHusb = true;
                                                                AgeValueForSpousalAt70 = ageHusb;
                                                                ValueForWifeAt70 = currBeneHusb;
                                                                husbBenefitSource = benefit_source.Spouse;
                                                            }
                                                            else
                                                            {
                                                                Globals.Instance.BoolSpousalHubsAfter70 = true;
                                                                Globals.Instance.HusbSpousalBenefitAfter70 = temBenefits;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (temBenefits > currBeneHusb && (husbBenefitSource != benefit_source.Spouse || husbBenefitSource != benefit_source.SpousalBenefit))
                                                            {
                                                                if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                                    Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                                currBeneHusb = temBenefits;
                                                                Globals.Instance.BoolSpousalChange = true;
                                                                Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                                Globals.Instance.BoolSpousalCliamedHusb = true;
                                                                AgeValueForSpousalAt70 = ageHusb;
                                                                ValueForWifeAt70 = currBeneHusb;
                                                                husbBenefitSource = benefit_source.Spouse;
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    currBeneWife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                                }
                                                currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intWifeAge, wifeFRA);
                                                Globals.Instance.BoolAge70ChangeWife = true;
                                                FbuildGridAt70 = true;
                                            }
                                        }
                                    }
                                    else if (ageWife == 70 && wifeBenefitSource != benefit_source.Self)
                                    {
                                        //code to check if age greater than FRA
                                        if (intAgeWife > wifeFRA)
                                        {
                                            //value at FRA for wife
                                            multiply = AgeAbove68YearsOfWife();
                                            if (currBeneWife == 0)
                                            {
                                                Globals.Instance.WifeStartingBenefitName = Constants.Working;
                                                Globals.Instance.BoolAge70StartingAgeWife = true;
                                            }
                                            currBeneWife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                            //spousal benefits for husb at same age
                                            decimal temBenefits = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                                            temBenefits = Convert.ToInt32(temBenefits / 2);
                                            if (ageSpan >= 10)
                                            {
                                                if (HusbOlderGreater)
                                                    odlgerGreater = 1;
                                            }
                                            else
                                            {
                                                decimal WHBHusband70 = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                                                WHBHusband70 = ColaUpSpousal(70, WHBHusband70, birthYearHusb, intAgeHusb, husbFRA);
                                                int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                                spousalBenefits = spousalBenefits / 2;

                                                if (WHBHusband70 > spousalBenefits)
                                                {
                                                    if (temBenefits > currBeneHusb && ageHusb >= husbFRA && (husbBenefitSource != benefit_source.Spouse || husbBenefitSource != benefit_source.SpousalBenefit))
                                                    {
                                                        if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                            Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                        currBeneHusb = temBenefits;
                                                        Globals.Instance.BoolSpousalChange = true;
                                                        Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                        Globals.Instance.BoolSpousalCliamedHusb = true;
                                                        AgeValueForSpousalAt70 = ageHusb;
                                                        ValueForWifeAt70 = currBeneHusb;
                                                        husbBenefitSource = benefit_source.Spouse;
                                                    }
                                                    else
                                                    {
                                                        Globals.Instance.BoolSpousalHubsAfter70 = true;
                                                        Globals.Instance.HusbSpousalBenefitAfter70 = temBenefits;
                                                    }
                                                }
                                                else
                                                {
                                                    if (temBenefits > currBeneHusb && (husbBenefitSource != benefit_source.Spouse || husbBenefitSource != benefit_source.SpousalBenefit))
                                                    {
                                                        if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                            Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                        currBeneHusb = temBenefits;
                                                        Globals.Instance.BoolSpousalChange = true;
                                                        Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                        Globals.Instance.BoolSpousalCliamedHusb = true;
                                                        AgeValueForSpousalAt70 = ageHusb;
                                                        ValueForWifeAt70 = currBeneHusb;
                                                        husbBenefitSource = benefit_source.Spouse;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            currBeneWife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                        }
                                        currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intWifeAge, wifeFRA);
                                        Globals.Instance.BoolAge70ChangeWife = true;
                                        FbuildGridAt70 = true;
                                    }
                                }
                                #endregion Code when wife benefits are greater
                            }
                            #endregion code when one benefits is less than half of another

                            #region code when one benefits is greater than half of another
                            if (decBeneHusb > decBeneWife && decBeneWife > halfBenHusb)
                            {
                                if (ageHusb == 70 && husbBenefitSource == benefit_source.Spouse)
                                {
                                    if (husbBirthDate < limit1)
                                    {
                                        {
                                            //code to check if age greater than FRA
                                            if (intAgeHusb > husbFRA)
                                            {
                                                //value at FRA for husb
                                                multiply = AgeAbove68YearsOfHusb();
                                                if (currBeneHusb == 0)
                                                {
                                                    Globals.Instance.HusbStartingBenefitName = Constants.Working;
                                                    Globals.Instance.BoolAge70StartingAgeHusb = true;
                                                }
                                                currBeneHusb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                                //spousal benefits for wife at same age
                                                decimal temBenefits = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                                                temBenefits = Convert.ToInt32(temBenefits / 2);
                                                if (ageSpan >= 10)
                                                {
                                                    if (WifeOlderGreater)
                                                        odlgerGreater = 1;
                                                }
                                                else
                                                {
                                                    decimal WHBWife70 = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                                                    WHBWife70 = ColaUpSpousal(70, WHBWife70, birthYearWife, intAgeWife, wifeFRA);
                                                    int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                    decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                                    spousalBenefits = spousalBenefits / 2;

                                                    if (WHBWife70 > spousalBenefits)
                                                    {
                                                        if (temBenefits > currBeneWife && ageWife >= wifeFRA && (wifeBenefitSource == benefit_source.Self || wifeBenefitSource == benefit_source.None))
                                                        {
                                                            if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                                Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                            currBeneWife = temBenefits;
                                                            Globals.Instance.BoolSpousalCliamedWife = true;
                                                            Globals.Instance.BoolSpousalChange = true;
                                                            Globals.Instance.StringSpousalChangeAgeWife = true;
                                                            AgeValueForSpousalAt70 = ageWife;
                                                            ValueForWifeAt70 = currBeneWife;
                                                            wifeBenefitSource = benefit_source.Spouse;
                                                        }
                                                        else
                                                        {
                                                            Globals.Instance.BoolSpousalWifeAfter70 = true;
                                                            Globals.Instance.WifeSpousalBenefitAfter70 = temBenefits;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (temBenefits > currBeneWife && (wifeBenefitSource == benefit_source.Self || wifeBenefitSource == benefit_source.None))
                                                        {
                                                            if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                                Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                            currBeneWife = temBenefits;
                                                            Globals.Instance.BoolSpousalCliamedWife = true;
                                                            Globals.Instance.BoolSpousalChange = true;
                                                            Globals.Instance.StringSpousalChangeAgeWife = true;
                                                            AgeValueForSpousalAt70 = ageWife;
                                                            ValueForWifeAt70 = currBeneWife;
                                                            wifeBenefitSource = benefit_source.Spouse;
                                                        }

                                                    }
                                                }
                                            }
                                            else
                                            {
                                                currBeneHusb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                            }
                                            currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intHusbAge, husbFRA);
                                            husbBenefitSource = benefit_source.Self;
                                            Globals.Instance.BoolAge70ChangeHusb = true;
                                        }
                                    }
                                }
                                else if (ageHusb == 70 && husbBenefitSource != benefit_source.Self)
                                {
                                    //code to check if age greater than FRA
                                    if (intAgeHusb > husbFRA)
                                    {
                                        //value at FRA for husb
                                        multiply = AgeAbove68YearsOfHusb();
                                        if (currBeneHusb == 0)
                                        {
                                            Globals.Instance.HusbStartingBenefitName = Constants.Working;
                                            Globals.Instance.BoolAge70StartingAgeHusb = true;
                                        }
                                        currBeneHusb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                        //spousal benefits for wife at same age
                                        decimal temBenefits = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                                        temBenefits = Convert.ToInt32(temBenefits / 2);
                                        if (ageSpan >= 10)
                                        {
                                            if (WifeOlderGreater)
                                                odlgerGreater = 1;
                                        }
                                        else
                                        {
                                            decimal WHBWife70 = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                                            WHBWife70 = ColaUpSpousal(70, WHBWife70, birthYearWife, intAgeWife, wifeFRA);
                                            int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                            decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                            spousalBenefits = spousalBenefits / 2;

                                            if (WHBWife70 > spousalBenefits)
                                            {
                                                if (temBenefits > currBeneWife && ageWife >= wifeFRA && (wifeBenefitSource == benefit_source.Self || wifeBenefitSource == benefit_source.None))
                                                {
                                                    if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                        Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                    currBeneWife = temBenefits;
                                                    Globals.Instance.BoolSpousalCliamedWife = true;
                                                    Globals.Instance.BoolSpousalChange = true;
                                                    Globals.Instance.StringSpousalChangeAgeWife = true;
                                                    AgeValueForSpousalAt70 = ageWife;
                                                    ValueForWifeAt70 = currBeneWife;
                                                    wifeBenefitSource = benefit_source.Spouse;
                                                }
                                                else
                                                {
                                                    Globals.Instance.BoolSpousalWifeAfter70 = true;
                                                    Globals.Instance.WifeSpousalBenefitAfter70 = temBenefits;
                                                }
                                            }
                                            else
                                            {
                                                if (temBenefits > currBeneWife && (wifeBenefitSource == benefit_source.Self || wifeBenefitSource == benefit_source.None))
                                                {
                                                    if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                                        Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                                    currBeneWife = temBenefits;
                                                    Globals.Instance.BoolSpousalCliamedWife = true;
                                                    Globals.Instance.BoolSpousalChange = true;
                                                    Globals.Instance.StringSpousalChangeAgeWife = true;
                                                    AgeValueForSpousalAt70 = ageWife;
                                                    ValueForWifeAt70 = currBeneWife;
                                                    wifeBenefitSource = benefit_source.Spouse;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        currBeneHusb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                    }
                                    currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intHusbAge, husbFRA);
                                    husbBenefitSource = benefit_source.Self;
                                    Globals.Instance.BoolAge70ChangeHusb = true;
                                }
                            }
                            else
                            {
                                if (decBeneHusb < decBeneWife && decBeneHusb > halfbenWife)
                                {
                                    if (wifeBenefitSource == benefit_source.Spouse)
                                    {
                                        if (wifeBirthDate < limit1)
                                        {
                                            if (ageWife == 70)
                                            {
                                                //code to check if age greater than FRA
                                                if (intAgeWife > wifeFRA)
                                                {
                                                    //value at FRA for wife
                                                    multiply = AgeAbove68YearsOfWife();
                                                    if (currBeneWife == 0)
                                                    {
                                                        Globals.Instance.WifeStartingBenefitName = Constants.Working;
                                                        Globals.Instance.BoolAge70StartingAgeWife = true;
                                                    }
                                                    currBeneWife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                                    //spousal benefits for husb at same age
                                                    decimal temBenefits = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                                                    temBenefits = Convert.ToInt32(temBenefits / 2);
                                                    if (ageSpan >= 10)
                                                    {
                                                        if (HusbOlderGreater)
                                                            odlgerGreater = 1;
                                                    }
                                                    else
                                                    {
                                                        decimal WHBHusband70 = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                                                        WHBHusband70 = ColaUpSpousal(70, WHBHusband70, birthYearHusb, intAgeHusb, husbFRA);
                                                        int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                        decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                                        spousalBenefits = spousalBenefits / 2;

                                                        if (WHBHusband70 > spousalBenefits)
                                                        {
                                                            if (temBenefits > currBeneHusb && ageHusb >= husbFRA && (husbBenefitSource == benefit_source.Self || husbBenefitSource == benefit_source.None))
                                                            {
                                                                if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                                    Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                                currBeneHusb = temBenefits;
                                                                Globals.Instance.BoolSpousalChange = true;
                                                                Globals.Instance.BoolSpousalCliamedHusb = true;
                                                                Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                                AgeValueForSpousalAt70 = ageHusb;
                                                                ValueForWifeAt70 = currBeneHusb;
                                                                husbBenefitSource = benefit_source.Spouse;
                                                            }
                                                            else
                                                            {
                                                                Globals.Instance.BoolSpousalHubsAfter70 = true;
                                                                Globals.Instance.HusbSpousalBenefitAfter70 = temBenefits;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (temBenefits > currBeneHusb && (husbBenefitSource == benefit_source.Self || husbBenefitSource == benefit_source.None))
                                                            {
                                                                if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                                    Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                                currBeneHusb = temBenefits;
                                                                Globals.Instance.BoolSpousalChange = true;
                                                                Globals.Instance.BoolSpousalCliamedHusb = true;
                                                                Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                                AgeValueForSpousalAt70 = ageHusb;
                                                                ValueForWifeAt70 = currBeneHusb;
                                                                husbBenefitSource = benefit_source.Spouse;
                                                            }
                                                        }

                                                    }
                                                }
                                                else
                                                {
                                                    currBeneWife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                                }
                                                currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intWifeAge, wifeFRA);// (currBeneWife) * Convert.ToDecimal(Math.Pow(1.025, (70 - intAgeWife)));
                                                Globals.Instance.BoolAge70ChangeWife = true;
                                                wifeBenefitSource = benefit_source.Self;
                                            }
                                        }
                                    }
                                    else if (ageWife == 70 && wifeBenefitSource != benefit_source.Self)
                                    {
                                        //code to check if age greater than FRA
                                        if (intAgeWife > wifeFRA)
                                        {
                                            //value at FRA for wife
                                            multiply = AgeAbove68YearsOfWife();
                                            if (currBeneWife == 0)
                                            {
                                                Globals.Instance.WifeStartingBenefitName = Constants.Working;
                                                Globals.Instance.BoolAge70StartingAgeWife = true;
                                            }
                                            currBeneWife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                            //spousal benefits for husb at same age
                                            decimal temBenefits = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                                            temBenefits = Convert.ToInt32(temBenefits / 2);
                                            if (ageSpan >= 10)
                                            {
                                                if (HusbOlderGreater)
                                                    odlgerGreater = 1;
                                            }
                                            else
                                            {
                                                decimal WHBHusband70 = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                                                WHBHusband70 = ColaUpSpousal(70, WHBHusband70, birthYearHusb, intAgeHusb, husbFRA);
                                                int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                                                decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                                spousalBenefits = spousalBenefits / 2;

                                                if (WHBHusband70 > spousalBenefits)
                                                {
                                                    if (temBenefits > currBeneHusb && ageHusb >= husbFRA && (husbBenefitSource == benefit_source.Self || husbBenefitSource == benefit_source.None))
                                                    {
                                                        if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                            Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                        currBeneHusb = temBenefits;
                                                        Globals.Instance.BoolSpousalChange = true;
                                                        Globals.Instance.BoolSpousalCliamedHusb = true;
                                                        Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                        AgeValueForSpousalAt70 = ageHusb;
                                                        ValueForWifeAt70 = currBeneHusb;
                                                        husbBenefitSource = benefit_source.Spouse;
                                                    }
                                                    else
                                                    {
                                                        Globals.Instance.BoolSpousalHubsAfter70 = true;
                                                        Globals.Instance.HusbSpousalBenefitAfter70 = temBenefits;
                                                    }
                                                }
                                                else
                                                {
                                                    if (temBenefits > currBeneHusb && (husbBenefitSource == benefit_source.Self || husbBenefitSource == benefit_source.None))
                                                    {
                                                        if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                                            Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                                        currBeneHusb = temBenefits;
                                                        Globals.Instance.BoolSpousalChange = true;
                                                        Globals.Instance.BoolSpousalCliamedHusb = true;
                                                        Globals.Instance.StringSpousalChangeAgeHusb = true;
                                                        AgeValueForSpousalAt70 = ageHusb;
                                                        ValueForWifeAt70 = currBeneHusb;
                                                        husbBenefitSource = benefit_source.Spouse;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            currBeneWife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                        }
                                        currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intWifeAge, wifeFRA);// (currBeneWife) * Convert.ToDecimal(Math.Pow(1.025, (70 - intAgeWife)));
                                        Globals.Instance.BoolAge70ChangeWife = true;
                                        wifeBenefitSource = benefit_source.Self;
                                    }
                                }
                            }
                            #endregion code when one benefits is greater than half of another
                        }
                        else
                        {
                            #region Birth date greater than 1954
                            if (ageWife == 70 && wifeBenefitSource != benefit_source.Self)
                            {
                                decimal workingBenefitsat70 = 0;
                                if (intAgeWife > wifeFRA)
                                {
                                    multiply = AgeAbove68YearsOfWife();
                                    workingBenefitsat70 = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                }
                                else
                                {
                                    workingBenefitsat70 = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                }
                                workingBenefitsat70 = ColaUpSpousal(ageWife, workingBenefitsat70, birthYearWife, intWifeAge, wifeFRA);
                                if (workingBenefitsat70 > currBeneWife)
                                {
                                    if (wifeBenefitSource == benefit_source.Spouse)
                                    {
                                        if (wifeBirthDate < limit1)
                                        {
                                            if (currBeneWife == 0)
                                            {
                                                Globals.Instance.BoolAge70StartingAgeWife = true;
                                                Globals.Instance.WifeStartingBenefitName = Constants.Working;
                                            }

                                            currBeneWife = workingBenefitsat70;
                                            wifeBenefitSource = benefit_source.Self;
                                            Globals.Instance.BoolAge70ChangeWife = true;
                                        }
                                    }
                                    else
                                    {
                                        if (currBeneWife == 0)
                                        {
                                            Globals.Instance.BoolAge70StartingAgeWife = true;
                                            Globals.Instance.WifeStartingBenefitName = Constants.Working;
                                        }

                                        currBeneWife = workingBenefitsat70;
                                        wifeBenefitSource = benefit_source.Self;
                                        Globals.Instance.BoolAge70ChangeWife = true;
                                    }
                                }
                                decimal spousalhusbbene;
                                if (intAgeWife > wifeFRA)
                                {
                                    if (ageHusb >= husbFRA)
                                    {
                                        spousalhusbbene = AgeAbove68YearsOfWife();
                                        spousalhusbbene = ColaUpSpousal(ageWife, spousalhusbbene, birthYearWife, intAgeWife, wifeFRA);
                                        spousalhusbbene = spousalhusbbene / 2;
                                    }
                                    else
                                    {
                                        spousalhusbbene = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                                    }
                                }
                                else
                                {
                                    if (ageHusb >= husbFRA)
                                    {
                                        spousalhusbbene = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                        spousalhusbbene = Convert.ToInt32(spousalhusbbene / 2);
                                    }
                                    else
                                    {
                                        spousalhusbbene = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                                    }
                                }
                                if (currBeneHusb < spousalhusbbene && ageHusb >= husbFRA && husbBenefitSource == benefit_source.Self && wifeBenefitSource == benefit_source.Self)
                                {
                                    if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                        Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                                    currBeneHusb = spousalhusbbene;
                                    Globals.Instance.StringSpousalChangeAgeHusb = true;
                                    Globals.Instance.BoolSpousalChange = true;
                                    AgeValueForSpousalAt70 = ageHusb;
                                    ValueForWifeAt70 = currBeneHusb;
                                    husbBenefitSource = benefit_source.Spouse;
                                    Globals.Instance.BoolSpousalCliamedHusb = true;
                                }
                            }
                            if (ageHusb == 70 && husbBenefitSource != benefit_source.Self)
                            {
                                decimal workingBenefitsat70 = 0;
                                if (intAgeHusb > husbFRA)
                                {
                                    multiply = AgeAbove68YearsOfHusb();
                                    workingBenefitsat70 = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                }
                                else
                                {
                                    workingBenefitsat70 = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                }
                                workingBenefitsat70 = ColaUpSpousal(ageHusb, workingBenefitsat70, birthYearHusb, intHusbAge, husbFRA);
                                if (currBeneHusb < workingBenefitsat70)
                                {
                                    if (husbBenefitSource == benefit_source.Spouse)
                                    {
                                        if (husbBirthDate < limit1)
                                        {
                                            if (currBeneHusb == 0)
                                            {
                                                Globals.Instance.HusbStartingBenefitName = Constants.Working;
                                                Globals.Instance.BoolAge70StartingAgeHusb = true;
                                            }
                                            currBeneHusb = workingBenefitsat70;
                                            husbBenefitSource = benefit_source.Self;
                                            Globals.Instance.BoolAge70ChangeHusb = true;
                                        }
                                    }
                                    else
                                    {
                                        if (currBeneHusb == 0)
                                        {
                                            Globals.Instance.HusbStartingBenefitName = Constants.Working;
                                            Globals.Instance.BoolAge70StartingAgeHusb = true;
                                        }
                                        currBeneHusb = workingBenefitsat70;
                                        husbBenefitSource = benefit_source.Self;
                                        Globals.Instance.BoolAge70ChangeHusb = true;
                                    }
                                }
                                decimal spousalhusbbene;
                                if (intAgeHusb > husbFRA)
                                {
                                    if (ageWife >= wifeFRA)
                                    {
                                        spousalhusbbene = AgeAbove68YearsOfWife();
                                        spousalhusbbene = ColaUpSpousal(ageHusb, spousalhusbbene, birthYearHusb, intAgeHusb, husbFRA);
                                        spousalhusbbene = spousalhusbbene / 2;
                                    }
                                    else
                                    {
                                        spousalhusbbene = SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                    }
                                }
                                else
                                {
                                    if (ageWife < wifeFRA)
                                    {
                                        spousalhusbbene = SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                    }
                                    else
                                    {
                                        spousalhusbbene = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                        spousalhusbbene = spousalhusbbene / 2;
                                    }
                                }
                                if (currBeneWife < spousalhusbbene && ageWife >= wifeFRA && wifeBenefitSource == benefit_source.Self && husbBenefitSource == benefit_source.Self)
                                {
                                    if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                        Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                                    currBeneWife = spousalhusbbene;
                                    Globals.Instance.StringSpousalChangeAgeWife = true;
                                    Globals.Instance.BoolSpousalChange = true;
                                    AgeValueForSpousalAt70 = ageWife;
                                    ValueForWifeAt70 = currBeneWife;
                                    wifeBenefitSource = benefit_source.Spouse;
                                    Globals.Instance.BoolSpousalCliamedWife = true;
                                }
                            }
                            #endregion Birth date greater than 1954
                        }
                        #region code to set the spousal benefit after the user age 70 for spouse
                        if (Globals.Instance.BoolSpousalWifeAfter70 && wifeBirthDate < limit1 && currBeneWife == 0 && ageWife >= wifeFRA && ageWife < 70 && ageHusb >= 70)
                        {
                            if (currBeneWife == 0 || wifeFirstPayAge == ageWife)
                                Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                            currBeneWife = Globals.Instance.WifeSpousalBenefitAfter70;
                            Globals.Instance.BoolSpousalCliamedWife = true;
                            Globals.Instance.BoolSpousalChange = true;
                            Globals.Instance.StringSpousalChangeAgeWife = true;
                            AgeValueForSpousalAt70 = ageWife;
                            ValueForWifeAt70 = currBeneWife;
                            wifeBenefitSource = benefit_source.Spouse;
                            Globals.Instance.BoolSpousalWifeAfter70 = false;
                        }
                        if (Globals.Instance.BoolSpousalHubsAfter70 && husbBirthDate < limit1 && currBeneHusb == 0 && ageHusb >= husbFRA && ageHusb < 70 && ageWife >= 70)
                        {
                            if (currBeneHusb == 0 || husbFirstPayAge == ageHusb)
                                Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                            currBeneHusb = Globals.Instance.HusbSpousalBenefitAfter70;
                            Globals.Instance.BoolSpousalChange = true;
                            Globals.Instance.StringSpousalChangeAgeHusb = true;
                            Globals.Instance.BoolSpousalCliamedHusb = true;
                            AgeValueForSpousalAt70 = ageHusb;
                            ValueForWifeAt70 = currBeneHusb;
                            husbBenefitSource = benefit_source.Spouse;
                            Globals.Instance.BoolSpousalHubsAfter70 = false;
                        }
                        #endregion code to set the spousal benefit after the user age 70 for spouse
                    }
                    #endregion Spousal Stratgey code at the age 70

                    #endregion code when age difference between0-4

                    #endregion CODE FOR CAL OF STRATEGY FOR SPOUSE AND HUS WHEN AGE 70

                    #region CODE FOR CLAIM AND SUSPEND WHEN HIGHER EARNER IS 66

                    ////  Calc Claim & Suspend when high earner is 66
                    //if (strategy == calc_strategy.Suspend && wifeBenefitSource != benefit_source.Spouse && husbBenefitSource != benefit_source.Spouse
                    // && wifeBenefitSource != benefit_source.SpousalBenefit && husbBenefitSource != benefit_source.SpousalBenefit)
                    //{
                    //    decimal decBeneWifeFRA = decBeneWife, decBeneHusbFRA = decBeneHusb;
                    //    if (intAgeWife > wifeFRA)
                    //    {
                    //        decBeneWifeFRA = AgeAbove68YearsOfWife();
                    //    }

                    //    if (intAgeHusb > husbFRA)
                    //    {
                    //        decBeneHusbFRA = AgeAbove68YearsOfHusb();
                    //    }
                    //    //needAsterisk = false;
                    //    if (ageHusb >= ageWife)
                    //    {
                    //        if (birthYearWife > 1954 && birthYearHusb > 1954)
                    //        {
                    //            if ((decBeneWife < decBeneHusb) && (startAge == calc_start.fra6667))
                    //            {
                    //                //code changed on 05/26/2015 to fit the age span more than 4 years so taht in cliam and suspend get benefits from FRA and not from age 70
                    //                if ((ageSpan >= 4) && (ageHusb >= husbFRA) && (ageHusb < 70) && ageWife >= wifeFRA && wifeBirthDate < limit)
                    //                {
                    //                    spousalBene = ColaUpSpousal(ageWife, decBeneWifeFRA, birthYearWife, intAgeWife, wifeFRA); // Change if COLA is YES 
                    //                    spousalBene /= 2;
                    //                    wifeFRAYear = FRAYear(wifeBirthDate);
                    //                    currBeneHusb = spousalBene;
                    //                    husbFirstPayAge = ageHusb;
                    //                    husbBenefitSource = benefit_source.Spouse;
                    //                    //gridNote.Text += "  Husband claimed and suspended at age " + ageHusb.ToString() + ".";
                    //                }
                    //                if (((ageHusb > husbFRA) && (ageWife == wifeFRA) && (ageHusb < husbFRA + 4) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && husbBirthDate < limit)  /* husb 5-7 yrs older */
                    //                || (ageWife >= wifeFRA) && (ageWife < 70) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && husbBirthDate < limit)       /*((ageHusb == husbFRA) && (ageWife >= wifeFRA))) husb 0-4 yrs older */
                    //                {
                    //                    spousalBene = ColaUpSpousal(ageHusb, decBeneHusbFRA, birthYearHusb, intAgeHusb, husbFRA); // Change if COLA is YES 
                    //                    spousalBene /= 2;
                    //                    husbFRAYear = FRAYear(husbBirthDate);
                    //                    currBeneWife = spousalBene;
                    //                    wifeFirstPayAge = ageWife;
                    //                    wifeBenefitSource = benefit_source.Spouse;
                    //                    //gridNote.Text += "  Husband claimed and suspended at age " + ageHusb.ToString() + ".";
                    //                    //if (ageHusb < 70)
                    //                    //    needAsterisk = true;
                    //                }
                    //            }
                    //        }
                    //        else
                    //        {
                    //            if ((decBeneWife < decBeneHusb) && (startAge == calc_start.fra6667))
                    //            {
                    //                //code changed on 05/26/2015 to fit the age span more than 4 years so taht in cliam and suspend get benefits from FRA and not from age 70
                    //                if ((ageSpan >= 4) && (ageHusb >= husbFRA) && (ageHusb < 70) && ageWife >= wifeFRA && wifeBirthDate < limit)
                    //                {
                    //                    spousalBene = ColaUpSpousal(ageWife, decBeneWifeFRA, birthYearWife, intAgeWife, wifeFRA); // Change if COLA is YES 
                    //                    spousalBene /= 2;
                    //                    wifeFRAYear = FRAYear(wifeBirthDate);
                    //                    currBeneHusb = spousalBene;
                    //                    husbFirstPayAge = ageHusb;
                    //                    husbBenefitSource = benefit_source.Spouse;
                    //                    //gridNote.Text += "  Husband claimed and suspended at age " + ageHusb.ToString() + ".";
                    //                }
                    //                if (((ageHusb > husbFRA) && (ageWife == wifeFRA) && (ageHusb < husbFRA + 4) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && husbBirthDate < limit)  /* husb 5-7 yrs older */
                    //                || (ageWife >= wifeFRA) && (ageWife < 70) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && husbBirthDate < limit)       /*((ageHusb == husbFRA) && (ageWife >= wifeFRA))) husb 0-4 yrs older */
                    //                {
                    //                    spousalBene = ColaUpSpousal(ageHusb, decBeneHusbFRA, birthYearHusb, intAgeHusb, husbFRA); // Change if COLA is YES 
                    //                    spousalBene = spousalBene / 2;
                    //                    husbFRAYear = FRAYear(husbBirthDate);
                    //                    currBeneWife = spousalBene;
                    //                    wifeFirstPayAge = ageWife;
                    //                    wifeBenefitSource = benefit_source.Spouse;
                    //                    //gridNote.Text += "  Husband claimed and suspended at age " + ageHusb.ToString() + ".";
                    //                    //if (ageHusb < 70)
                    //                    //    needAsterisk = true;
                    //                }
                    //            }
                    //        }

                    //        if ((birthYearHusb == 1957 || birthYearHusb == 1958 || birthYearHusb == 1959) && (birthYearWife == 1960 || birthYearWife == 1961 || birthYearWife == 1962))
                    //        {

                    //            if ((decBeneWife > decBeneHusb) && (startAge == calc_start.fra6667))
                    //            {
                    //                //code changed on 05/26/2015 to fit the age span more than 4 years so taht in cliam and suspend get benefits from FRA and not from age 70
                    //                if ((ageSpan >= 4) && (ageWife >= wifeFRA) && (ageWife < 70) && ageHusb >= husbFRA && husbBirthDate < limit)
                    //                {
                    //                    spousalBene = ColaUpSpousal(ageHusb, decBeneHusbFRA, birthYearHusb, intAgeHusb, husbFRA); // Change if COLA is YES 
                    //                    spousalBene /= 2;
                    //                    husbFRAYear = FRAYear(husbBirthDate);
                    //                    currBeneWife = spousalBene;
                    //                    wifeFirstPayAge = ageWife;
                    //                    wifeBenefitSource = benefit_source.Spouse;
                    //                    //gridNote.Text += "  Husband claimed and suspended at age " + ageHusb.ToString() + ".";
                    //                }

                    //                if (((ageWife > wifeFRA) && (ageHusb == husbFRA) && (ageWife < wifeFRA + 4) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && wifeBirthDate < limit) /* wife 5-7 yrs older */
                    //                || (ageHusb > (husbFRA + (ageHusb - ageWife))) && (ageHusb < 70) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && wifeBirthDate < limit)      /*((ageWife == wifeFRA) && (ageHusb >= husbFRA))) wife 0-4 yrs older */
                    //                {
                    //                    spousalBene = ColaUpSpousal(ageWife, decBeneWifeFRA, birthYearWife, intAgeWife, wifeFRA);//Change if COLA is YES 
                    //                    spousalBene /= 2;
                    //                    wifeFRAYear = FRAYear(wifeBirthDate);
                    //                    currBeneHusb = spousalBene;
                    //                    husbFirstPayAge = ageHusb;
                    //                    husbBenefitSource = benefit_source.Spouse;
                    //                    //gridNote.Text += "  Wife claimed and suspended at age " + ageWife.ToString() + ".";
                    //                    //if (ageWife < 70)
                    //                    //    needAsterisk = true;
                    //                }
                    //            }
                    //        }
                    //        else
                    //        {
                    //            if ((birthYearHusb == 1958 || birthYearHusb == 1959) && (birthYearWife == 1959 || birthYearWife == 1958))
                    //            {
                    //                if ((decBeneWife > decBeneHusb) && (startAge == calc_start.fra6667))
                    //                {
                    //                    //code changed on 05/26/2015 to fit the age span more than 4 years so taht in cliam and suspend get benefits from FRA and not from age 70
                    //                    if ((ageSpan >= 4) && (ageWife >= wifeFRA) && (ageWife < 70) && ageHusb >= husbFRA && husbBirthDate < limit)
                    //                    {
                    //                        spousalBene = ColaUpSpousal(ageHusb, decBeneHusbFRA, birthYearHusb, intAgeHusb, husbFRA); // Change if COLA is YES 
                    //                        spousalBene /= 2;
                    //                        husbFRAYear = FRAYear(husbBirthDate);
                    //                        currBeneWife = spousalBene;
                    //                        wifeFirstPayAge = ageWife;
                    //                        wifeBenefitSource = benefit_source.Spouse;
                    //                        //gridNote.Text += "  Husband claimed and suspended at age " + ageHusb.ToString() + ".";
                    //                    }

                    //                    if (((ageWife > wifeFRA) && (ageHusb == husbFRA) && (ageWife < wifeFRA + 4) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && wifeBirthDate < limit) /* wife 5-7 yrs older */
                    //                    || (ageWife >= husbFRA) && (ageHusb < 70) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && wifeBirthDate < limit)      /*((ageWife == wifeFRA) && (ageHusb >= husbFRA))) wife 0-4 yrs older */
                    //                    {
                    //                        spousalBene = ColaUpSpousal(ageWife, decBeneWifeFRA, birthYearWife, intAgeWife, wifeFRA);//Change if COLA is YES 
                    //                        spousalBene /= 2;
                    //                        wifeFRAYear = FRAYear(wifeBirthDate);
                    //                        currBeneHusb = spousalBene;
                    //                        husbFirstPayAge = ageHusb;
                    //                        husbBenefitSource = benefit_source.Spouse;
                    //                        //gridNote.Text += "  Wife claimed and suspended at age " + ageWife.ToString() + ".";
                    //                        //if (ageWife < 70)
                    //                        //    needAsterisk = true;
                    //                    }
                    //                }
                    //            }
                    //            else
                    //            {
                    //                if ((decBeneWife > decBeneHusb) && (startAge == calc_start.fra6667))
                    //                {
                    //                    //code changed on 05/26/2015 to fit the age span more than 4 years so taht in cliam and suspend get benefits from FRA and not from age 70
                    //                    if (ageSpan >= 4 && ageWife >= wifeFRA && (ageWife < 70) && ageHusb >= husbFRA && husbBirthDate < limit)
                    //                    {
                    //                        spousalBene = ColaUpSpousal(ageHusb, decBeneHusbFRA, birthYearHusb, intAgeHusb, husbFRA); // Change if COLA is YES 
                    //                        spousalBene /= 2;
                    //                        husbFRAYear = FRAYear(husbBirthDate);
                    //                        currBeneWife = spousalBene;
                    //                        wifeFirstPayAge = ageWife;
                    //                        wifeBenefitSource = benefit_source.Spouse;
                    //                        //gridNote.Text += "  Husband claimed and suspended at age " + ageHusb.ToString() + ".";
                    //                    }
                    //                    if (((ageWife > wifeFRA) && (ageHusb == husbFRA) && (ageWife < wifeFRA + 4) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && wifeBirthDate < limit) /* wife 5-7 yrs older */
                    //                    || (ageWife >= husbFRA) && (ageHusb < 70) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && wifeBirthDate < limit)      /*((ageWife == wifeFRA) && (ageHusb >= husbFRA))) wife 0-4 yrs older */
                    //                    {
                    //                        spousalBene = ColaUpSpousal(ageWife, decBeneWifeFRA, birthYearWife, intAgeWife, wifeFRA);//Change if COLA is YES 
                    //                        spousalBene /= 2;
                    //                        wifeFRAYear = FRAYear(wifeBirthDate);
                    //                        currBeneHusb = spousalBene;
                    //                        husbFirstPayAge = ageHusb;
                    //                        husbBenefitSource = benefit_source.Spouse;
                    //                        //gridNote.Text += "  Wife claimed and suspended at age " + ageWife.ToString() + ".";
                    //                        //if (ageWife < 70)
                    //                        //    needAsterisk = true;
                    //                    }
                    //                }
                    //            }
                    //        }
                    //        if (ageWife == wifeFRA && decBeneWife > decBeneHusb)
                    //        {
                    //            spousalBene = ColaUpSpousal(wifeFRA, decBeneWife, birthYearWife, intAgeWife, wifeFRA); // Change if COLA is YES 
                    //            spousalBene /= 2;
                    //            decimal ownSpousalBenefits = ColaUpSpousal(ageHusb, decBeneHusbFRA, birthYearHusb, intAgeHusb, husbFRA);
                    //            ownSpousalBenefits = ownSpousalBenefits / 2;
                    //            if (currBeneHusb < spousalBene)
                    //            {
                    //                decimal intdiff = spousalBene - currBeneHusb;
                    //                if (ownSpousalBenefits > intdiff)
                    //                {
                    //                    currBeneWife = ownSpousalBenefits;
                    //                    currBeneHusb = spousalBene;
                    //                    //Globals.Instance.ShowClaimAndSuspend = false;
                    //                    Globals.Instance.CurrHusValue = true;
                    //                    //needAsteriskForFullRetirement = true;
                    //                    //changeToSpousalBenefist = false;
                    //                    husbBenefitSource = benefit_source.Spouse;
                    //                    wifeFirstPayAge = ageWife;
                    //                    husbFirstPayAge = ageHusb;
                    //                }
                    //                else
                    //                {
                    //                    currBeneHusb = spousalBene;
                    //                    currBeneWife = 0;
                    //                    Globals.Instance.ShowClaimAndSuspend = false;
                    //                    Globals.Instance.CurrHusValue = true;
                    //                    //needAsteriskForFullRetirement = true;
                    //                    //changeToSpousalBenefist = false;
                    //                    husbBenefitSource = benefit_source.Spouse;
                    //                    wifeFirstPayAge = ageWife;
                    //                    husbFirstPayAge = ageHusb;
                    //                }
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if ((birthYearWife == 1957 || birthYearWife == 1958 || birthYearWife == 1959) && (birthYearHusb == 1960 || birthYearHusb == 1961 || birthYearHusb == 1962))
                    //        {
                    //            if ((decBeneWife < decBeneHusb) && (startAge == calc_start.fra6667))
                    //            {
                    //                //code changed on 05/26/2015 to fit the age span more than 4 years so taht in cliam and suspend get benefits from FRA and not from age 70
                    //                if ((ageSpan >= 4) && (ageHusb >= husbFRA) && (ageHusb < 70) && ageWife >= wifeFRA && wifeBirthDate < limit)
                    //                {
                    //                    spousalBene = ColaUpSpousal(ageWife, decBeneWifeFRA, birthYearWife, intAgeWife, wifeFRA); // Change if COLA is YES 
                    //                    spousalBene /= 2;
                    //                    wifeFRAYear = FRAYear(wifeBirthDate);
                    //                    currBeneHusb = spousalBene;
                    //                    husbFirstPayAge = ageHusb;
                    //                    husbBenefitSource = benefit_source.Spouse;
                    //                    //gridNote.Text += "  Husband claimed and suspended at age " + ageHusb.ToString() + ".";
                    //                }
                    //                if (((ageHusb > husbFRA) && (ageWife == wifeFRA) && (ageHusb < husbFRA + 4) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && husbBirthDate < limit)  /* husb 5-7 yrs older */
                    //                || (ageWife > (wifeFRA + (ageWife - ageHusb))) && (ageWife < 70) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && husbBirthDate < limit)       /*((ageHusb == husbFRA) && (ageWife >= wifeFRA))) husb 0-4 yrs older */
                    //                {
                    //                    spousalBene = ColaUpSpousal(ageHusb, decBeneHusbFRA, birthYearHusb, intAgeHusb, husbFRA); // Change if COLA is YES 
                    //                    spousalBene /= 2;
                    //                    husbFRAYear = FRAYear(husbBirthDate);
                    //                    currBeneWife = spousalBene;
                    //                    wifeFirstPayAge = ageWife;
                    //                    wifeBenefitSource = benefit_source.Spouse;
                    //                    // gridNote.Text += "  Husband claimed and suspended at age " + ageHusb.ToString() + ".";
                    //                    //if (ageHusb < 70)
                    //                    //    needAsterisk = true;
                    //                }
                    //            }
                    //        }
                    //        else
                    //        {
                    //            if ((birthYearWife == 1958 || birthYearWife == 1959) && (birthYearHusb == 1958 || birthYearHusb == 1959))
                    //            {
                    //                if ((decBeneWife < decBeneHusb) && (startAge == calc_start.fra6667))
                    //                {
                    //                    //code changed on 05/26/2015 to fit the age span more than 4 years so taht in cliam and suspend get benefits from FRA and not from age 70
                    //                    if ((ageSpan >= 4) && (ageHusb >= husbFRA) && (ageHusb < 70) && ageWife >= wifeFRA && wifeBirthDate < limit)
                    //                    {
                    //                        spousalBene = ColaUpSpousal(ageWife, decBeneWifeFRA, birthYearWife, intAgeWife, wifeFRA); // Change if COLA is YES 
                    //                        spousalBene /= 2;
                    //                        wifeFRAYear = FRAYear(wifeBirthDate);
                    //                        currBeneHusb = spousalBene;
                    //                        husbFirstPayAge = ageHusb;
                    //                        husbBenefitSource = benefit_source.Spouse;
                    //                        //gridNote.Text += "  Husband claimed and suspended at age " + ageHusb.ToString() + ".";
                    //                    }
                    //                    if (((ageHusb > husbFRA) && (ageWife == wifeFRA) && (ageHusb < husbFRA + 4) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && husbBirthDate < limit)  /* husb 5-7 yrs older */
                    //                    || (ageWife >= (wifeFRA + (ageWife - ageHusb))) && (ageWife < 70) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && husbBirthDate < limit)       /*((ageHusb == husbFRA) && (ageWife >= wifeFRA))) husb 0-4 yrs older */
                    //                    {
                    //                        spousalBene = ColaUpSpousal(ageHusb, decBeneHusbFRA, birthYearHusb, intAgeHusb, husbFRA); // Change if COLA is YES 
                    //                        spousalBene /= 2;
                    //                        husbFRAYear = FRAYear(husbBirthDate);
                    //                        currBeneWife = spousalBene;
                    //                        wifeFirstPayAge = ageWife;
                    //                        wifeBenefitSource = benefit_source.Spouse;
                    //                        //gridNote.Text += "  Husband claimed and suspended at age " + ageHusb.ToString() + ".";
                    //                        //if (ageHusb < 70)
                    //                        //    needAsterisk = true;
                    //                    }
                    //                }
                    //            }
                    //            else
                    //            {
                    //                if ((decBeneWife < decBeneHusb) && (startAge == calc_start.fra6667))
                    //                {
                    //                    //code changed on 05/26/2015 to fit the age span more than 4 years so taht in cliam and suspend get benefits from FRA and not from age 70
                    //                    if ((ageSpan >= 4) && (ageHusb >= husbFRA) && (ageHusb < 70) && ageWife >= wifeFRA && wifeBirthDate < limit)
                    //                    {
                    //                        spousalBene = ColaUpSpousal(ageWife, decBeneWifeFRA, birthYearWife, intAgeWife, wifeFRA); // Change if COLA is YES 
                    //                        spousalBene /= 2;
                    //                        wifeFRAYear = FRAYear(wifeBirthDate);
                    //                        currBeneHusb = spousalBene;
                    //                        husbFirstPayAge = ageHusb;
                    //                        husbBenefitSource = benefit_source.Spouse;
                    //                        //gridNote.Text += "  Husband claimed and suspended at age " + ageHusb.ToString() + ".";
                    //                    }
                    //                    if (((ageHusb > husbFRA) && (ageWife == wifeFRA) && (ageHusb < husbFRA + 4) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && husbBirthDate < limit)  /* husb 5-7 yrs older */
                    //                    || (ageWife >= (wifeFRA + (ageWife - ageHusb))) && (ageWife < 70) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && husbBirthDate < limit)       /*((ageHusb == husbFRA) && (ageWife >= wifeFRA))) husb 0-4 yrs older */
                    //                    {
                    //                        spousalBene = ColaUpSpousal(ageHusb, decBeneHusbFRA, birthYearHusb, intAgeHusb, husbFRA); // Change if COLA is YES 
                    //                        spousalBene /= 2;
                    //                        husbFRAYear = FRAYear(husbBirthDate);
                    //                        currBeneWife = spousalBene;
                    //                        wifeFirstPayAge = ageWife;
                    //                        wifeBenefitSource = benefit_source.Spouse;
                    //                        //gridNote.Text += "  Husband claimed and suspended at age " + ageHusb.ToString() + ".";
                    //                        //if (ageHusb < 70)
                    //                        //    needAsterisk = true;
                    //                    }
                    //                }
                    //            }
                    //        }
                    //        if ((decBeneWife >= decBeneHusb) && (startAge == calc_start.fra6667))
                    //        {
                    //            //code changed on 05/26/2015 to fit the age span more than 4 years so taht in cliam and suspend get benefits from FRA and not from age 70
                    //            if ((ageSpan >= 4) && (ageWife >= wifeFRA) && (ageWife < 70) && ageHusb >= husbFRA && husbBirthDate < limit)
                    //            {
                    //                spousalBene = ColaUpSpousal(ageHusb, decBeneHusbFRA, birthYearHusb, intAgeHusb, husbFRA); // Change if COLA is YES 
                    //                spousalBene /= 2;
                    //                husbFRAYear = FRAYear(husbBirthDate);
                    //                currBeneWife = spousalBene;
                    //                wifeFirstPayAge = ageWife;
                    //                wifeBenefitSource = benefit_source.Spouse;
                    //                //gridNote.Text += "  Husband claimed and suspended at age " + ageHusb.ToString() + ".";
                    //            }

                    //            if (((ageWife > wifeFRA) && (ageHusb == husbFRA) && (ageWife < wifeFRA + 4) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && wifeBirthDate < limit) /* wife 5-7 yrs older */
                    //            || (ageHusb >= husbFRA) && (ageHusb < 70) && HigherPIA_Age62yr <= LowerPIA_FRA_Year && wifeBirthDate < limit)      /*((ageWife == wifeFRA) && (ageHusb >= husbFRA))) wife 0-4 yrs older */
                    //            {
                    //                spousalBene = ColaUpSpousal(ageWife, decBeneWifeFRA, birthYearWife, intAgeWife, wifeFRA); // Change if COLA is YES 
                    //                currBeneHusb = spousalBene / 2;
                    //                husbFirstPayAge = ageHusb;
                    //                husbBenefitSource = benefit_source.Spouse;
                    //                // gridNote.Text += "  Wife claimed and suspended at age " + ageWife.ToString() + ".";
                    //                //if (ageWife < 70)
                    //                //    needAsterisk = true;
                    //            }
                    //        }
                    //        if (ageHusb == husbFRA && decBeneWife < decBeneHusb)
                    //        {
                    //            spousalBene = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA); // Change if COLA is YES 
                    //            spousalBene /= 2;
                    //            decimal ownSpousalBenefits = ColaUpSpousal(ageWife, decBeneWifeFRA, birthYearWife, intAgeWife, wifeFRA); // Change if COLA is YES 
                    //            ownSpousalBenefits /= 2;
                    //            if (currBeneWife < spousalBene)
                    //            {
                    //                decimal intdiff = spousalBene - currBeneWife;
                    //                if (ownSpousalBenefits > intdiff)
                    //                {
                    //                    currBeneHusb = ownSpousalBenefits;
                    //                    currBeneWife = spousalBene;
                    //                    // Globals.Instance.ShowClaimAndSuspend = false;
                    //                    Globals.Instance.CurrWifeValue = true;
                    //                    // needAsteriskForFullRetirement = true;
                    //                    // changeToSpousalBenefist = false;
                    //                    wifeBenefitSource = benefit_source.Spouse;
                    //                    wifeFirstPayAge = ageWife;
                    //                    husbFirstPayAge = ageHusb;
                    //                }
                    //                else
                    //                {
                    //                    currBeneWife = spousalBene;
                    //                    currBeneHusb = 0;
                    //                    Globals.Instance.ShowClaimAndSuspend = false;
                    //                    Globals.Instance.CurrWifeValue = true;
                    //                    //needAsteriskForFullRetirement = true;
                    //                    //changeToSpousalBenefist = false;
                    //                    wifeBenefitSource = benefit_source.Spouse;
                    //                    wifeFirstPayAge = ageWife;
                    //                    husbFirstPayAge = ageHusb;
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion

                    #region CODE TO USE AND CHECK THE WHB AND TO UPDATE THE TOTALS IN THE GRID
                    //  Update running totals
                    currBene = currBeneWife + currBeneHusb;
                    //Code change on 05/28/2015 as per update from client to modify the annual total as per the FRA year and months
                    //original code was currBeneAnnual = (currBene * 12);
                    #region Code to calculate the annual benefits according to DOB
                    int years, yearsHusb, remainingMonths, remainingMonthsHusb;
                    Customers customer = new Customers();
                    WifeAgeWhenSpouseAge62(out years, out remainingMonths);
                    HusbandAgeWhenSpouseAge62(out yearsHusb, out remainingMonthsHusb);
                    int resultHusb = FRAMonths(birthYearHusb);
                    int resultWife = FRAMonths(birthYearWife);
                    int intHusbandDifference = 0;
                    int intWifeDifference = 0;
                    DateTime datediffHusb70 = wifeBirthDate.AddYears(70);
                    TimeSpan difftimespanHusb70 = datediffHusb70.Subtract(husbBirthDate);
                    int yearHusb70 = (int)Math.Floor((double)difftimespanHusb70.Days / 365.2425);
                    int monthsHusb70 = Convert.ToInt32(((double)difftimespanHusb70.Days / 30.436875));
                    int multiplyHusb70 = monthsHusb70 - yearHusb70 * 12;
                    DateTime datediff70 = husbBirthDate.AddYears(70);
                    TimeSpan difftimespan70 = datediff70.Subtract(wifeBirthDate);
                    int yearsWife70 = (int)Math.Floor((double)difftimespan70.Days / 365.2425);
                    int monthsWife70 = Convert.ToInt32(((double)difftimespan70.Days / 30.436875));
                    int multiplyWife70 = monthsWife70 - yearsWife70 * 12;

                    if (birthYearWife >= 1955 && birthYearWife <= 1959 && ageWife == wifeFRA && wifeFirstPayAge == wifeFRA && currBeneWife != 0)
                    {
                        currAnnualBenefitsWife = annualBenefitsAsPerBirthDate(birthYearWife, currBeneWife);
                        Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12 - customer.FRAMonthsRemaining(birthYearWife));
                        Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(customer.FRAMonthsRemaining(birthYearWife));
                        BoolWifeAgePDF = true;
                        Globals.Instance.BoolWifeAgeAdjusted = true;
                    }
                    else if ((intAgeWife >= 62) && (currBeneWife != 0) && (wifeFirstPayAge == years) && (years != wifeFRA) && (ageWife == years) && (remainingMonths != 12) && (remainingMonths > 0))
                    {
                        currAnnualBenefitsWife = currBeneWife * (12m - remainingMonths);
                        BoolWifeAgePDF = true;
                        Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16((12m - remainingMonths));
                        Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16((remainingMonths));
                        Globals.Instance.WifeNumberofYearsAbove66 = ageWife;
                        Globals.Instance.BoolWifeAgeAdjusted = true;
                    }
                    else if ((intAgeWife >= 62) && (currBeneWife != 0) && (ageWife >= wifeFRA) && (wifeFirstPayAge == ageWife) && (ageWife < 70) && (ageHusb == 62) && (intAgeHusb < 62) && (strategy == calc_strategy.Spousal))
                    {
                        if (intAgeWifeMonth < intAgeHusbMonth && ageWife == wifeFRA && intAgeWife == wifeFRA)
                        {
                            int intMultiplyMonth = 0;
                            currAnnualBenefitsWife = WifeSpouseAgeLessThan62(out intMultiplyMonth, currBeneWife);
                            BoolWifeAgePDF = true;
                            Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(intMultiplyMonth);
                            Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(12 - intMultiplyMonth);
                            Globals.Instance.WifeNumberofYearsAbove66 = ageWife;
                            Globals.Instance.BoolWifeAgeAdjusted = true;
                        }
                    }
                    else if ((currBeneWife != 0) && (wifeFirstPayAge == ageWife) && (ageHusb == husbFRA) && (wifeBenefitSource == benefit_source.Spouse))
                    {
                        DateTime datediffWife = husbBirthDate.AddYears(husbFRA);
                        TimeSpan difftimespanWife = datediffWife.Subtract(wifeBirthDate);
                        int yearsWifeFRA = (int)((double)difftimespanWife.Days / 365.2425);
                        int monthsWifeFRA = Convert.ToInt32(((double)difftimespanWife.Days / 30.436875));
                        int inthusbmonthdiff;
                        if (husbFRA * 12 > resultHusb)
                            inthusbmonthdiff = husbFRA * 12 - resultHusb;
                        else
                            inthusbmonthdiff = resultHusb - husbFRA * 12;
                        int intMultiMonth = (monthsWifeFRA - yearsWifeFRA * 12) + inthusbmonthdiff;
                        if (yearsWifeFRA == husbFirstPayAge && intAgeHusbMonth >= intAgeWifeMonth)
                        {
                            currAnnualBenefitsWife = currBeneWife * ((12m - intMultiMonth));
                            if (intMultiMonth != 0 && intMultiMonth != 12)
                            {
                                Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16((12m - intMultiMonth));
                                Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(intMultiMonth);
                                BoolWifeAgePDF = true;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                                Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeFRA;
                            }
                        }
                        else if (yearsWifeFRA == husbFirstPayAge && intAgeHusbMonth < intAgeWifeMonth && ageWife == ageHusb)
                        {
                            currAnnualBenefitsWife = currBeneWife * (12m - intMultiMonth);
                            if (intMultiMonth != 0 && intMultiMonth != 12)
                            {
                                Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16((12m - intMultiMonth));
                                Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(intMultiMonth);
                                BoolWifeAgePDF = true;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                                Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeFRA;
                            }
                        }
                        //Newly added on 21/11/2016
                        else if (yearsWifeFRA == wifeFirstPayAge && ageWife == yearsWifeFRA && intMultiMonth < 12)
                        {
                            currAnnualBenefitsWife = currBeneWife * (12m - intMultiMonth);
                            if (intMultiMonth != 0 && intMultiMonth != 12)
                            {
                                Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16((12m - intMultiMonth));
                                Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(intMultiMonth);
                                BoolWifeAgePDF = true;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                                Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeFRA;
                            }
                        }
                        else
                        {
                            if (ageWife != ageHusb)
                            {

                                if (intMultiMonth > 0 && intMultiMonth < 12)
                                {
                                    currAnnualBenefitsWife = currBeneWife * (12m + (12m - intMultiMonth));
                                    Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m + (12m - intMultiMonth));
                                    Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(12m - (12m - intMultiMonth));
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.BoolWifeAgeAdjusted = true;
                                    Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeFRA;
                                }
                            }

                        }
                    }

                    else if (currBeneWife != 0 && ageWife < 70 && isWifeSpouse && strategy == calc_strategy.Max && intMonthsWife > 0 && intMonthsWife != 12)
                    {
                        if (ageWife == wifeFRA)
                        {
                            currAnnualBenefitsWife = currBeneWife * 12m;
                            isWifeSpouse = false;
                        }
                        else if (ageWife > intWifeYearsMax)
                        {
                            currAnnualBenefitsWife = currBeneWife * (12m + (12 - intMonthsWife));
                            Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m + (12 - intMonthsWife));
                            Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(intMonthsWife);
                            BoolWifeAgePDF = true;
                            isWifeSpouse = false;
                            Globals.Instance.BoolWifeAgeAdjusted = true;
                            Globals.Instance.WifeNumberofYears = intWifeYearsMax;
                        }
                        else
                        {
                            currAnnualBenefitsWife = currBeneWife * (12m - intMonthsWife);
                            Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m - intMonthsWife);
                            Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(intMonthsWife);
                            BoolWifeAgePDF = true;
                            isWifeSpouse = false;
                            Globals.Instance.BoolWifeAgeAdjusted = true;
                            Globals.Instance.WifeNumberofYears = intWifeYearsMax;
                        }
                    }

                    else if (Globals.Instance.BoolSpousalCliamedWife && ageHusb == 70 && multiplyWife70 != 0 && multiplyWife70 != 12)
                    {
                        if (multiplyWife70 > 12)
                        {
                            currAnnualBenefitsWife = currBeneWife * multiplyWife70;
                            Globals.Instance.WifeNumberofYears = yearsWife70;
                        }
                        else if (yearsWife70 == ageWife)
                        {
                            if (yearsWife70 == wifeFirstPayAge)//Added case on 06/12/2016 because at chart showing double asteric and at note showing single asteric and not showing months in steps
                            {
                                currAnnualBenefitsWife = currBeneWife * (12m - multiplyWife70);
                                Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m - multiplyWife70);
                                Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(multiplyWife70);
                                BoolWifeAgePDF = true;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                                Globals.Instance.WifeNumberofYearsAbove66 = yearsWife70;
                            }
                            else
                            {
                                currAnnualBenefitsWife = currBeneWife * (12 - multiplyWife70);
                                Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = Convert.ToInt16((12 - multiplyWife70));
                                BoolWifeSingleAsterisk = true;
                                Globals.Instance.BoolWHBSingleSwitchedWife = true;
                                Globals.Instance.WifeNumberofYears = yearsWife70;
                            }
                        }
                        else
                        {
                            currAnnualBenefitsWife = currBeneWife * (12 + (12 - multiplyWife70));
                            Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = Convert.ToInt16(12 + (12 - multiplyWife70));
                            BoolWifeDoubleAsterisk = true;
                            intWifeDifference = ageWife - yearsWife70;
                            Globals.Instance.WifeNumberofYears = yearsWife70;
                        }
                    }

                    //Conditions for Husband
                    if (birthYearHusb >= 1955 && birthYearHusb <= 1959 && ageHusb == husbFRA && husbFirstPayAge == husbFRA && currBeneHusb != 0)
                    {
                        currAnnualBenefitsHusb = annualBenefitsAsPerBirthDate(birthYearHusb, currBeneHusb);
                        Globals.Instance.BoolHusbandAgeAdjusted = true;
                        BoolHusbandAgePDF = true;

                        Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12 - customer.FRAMonthsRemaining(birthYearHusb));
                        Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(customer.FRAMonthsRemaining(birthYearHusb));
                    }
                    else if ((intAgeHusb >= 62) && (currBeneHusb != 0) && (remainingMonthsHusb != 12) && (yearsHusb != husbFRA) && (husbFirstPayAge == yearsHusb) && (ageHusb == yearsHusb) && (remainingMonthsHusb > 0))
                    {
                        currAnnualBenefitsHusb = currBeneHusb * (12m - remainingMonthsHusb);
                        BoolHusbandAgePDF = true;
                        Globals.Instance.HusbandNumberofMonthsinNote = (int)remainingMonthsHusb;
                        Globals.Instance.HusbandNumberofMonthsinSteps = (int)(12m - remainingMonthsHusb);
                        Globals.Instance.HusbNumberofYearsAbove66 = yearsHusb;
                        Globals.Instance.BoolHusbandAgeAdjusted = true;
                    }
                    else if ((intAgeHusb >= 62) && (currBeneHusb != 0) && (husbFirstPayAge == ageHusb) && (ageHusb >= husbFRA) && (ageHusb < 70) && (ageWife == 62) && (intAgeWife < 62) && (strategy == calc_strategy.Spousal))
                    {
                        if (intAgeWifeMonth > intAgeHusbMonth && ageHusb == husbFRA && intAgeHusb == husbFRA)
                        {
                            int intMultiplyMonth = 0;
                            currAnnualBenefitsHusb = HusbandSpouseAgeLessThan62(out intMultiplyMonth, currBeneHusb);
                            BoolHusbandAgePDF = true;
                            Globals.Instance.HusbandNumberofMonthsinNote = intMultiplyMonth;
                            Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(12 - intMultiplyMonth);
                            Globals.Instance.HusbNumberofYearsAbove66 = ageHusb;
                            Globals.Instance.BoolHusbandAgeAdjusted = true;
                        }
                    }
                    else if (currBeneHusb != 0 && ageHusb < 70 && isHusbSpouse && strategy == calc_strategy.Max && intMonthsHusb > 0 && intMonthsHusb != 12)
                    {
                        if (ageWife == wifeFRA)
                        {
                            currAnnualBenefitsHusb = currBeneHusb * 12m;
                            isHusbSpouse = false;
                        }
                        else if (ageHusb > intHusbandYearsMax)
                        {
                            currAnnualBenefitsHusb = currBeneHusb * (12m + (12 - intMonthsHusb));
                            Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m + (12 - intMonthsHusb));
                            Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(intMonthsHusb);
                            BoolHusbandAgePDF = true;
                            isHusbSpouse = false;
                            Globals.Instance.BoolHusbandAgeAdjusted = true;
                            Globals.Instance.HusbandNumberofYears = intHusbandYearsMax;
                        }
                        else
                        {
                            currAnnualBenefitsHusb = currBeneHusb * (12m - intMonthsHusb);
                            Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - intMonthsHusb);
                            Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(intMonthsHusb);
                            BoolHusbandAgePDF = true;
                            isHusbSpouse = false;
                            Globals.Instance.BoolHusbandAgeAdjusted = true;
                            Globals.Instance.HusbandNumberofYears = intHusbandYearsMax;
                        }
                    }
                    else if ((currBeneHusb != 0) && (husbFirstPayAge == ageHusb) && (ageWife == wifeFRA) && (husbBenefitSource == benefit_source.Spouse))
                    {
                        DateTime datediffHusb = wifeBirthDate.AddYears(wifeFRA);
                        TimeSpan difftimespanHusb = datediffHusb.Subtract(husbBirthDate);
                        int yearsHusbFRA = (int)((double)difftimespanHusb.Days / 365.2425);
                        int monthsHusbFRA = Convert.ToInt32(((double)difftimespanHusb.Days / 30.436875));
                        int intwifemonthdiff;
                        if (wifeFRA * 12 > resultWife)
                            intwifemonthdiff = wifeFRA * 12 - resultWife;
                        else
                            intwifemonthdiff = resultWife - wifeFRA * 12;
                        int intMultiMonth = (monthsHusbFRA - yearsHusbFRA * 12) + intwifemonthdiff;
                        if (yearsHusbFRA == husbFirstPayAge && intAgeWifeMonth >= intAgeHusbMonth)
                        {
                            currAnnualBenefitsHusb = currBeneHusb * (12m - intMultiMonth);
                            if (intMultiMonth != 0 && intMultiMonth != 12)
                            {
                                Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - intMultiMonth);
                                Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(intMultiMonth);
                                BoolHusbandAgePDF = true;
                                Globals.Instance.BoolHusbandAgeAdjusted = true;
                                Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbFRA;
                            }
                        }

                        else if (yearsHusbFRA == husbFirstPayAge && intAgeWifeMonth < intAgeHusbMonth && ageWife == ageHusb)
                        {
                            currAnnualBenefitsHusb = currBeneHusb * (12m - intMultiMonth);
                            if (intMultiMonth != 0 && intMultiMonth != 12)
                            {
                                Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - intMultiMonth);
                                Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(intMultiMonth);
                                BoolHusbandAgePDF = true;
                                Globals.Instance.BoolHusbandAgeAdjusted = true;
                                Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbFRA;
                            }
                        }
                        //Newly added on 21/11/2016
                        else if (yearsHusbFRA == husbFirstPayAge && ageHusb == yearsHusbFRA && intMultiMonth < 12)
                        {
                            currAnnualBenefitsHusb = currBeneHusb * (12m - intMultiMonth);
                            if (intMultiMonth != 0 && intMultiMonth != 12)
                            {
                                Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - intMultiMonth);
                                Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(intMultiMonth);
                                BoolHusbandAgePDF = true;
                                Globals.Instance.BoolHusbandAgeAdjusted = true;
                                Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbFRA;
                            }
                        }
                        else
                        {
                            if (ageWife != ageHusb)
                            {

                                if (intMultiMonth > 0 && intMultiMonth < 12)
                                {
                                    currAnnualBenefitsHusb = currBeneHusb * (12m + (12m - intMultiMonth));
                                    Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m + (12m - intMultiMonth));
                                    Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(12 - (12m - intMultiMonth));
                                    BoolHusbandAgePDF = true;
                                    Globals.Instance.BoolHusbandAgeAdjusted = true;
                                    Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbFRA;
                                }
                            }
                        }
                    }
                    else if (Globals.Instance.BoolSpousalCliamedHusb && ageWife == 70 && multiplyHusb70 != 0 && multiplyHusb70 != 12)
                    {
                        if (multiplyHusb70 > 12)
                        {
                            currAnnualBenefitsHusb = currBeneHusb * multiplyHusb70;
                            Globals.Instance.HusbandNumberofYears = yearHusb70;
                        }
                        else if (yearHusb70 == ageHusb)
                        {

                            if (yearHusb70 == husbFirstPayAge)//Added case on 06/12/2016 because at chart showing double asteric and at note showing single asteric and not showing months in steps
                            {
                                currAnnualBenefitsHusb = currBeneHusb * (12m - multiplyHusb70);
                                Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - multiplyHusb70);
                                Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(multiplyHusb70);
                                BoolWifeAgePDF = true;
                                Globals.Instance.BoolHusbandAgeAdjusted = true;
                                Globals.Instance.HusbNumberofYearsAbove66 = yearHusb70;
                            }
                            else
                            {
                                currAnnualBenefitsHusb = currBeneHusb * (12 - multiplyHusb70);
                                Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior = Convert.ToInt16(12 - multiplyHusb70);
                                BoolHusbSingleAsterisk = true;
                                Globals.Instance.BoolWHBSingleSwitchedHusb = true;
                                Globals.Instance.HusbandNumberofYears = yearHusb70;
                            }
                        }
                        else
                        {
                            currAnnualBenefitsHusb = currBeneHusb * (12 + (12 - multiplyHusb70));
                            Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior = Convert.ToInt16(12 + (12 - multiplyHusb70));
                            BoolHusbDoubleAsterisk = true;
                            intHusbandDifference = ageHusb - yearHusb70;
                            Globals.Instance.HusbandNumberofYears = yearHusb70;
                        }
                    }
                    //husband age is smaller than 70 when spouse is age 66 (FRA) // Added on 29/09/2016
                    //Added case currBeneWife !=0 --> Full strategy getting wrong at wife's FRA age //Added on 29/03/2017
                    else if (IsHusbLess70AtSpouse66 && ageWife == wifeFRA && birthYearHusb < 1954 && strategy == calc_strategy.Spousal && currBeneWife != 0)
                    {
                        int intMultiply = 12 - multiplyHusb66;
                        Globals.Instance.HusbSpousalBenMonthAt69 = intMultiply;
                        currAnnualBenefitsHusb = (currBeneHusb * 12) + (Globals.Instance.HusbSpousalBenefitWhenWife66 * intMultiply);
                        if (currAnnualBenefitsHusb != 0 && Globals.Instance.HusbSpousalBenefitWhenWife66 != 0)
                        {
                            Globals.Instance.HusbandNumberofMonthsinNote = multiplyHusb66;
                            Globals.Instance.HusbandNumberofMonthsinSteps = multiplyHusb66;
                            Globals.Instance.HusbNumberofYearsAbove66 = yearHusb66;
                            Globals.Instance.HusbAnnualIcomeAt70 = currBeneHusb;
                            Globals.Instance.HusbAnnualIcomeAt69 = Globals.Instance.HusbSpousalBenefitWhenWife66;
                            IsHusbLess70AtSpouse66 = false;
                            Globals.Instance.BoolSpousalWHBat70 = true;
                            Globals.Instance.BoolHusbandAgeAdjusted = true;
                        }
                    }

                    if (currAnnualBenefitsWife == 0)
                    {
                        currAnnualBenefitsWife = currBeneWife * 12;
                    }
                    if (currAnnualBenefitsHusb == 0)
                    {
                        currAnnualBenefitsHusb = currBeneHusb * 12;
                    }
                    if ((intCurrentAgeWifeMonth >= 1) && (intCurrentAgeWifeMonth != 12) && (ageWife == intCurrentAgeWifeYear) && (intCurrentAgeWifeYear == wifeFirstPayAge) && (ageWife < 70) && (currBeneWife != 0))
                    {
                        if (intAgeWifeMonth >= 6)
                        {
                            currAnnualBenefitsWife = 0;  //added on 01/11/2017 it was adding the annual benefit in previous 12 months benefit, it should not take 12 month additional benefit
                            currAnnualBenefitsWife += currBeneWife * (12 - intCurrentAgeWifeMonth);
                            Globals.Instance.WifeNumberofMonthinNote = (12 - intCurrentAgeWifeMonth);
                        }
                        else
                        {
                            currAnnualBenefitsWife -= currBeneWife * intCurrentAgeWifeMonth;
                            Globals.Instance.WifeNumberofMonthinNote = intCurrentAgeWifeMonth;
                        }
                        BoolWifeAgePDF = true;
                        Globals.Instance.BoolWifeAgeAdjusted = true;
                    }
                    if ((intCurrentAgeHusbMonth >= 1) && (intCurrentAgeWifeMonth != 12) && (ageHusb == husbFirstPayAge) && (intCurrentAgeHusbYear == husbFirstPayAge) && (ageHusb < 70) && (currBeneHusb != 0))
                    {
                        if (intAgeHusbMonth >= 6)
                        {
                            currAnnualBenefitsHusb = 0; //added on 01/11/2017 it was adding the annual benefit in previous 12 months benefit, it should not take 12 month additional benefit
                            currAnnualBenefitsHusb += currBeneHusb * (12 - intCurrentAgeHusbMonth);
                            Globals.Instance.HusbandNumberofMonthsinNote = (12 - intCurrentAgeHusbMonth);
                        }
                        else
                        {
                            currAnnualBenefitsHusb -= currBeneHusb * intCurrentAgeHusbMonth;
                            Globals.Instance.HusbandNumberofMonthsinNote = intCurrentAgeHusbMonth;
                        }
                        BoolHusbandAgePDF = true;
                        Globals.Instance.BoolHusbandAgeAdjusted = true;
                    }

                    #region Restricted Application Strategy Income
                    //Restricted Application Strategy Income
                    if (currBeneHusb != 0 && ageHusb < 70)
                    {
                        Globals.Instance.intRestrictAppIncomeFullHusb += (int)currAnnualBenefitsHusb;
                        Globals.Instance.intRestrictAppIncomeMaxHusb += (int)currAnnualBenefitsHusb;
                    }
                    if (currBeneWife != 0 && ageWife < 70)
                    {
                        Globals.Instance.intRestrictAppIncomeFullWife += (int)currAnnualBenefitsWife;
                        Globals.Instance.intRestrictAppIncomeMaxWife += (int)currAnnualBenefitsWife;
                    }
                    #endregion Restricted Application Strategy Income

                    if (currAnnualBenefitsWife == 0 && currAnnualBenefitsHusb == 0)
                    {
                        currBeneAnnual = currBene * 12;
                        currAnnualBenefitsWife = 0;
                        currAnnualBenefitsHusb = 0;
                    }
                    else
                    {

                        if (currBeneWife == 0)
                            currAnnualBenefitsWife = 0;
                        if (currBeneHusb == 0)
                            currAnnualBenefitsHusb = 0;
                        currBeneAnnual = currAnnualBenefitsWife + currAnnualBenefitsHusb;
                        if (ageWife == 69)
                            Globals.Instance.WifeOriginalAnnualIncome = currAnnualBenefitsWife;
                        if (ageHusb == 69)
                            Globals.Instance.HusbandOriginalAnnualIncome = currAnnualBenefitsHusb;
                        currAnnualBenefitsWife = 0;
                        currAnnualBenefitsHusb = 0;
                    }

                    #endregion Code to calculate the annual benefits according to DOB

                    #region Code to calculate the cummulative benefits before and after younger age 70
                    if (youngest == 70)
                    {
                        currBeneWifeLocal = currBeneWife;
                        currBeneHusbLocal = currBeneHusb;
                    }
                    if (youngest >= 75 && youngest <= 90)
                    {
                        if (youngest == 75)
                        {
                            decimal grandTotal = 0;
                            for (int i = 0; i < 5; i++)
                            {
                                currBeneWifeLocal = Convert.ToInt32(ColaUp(63, currBeneWifeLocal));
                                currBeneHusbLocal = Convert.ToInt32(ColaUp(63, currBeneHusbLocal));
                                decimal total = ((currBeneWifeLocal + currBeneHusbLocal) * 12);
                                grandTotal += Convert.ToInt32(total);
                            }
                            cummBene += grandTotal;
                        }
                        else
                        {
                            decimal grandTotal = 0;
                            for (int i = 0; i < 5; i++)
                            {
                                currBeneWifeLocal = Convert.ToInt32(ColaUp(63, currBeneWifeLocal));
                                currBeneHusbLocal = Convert.ToInt32(ColaUp(63, currBeneHusbLocal));
                                decimal total = ((currBeneWifeLocal + currBeneHusbLocal) * 12);
                                grandTotal += Convert.ToInt32(total);
                            }
                            cummBene += grandTotal;
                        }
                    }
                    if (youngest <= 71) //Previously (youngest <= 70), changed on Nov 09 2016
                    {
                        cummBene += currBeneAnnual;
                    }
                    #endregion Code to calculate the cummulative benefits before and after younger age 70
                    cummBeneWife += currBeneWife;
                    cummBeneHusb += currBeneHusb;
                    #endregion

                    #region SURVIVOR BENEFIT CALCULATION

                    //  Survivor's benefit
                    decimal survBeneHusb = 0, survBeneWife = 0, survBene = 0;
                    survBeneHusb = currBeneHusb;
                    survBeneWife = currBeneWife;
                    if (survBeneHusb > survBeneWife)
                        survBene = survBeneHusb;
                    else
                        survBene = survBeneWife;

                    if (ageHusb >= 70 && ageWife >= 70 && age70SurvBeneAge == string.Empty)
                    {
                        if (ageWife == ageHusb)
                        {
                            age70SurvBeneAge = ageHusb.ToString();
                        }
                        else
                        {
                            age70SurvBeneAge = ageWife.ToString() + Constants.BackSlash + ageHusb.ToString();
                        }
                        age70SurvBene = survBene * 12;
                    }



                    string ageCol = string.Empty;
                    //if (ageHusb == ageWife)
                    //    ageCol = aIx.ToString();
                    //else
                    ageCol = ageWife.ToString() + Constants.BackSlash + ageHusb.ToString();
                    breakEvenAmt[highBE] = (int)StdRound(cummBene);
                    breakEvenAge[highBE] = ageCol;
                    highBE += 1;

                    #endregion

                    #region POPULATE THE GRID ROWS WITH VALUES

                    //  For selected ages, build the display row
                    if (youngest <= 90)//(aIx <= topAix) || (aIx == topAix + 10) || didAutoAdjust || showThisLine)
                    {
                        System.Data.DataRow incomeRow = incomeTab.NewRow();
                        if (ageHusb == ageWife)
                            incomeRow[(int)disp_col.Age] = ageCol;
                        else
                            incomeRow[(int)disp_col.Age] = ageCol;
                        if (currBene > 0)
                        {
                            string regExp = "[^\\w]";
                            if (BoolHusbandAgePDF || BoolWifeAgePDF)
                            {
                                incomeRow[(int)disp_col.AnnInc] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + Constants.SingleHash;
                                BoolHusbandAgePDF = false;
                                BoolWifeAgePDF = false;
                            }
                            else if (BoolHusbDoubleAsterisk || BoolWifeDoubleAsterisk)
                            {
                                string strhashIncluded = string.Empty;
                                if (incomeRow[(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                    strhashIncluded = Constants.SingleHash;
                                incomeRow[(int)disp_col.AnnInc] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.DoubleAsterisk;
                                int intPrevBenefit = 0, intPrevAnnualIcome = 0;
                                string strBenefit = string.Empty;
                                if (BoolHusbDoubleAsterisk)
                                {
                                    if (intHusbandDifference > 0)
                                        intHusbandDifference = incomeTab.Rows.Count - intHusbandDifference;
                                    else
                                        intHusbandDifference = incomeTab.Rows.Count - 1;
                                    strBenefit = incomeTab.Rows[intHusbandDifference][(int)disp_col.HusbInc].ToString().TrimStart('$');
                                    intPrevBenefit = Convert.ToInt32((Regex.Replace(strBenefit, regExp, "")));
                                    intPrevAnnualIcome = intPrevBenefit * multiplyHusb70;
                                    if (incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                        strhashIncluded = Constants.SingleHash;
                                    if (intPrevAnnualIcome != 0)
                                    {
                                        incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc] = (Globals.Instance.WifeOriginalAnnualIncome + intPrevAnnualIcome).ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.SingleAsterisk;
                                        Globals.Instance.BoolWHBSwitchedHusb = true;

                                        #region updated on (17/01/2017) Special Married case posted on 27 dec 2016
                                        //Update the calculation if user taking his/her both the benefits in same year where annual income value attached
                                        //with "#" and "*" combinely --- updated on (17/01/2017)
                                        if (incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc].ToString().Contains("#*"))
                                        {
                                            int intMultiplyer = 0;
                                            Globals.Instance.BoolContainsHashStar = true;
                                            if (Globals.Instance.HusbandNumberofMonthsinNote > (Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior - 12))
                                                intMultiplyer = Globals.Instance.HusbandNumberofMonthsinNote - (Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior - 12);
                                            else
                                                intMultiplyer = (Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior - 12) - Globals.Instance.HusbandNumberofMonthsinNote;
                                            incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc] = (Globals.Instance.WifeOriginalAnnualIncome + (intPrevBenefit * intMultiplyer)).ToString(Constants.AMT_FORMAT) + strhashIncluded;// +Constants.SingleAsterisk;
                                        }
                                        //
                                        #endregion updated on (17/01/2017) Special Married case posted on 27 dec 2016
                                    }
                                    Int32 intCummAnnual = Int32.Parse(Regex.Replace(incomeTab.Rows[intHusbandDifference - 1][(int)disp_col.CummInc].ToString(), regExp, ""));
                                    incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc] = (intCummAnnual + int.Parse(Regex.Replace(incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc].ToString(), regExp, "")));
                                    cummBene = currBeneAnnual + decimal.Parse(incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc].ToString());
                                    incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc] = decimal.Parse(incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc].ToString()).ToString(Constants.AMT_FORMAT);
                                    Globals.Instance.HusbNumberofMonthsAboveGrid = multiplyHusb70;
                                    Globals.Instance.HusbNumberofMonthsBelowGrid = multiplyHusb70;
                                    BoolHusbDoubleAsterisk = false;
                                }
                                else
                                {
                                    if (intWifeDifference > 0)
                                        intWifeDifference = incomeTab.Rows.Count - intWifeDifference;
                                    else
                                        intWifeDifference = incomeTab.Rows.Count - 1;
                                    strBenefit = incomeTab.Rows[intWifeDifference][(int)disp_col.WifeInc].ToString().TrimStart('$');
                                    intPrevBenefit = Convert.ToInt32((Regex.Replace(strBenefit, regExp, "")));
                                    intPrevAnnualIcome = intPrevBenefit * multiplyWife70;
                                    if (incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                        strhashIncluded = Constants.SingleHash;
                                    if (intPrevAnnualIcome != 0)
                                    {
                                        incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc] = (Globals.Instance.HusbandOriginalAnnualIncome + intPrevAnnualIcome).ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.SingleAsterisk;
                                        Globals.Instance.BoolWHBSwitchedWife = true;

                                        #region updated on (17/01/2017) Special Married case posted on 27 dec 2016
                                        //Update the calculation if user taking his/her both the benefits in same year where annual income value attached
                                        //with "#*" updated on (17/01/2017)
                                        if (incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc].ToString().Contains("#*"))
                                        {
                                            int intMultiplyer = 0;
                                            Globals.Instance.BoolContainsHashStar = true;
                                            if (Globals.Instance.WifeNumberofMonthinNote > (Globals.Instance.WifeNumberofMonthsBelowGridAgePrior - 12))
                                                intMultiplyer = Globals.Instance.WifeNumberofMonthinNote - (Globals.Instance.WifeNumberofMonthsBelowGridAgePrior - 12);
                                            else
                                                intMultiplyer = (Globals.Instance.WifeNumberofMonthsBelowGridAgePrior - 12) - Globals.Instance.WifeNumberofMonthinNote;
                                            incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc] = (Globals.Instance.HusbandOriginalAnnualIncome + (intPrevBenefit * intMultiplyer)).ToString(Constants.AMT_FORMAT) + strhashIncluded;// +Constants.SingleAsterisk;
                                        }
                                        ////
                                        #endregion updated on (17/01/2017) Special Married case posted on 27 dec 2016
                                    }
                                    Int32 intCummAnnual = Int32.Parse(Regex.Replace(incomeTab.Rows[intWifeDifference - 1][(int)disp_col.CummInc].ToString(), regExp, ""));
                                    incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc] = (intCummAnnual + int.Parse(Regex.Replace(incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc].ToString(), regExp, "")));
                                    cummBene = currBeneAnnual + decimal.Parse(incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc].ToString());
                                    incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc] = decimal.Parse(incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc].ToString()).ToString(Constants.AMT_FORMAT);
                                    Globals.Instance.WifeNumberofMonthsAboveGrid = multiplyWife70;
                                    Globals.Instance.WifeNumberofMonthsBelowGrid = multiplyWife70;

                                    BoolWifeDoubleAsterisk = false;
                                }
                            }
                            else if (BoolWifeSingleAsterisk || BoolHusbSingleAsterisk)
                            {
                                string strhashIncluded = string.Empty;
                                if (incomeRow[(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                    strhashIncluded = Constants.SingleHash;
                                incomeRow[(int)disp_col.AnnInc] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.DoubleAsterisk;
                                int intPrevBenefit = 0, intPrevAnnualIcome = 0;
                                string strBenefit = string.Empty;
                                if (BoolHusbSingleAsterisk)
                                {
                                    if (intHusbandDifference > 0)
                                        intHusbandDifference = incomeTab.Rows.Count - intHusbandDifference;
                                    else
                                        intHusbandDifference = incomeTab.Rows.Count - 1;
                                    strBenefit = incomeTab.Rows[intHusbandDifference][(int)disp_col.HusbInc].ToString().TrimStart('$');
                                    intPrevBenefit = Convert.ToInt32((Regex.Replace(strBenefit, regExp, "")));
                                    intPrevAnnualIcome = intPrevBenefit * (12 + multiplyHusb70);
                                    if (incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                        strhashIncluded = Constants.SingleHash;
                                    if (intPrevAnnualIcome != 0)
                                    {
                                        incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc] = (Globals.Instance.WifeOriginalAnnualIncome + intPrevAnnualIcome).ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.SingleAsterisk;
                                        Globals.Instance.BoolWHBSwitchedHusb = true;
                                    }
                                    Int32 intCummAnnual = Int32.Parse(Regex.Replace(incomeTab.Rows[intHusbandDifference - 1][(int)disp_col.CummInc].ToString(), regExp, ""));
                                    incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc] = (intCummAnnual + int.Parse(Regex.Replace(incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc].ToString(), regExp, "")));
                                    incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc] = decimal.Parse(incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc].ToString()).ToString(Constants.AMT_FORMAT);
                                    Globals.Instance.HusbNumberofMonthsBelowGrid = (12 + multiplyHusb70);
                                    Globals.Instance.HusbNumberofMonthsAboveGrid = multiplyHusb70;
                                    BoolHusbSingleAsterisk = false;
                                }
                                else
                                {
                                    if (intWifeDifference > 0)
                                        intWifeDifference = incomeTab.Rows.Count - intWifeDifference;
                                    else
                                        intWifeDifference = incomeTab.Rows.Count - 1;
                                    strBenefit = incomeTab.Rows[intWifeDifference][(int)disp_col.WifeInc].ToString().TrimStart('$');
                                    intPrevBenefit = Convert.ToInt32((Regex.Replace(strBenefit, regExp, "")));
                                    intPrevAnnualIcome = intPrevBenefit * (12 + multiplyWife70);
                                    if (incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                        strhashIncluded = Constants.SingleHash;
                                    if (intPrevAnnualIcome != 0)
                                    {
                                        incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc] = (Globals.Instance.HusbandOriginalAnnualIncome + intPrevAnnualIcome).ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.SingleAsterisk;
                                        Globals.Instance.BoolWHBSwitchedWife = true;
                                    }
                                    Int32 intCummAnnual = Int32.Parse(Regex.Replace(incomeTab.Rows[intWifeDifference - 1][(int)disp_col.CummInc].ToString(), regExp, ""));
                                    incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc] = (intCummAnnual + int.Parse(Regex.Replace(incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc].ToString(), regExp, "")));
                                    incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc] = decimal.Parse(incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc].ToString()).ToString(Constants.AMT_FORMAT);
                                    Globals.Instance.WifeNumberofMonthsAboveGrid = (multiplyWife70);
                                    Globals.Instance.WifeNumberofMonthsBelowGrid = (12 + multiplyWife70);
                                    BoolWifeSingleAsterisk = false;
                                }
                            }
                            else
                            {
                                incomeRow[(int)disp_col.AnnInc] = currBeneAnnual.ToString(Constants.AMT_FORMAT);
                            }
                            incomeRow[(int)disp_col.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);
                            incomeRow[(int)disp_col.HusbInc] = currBeneHusb.ToString(Constants.AMT_FORMAT);
                            incomeRow[(int)disp_col.CombInc] = currBene.ToString(Constants.AMT_FORMAT);
                            incomeRow[(int)disp_col.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);
                            incomeRow[(int)disp_col.SurvInc] = survBene.ToString(Constants.AMT_FORMAT);
                        }
                        else
                        {
                            incomeRow[(int)disp_col.WifeInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_col.HusbInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_col.CombInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_col.AnnInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_col.CummInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_col.SurvInc] = survBene.ToString(Constants.AMT_FORMAT);
                        }
                        incomeTab.Rows.Add(incomeRow);
                    }
                    #endregion

                    #region cummulative value when PIA is equal
                    //highlight cummulativ value when PIA is equal
                    if (decBeneHusb == decBeneWife)
                    {
                        if (strategy == calc_strategy.Max)
                        {
                            if (ageHusb == 70)
                            {
                                if (intAgeHusb > husbFRA)
                                {
                                    multiply = AgeAbove68YearsOfHusb();
                                    workingBenefitsat70Husb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                }
                                else
                                {
                                    workingBenefitsat70Husb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                }
                                workingBenefitsat70Husb = ColaUpSpousal(ageHusb, workingBenefitsat70Husb, birthYearHusb, intHusbAge, husbFRA);
                            }
                            if (ageWife == 70)
                            {
                                if (intAgeWife > wifeFRA)
                                {
                                    multiply = AgeAbove68YearsOfWife();
                                    workingBenefitsat70Wife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                }
                                else
                                {
                                    workingBenefitsat70Wife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                }
                                workingBenefitsat70Wife = ColaUpSpousal(ageWife, workingBenefitsat70Wife, birthYearWife, intWifeAge, wifeFRA);
                            }
                            if (workingBenefitsat70Wife != 0 && workingBenefitsat70Husb != 0)
                            {
                                if (workingBenefitsat70Wife > workingBenefitsat70Husb)
                                    Globals.Instance.BoolHighlightCummAtWife69Max = true;
                                else
                                    Globals.Instance.BoolHighlightCummAtWife69Max = false;
                            }
                        }
                    }
                    #endregion cummulative value when PIA is equal

                    #region CODE TO INCREASE COLA AND INCREASE THE AGE TO ITTERATE AND BIND THE DATA
                    if (youngest >= 71 && youngest <= 90)
                    {
                        currBeneWife = ColaUp(67, currBeneWife);
                        currBeneHusb = ColaUp(67, currBeneHusb);
                    }
                    else
                    {
                        currBeneWife = ColaUp(63, currBeneWife);
                        currBeneHusb = ColaUp(63, currBeneHusb);
                    }
                    // Date : 20112014
                    // Description : Add Extra Column "YEAR" in Grid.
                    //incYr++;
                    if (aIx >= 71)
                    {
                        aIx = aIx + 5;
                        year = year + 5;
                        youngest = youngest + 5;
                    }
                    else
                    {
                        year++;
                        aIx++;
                        youngest++;
                    }
                }
                //  -------------
                //  Finalize Grid
                //  -------------
                Grid1.DataSource = incomeSet;
                Grid1.DataBind();
                //Grid1.CssClass = "gridWidth";
                TableItemStyle tisA = Grid1.AlternatingRowStyle;
                int cummTotal = 0;
                //  ---------------------------------
                //  HIGHLIGHT cells to match the book
                //  ---------------------------------
                #endregion

                #region CODE TO DESIGN AND FORMAT THE GRID
                // Use high earner's FRA to trigger cell coloring
                int highEarnerFRA = 0;
                if (decBeneHusb > decBeneWife)
                    highEarnerFRA = husbFRA;
                else
                    highEarnerFRA = wifeFRA;
                string preFraYear = (highEarnerFRA - 1).ToString();
                string fraYear = highEarnerFRA.ToString();
                string fraYear3 = (69).ToString();
                string fraYear4 = (70).ToString();
                string fraYear14 = (80).ToString();
                bool boolToRunLoopOnlyOnce = true;
                bool boolToRunLoopOnlyOnce1 = true;

                foreach (GridViewRow gvr in Grid1.Rows)
                {
                    // Date : 20112014
                    // Description : Add Extra Column "YEAR" in Grid.
                    //gvr.Cells[(int)disp_col.Year].BorderColor = LOW_GREY;
                    string regExp = "[^\\w]";
                    if (gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number70))
                    {
                        AgeForHusb70 = gvr.Cells[0].Text;
                        ValueForHusb70 = decimal.Parse(gvr.Cells[2].Text.Substring(1));
                    }
                    if (gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number70))
                    {
                        AgeForWife70 = gvr.Cells[0].Text;
                        ValueForWife70 = decimal.Parse(gvr.Cells[1].Text.Substring(1));
                    }
                    #region Code to get strating and age 69 and age 70 values for husb and wife
                    if (decBeneHusb < decBeneWife)
                    {
                        if (gvr.Cells[1].Text.Equals(Constants.ZeroWithAsterisk))
                        {
                            AgeWhenHigherEarnerClaimandSuspend = gvr.Cells[0].Text;
                        }
                        if (!((gvr.Cells[2].Text).Equals(Constants.ZeroWithOutAsterisk) || ((gvr.Cells[2].Text).Equals(Constants.ZeroWithAsterisk))))
                        {
                            if (boolToRunLoopOnlyOnce)
                            {
                                startingBenefitsValueHusb = decimal.Parse((gvr.Cells[2].Text.Substring(1)));
                                startingAgeValueHusb = gvr.Cells[0].Text;
                                boolToRunLoopOnlyOnce = false;
                            }
                        }
                        else
                        {
                            startingBenefitsValueHusb = 0;
                        }

                        if (!((gvr.Cells[1].Text).Equals(Constants.ZeroWithOutAsterisk) || ((gvr.Cells[1].Text).Equals(Constants.ZeroWithAsterisk))))
                        {
                            if (boolToRunLoopOnlyOnce1)
                            {
                                startingBenefitsValueWife = decimal.Parse((gvr.Cells[1].Text.Substring(1)));
                                startingAgeValueWife = gvr.Cells[0].Text;
                                boolToRunLoopOnlyOnce1 = false;
                            }
                        }
                        else
                        {
                            startingBenefitsValueWife = 0;
                        }
                        if ((gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number70)))
                        {
                            valueAtAge70 = decimal.Parse(Regex.Replace(gvr.Cells[4].Text, regExp, ""));
                            AgeValueForHigherEarner70 = gvr.Cells[0].Text;
                            ValueForHigherEarner70 = decimal.Parse((gvr.Cells[1].Text.Substring(1)));
                            lastBenefitsChangedAge70Value = decimal.Parse((gvr.Cells[1].Text.Substring(1)));
                        }
                        if ((gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number69)))
                        {
                            valueAtAge69 = decimal.Parse((gvr.Cells[5].Text.Substring(1)));
                        }
                        if ((gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number70)) && Globals.Instance.Age70ChangeForHusb)
                        {
                            AgeValueForWifeAt70 = gvr.Cells[0].Text;
                            BenefitsValueForWifeAt70 = decimal.Parse((gvr.Cells[2].Text.Substring(1)));
                        }
                    }
                    else
                    {
                        if (gvr.Cells[2].Text.Equals(Constants.ZeroWithAsterisk))
                        {
                            AgeWhenHigherEarnerClaimandSuspend = gvr.Cells[0].Text;
                        }
                        if (!((gvr.Cells[2].Text).Equals(Constants.ZeroWithOutAsterisk) || ((gvr.Cells[2].Text).Equals(Constants.ZeroWithAsterisk))))
                        {
                            if (boolToRunLoopOnlyOnce)
                            {
                                startingBenefitsValueHusb = decimal.Parse((gvr.Cells[2].Text.Substring(1)));
                                startingAgeValueHusb = gvr.Cells[0].Text;
                                boolToRunLoopOnlyOnce = false;
                            }
                        }
                        else
                        {
                            startingBenefitsValueHusb = 0;
                        }

                        if (!((gvr.Cells[1].Text).Equals(Constants.ZeroWithOutAsterisk) || ((gvr.Cells[1].Text).Equals(Constants.ZeroWithAsterisk))))
                        {
                            if (boolToRunLoopOnlyOnce1)
                            {
                                startingBenefitsValueWife = decimal.Parse((gvr.Cells[1].Text.Substring(1)));
                                startingAgeValueWife = gvr.Cells[0].Text;
                                boolToRunLoopOnlyOnce1 = false;
                            }
                        }
                        else
                        {
                            startingBenefitsValueWife = 0;
                        }

                        if ((gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number69)))
                        {
                            valueAtAge69 = decimal.Parse((gvr.Cells[5].Text.Substring(1)));
                        }
                        if ((gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number70)))
                        {
                            valueAtAge70 = decimal.Parse(Regex.Replace(gvr.Cells[4].Text, regExp, ""));
                            lastBenefitsChangedAge70Value = decimal.Parse((gvr.Cells[2].Text.Substring(1)));
                            AgeValueForHigherEarner70 = gvr.Cells[0].Text;
                            ValueForHigherEarner70 = decimal.Parse((gvr.Cells[2].Text.Substring(1)));
                        }
                        if ((gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number70)) && Globals.Instance.Age70ChangeForWife)
                        {
                            AgeValueForWifeAt70 = gvr.Cells[0].Text;
                            BenefitsValueForWifeAt70 = decimal.Parse((gvr.Cells[1].Text.Substring(1)));
                        }
                    }

                    #endregion Code to get strating and age 69 and age 70 values for husb and wife
                }
                return cummTotal;
            }
            #endregion
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// This routine builds Claim & Suspend For Marrried only   Date:31/12/2014
        /// </summary>
        /// <param name="Grid1"></param>
        /// <param name="strategy"></param>
        /// <param name="startAge"></param>
        /// <param name="gridNote"></param>
        /// <returns></returns>
        public int BuildGridSuspendRestrictApp(GridView Grid1, calc_restrict_strategy strategy, calc_start startAge, ref System.Web.UI.WebControls.Label gridNote, String gridname)
        {
            try
            {

                buildGridAt70 = false;
                fBuildGridat70 = false;
                at70 = false;

                DateTime dtLimit = new DateTime(1954, 01, 02);

                int intClaimedAgeHusb = RestrictAppIncomeProp.Instance.intHusbandClaimAge;
                int intCurrentBenHusb = RestrictAppIncomeProp.Instance.intHusbCurrentBenefit;
                int intClaimedBeneHusb = RestrictAppIncomeProp.Instance.intHusbClaimBenefit;

                int intClaimedAgeWife = RestrictAppIncomeProp.Instance.intWifeClaimAge;
                int intCurrentBenWife = RestrictAppIncomeProp.Instance.intWifeCurrentBenefit;
                int intClaimedBeneWife = RestrictAppIncomeProp.Instance.intWifeClaimBenefit;

                decimal decWifeSpousalAt66 = 0, decWifeWorkHistoryAt66 = 0, decWifeSpousalAt70 = 0, decWifeWorkHistoryAt70 = 0;
                decimal decHusbSpousalAt66 = 0, decHusbWorkHistoryAt66 = 0, decHusbSpousalAt70 = 0, decHusbWorkHistoryAt70 = 0;

                int AdditonalHusbStartClaimAge = 0, AdditonalWifeStartClaimAge = 0;

                #region VARIABLE DECLARATION
                cumm65 = 0;
                cumm80 = 0;
                int husbFirstPayAge = 0, wifeFirstPayAge = 0;
                badStrat = false;
                bool IsCalculatedFraSpousal = false;
                int baseAge = 62;
                AgeWhenHigherEarnerClaimandSuspend = string.Empty;
                odlgerGreater = 0;
                decimal workingBenefitsat70Husb = 0; decimal workingBenefitsat70Wife = 0, decWifeSpousalBen = 0, decHusbSpousalBen = 0, FRABenefit = 0; ;
                Globals.Instance.intRestrictAppIncomeFullHusb = 0;
                Globals.Instance.intRestrictAppIncomeMaxHusb = 0;
                Globals.Instance.intRestrictAppIncomeFullWife = 0;
                Globals.Instance.intRestrictAppIncomeMaxWife = 0;
                #endregion

                #region CODE TO CALCULATE THE AGE AND SET THE FRA YEAR AND FIRST PAY YEAR
                //if date of birth of husb or wife is 1958 or 1959
                //changed due to starting values were showing at wrong age
                if ((birthYearWife == 1959 || birthYearWife == 1958) && (birthYearHusb == 1959 || birthYearHusb == 1958))
                {
                    wifeFRA = 66;
                    husbFRA = 66;
                }
                else if (birthYearWife == 1959 || birthYearWife == 1958)
                {
                    husbFRA = Convert.ToInt32(FRA(birthYearHusb));
                    wifeFRA = 66;
                }
                else if (birthYearHusb == 1959 || birthYearHusb == 1958)
                {
                    wifeFRA = Convert.ToInt32(FRA(birthYearWife));
                    husbFRA = 66;
                }
                else
                {
                    husbFRA = Convert.ToInt32(FRA(birthYearHusb));
                    wifeFRA = Convert.ToInt32(FRA(birthYearWife));
                }
                //to set the first pay age ie when the benefits will start
                switch (startAge)
                {
                    case calc_start.fra6667:
                        if (decBeneHusb == decBeneWife)
                        {
                            husbFirstPayAge = 70;
                            wifeFirstPayAge = 70;
                            baseAge = 70;
                            wifeFirstPay = 70;
                            husFirstPay = 70;
                        }
                        else if (decBeneHusb > decBeneWife)
                        {
                            husbFirstPayAge = 70;
                            wifeFirstPayAge = wifeFRA;
                            baseAge = wifeFRA;
                            wifeFirstPay = wifeFRA;
                            husFirstPay = 70;
                        }
                        else
                        {
                            husbFirstPayAge = husbFRA;
                            wifeFirstPayAge = 70;
                            baseAge = husbFRA;
                            wifeFirstPay = 70;
                            husFirstPay = husbFRA;
                        }
                        break;
                    case calc_start.max70:
                        baseAge = Math.Min(husbFRA, wifeFRA);
                        husbFirstPayAge = husbFRA;
                        wifeFirstPayAge = wifeFRA;
                        wifeFirstPay = wifeFRA;
                        husFirstPay = husbFRA;
                        break;
                }
                #endregion

                #region TO BUILD THE COLUMN NAMES FOR THE GRID
                disp_col dispCol;
                gridNote.Text = string.Empty;
                // Build dataset
                System.Data.DataSet incomeSet = new System.Data.DataSet();
                System.Data.DataTable incomeTab = incomeSet.Tables.Add("incomeTab");
                // Initialize grid
                Grid1.Columns.Clear();
                Grid1.AutoGenerateColumns = false;
                // Build Display Columns and Dataset Columns
                var values = (disp_col[])Enum.GetValues(typeof(disp_col));
                //to show initila of husband and wife in the grid's age columns
                string sWifeInitials = Your_Name.Substring(0, 1);
                string sHusbInitials = Spouse_Name.Substring(0, 1);
                for (int dcIx = 0; dcIx < values.Length; dcIx++)
                {
                    dispCol = (disp_col)dcIx;
                    incomeTab.Columns.Add(dispCol.ToString());
                    BoundField bf = new BoundField();
                    bf.HtmlEncode = false;
                    TableItemStyle tis = Grid1.HeaderStyle;
                    //to build the header names for grids
                    switch (dispCol)
                    {
                        //case disp_col.Year:
                        //        bf.HeaderText = "Year";
                        //    break;
                        case disp_col.Age:
                            //if (intAgeWife == intAgeHusb)
                            //    bf.HeaderText = "Age";
                            //else
                            bf.HeaderText = Constants.HeaderA + Constants.HeaderBreak + Constants.Age + Environment.NewLine + Constants.OpeningBracket + sWifeInitials + Constants.BackSlash + sHusbInitials + Constants.CloseingBracket;
                            break;
                        case disp_col.WifeInc:
                            bf.HeaderText = Constants.HeaderB + Constants.HeaderBreak + Your_Name + Constants.MonthlyIncome;
                            break;
                        case disp_col.HusbInc:
                            bf.HeaderText = Constants.HeaderC + Constants.HeaderBreak + Spouse_Name + Constants.MonthlyIncome;
                            break;
                        case disp_col.CombInc:
                            bf.HeaderText = Constants.HeaderD + Constants.HeaderBreak + Constants.CombinedMonthlyIncome;
                            break;
                        case disp_col.AnnInc:
                            bf.HeaderText = Constants.HeaderE + Constants.HeaderBreak + Constants.CombinedAnnualIncome;
                            break;
                        case disp_col.CummInc:
                            bf.HeaderText = Constants.HeaderF + Constants.HeaderBreak + Constants.CumulativeAnnualIncome;
                            break;
                        case disp_col.SurvInc:
                            bf.HeaderText = Constants.SurvivorMonthlyIncome;
                            break;
                    }
                    // Column details
                    bf.DataField = dispCol.ToString();
                    bf.ReadOnly = true;
                    // Skip the survivor column if not requested
                    if ((showSurvivor) || (dcIx != values.Length - 1))
                    {
                        Grid1.Columns.Add(bf);
                    }
                }
                incomeTab.Columns.Add("Note");

                #endregion

                #region CODE TO POPULATE THE ROWS AND ADDING AGES TO THE GRID
                //  Populate rows
                // Date : 20112014
                // Description : Add Extra Column "YEAR" in Grid.
                #region local variables
                int year = 0;
                int HigherPIA_Age62yr = 0, LowerPIA_FRA_Year = 0;
                int ageHusb = 0, ageWife = 0, ageDiff = 0;
                age70SurvBene = 0;
                age70SurvBeneAge = string.Empty;
                decimal currBene = 0, currBeneAnnual = 0, currAnnualBenefitsWife = 0, currAnnualBenefitsHusb = 0;
                decimal currBeneWife = 0, currBeneHusb = 0, cummBeneWife = 0, cummBeneHusb = 0, cummBene = 0, newCurrBeneWife = 0, newCurrBeneHusb = 0;
                decimal spousalBene = 0;
                bool IsHusbLess70AtSpouse66 = false;

                DateTime datediffHusb66 = wifeBirthDate.AddYears(66);
                TimeSpan difftimespanHusb66 = datediffHusb66.Subtract(husbBirthDate);
                int yearHusb66 = (int)Math.Floor((double)difftimespanHusb66.Days / 365.2425);
                int monthsHusb66 = Convert.ToInt32(((double)difftimespanHusb66.Days / 30.436875));
                int multiplyHusb66 = monthsHusb66 - yearHusb66 * 12;
                //if (yearHusb66 < 70)
                //IsHusbLess70AtSpouse66 = true;
                //bool needAsterisk = false;
                //bool needAsteriskForFullRetirement = false;



                benefit_source wifeBenefitSource = benefit_source.None;
                benefit_source husbBenefitSource = benefit_source.None;
                int youngest = Math.Min(intAgeHusb, intAgeWife);
                int oldest = Math.Max(intAgeHusb, intAgeWife);
                int ageSpan = oldest - youngest;
                #endregion local variables
                if (youngest == intAgeWife && decBeneHusb > decBeneWife)
                    ageDiff = ageSpan; // most common case
                else
                    ageDiff = 0 - ageSpan; // inverted
                breakEvenAmt = new int[100];
                breakEvenAge = new string[100];
                int highBE = 0;
                //To print Age According to New Calculation    Date:12/29/2014
                if (intAgeHusb > intAgeWife)
                {
                    year = birthYearHusb + 62;
                    youngest = intAgeWife;
                    oldest = intAgeHusb;
                }
                else
                {
                    year = birthYearWife + 62;
                    youngest = intAgeHusb;
                    oldest = intAgeWife;
                }

                if (decBeneWife > decBeneHusb)
                {
                    HigherPIA_Age62yr = birthYearWife + 62;
                    LowerPIA_FRA_Year = FRAYear(husbBirthDate);
                }
                else
                {
                    HigherPIA_Age62yr = birthYearHusb + 62;
                    LowerPIA_FRA_Year = FRAYear(wifeBirthDate);
                }
                youngest = 62 - ageSpan;
                //  Build the data rows and set age of wife and husband
                for (int aIx = youngest; aIx <= 90;)//int aIx = 62 - ageSpan; aIx < 99; aIx++)
                {
                    ageWife = aIx;
                    ageHusb = aIx;
                    if (intAgeWife < intAgeHusb)
                        ageHusb = aIx + intAgeHusb - intAgeWife;
                    else
                        ageWife = aIx + intAgeWife - intAgeHusb;

                    #endregion

                    #region Restrict App Income Change

                    #region New Special Cases implemented on 24 march 2007
                    if (RestrictAppIncomeProp.Instance.BoolRestrictNewCase)
                    {
                        //Strategy Explaination
                        //Strategy-1 : Lower Benefit spouse will claim WHB at his age 62.. Higher benefit spouse will claim Maxed out WHB at 70.. Lower benefit will switches to spousal benefit when Higher claims Maxed Out WHB at 70
                        //Strategy-2 : Lower Benefit spouse will claim WHB at his age 66.. Higher benefit spouse will claim Maxed out WHB at 70.. Lower benefit will switches to spousal benefit when Higher claims Maxed Out WHB at 70

                        if (decBeneHusb >= decBeneWife) //Wife will be lower benefit spouse
                        {
                            if (strategy == calc_restrict_strategy.NoClaim62)
                            {
                                //if (ageHusb == intAgeHusb && currBeneHusb == 0)
                                //{
                                //    currBeneHusb = decBeneHusb;
                                //}

                                if (ageWife == 62 && currBeneWife == 0)
                                {
                                    //wife claims WHB
                                    currBeneWife = startFactor(ageWife, decBeneWife, birthYearWife);
                                    //code to check if age is greater than 62
                                    if (intAgeWife >= 62)
                                    {
                                        //code to check for age greater than 68
                                        if (intAgeWife > wifeFRA)
                                        {
                                            //give same PIA as the benefits no COlA
                                            currBeneWife = decBeneWife;
                                        }
                                        else
                                        {
                                            //calculate the benefis including COLA
                                            currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                        }
                                    }
                                }

                            }
                            else if (strategy == calc_restrict_strategy.NoClaim66)
                            {
                                //if (ageHusb == intAgeHusb && currBeneHusb == 0)
                                //{
                                //    currBeneHusb = decBeneHusb;
                                //}

                                if (ageWife == wifeFRA && currBeneWife == 0)
                                {
                                    //wife claims WHB
                                    if (intAgeWife > wifeFRA)
                                    {
                                        multiply = AgeAbove68YearsOfWife();
                                        currBeneWife = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                                        RestrictAppIncomeProp.Instance.boolClaimToWorkHistory = true;
                                    }
                                    else
                                    {
                                        currBeneWife = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                        RestrictAppIncomeProp.Instance.boolClaimToWorkHistory = true;
                                    }
                                }

                            }

                            if (ageHusb == 70)
                            {
                                #region maxed out Work History Benefit for Husband
                                decimal workingBenefitsat70;
                                if (intAgeHusb > husbFRA)
                                {
                                    multiply = AgeAbove68YearsOfHusb();
                                    workingBenefitsat70 = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                }
                                else
                                {
                                    workingBenefitsat70 = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                }
                                workingBenefitsat70 = ColaUpSpousal(ageHusb, workingBenefitsat70, birthYearHusb, intAgeHusb, husbFRA);

                                if (workingBenefitsat70 > currBeneHusb)
                                {
                                    currBeneHusb = workingBenefitsat70;
                                    at70 = true;
                                    Globals.Instance.BoolSkipWHBSwitchRestrict = false;
                                }
                                else
                                {
                                    Globals.Instance.BoolSkipWHBSwitchRestrict = true;
                                }
                                #endregion maxed out Work History Benefit for Husband

                                #region Spousal benefit switch for Wife
                                decimal spousalBenefits = 0;
                                if (ageWife == 62)
                                    spousalBenefits = SpousalBenefitsWifeAtHusbAge62(ageHusb, ageWife, currBeneWife);
                                else
                                {
                                    spousalBenefits = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                    spousalBenefits = spousalBenefits / 2;
                                }


                                if (ageWife == wifeFRA)
                                {
                                    if (currBeneWife < spousalBenefits)
                                    {
                                        currBeneWife = spousalBenefits;
                                        RestrictAppIncomeProp.Instance.boolClaimToWorkHistory = false;
                                    }

                                }
                                else
                                {
                                    if (currBeneWife < spousalBenefits)
                                    {
                                        currBeneWife = spousalBenefits;
                                    }
                                }
                                #endregion Spousal benefit switch for Wife
                            }
                        }
                        else
                        {
                            if (strategy == calc_restrict_strategy.NoClaim62)
                            {
                                //if (ageWife == intAgeWife && currBeneWife == 0)
                                //{
                                //    currBeneWife = decBeneWife;
                                //}

                                if (ageHusb == 62 && currBeneHusb == 0)
                                {
                                    //Husb claims WHB
                                    currBeneHusb = startFactor(ageHusb, decBeneHusb, birthYearHusb);
                                    //code to check if age is greater than 62
                                    if (intAgeHusb >= 62)
                                    {
                                        //code to check for age greater than 68
                                        if (intAgeHusb > husbFRA)
                                        {
                                            //give same PIA as the benefits no COlA
                                            currBeneHusb = decBeneHusb;
                                        }
                                        else
                                        {
                                            //calculate the benefis including COLA
                                            currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                        }
                                    }
                                }
                            }
                            else if (strategy == calc_restrict_strategy.NoClaim66)
                            {
                                //if (ageWife == intAgeWife && currBeneWife == 0)
                                //{
                                //    currBeneWife = decBeneWife;
                                //}

                                if (ageHusb == husbFRA && currBeneHusb == 0)
                                {
                                    //Husb claims WHB
                                    //Work History Benefit for husband
                                    if (intAgeHusb > husbFRA)
                                    {
                                        multiply = AgeAbove68YearsOfHusb();
                                        currBeneHusb = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                                    }
                                    else
                                    {
                                        currBeneHusb = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                    }
                                }
                            }

                            if (ageWife == 70)
                            {
                                #region Wife claims Maxed out WHB
                                decimal workingBenefitsat70;
                                if (intAgeWife > wifeFRA)
                                {
                                    multiply = AgeAbove68YearsOfWife();
                                    workingBenefitsat70 = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                }
                                else
                                {
                                    workingBenefitsat70 = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                }

                                workingBenefitsat70 = ColaUpSpousal(ageWife, workingBenefitsat70, birthYearWife, intAgeWife, wifeFRA);
                                if (workingBenefitsat70 > currBeneWife)
                                {
                                    currBeneWife = workingBenefitsat70;
                                    fBuildGridat70 = true;
                                    Globals.Instance.BoolSkipWHBSwitchRestrict = false;
                                }
                                else
                                {
                                    Globals.Instance.BoolSkipWHBSwitchRestrict = true;
                                }
                                #endregion Wife claims Maxed out WHB

                                #region Husb switches to spousal benefit
                                decimal spousalBenefits = 0;//SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                if (ageHusb == 62)
                                {
                                    spousalBenefits = SpousalBenefitsHusbAtWifeAge62(ageHusb, ageWife, currBeneHusb);
                                }
                                else
                                {
                                    spousalBenefits = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                    spousalBenefits = spousalBenefits / 2;
                                }

                                if (ageHusb == husbFRA)
                                {
                                    if (currBeneHusb < spousalBenefits)
                                    {
                                        currBeneHusb = spousalBenefits;
                                        RestrictAppIncomeProp.Instance.boolClaimToWorkHistory = false;
                                    }
                                }
                                else
                                {
                                    if (currBeneHusb < spousalBenefits)
                                    {
                                        currBeneHusb = spousalBenefits;
                                    }

                                }

                                #endregion Husb switches to spousal benefit

                            }

                        }

                    }
                    #endregion New Special Cases implemented on 24 march 2007
                    else
                    {
                        if (RestrictAppIncomeProp.Instance.BoolRestrictSpecialCase)
                        {
                            decimal SpousalBenefits = 0;

                            #region Additional Strategy 
                            //Strategy 1
                            #region Strategy starts at age 62 of spouse whose benefit is greater (Additional 1)
                            if (strategy == calc_restrict_strategy.Additional1)
                            {
                                #region Husband benefit greater than Wife
                                if (decBeneHusb > decBeneWife)
                                {
                                    if (ageHusb == intAgeHusb && ageHusb >= 62 && currBeneHusb == 0)
                                    {
                                        AdditonalHusbStartClaimAge = ageHusb;
                                        //decimal reducedworkingBenefitsat62;
                                        ////Reduced Work History benefit for husband
                                        //reducedworkingBenefitsat62 = startFactor(ageHusb, decBeneHusb, birthYearHusb);
                                        //currBeneHusb = reducedworkingBenefitsat62;
                                        currBeneHusb = startFactor(ageHusb, decBeneHusb, birthYearHusb);
                                        //code to check if age is greater than 62
                                        if (intAgeHusb >= 62)
                                        {
                                            //code to check for age greater than 68
                                            if (intAgeHusb > husbFRA)
                                            {
                                                //give same PIA as the benefits no COlA
                                                currBeneHusb = decBeneHusb;
                                            }
                                            else
                                            {
                                                //calculate the benefis including COLA
                                                currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                            }
                                        }
                                    }
                                    else if (ageHusb == 62 && intAgeHusb < 62 && currBeneHusb == 0)
                                    {
                                        AdditonalHusbStartClaimAge = ageHusb;
                                        currBeneHusb = startFactor(ageHusb, decBeneHusb, birthYearHusb);
                                        //code to check if age is greater than 62
                                        if (intAgeHusb >= 62)
                                        {
                                            //code to check for age greater than 68
                                            if (intAgeHusb > husbFRA)
                                            {
                                                //give same PIA as the benefits no COlA
                                                currBeneHusb = decBeneHusb;
                                            }
                                            else
                                            {
                                                //calculate the benefis including COLA
                                                currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                            }
                                        }
                                    }
                                    if (ageWife >= wifeFRA && ageHusb >= 62 && currBeneWife == 0)//Added new case ageHusb >= 62 on 03/17/2017.. removed case ageWife == WifeFirstPayAge
                                    {
                                        //Spousal benefit for Wife
                                        //code to check if age greater than 68
                                        if (intAgeHusb > husbFRA)
                                        {
                                            //to get back to fRA amount deducting the delayed credits for wife
                                            multiply = AgeAbove68YearsOfHusb();
                                            SpousalBenefits = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                                        }
                                        else
                                        {
                                            SpousalBenefits = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                        }

                                        currBeneWife = SpousalBenefits / 2;
                                    }
                                    if (ageWife == 70)
                                    {
                                        decimal workingBenefitsat70;
                                        if (intAgeWife > wifeFRA)
                                        {
                                            multiply = AgeAbove68YearsOfWife();
                                            workingBenefitsat70 = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                        }
                                        else
                                        {
                                            workingBenefitsat70 = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                        }

                                        workingBenefitsat70 = ColaUpSpousal(ageWife, workingBenefitsat70, birthYearWife, intAgeWife, wifeFRA);
                                        if (workingBenefitsat70 > currBeneWife)
                                        {
                                            currBeneWife = workingBenefitsat70;
                                            fBuildGridat70 = true;
                                            Globals.Instance.BoolSkipWHBSwitchRestrict = false;
                                        }
                                        else
                                        {
                                            Globals.Instance.BoolSkipWHBSwitchRestrict = true;
                                        }

                                        decimal spousalBenefits = 0;//SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                        if (ageHusb == 62)
                                        {
                                            spousalBenefits = SpousalBenefitsHusbAtWifeAge62(ageHusb, ageWife, currBeneHusb);
                                        }
                                        else
                                        {
                                            spousalBenefits = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                            spousalBenefits = spousalBenefits / 2;
                                        }
                                        if (currBeneHusb < spousalBenefits)
                                        {
                                            currBeneHusb = spousalBenefits;
                                            RestrictAppIncomeProp.Instance.boolSwitchToWorkHistory = true;
                                        }
                                    }
                                }
                                #endregion Husband benefit greater than Wife

                                #region Wife benefit greater than Husband
                                else
                                {
                                    //if Wife benefit is greater than Husnband
                                    if (ageWife == intAgeWife && ageWife >= 62 && currBeneWife == 0)
                                    {
                                        AdditonalWifeStartClaimAge = ageWife;
                                        //Reduced Work History benefit for wife
                                        //decimal reducedworkingBenefitsat62;
                                        //Reduced Work History benefit for husband
                                        //reducedworkingBenefitsat62 = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                        currBeneWife = startFactor(ageWife, decBeneWife, birthYearWife);
                                        //code to check if age is greater than 62
                                        if (intAgeWife >= 62)
                                        {
                                            //code to check for age greater than 68
                                            if (intAgeWife > wifeFRA)
                                            {
                                                //give same PIA as the benefits no COlA
                                                currBeneWife = decBeneWife;
                                            }
                                            else
                                            {
                                                //calculate the benefis including COLA
                                                currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                            }
                                        }
                                    }
                                    else if (ageWife == 62 && intAgeWife < 62 && currBeneWife == 0)
                                    {
                                        AdditonalWifeStartClaimAge = ageWife;
                                        currBeneWife = startFactor(ageWife, decBeneWife, birthYearWife);
                                        //code to check if age is greater than 62
                                        if (intAgeWife >= 62)
                                        {
                                            //code to check for age greater than 68
                                            if (intAgeWife > wifeFRA)
                                            {
                                                //give same PIA as the benefits no COlA
                                                currBeneWife = decBeneWife;
                                            }
                                            else
                                            {
                                                //calculate the benefis including COLA
                                                currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                            }
                                        }

                                    }

                                    if (ageHusb >= husbFRA && ageWife >= 62 && currBeneHusb == 0)//Added new case ageWife >= 62 on 03/17/2017.. removed case ageHusb==HusbFirstPayAge
                                    {
                                        //Spousal benefit for Husband
                                        //code to check if age greater than 68
                                        if (intAgeWife > wifeFRA)
                                        {
                                            //to get back to fRA amount deducting the delayed credits for wife
                                            multiply = AgeAbove68YearsOfWife();
                                            SpousalBenefits = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                                        }
                                        else
                                        {
                                            SpousalBenefits = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                        }

                                        currBeneHusb = SpousalBenefits / 2;
                                    }
                                    if (ageHusb == 70)
                                    {
                                        //maxed out Work History Benefit for Husband
                                        decimal workingBenefitsat70;
                                        if (intAgeHusb > husbFRA)
                                        {
                                            multiply = AgeAbove68YearsOfHusb();
                                            workingBenefitsat70 = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                        }
                                        else
                                        {
                                            workingBenefitsat70 = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                        }
                                        workingBenefitsat70 = ColaUpSpousal(ageHusb, workingBenefitsat70, birthYearHusb, intAgeHusb, husbFRA);

                                        if (workingBenefitsat70 > currBeneHusb)
                                        {
                                            currBeneHusb = workingBenefitsat70;
                                            at70 = true;
                                            Globals.Instance.BoolSkipWHBSwitchRestrict = false;
                                        }
                                        else
                                        {
                                            Globals.Instance.BoolSkipWHBSwitchRestrict = true;
                                        }
                                        decimal spousalBenefits = 0;
                                        if (ageWife == 62)
                                            spousalBenefits = SpousalBenefitsWifeAtHusbAge62(ageHusb, ageWife, currBeneWife);
                                        else
                                        {
                                            spousalBenefits = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                            spousalBenefits = spousalBenefits / 2;
                                        }
                                        if (currBeneWife < spousalBenefits)
                                        {
                                            currBeneWife = spousalBenefits;
                                            RestrictAppIncomeProp.Instance.boolSwitchToWorkHistory = true;
                                        }
                                    }
                                }
                                #endregion Wife benefit greater than Husband

                            }
                            #endregion Strategy starts at age 62 of spouse whose benefit is greater (Additional 1)

                            //Strategy 2
                            #region Strategy starts at FRA age of spouse whose benefit is greater (Additional 2)
                            else if (strategy == calc_restrict_strategy.Additional2)
                            {
                                if (decBeneHusb > decBeneWife)
                                {
                                    if (ageHusb == husbFRA && currBeneHusb == 0)
                                    {

                                        //Work History Benefit for husband
                                        if (intAgeHusb > husbFRA)
                                        {
                                            multiply = AgeAbove68YearsOfHusb();
                                            currBeneHusb = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                                        }
                                        else
                                        {
                                            currBeneHusb = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                        }
                                        //Spousal Benefit for Wife when husband's FRA age
                                        //code to check if age greater than 68
                                        int yearsWifeatHusbFra1, monthsWifeatHusbFRA1;
                                        WifeAgeatHusbandFRA(out yearsWifeatHusbFra1, out monthsWifeatHusbFRA1);
                                        int ageWifeMonth = (yearsWifeatHusbFra1 * 12) + monthsWifeatHusbFRA1;
                                        if (currBeneWife == 0 && (currBeneWife < currBeneHusb / 2) && (ageWifeMonth < 840))
                                            currBeneWife = currBeneHusb / 2;
                                    }

                                    if (ageWife == 69 && currBeneWife == 0)
                                        Globals.Instance.BoolNoClaimRestrictForAdd2 = true;

                                    if (ageWife == 70)
                                    {
                                        //Maxed out Work History Benefit for Wife
                                        decimal workingBenefitsat70;
                                        if (intAgeWife > wifeFRA)
                                        {
                                            multiply = AgeAbove68YearsOfWife();
                                            workingBenefitsat70 = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                        }
                                        else
                                        {
                                            workingBenefitsat70 = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                        }

                                        workingBenefitsat70 = ColaUpSpousal(ageWife, workingBenefitsat70, birthYearWife, intAgeWife, wifeFRA);

                                        if (workingBenefitsat70 > currBeneWife)
                                        {
                                            currBeneWife = workingBenefitsat70;
                                            fBuildGridat70 = true;
                                            Globals.Instance.BoolSkipWHBSwitchRestrict = false;
                                        }
                                        else
                                            Globals.Instance.BoolSkipWHBSwitchRestrict = true;

                                        decimal spousalBenefits = 0;
                                        if (ageHusb == 62)
                                        {
                                            spousalBenefits = SpousalBenefitsHusbAtWifeAge62(ageHusb, ageWife, currBeneHusb);
                                        }
                                        else
                                        {
                                            spousalBenefits = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                            spousalBenefits = spousalBenefits / 2;
                                        }
                                        if (currBeneHusb < spousalBenefits)
                                        {
                                            currBeneHusb = spousalBenefits;
                                            RestrictAppIncomeProp.Instance.boolSwitchToWorkHistory = true;
                                        }
                                    }
                                }
                                else
                                {
                                    if (ageWife == wifeFRA && currBeneWife == 0)
                                    {
                                        decimal workingBenefitsatFRA;
                                        //Work History Benefit for Wife
                                        multiply = AgeAbove68YearsOfWife();
                                        workingBenefitsatFRA = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                        currBeneWife = workingBenefitsatFRA;

                                        //Work History Benefit for husband
                                        if (intAgeWife > wifeFRA)
                                        {
                                            multiply = AgeAbove68YearsOfWife();
                                            currBeneWife = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                                        }
                                        else
                                        {
                                            currBeneWife = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                        }
                                        //Spousal Benefit for Wife when husband's FRA age
                                        //code to check if age greater than 68
                                        int yearsHusbatWifeFra1, monthsHusbAtWifeFRA1;
                                        HusbAgeatWifeFRA(out yearsHusbatWifeFra1, out monthsHusbAtWifeFRA1);
                                        int ageHusbMonth = (yearsHusbatWifeFra1 * 12) + monthsHusbAtWifeFRA1;
                                        if (currBeneHusb == 0 && (currBeneHusb < currBeneWife / 2) && ageHusbMonth < 840)
                                        {//if (currBeneHusb == 0 && (currBeneHusb < currBeneWife / 2))
                                            currBeneHusb = currBeneWife / 2;
                                        }

                                    }
                                    if (ageHusb == 69 && currBeneHusb == 0)
                                        Globals.Instance.BoolNoClaimRestrictForAdd2 = true;

                                    if (ageHusb == 70)
                                    {
                                        //maxed out Work History Benefit for Husband
                                        decimal workingBenefitsat70;
                                        if (intAgeHusb > husbFRA)
                                        {
                                            multiply = AgeAbove68YearsOfHusb();
                                            workingBenefitsat70 = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                        }
                                        else
                                        {
                                            workingBenefitsat70 = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                        }
                                        workingBenefitsat70 = ColaUpSpousal(ageHusb, workingBenefitsat70, birthYearHusb, intAgeHusb, husbFRA);

                                        if (workingBenefitsat70 > currBeneHusb)
                                        {
                                            currBeneHusb = workingBenefitsat70;
                                            at70 = true;
                                            Globals.Instance.BoolSkipWHBSwitchRestrict = false;
                                        }
                                        else
                                        {
                                            Globals.Instance.BoolSkipWHBSwitchRestrict = true;
                                        }
                                        decimal spousalBenefits = 0;
                                        if (ageWife == 62)
                                            spousalBenefits = SpousalBenefitsWifeAtHusbAge62(ageHusb, ageWife, currBeneWife);
                                        else
                                        {
                                            spousalBenefits = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                            spousalBenefits = spousalBenefits / 2;
                                        }
                                        if (currBeneWife < spousalBenefits)
                                        {
                                            currBeneWife = spousalBenefits;
                                            RestrictAppIncomeProp.Instance.boolSwitchToWorkHistory = true;
                                        }

                                    }
                                }
                            }
                            #endregion Strategy starts at FRA age 62 of spouse whose benefit is greater (Additional 2)

                            #endregion Additional Strategy
                        }
                        else
                        {
                            #region If unclaimed spouse's age is below the age limit (jan 2 1954)
                            if ((husbBirthDate < dtLimit && Globals.Instance.BoolIsWifeClaimedBenefit) || (wifeBirthDate < dtLimit && Globals.Instance.BoolIsHusbandClaimedBenefit))
                            {
                                //Strategy 1
                                #region Single Strategy
                                if (strategy == calc_restrict_strategy.SingleStrategy)
                                {
                                    #region If Husband already claimed
                                    if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                                    {
                                        if (intCurrentBenHusb > 0)
                                        {
                                            //Benefit for husband will start at his current age
                                            if (ageHusb == intAgeHusb && currBeneHusb == 0)
                                            {
                                                currBeneHusb = intCurrentBenHusb;
                                                FRABenefit = CalculateRestrictFRABenFromClaimBeneAge(intClaimedAgeHusb, intClaimedBeneHusb);
                                                //if (intAgeHusb < 66)
                                                //    FRABenefit = ColaUpSpousal(husbFRA, FRABenefit, birthYearHusb, intAgeHusb, husbFRA);
                                            }
                                            //Spousal Benefit Calculation For Wife 
                                            //starting age will be wifefirstpayage
                                            if (ageHusb >= 62 && currBeneWife == 0 && ageWife < 70 && ageWife >= wifeFRA)//ageWife == wifeFirstPayAge
                                            {
                                                //Calculate spousal benefit
                                                decWifeSpousalBen = CalculateSpousalBenefitsForRestrictAppIncome(intCurrentBenHusb, intClaimedBeneHusb, intAgeHusb, ageHusb, FRABenefit, wifeFRA);
                                                currBeneWife = decWifeSpousalBen;
                                            }
                                            if (ageWife == 70)
                                            {
                                                decimal workingBenefitsat70;
                                                if (intAgeWife > wifeFRA)
                                                {
                                                    multiply = AgeAbove68YearsOfWife();
                                                    workingBenefitsat70 = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                                }
                                                else
                                                {
                                                    workingBenefitsat70 = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                                }

                                                workingBenefitsat70 = ColaUpSpousal(ageWife, workingBenefitsat70, birthYearWife, intAgeWife, wifeFRA);

                                                if (workingBenefitsat70 > decWifeSpousalBen)
                                                {
                                                    currBeneWife = workingBenefitsat70;
                                                    fBuildGridat70 = true;
                                                    Globals.Instance.BoolSkipWHBSwitchRestrict = false;
                                                }
                                                else
                                                {
                                                    //  currBeneWife = decWifeSpousalBen;
                                                    Globals.Instance.BoolSkipWHBSwitchRestrict = true;
                                                }

                                                decimal spousalBenefits = 0;//SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                                if (ageHusb == 62)
                                                {
                                                    spousalBenefits = SpousalBenefitsHusbAtWifeAge62(ageHusb, ageWife, currBeneHusb);
                                                }
                                                else
                                                {
                                                    spousalBenefits = SpousalBenefitsHusbAtWifeAge70Restrict(ageHusb, ageWife, currBeneHusb);
                                                }
                                                if (currBeneHusb < spousalBenefits)
                                                {
                                                    currBeneHusb = spousalBenefits;
                                                    RestrictAppIncomeProp.Instance.boolSwitchToWorkHistory = true;
                                                }
                                            }
                                        }
                                    }
                                    #endregion If Husband already claimed

                                    #region If wife already claimed
                                    else if (Globals.Instance.BoolIsWifeClaimedBenefit)
                                    {
                                        //
                                        if (intCurrentBenWife > 0)
                                        {
                                            if (ageWife == intAgeWife && currBeneWife == 0)
                                            {
                                                currBeneWife = intCurrentBenWife;
                                                FRABenefit = CalculateRestrictFRABenFromClaimBeneAge(intClaimedAgeWife, intClaimedBeneWife);
                                                //if (intAgeWife < 66)
                                                //    FRABenefit = ColaUpSpousal(wifeFRA, FRABenefit, birthYearWife, intAgeWife, wifeFRA);
                                            }
                                            //Spousal Benefit Calculation For Husband
                                            if (ageWife >= 62 && currBeneHusb == 0 && ageHusb < 70 && ageHusb >= husbFRA)
                                            {
                                                //Calculate spousal benefit
                                                decHusbSpousalBen = CalculateSpousalBenefitsForRestrictAppIncome(intCurrentBenWife, intClaimedBeneWife, intAgeWife, ageWife, FRABenefit, husbFRA);
                                                currBeneHusb = decHusbSpousalBen;
                                            }
                                            if (ageHusb == 70)
                                            {
                                                decimal workingBenefitsat70;
                                                if (intAgeHusb > husbFRA)
                                                {
                                                    multiply = AgeAbove68YearsOfHusb();
                                                    workingBenefitsat70 = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                                }
                                                else
                                                {
                                                    workingBenefitsat70 = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                                }
                                                workingBenefitsat70 = ColaUpSpousal(ageHusb, workingBenefitsat70, birthYearHusb, intAgeHusb, husbFRA);
                                                //decHusbSpousalBen = CalculateSpousalBenefitsForRestrictAppIncome(intCurrentBenWife, intClaimedBeneWife, intAgeWife, ageWife, FRABenefit, husbFRA);

                                                if (workingBenefitsat70 > currBeneHusb)
                                                {
                                                    currBeneHusb = workingBenefitsat70;
                                                    at70 = true;
                                                    Globals.Instance.BoolSkipWHBSwitchRestrict = false;
                                                }
                                                else
                                                {
                                                    //currBeneHusb = decHusbSpousalBen;
                                                    Globals.Instance.BoolSkipWHBSwitchRestrict = true;
                                                }
                                                decimal spousalBenefits = 0;
                                                if (ageWife == 62)
                                                {
                                                    spousalBenefits = SpousalBenefitsWifeAtHusbAge62(ageHusb, ageWife, currBeneWife);
                                                }
                                                else
                                                {
                                                    spousalBenefits = SpousalBenefitsWifeAtHusbAge70Restrict(ageHusb, ageWife, currBeneWife);
                                                }
                                                if (currBeneWife < spousalBenefits)
                                                {
                                                    currBeneWife = spousalBenefits;
                                                    RestrictAppIncomeProp.Instance.boolSwitchToWorkHistory = true;
                                                }
                                            }
                                        }
                                    }
                                    #endregion If wife already claimed
                                }
                                #endregion Single Strategy

                                //Strategy 2
                                #region New Optional Strategy
                                else
                                {
                                    #region If Huband already claimed
                                    if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                                    {
                                        if (intCurrentBenHusb > 0)
                                        {
                                            //Benefit for husband will start at his current age
                                            if (ageHusb == intAgeHusb && currBeneHusb == 0)
                                            {
                                                currBeneHusb = intCurrentBenHusb;
                                                FRABenefit = CalculateRestrictFRABenFromClaimBeneAge(intClaimedAgeHusb, intClaimedBeneHusb);
                                                //if (intAgeHusb < 66)
                                                //    FRABenefit = ColaUpSpousal(husbFRA, FRABenefit, birthYearHusb, intAgeHusb, husbFRA);
                                            }
                                            //Spousal Benefit Calculation For Wife 
                                            //starting age will be wifefirstpayage
                                            if (ageHusb >= 62 && currBeneWife == 0 && ageWife < 70 && ageWife == intAgeWife)//ageWife == wifeFirstPayAge
                                            {
                                                //Calculate Reduced Spousal benefit
                                                decimal decWifeWHBAt62 = startFactor(ageWife, decBeneWife, birthYearWife);
                                                if (intAgeWife >= 62)
                                                {
                                                    //code to check for age greater than 68
                                                    if (intAgeWife > wifeFRA)
                                                    {
                                                        //give same PIA as the benefits no COlA
                                                        decWifeWHBAt62 = decBeneWife;
                                                    }
                                                    else
                                                    {
                                                        //calculate the benefis including COLA
                                                        decWifeWHBAt62 = ColaUpSpousal(ageWife, decWifeWHBAt62, birthYearWife, intAgeWife, wifeFRA);
                                                    }
                                                }
                                                //If spousal benefit at current age is greater than reduced work history benefit at current
                                                //age then assign them spousal benefit
                                                //decimal decWifeSpousalAt62 = SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, decBeneWife);

                                                decimal decWifeSpousalBefore66 = NewCalculateReducedSpousalBenefitofWife(ageHusb, ageWife, decBeneWife);
                                                if (decWifeSpousalBefore66 >= decWifeWHBAt62)
                                                {
                                                    currBeneWife = decWifeSpousalBefore66;
                                                    Globals.Instance.BoolRestrictSpousalAt62 = true;
                                                }
                                                else
                                                    currBeneWife = decWifeWHBAt62;
                                                startingBenefitsValueWife = currBeneWife;
                                            }
                                            if (ageWife == 70)
                                            {
                                                decimal workingBenefitsat70;
                                                if (intAgeWife > wifeFRA)
                                                {
                                                    multiply = AgeAbove68YearsOfWife();
                                                    workingBenefitsat70 = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                                }
                                                else
                                                    workingBenefitsat70 = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                                workingBenefitsat70 = ColaUpSpousal(ageWife, workingBenefitsat70, birthYearWife, intAgeWife, wifeFRA);
                                                //currBeneWife = workingBenefitsat70;
                                                //startingBenefitsValueWife = currBeneWife;

                                                //Calculate Work History at 70
                                                decWifeWorkHistoryAt70 = workingBenefitsat70;
                                                //Calculate spousal benefit at 70
                                                decWifeSpousalAt70 = CalculateSpousalBenefitsForRestrictAppIncome(intCurrentBenHusb, intClaimedBeneHusb, intAgeHusb, ageHusb, FRABenefit, 70);
                                                //Hide if strategy if spousal is greater than  work history at age 70 & FRA
                                                if (decWifeSpousalAt70 > decWifeWorkHistoryAt70)
                                                    Globals.Instance.BoolShowRestOptionalCurrentAge = true;
                                            }
                                        }
                                    }
                                    #endregion If Huband already claimed

                                    #region If Wife already claimed
                                    else if (Globals.Instance.BoolIsWifeClaimedBenefit)
                                    {
                                        //
                                        if (intCurrentBenWife > 0)
                                        {
                                            #region Starting benefit for Husband
                                            if (ageWife == intAgeWife && currBeneWife == 0)
                                            {
                                                currBeneWife = intCurrentBenWife;
                                                FRABenefit = CalculateRestrictFRABenFromClaimBeneAge(intClaimedAgeWife, intClaimedBeneWife);
                                                //if (intAgeWife < 66)
                                                //    FRABenefit = ColaUpSpousal(wifeFRA, FRABenefit, birthYearWife, intAgeWife, wifeFRA);
                                            }
                                            #endregion Starting benefit for Husband

                                            #region Spousal Benefit Calculation For Husband
                                            if (ageWife >= 62 && currBeneHusb == 0 && ageHusb < 70 && ageHusb == intAgeWife)
                                            {
                                                decimal decHusbWHBAt62 = startFactor(ageHusb, decBeneHusb, birthYearHusb);
                                                if (intAgeHusb >= 62)
                                                {
                                                    //code to check for age greater than 68
                                                    if (intAgeHusb > husbFRA)
                                                    {
                                                        //give same PIA as the benefits no COlA
                                                        decHusbWHBAt62 = decBeneHusb;
                                                    }
                                                    else
                                                    {
                                                        //calculate the benefis including COLA
                                                        decHusbWHBAt62 = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                                    }
                                                }

                                                //If spousal benefit at current age is greater than reduced work history benefit at current
                                                //age then assign them spousal benefit
                                                decimal decHusbSpousalBefore66 = NewCalculateRestrictReducedSpousalBenefitofHusband(ageWife, ageHusb, decBeneHusb);
                                                //decimal decHusbSpousalAt62 = SpousalBenefitsHusbAtWifeAge62(ageHusb, ageWife, decBeneHusb);
                                                if (decHusbSpousalBefore66 >= decHusbWHBAt62)
                                                {
                                                    currBeneHusb = decHusbSpousalBefore66;
                                                    Globals.Instance.BoolRestrictSpousalAt62 = true;
                                                }
                                                else
                                                    currBeneHusb = decHusbWHBAt62;

                                                startingBenefitsValueHusb = currBeneHusb;
                                            }
                                            #endregion Spousal Benefit Calculation For Husband

                                            #region Check is spousal benefit is greater than regular benefit at age 70
                                            if (ageHusb == 70)
                                            {
                                                decimal workingBenefitsat70;
                                                if (intAgeHusb > husbFRA)
                                                {
                                                    multiply = AgeAbove68YearsOfHusb();
                                                    workingBenefitsat70 = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                                }
                                                else
                                                    workingBenefitsat70 = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                                workingBenefitsat70 = ColaUpSpousal(ageHusb, workingBenefitsat70, birthYearHusb, intAgeHusb, husbFRA);
                                                currBeneHusb = workingBenefitsat70;

                                                startingBenefitsValueHusb = currBeneHusb;

                                                //Calculate Work History at 70
                                                decHusbWorkHistoryAt70 = workingBenefitsat70;
                                                //Calculate spousal benefit at 70
                                                decHusbSpousalAt70 = CalculateSpousalBenefitsForRestrictAppIncome(intCurrentBenWife, intClaimedBeneWife, intAgeWife, ageWife, FRABenefit, 70);
                                                //Hide if strategy if spousal is greater than  work history at age 70 & FRA
                                                if (decHusbSpousalAt70 > decHusbWorkHistoryAt70)
                                                    Globals.Instance.BoolShowRestOptionalCurrentAge = true;
                                            }
                                            #endregion Check is spousal benefit is greater than regular benefit at age 70

                                        }
                                    }
                                    #endregion If Wife already claimed
                                }
                                #endregion New Optional Strategy
                            }

                            #endregion If unclaimed spouse's age is below the age limit (jan 2 1954)

                            #region If unclaimed spouse's age is beyond the age limit (jan 2 1954)

                            else
                            {
                                //strategy 1
                                #region strategy 1 spouse started at age 70
                                if (strategy == calc_restrict_strategy.At70)
                                {

                                    #region If Husband already claimed
                                    if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                                    {

                                        if (intCurrentBenHusb > 0)
                                        {
                                            if (ageHusb == intAgeHusb && currBeneHusb == 0)
                                            {
                                                currBeneHusb = intCurrentBenHusb;
                                                startingBenefitsValueHusb = currBeneHusb;

                                                FRABenefit = CalculateRestrictFRABenFromClaimBeneAge(intClaimedAgeHusb, intClaimedBeneHusb);
                                            }
                                            if (ageWife == 70 && currBeneWife == 0)
                                            {
                                                decimal workingBenefitsat70;
                                                if (intAgeWife > wifeFRA)
                                                {
                                                    multiply = AgeAbove68YearsOfWife();
                                                    workingBenefitsat70 = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                                }
                                                else
                                                    workingBenefitsat70 = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                                workingBenefitsat70 = ColaUpSpousal(ageWife, workingBenefitsat70, birthYearWife, intAgeWife, wifeFRA);
                                                currBeneWife = workingBenefitsat70;
                                                startingBenefitsValueWife = currBeneWife;

                                                //Calculate Work History at 70
                                                decWifeWorkHistoryAt70 = workingBenefitsat70;
                                                //Calculate spousal benefit at 70
                                                decWifeSpousalAt70 = CalculateSpousalBenefitsForRestrictAppIncome(intCurrentBenHusb, intClaimedBeneHusb, intAgeHusb, ageHusb, FRABenefit, 70);
                                                //Hide if strategy if spousal is greater than  work history at age 70 & FRA
                                                if (decWifeSpousalAt66 > decWifeWorkHistoryAt66 && decWifeSpousalAt70 > decWifeWorkHistoryAt70)
                                                    Globals.Instance.BoolHideStrategyAge70 = true;
                                            }

                                            if (ageWife == wifeFRA)
                                            {
                                                //Calculate spousal benefit at FRA age
                                                decWifeSpousalAt66 = CalculateSpousalBenefitsForRestrictAppIncome(intCurrentBenHusb, intClaimedBeneHusb, intAgeHusb, ageHusb, FRABenefit, wifeFRA);

                                                //Calculate Work History at FRA
                                                if (intAgeWife > wifeFRA)
                                                {
                                                    multiply = AgeAbove68YearsOfWife();
                                                    decWifeWorkHistoryAt66 = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                                }
                                                else
                                                    decWifeWorkHistoryAt66 = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                                decWifeWorkHistoryAt66 = ColaUpSpousal(ageWife, decWifeWorkHistoryAt66, birthYearWife, intAgeWife, wifeFRA);
                                            }
                                        }
                                    }
                                    #endregion If Husband already claimed

                                    #region If Wife already claimed
                                    else if (Globals.Instance.BoolIsWifeClaimedBenefit)
                                    {

                                        if (intCurrentBenWife > 0)
                                        {
                                            if (ageWife == intAgeWife && currBeneWife == 0)
                                            {
                                                currBeneWife = intCurrentBenWife;
                                                FRABenefit = CalculateRestrictFRABenFromClaimBeneAge(intClaimedAgeWife, intClaimedBeneWife);
                                                //startingAgeValueWife = ageWife.ToString();
                                                startingBenefitsValueWife = currBeneWife;
                                            }
                                            if (ageHusb == 70 && currBeneHusb == 0)
                                            {
                                                decimal workingBenefitsat70;
                                                if (intAgeHusb > husbFRA)
                                                {
                                                    multiply = AgeAbove68YearsOfHusb();
                                                    workingBenefitsat70 = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                                }
                                                else
                                                    workingBenefitsat70 = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                                workingBenefitsat70 = ColaUpSpousal(ageHusb, workingBenefitsat70, birthYearHusb, intAgeHusb, husbFRA);
                                                currBeneHusb = workingBenefitsat70;

                                                startingBenefitsValueHusb = currBeneHusb;

                                                //Calculate Work History at 70
                                                decHusbWorkHistoryAt70 = workingBenefitsat70;
                                                //Calculate spousal benefit at 70
                                                decHusbSpousalAt70 = CalculateSpousalBenefitsForRestrictAppIncome(intCurrentBenWife, intClaimedBeneWife, intAgeWife, ageWife, FRABenefit, 70);
                                                //Hide if strategy if spousal is greater than  work history at age 70 & FRA
                                                if (decHusbSpousalAt66 > decHusbWorkHistoryAt66 && decHusbSpousalAt70 > decHusbWorkHistoryAt70)
                                                    Globals.Instance.BoolHideStrategyAge70 = true;


                                            }

                                            if (ageHusb == husbFRA)
                                            {
                                                //Calculate spousal benefit at FRA
                                                decHusbSpousalAt66 = CalculateSpousalBenefitsForRestrictAppIncome(intCurrentBenWife, intClaimedBeneWife, intAgeWife, ageWife, FRABenefit, husbFRA);
                                                //Calculate work history at FRA
                                                if (intAgeHusb > husbFRA)
                                                {
                                                    multiply = AgeAbove68YearsOfHusb();
                                                    decHusbWorkHistoryAt66 = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                                }
                                                else
                                                    decHusbWorkHistoryAt66 = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                                decHusbWorkHistoryAt66 = ColaUpSpousal(ageHusb, decHusbWorkHistoryAt66, birthYearHusb, intAgeHusb, husbFRA);
                                            }
                                        }
                                    }
                                    #endregion If Wife already claimed
                                }
                                #endregion strategy 1

                                //strategy 2
                                #region strategy 2 spouse started at FRA age
                                else if (strategy == calc_restrict_strategy.AtFra)
                                {
                                    #region If Husb already claimed

                                    if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                                    {
                                        if (intCurrentBenHusb > 0)
                                        {
                                            if (ageHusb == intAgeHusb && currBeneHusb == 0)
                                            {
                                                currBeneHusb = intCurrentBenHusb;
                                                FRABenefit = CalculateRestrictFRABenFromClaimBeneAge(intClaimedAgeHusb, intClaimedBeneHusb);
                                                startingBenefitsValueHusb = currBeneHusb;
                                            }
                                            if (ageWife == wifeFRA && currBeneWife == 0)
                                            {
                                                decimal whbBenifit = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);

                                                decWifeSpousalBen = CalculateSpousalBenefitsForRestrictAppIncome(intCurrentBenHusb, intClaimedBeneHusb, intAgeHusb, ageHusb, FRABenefit, wifeFRA);

                                                if (decWifeSpousalBen >= whbBenifit)//Compare Spousal Benefit and WHB
                                                {
                                                    currBeneWife = decWifeSpousalBen;
                                                    Globals.Instance.BoolRestrictSpousalAt66 = true;
                                                }
                                                else
                                                    currBeneWife = whbBenifit;

                                                startingBenefitsValueWife = currBeneWife;
                                            }
                                        }
                                    }

                                    #endregion If Husband already claimed

                                    #region If Wife already claimed
                                    else if (Globals.Instance.BoolIsWifeClaimedBenefit)
                                    {
                                        if (intCurrentBenWife > 0)
                                        {
                                            if (ageWife == intAgeWife && currBeneWife == 0)
                                            {
                                                currBeneWife = intCurrentBenWife;
                                                FRABenefit = CalculateRestrictFRABenFromClaimBeneAge(intClaimedAgeWife, intClaimedBeneWife);
                                                startingBenefitsValueWife = currBeneWife;
                                            }
                                            if (ageHusb == husbFRA && currBeneHusb == 0)
                                            {
                                                decimal WhBenefit = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                                decHusbSpousalBen = CalculateSpousalBenefitsForRestrictAppIncome(intCurrentBenWife, intClaimedBeneWife, intAgeWife, ageWife, FRABenefit, husbFRA);
                                                if (decHusbSpousalBen >= WhBenefit) //Compare Spousal Benefit and WHB
                                                {
                                                    currBeneHusb = decHusbSpousalBen;
                                                    Globals.Instance.BoolRestrictSpousalAt66 = true;
                                                }
                                                else
                                                    currBeneHusb = WhBenefit;
                                                startingBenefitsValueHusb = currBeneHusb;
                                            }
                                        }
                                    }
                                    #endregion If Wife already claimed
                                }
                                #endregion strategy 2

                                //strategy 3
                                #region  strategy 3 Spouse started at age 62
                                else if (strategy == calc_restrict_strategy.At62)
                                {
                                    #region If Husband already claimed
                                    if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                                    {
                                        if (intCurrentBenHusb > 0)
                                        {
                                            if (ageHusb == intAgeHusb && currBeneHusb == 0)
                                            {
                                                currBeneHusb = intCurrentBenHusb;
                                                startingBenefitsValueHusb = currBeneHusb;
                                            }
                                            if (ageWife == 62 && currBeneWife == 0)
                                            {
                                                decimal decWifeWHBAt62 = startFactor(ageWife, decBeneWife, birthYearWife);
                                                if (intAgeWife >= 62)
                                                {
                                                    //code to check for age greater than 68
                                                    if (intAgeWife > wifeFRA)
                                                    {
                                                        //give same PIA as the benefits no COlA
                                                        decWifeWHBAt62 = decBeneWife;
                                                    }
                                                    else
                                                    {
                                                        //calculate the benefis including COLA
                                                        decWifeWHBAt62 = ColaUpSpousal(ageWife, decWifeWHBAt62, birthYearWife, intAgeWife, wifeFRA);
                                                    }
                                                }
                                                decimal decWifeSpousalAt62 = SpousalBenefitsWifeAtHusbAge62(ageHusb, ageWife, decBeneWife);
                                                if (decWifeSpousalAt62 >= decWifeWHBAt62)
                                                {
                                                    currBeneWife = decWifeSpousalAt62;
                                                    Globals.Instance.BoolRestrictSpousalAt62 = true;
                                                }
                                                else
                                                    currBeneWife = decWifeWHBAt62;
                                                startingBenefitsValueWife = currBeneWife;
                                            }
                                        }
                                    }
                                    #endregion If Husband already claimed

                                    #region If Wife already claimed
                                    else if (Globals.Instance.BoolIsWifeClaimedBenefit)
                                    {
                                        if (intCurrentBenWife > 0)
                                        {
                                            if (ageWife == intAgeWife && currBeneWife == 0)
                                            {
                                                currBeneWife = intCurrentBenWife;
                                                startingBenefitsValueWife = currBeneWife;
                                            }
                                            if (ageHusb == 62 && currBeneHusb == 0)
                                            {
                                                decimal decHusbWHBAt62 = startFactor(ageHusb, decBeneHusb, birthYearHusb);
                                                if (intAgeHusb >= 62)
                                                {
                                                    //code to check for age greater than 68
                                                    if (intAgeHusb > husbFRA)
                                                    {
                                                        //give same PIA as the benefits no COlA
                                                        decHusbWHBAt62 = decBeneHusb;
                                                    }
                                                    else
                                                    {
                                                        //calculate the benefis including COLA
                                                        decHusbWHBAt62 = ColaUpSpousal(ageHusb, decHusbWHBAt62, birthYearHusb, intAgeHusb, husbFRA);
                                                    }
                                                }


                                                decimal decHusbSpousalAt62 = SpousalBenefitsHusbAtWifeAge62(ageHusb, ageWife, decBeneHusb);
                                                if (decHusbSpousalAt62 >= decHusbWHBAt62)
                                                {
                                                    currBeneHusb = decHusbSpousalAt62;
                                                    Globals.Instance.BoolRestrictSpousalAt62 = true;
                                                }
                                                else
                                                    currBeneHusb = decHusbWHBAt62;

                                                startingBenefitsValueHusb = currBeneHusb;
                                            }
                                        }
                                    }

                                    #endregion If Wife already claimed
                                }
                                #endregion strategy 3

                            }
                            #endregion If unclaimed spouse's age is beyond the age limit (jan 2 1954)

                        }
                    }
                    #endregion Restrict App Income Change

                    #region CODE TO USE AND CHECK THE WHB AND TO UPDATE THE TOTALS IN THE GRID
                    //  Update running totals
                    currBene = currBeneWife + currBeneHusb;
                    //Code change on 05/28/2015 as per update from client to modify the annual total as per the FRA year and months
                    //original code was currBeneAnnual = (currBene * 12);
                    #region Code to calculate the annual benefits according to DOB
                    int years, yearsHusb, remainingMonths, remainingMonthsHusb, yearsHusbCurrent, monthsHusbCurrent, yearsWifeCurrent, monthsWifeCurrent,
                        yearsHusbatWifeFra, monthsHusbAtWifeFRA, yearsWifeAtHusbFRA, monthsWifeAtHusbFRA;
                    Customers customer = new Customers();
                    WifeAgeWhenSpouseAge62(out years, out remainingMonths);
                    HusbandAgeWhenSpouseAge62(out yearsHusb, out remainingMonthsHusb);
                    int resultHusb = FRAMonths(birthYearHusb);
                    int resultWife = FRAMonths(birthYearWife);
                    int intHusbandDifference = 0;
                    int intWifeDifference = 0;
                    DateTime datediffHusb70 = wifeBirthDate.AddYears(70);
                    TimeSpan difftimespanHusb70 = datediffHusb70.Subtract(husbBirthDate);
                    int yearHusb70 = (int)Math.Floor((double)difftimespanHusb70.Days / 365.2425);
                    int monthsHusb70 = Convert.ToInt32(((double)difftimespanHusb70.Days / 30.436875));
                    int multiplyHusb70 = monthsHusb70 - yearHusb70 * 12;
                    DateTime datediff70 = husbBirthDate.AddYears(70);
                    TimeSpan difftimespan70 = datediff70.Subtract(wifeBirthDate);
                    int yearsWife70 = (int)Math.Floor((double)difftimespan70.Days / 365.2425);
                    int monthsWife70 = Convert.ToInt32(((double)difftimespan70.Days / 30.436875));
                    int multiplyWife70 = monthsWife70 - yearsWife70 * 12;

                    //Calculate current age and months
                    SpouseAgeInMonth_Year(husbBirthDate, out monthsHusbCurrent, out yearsHusbCurrent);
                    SpouseAgeInMonth_Year(wifeBirthDate, out monthsWifeCurrent, out yearsWifeCurrent);

                    //Calculate age at spouse's FRA
                    WifeAgeatHusbandFRA(out yearsWifeAtHusbFRA, out monthsWifeAtHusbFRA);
                    HusbAgeatWifeFRA(out yearsHusbatWifeFra, out monthsHusbAtWifeFRA);


                    #region Special Case Deducition Only
                    if (RestrictAppIncomeProp.Instance.BoolRestrictSpecialCase)
                    {
                        #region Additional 1 Deductions
                        if (strategy == calc_restrict_strategy.Additional1)
                        {
                            //Additional - 1 when Wife benefit is greater
                            #region Deduction For Wife if her benefit is higher
                            if (decBeneWife > decBeneHusb)
                            {
                                //For starting benefit at actual age of wife
                                if (ageWife == intAgeWife && intAgeWife >= 62 && (ageWife - 1) == yearsWifeCurrent && yearsWifeCurrent >= 62 && monthsWifeCurrent != 0 && monthsWifeCurrent != 12)
                                {
                                    currAnnualBenefitsWife = currBeneWife * (12m + (12m - monthsWifeCurrent));
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m + (12m - monthsWifeCurrent));
                                    Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16((monthsWifeCurrent));
                                    Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeCurrent;
                                    Globals.Instance.BoolWifeAgeAdjusted = true;
                                }
                                else if (ageWife == intAgeWife && intAgeWife >= 62 && ageWife == yearsWifeCurrent && monthsWifeCurrent != 0 && monthsWifeCurrent != 12)
                                {
                                    currAnnualBenefitsWife = currBeneWife * (12m - monthsWifeCurrent);
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m - monthsWifeCurrent);
                                    Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16((monthsWifeCurrent));
                                    Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeCurrent;
                                    Globals.Instance.BoolWifeAgeAdjusted = true;
                                }
                                //For starting spousal benefit of Husband #Added on 03/17/2017
                                else if (ageWife >= 62 && ageHusb >= husbFRA && yearsHusb == ageHusb && remainingMonthsHusb != 0 && remainingMonthsHusb != 12)
                                {
                                    currAnnualBenefitsHusb = currBeneHusb * (12m - remainingMonthsHusb);
                                    BoolHusbandAgePDF = true;
                                    Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - remainingMonthsHusb);
                                    Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16((remainingMonthsHusb));
                                    Globals.Instance.HusbNumberofYearsAbove66 = ageHusb;
                                    Globals.Instance.BoolHusbandAgeAdjusted = true;
                                }

                                //Added on 04/13/2017 when Wife takes benefit at 62 then Husband will take benefit at her current age
                                if (ageWife == AdditonalWifeStartClaimAge && yearsHusb == ageHusb && ageHusb >= husbFRA && remainingMonthsHusb != 0 && remainingMonthsHusb != 12)
                                {
                                    currAnnualBenefitsHusb = currBeneHusb * (12m - remainingMonthsHusb);
                                    BoolHusbandAgePDF = true;
                                    Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - remainingMonthsHusb);
                                    Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16((remainingMonthsHusb));
                                    Globals.Instance.HusbNumberofYearsAbove66 = ageHusb;
                                    Globals.Instance.BoolHusbandAgeAdjusted = true;
                                }

                                else if (ageWife == AdditonalWifeStartClaimAge && (ageHusb - 1 == yearsHusb) && yearsHusb >= husbFRA && ageHusb >= husbFRA && remainingMonthsHusb != 0 && remainingMonthsHusb != 12)
                                {
                                    currAnnualBenefitsHusb = currBeneHusb * (12m + (12m - remainingMonthsHusb));
                                    BoolHusbandAgePDF = true;
                                    Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m + (12m - remainingMonthsHusb));
                                    Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16((remainingMonthsHusb));
                                    Globals.Instance.HusbNumberofYearsAbove66 = yearsHusb;
                                    Globals.Instance.BoolHusbandAgeAdjusted = true;
                                }


                            }
                            #endregion Deduction For Wife if her benefit is higher
                            //Additional - 1 when Husband benefit is greater
                            #region Deduction For Husband if his benefit is higher
                            else
                            {
                                //For starting benefit at actual age of husband
                                if (ageHusb == intAgeHusb && intAgeHusb >= 62 && (ageHusb - 1) == yearsHusbCurrent && yearsHusbCurrent >= 62 && monthsHusbCurrent != 0 && monthsHusbCurrent != 12)
                                {
                                    currAnnualBenefitsHusb = currBeneHusb * (12 + (12m - monthsHusbCurrent));
                                    BoolHusbandAgePDF = true;
                                    Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12 + (12m - monthsHusbCurrent));
                                    Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16((monthsHusbCurrent));
                                    Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbCurrent;
                                    Globals.Instance.BoolHusbandAgeAdjusted = true;
                                }
                                else if (ageHusb == intAgeHusb && intAgeHusb >= 62 && ageHusb == yearsHusbCurrent && monthsHusbCurrent != 0 && monthsHusbCurrent != 12)
                                {
                                    currAnnualBenefitsHusb = currBeneHusb * (12m - monthsHusbCurrent);
                                    BoolHusbandAgePDF = true;
                                    Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - monthsHusbCurrent);
                                    Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16((monthsHusbCurrent));
                                    Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbCurrent;
                                    Globals.Instance.BoolHusbandAgeAdjusted = true;
                                }

                                //For starting spousal benefit of Wife #Added on 03/17/2017
                                else if (ageHusb >= 62 && ageWife >= wifeFRA && years == ageWife && remainingMonths != 0 && remainingMonths != 12)
                                {
                                    currAnnualBenefitsWife = currBeneWife * (12m - remainingMonths);
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m - remainingMonths);
                                    Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16((remainingMonths));
                                    Globals.Instance.WifeNumberofYearsAbove66 = ageWife;
                                    Globals.Instance.BoolWifeAgeAdjusted = true;

                                }

                                //Added on 04/13/2017 when Husband takes benefit at 62 then wife will take benefit at her current age
                                if (ageHusb == AdditonalHusbStartClaimAge && years == ageWife && ageWife >= wifeFRA && remainingMonths != 0 && remainingMonths != 12)
                                {
                                    currAnnualBenefitsWife = currBeneWife * (12m - remainingMonths);
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m - remainingMonths);
                                    Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16((remainingMonths));
                                    Globals.Instance.WifeNumberofYearsAbove66 = ageWife;
                                    Globals.Instance.BoolWifeAgeAdjusted = true;

                                }
                                else if (ageHusb == AdditonalHusbStartClaimAge && (ageWife - 1 == years) && (years >= wifeFRA) && ageWife >= wifeFRA && remainingMonths != 0 && remainingMonths != 12)
                                {
                                    currAnnualBenefitsWife = currBeneWife * (12m + (12m - remainingMonths));
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m + (12m - remainingMonths));
                                    Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16((remainingMonths));
                                    Globals.Instance.WifeNumberofYearsAbove66 = years;
                                    Globals.Instance.BoolWifeAgeAdjusted = true;
                                }


                            }
                            #endregion Deduction For Husband if his benefit is higher

                        }
                        #endregion Additional 1 Deductions

                        #region Additional 2 Deductions
                        else
                        {
                            if (strategy == calc_restrict_strategy.Additional2)
                            {
                                //Additional - 2 when Wife benefit is greater
                                #region Deduction For Wife if her benefit is higher
                                if (decBeneWife > decBeneHusb)
                                {
                                    //Benefit Deduction for wife at FRA age if FRA age in months (eg. 66 years and 2 months)
                                    int intfraYear = 0, intfraMonths = 0;
                                    CalculateFRAYearsAndMonths(ref intfraYear, ref intfraMonths, birthYearWife);
                                    if (ageWife == wifeFRA && intfraYear == wifeFRA && intfraMonths != 0)
                                    {
                                        currAnnualBenefitsWife = currBeneWife * (12m - intfraMonths);
                                        BoolWifeAgePDF = true;
                                        Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m - intfraMonths);
                                        Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(intfraMonths);
                                        Globals.Instance.WifeNumberofYearsAbove66 = wifeFRA;
                                        Globals.Instance.BoolWifeAgeAdjusted = true;
                                    }



                                    //Benefit deduction for husbnad if wife taking benefit at her FRA
                                    if (ageWife == wifeFRA && (ageHusb - 1) == yearsHusbatWifeFra && monthsHusbAtWifeFRA != 0 && monthsHusbAtWifeFRA != 12)
                                    {
                                        currAnnualBenefitsHusb = currBeneHusb * (12 + (12m - monthsHusbAtWifeFRA));
                                        BoolHusbandAgePDF = true;
                                        Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12 + (12m - monthsHusbAtWifeFRA));
                                        Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16((monthsHusbAtWifeFRA));
                                        Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbatWifeFra;
                                        Globals.Instance.BoolHusbandAgeAdjusted = true;
                                    }
                                    else if (ageWife == wifeFRA && ageHusb == yearsHusbatWifeFra && monthsHusbAtWifeFRA != 0 && monthsHusbAtWifeFRA != 12)
                                    {
                                        currAnnualBenefitsHusb = currBeneHusb * (12m - monthsHusbAtWifeFRA);
                                        BoolHusbandAgePDF = true;
                                        Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - monthsHusbAtWifeFRA);
                                        Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16((monthsHusbAtWifeFRA));
                                        Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbatWifeFra;
                                        Globals.Instance.BoolHusbandAgeAdjusted = true;
                                    }


                                }
                                #endregion Deduction For Wife if her benefit is higher
                                //Additional - 2 when Husband benefit is greater
                                #region Deduction For Husband if his benefit is higher
                                else
                                {
                                    // Benefit Deduction for wife at FRA age if FRA age in months(eg. 66 years and 2 months)
                                    int intfraYear = 0, intfraMonths = 0;
                                    CalculateFRAYearsAndMonths(ref intfraYear, ref intfraMonths, birthYearHusb);
                                    if (ageHusb == husbFRA && intfraYear == husbFRA && intfraMonths != 0)
                                    {
                                        currAnnualBenefitsHusb = currBeneHusb * (12m - intfraMonths);
                                        BoolHusbandAgePDF = true;
                                        Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - intfraMonths);
                                        Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16((intfraMonths));
                                        Globals.Instance.HusbNumberofYearsAbove66 = husbFRA;
                                        Globals.Instance.BoolHusbandAgeAdjusted = true;
                                    }

                                    //Benefit deduction for Wife if Husband taking benefit at his FRA
                                    if (ageHusb == husbFRA && (ageWife - 1) == yearsWifeAtHusbFRA && monthsWifeAtHusbFRA != 0 && monthsWifeAtHusbFRA != 12)
                                    {
                                        currAnnualBenefitsWife = currBeneWife * (12m + (12m - monthsWifeAtHusbFRA));
                                        BoolWifeAgePDF = true;
                                        Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m + (12m - monthsWifeAtHusbFRA));
                                        Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16((monthsWifeAtHusbFRA));
                                        Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeAtHusbFRA;
                                        Globals.Instance.BoolWifeAgeAdjusted = true;
                                    }
                                    else if (ageHusb == husbFRA && ageWife == yearsWifeAtHusbFRA && monthsWifeAtHusbFRA != 0 && monthsWifeAtHusbFRA != 12)
                                    {
                                        currAnnualBenefitsWife = currBeneWife * (12m - monthsWifeAtHusbFRA);
                                        BoolWifeAgePDF = true;
                                        Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m - monthsWifeAtHusbFRA);
                                        Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(monthsWifeAtHusbFRA);
                                        Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeAtHusbFRA;
                                        Globals.Instance.BoolWifeAgeAdjusted = true;
                                    }
                                }
                                #endregion Deduction For Husband if his benefit is higher
                            }
                        }
                        #endregion Additional 2 Deductions

                        //Common Deduction at age 70 for Higher benefit User for both Strategies
                        #region Deduction for higher benefit spouse if lower benefit switching at age 70

                        #region Deduction for Wife at Husband age70
                        if (decBeneWife > decBeneHusb)
                        {
                            // When Husband switches to maxed out work history at 70, Wife switches to spousal benefit
                            if (RestrictAppIncomeProp.Instance.boolSwitchToWorkHistory && ageHusb == 70 && multiplyWife70 != 0 && multiplyWife70 != 12)
                            {
                                if (multiplyWife70 > 12)
                                {
                                    currAnnualBenefitsWife = currBeneWife * multiplyWife70;
                                    Globals.Instance.WifeNumberofYears = yearsWife70;
                                }
                                else if (yearsWife70 == ageWife)
                                {
                                    if (yearsWife70 == wifeFirstPayAge)//Added case on 06/12/2016 because at chart showing double asteric and at note showing single asteric and not showing months in steps
                                    {
                                        currAnnualBenefitsWife = currBeneWife * (12m - multiplyWife70);
                                        Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m - multiplyWife70);
                                        Globals.Instance.WifeNumberofMonthsAboveGrid = Convert.ToInt16(multiplyWife70);
                                        BoolWifeAgePDF = true;
                                        Globals.Instance.BoolWifeAgeAdjusted = true;
                                        Globals.Instance.WifeNumberofYears = yearsWife70;
                                    }
                                    else
                                    {
                                        currAnnualBenefitsWife = currBeneWife * (12 - multiplyWife70);
                                        Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = Convert.ToInt16((12 - multiplyWife70));
                                        BoolWifeSingleAsterisk = true;
                                        Globals.Instance.BoolWHBSingleSwitchedWife = true;
                                        Globals.Instance.WifeNumberofYears = yearsWife70;
                                    }
                                }
                                else
                                {
                                    currAnnualBenefitsWife = currBeneWife * (12 + (12 - multiplyWife70));
                                    Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = Convert.ToInt16(12 + (12 - multiplyWife70));
                                    BoolWifeDoubleAsterisk = true;
                                    intWifeDifference = ageWife - yearsWife70;
                                    Globals.Instance.WifeNumberofYears = yearsWife70;
                                }
                            }
                        }
                        #endregion Deduction for Wife at Husband age70

                        #region Deduction for Husband at Wife age70
                        else
                        {
                            // When wife switches to maxed out work history at 70 husband switches to spousal benefit
                            if (RestrictAppIncomeProp.Instance.boolSwitchToWorkHistory && ageWife == 70 && multiplyHusb70 != 0 && multiplyHusb70 != 12)
                            {
                                if (multiplyHusb70 > 12)
                                {
                                    currAnnualBenefitsHusb = currBeneHusb * multiplyHusb70;
                                    Globals.Instance.HusbandNumberofYears = yearHusb70;
                                }
                                else if (yearHusb70 == ageHusb)
                                {

                                    if (yearHusb70 == husbFirstPayAge)//Added case on 06/12/2016 because at chart showing double asteric and at note showing single asteric and not showing months in steps
                                    {
                                        currAnnualBenefitsHusb = currBeneHusb * (12m - multiplyHusb70);
                                        Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - multiplyHusb70);
                                        Globals.Instance.HusbNumberofMonthsAboveGrid = Convert.ToInt16(multiplyHusb70);
                                        BoolWifeAgePDF = true;
                                        Globals.Instance.BoolHusbandAgeAdjusted = true;
                                        Globals.Instance.HusbandNumberofYears = yearHusb70;
                                    }
                                    else
                                    {
                                        currAnnualBenefitsHusb = currBeneHusb * (12 - multiplyHusb70);
                                        Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior = Convert.ToInt16(12 - multiplyHusb70);
                                        BoolHusbSingleAsterisk = true;
                                        Globals.Instance.BoolWHBSingleSwitchedHusb = true;
                                        Globals.Instance.HusbandNumberofYears = yearHusb70;
                                    }
                                }
                                else
                                {
                                    currAnnualBenefitsHusb = currBeneHusb * (12 + (12 - multiplyHusb70));
                                    Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior = Convert.ToInt16(12 + (12 - multiplyHusb70));
                                    BoolHusbDoubleAsterisk = true;
                                    intHusbandDifference = ageHusb - yearHusb70;
                                    Globals.Instance.HusbandNumberofYears = yearHusb70;
                                }
                            }
                        }
                        #endregion Deduction for Husband at Wife age70

                        #endregion Deduction for higher benefit spouse if lower benefit switching at age 70
                    }
                    #endregion Special Case Deducition Only

                    #region Three Strategies Deductions
                    if (strategy == calc_restrict_strategy.At62)
                    {
                        if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                        {
                            if (ageWife >= 62 && intAgeWife == ageWife && monthsWifeCurrent > 0 && monthsWifeCurrent < 12 && yearsWifeCurrent == ageWife)
                            {
                                currAnnualBenefitsWife = currBeneWife * (12m - monthsWifeCurrent);
                                Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16((12m - monthsWifeCurrent));
                                Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(monthsWifeCurrent);
                                BoolWifeAgePDF = true;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                                Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeCurrent;

                            }
                            else if (ageWife >= 62 && intAgeWife == ageWife && monthsWifeCurrent > 0 && monthsWifeCurrent < 12 && yearsWifeCurrent == ageWife - 1)
                            {
                                currAnnualBenefitsWife = currBeneWife * (12m + (12m - monthsWifeCurrent));
                                Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m + (12m - monthsWifeCurrent));
                                Globals.Instance.WifeNumberofMonthsinSteps = monthsWifeCurrent;
                                BoolWifeAgePDF = true;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                                Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeCurrent;
                            }
                        }
                        else
                        {
                            if (ageHusb >= 62 && intAgeHusb == ageHusb && monthsHusbCurrent > 0 && monthsHusbCurrent < 12 && yearsHusbCurrent == ageHusb)
                            {
                                currAnnualBenefitsHusb = currBeneHusb * (12m - monthsHusbCurrent);
                                Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - monthsHusbCurrent);
                                Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(monthsHusbCurrent);
                                BoolHusbandAgePDF = true;
                                Globals.Instance.BoolHusbandAgeAdjusted = true;
                                Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbCurrent;
                            }
                            else if (ageHusb >= 62 && intAgeHusb == ageHusb && monthsHusbCurrent > 0 && monthsHusbCurrent < 12 && yearsHusbCurrent == ageHusb - 1)
                            {
                                currAnnualBenefitsHusb = currBeneHusb * (12m + (12m - monthsHusbCurrent));
                                Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m + (12m - monthsHusbCurrent));
                                Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(monthsHusbCurrent);
                                BoolHusbandAgePDF = true;
                                Globals.Instance.BoolHusbandAgeAdjusted = true;
                                Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbCurrent;
                            }
                        }
                    }

                    #endregion Three Strategies Deductions

                    #region Other Regular Deductions
                    else
                    {
                        //Conditions for Wife
                        #region Deduction for Wife

                        if (Globals.Instance.BoolIsShowRestrictFull)
                        {
                            if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                            {
                                if (intAgeHusb == ageHusb && ageHusb >= 62 && ageWife < 70 && ageWife >= wifeFRA && (yearsWifeCurrent >= wifeFRA) && (yearsWifeCurrent == ageWife - 1) && (12 > monthsWifeCurrent) && (monthsWifeCurrent > 0))
                                {
                                    currAnnualBenefitsWife = currBeneWife * (12m + (12m - monthsWifeCurrent));
                                    Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m + (12m - monthsWifeCurrent));
                                    Globals.Instance.WifeNumberofMonthsinSteps = monthsWifeCurrent;
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.BoolWifeAgeAdjusted = true;
                                    Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeCurrent;
                                }
                                else if (intAgeHusb == ageHusb && ageHusb >= 62 && ageWife < 70 && ageWife >= wifeFRA && yearsWifeCurrent == ageWife && (12 > monthsWifeCurrent) && (monthsWifeCurrent > 0))
                                {
                                    currAnnualBenefitsWife = currBeneWife * (12m - monthsWifeCurrent);
                                    Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16((12m - monthsWifeCurrent));
                                    Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(monthsWifeCurrent);
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.BoolWifeAgeAdjusted = true;
                                    Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeCurrent;
                                }
                            }
                            else
                            {
                                if (intAgeWife == ageWife && ageWife >= 62 && ageHusb < 70 && ageHusb >= husbFRA && (yearsHusbCurrent >= husbFRA) && (yearsHusbCurrent == ageHusb - 1) && (12 > monthsHusbCurrent) && (monthsHusbCurrent > 0))
                                {
                                    currAnnualBenefitsHusb = currBeneHusb * (12m + (12m - monthsHusbCurrent));
                                    Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m + (12m - monthsHusbCurrent));
                                    Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(monthsHusbCurrent);
                                    BoolHusbandAgePDF = true;
                                    Globals.Instance.BoolHusbandAgeAdjusted = true;
                                    Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbCurrent;
                                }
                                else if (intAgeWife == ageWife && ageWife >= 62 && ageHusb < 70 && ageHusb >= husbFRA && yearsHusbCurrent == ageHusb && (12 > monthsHusbCurrent) && (monthsHusbCurrent > 0))
                                {
                                    currAnnualBenefitsHusb = currBeneHusb * (12m - monthsHusbCurrent);
                                    Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - monthsHusbCurrent);
                                    Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(monthsHusbCurrent);
                                    BoolHusbandAgePDF = true;
                                    Globals.Instance.BoolHusbandAgeAdjusted = true;
                                    Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbCurrent;
                                }
                            }

                        }





                        if (birthYearWife >= 1955 && birthYearWife <= 1959 && ageWife == wifeFRA && wifeFirstPayAge == wifeFRA && currBeneWife != 0 && strategy != calc_restrict_strategy.At62)
                        {

                            currAnnualBenefitsWife = annualBenefitsAsPerBirthDate(birthYearWife, currBeneWife);
                            Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12 - customer.FRAMonthsRemaining(birthYearWife));
                            Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(customer.FRAMonthsRemaining(birthYearWife));
                            BoolWifeAgePDF = true;
                            Globals.Instance.BoolWifeAgeAdjusted = true;
                            Globals.Instance.WifeNumberofYearsAbove66 = ageWife;
                        }
                        else if ((intAgeWife >= 62) && (currBeneWife != 0) && (wifeFirstPayAge == years) && (years != wifeFRA) && (ageWife == years) && (remainingMonths != 12) && (remainingMonths > 0))
                        {
                            currAnnualBenefitsWife = currBeneWife * (12m - remainingMonths);
                            BoolWifeAgePDF = true;
                            Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16((12m - remainingMonths));
                            Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16((remainingMonths));
                            Globals.Instance.WifeNumberofYearsAbove66 = ageWife;
                            Globals.Instance.BoolWifeAgeAdjusted = true;
                        }
                        else if ((intAgeWife >= 62) && (currBeneWife != 0) && (ageWife >= wifeFRA) && (wifeFirstPayAge == ageWife) && (ageWife < 70) && (ageHusb == 62) && (intAgeHusb < 62) && (strategy == calc_restrict_strategy.SingleStrategy))
                        {
                            if (intAgeWifeMonth < intAgeHusbMonth && ageWife == wifeFRA && intAgeWife == wifeFRA)
                            {
                                int intMultiplyMonth = 0;
                                currAnnualBenefitsWife = WifeSpouseAgeLessThan62(out intMultiplyMonth, currBeneWife);
                                BoolWifeAgePDF = true;
                                Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(intMultiplyMonth);
                                Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(12 - intMultiplyMonth);
                                Globals.Instance.WifeNumberofYearsAbove66 = ageWife;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                            }
                        }
                        else if ((currBeneWife != 0) && (wifeFirstPayAge == ageWife) && (ageHusb == husbFRA) && (wifeBenefitSource == benefit_source.Spouse))
                        {
                            DateTime datediffWife = husbBirthDate.AddYears(husbFRA);
                            TimeSpan difftimespanWife = datediffWife.Subtract(wifeBirthDate);
                            int yearsWifeFRA = (int)((double)difftimespanWife.Days / 365.2425);
                            int monthsWifeFRA = Convert.ToInt32(((double)difftimespanWife.Days / 30.436875));
                            int inthusbmonthdiff;
                            if (husbFRA * 12 > resultHusb)
                                inthusbmonthdiff = husbFRA * 12 - resultHusb;
                            else
                                inthusbmonthdiff = resultHusb - husbFRA * 12;
                            int intMultiMonth = (monthsWifeFRA - yearsWifeFRA * 12) + inthusbmonthdiff;
                            if (yearsWifeFRA == husbFirstPayAge && intAgeHusbMonth >= intAgeWifeMonth)
                            {
                                currAnnualBenefitsWife = currBeneWife * ((12m - intMultiMonth));
                                if (intMultiMonth != 0 && intMultiMonth != 12)
                                {
                                    Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16((12m - intMultiMonth));
                                    Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(intMultiMonth);
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.BoolWifeAgeAdjusted = true;
                                    Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeFRA;
                                }
                            }
                            else if (yearsWifeFRA == husbFirstPayAge && intAgeHusbMonth < intAgeWifeMonth && ageWife == ageHusb)
                            {
                                currAnnualBenefitsWife = currBeneWife * (12m - intMultiMonth);
                                if (intMultiMonth != 0 && intMultiMonth != 12)
                                {
                                    Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16((12m - intMultiMonth));
                                    Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(intMultiMonth);
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.BoolWifeAgeAdjusted = true;
                                    Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeFRA;
                                }
                            }
                            //Newly added on 21/11/2016
                            else if (yearsWifeFRA == wifeFirstPayAge && ageWife == yearsWifeFRA && intMultiMonth < 12)
                            {
                                currAnnualBenefitsWife = currBeneWife * (12m - intMultiMonth);
                                if (intMultiMonth != 0 && intMultiMonth != 12)
                                {
                                    Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16((12m - intMultiMonth));
                                    Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(intMultiMonth);
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.BoolWifeAgeAdjusted = true;
                                    Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeFRA;
                                }
                            }
                            else
                            {
                                if (ageWife != ageHusb)
                                {

                                    if (intMultiMonth > 0 && intMultiMonth < 12)
                                    {
                                        currAnnualBenefitsWife = currBeneWife * (12m + (12m - intMultiMonth));
                                        Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m + (12m - intMultiMonth));
                                        Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(12m - (12m - intMultiMonth));
                                        BoolWifeAgePDF = true;
                                        Globals.Instance.BoolWifeAgeAdjusted = true;
                                        Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeFRA;
                                    }
                                }

                            }
                        }

                        else if (currBeneWife != 0 && ageWife < 70 && isWifeSpouse && strategy == calc_restrict_strategy.At70 && intMonthsWife > 0 && intMonthsWife != 12)
                        {
                            if (ageWife == wifeFRA)
                            {
                                currAnnualBenefitsWife = currBeneWife * 12m;
                                isWifeSpouse = false;
                            }
                            else if (ageWife > intWifeYearsMax)
                            {
                                currAnnualBenefitsWife = currBeneWife * (12m + (12 - intMonthsWife));
                                Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m + (12 - intMonthsWife));
                                Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(intMonthsWife);
                                BoolWifeAgePDF = true;
                                isWifeSpouse = false;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                                Globals.Instance.WifeNumberofYears = intWifeYearsMax;
                            }
                            else
                            {
                                currAnnualBenefitsWife = currBeneWife * (12m - intMonthsWife);
                                Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m - intMonthsWife);
                                Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(intMonthsWife);
                                BoolWifeAgePDF = true;
                                isWifeSpouse = false;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                                Globals.Instance.WifeNumberofYears = intWifeYearsMax;
                            }
                        }

                        else if (!Globals.Instance.BoolSkipWHBSwitchRestrict && (decBeneHusb < decBeneWife && RestrictAppIncomeProp.Instance.BoolRestrictSpecialCase) && ageHusb == 70 && multiplyWife70 != 0 && multiplyWife70 != 12)
                        {
                            if (multiplyWife70 > 12)
                            {
                                currAnnualBenefitsWife = currBeneWife * multiplyWife70;
                                Globals.Instance.WifeNumberofYears = yearsWife70;
                            }
                            else if (yearsWife70 == ageWife)
                            {
                                if (yearsWife70 == wifeFirstPayAge)//Added case on 06/12/2016 because at chart showing double asteric and at note showing single asteric and not showing months in steps
                                {
                                    currAnnualBenefitsWife = currBeneWife * (12m - multiplyWife70);
                                    Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m - multiplyWife70);
                                    Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(multiplyWife70);
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.BoolWifeAgeAdjusted = true;
                                    Globals.Instance.WifeNumberofYearsAbove66 = yearsWife70;
                                }
                                else
                                {
                                    currAnnualBenefitsWife = currBeneWife * (12 - multiplyWife70);
                                    Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = Convert.ToInt16((12 - multiplyWife70));
                                    BoolWifeSingleAsterisk = true;
                                    Globals.Instance.BoolWHBSingleSwitchedWife = true;
                                    Globals.Instance.WifeNumberofYears = yearsWife70;
                                }
                            }
                            else
                            {
                                currAnnualBenefitsWife = currBeneWife * (12 + (12 - multiplyWife70));
                                Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = Convert.ToInt16(12 + (12 - multiplyWife70));
                                BoolWifeDoubleAsterisk = true;
                                intWifeDifference = ageWife - yearsWife70;
                                Globals.Instance.WifeNumberofYearsAbove66 = yearsWife70;
                            }
                        }


                        #endregion Deduction for Wife

                        //Conditions for Husband
                        #region Deduction for Husband

                        if (birthYearHusb >= 1955 && birthYearHusb <= 1959 && ageHusb == husbFRA && husbFirstPayAge == husbFRA && currBeneHusb != 0)
                        {
                            currAnnualBenefitsHusb = annualBenefitsAsPerBirthDate(birthYearHusb, currBeneHusb);
                            Globals.Instance.BoolHusbandAgeAdjusted = true;
                            BoolHusbandAgePDF = true;
                            Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12 - customer.FRAMonthsRemaining(birthYearHusb));
                            Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(customer.FRAMonthsRemaining(birthYearHusb));
                            Globals.Instance.HusbNumberofYearsAbove66 = ageHusb;
                        }
                        else if ((intAgeHusb >= 62) && (currBeneHusb != 0) && (remainingMonthsHusb != 12) && (yearsHusb != husbFRA) && (husbFirstPayAge == yearsHusb) && (ageHusb == yearsHusb) && (remainingMonthsHusb > 0))
                        {
                            currAnnualBenefitsHusb = currBeneHusb * (12m - remainingMonthsHusb);
                            BoolHusbandAgePDF = true;
                            Globals.Instance.HusbandNumberofMonthsinNote = (int)remainingMonthsHusb;
                            Globals.Instance.HusbandNumberofMonthsinSteps = (int)(12m - remainingMonthsHusb);
                            Globals.Instance.HusbNumberofYearsAbove66 = yearsHusb;
                            Globals.Instance.BoolHusbandAgeAdjusted = true;
                        }
                        else if ((intAgeHusb >= 62) && (currBeneHusb != 0) && (husbFirstPayAge == ageHusb) && (ageHusb >= husbFRA) && (ageHusb < 70) && (ageWife == 62) && (intAgeWife < 62) && (strategy == calc_restrict_strategy.SingleStrategy))
                        {
                            if (intAgeWifeMonth > intAgeHusbMonth && ageHusb == husbFRA && intAgeHusb == husbFRA)
                            {
                                int intMultiplyMonth = 0;
                                currAnnualBenefitsHusb = HusbandSpouseAgeLessThan62(out intMultiplyMonth, currBeneHusb);
                                BoolHusbandAgePDF = true;
                                Globals.Instance.HusbandNumberofMonthsinNote = intMultiplyMonth;
                                Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(12 - intMultiplyMonth);
                                Globals.Instance.HusbNumberofYearsAbove66 = ageHusb;
                                Globals.Instance.BoolHusbandAgeAdjusted = true;
                            }
                        }
                        else if (currBeneHusb != 0 && ageHusb < 70 && isHusbSpouse && strategy == calc_restrict_strategy.At70 && intMonthsHusb > 0 && intMonthsHusb != 12)
                        {
                            if (ageWife == wifeFRA)
                            {
                                currAnnualBenefitsHusb = currBeneHusb * 12m;
                                isHusbSpouse = false;
                            }
                            else if (ageHusb > intHusbandYearsMax)
                            {
                                currAnnualBenefitsHusb = currBeneHusb * (12m + (12 - intMonthsHusb));
                                Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m + (12 - intMonthsHusb));
                                Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(intMonthsHusb);
                                BoolHusbandAgePDF = true;
                                isHusbSpouse = false;
                                Globals.Instance.BoolHusbandAgeAdjusted = true;
                                Globals.Instance.HusbandNumberofYears = intHusbandYearsMax;
                            }
                            else
                            {
                                currAnnualBenefitsHusb = currBeneHusb * (12m - intMonthsHusb);
                                Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - intMonthsHusb);
                                Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(intMonthsHusb);
                                BoolHusbandAgePDF = true;
                                isHusbSpouse = false;
                                Globals.Instance.BoolHusbandAgeAdjusted = true;
                                Globals.Instance.HusbandNumberofYears = intHusbandYearsMax;
                            }
                        }
                        else if ((currBeneHusb != 0) && (husbFirstPayAge == ageHusb) && (ageWife == wifeFRA) && (husbBenefitSource == benefit_source.Spouse))
                        {
                            DateTime datediffHusb = wifeBirthDate.AddYears(wifeFRA);
                            TimeSpan difftimespanHusb = datediffHusb.Subtract(husbBirthDate);
                            int yearsHusbFRA = (int)((double)difftimespanHusb.Days / 365.2425);
                            int monthsHusbFRA = Convert.ToInt32(((double)difftimespanHusb.Days / 30.436875));
                            int intwifemonthdiff;
                            if (wifeFRA * 12 > resultWife)
                                intwifemonthdiff = wifeFRA * 12 - resultWife;
                            else
                                intwifemonthdiff = resultWife - wifeFRA * 12;
                            int intMultiMonth = (monthsHusbFRA - yearsHusbFRA * 12) + intwifemonthdiff;
                            if (yearsHusbFRA == husbFirstPayAge && intAgeWifeMonth >= intAgeHusbMonth)
                            {
                                currAnnualBenefitsHusb = currBeneHusb * (12m - intMultiMonth);
                                if (intMultiMonth != 0 && intMultiMonth != 12)
                                {
                                    Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - intMultiMonth);
                                    Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(intMultiMonth);
                                    BoolHusbandAgePDF = true;
                                    Globals.Instance.BoolHusbandAgeAdjusted = true;
                                    Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbFRA;
                                }
                            }

                            else if (yearsHusbFRA == husbFirstPayAge && intAgeWifeMonth < intAgeHusbMonth && ageWife == ageHusb)
                            {
                                currAnnualBenefitsHusb = currBeneHusb * (12m - intMultiMonth);
                                if (intMultiMonth != 0 && intMultiMonth != 12)
                                {
                                    Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - intMultiMonth);
                                    Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(intMultiMonth);
                                    BoolHusbandAgePDF = true;
                                    Globals.Instance.BoolHusbandAgeAdjusted = true;
                                    Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbFRA;
                                }
                            }
                            //Newly added on 21/11/2016
                            else if (yearsHusbFRA == husbFirstPayAge && ageHusb == yearsHusbFRA && intMultiMonth < 12)
                            {
                                currAnnualBenefitsHusb = currBeneHusb * (12m - intMultiMonth);
                                if (intMultiMonth != 0 && intMultiMonth != 12)
                                {
                                    Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - intMultiMonth);
                                    Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(intMultiMonth);
                                    BoolHusbandAgePDF = true;
                                    Globals.Instance.BoolHusbandAgeAdjusted = true;
                                    Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbFRA;
                                }
                            }
                            else
                            {
                                if (ageWife != ageHusb)
                                {

                                    if (intMultiMonth > 0 && intMultiMonth < 12)
                                    {
                                        currAnnualBenefitsHusb = currBeneHusb * (12m + (12m - intMultiMonth));
                                        Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m + (12m - intMultiMonth));
                                        Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(12 - (12m - intMultiMonth));
                                        BoolHusbandAgePDF = true;
                                        Globals.Instance.BoolHusbandAgeAdjusted = true;
                                        Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbFRA;
                                    }
                                }
                            }
                        }
                        else if (!Globals.Instance.BoolSkipWHBSwitchRestrict && (decBeneHusb >= decBeneWife && RestrictAppIncomeProp.Instance.BoolRestrictSpecialCase) && ageWife == 70 && multiplyHusb70 != 0 && multiplyHusb70 != 12)
                        {
                            if (multiplyHusb70 > 12)
                            {
                                currAnnualBenefitsHusb = currBeneHusb * multiplyHusb70;
                                Globals.Instance.HusbandNumberofYears = yearHusb70;
                            }
                            else if (yearHusb70 == ageHusb)
                            {

                                if (yearHusb70 == husbFirstPayAge)//Added case on 06/12/2016 because at chart showing double asteric and at note showing single asteric and not showing months in steps
                                {
                                    currAnnualBenefitsHusb = currBeneHusb * (12m - multiplyHusb70);
                                    Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - multiplyHusb70);
                                    Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(multiplyHusb70);
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.BoolHusbandAgeAdjusted = true;
                                    Globals.Instance.HusbNumberofYearsAbove66 = yearHusb70;
                                }
                                else
                                {
                                    currAnnualBenefitsHusb = currBeneHusb * (12 - multiplyHusb70);
                                    Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior = Convert.ToInt16(12 - multiplyHusb70);
                                    BoolHusbSingleAsterisk = true;
                                    Globals.Instance.BoolWHBSingleSwitchedHusb = true;
                                    Globals.Instance.HusbandNumberofYears = yearHusb70;
                                }
                            }
                            else
                            {
                                currAnnualBenefitsHusb = currBeneHusb * (12 + (12 - multiplyHusb70));
                                Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior = Convert.ToInt16(12 + (12 - multiplyHusb70));
                                BoolHusbDoubleAsterisk = true;
                                intHusbandDifference = ageHusb - yearHusb70;
                                Globals.Instance.HusbandNumberofYears = yearHusb70;
                            }
                        }
                        //husband age is smaller than 70 when spouse is age 66 (FRA) // Added on 29/09/2016
                        else if (IsHusbLess70AtSpouse66 && ageWife == wifeFRA && birthYearHusb < 1954 && strategy == calc_restrict_strategy.SingleStrategy)
                        {
                            int intMultiply = 12 - multiplyHusb66;
                            Globals.Instance.HusbSpousalBenMonthAt69 = intMultiply;
                            currAnnualBenefitsHusb = (currBeneHusb * 12) + (Globals.Instance.HusbSpousalBenefitWhenWife66 * intMultiply);
                            if (currAnnualBenefitsHusb != 0)
                            {
                                Globals.Instance.HusbandNumberofMonthsinNote = multiplyHusb66;
                                Globals.Instance.HusbandNumberofMonthsinSteps = multiplyHusb66;
                                Globals.Instance.HusbNumberofYearsAbove66 = yearHusb66;
                                Globals.Instance.HusbAnnualIcomeAt70 = currBeneHusb;
                                Globals.Instance.HusbAnnualIcomeAt69 = Globals.Instance.HusbSpousalBenefitWhenWife66;
                                IsHusbLess70AtSpouse66 = false;
                                Globals.Instance.BoolSpousalWHBat70 = true;
                                Globals.Instance.BoolHusbandAgeAdjusted = true;
                            }
                        }

                        #endregion Deduction for Husband
                    }
                    #endregion Other Regular Deductions

                    #region Optional Strategy Deduction
                    if (strategy == calc_restrict_strategy.RestrOptionAtCurrent)
                    {
                        if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                        {
                            //For starting benefit at actual age of wife
                            if (ageWife == intAgeWife && intAgeWife >= 62 && (ageWife - 1) == yearsWifeCurrent && monthsWifeCurrent != 0 && monthsWifeCurrent != 12)
                            {
                                currAnnualBenefitsWife = currBeneWife * (12m + (12m - monthsWifeCurrent));
                                BoolWifeAgePDF = true;
                                Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m + (12m - monthsWifeCurrent));
                                Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16((monthsWifeCurrent));
                                Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeCurrent;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                            }
                            else if (ageWife == intAgeWife && intAgeWife >= 62 && ageWife == yearsWifeCurrent && monthsWifeCurrent != 0 && monthsWifeCurrent != 12)
                            {
                                currAnnualBenefitsWife = currBeneWife * (12m - monthsWifeCurrent);
                                BoolWifeAgePDF = true;
                                Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12m - monthsWifeCurrent);
                                Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16((monthsWifeCurrent));
                                Globals.Instance.WifeNumberofYearsAbove66 = yearsWifeCurrent;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                            }
                        }
                        else
                        {
                            //For starting benefit at actual age of husband
                            if (ageHusb == intAgeHusb && intAgeHusb >= 62 && (ageHusb - 1) == yearsHusbCurrent && monthsHusbCurrent != 0 && monthsHusbCurrent != 12)
                            {
                                currAnnualBenefitsHusb = currBeneHusb * (12 + (12m - monthsHusbCurrent));
                                BoolHusbandAgePDF = true;
                                Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12 + (12m - monthsHusbCurrent));
                                Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16((monthsHusbCurrent));
                                Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbCurrent;
                                Globals.Instance.BoolHusbandAgeAdjusted = true;
                            }
                            else if (ageHusb == intAgeHusb && intAgeHusb >= 62 && ageHusb == yearsHusbCurrent && monthsHusbCurrent != 0 && monthsHusbCurrent != 12)
                            {
                                currAnnualBenefitsHusb = currBeneHusb * (12m - monthsHusbCurrent);
                                BoolHusbandAgePDF = true;
                                Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12m - monthsHusbCurrent);
                                Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16((monthsHusbCurrent));
                                Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbCurrent;
                                Globals.Instance.BoolHusbandAgeAdjusted = true;
                            }
                        }

                    }
                    #endregion Optional Strategy Deduction


                    if (currAnnualBenefitsWife == 0)
                    {
                        currAnnualBenefitsWife = currBeneWife * 12;
                    }
                    if (currAnnualBenefitsHusb == 0)
                    {
                        currAnnualBenefitsHusb = currBeneHusb * 12;
                    }
                    #region Unused
                    //if ((intCurrentAgeWifeMonth >= 1) && (intCurrentAgeWifeMonth != 12) && (intCurrentAgeWifeYear == wifeFirstPayAge) && (ageWife < 70) && (currBeneWife != 0))
                    //{
                    //    if (intAgeWifeMonth >= 6)
                    //    {
                    //        currAnnualBenefitsWife += currBeneWife * (12 - intCurrentAgeWifeMonth);
                    //        Globals.Instance.WifeNumberofMonthinNote = (12 - intCurrentAgeWifeMonth);
                    //    }
                    //    else
                    //    {
                    //        currAnnualBenefitsWife -= currBeneWife * intCurrentAgeWifeMonth;
                    //        Globals.Instance.WifeNumberofMonthinNote = intCurrentAgeWifeMonth;
                    //    }
                    //    BoolWifeAgePDF = true;
                    //    Globals.Instance.BoolWifeAgeAdjusted = true;
                    //}
                    //if ((intCurrentAgeHusbMonth >= 1) && (intCurrentAgeWifeMonth != 12) && (intCurrentAgeHusbYear == husbFirstPayAge) && (ageHusb < 70) && (currBeneHusb != 0))
                    //{
                    //    if (intAgeHusbMonth >= 6)
                    //    {
                    //        currAnnualBenefitsHusb += currBeneHusb * (12 - intCurrentAgeHusbMonth);
                    //        Globals.Instance.HusbandNumberofMonthsinNote = (12 - intCurrentAgeHusbMonth);
                    //    }
                    //    else
                    //    {
                    //        currAnnualBenefitsHusb -= currBeneHusb * intCurrentAgeHusbMonth;
                    //        Globals.Instance.HusbandNumberofMonthsinNote = intCurrentAgeHusbMonth;
                    //    }
                    //    BoolHusbandAgePDF = true;
                    //    Globals.Instance.BoolHusbandAgeAdjusted = true;
                    //}
                    #endregion Unused

                    #region Restricted Application Strategy Income Number
                    //Restricted Application Strategy Income
                    if (currBeneHusb != 0 && ageHusb < 70)
                    {
                        Globals.Instance.intRestrictAppIncomeFullHusb += (int)currAnnualBenefitsHusb;
                        Globals.Instance.intRestrictAppIncomeMaxHusb += (int)currAnnualBenefitsHusb;
                    }
                    if (currBeneWife != 0 && ageWife < 70)
                    {
                        Globals.Instance.intRestrictAppIncomeFullWife += (int)currAnnualBenefitsWife;
                        Globals.Instance.intRestrictAppIncomeMaxWife += (int)currAnnualBenefitsWife;
                    }
                    #endregion Restricted Application Strategy Income Number

                    if (currAnnualBenefitsWife == 0 && currAnnualBenefitsHusb == 0)
                    {
                        currBeneAnnual = currBene * 12;
                    }
                    else
                    {
                        currBeneAnnual = currAnnualBenefitsWife + currAnnualBenefitsHusb;
                        if (ageWife == 69)
                            Globals.Instance.WifeOriginalAnnualIncome = currAnnualBenefitsWife;
                        if (ageHusb == 69)
                            Globals.Instance.HusbandOriginalAnnualIncome = currAnnualBenefitsHusb;
                        currAnnualBenefitsWife = 0;
                        currAnnualBenefitsHusb = 0;
                    }

                    #endregion Code to calculate the annual benefits according to DOB

                    #region Code to calculate the cummulative benefits before and after younger age 70
                    if (youngest == 70)
                    {
                        currBeneWifeLocal = currBeneWife;
                        currBeneHusbLocal = currBeneHusb;
                    }
                    if (youngest >= 75 && youngest <= 90)
                    {
                        if (youngest == 75)
                        {
                            decimal grandTotal = 0;
                            for (int i = 0; i < 5; i++)
                            {
                                currBeneWifeLocal = Convert.ToInt32(ColaUp(63, currBeneWifeLocal));
                                currBeneHusbLocal = Convert.ToInt32(ColaUp(63, currBeneHusbLocal));
                                decimal total = ((currBeneWifeLocal + currBeneHusbLocal) * 12);
                                grandTotal += Convert.ToInt32(total);
                            }
                            cummBene += grandTotal;
                        }
                        else
                        {
                            decimal grandTotal = 0;
                            for (int i = 0; i < 5; i++)
                            {
                                currBeneWifeLocal = Convert.ToInt32(ColaUp(63, currBeneWifeLocal));
                                currBeneHusbLocal = Convert.ToInt32(ColaUp(63, currBeneHusbLocal));
                                decimal total = ((currBeneWifeLocal + currBeneHusbLocal) * 12);
                                grandTotal += Convert.ToInt32(total);
                            }
                            cummBene += grandTotal;
                        }
                    }
                    if (youngest <= 71) //Previously (youngest <= 70), changed on Nov 09 2016
                    {
                        cummBene += currBeneAnnual;
                    }
                    #endregion Code to calculate the cummulative benefits before and after younger age 70
                    cummBeneWife += currBeneWife;
                    cummBeneHusb += currBeneHusb;
                    #endregion

                    #region SURVIVOR BENEFIT CALCULATION

                    //  Survivor's benefit
                    decimal survBeneHusb = 0, survBeneWife = 0, survBene = 0;
                    survBeneHusb = currBeneHusb;
                    survBeneWife = currBeneWife;
                    if (survBeneHusb > survBeneWife)
                        survBene = survBeneHusb;
                    else
                        survBene = survBeneWife;

                    if (ageHusb >= 70 && ageWife >= 70 && age70SurvBeneAge == string.Empty)
                    {
                        if (ageWife == ageHusb)
                        {
                            age70SurvBeneAge = ageHusb.ToString();
                        }
                        else
                        {
                            age70SurvBeneAge = ageWife.ToString() + Constants.BackSlash + ageHusb.ToString();
                        }
                        age70SurvBene = survBene * 12;
                    }



                    string ageCol = string.Empty;
                    //if (ageHusb == ageWife)
                    //    ageCol = aIx.ToString();
                    //else
                    ageCol = ageWife.ToString() + Constants.BackSlash + ageHusb.ToString();
                    breakEvenAmt[highBE] = (int)StdRound(cummBene);
                    breakEvenAge[highBE] = ageCol;
                    highBE += 1;

                    #endregion

                    #region POPULATE THE GRID ROWS WITH VALUES

                    //  For selected ages, build the display row
                    if (youngest <= 90)//(aIx <= topAix) || (aIx == topAix + 10) || didAutoAdjust || showThisLine)
                    {
                        System.Data.DataRow incomeRow = incomeTab.NewRow();
                        if (ageHusb == ageWife)
                            incomeRow[(int)disp_col.Age] = ageCol;
                        else
                            incomeRow[(int)disp_col.Age] = ageCol;
                        if (currBene > 0)
                        {
                            string regExp = "[^\\w]";
                            if (BoolHusbandAgePDF || BoolWifeAgePDF)
                            {
                                incomeRow[(int)disp_col.AnnInc] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + Constants.SingleHash;
                                BoolHusbandAgePDF = false;
                                BoolWifeAgePDF = false;
                            }
                            else if (BoolHusbDoubleAsterisk || BoolWifeDoubleAsterisk)
                            {
                                string strhashIncluded = string.Empty;
                                if (incomeRow[(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                    strhashIncluded = Constants.SingleHash;
                                incomeRow[(int)disp_col.AnnInc] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.DoubleAsterisk;
                                int intPrevBenefit = 0, intPrevAnnualIcome = 0;
                                string strBenefit = string.Empty;
                                if (BoolHusbDoubleAsterisk)
                                {
                                    if (intHusbandDifference > 0)
                                        intHusbandDifference = incomeTab.Rows.Count - intHusbandDifference;
                                    else
                                        intHusbandDifference = incomeTab.Rows.Count - 1;
                                    strBenefit = incomeTab.Rows[intHusbandDifference][(int)disp_col.HusbInc].ToString().TrimStart('$');
                                    intPrevBenefit = Convert.ToInt32((Regex.Replace(strBenefit, regExp, "")));
                                    intPrevAnnualIcome = intPrevBenefit * multiplyHusb70;
                                    if (incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                        strhashIncluded = Constants.SingleHash;
                                    if (intPrevAnnualIcome != 0)
                                    {
                                        incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc] = (Globals.Instance.WifeOriginalAnnualIncome + intPrevAnnualIcome).ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.SingleAsterisk;
                                        Globals.Instance.BoolWHBSwitchedHusb = true;
                                    }
                                    Int32 intCummAnnual = Int32.Parse(Regex.Replace(incomeTab.Rows[intHusbandDifference - 1][(int)disp_col.CummInc].ToString(), regExp, ""));
                                    incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc] = (intCummAnnual + int.Parse(Regex.Replace(incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc].ToString(), regExp, "")));
                                    cummBene = currBeneAnnual + decimal.Parse(incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc].ToString());
                                    incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc] = decimal.Parse(incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc].ToString()).ToString(Constants.AMT_FORMAT);
                                    Globals.Instance.HusbNumberofMonthsAboveGrid = multiplyHusb70;
                                    Globals.Instance.HusbNumberofMonthsBelowGrid = multiplyHusb70;
                                    BoolHusbDoubleAsterisk = false;
                                }
                                else
                                {
                                    if (intWifeDifference > 0)
                                        intWifeDifference = incomeTab.Rows.Count - intWifeDifference;
                                    else
                                        intWifeDifference = incomeTab.Rows.Count - 1;
                                    strBenefit = incomeTab.Rows[intWifeDifference][(int)disp_col.WifeInc].ToString().TrimStart('$');
                                    intPrevBenefit = Convert.ToInt32((Regex.Replace(strBenefit, regExp, "")));
                                    intPrevAnnualIcome = intPrevBenefit * multiplyWife70;
                                    if (incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                        strhashIncluded = Constants.SingleHash;
                                    if (intPrevAnnualIcome != 0)
                                    {
                                        incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc] = (Globals.Instance.HusbandOriginalAnnualIncome + intPrevAnnualIcome).ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.SingleAsterisk;
                                        Globals.Instance.BoolWHBSwitchedWife = true;
                                    }
                                    Int32 intCummAnnual = Int32.Parse(Regex.Replace(incomeTab.Rows[intWifeDifference - 1][(int)disp_col.CummInc].ToString(), regExp, ""));
                                    incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc] = (intCummAnnual + int.Parse(Regex.Replace(incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc].ToString(), regExp, "")));
                                    cummBene = currBeneAnnual + decimal.Parse(incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc].ToString());
                                    incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc] = decimal.Parse(incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc].ToString()).ToString(Constants.AMT_FORMAT);
                                    Globals.Instance.WifeNumberofMonthsAboveGrid = multiplyWife70;
                                    Globals.Instance.WifeNumberofMonthsBelowGrid = multiplyWife70;

                                    BoolWifeDoubleAsterisk = false;
                                }
                            }
                            else if (BoolWifeSingleAsterisk || BoolHusbSingleAsterisk)
                            {
                                string strhashIncluded = string.Empty;
                                if (incomeRow[(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                    strhashIncluded = Constants.SingleHash;
                                incomeRow[(int)disp_col.AnnInc] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.DoubleAsterisk;
                                int intPrevBenefit = 0, intPrevAnnualIcome = 0;
                                string strBenefit = string.Empty;
                                if (BoolHusbSingleAsterisk)
                                {
                                    if (intHusbandDifference > 0)
                                        intHusbandDifference = incomeTab.Rows.Count - intHusbandDifference;
                                    else
                                        intHusbandDifference = incomeTab.Rows.Count - 1;
                                    strBenefit = incomeTab.Rows[intHusbandDifference][(int)disp_col.HusbInc].ToString().TrimStart('$');
                                    intPrevBenefit = Convert.ToInt32((Regex.Replace(strBenefit, regExp, "")));
                                    intPrevAnnualIcome = intPrevBenefit * (12 + multiplyHusb70);
                                    if (incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                        strhashIncluded = Constants.SingleHash;
                                    if (intPrevAnnualIcome != 0)
                                    {
                                        incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc] = (Globals.Instance.WifeOriginalAnnualIncome + intPrevAnnualIcome).ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.SingleAsterisk;
                                        Globals.Instance.BoolWHBSwitchedHusb = true;
                                    }
                                    Int32 intCummAnnual = Int32.Parse(Regex.Replace(incomeTab.Rows[intHusbandDifference - 1][(int)disp_col.CummInc].ToString(), regExp, ""));
                                    incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc] = (intCummAnnual + int.Parse(Regex.Replace(incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc].ToString(), regExp, "")));
                                    incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc] = decimal.Parse(incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc].ToString()).ToString(Constants.AMT_FORMAT);
                                    //
                                    cummBene = currBeneAnnual + decimal.Parse(Regex.Replace(incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc].ToString(), regExp, ""));
                                    Globals.Instance.HusbNumberofMonthsBelowGrid = (12 + multiplyHusb70);
                                    Globals.Instance.HusbNumberofMonthsAboveGrid = multiplyHusb70;
                                    BoolHusbSingleAsterisk = false;
                                }
                                else
                                {
                                    if (intWifeDifference > 0)
                                        intWifeDifference = incomeTab.Rows.Count - intWifeDifference;
                                    else
                                        intWifeDifference = incomeTab.Rows.Count - 1;
                                    strBenefit = incomeTab.Rows[intWifeDifference][(int)disp_col.WifeInc].ToString().TrimStart('$');
                                    intPrevBenefit = Convert.ToInt32((Regex.Replace(strBenefit, regExp, "")));
                                    intPrevAnnualIcome = intPrevBenefit * (12 + multiplyWife70);
                                    if (incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                        strhashIncluded = Constants.SingleHash;
                                    if (intPrevAnnualIcome != 0)
                                    {
                                        incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc] = (Globals.Instance.HusbandOriginalAnnualIncome + intPrevAnnualIcome).ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.SingleAsterisk;
                                        Globals.Instance.BoolWHBSwitchedWife = true;
                                    }
                                    Int32 intCummAnnual = Int32.Parse(Regex.Replace(incomeTab.Rows[intWifeDifference - 1][(int)disp_col.CummInc].ToString(), regExp, ""));
                                    incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc] = (intCummAnnual + int.Parse(Regex.Replace(incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc].ToString(), regExp, "")));
                                    incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc] = decimal.Parse(incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc].ToString()).ToString(Constants.AMT_FORMAT);
                                    //
                                    cummBene = currBeneAnnual + decimal.Parse(Regex.Replace(incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc].ToString(), regExp, ""));
                                    Globals.Instance.WifeNumberofMonthsAboveGrid = (multiplyWife70);
                                    Globals.Instance.WifeNumberofMonthsBelowGrid = (12 + multiplyWife70);
                                    BoolWifeSingleAsterisk = false;
                                }
                            }
                            else
                            {
                                incomeRow[(int)disp_col.AnnInc] = currBeneAnnual.ToString(Constants.AMT_FORMAT);
                            }
                            incomeRow[(int)disp_col.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);
                            incomeRow[(int)disp_col.HusbInc] = currBeneHusb.ToString(Constants.AMT_FORMAT);
                            incomeRow[(int)disp_col.CombInc] = currBene.ToString(Constants.AMT_FORMAT);
                            incomeRow[(int)disp_col.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);
                            incomeRow[(int)disp_col.SurvInc] = survBene.ToString(Constants.AMT_FORMAT);
                        }
                        else
                        {
                            incomeRow[(int)disp_col.WifeInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_col.HusbInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_col.CombInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_col.AnnInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_col.CummInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_col.SurvInc] = survBene.ToString(Constants.AMT_FORMAT);
                        }
                        incomeTab.Rows.Add(incomeRow);
                    }
                    #endregion

                    #region cummulative value when PIA is equal
                    //highlight cummulativ value when PIA is equal
                    if (decBeneHusb == decBeneWife)
                    {
                        if (strategy == calc_restrict_strategy.At70)
                        {
                            if (ageHusb == 70)
                            {
                                if (intAgeHusb > husbFRA)
                                {
                                    multiply = AgeAbove68YearsOfHusb();
                                    workingBenefitsat70Husb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                }
                                else
                                {
                                    workingBenefitsat70Husb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                }
                                workingBenefitsat70Husb = ColaUpSpousal(ageHusb, workingBenefitsat70Husb, birthYearHusb, intHusbAge, husbFRA);
                            }
                            if (ageWife == 70)
                            {
                                if (intAgeWife > wifeFRA)
                                {
                                    multiply = AgeAbove68YearsOfWife();
                                    workingBenefitsat70Wife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                }
                                else
                                {
                                    workingBenefitsat70Wife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                }
                                workingBenefitsat70Wife = ColaUpSpousal(ageWife, workingBenefitsat70Wife, birthYearWife, intWifeAge, wifeFRA);
                            }
                            if (workingBenefitsat70Wife != 0 && workingBenefitsat70Husb != 0)
                            {
                                if (workingBenefitsat70Wife > workingBenefitsat70Husb)
                                    Globals.Instance.BoolHighlightCummAtWife69Max = true;
                                else
                                    Globals.Instance.BoolHighlightCummAtWife69Max = false;
                            }
                        }
                    }
                    #endregion cummulative value when PIA is equal

                    #region CODE TO INCREASE COLA AND INCREASE THE AGE TO ITTERATE AND BIND THE DATA
                    if (youngest >= 71 && youngest <= 90)
                    {
                        currBeneWife = ColaUp(67, currBeneWife);
                        currBeneHusb = ColaUp(67, currBeneHusb);
                    }
                    else
                    {
                        currBeneWife = ColaUp(63, currBeneWife);
                        currBeneHusb = ColaUp(63, currBeneHusb);
                    }
                    // Date : 20112014
                    // Description : Add Extra Column "YEAR" in Grid.
                    //incYr++;
                    if (aIx >= 71)
                    {
                        aIx = aIx + 5;
                        year = year + 5;
                        youngest = youngest + 5;
                    }
                    else
                    {
                        year++;
                        aIx++;
                        youngest++;
                    }
                }
                //  -------------
                //  Finalize Grid
                //  -------------
                Grid1.DataSource = incomeSet;
                Grid1.DataBind();
                //Grid1.CssClass = "gridWidth";
                TableItemStyle tisA = Grid1.AlternatingRowStyle;
                int cummTotal = 0;
                //  ---------------------------------
                //  HIGHLIGHT cells to match the book
                //  ---------------------------------
                #endregion

                #region CODE TO DESIGN AND FORMAT THE GRID
                // Use high earner's FRA to trigger cell coloring
                int highEarnerFRA = 0;
                if (decBeneHusb > decBeneWife)
                    highEarnerFRA = husbFRA;
                else
                    highEarnerFRA = wifeFRA;
                string preFraYear = (highEarnerFRA - 1).ToString();
                string fraYear = highEarnerFRA.ToString();
                string fraYear3 = (69).ToString();
                string fraYear4 = (70).ToString();
                string fraYear14 = (80).ToString();
                bool boolToRunLoopOnlyOnce = true, boolToRunLoopOnlyOnceRestrict = true;
                bool boolToRunLoopOnlyOnce1 = true, boolToRunLoopOnlyOnceRestrict1 = true;
                string regExp1 = "[^\\w]";
                foreach (GridViewRow gvr in Grid1.Rows)
                {
                    // Date : 20112014
                    // Description : Add Extra Column "YEAR" in Grid.
                    //gvr.Cells[(int)disp_col.Year].BorderColor = LOW_GREY;

                    if (gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number70))
                    {
                        AgeForHusb70 = gvr.Cells[0].Text;
                        ValueForHusb70 = decimal.Parse(gvr.Cells[2].Text.Substring(1));
                    }
                    if (gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number70))
                    {
                        AgeForWife70 = gvr.Cells[0].Text;
                        ValueForWife70 = decimal.Parse(gvr.Cells[1].Text.Substring(1));
                    }
                    #region Code to get strating and age 69 and age 70 values for husb and wife
                    if (decBeneHusb < decBeneWife)
                    {
                        if (gvr.Cells[1].Text.Equals(Constants.ZeroWithAsterisk))
                        {
                            AgeWhenHigherEarnerClaimandSuspend = gvr.Cells[0].Text;
                        }
                        if (!((gvr.Cells[2].Text).Equals(Constants.ZeroWithOutAsterisk) || ((gvr.Cells[2].Text).Equals(Constants.ZeroWithAsterisk))))
                        {
                            if (boolToRunLoopOnlyOnce)
                            {
                                startingBenefitsValueHusb = decimal.Parse((gvr.Cells[2].Text.Substring(1)));
                                startingAgeValueHusb = gvr.Cells[0].Text;
                                boolToRunLoopOnlyOnce = false;
                            }
                        }
                        else
                        {
                            startingBenefitsValueHusb = 0;
                        }

                        if (!((gvr.Cells[1].Text).Equals(Constants.ZeroWithOutAsterisk) || ((gvr.Cells[1].Text).Equals(Constants.ZeroWithAsterisk))))
                        {
                            if (boolToRunLoopOnlyOnce1)
                            {
                                startingBenefitsValueWife = decimal.Parse((gvr.Cells[1].Text.Substring(1)));
                                startingAgeValueWife = gvr.Cells[0].Text;
                                boolToRunLoopOnlyOnce1 = false;
                            }
                        }
                        else
                        {
                            startingBenefitsValueWife = 0;
                        }
                        if ((gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number70)))
                        {
                            valueAtAge70 = decimal.Parse(Regex.Replace(gvr.Cells[4].Text, regExp1, ""));
                            AgeValueForHigherEarner70 = gvr.Cells[0].Text;
                            ValueForHigherEarner70 = decimal.Parse((gvr.Cells[1].Text.Substring(1)));
                            lastBenefitsChangedAge70Value = decimal.Parse((gvr.Cells[1].Text.Substring(1)));
                        }
                        if ((gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number69)))
                        {
                            valueAtAge69 = decimal.Parse((gvr.Cells[5].Text.Substring(1)));
                        }
                        if ((gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number70)) && Globals.Instance.Age70ChangeForHusb)
                        {
                            AgeValueForWifeAt70 = gvr.Cells[0].Text;
                            BenefitsValueForWifeAt70 = decimal.Parse((gvr.Cells[2].Text.Substring(1)));
                        }
                    }
                    else
                    {
                        if (gvr.Cells[2].Text.Equals(Constants.ZeroWithAsterisk))
                        {
                            AgeWhenHigherEarnerClaimandSuspend = gvr.Cells[0].Text;
                        }
                        if (!((gvr.Cells[2].Text).Equals(Constants.ZeroWithOutAsterisk) || ((gvr.Cells[2].Text).Equals(Constants.ZeroWithAsterisk))))
                        {
                            if (boolToRunLoopOnlyOnce)
                            {
                                startingBenefitsValueHusb = decimal.Parse((gvr.Cells[2].Text.Substring(1)));
                                startingAgeValueHusb = gvr.Cells[0].Text;
                                boolToRunLoopOnlyOnce = false;
                            }
                        }
                        else
                        {
                            startingBenefitsValueHusb = 0;
                        }

                        if (!((gvr.Cells[1].Text).Equals(Constants.ZeroWithOutAsterisk) || ((gvr.Cells[1].Text).Equals(Constants.ZeroWithAsterisk))))
                        {
                            if (boolToRunLoopOnlyOnce1)
                            {
                                startingBenefitsValueWife = decimal.Parse((gvr.Cells[1].Text.Substring(1)));
                                startingAgeValueWife = gvr.Cells[0].Text;
                                boolToRunLoopOnlyOnce1 = false;
                            }
                        }
                        else
                        {
                            startingBenefitsValueWife = 0;
                        }

                        if ((gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number69)))
                        {
                            valueAtAge69 = decimal.Parse((gvr.Cells[5].Text.Substring(1)));
                        }
                        if ((gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number70)))
                        {
                            valueAtAge70 = decimal.Parse(Regex.Replace(gvr.Cells[4].Text, regExp1, ""));
                            lastBenefitsChangedAge70Value = decimal.Parse((gvr.Cells[2].Text.Substring(1)));
                            AgeValueForHigherEarner70 = gvr.Cells[0].Text;
                            ValueForHigherEarner70 = decimal.Parse((gvr.Cells[2].Text.Substring(1)));
                        }
                        if ((gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number70)) && Globals.Instance.Age70ChangeForWife)
                        {
                            AgeValueForWifeAt70 = gvr.Cells[0].Text;
                            BenefitsValueForWifeAt70 = decimal.Parse((gvr.Cells[1].Text.Substring(1)));
                        }
                    }
                    #endregion Code to get strating and age 69 and age 70 values for husb and wife
                }

                #region Code Restrict
                if (Globals.Instance.BoolIsHusbandClaimedBenefit || Globals.Instance.BoolIsWifeClaimedBenefit)
                {
                    foreach (GridViewRow gridRow in Grid1.Rows)
                    {
                        if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                        {
                            //Age 70 value wife
                            if (gridRow.Cells[0].Text.Substring(0, 2).Equals(Constants.Number70))
                            {
                                lastBenefitsChangedAge70Value = decimal.Parse((gridRow.Cells[1].Text.Substring(1)));
                                valueAtAge70 = decimal.Parse(Regex.Replace(gridRow.Cells[4].Text, regExp1, ""));
                                AgeValueForHigherEarner70 = gridRow.Cells[0].Text;
                                ValueForHigherEarner70 = decimal.Parse((gridRow.Cells[1].Text.Substring(1)));
                                ValueForWife70 = decimal.Parse((gridRow.Cells[1].Text.Substring(1)));
                            }
                            //Code to get claimed user first benefit 
                            if (!((gridRow.Cells[2].Text).Equals(Constants.ZeroWithOutAsterisk) || ((gridRow.Cells[2].Text).Equals(Constants.ZeroWithAsterisk))))
                            {
                                if (boolToRunLoopOnlyOnceRestrict)
                                {
                                    startingBenefitsValueHusb = decimal.Parse((gridRow.Cells[2].Text.Substring(1)));
                                    startingAgeValueHusb = gridRow.Cells[0].Text;
                                    boolToRunLoopOnlyOnceRestrict = false;
                                }
                            }
                            if (!((gridRow.Cells[1].Text).Equals(Constants.ZeroWithOutAsterisk) || ((gridRow.Cells[1].Text).Equals(Constants.ZeroWithAsterisk))))
                            {
                                if (boolToRunLoopOnlyOnce1)
                                {
                                    startingBenefitsValueWife = decimal.Parse((gridRow.Cells[1].Text.Substring(1)));
                                    startingAgeValueWife = gridRow.Cells[0].Text;
                                    boolToRunLoopOnlyOnce1 = false;
                                }
                            }


                        }
                        else
                        {
                            //Age 70 value husband
                            if (gridRow.Cells[0].Text.Substring(3, 2).Equals(Constants.Number70))
                            {
                                lastBenefitsChangedAge70Value = decimal.Parse((gridRow.Cells[2].Text.Substring(1)));
                                valueAtAge70 = decimal.Parse(Regex.Replace(gridRow.Cells[4].Text, regExp1, ""));
                                AgeValueForHigherEarner70 = gridRow.Cells[0].Text;
                                ValueForHigherEarner70 = decimal.Parse((gridRow.Cells[2].Text.Substring(1)));
                                ValueForHusb70 = decimal.Parse((gridRow.Cells[2].Text.Substring(1)));
                            }
                            if (!((gridRow.Cells[1].Text).Equals(Constants.ZeroWithOutAsterisk) || ((gridRow.Cells[1].Text).Equals(Constants.ZeroWithAsterisk))))
                            {
                                if (boolToRunLoopOnlyOnceRestrict)
                                {
                                    startingBenefitsValueHusb = decimal.Parse((gridRow.Cells[1].Text.Substring(1)));
                                    startingAgeValueHusb = gridRow.Cells[0].Text;
                                    boolToRunLoopOnlyOnceRestrict = false;
                                }
                            }
                            if (!((gridRow.Cells[2].Text).Equals(Constants.ZeroWithOutAsterisk) || ((gridRow.Cells[2].Text).Equals(Constants.ZeroWithAsterisk))))
                            {
                                if (boolToRunLoopOnlyOnceRestrict1)
                                {
                                    startingBenefitsValueWife = decimal.Parse((gridRow.Cells[2].Text.Substring(1)));
                                    startingAgeValueWife = gridRow.Cells[0].Text;
                                    boolToRunLoopOnlyOnceRestrict1 = false;
                                }
                            }

                        }
                    }
                }
                #endregion Code Restrict
                return cummTotal;

                #endregion

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// Calculates Husband age at Wife's FRA age
        /// </summary>
        /// <param name="yearsHusbatWifeFra">Age of Husband in Years</param>
        /// <param name="monthsHusbAtWifeFRA">Age of Husband in Months</param>
        private void HusbAgeatWifeFRA(out int yearsHusbatWifeFra, out int monthsHusbAtWifeFRA)
        {
            try
            {
                int intTotalFRAMonths = FRAMonths(birthYearWife);
                DateTime datediffHusb = wifeBirthDate.AddMonths(intTotalFRAMonths);
                //DateTime datediffHusb = wifeBirthDate.AddYears(wifeFRA);
                TimeSpan difftimespanHusb = datediffHusb.Subtract(husbBirthDate);
                yearsHusbatWifeFra = Convert.ToInt32((double)difftimespanHusb.Days / 365.2425);
                int monthsHusb = Convert.ToInt32(((double)difftimespanHusb.Days / 30.436875));
                if (monthsHusb < yearsHusbatWifeFra * 12)
                    yearsHusbatWifeFra -= 1;
                monthsHusbAtWifeFRA = Math.Abs(monthsHusb - yearsHusbatWifeFra * 12);

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                yearsHusbatWifeFra = 0;
                monthsHusbAtWifeFRA = 0;
            }
        }

        /// <summary>
        /// Calculates Wife age at Husband's FRA age
        /// </summary>
        /// <param name="yearsWifeAtHusbFRA">Age of wife in Years</param>
        /// <param name="monthsWifeAtHusbFRA">Age of wife in Months</param>
        private void WifeAgeatHusbandFRA(out int yearsWifeAtHusbFRA, out int monthsWifeAtHusbFRA)
        {
            try
            {
                int intTotalFRAMonths = FRAMonths(birthYearHusb);
                DateTime datediff = husbBirthDate.AddMonths(intTotalFRAMonths);
                //DateTime datediff = husbBirthDate.AddYears(husbFRA);
                TimeSpan difftimespan = datediff.Subtract(wifeBirthDate);
                yearsWifeAtHusbFRA = Convert.ToInt32((double)difftimespan.Days / 365.2425);
                int months = Convert.ToInt32(((double)difftimespan.Days / 30.436875));
                if (months < yearsWifeAtHusbFRA * 12)
                    yearsWifeAtHusbFRA -= 1;
                monthsWifeAtHusbFRA = Math.Abs(months - yearsWifeAtHusbFRA * 12);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                yearsWifeAtHusbFRA = 0;
                monthsWifeAtHusbFRA = 0;
            }
        }


        /// <summary>
        /// Calculate Spousal Benefit for Spouse
        /// </summary>
        /// <param name="intCurrentBen">Current Benefit of Claiming person</param>
        /// <param name="intClaimedBen">Claimed benefit</param>
        /// <param name="intCurrentAge">Current Age of Claiming person</param>
        /// <param name="intChartAge">ageHusb/ageWife</param>
        /// <param name="decFRABenifit">FRA benefit of Claiming person</param>
        /// <param name="intSpouseFRAAge">Spouse's FRA age</param>
        /// <returns>Returns Spousal Benefit</returns>
        private decimal CalculateSpousalBenefitsForRestrictAppIncome(int intCurrentBen, int intClaimedBen, int intCurrentAge, int intChartAge, decimal decFRABenifit, int intSpouseFRAAge)
        {
            try
            {
                decimal decSpousalBen = 0;
                double dblAddColaPercent = 0, dblAmntAftAddPercent = 0;
                //Get how much percent of amount is added after claimed age to current age
                dblAddColaPercent = (((double)intCurrentBen - (double)intClaimedBen) / (double)intClaimedBen);
                //Get new amount number after multiplying the % amount to FRA benefit 
                dblAmntAftAddPercent = ((double)decFRABenifit * dblAddColaPercent) + (double)decFRABenifit;
                //Get spousal amount after adding specific years of cola
                if (intChartAge > intCurrentAge)
                    decSpousalBen = ColaUpSpousal(intChartAge, (decimal)dblAmntAftAddPercent, birthYearWife, intCurrentAge, intSpouseFRAAge);
                else
                    decSpousalBen = ColaUpSpousal(intCurrentAge, (decimal)dblAmntAftAddPercent, birthYearWife, intChartAge, intSpouseFRAAge);
                return decSpousalBen /= 2;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }

        }


        /// <summary>
        /// Used to calculate FRA benefit using age
        /// </summary>
        /// <param name="age"> Claimed Age </param>
        /// <param name="intBeneAmount"> </param>
        /// <returns></returns>
        private decimal CalculateRestrictFRABenFromClaimBeneAge(int age, int intBeneAmount)
        {
            try
            {
                int decFRAAmount = 0;
                switch (age)
                {
                    case 62:
                        decFRAAmount = Convert.ToInt32(intBeneAmount / 0.75);

                        break;
                    case 63:
                        decFRAAmount = Convert.ToInt32(intBeneAmount / 0.80);
                        break;
                    case 64:
                        decFRAAmount = Convert.ToInt32(intBeneAmount / 0.85);
                        break;
                    case 65:
                        decFRAAmount = Convert.ToInt32(intBeneAmount / 0.90);
                        break;
                    case 66:
                        decFRAAmount = Convert.ToInt32(intBeneAmount);
                        break;
                    case 67:
                        decFRAAmount = Convert.ToInt32(intBeneAmount / 1.08);
                        break;
                    case 68:
                        decFRAAmount = Convert.ToInt32(intBeneAmount / 1.16);
                        break;
                    case 69:
                        decFRAAmount = Convert.ToInt32(intBeneAmount / 1.24);
                        break;
                    case 70:
                        decFRAAmount = Convert.ToInt32(intBeneAmount / 1.32);
                        break;

                    default:
                        decFRAAmount = 0;
                        break;
                }
                return decFRAAmount;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// Calculates Restricted Current Benefit Using FRA Benefit
        /// </summary>
        /// <param name="FRABenefit"> FRA Benefit </param>
        /// <param name="CurrentAge"> Current Age </param>
        /// <param name="FRAAge"> FRA Age </param>
        /// <returns>Current Benefit</returns>
        private decimal CalculateRestrictCurrBenefitUsingFRABene(decimal FRABenefit, int CurrentAge, int FRAAge)
        {
            try
            {
                decimal benefit = 0;
                if (CurrentAge < FRAAge)
                    benefit = Convert.ToDecimal((double)FRABenefit / (Math.Pow(1.025, FRAAge - CurrentAge)));
                else
                    benefit = Convert.ToDecimal((double)FRABenefit * (Math.Pow(1.025, CurrentAge - FRAAge)));
                return benefit;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }


        /// <summary>
        /// code to get the spousal benefit for the younger spouse when the odler reaches the age 70
        /// </summary>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="currBeneWife"></param>
        /// <param name="currBeneHusb"></param>
        /// <param name="wifeBenefitSource"></param>
        /// <param name="husbBenefitSource"></param>
        private void GetSpousalBenefitForIntAge(int ageHusb, int ageWife, ref decimal currBeneWife, ref decimal currBeneHusb, ref benefit_source wifeBenefitSource, ref benefit_source husbBenefitSource, ref int husbFirstPayAge, ref int wifeFirstPayAge)
        {
            try
            {
                DateTime datediff = husbBirthDate.AddYears(70);
                TimeSpan difftimespan = datediff.Subtract(wifeBirthDate);
                int yearsWife1 = (int)Math.Floor((double)difftimespan.Days / 365.2425);
                int monthsWife = Convert.ToInt32(((double)difftimespan.Days / 30.436875));
                DateTime datediffHusb = wifeBirthDate.AddYears(70);
                TimeSpan difftimespanHusb = datediffHusb.Subtract(husbBirthDate);
                int yearsHusb1 = (int)Math.Floor((double)difftimespanHusb.Days / 365.2425);
                int monthsHusb = Convert.ToInt32(((double)difftimespanHusb.Days / 30.436875));
                bool wifeshow = false;
                bool Husbshow = false;
                intMonthsWife = Math.Abs(monthsWife - yearsWife1 * 12);
                intMonthsHusb = Math.Abs(monthsHusb - yearsHusb1 * 12);
                intHusbandYearsMax = yearsHusb1;
                intWifeYearsMax = yearsWife1;
                if (yearsHusb1 < 70)
                {
                    Husbshow = true;
                }
                else
                {
                    if (yearsWife1 < 70)
                        wifeshow = true;
                }
                if (Husbshow)
                {
                    int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                    decimal temBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneWife, birthYearWife, intWifeAge, wifeFRA);
                    temBenefits = temBenefits / 2;
                    decimal WHBHusb = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                    WHBHusb = ColaUpSpousal(70, WHBHusb, birthYearHusb, intAgeHusb, husbFRA);
                    decimal spousalBenefits = 0;
                    if (intAgeWife > wifeFRA)
                    {
                        multiply = AgeAbove68YearsOfWife();
                        spousalBenefits = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                    }
                    else
                    {
                        spousalBenefits = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                    }
                    spousalBenefits = spousalBenefits / 2;
                    if (ageWife == ageHusb)
                    {
                        if (currBeneHusb == 0 && yearsHusb1 <= ageHusb && ageHusb >= husbFRA && husbBirthDate < limit1 && (temBenefits < WHBHusb))
                        {
                            currBeneHusb = spousalBenefits;
                            husbBenefitSource = benefit_source.Spouse;
                            isHusbSpouse = true;
                            husbFirstPayAge = ageHusb;
                        }
                    }
                    else
                    {
                        if (currBeneHusb == 0 && yearsHusb1 <= ageHusb && ageHusb >= husbFRA && ageHusb < 70 && ageWife >= 70 && husbBirthDate < limit1 && (temBenefits < WHBHusb))
                        {
                            currBeneHusb = spousalBenefits;
                            husbBenefitSource = benefit_source.Spouse;
                            isHusbSpouse = true;
                            husbFirstPayAge = ageHusb;
                        }
                    }
                }
                else if (wifeshow)
                {
                    decimal WHBWife = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                    WHBWife = ColaUpSpousal(70, WHBWife, birthYearWife, intAgeWife, wifeFRA);
                    int intAgeAtSpouse70 = GetAgeofHusbWhenWifeAge70();
                    decimal spousalBenefits = ColaUpSpousal(intAgeAtSpouse70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                    spousalBenefits = spousalBenefits / 2;
                    decimal temBenefits = 0;
                    if (intAgeHusb > husbFRA)
                    {
                        multiply = AgeAbove68YearsOfHusb();
                        temBenefits = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                    }
                    else
                    {
                        temBenefits = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                    }
                    if (ageWife == ageHusb)
                    {
                        if (currBeneWife == 0 && yearsWife1 <= ageWife && ageWife >= wifeFRA && wifeBirthDate < limit1 && (spousalBenefits < WHBWife))
                        {
                            currBeneWife = temBenefits / 2;
                            wifeBenefitSource = benefit_source.Spouse;
                            isWifeSpouse = true;
                            wifeFirstPayAge = ageWife;
                        }
                    }
                    else
                    {
                        if (currBeneWife == 0 && yearsWife1 <= ageWife && ageHusb >= 70 && ageWife < 70 && ageWife >= wifeFRA & wifeBirthDate < limit1 && (spousalBenefits < WHBWife))
                        {
                            currBeneWife = temBenefits / 2;
                            wifeBenefitSource = benefit_source.Spouse;
                            isWifeSpouse = true;
                            wifeFirstPayAge = ageWife;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }




        /// <summary>
        /// to calculate husband age when wife is age 62
        /// </summary>
        /// <param name="yearsHusb"></param>
        /// <param name="remainingMonthsHusb"></param>
        public void HusbandAgeWhenSpouseAge62(out int yearsHusb, out int remainingMonthsHusb)
        {
            try
            {
                DateTime datediffHusb = wifeBirthDate.AddYears(62);
                TimeSpan difftimespanHusb = datediffHusb.Subtract(husbBirthDate);
                yearsHusb = Convert.ToInt32((double)difftimespanHusb.Days / 365.2425);
                int monthsHusb = Convert.ToInt32(((double)difftimespanHusb.Days / 30.436875));
                remainingMonthsHusb = Math.Abs(monthsHusb - yearsHusb * 12);
                if (monthsHusb < yearsHusb * 12)
                    yearsHusb -= 1;

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                yearsHusb = 0;
                remainingMonthsHusb = 0;
            }
        }

        /// <summary>
        /// to calculate wife age when husband is age 62
        /// </summary>
        /// <param name="years"></param>
        /// <param name="remainingMonths"></param>
        private void WifeAgeWhenSpouseAge62(out int years, out int remainingMonths)
        {
            try
            {
                DateTime datediff = husbBirthDate.AddYears(62);
                TimeSpan difftimespan = datediff.Subtract(wifeBirthDate);
                years = Convert.ToInt32((double)difftimespan.Days / 365.2425);
                int months = Convert.ToInt32(((double)difftimespan.Days / 30.436875));
                remainingMonths = Math.Abs(months - years * 12);
                if (months < years * 12)
                    years -= 1;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                years = 0;
                remainingMonths = 0;
            }
        }

        /// <summary>
        /// Calculates age in month and year
        /// </summary>
        /// <param name="DOB">User DOB</param>
        /// <param name="agemonth">Returns months</param>
        /// <param name="ageyear"></param>
        /// <returns></returns>
        private string SpouseAgeInMonth_Year(DateTime DOB, out int agemonth, out int ageyear)
        {
            try
            {
                DateTime today = DateTime.Today;

                agemonth = today.Month - DOB.Month;
                ageyear = today.Year - DOB.Year;

                if (today.Day < DOB.Day)
                    agemonth--;

                if (agemonth < 0)
                {
                    ageyear--;
                    agemonth += 12;
                }

                int days = (today - DOB.AddMonths((ageyear * 12) + agemonth)).Days;

                return string.Format("{0} year & {1} months",
                                     ageyear,
                                     agemonth);


            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                agemonth = 0;
                ageyear = 0;
                return string.Empty;
            }

        }

        /// <summary>
        /// code to get the spousal age when one spouse reaches to the age 70
        /// </summary>
        /// <returns></returns>
        private int GetAgeofHusbWhenWifeAge70()
        {
            try
            {
                int intAgeAtSpouse70 = 0;
                int intAgeSpan = intAgeHusb - intAgeWife;
                intAgeAtSpouse70 = 70 + intAgeSpan;
                return intAgeAtSpouse70;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        ///  function to get the value at FRA age when wife age is greater than his FRA age
        /// </summary>
        /// <returns></returns>
        private decimal AgeAbove68YearsOfWife()
        {
            try
            {
                decimal multiply, tempbenefit;
                multiply = 8 * (intAgeWife - wifeFRA);
                multiply = multiply / 100;
                multiply = 1 + multiply;
                tempbenefit = decBeneWife / multiply;
                return tempbenefit;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// function to get the value at FRA age when husband age is greater than his FRA age
        /// </summary>
        /// <returns></returns>
        private decimal AgeAbove68YearsOfHusb()
        {
            try
            {
                decimal multiply, tempbenefit;
                multiply = 8 * (intAgeHusb - husbFRA);
                multiply = multiply / 100;
                multiply = 1 + multiply;
                tempbenefit = decBeneHusb / multiply;
                return tempbenefit;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// To Calculate Benefits as per Client New given percentage for Married Scenario 
        /// (labour charger or late charges)
        /// </summary>
        /// <param name="age"></param>
        /// <param name="amt"></param>
        /// <param name="birthYear"></param>
        /// <returns></returns>
        protected decimal startFactorMarridSuspend(int age, decimal amt, int birthYear)
        {
            try
            {
                decimal result = 0;
                float MultiplyPercentage;
                float fFRAyr = FRA(birthYear);
                int iRndYr = Convert.ToInt32(fFRAyr);
                int fRndMnt = Convert.ToInt32((fFRAyr - iRndYr) * 12);
                if (birthYear <= 1942)
                    MultiplyPercentage = 0.00625f;
                else
                    MultiplyPercentage = 0.00667f;
                result = Convert.ToInt32(Convert.ToDouble(amt) * (1 + (MultiplyPercentage * (age * 12 - (iRndYr * 12 + fRndMnt)))));
                return result;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        ///  Determine the adjustment to the starting amount based on age
        /// </summary>
        /// <param name="age"></param>
        /// <param name="amt"></param>
        /// <param name="birthYear"></param>
        /// <returns></returns>
        protected decimal startFactor(int age, decimal amt, int birthYear)
        {
            #region Set factors for husband according to SSA rules
            try
            {
                //  Set factors according to SSA rules
                switch (age)
                {
                    case 62:
                        switch (birthYear)
                        {
                            case 1955: result = amt - (amt * .2583m); break;
                            case 1956: result = amt - (amt * .2667m); break;
                            case 1957: result = amt - (amt * .2750m); break;
                            case 1958: result = amt - (amt * .2833m); break;
                            case 1959: result = amt - (amt * .2917m); break;
                            default:
                                if (birthYear < 1955) result = amt - (amt * .2500m);
                                if (birthYear > 1959) result = amt - (amt * .3000m);
                                break;
                        }
                        break;
                    case 63:
                        switch (birthYear)
                        {
                            case 1955: result = amt - (amt * .2083m); break;
                            case 1956: result = amt - (amt * .2167m); break;
                            case 1957: result = amt - (amt * .2250m); break;
                            case 1958: result = amt - (amt * .2333m); break;
                            case 1959: result = amt - (amt * .2417m); break;
                            default:
                                if (birthYear < 1955) result = amt - (amt * .2000m);       // 12/15
                                if (birthYear > 1959) result = amt - (amt * .2500m);
                                break;
                        }
                        break;
                    case 64:
                        switch (birthYear)
                        {
                            case 1955: result = amt - (amt * .1444m); break;
                            case 1956: result = amt - (amt * .1556m); break;
                            case 1957: result = amt - (amt * .1667m); break;
                            case 1958: result = amt - (amt * .1778m); break;
                            case 1959: result = amt - (amt * .1889m); break;
                            default:
                                if (birthYear < 1955) result = amt - (amt * 2 / 15);   // .866666m
                                if (birthYear > 1959) result = amt - (amt * .2000m);
                                break;
                        }
                        break;
                    case 65:
                        switch (birthYear)
                        {
                            case 1955: result = amt - (amt * .0778m); break;
                            case 1956: result = amt - (amt * .0889m); break;
                            case 1957: result = amt - (amt * .1000m); break;
                            case 1958: result = amt - (amt * .1111m); break;
                            case 1959: result = amt - (amt * .1222m); break;
                            default:
                                if (birthYear < 1955) result = amt - (amt * 1 / 15);   // .933333m;  
                                if (birthYear > 1959) result = amt - (amt * .1333m);
                                break;
                        }
                        break;
                    case 66:
                        result = amt;             // 15/15
                        switch (birthYear)
                        {
                            case 1955: result = amt - (amt * .0111m); break;
                            case 1956: result = amt - (amt * .0222m); break;
                            case 1957: result = amt - (amt * .0333m); break;
                            case 1958: result = amt - (amt * .0444m); break;
                            case 1959: result = amt - (amt * .0556m); break;
                            default:
                                if (birthYear < 1955) result = amt;
                                if (birthYear > 1959) result = amt - (amt * .0667m);
                                break;
                        }
                        break;
                    case 67:
                        if (birthYear < 1958)
                            result = amt * 1.08m;
                        else
                            result = amt * 1.00m;
                        break;
                    case 68:
                        if (birthYear < 1958)
                            result = amt * 1.16m;
                        else
                            result = amt * 1.08m;
                        break;
                    case 69:
                        if (birthYear < 1958)
                            result = amt * 1.24m;
                        else
                            result = amt * 1.16m;
                        break;
                    case 70:
                        if (birthYear < 1958)
                            result = amt * 1.32m;
                        else
                            result = amt * 1.24m;
                        break;
                    default:
                        result = amt;
                        break;
                }
                return result;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
            #endregion
        }

        /// <summary>
        /// code built to get the annual total as per the number of months in the fra year
        /// bult on 05/28/2015 as per the client changes 
        /// for only one FRA year rest totals remains the same
        /// </summary>
        /// <param name="birthYear"></param>
        /// <param name="currBene"></param>
        /// <returns></returns>
        protected decimal annualBenefitsAsPerBirthDate(int birthYear, decimal currBene)
        {
            try
            {
                decimal intCurrBene = Math.Ceiling(currBene);
                switch (birthYear)
                {
                    case 1938: result = intCurrBene * (12 - 2); break;
                    case 1939: result = intCurrBene * (12 - 4); break;
                    case 1940: result = intCurrBene * (12 - 6); break;
                    case 1941: result = intCurrBene * (12 - 8); break;
                    case 1942: result = intCurrBene * (12 - 10); break;
                    case 1955: result = intCurrBene * (12 - 2); break;
                    case 1956: result = intCurrBene * (12 - 4); break;
                    case 1957: result = intCurrBene * (12 - 6); break;
                    case 1958: result = intCurrBene * (12 - 8); break;
                    case 1959: result = intCurrBene * (12 - 10); break;
                    default:
                        result = intCurrBene * (12); break;
                }
                return result;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// code to get annual benefits for Widows because their FRA ages are different from others
        /// </summary>
        /// <param name="birthYear"></param>
        /// <param name="currBene"></param>
        /// <returns></returns>
        protected decimal annualBenefitsAsPerBirthDateWidow(int birthYear, decimal currBene)
        {
            try
            {
                decimal intCurrBene = Math.Ceiling(currBene);
                switch (birthYear)
                {
                    case 1940: result = intCurrBene * (12 - 2); break;
                    case 1941: result = intCurrBene * (12 - 4); break;
                    case 1942: result = intCurrBene * (12 - 6); break;
                    case 1943: result = intCurrBene * (12 - 8); break;
                    case 1944: result = intCurrBene * (12 - 10); break;
                    case 1957: result = intCurrBene * (12 - 2); break;
                    case 1958: result = intCurrBene * (12 - 4); break;
                    case 1959: result = intCurrBene * (12 - 6); break;
                    case 1960: result = intCurrBene * (12 - 8); break;
                    case 1961: result = intCurrBene * (12 - 10); break;
                    default:
                        result = intCurrBene * (12); break;
                }
                return result;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// code built to get cola adjustments if a person age greatre than 62yrs
        /// </summary>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="age"></param>
        /// <param name="BeneWife"></param>
        /// <returns></returns>
        protected decimal colaFullRetireAsAge(int age, int FRAage, decimal BeneWife)
        {
            try
            {
                decimal currBene = 0;
                if (age == FRAage)
                {
                    currBene = (BeneWife) * Convert.ToDecimal(Math.Pow(1.025, (0)));
                }
                else if (age > FRAage)
                {
                    currBene = (BeneWife) * Convert.ToDecimal(Math.Pow(1.025, (age - FRAage)));
                }
                else
                {
                    currBene = (BeneWife) * Convert.ToDecimal(Math.Pow(1.025, (FRAage - age)));
                }
                return currBene;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// Determine the spousal benefit based on age
        /// </summary>
        /// <param name="age"></param>
        /// <param name="spousalBene"></param>
        /// <param name="birthYear"></param>
        /// <returns></returns>
        protected decimal spouseFactor(int age, decimal spousalBene, int birthYear)
        {

            #region Set factors for spouse according to SSA rules

            try
            {
                //  Set factors according to SSA rules
                switch (age)
                {
                    case 62:
                        switch (birthYear)
                        {
                            case 1955: result = spousalBene * (1 - .3083m); break;
                            case 1956: result = spousalBene * (1 - .3167m); break;
                            case 1957: result = spousalBene * (1 - .3250m); break;
                            case 1958: result = spousalBene * (1 - .3333m); break;
                            case 1959: result = spousalBene * (1 - .3417m); break;
                            default:
                                if (birthYear < 1955) result = spousalBene * (1 - .3000m);
                                if (birthYear > 1959) result = spousalBene * (1 - .3500m);
                                break;
                        }
                        break;
                    case 63:
                        switch (birthYear)
                        {
                            case 1955: result = spousalBene * (1 - .2583m); break;
                            case 1956: result = spousalBene * (1 - .2667m); break;
                            case 1957: result = spousalBene * (1 - .2750m); break;
                            case 1958: result = spousalBene * (1 - .2833M); break;
                            case 1959: result = spousalBene * (1 - .2917m); break;
                            default:
                                if (birthYear < 1955) result = spousalBene * (1 - .2500m);
                                if (birthYear > 1959) result = spousalBene * (1 - .3000m);
                                break;
                        }
                        break;
                    case 64:
                        switch (birthYear)
                        {
                            case 1955: result = spousalBene * (1 - .1806m); break;
                            case 1956: result = spousalBene * (1 - .1944m); break;
                            case 1957: result = spousalBene * (1 - .2083m); break;
                            case 1958: result = spousalBene * (1 - .2222M); break;
                            case 1959: result = spousalBene * (1 - .2361m); break;
                            default:
                                if (birthYear < 1955) result = spousalBene * (1 - .1667m);
                                if (birthYear > 1959) result = spousalBene * (1 - .2500m);
                                break;
                        }
                        break;
                    case 65:
                        switch (birthYear)
                        {
                            case 1955: result = spousalBene * (1 - .0972m); break;
                            case 1956: result = spousalBene * (1 - .1111m); break;
                            case 1957: result = spousalBene * (1 - .1250m); break;
                            case 1958: result = spousalBene * (1 - .1389M); break;
                            case 1959: result = spousalBene * (1 - .1528m); break;
                            default:
                                if (birthYear < 1955) result = spousalBene * (1 - .0833m);
                                if (birthYear > 1959) result = spousalBene * (1 - .1667m);
                                break;
                        }
                        break;
                    case 66:
                        switch (birthYear)
                        {
                            case 1955: result = spousalBene * (1 - .0139m); break;
                            case 1956: result = spousalBene * (1 - .0278m); break;
                            case 1957: result = spousalBene * (1 - .0417m); break;
                            case 1958: result = spousalBene * (1 - .0556M); break;
                            case 1959: result = spousalBene * (1 - .0694m); break;
                            default:
                                if (birthYear < 1955) result = spousalBene * (1 - .0000m);
                                if (birthYear > 1959) result = spousalBene * (1 - .0833m);
                                break;
                        }
                        break;
                    default:
                        result = spousalBene * (1);//if we will multiply by 1.03
                        break;
                }
                return result;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
            #endregion
        }

        /// <summary>
        /// Cola up an amount to the specified age
        /// </summary>
        /// <param name="age"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        protected decimal ColaUp(int age, decimal amount)
        {
            try
            {
                decimal result = StdRound(amount);
                //if cola not selected the return the benefits as it is
                //removed from the code by default amt of 1.025 is applied now
                if (colaAmt == 1) return result;
                //increase the cola by the age diffrence from current age to age 62
                int loopAge = 62;
                while (loopAge < age)
                {
                    result = result * colaAmt;
                    loopAge += 1;
                    //result = StdRound(result);
                }
                //convert from decimal to integer
                result = Convert.ToInt32(result);
                return result;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        ///  Cola up an amount to the specified age for ColaUpSpousal
        /// </summary>
        /// <param name="age"></param>
        /// <param name="amount"></param>
        /// <param name="birthYear"></param>
        /// <returns></returns>
        protected decimal ColaUpSpousal(int age, decimal amount, int birthYear, int intAge, int intFra)
        {
            try
            {
                decimal result = StdRound(amount);
                decimal result1;
                //if cola not selected the return the benefits as it is
                //removed from the code by default amt of 1.025 is applied now
                if (colaAmt == 1) return result;
                //get FRA year as per date of birth
                float fFRAyr = FRA(birthYear);
                //get FRA month as per date of birth
                float iRndYr = Convert.ToInt32(fFRAyr);
                float fRndMnt = (fFRAyr - iRndYr);
                //if current age less than 62 than apply the cola for diffrence from age when starts benefits to age 62
                if (intAge <= 62)
                {
                    result1 = Convert.ToInt32(result * Convert.ToDecimal(Math.Pow(1.025, (age - 62))));
                    //result2 = Convert.ToInt32(result * Convert.ToDecimal(Math.Pow(1.025, (fRndMnt)))) - result;
                    //result3 = result2 + result1;
                }
                //if current age less than 62 than apply the cola for diffrence from age when starts benefits to current age
                else
                {
                    result1 = (int)(result * Convert.ToDecimal(Math.Pow(1.025, (age - intAge))));
                    //result2 = Convert.ToInt32(result * Convert.ToDecimal(Math.Pow(1.025, (fRndMnt)))) - result;
                    //result3 = result2 + result1;
                }
                return result1;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// Cola up an amount to the specified age for ColaUpSpousalCOLA when Cola is YES & take spousal benefits
        /// </summary>
        /// <param name="Difference"></param>
        /// <param name="amount"></param>
        /// <param name="birthYear"></param>
        /// <returns></returns>
        protected decimal ColaUpSpousalCOLA(int Difference, decimal amount, int birthYear)
        {
            try
            {
                decimal result = StdRound(amount);
                if (colaAmt == 1) return result;
                result = Convert.ToInt32(result * Convert.ToDecimal(Math.Pow(1.025, Difference)));
                return result;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// TO Calculate the FRA Year for user
        /// </summary>
        /// <param name="birthDate"></param>
        /// <returns></returns>
        protected int FRAYear(DateTime birthDate)
        {
            try
            {
                int FRAYear = 0;
                int birthYear = Convert.ToInt32(birthDate.Year);
                float fFRAyr = FRA(birthYear);
                int iRndYr = Convert.ToInt32(fFRAyr);
                int Month = Convert.ToInt32((fFRAyr - iRndYr) * 12);
                DateTime dttmpDate = birthDate.AddYears(iRndYr);
                dttmpDate = dttmpDate.AddMonths(Month);
                FRAYear = dttmpDate.Year;
                return FRAYear;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// Common rounding routine - use standard rounding instead of .Net default of banker's rounding
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected decimal StdRound(decimal value)
        {
            try
            {
                return (int)value;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// This routine builds any of the grids for non-couples
        /// </summary>
        /// <param name="Grid1"></param>
        /// <param name="strategy"></param>
        /// <param name="soloType"></param>
        /// <param name="startAge"></param>
        /// <param name="gridNote"></param>
        /// <returns></returns>
        public int BuildSingle(GridView Grid1, calc_strategy strategy, calc_solo soloType, calc_start startAge, ref System.Web.UI.WebControls.Label gridNote, String gridname)
        {
            try
            {
                #region FRA AND BASE AGE CALCUALTION
                badStrat = false;
                bool AsapAge62 = false;
                bool SuspendAge66 = true;
                bool boolDoubleAsterikCondition = true;
                decimal decPrvBenefits = 0;
                string regExp = "[^\\w]";
                AgeValueAt70 = 0;
                DCummValue = 0;
                WCummValue = 0;
                BoolAboveFRAageStep2 = true;
                int baseAge = 62;
                SpousalBenefitsDivorce = 0;
                DivorceSpousalBenefitChange = string.Empty;
                int intWifeTotalMonths = 0, intHusbTotalMonths = 0;
                Globals.Instance.IntShowRestrictIncomeForDivorce = 0;
                #region Calculate age of husband and wife in months
                intWifeTotalMonths = CalculateAgeinTotalMonth(wifeBirthDate);
                intHusbTotalMonths = CalculateAgeinTotalMonth(husbBirthDate);
                #endregion Calculate age of husband and wife in months

                Customers customer = new Customers();
                switch (startAge)
                {
                    case calc_start.asap62:
                        baseAge = 62;
                        break;
                    case calc_start.asap60:
                        baseAge = 60;
                        break;
                    case calc_start.fra6667:
                        baseAge = wifeFRA;
                        break;
                    case calc_start.max70:
                        baseAge = 70;
                        break;
                }
                if (soloType == calc_solo.Widowed)
                    wifeFRA = Convert.ToInt32(WidowFRA(birthYearWife));
                #endregion FRA AND BASE AGE CALCUALTION

                #region BIND DATA TO GRID AND GENERATE THE COLUMNS WITH COLUMN HEADERS
                disp_cols dispCol;
                gridNote.Text = string.Empty;
                string sHusbInitials = Your_Name.Substring(0, 1);
                // Build dataset
                System.Data.DataSet incomeSet = new System.Data.DataSet();
                System.Data.DataTable incomeTab = incomeSet.Tables.Add("incomeTab");
                // Initialize grid
                Grid1.Columns.Clear();
                Grid1.AutoGenerateColumns = false;
                // Build Display Columns and Dataset Columns
                var values = (disp_cols[])Enum.GetValues(typeof(disp_cols));
                //set the grid column headers
                for (int dcIx = 0; dcIx < values.Length; dcIx++)
                {
                    dispCol = (disp_cols)dcIx;
                    incomeTab.Columns.Add(dispCol.ToString());
                    BoundField bf = new BoundField();
                    bf.HtmlEncode = false;
                    TableItemStyle tis = Grid1.HeaderStyle;
                    tis.Wrap = true;
                    switch (dispCol)
                    {
                        // case disp_cols.Year:
                        //   bf.HeaderText = "Year";
                        // break;
                        case disp_cols.Age:
                            bf.HeaderText = Constants.HeaderA + Constants.HeaderBreak + Constants.Age + Constants.OpeningBracket + sHusbInitials + Constants.CloseingBracket;
                            break;
                        case disp_cols.WifeInc:
                            bf.HeaderText = Constants.HeaderB + Constants.HeaderBreak + Constants.MonthlyIncomeSingle;
                            break;
                        case disp_cols.AnnInc:
                            bf.HeaderText = Constants.HeaderC + Constants.HeaderBreak + Constants.AnnualIncome;
                            break;
                        case disp_cols.CummInc:
                            bf.HeaderText = Constants.HeaderD + Constants.HeaderBreak + Constants.CumulativeIncome;
                            break;
                    }
                    bf.DataField = dispCol.ToString();
                    bf.ReadOnly = true;
                    Grid1.Columns.Add(bf);
                }
                incomeTab.Columns.Add("Note");

                // Date : 20112014
                // Description : Add Extra Column "YEAR" in Grid.
                // int year, incYr = 0;
                int ageWife, ageHusb;
                int wifeFirstPayAge = 0;
                decimal currBene = 0, currBeneAnnual = 0, currAnnualBenefitsWife = 0, cummBene = 0, newWorkingBeneWife = 0, currBeneWife = 0, cummBeneWife = 0, newBeneWife = 0;
                //bool showThisLine;
                #endregion BIND DATA TO GRID AND GENERATE THE COLUMNS WITH COLUMN HEADERS

                #region To build data in the columns
                //benefit_source wifeBenefitSource = benefit_source.None;
                int youngest = Math.Min(intAgeHusb, intAgeWife);
                int oldest = Math.Max(intAgeHusb, intAgeWife);
                int topAix = 70;
                bool boolIsWorkHistBenTaken = false;
                benefit_source wifeBenefitSource = benefit_source.None;
                //  Support a start point for standard strategies 
                if (baseAge > 64)
                    topAix = Math.Max(topAix, baseAge);
                int BotAix;
                if (soloType == Calc.calc_solo.Widowed)
                {
                    BotAix = 60;
                }
                else
                {
                    BotAix = 62;
                }
                #endregion To build data in the columns

                #region code build all alternate strategies
                //  Build the data rows
                for (int aIx = BotAix; aIx <= 90;)//(int aIx = BotAix; aIx < 99; aIx++)
                {
                    // Date : 20112014
                    // Description : Add Extra Column "YEAR" in Grid.
                    // Set year
                    //year = birthYearWife + BotAix + incYr;
                    // Set age
                    ageWife = aIx;
                    ageHusb = aIx + intAgeHusb - intAgeWife;

                    #region Code for widow initial values for any age
                    //  Widowed ony: initial non-spousal payout amount 
                    //Code for widow initial for only two strategies Calc.calc_strategy.Widowed66 ,Calc.calc_strategy.Widowed70 values for any age
                    if ((currBeneWife == 0)     /* don't change if started */
                    && soloType == Calc.calc_solo.Widowed
                    && (strategy == Calc.calc_strategy.Widowed66 || strategy == Calc.calc_strategy.Widowed70)
                    && (ageWife >= intAgeWife)) /* watch out for current age > 60 */
                    {
                        currBeneWife = SurvivorBene(ageWife, decBeneHusb, birthYearWife, 66, 1958);
                        wifeBenefitSource = benefit_source.Spouse;
                        if (intAgeWife >= 62)
                            currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intAgeWife, wifeFRA);//ColaUp(ageWife, currBeneWife);
                        wifeFirstPayAge = ageWife;
                    }
                    #endregion Code for widow initial values for any age

                    #region Code for all strategies initial values at age 62
                    //  Set initial non-spousal payout amount 3
                    //for age always greater than 62 yrs
                    if ((currBeneWife == 0)     /* don't change if started */
                    && (ageWife >= baseAge)     /* don't start before begin point for this chart */
                    && (ageWife >= 62)     /* minimum SS age */
                     && (ageWife >= intAgeWife)) /* watch out for current age > 62 */
                    {
                        if (soloType == Calc.calc_solo.Single && strategy == Calc.calc_strategy.Spousal)
                        {
                            if (intAgeWife > wifeFRA)
                                currBeneWife = decBeneWife;
                            else
                                currBeneWife = startFactor(ageWife, decBeneWife, birthYearWife);
                            if (intAgeWife == wifeFRA)
                                Globals.Instance.BoolHideSpouseStrategySingle = true;
                            wifeBenefitSource = benefit_source.Self;
                        }
                        else
                        {
                            //for when only one strategy is shown we use reduction factor accroding to the first pay age
                            if (strategy == Calc.calc_strategy.WidowedSelf && ageWife < wifeFRA)
                            {
                                //reduction factors taken from internet
                                if (currBeneWife == 0)
                                    wifeFirstPayAge = ageWife;
                                currBeneWife = WorkingBenefitsReductionFacorsForSpousal(ageWife, decBeneWife);
                                Globals.Instance.strWidowStarting = Constants.Working;
                                wifeBenefitSource = benefit_source.Self;
                            }
                            if (strategy == Calc.calc_strategy.WidowedSelf && ageWife >= wifeFRA)
                            {
                                //reduction factors for surviour benefits based on the birth year
                                if (currBeneWife == 0)
                                    wifeFirstPayAge = ageWife;
                                currBeneWife = SurvivorBene(ageWife, decBeneHusb, birthYearWife, 66, 1958);
                                Globals.Instance.strWidowStarting = Constants.spousal;
                                wifeBenefitSource = benefit_source.Self;
                            }
                            decimal ownBenefits, spousalBenefits, spousalBenefitsCurrentAge, ownBenefitsatCurrentAge = 0;
                            spousalBenefitsCurrentAge = ColaUpSpousal(70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                            spousalBenefitsCurrentAge = spousalBenefitsCurrentAge / 2;
                            spousalBenefits = ColaUpSpousal(husbFRA, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                            spousalBenefits = spousalBenefits / 2;
                            ownBenefitsatCurrentAge = ColaUpSpousal(wifeFRA, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                            ownBenefits = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                            ownBenefits = ColaUpSpousal(70, ownBenefits, birthYearWife, intAgeWife, wifeFRA);

                            if (soloType == Calc.calc_solo.Divorced && HusbDataofBirth < limit1 && strategy.Equals(calc_strategy.Standard) && ageHusb < 62)
                            {
                                if (spousalBenefits > ownBenefitsatCurrentAge)
                                {
                                    currBeneWife = startFactor(ageWife, decBeneWife, birthYearWife);
                                    wifeBenefitSource = benefit_source.Self;
                                    AsapAge62 = true;
                                    Globals.Instance.strDivorceStarting = Constants.Working;
                                }
                            }
                            if ((soloType == Calc.calc_solo.Divorced) && strategy.Equals(calc_strategy.Standard) && (ageHusb < ageWife) && (ageHusb < 62) && (spousalBenefits > ownBenefitsatCurrentAge && spousalBenefitsCurrentAge > ownBenefits))
                            {
                                currBeneWife = startFactor(ageWife, decBeneWife, birthYearWife);
                                wifeBenefitSource = benefit_source.Self;
                                AsapAge62 = true;
                                Globals.Instance.strDivorceStarting = Constants.Working;
                            }
                            if ((soloType == Calc.calc_solo.Divorced) && strategy.Equals(calc_strategy.Standard) && HusbDataofBirth < limit1 && (ageHusb > 62) && (spousalBenefits > ownBenefitsatCurrentAge && spousalBenefitsCurrentAge < ownBenefits))
                                Globals.Instance.BoolHideSpouseStrategyDivorce = true;

                            //when the spousal benefit greater at fra but not at age 70 and age of husband is less than 62 years we show 3 strategies
                            if (soloType == Calc.calc_solo.Divorced && (strategy == Calc.calc_strategy.Standard) && (intAgeHusb <= 62) && (ageHusb <= ageWife) && (spousalBenefits > ownBenefitsatCurrentAge && spousalBenefits < spousalBenefitsCurrentAge))
                            {
                                if (intHusbTotalMonths < intWifeTotalMonths)
                                {
                                    currBeneWife = startFactor(ageWife, decBeneWife, birthYearWife);
                                    wifeBenefitSource = benefit_source.Self;
                                    AsapAge62 = true;
                                    Globals.Instance.strDivorceStarting = Constants.Working;
                                    Globals.Instance.BoolShowThirdDivorceStrategy = true;
                                }
                            }
                        }
                        currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intAgeWife, wifeFRA);
                        wifeFirstPayAge = ageWife;
                    }
                    #endregion Code for all strategies initial values at age 62

                    #region Code for divorce values at age 66

                    #region FRA age calculations for Suspend strategy
                    if (soloType == Calc.calc_solo.Divorced && strategy == Calc.calc_strategy.Suspend && ageWife == wifeFRA && !Globals.Instance.BoolShowThirdDivorceStrategy)
                    {
                        if (currBeneWife == 0)
                            Globals.Instance.strDivorceStarting = Constants.Working;
                        currBeneWife = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                        SuspendAge66 = false;
                        //DivorceAtAge66 = true;
                    }
                    #endregion FRA age calculations for Suspend strategy

                    #region FRA age calculations for Standard strategy
                    if (soloType == Calc.calc_solo.Divorced && strategy == Calc.calc_strategy.Standard && (wifeBenefitSource == benefit_source.None || wifeBenefitSource == benefit_source.Self))
                    {
                        decimal spousalBenefits = 0;
                        if (ageWife >= wifeFRA)
                        {
                            if (HusbDataofBirth < limit1 || AsapAge62)
                            {
                                if (ageHusb >= 62 && AsapAge62)
                                {
                                    spousalBenefits = SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                    if (currBeneWife < spousalBenefits)
                                    {
                                        if (currBeneWife == 0)
                                        {
                                            Globals.Instance.strDivorceStarting = Constants.spousal;
                                            Globals.Instance.strDivorceStartingAsap = Constants.spousal;
                                        }
                                        currBeneWife = spousalBenefits;
                                        DivorceSpousalBenefitChange = ageWife.ToString();
                                        SpousalBenefitsDivorce = ageWife;
                                        intSpousalBenefitAge = ageWife; //Added on 16 May to change in steps in pdf 
                                        wifeBenefitSource = benefit_source.SpousalBenefit;
                                    }
                                    else
                                    {
                                        if (currBeneWife == 0)
                                        {
                                            Globals.Instance.strDivorceStarting = Constants.Working;
                                            Globals.Instance.strDivorceStartingAsap = Constants.Working;
                                        }
                                        currBeneWife = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                        Globals.Instance.BoolHideAsapStrategyDivorce = true;
                                    }
                                }
                                else
                                {
                                    if (!AsapAge62 && ageWife >= intAgeWife)
                                    {
                                        if (currBeneWife == 0)
                                        {
                                            Globals.Instance.strDivorceStarting = Constants.Working;
                                            Globals.Instance.strDivorceStartingAsap = Constants.Working;
                                        }
                                        currBeneWife = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                        if (Globals.Instance.BoolHideSpouseStrategyDivorce)
                                            Globals.Instance.BoolHideAsapStrategyDivorce = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (AsapAge62 && ageHusb >= 62 && ageWife >= wifeFRA)
                            {
                                spousalBenefits = SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                if (currBeneWife < spousalBenefits)
                                {
                                    if (currBeneWife == 0)
                                    {
                                        Globals.Instance.strDivorceStarting = Constants.spousal;
                                        Globals.Instance.strDivorceStartingAsap = Constants.spousal;
                                    }
                                    currBeneWife = spousalBenefits;
                                    DivorceSpousalBenefitChange = ageWife.ToString();
                                    SpousalBenefitsDivorce = ageWife;
                                    Globals.Instance.BoolWorkingBenefitsShown = true;
                                    wifeBenefitSource = benefit_source.SpousalBenefit;
                                }
                            }
                        }
                    }
                    #endregion FRA age calculations for Standard strategy

                    #region FRA age calculations for Spousal strategy
                    //  Divorced Spousal age 66: use spousal to grow work history benefit
                    if (soloType == Calc.calc_solo.Divorced && strategy == Calc.calc_strategy.Spousal && ageWife >= wifeFRA && wifeBenefitSource == benefit_source.None)
                    {
                        decimal ownBenefits, spousalBenefits, spousalBenefitsCurrentAge, ownBenefitsatCurrentAge = 0;
                        //working benefits for wife at age 70
                        ownBenefits = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                        ownBenefits = ColaUpSpousal(70, ownBenefits, birthYearWife, intAgeWife, wifeFRA);
                        //working benefits for wife at current age
                        ownBenefitsatCurrentAge = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                        //code to get the spousal benefits at FRA age for Wife
                        spousalBenefits = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                        spousalBenefits = spousalBenefits / 2;
                        //code to get the spousal benefits at age 70 for Wife
                        spousalBenefitsCurrentAge = ColaUpSpousal(70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                        spousalBenefitsCurrentAge = spousalBenefitsCurrentAge / 2;
                        if (spousalBenefits > ownBenefitsatCurrentAge && spousalBenefitsCurrentAge > ownBenefits)
                            Globals.Instance.BoolSpousalGreaterAtFRAandAge70 = true;
                        if (HusbDataofBirth < limit1)
                        {
                            if (ageHusb >= 62)
                            {
                                if (currBeneWife < spousalBenefits)
                                {
                                    if (currBeneWife == 0)
                                    {
                                        Globals.Instance.strDivorceStartingSpouse = Constants.spousal;
                                        Globals.Instance.strDivorceStarting = Constants.spousal;
                                    }
                                    currBeneWife = spousalBenefits;
                                    DivorceSpousalBenefitChange = ageWife.ToString();
                                    wifeBenefitSource = benefit_source.SpousalBenefit;

                                }
                                if (spousalBenefits > ownBenefitsatCurrentAge)
                                    Globals.Instance.BoolSpGrternWrkgFRADivorce = false;
                            }
                            else
                            {
                                if (spousalBenefits > ownBenefitsatCurrentAge && spousalBenefitsCurrentAge > ownBenefits && ageWife > intAgeWife)
                                {
                                    if (currBeneWife == 0)
                                    {
                                        Globals.Instance.strDivorceStarting = Constants.Working;
                                        Globals.Instance.strDivorceStartingSpouse = Constants.Working;
                                    }
                                    currBeneWife = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                    wifeBenefitSource = benefit_source.Self;
                                    Globals.Instance.BoolWorkingBenefitsShown = true;
                                    boolIsWorkHistBenTaken = true;
                                }
                            }
                        }
                        else
                        {
                            if (strategy == Calc.calc_strategy.Spousal)
                            {
                                if (spousalBenefitsCurrentAge > ownBenefits && ageHusb >= 62)
                                {
                                    if (spousalBenefits > ownBenefitsatCurrentAge)
                                    {
                                        if (currBeneWife == 0)
                                        {
                                            Globals.Instance.strDivorceStartingSpouse = Constants.spousal;
                                            Globals.Instance.strDivorceStarting = Constants.spousal;
                                        }
                                        currBeneWife = spousalBenefits;
                                        Globals.Instance.BoolShowSecondDivorce = true;
                                        wifeBenefitSource = benefit_source.SpousalBenefit;

                                    }
                                }
                                else
                                {
                                    if (spousalBenefits < ownBenefitsatCurrentAge)
                                    {
                                        if (spousalBenefitsCurrentAge < ownBenefits)
                                        {
                                            if (currBeneWife == 0)
                                            {
                                                Globals.Instance.strDivorceStarting = Constants.Working;
                                                Globals.Instance.strDivorceStartingSpouse = Constants.Working;
                                            }
                                            currBeneWife = ownBenefitsatCurrentAge;
                                            Globals.Instance.BoolWorkingBenefitsShown = true;
                                            wifeBenefitSource = benefit_source.Self;
                                        }
                                    }
                                    else
                                    {
                                        if (spousalBenefits > ownBenefitsatCurrentAge && ageHusb >= 62)
                                        {
                                            if (spousalBenefitsCurrentAge < ownBenefits)
                                            {
                                                if (currBeneWife == 0)
                                                {
                                                    Globals.Instance.strDivorceStarting = Constants.spousal;
                                                    Globals.Instance.strDivorceStartingSpouse = Constants.spousal;
                                                }
                                                currBeneWife = spousalBenefits;
                                                wifeBenefitSource = benefit_source.SpousalBenefit;
                                                SpousalBenefitsDivorce = ageWife;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion FRA age calculations for Spousal strategy

                    #endregion Code for divorce values at age 66

                    #region Code for SSA adjustment for all strategies
                    //  SSA automatically adjusts when spousal benefit would be better
                    //bool didAutoAdjust = false;
                    //this would be for the suspend strategy, startegy 2 on the grid in Divorce case
                    if ((soloType == Calc.calc_solo.Divorced && ageHusb >= 62 && ageWife == wifeFRA && strategy == calc_strategy.Suspend))
                    {
                        decimal ssaAlternative = 0, earlyBene = 0, spousalBene = 0;
                        if (wifeBenefitSource == benefit_source.Self)
                        {
                            spousalBene = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA); // Change if COLA is YES 
                            spousalBene /= 2;
                            earlyBene = 0;
                            earlyBene = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA); //Convert.ToInt32(decBeneHusb * Convert.ToDecimal(Math.Pow(1.025, ageHusb - 62)));
                            ssaAlternative = spousalBene + currBeneWife - earlyBene;
                            if (StdRound(ssaAlternative) > StdRound(currBeneWife))
                            {
                                wifeBenefitSource = benefit_source.Spouse;
                                //gridNote.Text = "NOTE: Age " + ageWife.ToString() + " the spousal benefit exceeded the work history benefit of " + currBeneWife.ToString(Constants.AMT_FORMAT) + ".";
                                currBeneWife = ssaAlternative;
                                //didAutoAdjust = true;
                                //boolean value to show the second strategy(Standard,Grid ASAP) in Divorce
                                SpousalBenefitsDivorce = ageWife;
                            }
                        }
                    }
                    //this would be for the standard strategy, startegy 1 on the grid in Divorce case
                    if (soloType == Calc.calc_solo.Divorced && ageHusb >= 62 && strategy == calc_strategy.Spousal)
                    {
                        decimal ssaAlternative = 0, earlyBene = 0, spousalBene = 0;
                        if (wifeBenefitSource == benefit_source.Self)
                        {
                            spousalBene = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA); // Change if COLA is YES 
                            spousalBene /= 2;
                            earlyBene = 0;
                            earlyBene = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA); //Convert.ToInt32(decBeneHusb * Convert.ToDecimal(Math.Pow(1.025, ageHusb - 62)));
                            ssaAlternative = spousalBene + currBeneWife - earlyBene;
                            if (StdRound(ssaAlternative) > StdRound(currBeneWife))
                            {
                                wifeBenefitSource = benefit_source.Spouse;
                                //gridNote.Text = "NOTE: Age " + ageWife.ToString() + " the spousal benefit exceeded the work history benefit of " + currBeneWife.ToString(Constants.AMT_FORMAT) + ".";
                                currBeneWife = ssaAlternative;
                                //didAutoAdjust = true;
                                //boolean value to show the second strategy(Standard,Grid ASAP) in Divorce
                                SpousalBenefitsDivorce = ageWife;
                                DivorceSpousalBenefitChange = ageWife.ToString();
                            }
                        }
                    }
                    #endregion Code for SSA adjustment for all strategies

                    #region Code for single at FRA age for standard strategy
                    if (soloType == Calc.calc_solo.Single && strategy == Calc.calc_strategy.Standard && ageWife == wifeFRA)
                    {
                        if (intAgeWife > wifeFRA)
                            currBeneWife = decBeneWife;
                        else
                            currBeneWife = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                        wifeBenefitSource = benefit_source.Self;
                        wifeFirstPayAge = ageWife;
                    }
                    #endregion Code for single at FRA age for standard strategy

                    #region Code for single at 70 age for Suspend strategy
                    if (soloType == Calc.calc_solo.Single && strategy == Calc.calc_strategy.Suspend && ageWife == 70)
                    {
                        decimal ownBenefitsAt70;
                        if (intAgeWife > wifeFRA)
                            ownBenefitsAt70 = AgeAbove68YearsOfWife();
                        else
                            ownBenefitsAt70 = decBeneWife;
                        //calculating the working values for wife
                        ownBenefitsAt70 = startFactorMarridSuspend(ageWife, ownBenefitsAt70, birthYearWife);
                        currBeneWife = ColaUpSpousal(ageWife, ownBenefitsAt70, birthYearWife, intAgeWife, wifeFRA);
                        wifeBenefitSource = benefit_source.Self;
                        wifeFirstPayAge = ageWife;
                    }
                    #endregion Code for single at 70 age for Suspend strategy

                    #region Code for divorce values at age 70
                    ///*****************method to be changed to get the details
                    if ((soloType == Calc.calc_solo.Divorced && ageWife == 70 && wifeBenefitSource != benefit_source.Self) || (soloType == Calc.calc_solo.Divorced && ageWife == 70 && !boolIsWorkHistBenTaken)) // Previous it was (wifeBenefitSource != benefit_source.Self) changed on 27April2016
                    {
                        //  Divorced Spousal age 70: used spousal to grow work history benefit
                        //working benefits for wife at age 70
                        decimal ownBenefitsAt70, spousalBenefitsAt70, tempdecBenefit;
                        if (intAgeWife > wifeFRA)
                        {
                            tempdecBenefit = AgeAbove68YearsOfWife();
                        }
                        else
                        {
                            tempdecBenefit = decBeneWife;
                        }
                        //calculating the working values for wife
                        ownBenefitsAt70 = startFactorMarridSuspend(ageWife, tempdecBenefit, birthYearWife);
                        ownBenefitsAt70 = ColaUpSpousal(ageWife, ownBenefitsAt70, birthYearWife, intAgeWife, wifeFRA);
                        //calculating the spousal values for wife
                        spousalBenefitsAt70 = ColaUpSpousal(ageHusb, tempdecBenefit, birthYearHusb, intAgeHusb, husbFRA);
                        spousalBenefitsAt70 = spousalBenefitsAt70 / 2;

                        if (strategy == Calc.calc_strategy.Spousal && HusbDataofBirth < limit1)
                        {
                            //switch from working benefits to spousal benefits
                            if (spousalBenefitsAt70 < ownBenefitsAt70)
                            {
                                if (currBeneWife < ownBenefitsAt70)
                                {
                                    currBeneWife = ownBenefitsAt70;
                                    //variable to highlight age 69 value in grid and PDF
                                    dat70 = true;
                                    BenefitAt70Change = true;
                                    //DivorceChangeAtAge70 = true;
                                }
                            }
                        }
                        if (strategy == Calc.calc_strategy.Standard)
                        {
                            if (HusbDataofBirth > limit1)
                            {
                                //switch from working benefits to spousal benefits
                                if (ownBenefitsAt70 > spousalBenefitsAt70)
                                {
                                    if (currBeneWife < ownBenefitsAt70)
                                    {
                                        currBeneWife = ownBenefitsAt70;
                                    }
                                }
                            }
                        }
                        if (strategy == Calc.calc_strategy.Suspend && SuspendAge66)
                        {
                            if (ownBenefitsAt70 > spousalBenefitsAt70)
                            {
                                if (currBeneWife < ownBenefitsAt70)
                                {
                                    currBeneWife = ownBenefitsAt70;
                                }
                            }
                        }
                    }
                    #endregion Code for divorce values at age 70

                    #region Code for widow surviour values at age 66
                    if (soloType == Calc.calc_solo.Widowed &&
                    (strategy == Calc.calc_strategy.WidowedSelf || strategy == Calc.calc_strategy.Widowed66)
                    && wifeBenefitSource == benefit_source.Self
                    && ageWife >= wifeFRA)
                    {
                        if (strategy == Calc.calc_strategy.Widowed66)
                        {
                            if (currBeneWife == 0)
                            {
                                Globals.Instance.strWidowStarting = Constants.spousal;
                                wifeFirstPayAge = ageWife;
                            }
                            currBeneWife = ColaUpSpousal(ageWife, decBeneHusb, birthYearWife, intAgeWife, wifeFRA);//ColaUp(ageWife, newBeneWife);
                            wifeBenefitSource = benefit_source.Spouse;
                            BenefitAt66Change = true;
                            swat66 = true;
                        }
                        else
                        {
                            newBeneWife = ColaUpSpousal(ageWife, decBeneHusb, birthYearWife, intAgeWife, wifeFRA);//ColaUp(ageWife, newBeneWife);
                            wifeBenefitSource = benefit_source.Spouse;
                            swat66 = true;
                            if (newBeneWife < currBeneWife)
                            {
                                badStrat = true;
                            }
                            else
                            {
                                if (currBeneWife == 0)
                                {
                                    Globals.Instance.strWidowStarting = Constants.spousal;
                                    wifeFirstPayAge = ageWife;
                                }
                                currBeneWife = newBeneWife;
                                BenefitAt66Change = true;
                            }
                        }
                    }
                    #endregion Code for widow surviour values at age 66

                    #region Code for widow working values at age 66
                    if (soloType == Calc.calc_solo.Widowed
                    && strategy == Calc.calc_strategy.Widowed66
                    && wifeBenefitSource == benefit_source.Spouse
                    && ageWife >= wifeFRA)
                    {
                        newBeneWife = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);//decBeneWife * Convert.ToDecimal(Math.Pow(1.025, (ageWife - 62)));
                        wifeBenefitSource = benefit_source.Self;
                        wat66 = true;
                        if (newBeneWife < currBeneWife)
                        {
                            badStrat = true;
                        }
                        else
                        {
                            if (currBeneWife == 0)
                            {
                                Globals.Instance.strWidowStarting = Constants.spousal;
                                wifeFirstPayAge = ageWife;
                            }
                            else
                            {
                                Globals.Instance.strWidowStarting = Constants.Working;
                            }
                            currBeneWife = newBeneWife;
                            Globals.Instance.BoolWHBTakenWidowAt66 = true;
                            BenefitAt66Change = true;
                        }
                    }
                    #endregion Code for widow working values at age 66

                    #region Code for widow surviour values at age 70
                    //  Widowed survivor benefit age 70: used own to grow survivor benefit
                    if (soloType == Calc.calc_solo.Widowed
                    && strategy == Calc.calc_strategy.Widowed70
                    && wifeBenefitSource == benefit_source.Spouse
                    && ageWife == 70)
                    {
                        if (intAgeWife > wifeFRA)
                            multiply = AgeAbove68YearsOfWife();
                        else
                            multiply = decBeneWife;
                        newBeneWife = SurvivorBene(ageWife, multiply, birthYearWife, 66, 1956);
                        newBeneWife = ColaUpSpousal(ageWife, newBeneWife, birthYearWife, intAgeWife, wifeFRA);//ColaUp(ageWife, newBeneWife);
                        newWorkingBeneWife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                        newWorkingBeneWife = ColaUpSpousal(ageWife, newWorkingBeneWife, birthYearWife, intAgeWife, wifeFRA);
                        if (newBeneWife > newWorkingBeneWife)
                        {
                            wifeBenefitSource = benefit_source.Self;
                        }
                        else
                        {
                            newBeneWife = newWorkingBeneWife;
                        }
                        if (newBeneWife < currBeneWife)
                        {
                            badStrat = true;
                        }
                        else
                        {
                            if (currBeneWife == 0)
                                wifeFirstPayAge = ageWife;
                            currBeneWife = newBeneWife;
                            wifeBenefitSource = benefit_source.Spouse;
                            dat70 = true;
                            BenefitAt70Change = true;
                            Globals.Instance.WidowChangeAt70 = true;
                        }
                    }
                    #endregion Code for widow surviour values at age 70

                    #region Code for the Working and Survivour values at age 66 and at age 77
                    newBeneWife = SurvivorBene(wifeFRA, decBeneHusb, birthYearWife, 66, 1956);
                    newBeneWife = ColaUpSpousal(wifeFRA, newBeneWife, birthYearWife, intAgeWife, wifeFRA);//ColaUp(ageWife, newBeneWife);
                    surviourValueAtAge66 = Convert.ToInt32(newBeneWife);

                    newBeneWife = ColaUpSpousal(wifeFRA, decBeneWife, birthYearWife, intAgeWife, wifeFRA);//decBeneWife * Convert.ToDecimal(Math.Pow(1.025, (ageWife - 62)));
                    workingValueAtAge66 = Convert.ToInt32(newBeneWife);

                    newBeneWife = SurvivorBene(70, decBeneHusb, birthYearWife, 66, 1956);
                    newBeneWife = ColaUpSpousal(70, newBeneWife, birthYearWife, intAgeWife, wifeFRA);//ColaUp(ageWife, newBeneWife);
                    surviourValueAtAge70 = Convert.ToInt32(newBeneWife);

                    newWorkingBeneWife = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                    newWorkingBeneWife = ColaUpSpousal(70, newWorkingBeneWife, birthYearWife, intAgeWife, wifeFRA);
                    workingValueAtAge70 = newWorkingBeneWife;

                    if (ageWife == 62)
                    {
                        if (ageHusb >= 62)
                        {
                            Globals.Instance.BoolShowSecondConditionDivorce = false;
                        }
                        else
                        {
                            Globals.Instance.BoolShowSecondConditionDivorce = true;
                        }
                    }
                    #endregion Code for the Working and Survivour values at age 66 and at age 77

                    #endregion code build all alternate strategies

                    #region code to update totals in the grid and iterate the age and cola applied
                    //  Update running totals
                    currBene = currBeneWife;
                    //Code change on 05/28/2015 as per update from client to modify the annual total as per the FRA year and months
                    //original code was currBeneAnnual = (currBene * 12);
                    DateTime datediff = husbBirthDate.AddYears(62);
                    TimeSpan difftimespan = datediff.Subtract(wifeBirthDate);
                    int yearsWifeStarting, remainingMonthsStarting;
                    WifeAgeWhenSpouseAge62(out yearsWifeStarting, out remainingMonthsStarting);
                    int years = Convert.ToInt32((double)difftimespan.Days / 365.2425);
                    int months = Convert.ToInt32(((double)difftimespan.Days / 30.436875));
                    int remainingMonths = 0;
                    if (years * 12 > months)
                    {
                        remainingMonths = months % 12;
                        if (remainingMonths > 0)
                            years -= YearsAtSpouse62(remainingMonths);
                    }
                    else
                    {
                        remainingMonths = months % 12;
                    }
                    int intmonthHusb, intmultiplyMonth, intAgeWifemonthtemp = 0;
                    intAgeWifemonthtemp = intAgeWifeMonth;
                    intmonthHusb = intAgeHusbMonth;
                    int intresulttemp = 12 - intAgeWifemonthtemp;
                    int intresulttemp1 = intresulttemp + intmonthHusb;
                    if (intmonthHusb > intAgeWifeMonth)
                        intmultiplyMonth = (12 - (intmonthHusb - intAgeWifemonthtemp));
                    else
                        intmultiplyMonth = 12 - intresulttemp1;
                    if (birthYearWife >= 1955 && birthYearWife <= 1959 && ageWife == wifeFRA && (wifeFirstPayAge == wifeFRA || Globals.Instance.BoolWHBTakenWidowAt66))
                    {
                        if (soloType == calc_solo.Widowed)
                        {
                            if (Globals.Instance.BoolWHBTakenWidowAt66)
                                currAnnualBenefitsWife = annualBenefitsAsPerBirthDate(birthYearWife, currBeneWife);
                            else
                                currAnnualBenefitsWife = annualBenefitsAsPerBirthDateWidow(birthYearWife, currBeneWife);
                            if (currBeneWife > 0)
                            {
                                if (currAnnualBenefitsWife / currBeneWife != 12)
                                {
                                    if (Globals.Instance.BoolWHBTakenWidowAt66)
                                    {
                                        Globals.Instance.WifeNumberofMonthsBelowGrid = Convert.ToInt16(currAnnualBenefitsWife / currBeneWife);
                                        Globals.Instance.WifeNumberofYears = ageWife;
                                        Globals.Instance.WifeNumberofMonthsAboveGrid = (12 - Globals.Instance.WifeNumberofMonthsBelowGrid);
                                        BoolWifeDoubleAsterisk = true;
                                        remainingMonths = Globals.Instance.WifeNumberofMonthsAboveGrid;
                                    }
                                    else
                                    {
                                        if (currAnnualBenefitsWife != 0 && currBeneWife != 0)
                                        {
                                            Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(currAnnualBenefitsWife / currBeneWife);
                                            Globals.Instance.WifeNumberofMonthsinSteps = (12 - Globals.Instance.WifeNumberofMonthinNote);
                                            Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = ageWife;
                                            BoolWifeAgePDF = true;
                                        }
                                    }
                                    if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                                        newCombinedOncomeAgeDivorce = (ageWife + 1).ToString();
                                    Globals.Instance.BoolWHBTakenWidowAt66 = false;
                                }
                            }
                        }
                        else
                        {
                            currAnnualBenefitsWife = annualBenefitsAsPerBirthDate(birthYearWife, currBeneWife);
                            if (currBeneWife > 0)
                            {
                                if (currAnnualBenefitsWife / currBeneWife != 12)
                                {
                                    if (currAnnualBenefitsWife != 0 && currBeneWife != 0)
                                    {
                                        Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(currAnnualBenefitsWife / currBeneWife);
                                        Globals.Instance.WifeNumberofMonthsinSteps = (12 - Globals.Instance.WifeNumberofMonthinNote);
                                        Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = ageWife;
                                        BoolWifeAgePDF = true;
                                    }
                                    if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                                        newCombinedOncomeAgeDivorce = (ageWife + 1).ToString();
                                }
                            }
                        }
                    }
                    else if (birthYearWife >= 1955 && birthYearWife <= 1959 && ageWife == wifeFRA && (currBeneWife != 0) && (soloType == calc_solo.Widowed) && strategy.Equals(calc_strategy.WidowedSelf))
                    {
                        currAnnualBenefitsWife = annualBenefitsAsPerBirthDateWidow(birthYearWife, currBeneWife);
                        if (currBeneWife > 0)
                        {
                            if (currAnnualBenefitsWife / currBeneWife != 12)
                            {
                                if (currAnnualBenefitsWife != 0 && currBeneWife != 0)
                                {
                                    Globals.Instance.WifeNumberofYears = ageWife;
                                    Globals.Instance.WifeNumberofMonthsBelowGrid = Convert.ToInt16(currAnnualBenefitsWife / currBeneWife);
                                    Globals.Instance.WifeNumberofMonthsAboveGrid = (12 - Globals.Instance.WifeNumberofMonthsBelowGrid);
                                    remainingMonths = (12 - Globals.Instance.WifeNumberofMonthsBelowGrid);
                                }
                                if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                                    newCombinedOncomeAgeDivorce = (ageWife + 1).ToString();
                                BoolWifeDoubleAsterisk = true;
                            }
                        }
                    }
                    else if ((intAgeWife >= 62) && (currBeneWife != 0) && (Globals.Instance.strDivorceStarting == Constants.spousal || Globals.Instance.strWidowStarting == Constants.spousal) && (wifeFirstPayAge < years) && (ageWife >= wifeFRA) && (intAgeHusb < 62) && (ageWife < 70) && (soloType == calc_solo.Divorced))
                    {
                        if (wifeFirstPayAge == wifeFRA)
                        {
                            if ((cummBene == 0) && (ageWife >= wifeFRA))
                            {
                                int intMultiplyMonth = 0;
                                currAnnualBenefitsWife = WifeSpouseAgeLessThan62(out intMultiplyMonth, currBeneWife);
                                if (intMultiplyMonth != 12)
                                {
                                    if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                                        newCombinedOncomeAgeDivorce = (ageWife + 1).ToString();
                                    if (currBeneWife != 0)
                                    {
                                        Globals.Instance.WifeNumberofMonthinNote = (12 - intMultiplyMonth);
                                        Globals.Instance.WifeNumberofMonthsinSteps = intMultiplyMonth;
                                        Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = ageWife;
                                    }
                                    BoolWifeAgePDF = true;
                                }
                            }
                        }
                        else
                        {
                            if (ageWife == wifeFRA)
                            {
                                int intMultiplyMonth = 0;
                                currAnnualBenefitsWife = WifeSpouseAgeLessThan62(out intMultiplyMonth, currBeneWife);
                                currAnnualBenefitsWife += (decPrvBenefits * (12 - intmultiplyMonth));
                                if (intMultiplyMonth != 12)
                                {
                                    if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                                        newCombinedOncomeAgeDivorce = (ageWife + 1).ToString();
                                    if (currBeneWife != 0)
                                    {
                                        Globals.Instance.WifeNumberofMonthinNote = (12 - intMultiplyMonth);
                                        Globals.Instance.WifeNumberofMonthsinSteps = intMultiplyMonth;
                                        Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = ageWife;
                                    }
                                    BoolWifeAgePDF = true;
                                }
                            }
                            else if (ageWife >= wifeFRA && (cummBene == 0))
                            {
                                int intMultiplyMonth = 0;
                                currAnnualBenefitsWife = WifeSpouseAgeLessThan62(out intMultiplyMonth, currBeneWife);
                                if (intMultiplyMonth != 12)
                                {
                                    if (currBeneWife != 0)
                                    {
                                        Globals.Instance.WifeNumberofMonthinNote = (12 - intMultiplyMonth);
                                        Globals.Instance.WifeNumberofMonthsinSteps = intMultiplyMonth;
                                        Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = ageWife;
                                    }
                                    if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                                        newCombinedOncomeAgeDivorce = (ageWife + 1).ToString();
                                    BoolWifeAgePDF = true;
                                }
                            }
                        }
                    }
                    //loop fro when the fra has number of months and first pay age is graeter than fra than 1 yrs
                    else if ((intCurrentAgeWifeMonth >= 1) && (!BoolWifeAgePDF) && (intCurrentAgeWifeMonth != 12) && (intCurrentAgeWifeMonth != 0) && (wifeFRA == ageWife) && (ageWife < 70) && (currBeneWife != 0) && soloType == calc_solo.Widowed && strategy == calc_strategy.WidowedSelf)
                    {
                        int intDiff = 0;
                        int tempFRA = 0;
                        tempFRA = Convert.ToInt16(customer.WidowFRA(birthYearWife).Substring(0, 2));

                        if (tempFRA < wifeFRA)
                            intDiff = wifeFRA - tempFRA;
                        else
                            intDiff = tempFRA - wifeFRA;
                        if (intDiff >= 0 && intDiff <= 1)
                        {
                            currAnnualBenefitsWife = 0;
                            if (intDiff == 1 && wifeFRA - 1 == tempFRA && tempFRA >= 62)
                                BoolWifeDoubleAsterisk = true;
                        }
                    }

                    else if ((remainingMonths != 12) && boolDoubleAsterikCondition && (remainingMonths > 0) && (soloType == calc_solo.Divorced || soloType == calc_solo.Widowed) && (wifeBenefitSource == benefit_source.Spouse || wifeBenefitSource == benefit_source.SpousalBenefit))
                    {
                        if (currBeneWife != 0 && (ageWife == years))
                        {
                            currAnnualBenefitsWife = currBene * (12m - remainingMonths);
                            if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                                newCombinedOncomeAgeDivorce = (ageWife + 1).ToString();
                            if (cummBene == 0 && ageWife == wifeFirstPayAge)
                            {
                                Globals.Instance.WifeNumberofMonthinNote = (12 - remainingMonths);
                                Globals.Instance.WifeNumberofMonthsinSteps = remainingMonths;
                                Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = ageWife;
                                BoolWifeAgePDF = true;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                            }
                            else
                            {
                                Globals.Instance.WifeNumberofMonthsAboveGrid = Convert.ToInt16(remainingMonths);
                                Globals.Instance.WifeNumberofYears = ageWife;
                                Globals.Instance.WifeNumberofMonthsBelowGrid = (12 - remainingMonths);
                                BoolWifeDoubleAsterisk = true;
                            }
                        }
                        else
                        {
                            if (ageWife == years + 1)
                            {
                                currAnnualBenefitsWife = currBene * (12m + remainingMonths);
                                if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                                    newCombinedOncomeAgeDivorce = (ageWife + 1).ToString();
                                if (cummBene == 0 && ageWife == wifeFirstPayAge && years >= wifeFRA && ageWife > intAgeWife)
                                {
                                    Globals.Instance.WifeNumberofMonthinNote = (12 + remainingMonths);
                                    Globals.Instance.WifeNumberofMonthsinSteps = remainingMonths;
                                    Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = ageWife - 1;
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.BoolWifeAgeAdjusted = true;
                                }
                                else
                                {
                                    if (cummBene != 0 && ageWife != wifeFirstPayAge && wifeFirstPayAge > 0)
                                    {
                                        Globals.Instance.WifeNumberofMonthsAboveGrid = Convert.ToInt16(12m - remainingMonths);
                                        Globals.Instance.WifeNumberofYears = ageWife;
                                        Globals.Instance.WifeNumberofMonthsBelowGrid = (remainingMonths);
                                        BoolWifeDoubleAsterisk = true;
                                    }
                                }
                            }
                        }
                        boolDoubleAsterikCondition = false;
                    }
                    if (currAnnualBenefitsWife == 0)
                    {
                        currAnnualBenefitsWife = currBeneWife * 12;
                    }

                    if ((intCurrentAgeWifeMonth >= 1) && (!BoolWifeAgePDF) && (intCurrentAgeWifeMonth != 12) && (intCurrentAgeWifeMonth != 0) && (wifeFirstPayAge == ageWife) && (intCurrentAgeWifeYear <= wifeFirstPayAge) && (ageWife < 70) && (currBeneWife != 0))
                    {
                        int intDiff = 0;
                        if (intCurrentAgeWifeYear > wifeFirstPayAge)
                            intDiff = intCurrentAgeWifeYear - wifeFirstPayAge;
                        else
                            intDiff = wifeFirstPayAge - intCurrentAgeWifeYear;
                        if (intDiff >= 0 && intDiff <= 1 && soloType != calc_solo.Divorced)
                        {
                            currAnnualBenefitsWife = 0;
                            if (intDiff == 1 && wifeFirstPayAge - 1 == intCurrentAgeWifeYear && ((intCurrentAgeWifeYear >= 62 && soloType == calc_solo.Single) || (soloType == calc_solo.Widowed)))
                            {
                                currAnnualBenefitsWife = currBeneWife * (12 + (12 - intCurrentAgeWifeMonth));
                                Globals.Instance.WifeNumberofMonthinNote = (12 + (12 - intCurrentAgeWifeMonth));
                                Globals.Instance.WifeNumberofMonthsinSteps = (intCurrentAgeWifeMonth);
                                Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = intCurrentAgeWifeYear;
                                BoolWifeAgePDF = true;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                            }
                            else
                            {
                                if (wifeFirstPayAge == intCurrentAgeWifeYear && soloType != calc_solo.Divorced)
                                {
                                    currAnnualBenefitsWife = currBeneWife * (12 - intCurrentAgeWifeMonth);
                                    Globals.Instance.WifeNumberofMonthinNote = (12 - intCurrentAgeWifeMonth);
                                    Globals.Instance.WifeNumberofMonthsinSteps = (intCurrentAgeWifeMonth);
                                    Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = ageWife;
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.BoolWifeAgeAdjusted = true;
                                }
                            }
                        }
                        else
                        {
                            if (intCurrentAgeWifeYear == wifeFirstPayAge && soloType == calc_solo.Divorced)
                            {
                                currAnnualBenefitsWife = 0;
                                currAnnualBenefitsWife = currBeneWife * (12 - intCurrentAgeWifeMonth);
                                Globals.Instance.WifeNumberofMonthinNote = (12 - intCurrentAgeWifeMonth);
                                Globals.Instance.WifeNumberofMonthsinSteps = (intCurrentAgeWifeMonth);
                                Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = ageWife;
                                BoolWifeAgePDF = true;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                            }
                            else
                            {
                                if (intDiff == 1 && wifeFirstPayAge == intCurrentAgeWifeYear - 1 && soloType == calc_solo.Divorced)
                                {
                                    currAnnualBenefitsWife = 0;
                                    currAnnualBenefitsWife = currBeneWife * (12 + (12 - intCurrentAgeWifeMonth));
                                    Globals.Instance.WifeNumberofMonthinNote = 12 + (12 - intCurrentAgeWifeMonth);
                                    Globals.Instance.WifeNumberofMonthsinSteps = (intCurrentAgeWifeMonth);
                                    Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = intCurrentAgeWifeYear;
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.BoolWifeAgeAdjusted = true;
                                }
                            }
                        }
                    }



                    if (currAnnualBenefitsWife == 0)
                    {
                        currBeneAnnual = currBene * 12;
                        if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                            newCombinedOncomeAgeDivorce = (ageWife).ToString();
                    }
                    else
                    {
                        currBeneAnnual = currAnnualBenefitsWife;
                        currAnnualBenefitsWife = 0;
                        if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                            newCombinedOncomeAgeDivorce = (ageWife + 1).ToString();
                    }
                    #region Code to calculate the cummulative benefits before and after younger age 70
                    if (aIx == 70)
                    {
                        currBeneWifeLocal = currBeneWife;
                    }
                    if (aIx >= 75 && aIx <= 90)
                    {
                        if (aIx == 75)
                        {
                            decimal grandTotal = 0;
                            for (int i = 0; i < 5; i++)
                            {
                                currBeneWifeLocal = ColaUp(63, currBeneWifeLocal);
                                decimal total = (currBeneWifeLocal * 12);
                                grandTotal += Convert.ToInt32(total);
                            }
                            cummBene += grandTotal;
                        }
                        else
                        {
                            decimal grandTotal = 0;
                            for (int i = 0; i < 5; i++)
                            {
                                currBeneWifeLocal = ColaUp(63, currBeneWifeLocal);
                                decimal total = (currBeneWifeLocal * 12);
                                grandTotal += Convert.ToInt32(total);
                            }
                            cummBene += grandTotal;
                        }
                    }
                    if (aIx <= 70)
                    {
                        cummBene += currBeneAnnual;
                    }
                    #endregion Code to calculate the cummulative benefits before and after younger age 70
                    cummBeneWife += currBeneWife;





                    //  Set flag if this is the high wage earner's life expectancy year
                    //if (ageWife == 85)
                    //    showThisLine = true;
                    //else
                    //    showThisLine = false;
                    string ageCol = aIx.ToString();

                    //  For selected ages, build the display row
                    if (youngest <= 90)// ((aIx <= topAix) || (aIx == topAix + 10) || showThisLine || didAutoAdjust)
                    {
                        System.Data.DataRow incomeRow = incomeTab.NewRow();
                        incomeRow[(int)disp_cols.Age] = ageCol;
                        if (currBene > 0)
                        {
                            #region BoolWifeAgePDF
                            if (BoolWifeAgePDF)
                            {
                                int intDiff = 0;
                                intDiff = wifeFirstPayAge - intCurrentAgeWifeYear;
                                int intMultiplyMonth = 0;
                                if (intDiff == 1)
                                {
                                    if (currBeneWife != 0)
                                        intMultiplyMonth = Convert.ToInt16(currBeneAnnual / currBeneWife) - 12;
                                    currBeneAnnual = currBeneWife * intMultiplyMonth;
                                    cummBene = currBeneAnnual;

                                    //Update the previous row with new values
                                    if (incomeTab.Rows.Count > 0)
                                    {
                                        incomeTab.Rows[incomeTab.Rows.Count - 1][(int)disp_cols.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);
                                        incomeTab.Rows[incomeTab.Rows.Count - 1][2] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + "#";
                                        incomeTab.Rows[incomeTab.Rows.Count - 1][(int)disp_cols.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);
                                        Globals.Instance.WifeNumberofMonthinNote = intMultiplyMonth;
                                        Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = Convert.ToInt16(incomeTab.Rows[incomeTab.Rows.Count - 1][(int)disp_cols.Age]);

                                        currBeneWife = currBeneWife * 1.025m;
                                        currBeneAnnual = currBeneWife * 12;
                                        cummBene = currBeneAnnual + cummBene;
                                    }
                                    //Update the current row with new values
                                    incomeRow[2] = currBeneAnnual.ToString(Constants.AMT_FORMAT);
                                    BoolWifeAgePDF = false;
                                    incomeRow[(int)disp_cols.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);
                                    incomeRow[(int)disp_cols.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);
                                }
                                else if (wifeFirstPayAge == wifeFRA && (birthYearWife == 1958 || birthYearWife == 1959))
                                {
                                    if (birthYearWife == 1958)
                                        intMultiplyMonth = 4;
                                    else if (birthYearWife == 1959)
                                        intMultiplyMonth = 2;
                                    currBeneAnnual = currBeneWife * intMultiplyMonth;
                                    cummBene = currBeneAnnual;

                                    //Update the previous row with new values
                                    if (incomeTab.Rows.Count > 0)
                                    {
                                        incomeTab.Rows[incomeTab.Rows.Count - 1][(int)disp_cols.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);
                                        incomeTab.Rows[incomeTab.Rows.Count - 1][2] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + "#";
                                        incomeTab.Rows[incomeTab.Rows.Count - 1][(int)disp_cols.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);
                                        Globals.Instance.WifeNumberofMonthinNote = intMultiplyMonth;
                                        Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = Convert.ToInt16(incomeTab.Rows[incomeTab.Rows.Count - 1][(int)disp_cols.Age]);

                                        currBeneWife = currBeneWife * 1.025m;
                                        currBeneAnnual = currBeneWife * 12;
                                        cummBene = currBeneAnnual + cummBene;
                                    }
                                    //Update the current row with new values
                                    incomeRow[2] = currBeneAnnual.ToString(Constants.AMT_FORMAT);
                                    BoolWifeAgePDF = false;
                                    incomeRow[(int)disp_cols.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);
                                    incomeRow[(int)disp_cols.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);
                                }
                                else if (wifeFirstPayAge == (years + 1) && wifeFirstPayAge == ageWife && ageWife > 62)
                                {
                                    currBeneAnnual = currBeneWife * (12 - remainingMonths);
                                    cummBene = currBeneAnnual;

                                    //Update the previous row with new values
                                    if (incomeTab.Rows.Count > 0)
                                    {
                                        incomeTab.Rows[incomeTab.Rows.Count - 1][(int)disp_cols.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);
                                        incomeTab.Rows[incomeTab.Rows.Count - 1][2] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + "#";
                                        incomeTab.Rows[incomeTab.Rows.Count - 1][(int)disp_cols.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);
                                        Globals.Instance.WifeNumberofMonthinNote = (12 - remainingMonths);
                                        Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = Convert.ToInt16(incomeTab.Rows[incomeTab.Rows.Count - 1][(int)disp_cols.Age]);

                                        currBeneWife = currBeneWife * 1.025m;
                                        currBeneAnnual = currBeneWife * 12;
                                        cummBene = currBeneAnnual + cummBene;
                                    }
                                    //Update the current row with new values
                                    incomeRow[2] = currBeneAnnual.ToString(Constants.AMT_FORMAT);
                                    BoolWifeAgePDF = false;
                                    incomeRow[(int)disp_cols.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);
                                    incomeRow[(int)disp_cols.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);
                                    wifeFirstPayAge = wifeFirstPayAge - 1;
                                    DivorceSpousalBenefitChange = wifeFirstPayAge.ToString();
                                }
                                else
                                {
                                    incomeRow[2] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + "#";
                                    BoolWifeAgePDF = false;
                                    incomeRow[(int)disp_cols.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);
                                    incomeRow[(int)disp_cols.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);
                                }
                            }
                            #endregion BoolWifeAgePDF

                            # region BoolWifeDoubleAsterisk
                            else if (BoolWifeDoubleAsterisk)
                            {
                                string strTempFRA = customer.WidowFRA(birthYearWife);

                                int intTempFRA = Convert.ToInt32(strTempFRA.Substring(0, 2));
                                string strBenefit = string.Empty;
                                int intPrevBenefit = 0, intPrevAnnualIcome = 0, intPrevCummIncome = 0;

                                //Added to when FRA is months and user age is less than 62
                                if (strTempFRA.Contains("Months") && soloType == calc_solo.Widowed && intTempFRA < wifeFRA)
                                {
                                    strBenefit = incomeTab.Rows[incomeTab.Rows.Count - 1][1].ToString().TrimStart('$');
                                    intPrevBenefit = Convert.ToInt32((Regex.Replace(strBenefit, regExp, "")));
                                    int intMonth = customer.WidowFRAMonths(birthYearWife);
                                    intPrevAnnualIcome = intPrevBenefit * intMonth;
                                    Globals.Instance.HusbNumberofMonthsBelowGrid = intMonth;
                                    Globals.Instance.WifeNumberofMonthsAboveGrid = intMonth;
                                    if (intPrevAnnualIcome != 0)
                                    {
                                        incomeTab.Rows[incomeTab.Rows.Count - 1][2] = (intPrevAnnualIcome).ToString(Constants.AMT_FORMAT) + Constants.SingleAsterisk;
                                        Globals.Instance.BoolWHBSwitchedHusb = true;
                                    }
                                    Int32 intCummAnnual = Int32.Parse(Regex.Replace(incomeTab.Rows[incomeTab.Rows.Count - 2][3].ToString(), regExp, ""));
                                    incomeTab.Rows[incomeTab.Rows.Count - 1][3] = (intCummAnnual + int.Parse(Regex.Replace(incomeTab.Rows[incomeTab.Rows.Count - 1][2].ToString(), regExp, "")));
                                    intPrevCummIncome = Convert.ToInt32(incomeTab.Rows[incomeTab.Rows.Count - 1][3]);
                                    incomeTab.Rows[incomeTab.Rows.Count - 1][3] = decimal.Parse(incomeTab.Rows[incomeTab.Rows.Count - 1][3].ToString()).ToString(Constants.AMT_FORMAT);
                                    BoolWifeDoubleAsterisk = false;
                                    incomeRow[(int)disp_cols.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);
                                    currBeneAnnual = currBeneWife * (12 + (12 - intMonth));
                                    incomeRow[2] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + Constants.DoubleAsterisk;
                                    cummBene = intPrevCummIncome + currBeneAnnual;
                                    incomeRow[(int)disp_cols.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);

                                    Globals.Instance.WifeNumberofMonthsBelowGrid = 12 + (12 - intMonth);
                                }
                                else
                                {
                                    string strhashIncluded = string.Empty;
                                    if (incomeRow[2].ToString().Contains(Constants.SingleHash))
                                        strhashIncluded = Constants.SingleHash;

                                    strBenefit = incomeTab.Rows[incomeTab.Rows.Count - 1][1].ToString().TrimStart('$');
                                    intPrevBenefit = Convert.ToInt32((Regex.Replace(strBenefit, regExp, "")));
                                    intPrevAnnualIcome = intPrevBenefit * (12 + remainingMonths);
                                    if (incomeTab.Rows[incomeTab.Rows.Count - 1][2].ToString().Contains(Constants.SingleHash))
                                        strhashIncluded = Constants.SingleHash;
                                    if (intPrevAnnualIcome != 0)
                                    {
                                        incomeTab.Rows[incomeTab.Rows.Count - 1][2] = (intPrevAnnualIcome).ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.SingleAsterisk;
                                        Globals.Instance.BoolWHBSwitchedHusb = true;
                                    }
                                    Int32 intCummAnnual = Int32.Parse(Regex.Replace(incomeTab.Rows[incomeTab.Rows.Count - 2][3].ToString(), regExp, ""));
                                    incomeTab.Rows[incomeTab.Rows.Count - 1][3] = (intCummAnnual + int.Parse(Regex.Replace(incomeTab.Rows[incomeTab.Rows.Count - 1][2].ToString(), regExp, "")));
                                    intPrevCummIncome = Convert.ToInt32(incomeTab.Rows[incomeTab.Rows.Count - 1][3]);
                                    incomeTab.Rows[incomeTab.Rows.Count - 1][3] = decimal.Parse(incomeTab.Rows[incomeTab.Rows.Count - 1][3].ToString()).ToString(Constants.AMT_FORMAT);
                                    Globals.Instance.HusbNumberofMonthsBelowGrid = 12 + remainingMonths;
                                    BoolWifeDoubleAsterisk = false;

                                    incomeRow[(int)disp_cols.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);

                                    //Commented to apply both for Widowed and Divorced 28/12/2016
                                    //newly added
                                    //if (soloType == calc_solo.Widowed)
                                    //{
                                    //    currBeneAnnual = currBeneWife * (24 - Globals.Instance.HusbNumberofMonthsBelowGrid);
                                    //}
                                    //else
                                    //currBeneAnnual = currBeneWife * remainingMonths;

                                    currBeneAnnual = currBeneWife * (24 - Globals.Instance.HusbNumberofMonthsBelowGrid);

                                    incomeRow[2] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.DoubleAsterisk;
                                    cummBene = intPrevCummIncome + currBeneAnnual;
                                    incomeRow[(int)disp_cols.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);
                                }
                            }
                            # endregion BoolWifeDoubleAsterisk

                            else
                            {
                                incomeRow[2] = currBeneAnnual.ToString(Constants.AMT_FORMAT);
                                incomeRow[(int)disp_cols.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);
                                incomeRow[(int)disp_cols.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);
                            }
                        }
                        else
                        {
                            incomeRow[(int)disp_cols.WifeInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_cols.AnnInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_cols.CummInc] = Constants.ZeroWithOutAsterisk;
                        }
                        incomeTab.Rows.Add(incomeRow);


                    }

                    //Add Restricted Application Income
                    if (strategy == calc_strategy.Spousal && currBeneWife != 0 && ageWife < 70)
                        Globals.Instance.IntShowRestrictIncomeForDivorce += (int)currBeneAnnual;

                    if (soloType.ToString() == Constants.Divorced)
                    {
                        if (strategy.Equals(calc_strategy.Standard) && aIx.ToString().Equals(wifeFirstPayAge.ToString()))
                        {
                            decPrvBenefits = decimal.Parse(currBeneWife.ToString().Replace(",", ""));
                        }
                    }
                    int ageDiff = intAgeWife - intAgeHusb;
                    if (aIx >= 70 && (ageDiff >= 10) && (soloType == calc_solo.Divorced))
                    {
                        if (aIx >= intAgeWife + ageDiff)
                        {
                            //code to increase the age by 5 after younger reaches at age 70
                            aIx = aIx + 5;
                            oldest = oldest + 5;
                            youngest = youngest + 5;
                            currBeneWife = ColaUp(67, currBeneWife);
                        }
                        else
                        {
                            //code to increase the age earlier
                            oldest++;
                            aIx++;
                            youngest++;
                            currBeneWife = ColaUp(63, currBeneWife);
                        }
                    }
                    else
                    {
                        if (aIx >= 70)
                        {
                            //code to increase the age by 5 after younger reaches at age 70
                            aIx = aIx + 5;
                            oldest = oldest + 5;
                            youngest = youngest + 5;
                            currBeneWife = ColaUp(67, currBeneWife);
                        }
                        else
                        {
                            //code to increase the age earlier
                            oldest++;
                            aIx++;
                            youngest++;
                            currBeneWife = ColaUp(63, currBeneWife);
                        }
                    }
                }
                #endregion code to update totals in thegrid and iterate the age and cola applied

                #region Code to build clumns with setting the basic variable and back color
                //  -------------
                //  Finalize Grid
                //  -------------
                Grid1.DataSource = incomeSet;
                Grid1.DataBind();
                //Grid1.CssClass = "gridWidth";
                TableItemStyle tisA = Grid1.AlternatingRowStyle;
                //tisA.BackColor = Color.LightGray;
                //Grid1.BorderStyle = Constants.STD_BORDER;
                //Grid1.BorderWidth = Constants.STD_BORDER_WIDTH;
                int cummTotal = 0;
                //  ---------------------------------
                //  HIGHLIGHT cells to match the book
                //  ---------------------------------
                // Use high earner's FRA to trigger cell coloring
                int highEarnerFRA = wifeFRA;
                string fraYear = highEarnerFRA.ToString();
                string fraYear3 = (69).ToString();
                string fraYear4 = (70).ToString();
                string fraYear14 = (80).ToString();
                bool boolToRunLoopOnlyOnce = true;
                bool boolToRunLoopOnly1 = true;
                foreach (GridViewRow gvr in Grid1.Rows)
                {
                    float fFRAyr = FRA(birthYearWife);
                    //get FRA month as per date of birth
                    float iRndYr = Convert.ToInt32(fFRAyr);
                    int fRndMnt = Convert.ToInt32(fFRAyr - iRndYr);
                    if (soloType.ToString() == Constants.Divorced)
                    {
                        if (gvr.Cells[0].Text.Equals(SpousalBenefitsDivorce.ToString()))
                        {
                            DivorceSpousalBenefits = decimal.Parse(gvr.Cells[1].Text.Substring(1));
                            DivorceSpousalAge = gvr.Cells[0].Text;
                        }
                        else if (gvr.Cells[0].Text.Equals(DivorceSpousalBenefitChange.ToString()))
                        {
                            DivorceSpousalBenefits = decimal.Parse(gvr.Cells[1].Text.Substring(1));
                            DivorceSpousalAge = gvr.Cells[0].Text;
                        }

                    }
                    if (soloType.ToString().Equals(Constants.Widowed))
                    {

                        if (BenefitAt66Change)
                        {
                            if (gvr.Cells[0].Text.Equals((wifeFRA).ToString()))
                            {
                                WidowAnnualIncomeValue = decimal.Parse((Regex.Replace(gvr.Cells[2].Text.Substring(1), regExp, "")));
                            }
                        }
                        if (BenefitAt70Change)
                        {
                            if (gvr.Cells[0].Text.Equals(Constants.Number70.ToString()))
                            {
                                WidowAnnualIncomeValue = decimal.Parse((Regex.Replace(gvr.Cells[2].Text.Substring(1), regExp, "")));
                            }
                        }
                    }
                    if (gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[1].Text != Constants.ZeroWithAsterisk)
                    {
                        if (boolToRunLoopOnlyOnce)
                        {
                            SAgeValue = gvr.Cells[0].Text;
                            SValueStarting = decimal.Parse(gvr.Cells[1].Text.Substring(1));
                            boolToRunLoopOnlyOnce = false;
                        }
                    }
                    if (gvr.Cells[0].Text.Equals(wifeFRA.ToString()) && (BenefitAt66Change))
                    {
                        AgeValueAt70 = decimal.Parse(gvr.Cells[1].Text.Substring(1));
                        AgeAtLastChange = gvr.Cells[0].Text;
                    }
                    if (gvr.Cells[0].Text.Equals(Constants.Number70) && (BenefitAt70Change))
                    {
                        AgeValueAt70 = decimal.Parse(gvr.Cells[1].Text.Substring(1));
                        AgeAtLastChange = gvr.Cells[0].Text;
                    }
                    if ((gridname.Equals(Constants.WSpouse) || gridname.Equals(Constants.WAsap)) && gvr.Cells[0].Text.Equals(intAgeWife.ToString()) && intAgeWife > wifeFRA)
                    {
                        AgeValueAt70 = SValueStarting;
                        AgeAtLastChange = SAgeValue;
                        BoolAboveFRAageStep2 = false;
                    }
                    if (gvr.Cells[0].Text.Equals(SpousalBenefitsDivorce.ToString()))
                    {
                        if (boolToRunLoopOnly1)
                        {
                            DivorceSpousalBenefits = decimal.Parse(gvr.Cells[1].Text.Substring(1));
                            DivorceSpousalAge = gvr.Cells[0].Text;
                            boolToRunLoopOnly1 = false;
                        }
                        else if (gvr.Cells[0].Text.Equals(DivorceSpousalBenefitChange.ToString()))
                        {
                            DivorceSpousalBenefits = decimal.Parse(gvr.Cells[1].Text.Substring(1));
                            DivorceSpousalAge = gvr.Cells[0].Text;
                        }
                    }
                    gvr.Cells[(int)disp_cols.Age].BorderColor = LOW_GREY;
                    gvr.Cells[(int)disp_cols.WifeInc].BorderColor = LOW_GREY;
                    gvr.Cells[(int)disp_cols.AnnInc].BorderColor = LOW_GREY;
                    gvr.Cells[(int)disp_cols.CummInc].BorderColor = LOW_GREY;
                    string rowAge = gvr.Cells[(int)disp_cols.Age].Text;
                    if (rowAge == fraYear3) //  69
                    {
                        if (gvr.Cells[(int)disp_cols.CummInc].Text != Constants.ZeroWithOutAsterisk)
                        {
                            //gvr.Cells[(int)disp_cols.CummInc].CssClass = HIGHLIGHT_CLASS;
                            string[] cummBucks = gvr.Cells[(int)disp_cols.CummInc].Text.Substring(1).Split(',');
                            cummTotal = int.Parse(string.Join("", cummBucks));
                        }
                    }
                    if (rowAge == fraYear4) //  70
                    {
                        gvr.Cells[(int)disp_cols.WifeInc].CssClass = HIGHLIGHT_CLASS;
                        string[] combBucks = gvr.Cells[(int)disp_cols.WifeInc].Text.Substring(1).Split(',');
                        age70CombBene = int.Parse(string.Join(string.Empty, combBucks));
                        gvr.Cells[(int)disp_cols.AnnInc].CssClass = HIGHLIGHT_CLASS;
                        string[] wifeBucks = gvr.Cells[(int)disp_cols.WifeInc].Text.Substring(1).Split(',');
                        gvr.Cells[(int)disp_cols.WifeInc].CssClass = Constants.HIGHLIGHT_70;
                        age70HighBene = int.Parse(string.Join(string.Empty, wifeBucks));
                    }
                    if (rowAge == fraYear14) //  80
                    {
                        string[] combBucks = gvr.Cells[(int)disp_cols.WifeInc].Text.Substring(1).Split(',');
                        age80CombBene = int.Parse(string.Join(string.Empty, combBucks));
                    }
                }
                #endregion Code to build clumns with setting the basic variable and back color
                return cummTotal;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// Used to calaulate age of user/spouse in months 
        /// </summary>
        /// <returns>age of wife</returns>
        public int CalculateAgeinTotalMonth(DateTime dtUserDate)
        {
            DateTime Today = DateTime.Now;
            TimeSpan ts;
            DateTime Age;
            int Years, intTotalMonths;
            try
            {
                ts = Today - dtUserDate;
                Age = DateTime.MinValue + ts;
                Years = Age.Year - 1;
                intTotalMonths = (Years * 12) + (Age.Month - 1);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
            return intTotalMonths;
        }

        /// <summary>
        /// get number of years when spouse is 62 and determine the users years.
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        private int YearsAtSpouse62(int startAge)
        {
            try
            {
                int FRA_Age = 0;
                if (startAge < 12)
                    FRA_Age = 1;
                else if (startAge > 12 && startAge < 24)
                    FRA_Age = 2;
                else if (startAge > 24 && startAge < 36)
                    FRA_Age = 3;
                else if (startAge > 36 && startAge < 48)
                    FRA_Age = 4;
                else if (startAge > 48 && startAge < 60)
                    FRA_Age = 5;
                else if (startAge > 60 && startAge < 72)
                    FRA_Age = 6;
                return FRA_Age;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// calculate Annual Benefit when spouse age less than 62 yrs
        /// </summary>
        /// <param name="currBeneWife"></param>
        /// <returns></returns>
        /// HusbandSpouseAgeLessThan62(out int intMultiplyMonth, decimal currBeneHusb)
        private decimal WifeSpouseAgeLessThan62(out int intMultiplyMonth, decimal currBeneWife)
        {
            try
            {
                int intMonthHusb, intAgeWifeMonthtemp = 0;
                intAgeWifeMonthtemp = intAgeWifeMonth;
                intMonthHusb = intAgeHusbMonth;
                int intresulttemp = 12 - intAgeWifeMonthtemp;
                int intresulttemp1 = intresulttemp + intMonthHusb;
                if (intMonthHusb > intAgeWifeMonth)
                    intMultiplyMonth = (12 - (intMonthHusb - intAgeWifeMonthtemp));
                else
                    intMultiplyMonth = 12 - intresulttemp1;
                currBeneWife = Convert.ToInt32(currBeneWife * intMultiplyMonth);
                return currBeneWife;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                intMultiplyMonth = 0;
                currBeneWife = 0;
                return 0;
            }
        }

        /// <summary>
        /// calculate Annual Benefit when spouse age less than 62 yrs
        /// </summary>
        /// <param name="currBeneHusb"></param>
        /// <returns></returns>
        private decimal HusbandSpouseAgeLessThan62(out int intMultiplyMonth, decimal currBeneHusb)
        {
            try
            {
                int intMonthWife, intAgeHusbMonthtemp = 0;
                intAgeHusbMonthtemp = intAgeHusbMonth;
                intMonthWife = intAgeWifeMonth;
                int intresulttemp = 12 - intAgeHusbMonthtemp;
                int intresulttemp1 = intresulttemp + intMonthWife;
                if (intMonthWife > intAgeHusbMonth)
                    intMultiplyMonth = (12 - (intMonthWife - intAgeHusbMonthtemp));
                else
                    intMultiplyMonth = 12 - intresulttemp1;
                currBeneHusb = Convert.ToInt32(currBeneHusb * intMultiplyMonth);
                return currBeneHusb;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                intMultiplyMonth = 0;
                currBeneHusb = 0;
                return 0;
            }
        }

        /// <summary>
        /// method to get the working benefits with the reduction factors accroding to the firstpayage
        /// </summary>
        /// <param name="survAge"></param>
        /// <param name="amt"></param>
        /// <returns></returns>
        protected decimal WorkingBenefitsReductionFacors(int survAge, decimal amt)
        {
            //loop over the first pay age
            try
            {
                switch (survAge)
                {
                    case 62: result = (amt * 0.75m); break;
                    case 63: result = (amt * 0.82m); break;
                    case 64: result = (amt * 0.87m); break;
                    case 65: result = (amt * 0.93m); break;
                        //case 66: result = (amt * 1m); break;
                }
                return result;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="survAge"></param>
        /// <param name="amt"></param>
        /// <returns></returns>
        protected decimal WorkingBenefitsReductionFacorsForSpousal(int survAge, decimal amt)
        {
            //loop over the first pay age
            try
            {
                switch (survAge)
                {
                    case 62: result = (amt * 0.725m); break;
                    case 63: result = (amt * 0.80m); break;
                    case 64: result = (amt * 0.85m); break;
                    case 65: result = (amt * 0.90m); break;
                        //case 66: result = (amt * 1m); break;
                }
                return result;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// spousal reduction fatcor when the user cliams the benefits early
        /// </summary>
        /// <param name="survAge"></param>
        /// <param name="amt"></param>
        /// <returns></returns>
        protected decimal SpousalBenefitsReductionFacorsWhenWorkbenefitsclaimEarly(int survAge, decimal amt)
        {
            //loop over the first pay age
            try
            {
                switch (survAge)
                {
                    case 62: result = (amt * 0.858m); break;
                    case 63: result = (amt * 0.908m); break;
                    case 64: result = (amt * 0.958m); break;
                        //case 66: result = (amt * 1m); break;
                }
                return result;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// function to calculate the fourth strategy in widow scenario
        /// </summary>
        /// <param name="Grid1"></param>
        /// <param name="strategy"></param>
        /// <param name="soloType"></param>
        /// <param name="startAge"></param>
        /// <param name="gridNote"></param>
        /// <param name="gridname"></param>
        /// <returns></returns>
        public int BuildSingleWidow(GridView Grid1, calc_strategy strategy, calc_solo soloType, calc_start startAge, ref System.Web.UI.WebControls.Label gridNote, String gridname)
        {

            try
            {
                #region FRA AND BASE AGE CALCUALTION
                badStrat = false;
                int baseAge = 62;
                switch (startAge)
                {
                    case calc_start.asap62:
                        baseAge = 62;
                        break;
                    case calc_start.asap60:
                        baseAge = 60;
                        break;
                    case calc_start.fra6667:
                        baseAge = wifeFRA;
                        break;
                    case calc_start.max70:
                        baseAge = 70;
                        break;
                }
                wifeFRA = Convert.ToInt32(WidowFRA(birthYearWife));
                #endregion
                Customers customer = new Customers();
                #region BIND DATA TO GRID AND GENERATE THE COLUMNS WITH COLUMN HEADERS

                disp_cols dispCol;
                gridNote.Text = string.Empty;

                // Build dataset
                System.Data.DataSet incomeSet = new System.Data.DataSet();
                System.Data.DataTable incomeTab = incomeSet.Tables.Add("incomeTab");

                // Initialize grid
                Grid1.Columns.Clear();
                Grid1.AutoGenerateColumns = false;

                // --------------------------------------------------------------------------------
                // Build Display Columns and Dataset Columns
                // --------------------------------------------------------------------------------
                var values = (disp_cols[])Enum.GetValues(typeof(disp_cols));
                string sHusbInitials = Your_Name.Substring(0, 1);
                for (int dcIx = 0; dcIx < values.Length; dcIx++)
                {
                    dispCol = (disp_cols)dcIx;
                    incomeTab.Columns.Add(dispCol.ToString());
                    BoundField bf = new BoundField();
                    bf.HtmlEncode = false;
                    TableItemStyle tis = Grid1.HeaderStyle;
                    tis.Wrap = true;
                    switch (dispCol)
                    {
                        // case disp_cols.Year:
                        //   bf.HeaderText = "Year";
                        // break;
                        case disp_cols.Age:
                            bf.HeaderText = Constants.HeaderA + Constants.HeaderBreak + Constants.Age + Constants.OpeningBracket + sHusbInitials + Constants.CloseingBracket;
                            break;
                        case disp_cols.WifeInc:
                            bf.HeaderText = Constants.HeaderB + Constants.HeaderBreak + Constants.MonthlyIncomeSingle;
                            break;
                        case disp_cols.AnnInc:
                            bf.HeaderText = Constants.HeaderC + Constants.HeaderBreak + Constants.AnnualIncome;
                            break;
                        case disp_cols.CummInc:
                            bf.HeaderText = Constants.HeaderD + Constants.HeaderBreak + Constants.CumulativeIncome;
                            break;
                    }
                    bf.DataField = dispCol.ToString();
                    bf.ReadOnly = true;
                    Grid1.Columns.Add(bf);
                }
                incomeTab.Columns.Add("Note");

                // Date : 20112014
                // Description : Add Extra Column "YEAR" in Grid.
                // int year, incYr = 0;
                int ageWife, ageHusb;
                int wifeFirstPayAge = 0;
                decimal currBene = 0, currBeneAnnual = 0, currAnnualBenefitsWife = 0;
                decimal currBeneWife = 0;
                decimal cummBeneWife = 0;
                decimal cummBene = 0;


                #endregion

                #region Code to set the ages and other values
                //benefit_source wifeBenefitSource = benefit_source.None;
                int youngest = Math.Min(intAgeHusb, intAgeWife);
                int oldest = Math.Max(intAgeHusb, intAgeWife);
                int topAix = 70;
                benefit_source wifeBenefitSource = benefit_source.None;
                //  Support a start point for standard strategies 
                if (baseAge > 64)
                    topAix = Math.Max(topAix, baseAge);
                int BotAix;
                if (soloType == Calc.calc_solo.Widowed)
                {
                    BotAix = 60;
                }
                else
                {
                    BotAix = 62;
                }
                #endregion Code to set the ages and other values

                #region Code to get surviour values at FRA
                //  Build the data rows
                for (int aIx = BotAix; aIx <= 90;)//(int aIx = BotAix; aIx < 99; aIx++)
                {
                    ageWife = aIx;
                    ageHusb = aIx + intAgeHusb - intAgeWife;
                    if (soloType == Calc.calc_solo.Widowed
                    && strategy == Calc.calc_strategy.Claim67
                    && wifeBenefitSource == benefit_source.None
                    && ageWife >= wifeFRA)
                    {
                        //currBeneWife = SurvivorBene(ageWife, decBeneHusb, birthYearWife, 66, 1956);
                        Globals.Instance.strWidowStarting = Constants.Working;
                        currBeneWife = ColaUpSpousal(ageWife, decBeneHusb, birthYearWife, intAgeWife, wifeFRA);//ColaUp(ageWife, currBeneWife);
                        wifeBenefitSource = benefit_source.Self;
                        wifeFirstPayAge = ageWife;
                    }
                    #endregion Code to get surviour values at FRA

                    #region Code to get the working value at age 70
                    if (soloType == Calc.calc_solo.Widowed
                            && strategy == Calc.calc_strategy.Claim67
                            && wifeBenefitSource == benefit_source.Self
                            && ageWife == 70)
                    {
                        //currBeneWife = decBeneWife * Convert.ToDecimal(Math.Pow(1.025, (wifeFRA - 62)));
                        //labour percentage included to calulate the working benefits at 70 which would be 8 multiply by number diff between FRA age and the age 70
                        if (currBeneWife == 0)
                            Globals.Instance.strWidowStarting = Constants.Working;
                        currBeneWife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife); //((decBeneWife * (8 * (70 - wifeFRA))) / 100) + decBeneWife;
                        currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intAgeWife, wifeFRA);//currBeneWife * Convert.ToDecimal(Math.Pow(1.025, (ageWife - 62)));
                        wifeBenefitSource = benefit_source.Spouse;
                        //newDat70 = true;
                        dat70 = true;
                    }
                    #endregion Code to get the working value at age 70

                    #region Code to update the totals in the Grid
                    //  Update running totals
                    currBene = currBeneWife;
                    //Code change on 05/28/2015 as per update from client to modify the annual total as per the FRA year and months
                    //original code was currBeneAnnual = (currBene * 12);
                    #region Code to update Annual benefits in grid
                    DateTime datediff = husbBirthDate.AddYears(62);
                    TimeSpan difftimespan = datediff.Subtract(wifeBirthDate);
                    int years = Convert.ToInt32((double)difftimespan.Days / 365.2425);
                    int months = Convert.ToInt32(((double)difftimespan.Days / 30.436875));
                    int remainingMonths = months - years * 12;
                    if (birthYearWife >= 1955 && birthYearWife <= 1959 && ageWife == wifeFRA && wifeFirstPayAge == wifeFRA)
                    {
                        if (soloType == calc_solo.Widowed)
                        {
                            currAnnualBenefitsWife = annualBenefitsAsPerBirthDateWidow(birthYearWife, currBeneWife);
                            if (currAnnualBenefitsWife / currBeneWife != 12)
                            {
                                if (currAnnualBenefitsWife != 0 && currBeneWife != 0)
                                {
                                    Globals.Instance.WifeNumberofYears = ageWife;
                                    Globals.Instance.WifeNumberofMonthsBelowGrid = Convert.ToInt16(currAnnualBenefitsWife / currBeneWife);
                                    Globals.Instance.WifeNumberofMonthsAboveGrid = (12 - Globals.Instance.WifeNumberofMonthsBelowGrid);
                                    BoolWifeAgePDF = true;
                                }
                                if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                                    newCombinedOncomeAgeDivorce = (ageWife + 1).ToString();
                            }
                        }
                        else
                        {
                            currAnnualBenefitsWife = annualBenefitsAsPerBirthDate(birthYearWife, currBeneWife);
                            if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                                newCombinedOncomeAgeDivorce = (ageWife + 1).ToString();
                        }
                    }
                    else if ((intAgeWife >= 62) && (currBeneWife != 0) && (Globals.Instance.strWidowStarting == Constants.spousal) && (wifeFirstPayAge == years) && (ageWife == years) && (remainingMonths > 0) && (remainingMonths != 12))
                    {
                        currBeneAnnual = currBene * (12m - remainingMonths);
                        if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                            newCombinedOncomeAgeDivorce = (ageWife + 1).ToString();
                    }
                    //loop fro when the fra has number of months and first pay age is graeter than fra than 1 yrs
                    else if ((intCurrentAgeWifeMonth >= 1) && (!BoolWifeAgePDF) && (intCurrentAgeWifeMonth != 12) && (intCurrentAgeWifeMonth != 0) && (wifeFirstPayAge == ageWife) && (wifeFRA <= wifeFirstPayAge) && (ageWife < 70) && (currBeneWife != 0))
                    {
                        int intDiff = 0;
                        int tempFRA = 0;
                        tempFRA = Convert.ToInt16(customer.WidowFRA(birthYearWife).Substring(0, 2));

                        if (tempFRA < wifeFirstPayAge)
                            intDiff = wifeFirstPayAge - tempFRA;
                        else
                            intDiff = tempFRA - wifeFirstPayAge;
                        if (intDiff >= 0 && intDiff <= 1)
                        {
                            currAnnualBenefitsWife = 0;
                            if (intDiff == 1 && wifeFirstPayAge - 1 == tempFRA && tempFRA >= 62)
                            {
                                //currAnnualBenefitsWife = currBeneWife * ( 12 + (12 - intCurrentAgeWifeMonth));
                                //Globals.Instance.WifeNumberofMonthinNote = (12 + (12 - intCurrentAgeWifeMonth));
                                Globals.Instance.WifeNumberofMonthsinSteps = (intCurrentAgeWifeMonth);
                                Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = intCurrentAgeWifeYear;
                                BoolWifeAgePDF = true;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                            }

                        }
                    }
                    else if ((intCurrentAgeWifeMonth >= 1) && (!BoolWifeAgePDF) && (intCurrentAgeWifeMonth != 12) && (intCurrentAgeWifeMonth != 0) && (wifeFirstPayAge == ageWife) && (intCurrentAgeWifeYear <= wifeFirstPayAge) && (ageWife < 70) && (currBeneWife != 0))
                    {
                        int intDiff = 0;
                        if (intCurrentAgeWifeYear > wifeFirstPayAge)
                            intDiff = intCurrentAgeWifeYear - wifeFirstPayAge;
                        else
                            intDiff = wifeFirstPayAge - intCurrentAgeWifeYear;
                        if (intDiff >= 0 && intDiff <= 1 && soloType != calc_solo.Divorced)
                        {
                            currAnnualBenefitsWife = 0;
                            if (intDiff == 1 && wifeFirstPayAge - 1 == intCurrentAgeWifeYear && intCurrentAgeWifeYear >= 62)
                            {
                                currAnnualBenefitsWife = currBeneWife * (12 + (12 - intCurrentAgeWifeMonth));
                                Globals.Instance.WifeNumberofMonthinNote = (12 + (12 - intCurrentAgeWifeMonth));
                                Globals.Instance.WifeNumberofMonthsinSteps = (intCurrentAgeWifeMonth);
                                Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = intCurrentAgeWifeYear;
                                BoolWifeAgePDF = true;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                            }
                            else
                            {
                                if (wifeFirstPayAge == intCurrentAgeWifeYear && soloType != calc_solo.Divorced)
                                {
                                    currAnnualBenefitsWife = currBeneWife * (12 - intCurrentAgeWifeMonth);
                                    Globals.Instance.WifeNumberofMonthinNote = (12 - intCurrentAgeWifeMonth);
                                    Globals.Instance.WifeNumberofMonthsinSteps = (intCurrentAgeWifeMonth);
                                    Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = ageWife;
                                    BoolWifeAgePDF = true;
                                    Globals.Instance.BoolWifeAgeAdjusted = true;
                                }
                            }
                        }
                    }

                    if (currAnnualBenefitsWife == 0)
                    {
                        currBeneAnnual = currBene * 12;
                    }
                    else
                    {
                        currBeneAnnual = currAnnualBenefitsWife;
                        currAnnualBenefitsWife = 0;
                        if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                            newCombinedOncomeAgeDivorce = (ageWife + 1).ToString();
                    }
                    #endregion Code to update Annual benefits in grid

                    #region Code to calculate the cummulative benefits before and after younger age 70
                    if (aIx == 70)
                    {
                        currBeneWifeLocal = currBeneWife;
                    }
                    if (aIx >= 75 && aIx <= 90)
                    {
                        if (aIx == 75)
                        {
                            decimal grandTotal = 0;
                            for (int i = 0; i < 5; i++)
                            {
                                currBeneWifeLocal = ColaUp(63, currBeneWifeLocal);
                                decimal total = (currBeneWifeLocal * 12);
                                grandTotal += Convert.ToInt32(total);
                            }
                            cummBene += grandTotal;
                        }
                        else
                        {
                            decimal grandTotal = 0;
                            for (int i = 0; i < 5; i++)
                            {
                                currBeneWifeLocal = ColaUp(63, currBeneWifeLocal);
                                decimal total = (currBeneWifeLocal * 12);
                                grandTotal += Convert.ToInt32(total);
                            }
                            cummBene += grandTotal;
                        }
                    }
                    if (aIx <= 70)
                    {
                        cummBene += currBeneAnnual;
                    }
                    #endregion Code to calculate the cummulative benefits before and after younger age 70
                    cummBeneWife += currBeneWife;
                    //  Set flag if this is the high wage earner's life expectancy year
                    string ageCol = aIx.ToString();
                    //  For selected ages, build the display row
                    if (youngest <= 90)// ((aIx <= topAix) || (aIx == topAix + 10) || showThisLine || didAutoAdjust)
                    {
                        System.Data.DataRow incomeRow = incomeTab.NewRow();
                        // Date : 20112014
                        // Description : Add Extra Column "YEAR" in Grid.
                        //incomeRow[(int)disp_cols.Year] = year;
                        incomeRow[(int)disp_cols.Age] = ageCol;
                        if (currBene > 0)
                        {
                            if (BoolWifeAgePDF)
                            {
                                int intDiff = 0;
                                int intTempFRA = 0;
                                int intMultiplyMonth = 0;
                                intTempFRA = Convert.ToInt16(customer.WidowFRA(birthYearWife).Substring(0, 2));
                                if (wifeFirstPayAge > intTempFRA)
                                    intDiff = wifeFirstPayAge - intTempFRA;
                                else
                                    intDiff = intTempFRA - wifeFirstPayAge;

                                if (intDiff == 1)
                                {
                                    if (currBeneWife != 0)
                                    {
                                        if (intCurrentAgeWifeYear < 62 && birthYearWife <= 1962)
                                            intMultiplyMonth = 12 - customer.WidowFRAMonths(birthYearWife);
                                        else
                                            intMultiplyMonth = Convert.ToInt16(currBeneAnnual / currBeneWife) - 12;
                                    }
                                    currBeneAnnual = currBeneWife * intMultiplyMonth;
                                    cummBene = currBeneAnnual;

                                    //Update the previous row with new values
                                    incomeTab.Rows[incomeTab.Rows.Count - 1][(int)disp_cols.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);
                                    incomeTab.Rows[incomeTab.Rows.Count - 1][2] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + "#";
                                    incomeTab.Rows[incomeTab.Rows.Count - 1][(int)disp_cols.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);
                                    Globals.Instance.WifeNumberofMonthinNote = intMultiplyMonth;
                                    Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = Convert.ToInt16(incomeTab.Rows[incomeTab.Rows.Count - 1][(int)disp_cols.Age]);

                                    currBeneWife = currBeneWife * 1.025m;
                                    currBeneAnnual = currBeneWife * 12;
                                    cummBene = currBeneAnnual + cummBene;

                                    //Update the current row with new values
                                    incomeRow[2] = currBeneAnnual.ToString(Constants.AMT_FORMAT);
                                    BoolWifeAgePDF = false;
                                    incomeRow[(int)disp_cols.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);
                                    incomeRow[(int)disp_cols.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);
                                }
                                else
                                {
                                    incomeRow[2] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + "#";
                                    BoolWifeAgePDF = false;
                                    incomeRow[(int)disp_cols.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);
                                    incomeRow[(int)disp_cols.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);
                                }
                            }
                            else
                            {
                                incomeRow[(int)disp_cols.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);
                                incomeRow[(int)disp_cols.AnnInc] = currBeneAnnual.ToString(Constants.AMT_FORMAT);
                                incomeRow[(int)disp_cols.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);
                            }
                        }
                        else
                        {
                            incomeRow[(int)disp_cols.WifeInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_cols.AnnInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_cols.CummInc] = Constants.ZeroWithOutAsterisk;
                        }
                        incomeTab.Rows.Add(incomeRow);
                    }
                    #endregion Code to update the totals in the Grid

                    #region Code to itterate the loop for younger before and afetr age 70
                    // Date : 20112014
                    // Description : Add Extra Column "YEAR" in Grid.
                    //incYr++;
                    if (aIx >= 70)
                    {
                        aIx = aIx + 5;
                        oldest = oldest + 5;
                        youngest = youngest + 5;
                        currBeneWife = ColaUp(67, currBeneWife);
                    }
                    else
                    {
                        oldest++;
                        aIx++;
                        youngest++;
                        currBeneWife = ColaUp(63, currBeneWife);
                    }
                    // currBeneWife = ColaUp(63, currBeneWife);
                    // Date : 20112014
                    // Description : Add Extra Column "YEAR" in Grid.
                    //incYr++;
                }
                //  -------------
                //  Finalize Grid
                //  -------------
                Grid1.DataSource = incomeSet;
                Grid1.DataBind();
                //Grid1.CssClass = "gridWidth";
                TableItemStyle tisA = Grid1.AlternatingRowStyle;
                //tisA.BackColor = Color.LightGray;
                //Grid1.BorderStyle = Constants.STD_BORDER;
                //Grid1.BorderWidth = Constants.STD_BORDER_WIDTH;
                int cummTotal = 0;
                #endregion Code to itterate the loop for younger before and afetr age 70

                #region Code to highlight the grid values

                //  ---------------------------------
                //  HIGHLIGHT cells to match the book
                //  ---------------------------------

                // Use high earner's FRA to trigger cell coloring
                int highEarnerFRA = wifeFRA;
                string fraYear = highEarnerFRA.ToString();
                string fraYear3 = (69).ToString();
                string fraYear4 = (70).ToString();
                string fraYear14 = (80).ToString();

                foreach (GridViewRow gvr in Grid1.Rows)
                {
                    // Date : 20112014
                    // Description : Add Extra Column "YEAR" in Grid.
                    // gvr.Cells[(int)disp_cols.Year].BorderColor = LOW_GREY;
                    //if (soloType.ToString() == Constants.Widowed)
                    //{
                    //    if ((gvr.Cells[(int)disp_col.Age].Text.ToString().Equals(Constants.Number69)) && (gridname.Equals(Constants.WClaim67)))
                    //    {
                    //        WCummValue = decimal.Parse(gvr.Cells[3].Text.Substring(1));
                    //    }
                    //}

                    //Added when FRA age is in month (Addded on 21/09/2016)
                    string WifraAtFra = Convert.ToString(customer.WidowFRA(birthYearWife)).Substring(0, 2);

                    if (gvr.Cells[0].Text.Equals(WifraAtFra.ToString()))
                    {
                        SValueStarting = decimal.Parse(gvr.Cells[1].Text.Substring(1));
                        SAgeValue = gvr.Cells[0].Text;
                    }
                    if (gvr.Cells[0].Text.Equals(Constants.Number70))
                    {
                        AgeValueAt70 = decimal.Parse(gvr.Cells[1].Text.Substring(1));
                        AgeAtLastChange = gvr.Cells[0].Text;
                        WidowAnnualIncomeValue = decimal.Parse(gvr.Cells[2].Text.Substring(1));
                    }
                    gvr.Cells[(int)disp_cols.Age].BorderColor = LOW_GREY;
                    gvr.Cells[(int)disp_cols.WifeInc].BorderColor = LOW_GREY;
                    gvr.Cells[(int)disp_cols.AnnInc].BorderColor = LOW_GREY;
                    gvr.Cells[(int)disp_cols.CummInc].BorderColor = LOW_GREY;

                    string rowAge = gvr.Cells[(int)disp_cols.Age].Text;
                    if (rowAge == fraYear) //  66
                    {
                        if (gvr.Cells[(int)disp_cols.WifeInc].Text != Constants.ZeroWithOutAsterisk)
                        {
                            //gvr.Cells[(int)disp_cols.WifeInc].CssClass = HIGHLIGHT_CLASS;
                            //gvr.Cells[(int)disp_cols.AnnInc].CssClass = HIGHLIGHT_CLASS;
                        }
                    }

                    if (rowAge == fraYear3) //  69
                    {
                        if (gvr.Cells[(int)disp_cols.CummInc].Text != Constants.ZeroWithOutAsterisk)
                        {
                            //gvr.Cells[(int)disp_cols.CummInc].CssClass = HIGHLIGHT_CLASS;
                            string[] cummBucks = gvr.Cells[(int)disp_cols.CummInc].Text.Substring(1).Split(',');
                            cummTotal = int.Parse(string.Join(string.Empty, cummBucks));
                        }
                    }

                    if (rowAge == fraYear4) //  70
                    {
                        gvr.Cells[(int)disp_cols.WifeInc].CssClass = HIGHLIGHT_CLASS;
                        string[] combBucks = gvr.Cells[(int)disp_cols.WifeInc].Text.Substring(1).Split(',');
                        age70CombBene = int.Parse(string.Join(string.Empty, combBucks));

                        gvr.Cells[(int)disp_cols.AnnInc].CssClass = HIGHLIGHT_CLASS;

                        string[] wifeBucks = gvr.Cells[(int)disp_cols.WifeInc].Text.Substring(1).Split(',');
                        gvr.Cells[(int)disp_cols.WifeInc].CssClass = Constants.HIGHLIGHT_70;
                        age70HighBene = int.Parse(string.Join(string.Empty, wifeBucks));
                    }
                    if (rowAge == fraYear14) //  80
                    {
                        string[] combBucks = gvr.Cells[(int)disp_cols.WifeInc].Text.Substring(1).Split(',');
                        age80CombBene = int.Parse(string.Join(string.Empty, combBucks));
                    }
                }
                #endregion Code to highlight the grid values

                return cummTotal;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// Determine the surivor's benefit based on age and birth year
        /// </summary>
        /// <param name="survAge"></param>
        /// <param name="amt"></param>
        /// <param name="survBirthYear"></param>
        /// <param name="decAge"></param>
        /// <param name="decBirthYear"></param>
        /// <returns></returns>
        protected decimal SurvivorBene(int survAge, decimal amt, int survBirthYear, int decAge, int decBirthYear)
        {
            try
            {
                #region If the decedant lives past FRA, the 8%s can accumulate

                // If the decedant lives past FRA, the 8%s can accumulate
                switch (decAge)
                {
                    case 67:
                        if (decBirthYear < 1958)
                            amt = amt * 1.08m;
                        else
                            amt = amt * 1.00m;
                        break;
                    case 68:
                        if (decBirthYear < 1958)
                            amt = amt * 1.16m;
                        else
                            amt = amt * 1.08m;
                        break;
                    case 69:
                        if (decBirthYear < 1958)
                            amt = amt * 1.24m;
                        else
                            amt = amt * 1.16m;
                        break;
                    case 70:
                        if (decBirthYear < 1958)
                            amt = amt * 1.32m;
                        else
                            amt = amt * 1.24m;
                        break;
                    default:
                        break;
                }
                #endregion

                #region Set factors according to SSA rules

                //  Set factors according to SSA rules
                switch (survAge)
                {
                    case 60:
                        result = (amt * .715m);
                        break;
                    case 61:
                        switch (survBirthYear)
                        {
                            case 1957: result = amt - (amt * .239m); break;
                            case 1958: result = amt - (amt * .240m); break;
                            case 1959: result = amt - (amt * .241m); break;
                            case 1960: result = amt - (amt * .242m); break;
                            case 1961: result = amt - (amt * .243m); break;
                            default:
                                if (survBirthYear < 1957) result = amt - (amt * .237m);
                                if (survBirthYear > 1961) result = amt - (amt * .244m);
                                break;
                        }
                        break;
                    case 62:
                        switch (survBirthYear)
                        {
                            case 1957: result = amt - (amt * .193m); break;
                            case 1958: result = amt - (amt * .195m); break;
                            case 1959: result = amt - (amt * .197m); break;
                            case 1960: result = amt - (amt * .199m); break;
                            case 1961: result = amt - (amt * .202m); break;
                            default:
                                if (survBirthYear < 1957) result = amt - (amt * .190m);
                                if (survBirthYear > 1961) result = amt - (amt * .204m);
                                break;
                        }
                        break;
                    case 63:
                        switch (survBirthYear)
                        {
                            case 1957: result = amt - (amt * .146m); break;
                            case 1958: result = amt - (amt * .150m); break;
                            case 1959: result = amt - (amt * .153m); break;
                            case 1960: result = amt - (amt * .157m); break;
                            case 1961: result = amt - (amt * .160m); break;
                            default:
                                if (survBirthYear < 1957) result = amt - (amt * .142m);
                                if (survBirthYear > 1961) result = amt - (amt * .163m);
                                break;
                        }
                        break;
                    case 64:
                        switch (survBirthYear)
                        {
                            case 1957: result = amt - (amt * .100m); break;
                            case 1958: result = amt - (amt * .105m); break;
                            case 1959: result = amt - (amt * .110m); break;
                            case 1960: result = amt - (amt * .114m); break;
                            case 1961: result = amt - (amt * .118m); break;
                            default:
                                if (survBirthYear < 1957) result = amt - (amt * .095m);
                                if (survBirthYear > 1961) result = amt - (amt * .122m);
                                break;
                        }
                        break;
                    case 65:
                        switch (survBirthYear)
                        {
                            case 1957: result = amt - (amt * .054m); break;
                            case 1958: result = amt - (amt * .060m); break;
                            case 1959: result = amt - (amt * .066m); break;
                            case 1960: result = amt - (amt * .071m); break;
                            case 1961: result = amt - (amt * .076m); break;
                            default:
                                if (survBirthYear < 1957) result = amt - (amt * .047m);
                                if (survBirthYear > 1961) result = amt - (amt * .081m);
                                break;
                        }
                        break;
                    case 66:
                        switch (survBirthYear)
                        {
                            case 1957: result = amt - (amt * .008m); break;
                            case 1958: result = amt - (amt * .015m); break;
                            case 1959: result = amt - (amt * .022m); break;
                            case 1960: result = amt - (amt * .028m); break;
                            case 1961: result = amt - (amt * .035m); break;
                            default:
                                if (survBirthYear < 1957) result = amt - (amt * .000m);
                                if (survBirthYear > 1961) result = amt - (amt * .041m);
                                break;
                        }
                        break;
                    default:
                        result = amt;
                        break;
                }
                return result;
                #endregion
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// code to build grid values for early claim and suspend and Claim Early Claim Late
        /// </summary>
        /// <param name="Grid1"></param>
        /// <param name="strategy"></param>
        /// <param name="startAge"></param>
        /// <param name="gridNote"></param>
        /// <returns></returns>
        public int BuildGrid(GridView Grid1, calc_strategy strategy, calc_start startAge, ref System.Web.UI.WebControls.Label gridNote)
        {
            try
            {
                #region to declare variables and set base age
                #region CODE TO CALCULATE THE AGE AND SET THE FRA YEAR AND FIRST PAY YEAR
                if ((birthYearWife == 1959 || birthYearWife == 1958) && (birthYearHusb == 1959 || birthYearHusb == 1958))
                {
                    wifeFRA = 66;
                    husbFRA = 66;
                }
                else if (birthYearWife == 1959 || birthYearWife == 1958)
                {
                    husbFRA = Convert.ToInt32(FRA(birthYearHusb));
                    wifeFRA = 66;
                }
                else if (birthYearHusb == 1959 || birthYearHusb == 1958)
                {
                    wifeFRA = Convert.ToInt32(FRA(birthYearWife));
                    husbFRA = 66;
                }
                else
                {
                    husbFRA = Convert.ToInt32(FRA(birthYearHusb));
                    wifeFRA = Convert.ToInt32(FRA(birthYearWife));

                }
                #endregion CODE TO CALCULATE THE AGE AND SET THE FRA YEAR AND FIRST PAY YEAR
                cumm65 = 0;
                cumm80 = 0;

                //Restricted Application Strategy Income
                Globals.Instance.intRestrictAppIncomeBestHusb = 0;
                Globals.Instance.intRestrictAppIncomeEarlyHusb = 0;
                Globals.Instance.intRestrictAppIncomeBestWife = 0;
                Globals.Instance.intRestrictAppIncomeEarlyWife = 0;


                badStrat = false;
                int baseAge = 62;
                switch (startAge)
                {
                    case calc_start.asap62:
                        baseAge = 62;
                        break;
                    case calc_start.fra6667:
                        baseAge = Math.Min(husbFRA, wifeFRA);
                        break;
                    case calc_start.max70:
                        baseAge = 70;
                        break;
                }
                #endregion to declare variables and set base age

                #region to build the column headrers for the grids
                disp_col dispCol;
                gridNote.Text = string.Empty;
                string sWifeInitials = Your_Name.Substring(0, 1);
                string sHusbInitials = Spouse_Name.Substring(0, 1);
                // Build dataset
                System.Data.DataSet incomeSet = new System.Data.DataSet();
                System.Data.DataTable incomeTab = incomeSet.Tables.Add("incomeTab");
                // Initialize grid
                Grid1.Columns.Clear();
                Grid1.AutoGenerateColumns = false;
                // Build Display Columns and Dataset Columns
                var values = (disp_col[])Enum.GetValues(typeof(disp_col));
                for (int dcIx = 0; dcIx < values.Length; dcIx++)
                {
                    dispCol = (disp_col)dcIx;
                    incomeTab.Columns.Add(dispCol.ToString());
                    BoundField bf = new BoundField();
                    bf.HtmlEncode = false;
                    TableItemStyle tis = Grid1.HeaderStyle;
                    switch (dispCol)
                    {
                        //case disp_col.Year:
                        //    bf.HeaderText = "Year";
                        //    break;
                        case disp_col.Age:
                            //if (intAgeWife == intAgeHusb)
                            //    bf.HeaderText = "Age";
                            //else
                            bf.HeaderText = Constants.HeaderA + Constants.HeaderBreak + Constants.Age + Environment.NewLine + Constants.OpeningBracket + sWifeInitials + Constants.BackSlash + sHusbInitials + Constants.CloseingBracket;
                            break;
                        case disp_col.WifeInc:
                            bf.HeaderText = Constants.HeaderB + Constants.HeaderBreak + Your_Name + Constants.MonthlyIncome;
                            break;
                        case disp_col.HusbInc:
                            bf.HeaderText = Constants.HeaderC + Constants.HeaderBreak + Spouse_Name + Constants.MonthlyIncome;
                            break;
                        case disp_col.CombInc:
                            bf.HeaderText = Constants.HeaderD + Constants.HeaderBreak + Constants.CombinedMonthlyIncome;
                            break;
                        case disp_col.AnnInc:
                            bf.HeaderText = Constants.HeaderE + Constants.HeaderBreak + Constants.CombinedAnnualIncome;
                            break;
                        case disp_col.CummInc:
                            bf.HeaderText = Constants.HeaderF + Constants.HeaderBreak + Constants.CumulativeAnnualIncome;
                            break;
                        case disp_col.SurvInc:
                            bf.HeaderText = Constants.SurvivorMonthlyIncome;
                            break;
                    }
                    // Column details
                    bf.DataField = dispCol.ToString();
                    bf.ReadOnly = true;
                    // Skip the survivor column if not requested
                    if ((showSurvivor) || (dcIx != values.Length - 1))
                    {
                        Grid1.Columns.Add(bf);
                    }
                }
                incomeTab.Columns.Add("Note");
                #endregion to build the column headrers for the grids

                #region to set all local variables
                //  --------------------------------------------------------------
                //  Populate rows
                //  --------------------------------------------------------------
                // Date : 20112014
                // Description : Add Extra Column "YEAR" in Grid.
                //int year, incYr = 0;
                int ageHusb, ageWife, ageDiff;
                int husbFirstPayAge = 0, wifeFirstPayAge = 0;
                age70SurvBene = 0;
                age70SurvBeneAge = string.Empty;
                decimal currBene = 0, currBeneWife = 0, currBeneHusb = 0, cummBeneWife = 0, cummBeneHusb = 0, cummBene = 0, currBeneAnnual = 0, currAnnualBenefitsWife = 0, currAnnualBenefitsHusb = 0;
                bool needAsterisk = false;
                bool boolToRunLoopOnlyOnce = true;
                bool boolToRunLoopOnlyOnce1 = true;
                bool wifeStartingDeduction = false;
                bool husbStartingDeduction = false;
                benefit_source wifeBenefitSource = benefit_source.None;
                benefit_source husbBenefitSource = benefit_source.None;
                int youngest = Math.Min(intAgeHusb, intAgeWife);
                int oldest = Math.Max(intAgeHusb, intAgeWife);
                int ageSpan = oldest - youngest;
                if (youngest == intAgeWife && decBeneHusb > decBeneWife)
                    ageDiff = ageSpan; // most common case
                else
                    ageDiff = 0 - ageSpan; // inverted
                int topAix = 0;
                breakEvenAmt = new int[100];
                breakEvenAge = new string[100];
                int highBE = 0;
                //  Determine stopping point - when the higher earner hits 70
                if (decBeneHusb > decBeneWife)
                    if (intAgeHusb < intAgeWife)
                        topAix = 70;
                    else
                        topAix = 70 - ageSpan;
                else
                    if (intAgeWife < intAgeHusb)
                    topAix = 70;
                else
                    topAix = 70 - ageSpan;
                //  Support a start point for standard strategies 
                if ((baseAge > 64) && (strategy == calc_strategy.Standard))
                    topAix = Math.Max(topAix, baseAge);
                //  Ths adjustment insures the loop continues until younger is 70:
                if (ageSpan > 8)
                    topAix = topAix + ageSpan - 8;
                youngest = 62 - ageSpan;
                #endregion to set all local variables

                #region to build both the strategies early claim and suspend/Claim Now, Claim More Later
                //  Build the data rows
                for (int aIx = youngest; aIx <= 90;)//(int aIx = 62 - ageSpan; aIx < 99; aIx++)
                {
                    // Date : 20112014
                    // Description : Add Extra Column "YEAR" in Grid.
                    // Set year
                    //year = birthYearWife + 62 + incYr;
                    //year = birthYearWife + 62;

                    // Set ages - aIx is the lower of the two ages
                    ageWife = aIx;
                    ageHusb = aIx;
                    if (intAgeWife < intAgeHusb)
                        ageHusb = aIx + intAgeHusb - intAgeWife;
                    else
                        ageWife = aIx + intAgeWife - intAgeHusb;
                    #region code to get values for husband and wife at age 62
                    //  Set initial non-spousal payout amount 
                    if ((currBeneWife == 0)     /* don't change if started */
                    && (ageWife >= baseAge)     /* don't start before begin point for this chart */
                    && (ageWife >= 62)          /* minimum SS age */
                    && (ageWife >= intAgeWife)) /* watch out for current age > 62 */
                        // Do it for standard calcs and lower-earner in spousal/suspend
                        if ((strategy == calc_strategy.Standard)
                        || (strategy == calc_strategy.Spousal && decBeneWife <= decBeneHusb)
                        || (strategy == calc_strategy.Suspend && decBeneWife <= decBeneHusb))
                        {
                            currBeneWife = startFactor(ageWife, decBeneWife, birthYearWife);
                            //code to check if age is greater than 62
                            if (intAgeWife >= 62)
                            {
                                //code to check for age greater than 68
                                if (intAgeWife > wifeFRA)
                                {
                                    //give same PIA as the benefits no COlA
                                    currBeneWife = decBeneWife;
                                }
                                else
                                {
                                    //calculate the benefis including COLA
                                    currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intAgeWife, wifeFRA);
                                }
                            }
                            decimal spousalBenefits = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                            spousalBenefits = spousalBenefits / 2;
                            decimal spousalBenefitsWife = 0;

                            if (ageWife == 62)
                            {
                                spousalBenefitsWife = SpousalBenefitsWifeAtHusbAge62(ageHusb, ageWife, currBeneWife);
                            }
                            else
                            {
                                spousalBenefitsWife = SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                            }

                            decimal spousalBenefitsWif70 = ColaUpSpousal(ageHusb, spousalBenefitsWife, birthYearHusb, intAgeHusb, husbFRA);
                            decimal WHBBenefitsWif70 = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intAgeWife, wifeFRA);

                            if (currBeneHusb < spousalBenefits && husbBirthDate < limit1 && ageHusb >= husbFRA)
                            {
                                currBeneHusb = spousalBenefits;
                                husbBenefitSource = benefit_source.Spouse;
                                Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                            }

                            if (currBeneWife < spousalBenefitsWife && husbBenefitSource == benefit_source.Self && spousalBenefitsWif70 > WHBBenefitsWif70)
                            {
                                currBeneWife = spousalBenefitsWife;
                                wifeBenefitSource = benefit_source.Spouse;
                                wifeFirstPayAge = ageWife;
                                wifeFirstPay = ageHusb;
                                Globals.Instance.BoolSpousalCliamedWifeSt1 = true;
                                Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                            }
                            else
                            {
                                wifeBenefitSource = benefit_source.Self;
                                Globals.Instance.WifeStartingBenefitName = Constants.Working;
                                wifeFirstPayAge = ageWife;
                                wifeFirstPay = ageHusb;
                            }
                        }
                    // Repeat for husband
                    if ((currBeneHusb == 0)
                    && (ageHusb >= baseAge)
                    && (ageHusb >= 62)
                    && (ageHusb >= intAgeHusb))
                        if ((strategy == calc_strategy.Standard)
                        || (strategy == calc_strategy.Spousal && decBeneWife > decBeneHusb)
                        || (strategy == calc_strategy.Suspend && decBeneWife > decBeneHusb))
                        {
                            currBeneHusb = startFactor(ageHusb, decBeneHusb, birthYearHusb);
                            //code to check if age is greater than 62
                            if (intAgeHusb >= 62)
                            {
                                //code to check for age greater than 68
                                if (intAgeHusb > husbFRA)
                                {
                                    //give same PIA as the benefits no COlA
                                    currBeneHusb = decBeneHusb;
                                }
                                else
                                {
                                    //calculate the benefis including COLA
                                    currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                                }
                            }
                            decimal spousalBenefits = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                            spousalBenefits = spousalBenefits / 2;
                            decimal spousalBenefitsHusb = 0;
                            decimal spousalBenefitsHusb70 = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                            decimal WHBBenefitsHusb70 = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                            if (ageHusb == 62)
                            {
                                spousalBenefitsHusb = SpousalBenefitsHusbAtWifeAge62(ageHusb, ageWife, currBeneHusb);
                            }
                            else
                            {
                                spousalBenefitsHusb = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                            }

                            if (currBeneHusb < spousalBenefitsHusb && wifeBenefitSource == benefit_source.Self && spousalBenefitsHusb70 > WHBBenefitsHusb70)
                            {
                                currBeneHusb = spousalBenefitsHusb;
                                husbBenefitSource = benefit_source.Spouse;
                                husbFirstPayAge = ageHusb;
                                husFirstPay = ageHusb;
                                Globals.Instance.BoolSpousalCliamedHusbSt1 = true;
                                Globals.Instance.HusbStartingBenefitName = Constants.spousal;
                            }
                            else
                            {
                                husbBenefitSource = benefit_source.Self;
                                Globals.Instance.HusbStartingBenefitName = Constants.Working;
                                husbFirstPayAge = ageHusb;
                                husFirstPay = ageHusb;
                            }
                            if (currBeneWife < spousalBenefits && wifeBirthDate < limit1 && ageWife >= wifeFRA)
                            {
                                currBeneWife = spousalBenefits;
                                wifeBenefitSource = benefit_source.Spouse;
                                Globals.Instance.WifeStartingBenefitName = Constants.spousal;
                            }
                        }
                    #endregion #region code to get values for husband and wife at age 62

                    #region code to get spousal values for husband and wife at age 66
                    //  Spousal strategy changes at age 66 
                    if (strategy == calc_strategy.Spousal)
                    {
                        decimal SpousalBenefits;
                        if ((decBeneWife <= decBeneHusb) && (cummBeneHusb == 0) && (currBeneWife > 0) && (ageHusb >= intAgeHusb) && (ageHusb >= husbFRA) && (intAgeHusb >= baseAge))
                        {
                            decimal workingBenefitsat70 = startFactorMarridSuspend(70, decBeneHusb, birthYearHusb);
                            workingBenefitsat70 = ColaUpSpousal(ageHusb, workingBenefitsat70, birthYearHusb, intAgeHusb, husbFRA);

                            decimal spousalBenefits = ColaUpSpousal(70, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                            spousalBenefits = spousalBenefits / 2;

                            //code to check if age greater than 68
                            if (intAgeWife > wifeFRA)
                            {
                                //to get back to fRA amount deducting the delayed credits for wife
                                multiply = AgeAbove68YearsOfWife();
                                SpousalBenefits = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                            }
                            else
                            {
                                SpousalBenefits = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                            }

                            if (workingBenefitsat70 > spousalBenefits)
                            {
                                if (husbBirthDate < limit1)
                                {
                                    currBeneHusb = SpousalBenefits / 2;
                                    husbFirstPayAge = ageHusb;
                                    husbBenefitSource = benefit_source.Spouse;
                                    Globals.Instance.BoolSpousalAtFRAAgeCliamedHusb = true;
                                }
                            }
                            else
                            {
                                currBeneHusb = SpousalBenefits / 2;
                                husbFirstPayAge = ageHusb;
                                husbBenefitSource = benefit_source.Spouse;
                                Globals.Instance.BoolSpousalAtFRAAgeCliamedHusb = true;
                            }

                        }
                        if ((decBeneWife > decBeneHusb) && (cummBeneWife == 0) && (currBeneHusb > 0) && (ageWife >= intAgeWife) && (ageWife >= wifeFRA) && (intAgeWife >= baseAge))
                        {
                            decimal workingBenefitsat70 = startFactorMarridSuspend(70, decBeneWife, birthYearWife);
                            workingBenefitsat70 = ColaUpSpousal(ageWife, workingBenefitsat70, birthYearHusb, intAgeWife, wifeFRA);

                            decimal spousalBenefits = ColaUpSpousal(70, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                            spousalBenefits = spousalBenefits / 2;

                            //code to check if age greater than 68
                            if (intAgeHusb > husbFRA)
                            {
                                //to get back to fRA amount deducting the delayed credits for husb
                                multiply = AgeAbove68YearsOfHusb();
                                SpousalBenefits = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                            }
                            else
                            {
                                SpousalBenefits = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                            }
                            if (workingBenefitsat70 > spousalBenefits)
                            {
                                if (wifeBirthDate < limit1)
                                    currBeneWife = SpousalBenefits / 2;
                                Globals.Instance.BoolSpousalAtFRAAgeCliamedWife = true;
                            }
                            else
                            {
                                currBeneWife = SpousalBenefits / 2;
                                Globals.Instance.BoolSpousalAtFRAAgeCliamedWife = true;
                            }
                            wifeFirstPayAge = ageWife;
                            wifeBenefitSource = benefit_source.Spouse;
                        }
                    }
                    #endregion code to get spousal values for husband and wife at age 66

                    #region Code to working benefits at age 70 for husband and wife
                    //  Spousal and Suspend strategy changes at age 70
                    if ((strategy == calc_strategy.Spousal) || (strategy == calc_strategy.Suspend))
                    {
                        spanHusb = DateTime.Now - husbBirthDate;
                        intHusbAge = Convert.ToInt32(spanHusb.TotalDays / 365.2425);
                        spanWife = DateTime.Now - wifeBirthDate;
                        intWifeAge = Convert.ToInt32(spanWife.TotalDays / 365.2425);

                        if ((decBeneWife <= decBeneHusb) && (ageHusb == 70) && husbBenefitSource != benefit_source.Self)
                        {
                            decimal spousalBenefits;
                            //code to check if age greater than 68
                            if (intAgeHusb > husbFRA)
                            {
                                //code to get the fra age value for husb
                                multiply = AgeAbove68YearsOfHusb();
                                //code to delayed credits
                                currBeneHusb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                if (ageWife == 62)
                                {
                                    spousalBenefits = SpousalBenefitsWifeAtHusbAge62(ageHusb, ageWife, currBeneWife);
                                }
                                else
                                {
                                    spousalBenefits = SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                }
                                //decimal spousalBenefits = SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                if (currBeneWife < spousalBenefits && wifeBenefitSource == benefit_source.Self)
                                {
                                    currBeneWife = spousalBenefits;
                                    fBuildGridat70SpousalWife = true;
                                    AgeValueForSpousalAt70 = ageWife;
                                    ValueForWifeAt70 = currBeneWife;
                                    Globals.Instance.BoolSpousalChangeAtAge70 = true;
                                    wifeBenefitSource = benefit_source.Spouse;
                                    Globals.Instance.BoolSpousalAfterWorkWife = true;
                                }
                                else
                                {
                                    Globals.Instance.BoolSpousalChangeAtAge70 = false;
                                }
                            }
                            else
                            {
                                currBeneHusb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                if (ageWife == 62)
                                {
                                    spousalBenefits = SpousalBenefitsWifeAtHusbAge62(ageHusb, ageWife, currBeneWife);
                                }
                                else
                                {
                                    spousalBenefits = SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                }
                                if (currBeneWife < spousalBenefits && wifeBenefitSource == benefit_source.Self)
                                {
                                    currBeneWife = spousalBenefits;
                                    fBuildGridat70SpousalWife = true;
                                    AgeValueForSpousalAt70 = ageWife;
                                    ValueForWifeAt70 = currBeneWife;
                                    Globals.Instance.BoolSpousalChangeAtAge70 = true;
                                    wifeBenefitSource = benefit_source.Spouse;
                                    Globals.Instance.BoolSpousalAfterWorkWife = true;
                                }
                                else
                                {
                                    Globals.Instance.BoolSpousalChangeAtAge70 = false;
                                }
                            }
                            currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intHusbAge, husbFRA);
                            husbBenefitSource = benefit_source.Self;
                            husbFirstPayAge = ageHusb;
                            husFirstPay = ageHusb;
                            Globals.Instance.BoolWHBHusbat70 = true;
                            //gridNote.Text += "  Husband switched to his own work history benefit at age " + ageHusb.ToString() + ".";
                            buildGridAt70 = true;
                        }

                        if ((decBeneWife > decBeneHusb) && (ageWife == 70) && wifeBenefitSource != benefit_source.Self)
                        {
                            decimal spousalBenefits;
                            //code to check if age greater than 68
                            if (intAgeWife > wifeFRA)
                            {
                                //code to get the fra age value for husb
                                multiply = AgeAbove68YearsOfWife();
                                //code to delayed credits
                                currBeneWife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                // decimal spousalBenefits = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                                if (ageHusb == 62)
                                {
                                    spousalBenefits = SpousalBenefitsHusbAtWifeAge62(ageHusb, ageWife, currBeneHusb);
                                }
                                else
                                {
                                    spousalBenefits = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                                }
                                if (currBeneHusb < spousalBenefits && husbBenefitSource == benefit_source.Self)
                                {
                                    currBeneHusb = spousalBenefits;
                                    fBuildGridat70SpousalHusb = true;
                                    AgeValueForSpousalAt70 = ageHusb;
                                    ValueForWifeAt70 = currBeneHusb;
                                    Globals.Instance.BoolSpousalChangeAtAge70 = true;
                                    husbBenefitSource = benefit_source.Spouse;
                                    Globals.Instance.BoolSpousalAfterWorkHusb = true;
                                }
                                else
                                {
                                    Globals.Instance.BoolSpousalChangeAtAge70 = false;
                                }
                            }
                            else
                            {
                                //Changes made on 30/04/2015 for testing values as per client on call
                                currBeneWife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                if (ageHusb == 62)
                                {
                                    spousalBenefits = SpousalBenefitsHusbAtWifeAge62(ageHusb, ageWife, currBeneHusb);
                                }
                                else
                                {
                                    spousalBenefits = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                                }
                                if (currBeneHusb < spousalBenefits && husbBenefitSource == benefit_source.Self)
                                {
                                    currBeneHusb = spousalBenefits;
                                    fBuildGridat70SpousalHusb = true;
                                    AgeValueForSpousalAt70 = ageHusb;
                                    ValueForWifeAt70 = currBeneHusb;
                                    Globals.Instance.BoolSpousalChangeAtAge70 = true;
                                    husbBenefitSource = benefit_source.Spouse;
                                    Globals.Instance.BoolSpousalAfterWorkHusb = true;
                                }
                                else
                                {
                                    Globals.Instance.BoolSpousalChangeAtAge70 = false;
                                }
                            }
                            currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intWifeAge, wifeFRA);
                            wifeBenefitSource = benefit_source.Self;
                            wifeFirstPayAge = ageHusb;
                            wifeFirstPay = ageHusb;
                            Globals.Instance.BoolWHBWifeat70 = true;
                            //gridNote.Text += "  Wife switched to her own work history benefit at age " + ageWife.ToString() + ".";
                            fBuildGridat70 = true;
                        }
                    }
                    #endregion Code to working benefits at age 70 for husband and wife

                    #region Code to get working benefit at FRA age for user having non-zero benefit and spouse having zero benefit

                    if ((strategy == calc_strategy.ZeroBenefit))
                    {
                        spanHusb = DateTime.Now - husbBirthDate;
                        intHusbAge = Convert.ToInt32(spanHusb.TotalDays / 365.2425);
                        spanWife = DateTime.Now - wifeBirthDate;
                        intWifeAge = Convert.ToInt32(spanWife.TotalDays / 365.2425);

                        if ((decBeneWife <= decBeneHusb) && (ageHusb == 70) && husbBenefitSource != benefit_source.Self)
                        {
                            decimal spousalBenefits;
                            //code to check if age greater than 68
                            if (intAgeHusb > husbFRA)
                            {
                                //code to get the fra age value for husb
                                multiply = AgeAbove68YearsOfHusb();
                                //code to delayed credits
                                currBeneHusb = startFactorMarridSuspend(ageHusb, multiply, birthYearHusb);
                                if (ageWife == 62)
                                {
                                    spousalBenefits = SpousalBenefitsWifeAtHusbAge62(ageHusb, ageWife, currBeneWife);
                                }
                                else
                                {
                                    spousalBenefits = SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                }
                                //decimal spousalBenefits = SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                if (currBeneWife < spousalBenefits)
                                {
                                    currBeneWife = spousalBenefits;
                                    fBuildGridat70SpousalWife = true;
                                    AgeValueForSpousalAt70 = ageWife;
                                    ValueForWifeAt70 = currBeneWife;
                                    Globals.Instance.BoolSpousalChangeAtAge70 = true;
                                    wifeBenefitSource = benefit_source.Spouse;
                                    Globals.Instance.BoolSpousalAfterWorkWife = true;
                                }
                                else
                                {
                                    Globals.Instance.BoolSpousalChangeAtAge70 = false;
                                }
                            }
                            else
                            {
                                currBeneHusb = startFactorMarridSuspend(ageHusb, decBeneHusb, birthYearHusb);
                                if (ageWife == 62)
                                {
                                    spousalBenefits = SpousalBenefitsWifeAtHusbAge62(ageHusb, ageWife, currBeneWife);
                                }
                                else
                                {
                                    spousalBenefits = SpousalBenefitsWifeAtHusbAge70(ageHusb, ageWife, currBeneWife);
                                }
                                if (currBeneWife < spousalBenefits)
                                {
                                    currBeneWife = spousalBenefits;
                                    fBuildGridat70SpousalWife = true;
                                    AgeValueForSpousalAt70 = ageWife;
                                    ValueForWifeAt70 = currBeneWife;
                                    Globals.Instance.BoolSpousalChangeAtAge70 = true;
                                    wifeBenefitSource = benefit_source.Spouse;
                                    Globals.Instance.BoolSpousalAfterWorkWife = true;
                                }
                                else
                                {
                                    Globals.Instance.BoolSpousalChangeAtAge70 = false;
                                }
                            }
                            currBeneHusb = ColaUpSpousal(ageHusb, currBeneHusb, birthYearHusb, intHusbAge, husbFRA);
                            husbBenefitSource = benefit_source.Self;
                            husbFirstPayAge = ageHusb;
                            husFirstPay = ageHusb;
                            //gridNote.Text += "  Husband switched to his own work history benefit at age " + ageHusb.ToString() + ".";
                            buildGridAt70 = true;
                        }

                        if ((decBeneWife > decBeneHusb) && (ageWife == 70) && wifeBenefitSource != benefit_source.Self)
                        {
                            decimal spousalBenefits;
                            //code to check if age greater than 68
                            if (intAgeWife > wifeFRA)
                            {
                                //code to get the fra age value for husb
                                multiply = AgeAbove68YearsOfWife();
                                //code to delayed credits
                                currBeneWife = startFactorMarridSuspend(ageWife, multiply, birthYearWife);
                                // decimal spousalBenefits = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                                if (ageHusb == 62)
                                {
                                    spousalBenefits = SpousalBenefitsHusbAtWifeAge62(ageHusb, ageWife, currBeneHusb);
                                }
                                else
                                {
                                    spousalBenefits = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                                }
                                if (currBeneHusb < spousalBenefits)
                                {
                                    currBeneHusb = spousalBenefits;
                                    fBuildGridat70SpousalHusb = true;
                                    AgeValueForSpousalAt70 = ageHusb;
                                    ValueForWifeAt70 = currBeneHusb;
                                    Globals.Instance.BoolSpousalChangeAtAge70 = true;
                                    husbBenefitSource = benefit_source.Spouse;
                                    Globals.Instance.BoolSpousalAfterWorkHusb = true;
                                }
                                else
                                {
                                    Globals.Instance.BoolSpousalChangeAtAge70 = false;
                                }
                            }
                            else
                            {
                                //Changes made on 30/04/2015 for testing values as per client on call
                                currBeneWife = startFactorMarridSuspend(ageWife, decBeneWife, birthYearWife);
                                if (ageHusb == 62)
                                {
                                    spousalBenefits = SpousalBenefitsHusbAtWifeAge62(ageHusb, ageWife, currBeneHusb);
                                }
                                else
                                {
                                    spousalBenefits = SpousalBenefitsHusbAtWifeAge70(ageHusb, ageWife, currBeneHusb);
                                }
                                if (currBeneHusb < spousalBenefits)
                                {
                                    currBeneHusb = spousalBenefits;
                                    fBuildGridat70SpousalHusb = true;
                                    AgeValueForSpousalAt70 = ageHusb;
                                    ValueForWifeAt70 = currBeneHusb;
                                    Globals.Instance.BoolSpousalChangeAtAge70 = true;
                                    husbBenefitSource = benefit_source.Spouse;
                                    Globals.Instance.BoolSpousalAfterWorkHusb = true;
                                }
                                else
                                {
                                    Globals.Instance.BoolSpousalChangeAtAge70 = false;
                                }
                            }
                            currBeneWife = ColaUpSpousal(ageWife, currBeneWife, birthYearWife, intWifeAge, wifeFRA);
                            wifeBenefitSource = benefit_source.Self;
                            wifeFirstPayAge = ageHusb;
                            wifeFirstPay = ageHusb;
                            //gridNote.Text += "  Wife switched to her own work history benefit at age " + ageWife.ToString() + ".";
                            fBuildGridat70 = true;
                        }

                    }
                    #endregion Code to get working benefit at FRA age for user having non-zero benefit and spouse having zero benefit

                    #endregion to build both the strategies early claim and suspend/Claim Now, Claim More Later

                    #region Updating the totals in the grid
                    //  Update running totals
                    currBene = currBeneWife + currBeneHusb;
                    #region Code to calculate the annual incom according to the DateOfBirth
                    Customers customer = new Customers();
                    int years, yearsHusb, remainingMonths, remainingMonthsHusb, monthsHusbCurrent, monthsWifeCurrent, yearsHusbCurrent, yearsWifeCurrent;
                    WifeAgeWhenSpouseAge62(out years, out remainingMonths);
                    HusbandAgeWhenSpouseAge62(out yearsHusb, out remainingMonthsHusb);
                    int resultHusb = FRAMonths(birthYearHusb);
                    int resultWife = FRAMonths(birthYearWife);
                    int intHusbandDifference = 0;
                    int intWifeDifference = 0;
                    DateTime datediffHusb = wifeBirthDate.AddYears(70);
                    TimeSpan difftimespanHusb = datediffHusb.Subtract(husbBirthDate);
                    int yearHusb = (int)Math.Floor((double)difftimespanHusb.Days / 365.2425);
                    int monthsHusb = Convert.ToInt32(((double)difftimespanHusb.Days / 30.436875));
                    int multiplyHusb = monthsHusb - yearHusb * 12;
                    DateTime datediff = husbBirthDate.AddYears(70);
                    TimeSpan difftimespan = datediff.Subtract(wifeBirthDate);
                    int yearsWife = (int)Math.Floor((double)difftimespan.Days / 365.2425);
                    int monthsWife = Convert.ToInt32(((double)difftimespan.Days / 30.436875));
                    int multiplyWife = monthsWife - yearsWife * 12;

                    //Calculate current age and months (Added on 28 April 2017)
                    SpouseAgeInMonth_Year(husbBirthDate, out monthsHusbCurrent, out yearsHusbCurrent);
                    SpouseAgeInMonth_Year(wifeBirthDate, out monthsWifeCurrent, out yearsWifeCurrent);


                    if (birthYearWife >= 1955 && birthYearWife <= 1959 && ageWife == wifeFRA && wifeFirstPayAge == wifeFRA && currBeneWife != 0)
                    {
                        currAnnualBenefitsWife = annualBenefitsAsPerBirthDate(birthYearWife, currBeneWife);
                        Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12 - customer.FRAMonthsRemaining(birthYearWife));
                        Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(customer.FRAMonthsRemaining(birthYearWife));
                        BoolWifeAgePDF = true;
                        Globals.Instance.BoolWifeAgeAdjusted = true;
                    }

                    //(Added on 28 April 2017 for deduction of wife who taking her benefit immediately)
                    else if ((intAgeWife >= 62) && (currBeneWife != 0) && strategy == calc_strategy.Spousal && ((wifeFirstPayAge - 1) == yearsWifeCurrent) && ((ageWife - 1) == yearsWifeCurrent) && (monthsWifeCurrent > 0) && (monthsWifeCurrent != 12))
                    {
                        currAnnualBenefitsWife = currBeneWife * (12m + (12m - remainingMonths));
                        BoolWifeAgePDF = true;
                        Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16((12m + (12m - remainingMonths)));
                        Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16((remainingMonths));
                        Globals.Instance.WifeNumberofYearsAbove66 = ageWife;
                        Globals.Instance.BoolWifeAgeAdjusted = true;
                    }


                    /////////////////////Added below currBeneHusb != 0 when wife should take his first benefit at current age not when husb is age 62
                    else if ((intAgeWife >= 62) && (currBeneWife != 0) && (wifeFirstPayAge == years) && (ageWife == years) && (remainingMonths > 0) && (remainingMonths != 12))
                    {
                        if (currBeneHusb != 0)
                        {
                            currAnnualBenefitsWife = currBeneWife * (12m - remainingMonths);
                            BoolWifeAgePDF = true;
                            Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16((12m - remainingMonths));
                            Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16((remainingMonths));
                            Globals.Instance.WifeNumberofYearsAbove66 = ageWife;
                            Globals.Instance.BoolWifeAgeAdjusted = true;
                        }
                    }
                    //Added on 24/10/2016
                    else if ((intAgeWife >= 62) && (currBeneWife != 0) && ((ageWife - 1) == yearsWife) && ((wifeFirstPayAge - 1) == yearsWife) && (ageWife == 62) && (remainingMonths != 12) && (remainingMonths > 0))
                    {
                        currAnnualBenefitsWife = currBeneWife * (12m + remainingMonths);
                        BoolWifeAgePDF = true;
                        Globals.Instance.WifeNumberofMonthinNote = (int)(12m + remainingMonths);
                        Globals.Instance.WifeNumberofMonthsinSteps = (int)(12m - remainingMonths);
                        Globals.Instance.WifeNumberofYearsAbove66 = yearsWife;
                        Globals.Instance.BoolWifeAgeAdjusted = true;
                    }

                    else if ((intAgeWife >= 62) && (currBeneWife != 0) && (ageWife >= wifeFRA) && (ageWife < 70) && (wifeFirstPayAge == ageWife) && (ageHusb == 62) && (intAgeHusb < 62) && (strategy == calc_strategy.Spousal))
                    {
                        int intMultiplyMonth = 0;
                        currAnnualBenefitsWife = WifeSpouseAgeLessThan62(out intMultiplyMonth, currBeneWife);
                        BoolWifeAgePDF = true;
                        Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(intMultiplyMonth);
                        Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(12 - intMultiplyMonth);
                        Globals.Instance.WifeNumberofYearsAbove66 = ageWife;
                        Globals.Instance.BoolWifeAgeAdjusted = true;
                    }
                    else if ((intAgeWife >= 62) && (currBeneWife != 0) && (wifeFirstPayAge == intAgeWife) && (ageWife == wifeFirstPayAge) && (intCurrentAgeWifeYear >= 62) && (intAgeWife != intCurrentAgeWifeYear) && (intCurrentAgeWifeMonth != 12) && ((12 - intCurrentAgeWifeMonth) > 0))
                    {
                        if (intAgeWifeMonth < intAgeHusbMonth && ageWife == wifeFRA && intAgeWife == wifeFRA)
                        {
                            currAnnualBenefitsWife = currBeneWife * (12m + (12 - intCurrentAgeWifeMonth));
                            BoolWifeAgePDF = true;
                            Globals.Instance.BoolWifeAgeAdjusted = true;
                            Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12 + (12 - intCurrentAgeWifeMonth));
                            Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(intCurrentAgeWifeMonth);
                            Globals.Instance.WifeNumberofYearsAbove66 = intCurrentAgeWifeYear;
                            Globals.Instance.BoolSpousalAtFRAAgeCliamedWife = false;
                        }
                    }
                    else if (Globals.Instance.BoolSpousalAfterWorkWife && ageHusb == 70 && multiplyWife != 0 && multiplyWife != 12)
                    {
                        if (multiplyWife > 12)
                        {
                            currAnnualBenefitsWife = currBeneWife * multiplyWife;
                            Globals.Instance.WifeNumberofYears = yearsWife;
                        }
                        else if (yearsWife == ageWife)
                        {
                            currAnnualBenefitsWife = currBeneWife * ((12 - multiplyWife));
                            Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = Convert.ToInt16((12 - multiplyWife));
                            BoolWifeSingleAsterisk = true;
                            intWifeDifference = ageWife - yearsWife;
                            Globals.Instance.WifeNumberofYears = yearsWife;
                        }
                        else
                        {
                            currAnnualBenefitsWife = currBeneWife * (12 + (12 - multiplyWife));
                            Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = Convert.ToInt16(12 + (12 - multiplyWife));
                            intWifeDifference = ageWife - yearsWife;
                            BoolWifeDoubleAsterisk = true;
                            Globals.Instance.WifeNumberofYears = yearsWife;

                            if (strategy == calc_strategy.ZeroBenefit)
                            {
                                Globals.Instance.WifeNumberofMonthsinSteps = multiplyWife;
                                Globals.Instance.BoolWHBSingleSwitchedWife = true;
                            }
                        }
                    }

                    //Update for Husband
                    if (birthYearHusb >= 1955 && birthYearHusb <= 1959 && ageHusb == husbFRA && husbFirstPayAge == husbFRA && currBeneHusb != 0)
                    {
                        currAnnualBenefitsHusb = annualBenefitsAsPerBirthDate(birthYearHusb, currBeneHusb);
                        Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12 - customer.FRAMonthsRemaining(birthYearHusb));
                        Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(customer.FRAMonthsRemaining(birthYearHusb));
                        BoolHusbandAgePDF = true;
                        Globals.Instance.BoolHusbandAgeAdjusted = true;
                    }

                    //(Added on 28 April 2017)
                    else if ((intAgeHusb >= 62) && (currBeneHusb != 0) && strategy == calc_strategy.Spousal && ((husbFirstPayAge - 1) == yearsHusbCurrent) && ((ageHusb - 1) == yearsHusbCurrent) && (monthsHusbCurrent != 12) && (monthsHusbCurrent > 0))
                    {
                        currAnnualBenefitsHusb = currBeneHusb * (12m + (12m - monthsHusbCurrent));
                        BoolHusbandAgePDF = true;
                        Globals.Instance.HusbandNumberofMonthsinNote = (int)(12m + (12m - monthsHusbCurrent));
                        Globals.Instance.HusbandNumberofMonthsinSteps = monthsHusbCurrent;
                        Globals.Instance.HusbNumberofYearsAbove66 = yearsHusbCurrent;
                        Globals.Instance.BoolHusbandAgeAdjusted = true;
                    }

                    ///////////////////////Added below currBeneWife != 0 when husband should take his first benefit at current age not when wife is age 62
                    else if ((intAgeHusb >= 62) && (currBeneHusb != 0) && (husbFirstPayAge == yearsHusb) && (ageHusb == yearsHusb) && (remainingMonthsHusb != 12) && (remainingMonthsHusb > 0))
                    {
                        if (currBeneWife != 0)
                        {
                            currAnnualBenefitsHusb = currBeneHusb * (12m - remainingMonthsHusb);
                            BoolHusbandAgePDF = true;
                            Globals.Instance.HusbandNumberofMonthsinNote = (int)(12m - remainingMonthsHusb);
                            Globals.Instance.HusbandNumberofMonthsinSteps = (int)remainingMonthsHusb;
                            Globals.Instance.HusbNumberofYearsAbove66 = yearsHusb;
                            Globals.Instance.BoolHusbandAgeAdjusted = true;
                        }
                    }
                    //Added on 24/10/2016 for strategy 1 text issue.. was not taking monthly benefit, taking whole benefit at 69 it should be 68 years & .. months
                    else if ((intAgeHusb >= 62) && (currBeneHusb != 0) && ((ageHusb - 1) == yearsHusb) && ((husbFirstPayAge - 1) == yearsHusb) && (ageWife == 62) && (remainingMonthsHusb != 12) && (remainingMonthsHusb > 0))
                    {
                        currAnnualBenefitsHusb = currBeneHusb * (12m + remainingMonthsHusb);
                        BoolHusbandAgePDF = true;
                        Globals.Instance.HusbandNumberofMonthsinNote = (int)(12m + remainingMonthsHusb);
                        Globals.Instance.HusbandNumberofMonthsinSteps = (int)(12m - remainingMonthsHusb);
                        Globals.Instance.HusbNumberofYearsAbove66 = yearsHusb;
                        Globals.Instance.BoolHusbandAgeAdjusted = true;
                    }

                    else if ((intAgeHusb >= 62) && (currBeneHusb != 0) && (ageHusb >= husbFRA) && (husbFirstPayAge == ageHusb) && (ageHusb < 70) && (ageWife == 62) && (intAgeWife < 62) && (strategy == calc_strategy.Spousal))
                    {
                        if (intAgeWifeMonth > intAgeHusbMonth && ageHusb == husbFRA && intAgeHusb == husbFRA)
                        {
                            int intMultiplyMonth = 0;
                            currAnnualBenefitsHusb = HusbandSpouseAgeLessThan62(out intMultiplyMonth, currBeneHusb);
                            BoolHusbandAgePDF = true;
                            Globals.Instance.HusbandNumberofMonthsinNote = intMultiplyMonth;
                            Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(12 - intMultiplyMonth);
                            Globals.Instance.HusbNumberofYearsAbove66 = ageHusb;
                            Globals.Instance.BoolHusbandAgeAdjusted = true;
                        }
                    }

                    else if ((intAgeHusb >= 62) && (currBeneHusb != 0) && (husbFirstPayAge == intAgeHusb) && (intCurrentAgeHusbMonth != 12) && (ageHusb == husbFirstPayAge) && (intCurrentAgeHusbYear >= 62) && (intAgeHusb != intCurrentAgeHusbYear) && ((12 - intCurrentAgeHusbMonth) > 0))
                    {
                        if (!(Globals.Instance.BoolSpousalAtFRAAgeCliamedHusb && ageHusb == husbFRA))
                        {
                            currAnnualBenefitsHusb = currBeneHusb * (12m + (12 - intCurrentAgeHusbMonth));
                            BoolHusbandAgePDF = true;
                            Globals.Instance.BoolHusbandAgeAdjusted = true;
                            Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12 + (12 - intCurrentAgeHusbMonth));
                            Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(intCurrentAgeHusbMonth);
                            Globals.Instance.HusbNumberofYearsAbove66 = intCurrentAgeHusbYear;
                            Globals.Instance.BoolSpousalAtFRAAgeCliamedHusb = false;
                        }
                    }
                    else if (Globals.Instance.BoolSpousalAfterWorkHusb && (ageWife == 70) && (multiplyHusb != 0) && (multiplyHusb != 12))
                    {
                        if (multiplyHusb > 12)
                        {
                            currAnnualBenefitsHusb = currBeneHusb * multiplyHusb;
                            Globals.Instance.HusbandNumberofYears = yearHusb;
                        }
                        else if (yearHusb == ageHusb)
                        {
                            currAnnualBenefitsHusb = currBeneHusb * ((12 - multiplyHusb));
                            Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior = Convert.ToInt16((12 - multiplyHusb));
                            BoolHusbSingleAsterisk = true;
                            intHusbandDifference = ageHusb - yearHusb;
                            Globals.Instance.HusbandNumberofYears = yearHusb;
                        }
                        else
                        {
                            currAnnualBenefitsHusb = currBeneHusb * (12 + (12 - multiplyHusb));
                            Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior = Convert.ToInt16(12 + (12 - multiplyHusb));
                            BoolHusbDoubleAsterisk = true;
                            intHusbandDifference = ageHusb - yearHusb;
                            Globals.Instance.HusbandNumberofYears = yearHusb;
                            if (strategy == calc_strategy.ZeroBenefit)
                            {
                                Globals.Instance.BoolWHBSwitchedHusb = true;
                                Globals.Instance.HusbandNumberofMonthsinSteps = multiplyHusb;
                            }
                        }
                    }

                    if (currAnnualBenefitsWife == 0)
                    {
                        currAnnualBenefitsWife = currBeneWife * 12;
                    }
                    if (currAnnualBenefitsHusb == 0)
                    {
                        currAnnualBenefitsHusb = currBeneHusb * 12;
                    }
                    if ((intCurrentAgeHusbMonth >= 1) && (intCurrentAgeHusbMonth != 12) && (ageHusb >= 62) && (ageHusb == intCurrentAgeHusbYear) && (ageHusb < 70))
                    {
                        if (ageHusb == intCurrentAgeHusbYear - 1)
                        {
                            husbStartingDeduction = true;
                        }
                    }
                    if (husbStartingDeduction && (ageHusb == intCurrentAgeHusbYear + 1) && (!Globals.Instance.BoolSpousalAtFRAAgeCliamedHusb) && (ageHusb == husbFirstPayAge) && (ageHusb < 70) && (intCurrentAgeHusbMonth >= 1) && (intCurrentAgeHusbMonth != 12))
                    {
                        currAnnualBenefitsHusb += currBeneHusb * (12 - intCurrentAgeHusbMonth);
                        Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12 + (12 - intCurrentAgeHusbMonth));
                        Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(intCurrentAgeHusbMonth);
                        BoolHusbandAgePDF = true;
                        Globals.Instance.BoolHusbandAgeAdjusted = true;
                        Globals.Instance.HusbNumberofYearsAbove66 = intCurrentAgeHusbYear;
                        husbStartingDeduction = false;
                    }
                    if ((intCurrentAgeHusbMonth >= 1) && (intCurrentAgeHusbMonth != 12) && (ageHusb == intCurrentAgeHusbYear) && (ageHusb < 70) && (currBeneHusb != 0))
                    {
                        if (intAgeHusbMonth >= 6)
                        {
                            currAnnualBenefitsHusb = 0; //added on 01/11/2017 it was adding the annual benefit in previous 12 months benefit, it should not take 12 month additional benefit
                            currAnnualBenefitsHusb += currBeneHusb * (12 - intCurrentAgeHusbMonth);
                            if (currBeneHusb != 0)
                            {
                                Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12 - intCurrentAgeHusbMonth);
                                Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(intCurrentAgeHusbMonth);
                                BoolHusbandAgePDF = true;
                                Globals.Instance.BoolHusbandAgeAdjusted = true;
                                Globals.Instance.HusbNumberofYearsAbove66 = intCurrentAgeHusbYear;
                            }
                        }
                        else
                        {
                            currAnnualBenefitsHusb -= currBeneHusb * intCurrentAgeHusbMonth;
                            Globals.Instance.HusbandNumberofMonthsinNote = Convert.ToInt16(12 - intCurrentAgeHusbMonth);
                            Globals.Instance.HusbandNumberofMonthsinSteps = Convert.ToInt16(intCurrentAgeHusbMonth);
                            BoolHusbandAgePDF = true;
                            Globals.Instance.BoolHusbandAgeAdjusted = true;
                            Globals.Instance.HusbNumberofYearsAbove66 = intCurrentAgeHusbYear;
                        }
                    }
                    if ((intCurrentAgeWifeMonth >= 1) && (intCurrentAgeWifeMonth != 12) && (ageWife >= 62) && (ageWife == intCurrentAgeWifeYear) && (ageWife < 70))
                    {
                        wifeStartingDeduction = true;
                    }
                    if (wifeStartingDeduction && (ageWife == intCurrentAgeWifeYear + 1) && (!Globals.Instance.BoolSpousalAtFRAAgeCliamedWife) && (ageWife == wifeFirstPayAge) && (ageWife < 70) && (intCurrentAgeWifeMonth >= 1) && (intCurrentAgeWifeMonth != 12))
                    {
                        currAnnualBenefitsWife += currBeneWife * (12 - intCurrentAgeWifeMonth);
                        Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(intCurrentAgeWifeMonth);
                        Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12 + (12 - intCurrentAgeWifeMonth));
                        BoolWifeAgePDF = true;
                        Globals.Instance.BoolWifeAgeAdjusted = true;
                        Globals.Instance.WifeNumberofYearsAbove66 = intCurrentAgeWifeYear;
                        wifeStartingDeduction = false;
                    }
                    if ((intCurrentAgeWifeMonth >= 1) && (intCurrentAgeWifeMonth != 12) && (ageWife == intCurrentAgeWifeYear) && (ageWife < 70) && (currBeneWife != 0))
                    {
                        if (intAgeWifeMonth >= 6)
                        {
                            currAnnualBenefitsWife = 0; //added on 01/11/2017 it was adding the annual benefit in previous 12 months benefit, it should not take 12 month additional benefit
                            currAnnualBenefitsWife += currBeneWife * (12 - intCurrentAgeWifeMonth);
                            if (currBeneWife != 0)
                            {
                                Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(intCurrentAgeWifeMonth);
                                Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12 - intCurrentAgeWifeMonth);
                                BoolWifeAgePDF = true;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                                Globals.Instance.WifeNumberofYearsAbove66 = intCurrentAgeWifeYear;
                            }
                        }
                        else
                        {
                            currAnnualBenefitsWife -= currBeneWife * intCurrentAgeWifeMonth;
                            if (currBeneWife != 0)
                            {
                                Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12 - intCurrentAgeWifeMonth);
                                Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(intCurrentAgeWifeMonth);
                                BoolWifeAgePDF = true;
                                Globals.Instance.BoolWifeAgeAdjusted = true;
                                Globals.Instance.WifeNumberofYearsAbove66 = intCurrentAgeWifeYear;
                            }
                        }
                    }
                    //New case when wife current age is 1 year less than ageWife
                    //else if ((intCurrentAgeWifeMonth >= 1) && (intCurrentAgeWifeMonth != 12) && ((ageWife - 1) == intCurrentAgeWifeYear) && (ageWife < 70) && (currBeneWife != 0))
                    //{
                    //    currAnnualBenefitsWife = currBeneWife * (12 + (12 - intCurrentAgeWifeMonth));
                    //    if (currBeneWife != 0)
                    //    {
                    //        Globals.Instance.WifeNumberofMonthsinSteps = Convert.ToInt16(intCurrentAgeWifeMonth);
                    //        Globals.Instance.WifeNumberofMonthinNote = Convert.ToInt16(12 + (12 - intCurrentAgeWifeMonth));
                    //        BoolWifeAgePDF = true;
                    //        Globals.Instance.BoolWifeAgeAdjusted = true;
                    //        Globals.Instance.WifeNumberofYearsAbove66 = intCurrentAgeWifeYear;
                    //    }
                    //}


                    //Restricted Application Strategy Income
                    if (currBeneHusb != 0 && ageHusb < 70)
                    {
                        Globals.Instance.intRestrictAppIncomeBestHusb += (int)currAnnualBenefitsHusb;
                        Globals.Instance.intRestrictAppIncomeEarlyHusb += (int)currAnnualBenefitsHusb;
                    }
                    if (currBeneWife != 0 && ageWife < 70)
                    {
                        Globals.Instance.intRestrictAppIncomeBestWife += (int)currAnnualBenefitsWife;
                        Globals.Instance.intRestrictAppIncomeEarlyWife += (int)currAnnualBenefitsWife;
                    }


                    if (currAnnualBenefitsWife == 0 && currAnnualBenefitsHusb == 0)
                    {
                        currBeneAnnual = currBene * 12;
                    }
                    else
                    {
                        currBeneAnnual = currAnnualBenefitsWife + currAnnualBenefitsHusb;
                        if (ageWife == 69)
                            Globals.Instance.WifeOriginalAnnualIncome = currAnnualBenefitsWife;
                        if (ageHusb == 69)
                            Globals.Instance.HusbandOriginalAnnualIncome = currAnnualBenefitsHusb;
                        currAnnualBenefitsWife = 0;
                        currAnnualBenefitsHusb = 0;
                    }

                    #endregion Code to calculate the annual incom according to the DateOfBirth

                    #region Code to calculate the cummulative benefits before and after younger age 70
                    if (youngest == 70)
                    {
                        currBeneWifeLocal = currBeneWife;
                        currBeneHusbLocal = currBeneHusb;
                    }
                    if (youngest >= 75 && youngest <= 90)
                    {
                        if (youngest == 75)
                        {
                            decimal grandTotal = 0;
                            for (int i = 0; i < 5; i++)
                            {
                                currBeneWifeLocal = Convert.ToInt32(ColaUp(63, currBeneWifeLocal));
                                currBeneHusbLocal = Convert.ToInt32(ColaUp(63, currBeneHusbLocal));
                                decimal total = Convert.ToInt32((currBeneWifeLocal + currBeneHusbLocal) * 12);
                                grandTotal += Convert.ToInt32(total);
                            }
                            cummBene += grandTotal;
                        }
                        else
                        {
                            decimal grandTotal = 0;
                            for (int i = 0; i < 5; i++)
                            {
                                currBeneWifeLocal = Convert.ToInt32(ColaUp(63, currBeneWifeLocal));
                                currBeneHusbLocal = Convert.ToInt32(ColaUp(63, currBeneHusbLocal));
                                decimal total = Convert.ToInt32((currBeneWifeLocal + currBeneHusbLocal) * 12);
                                grandTotal += Convert.ToInt32(total);
                            }
                            cummBene += grandTotal;
                        }
                    }
                    if (youngest <= 71) //Previously (youngest <= 70), changed on Nov 09 2016
                    {
                        cummBene += currBeneAnnual;
                    }
                    #endregion Code to calculate the cummulative benefits before and after younger age 70

                    cummBeneWife += currBeneWife;
                    cummBeneHusb += currBeneHusb;

                    currbenewife = currBeneWife;
                    currbenehus = currBeneHusb;
                    //Survivor's benefit
                    decimal survBeneHusb = 0, survBeneWife = 0, survBene = 0;
                    survBeneHusb = currBeneHusb;
                    survBeneWife = currBeneWife;
                    if (survBeneHusb > survBeneWife)
                        survBene = survBeneHusb;
                    else
                        survBene = survBeneWife;
                    if (ageHusb >= 70 && ageWife >= 70 && age70SurvBeneAge == string.Empty)
                    {
                        if (ageWife == ageHusb)
                        {
                            age70SurvBeneAge = ageHusb.ToString();
                        }
                        else
                        {
                            age70SurvBeneAge = ageWife.ToString() + Constants.BackSlash + ageHusb.ToString();
                        }
                        age70SurvBene = survBene * 12;
                    }



                    string ageCol = string.Empty;
                    //if (ageHusb == ageWife)
                    //    ageCol = aIx.ToString();
                    //else
                    ageCol = ageWife.ToString() + Constants.BackSlash + ageHusb.ToString();
                    breakEvenAmt[highBE] = (int)StdRound(cummBene);
                    breakEvenAge[highBE] = ageCol;
                    highBE += 1;
                    //  For selected ages, build the display row
                    if (youngest <= 90)//((aIx <= topAix) || (aIx == topAix + 10) || didAutoAdjust || showThisLine)
                    {
                        System.Data.DataRow incomeRow = incomeTab.NewRow();
                        // Date : 20112014
                        // Description : Add Extra Column "YEAR" in Grid.
                        //incomeRow[(int)disp_col.Year] = year;
                        if (ageHusb == ageWife)
                            incomeRow[(int)disp_col.Age] = ageCol;
                        else
                            incomeRow[(int)disp_col.Age] = ageCol;
                        if (currBene > 0)
                        {
                            string regExp = "[^\\w]";
                            if (BoolHusbandAgePDF || BoolWifeAgePDF)
                            {
                                incomeRow[(int)disp_col.AnnInc] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + Constants.SingleHash;
                                BoolHusbandAgePDF = false;
                                BoolWifeAgePDF = false;
                            }
                            else if (BoolHusbDoubleAsterisk || BoolWifeDoubleAsterisk)
                            {
                                string strhashIncluded = string.Empty;
                                if (incomeRow[(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                    strhashIncluded = Constants.SingleHash;
                                incomeRow[(int)disp_col.AnnInc] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.DoubleAsterisk;
                                int intPrevBenefit = 0, intPrevAnnualIcome = 0;
                                string strBenefit = string.Empty;
                                if (BoolHusbDoubleAsterisk)
                                {
                                    if (intHusbandDifference > 0)
                                        intHusbandDifference = incomeTab.Rows.Count - intHusbandDifference;
                                    else
                                        intHusbandDifference = incomeTab.Rows.Count - 1;
                                    Int32 intCummAnnual = Int32.Parse(Regex.Replace(incomeTab.Rows[intHusbandDifference - 1][(int)disp_col.CummInc].ToString(), regExp, ""));
                                    strBenefit = incomeTab.Rows[intHusbandDifference][(int)disp_col.HusbInc].ToString().TrimStart('$');
                                    intPrevBenefit = Convert.ToInt32((Regex.Replace(strBenefit, regExp, "")));
                                    intPrevAnnualIcome = intPrevBenefit * multiplyHusb;
                                    if (incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                        strhashIncluded = Constants.SingleHash;
                                    if (intPrevAnnualIcome != 0)
                                    {
                                        incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc] = (Globals.Instance.WifeOriginalAnnualIncome + intPrevAnnualIcome).ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.SingleAsterisk;
                                        Globals.Instance.BoolWHBSwitchedHusb = true;

                                        #region updated on (17/01/2017) Special Married case posted on 27 dec 2016
                                        //Update the calculation if user taking his/her both the benefits in same year where annual income value attached
                                        //with "#" and "*" combinely ---updated on (17/01/2017)
                                        if (incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc].ToString().Contains("#*"))
                                        {
                                            int intMultiplyer = 0;
                                            Globals.Instance.BoolContainsHashStar = true;
                                            if (Globals.Instance.HusbandNumberofMonthsinNote > (Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior - 12))
                                                intMultiplyer = Globals.Instance.HusbandNumberofMonthsinNote - (Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior - 12);
                                            else
                                                intMultiplyer = (Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior - 12) - Globals.Instance.HusbandNumberofMonthsinNote;
                                            incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc] = (Globals.Instance.WifeOriginalAnnualIncome + (intPrevBenefit * intMultiplyer)).ToString(Constants.AMT_FORMAT) + strhashIncluded;// +Constants.SingleAsterisk;
                                        }
                                        ////
                                        #endregion updated on (17/01/2017) Special Married case posted on 27 dec 2016
                                    }
                                    incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc] = (intCummAnnual + int.Parse(Regex.Replace(incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc].ToString(), regExp, "")));
                                    cummBene = currBeneAnnual + decimal.Parse(incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc].ToString());
                                    incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc] = decimal.Parse(incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc].ToString()).ToString(Constants.AMT_FORMAT);
                                    Globals.Instance.HusbNumberofMonthsBelowGrid = multiplyHusb;
                                    Globals.Instance.HusbNumberofMonthsAboveGrid = multiplyHusb;
                                    BoolHusbDoubleAsterisk = false;
                                }
                                else
                                {
                                    if (intWifeDifference > 0)
                                        intWifeDifference = incomeTab.Rows.Count - intWifeDifference;
                                    else
                                        intWifeDifference = incomeTab.Rows.Count - 1;
                                    Int32 intCummAnnual = Int32.Parse(Regex.Replace(incomeTab.Rows[intWifeDifference - 1][(int)disp_col.CummInc].ToString(), regExp, ""));
                                    strBenefit = incomeTab.Rows[intWifeDifference][(int)disp_col.WifeInc].ToString().TrimStart('$');
                                    intPrevBenefit = Convert.ToInt32((Regex.Replace(strBenefit, regExp, "")));
                                    intPrevAnnualIcome = intPrevBenefit * multiplyWife;
                                    if (incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                        strhashIncluded = Constants.SingleHash;
                                    if (intPrevAnnualIcome != 0)
                                    {
                                        incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc] = (Globals.Instance.HusbandOriginalAnnualIncome + intPrevAnnualIcome).ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.SingleAsterisk;
                                        Globals.Instance.BoolWHBSwitchedWife = true;

                                        #region updated on (17/01/2017) Special Married case
                                        //Update the calculation if user taking his/her both the benefits in same year where annual income value attached
                                        //with "#" and "*" ----- updated on (17/01/2017) posted on 27 dec 2016
                                        if (incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc].ToString().Contains("#*"))
                                        {
                                            int intMultiplyer = 0;
                                            Globals.Instance.BoolContainsHashStar = true;
                                            if (Globals.Instance.WifeNumberofMonthinNote > (Globals.Instance.WifeNumberofMonthsBelowGridAgePrior - 12))
                                                intMultiplyer = Globals.Instance.WifeNumberofMonthinNote - (Globals.Instance.WifeNumberofMonthsBelowGridAgePrior - 12);
                                            else
                                                intMultiplyer = (Globals.Instance.WifeNumberofMonthsBelowGridAgePrior - 12) - Globals.Instance.WifeNumberofMonthinNote;
                                            incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc] = (Globals.Instance.HusbandOriginalAnnualIncome + (intPrevBenefit * intMultiplyer)).ToString(Constants.AMT_FORMAT) + strhashIncluded;// +Constants.SingleAsterisk;
                                        }
                                        ///
                                        #endregion updated on (17/01/2017) Special Married case
                                    }
                                    incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc] = (intCummAnnual + int.Parse(Regex.Replace(incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc].ToString(), regExp, "")));
                                    cummBene = currBeneAnnual + decimal.Parse(incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc].ToString());
                                    incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc] = decimal.Parse(incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc].ToString()).ToString(Constants.AMT_FORMAT);
                                    Globals.Instance.WifeNumberofMonthsBelowGrid = multiplyWife;
                                    Globals.Instance.WifeNumberofMonthsAboveGrid = multiplyWife;
                                    BoolWifeDoubleAsterisk = false;
                                }
                            }
                            else if (BoolWifeSingleAsterisk || BoolHusbSingleAsterisk)
                            {
                                string strhashIncluded = string.Empty;
                                if (incomeRow[(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                    strhashIncluded = Constants.SingleHash;
                                incomeRow[(int)disp_col.AnnInc] = currBeneAnnual.ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.DoubleAsterisk;
                                int intPrevBenefit = 0, intPrevAnnualIcome = 0;
                                string strBenefit = string.Empty;
                                if (BoolHusbSingleAsterisk)
                                {
                                    if (intHusbandDifference > 0)
                                        intHusbandDifference = incomeTab.Rows.Count - intHusbandDifference;
                                    else
                                        intHusbandDifference = incomeTab.Rows.Count - 1;
                                    Int32 intCummAnnual = Int32.Parse(Regex.Replace(incomeTab.Rows[intHusbandDifference - 1][(int)disp_col.CummInc].ToString(), regExp, ""));
                                    strBenefit = incomeTab.Rows[intHusbandDifference][(int)disp_col.HusbInc].ToString().TrimStart('$');
                                    intPrevBenefit = Convert.ToInt32((Regex.Replace(strBenefit, regExp, "")));
                                    intPrevAnnualIcome = intPrevBenefit * (12 + multiplyHusb);
                                    if (incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                        strhashIncluded = Constants.SingleHash;
                                    if (intPrevAnnualIcome != 0)
                                    {
                                        incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc] = (Globals.Instance.WifeOriginalAnnualIncome + intPrevAnnualIcome).ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.SingleAsterisk;
                                        Globals.Instance.BoolWHBSwitchedHusb = true;
                                    }
                                    incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc] = (intCummAnnual + int.Parse(Regex.Replace(incomeTab.Rows[intHusbandDifference][(int)disp_col.AnnInc].ToString(), regExp, "")));
                                    incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc] = decimal.Parse(incomeTab.Rows[intHusbandDifference][(int)disp_col.CummInc].ToString()).ToString(Constants.AMT_FORMAT);
                                    Globals.Instance.HusbNumberofMonthsBelowGrid = (12 + multiplyHusb);
                                    Globals.Instance.HusbNumberofMonthsAboveGrid = multiplyHusb;
                                    BoolHusbSingleAsterisk = false;
                                }
                                else
                                {
                                    if (intWifeDifference > 0)
                                        intWifeDifference = incomeTab.Rows.Count - intWifeDifference;
                                    else
                                        intWifeDifference = incomeTab.Rows.Count - 1;
                                    Int32 intCummAnnual = Int32.Parse(Regex.Replace(incomeTab.Rows[intWifeDifference - 1][(int)disp_col.CummInc].ToString(), regExp, ""));
                                    strBenefit = incomeTab.Rows[intWifeDifference][(int)disp_col.WifeInc].ToString().TrimStart('$');
                                    intPrevBenefit = Convert.ToInt32((Regex.Replace(strBenefit, regExp, "")));
                                    intPrevAnnualIcome = intPrevBenefit * (12 + multiplyWife);
                                    if (incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc].ToString().Contains(Constants.SingleHash))
                                        strhashIncluded = Constants.SingleHash;
                                    if (intPrevAnnualIcome != 0)
                                    {
                                        incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc] = (Globals.Instance.HusbandOriginalAnnualIncome + intPrevAnnualIcome).ToString(Constants.AMT_FORMAT) + strhashIncluded + Constants.SingleAsterisk;
                                        Globals.Instance.BoolWHBSwitchedWife = true;
                                    }
                                    incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc] = (intCummAnnual + int.Parse(Regex.Replace(incomeTab.Rows[intWifeDifference][(int)disp_col.AnnInc].ToString(), regExp, "")));
                                    incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc] = decimal.Parse(incomeTab.Rows[intWifeDifference][(int)disp_col.CummInc].ToString()).ToString(Constants.AMT_FORMAT);
                                    Globals.Instance.WifeNumberofMonthsBelowGrid = (12 + multiplyWife);
                                    Globals.Instance.WifeNumberofMonthsAboveGrid = multiplyWife;
                                    BoolWifeSingleAsterisk = false;
                                }
                            }
                            else
                            {
                                incomeRow[(int)disp_col.AnnInc] = currBeneAnnual.ToString(Constants.AMT_FORMAT);
                            }
                            incomeRow[(int)disp_col.WifeInc] = currBeneWife.ToString(Constants.AMT_FORMAT);
                            incomeRow[(int)disp_col.HusbInc] = currBeneHusb.ToString(Constants.AMT_FORMAT);
                            incomeRow[(int)disp_col.CombInc] = currBene.ToString(Constants.AMT_FORMAT);
                            incomeRow[(int)disp_col.CummInc] = cummBene.ToString(Constants.AMT_FORMAT);
                            incomeRow[(int)disp_col.SurvInc] = survBene.ToString(Constants.AMT_FORMAT);
                        }
                        else
                        {
                            incomeRow[(int)disp_col.WifeInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_col.HusbInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_col.CombInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_col.AnnInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_col.CummInc] = Constants.ZeroWithOutAsterisk;
                            incomeRow[(int)disp_col.SurvInc] = survBene.ToString(Constants.AMT_FORMAT);
                        }
                        if (strategy == calc_strategy.Suspend)
                        {
                            if ((decBeneWife > decBeneHusb) && (needAsterisk || combinedBenefts))
                            {
                                if (incomeRow[(int)disp_col.Age].ToString().Substring(0, 2).Equals(wifeFRA.ToString()))
                                {
                                    incomeRow[(int)disp_col.WifeInc] = Constants.ZeroWithAsterisk; ;
                                }
                            }

                            if ((decBeneWife < decBeneHusb) && (needAsterisk || combinedBenefts))
                            {
                                if (incomeRow[(int)disp_col.Age].ToString().Substring(3, 2).Equals(husbFRA.ToString()))
                                {
                                    incomeRow[(int)disp_col.HusbInc] = Constants.ZeroWithAsterisk; ;
                                }
                            }
                        }

                        incomeTab.Rows.Add(incomeRow);
                    }
                    #endregion Updating the totals in the grid

                    #region to itterate the ages and COLA
                    if (youngest >= 71 && youngest <= 90)
                    {
                        //to itterate by 5yrs when younger at age 70 (62 is normal and then 67-62 is 5yrs difference)
                        currBeneWife = ColaUp(67, currBeneWife);
                        currBeneHusb = ColaUp(67, currBeneHusb);
                    }
                    else
                    {
                        //to itterate by one year
                        currBeneWife = ColaUp(63, currBeneWife);
                        currBeneHusb = ColaUp(63, currBeneHusb);
                    }
                    if (aIx >= 71)
                    {
                        //to itterate by 5yrs when younger at age 70
                        aIx = aIx + 5;
                        oldest = oldest + 5;
                        youngest = youngest + 5;
                    }
                    else
                    {
                        //to itterate by one year
                        oldest++;
                        aIx++;
                        youngest++;
                    }
                }
                #endregion to itterate the ages and COLA

                #region code to highlight the grid values
                Grid1.DataSource = incomeSet;
                Grid1.DataBind();
                TableItemStyle tisA = Grid1.AlternatingRowStyle;
                int cummTotal = 0;
                int highEarnerFRA = 0;
                if (decBeneHusb > decBeneWife)
                    highEarnerFRA = husbFRA;
                else
                    highEarnerFRA = wifeFRA;

                string preFraYear = (highEarnerFRA - 1).ToString();
                string fraYear = highEarnerFRA.ToString();
                string fraYear3 = (69).ToString();
                string fraYear4 = (70).ToString();
                string fraYear14 = (80).ToString();

                foreach (GridViewRow gvr in Grid1.Rows)
                {
                    string regExp = "[^\\w]";
                    if (gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number70))
                    {
                        AgeForHusb70 = gvr.Cells[0].Text;
                        ValueForHusb70 = decimal.Parse(gvr.Cells[2].Text.Substring(1));
                    }
                    if (gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number70))
                    {
                        AgeForWife70 = gvr.Cells[0].Text;
                        ValueForWife70 = decimal.Parse(gvr.Cells[1].Text.Substring(1));
                    }
                    if (decBeneHusb < decBeneWife)
                    {
                        if ((gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number70)))
                        {
                            valueAtAge70 = decimal.Parse(Regex.Replace(gvr.Cells[4].Text, regExp, ""));
                            AgeValueForHigherEarner70 = gvr.Cells[0].Text;
                            ValueForHigherEarner70 = decimal.Parse((gvr.Cells[1].Text.Substring(1)));
                            lastBenefitsChangedAge70Value = decimal.Parse((gvr.Cells[1].Text.Substring(1)));
                        }
                        if ((gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number69)))
                        {
                            valueAtAge69 = decimal.Parse((gvr.Cells[5].Text.Substring(1)));
                        }

                        if (!((gvr.Cells[2].Text).Equals(Constants.ZeroWithOutAsterisk) || ((gvr.Cells[2].Text).Equals(Constants.ZeroWithAsterisk))))
                        {
                            if (boolToRunLoopOnlyOnce)
                            {
                                startingBenefitsValueHusb = decimal.Parse((gvr.Cells[2].Text.Substring(1)));
                                startingAgeValueHusb = gvr.Cells[0].Text;
                                boolToRunLoopOnlyOnce = false;
                            }
                        }
                        else
                        {
                            startingBenefitsValueHusb = 0;
                        }

                        if (!((gvr.Cells[1].Text).Equals(Constants.ZeroWithOutAsterisk) || ((gvr.Cells[1].Text).Equals(Constants.ZeroWithAsterisk))))
                        {
                            if (boolToRunLoopOnlyOnce1)
                            {
                                startingBenefitsValueWife = decimal.Parse((gvr.Cells[1].Text.Substring(1)));
                                startingAgeValueWife = gvr.Cells[0].Text;
                                boolToRunLoopOnlyOnce1 = false;
                            }
                        }
                        else
                        {
                            startingBenefitsValueWife = 0;
                        }
                    }
                    else
                    {
                        if ((gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number70)))
                        {


                            valueAtAge70 = decimal.Parse(Regex.Replace(gvr.Cells[4].Text, regExp, ""));
                            lastBenefitsChangedAge70Value = decimal.Parse((gvr.Cells[2].Text.Substring(1)));
                            AgeValueForHigherEarner70 = gvr.Cells[0].Text;
                            ValueForHigherEarner70 = decimal.Parse((gvr.Cells[2].Text.Substring(1)));

                        }
                        if ((gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number69)))
                        {
                            valueAtAge69 = decimal.Parse((gvr.Cells[5].Text.Substring(1)));
                        }
                        if (!(((gvr.Cells[2].Text).Equals(Constants.ZeroWithOutAsterisk)) || ((gvr.Cells[2].Text).Equals(Constants.ZeroWithAsterisk))))
                        {
                            if (boolToRunLoopOnlyOnce)
                            {
                                startingBenefitsValueHusb = decimal.Parse((gvr.Cells[2].Text.Substring(1)));
                                startingAgeValueHusb = gvr.Cells[0].Text;
                                boolToRunLoopOnlyOnce = false;
                            }
                        }
                        else
                        {
                            startingBenefitsValueHusb = 0;
                        }

                        if (!((gvr.Cells[1].Text).Equals(Constants.ZeroWithOutAsterisk) || ((gvr.Cells[1].Text).Equals(Constants.ZeroWithAsterisk))))
                        {
                            if (boolToRunLoopOnlyOnce1)
                            {
                                startingBenefitsValueWife = decimal.Parse((gvr.Cells[1].Text.Substring(1)));
                                startingAgeValueWife = gvr.Cells[0].Text;
                                boolToRunLoopOnlyOnce1 = false;
                            }
                        }
                        else
                        {
                            startingBenefitsValueWife = 0;
                        }
                    }
                }
                return cummTotal;
                #endregion code to highlight the grid values
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorCalc + "BuildGrid() - " + ex.ToString());
                return 0;
            }
        }

        /// <summary>
        /// function to get the reduction spousal benefits for wife when husband turns age 70
        /// </summary>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="currBeneHusb"></param>
        /// <returns></returns>
        private decimal SpousalBenefitsHusbAtWifeAge70(int ageHusb, int ageWife, decimal currBeneHusb)
        {
            try
            {
                decimal multiply = 0; decimal result = 0; decimal husbReductionAmt = 0; decimal reducedMultiply = 0;
                int FRAmonths, Currentmonths, forExtraMonths, remainigMonths;
                if (intAgeWife > wifeFRA)
                    multiply = AgeAbove68YearsOfWife();
                else
                    multiply = decBeneWife;
                multiply = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                multiply = multiply / 2;

                if (ageHusb > husbFRA)
                {
                    husbReductionAmt = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                }
                else
                {
                    FRAmonths = FRAMonths(birthYearHusb);
                    Currentmonths = ageHusb * 12;
                    forExtraMonths = 0;
                    remainigMonths = (FRAmonths - Currentmonths);
                    if (remainigMonths > 36)
                        forExtraMonths = remainigMonths - 36;

                    if (ageHusb <= husbFRA)
                    {
                        if (remainigMonths > 36)
                        {
                            reducedMultiply = 25m;
                            decimal wifeReductionAmt1 = (5m / 12m) * forExtraMonths;
                            decimal reductionfinal = reducedMultiply + wifeReductionAmt1;
                            decimal reductionfinal1 = 1 - (reductionfinal / 100m);
                            multiply = multiply * reductionfinal1;

                            decimal tempDecBeneWife = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                            decimal wifeReductionAmtWork = 20m;
                            decimal wifeReductionAmt1Work = (5m / 12m) * forExtraMonths;
                            decimal reductionfinalWork = wifeReductionAmtWork + wifeReductionAmt1Work;
                            decimal reductionfinal1Work = 1 - (reductionfinalWork / 100m);
                            husbReductionAmt = tempDecBeneWife * reductionfinal1Work;
                        }
                        else
                        {
                            reducedMultiply = (25m / 36m) * remainigMonths;
                            reducedMultiply = 1 - (reducedMultiply / 100m);
                            multiply = multiply * reducedMultiply;

                            decimal tempDecBeneWife = ColaUpSpousal(ageHusb, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                            decimal wifeReductionAmtWork = (5m / 9m) * remainigMonths;
                            wifeReductionAmtWork = 1 - (wifeReductionAmtWork / 100m);
                            husbReductionAmt = tempDecBeneWife * wifeReductionAmtWork;
                        }
                    }
                }
                result = currBeneHusb + (Convert.ToInt32(multiply) - Convert.ToInt32(husbReductionAmt));
                return result;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// function to get the reduction spousal benefits for husband when wife turns age 70
        /// </summary>
        /// <param name="ageHusb">current  age of husband</param>
        /// <param name="ageWife">current  age of wife</param>
        /// <param name="currBeneWife">current  benefits of wife</param>
        /// <returns></returns>
        private decimal SpousalBenefitsWifeAtHusbAge70(int ageHusb, int ageWife, decimal currBeneWife)
        {
            try
            {
                decimal multiply = 0; decimal result = 0; decimal wifeReductionAmt = 0; decimal reducedMultiply = 0;
                int FRAmonths, Currentmonths, forExtraMonths, remainigMonths;

                if (intAgeHusb > husbFRA)
                    multiply = AgeAbove68YearsOfHusb();
                else
                    multiply = decBeneHusb;
                multiply = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                multiply = multiply / 2;
                if (ageWife >= wifeFRA)
                {
                    wifeReductionAmt = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                }
                else
                {
                    FRAmonths = FRAMonths(birthYearWife);
                    Currentmonths = ageWife * 12;
                    forExtraMonths = 0;
                    remainigMonths = (FRAmonths - Currentmonths);
                    if (remainigMonths > 36)
                        forExtraMonths = remainigMonths - 36;
                    if (ageWife < wifeFRA)
                    {
                        if (remainigMonths > 36)
                        {
                            reducedMultiply = 25m;
                            decimal wifeReductionAmt1 = (5m / 12m) * forExtraMonths;
                            decimal reductionfinal = reducedMultiply + wifeReductionAmt1;
                            decimal reductionfinal1 = 1 - (reductionfinal / 100m);
                            multiply = multiply * reductionfinal1;

                            decimal tempDecBeneWife = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                            decimal wifeReductionAmtWork = 20m;
                            decimal wifeReductionAmt1Work = (5m / 12m) * forExtraMonths;
                            decimal reductionfinalWork = wifeReductionAmtWork + wifeReductionAmt1Work;
                            decimal reductionfinal1Work = 1 - (reductionfinalWork / 100m);
                            wifeReductionAmt = tempDecBeneWife * reductionfinal1Work;
                        }
                        else
                        {
                            reducedMultiply = (25m / 36m) * remainigMonths;
                            reducedMultiply = 1 - (reducedMultiply / 100m);
                            multiply = multiply * reducedMultiply;

                            decimal tempDecBeneWife = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                            decimal wifeReductionAmtWork = (5m / 9m) * remainigMonths;
                            wifeReductionAmtWork = 1 - (wifeReductionAmtWork / 100m);
                            wifeReductionAmt = tempDecBeneWife * wifeReductionAmtWork;
                        }
                    }
                }
                result = currBeneWife + (Convert.ToInt32(multiply) - Convert.ToInt32(wifeReductionAmt));
                return result;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// code to calculate husband Annual Income value for age greater than 62
        /// </summary>
        /// <param name="currBeneHusb"></param>
        /// <param name="currBeneAnnual"></param>
        /// <returns></returns>
        private decimal AnnualBenefitsForHusbGreater62(decimal currBeneHusb)
        {
            try
            {
                int intMonthToday, intMultiplyMonth, currBeneAnnual, intAgeHusbMonthtemp;
                intAgeHusbMonthtemp = intAgeHusbMonth;
                intMonthToday = DateTime.Now.Month;
                if (intAgeHusbDays < 28)
                    intAgeHusbMonthtemp -= 1;
                if (intMonthToday > intAgeHusbMonth)
                    intMultiplyMonth = (12 - intMonthToday) + intAgeHusbMonthtemp;
                else
                    intMultiplyMonth = intAgeHusbMonth - intMonthToday;
                currBeneAnnual = Convert.ToInt32(currBeneHusb * intMultiplyMonth);
                return currBeneAnnual;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// code to calculate wife Annual Income value for age greater than 62
        /// </summary>
        /// <param name="currBeneWife"></param>
        /// <param name="currBeneAnnual"></param>
        /// <returns></returns>
        private decimal AnnualBenefitsForWifeGreater62(decimal currBeneHusb)
        {
            try
            {
                int intMonthToday, intMultiplyMonth, currBeneAnnual, intAgeWifeMonthtemp = 0;
                intAgeWifeMonthtemp = intAgeWifeMonth;
                intMonthToday = DateTime.Now.Month;
                if (intAgeWifeDays < 28)
                    intAgeWifeMonthtemp -= 1;
                if (intMonthToday > intAgeWifeMonth)
                    intMultiplyMonth = (12 - intMonthToday) + intAgeWifeMonthtemp;
                else
                    intMultiplyMonth = intAgeWifeMonth - intMonthToday;
                currBeneAnnual = Convert.ToInt32(currBeneHusb * intMultiplyMonth);
                return currBeneAnnual;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// code to highlight the numbers in all the grids for all the strategies
        /// </summary>
        /// <param name="Grid1"></param>
        /// <param name="soloType"></param>
        public void Highlight(GridView Grid1, calc_solo soloType, string gridName)
        {
            try
            {
                //bool values used to highlight the values only in grid or to run the loops only once
                bool boolToRunLoopOnlyOnce = true, boolToRunLoopOnlyOnce1 = true, boolFirstAnnualIncome = false;
                string strCummulativeIncome = string.Empty;
                // bool boolTofindValueForHusb = false, boolTofindValueForWife = false;
                string combinedOncomeAge = string.Empty, startBenefitAgeWife = string.Empty, startBenefitAgeHusb = string.Empty;
                string strAnnualIncomeAge = string.Empty;//set value of starting benefit age to check with annual income age to show/hide paid to wait number
                string newCombinedOncomeAge = string.Empty;
                string newAnnualIncomeAge = string.Empty;
                float fRndMnt, fRndMntHusb;
                float fFRAyr = FRA(birthYearWife);
                //get FRA month as per date of birth
                float iRndYr = Convert.ToInt32(fFRAyr);
                if (fFRAyr > iRndYr)
                    fRndMnt = (fFRAyr - iRndYr);
                else
                    fRndMnt = (iRndYr - fFRAyr);

                float fFRAyrHusb = FRA(birthYearHusb);
                //get FRA month as per date of birth
                float iRndYrHusb = Convert.ToInt32(fFRAyrHusb);
                if (fFRAyrHusb > iRndYrHusb)
                    fRndMntHusb = (fFRAyrHusb - iRndYrHusb);
                else
                    fRndMntHusb = (iRndYrHusb - fFRAyrHusb);

                foreach (GridViewRow gvr in Grid1.Rows)
                {
                    //highlight Married values in Grid
                    if (soloType == calc_solo.Married)
                    {
                        #region Married Style
                        #region Code to highlight the starting benefist and the combined benefits
                        if (decBeneHusb > decBeneWife)
                        {
                            //loop to highlight the cummulative value for higher earner reaching age 69 and if benefit not 0
                            if (gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number69) && gvr.Cells[5].Text != Constants.ZeroWithOutAsterisk)
                            {
                                strCummulativeIncome = gvr.Cells[0].Text;
                            }
                            //loop to highlight the starting value for husb
                            if (gvr.Cells[2].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[2].Text != Constants.ZeroWithAsterisk)
                            {
                                //bool value to run the loop only once
                                if (boolToRunLoopOnlyOnce)
                                {
                                    //to highlight the starting value fro husb
                                    gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[2].Font.Bold = true;
                                    //to highlight the age
                                    gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[0].Font.Bold = true;
                                    //bool value made false to run the loop only once
                                    boolToRunLoopOnlyOnce = false;
                                    startBenefitAgeWife = gvr.Cells[0].Text.Substring(3, 2);
                                }
                            }
                            //loop to highlight the starting value for wife
                            if (gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[1].Text != Constants.ZeroWithAsterisk)
                            {
                                //bool value to run the loop only once
                                if (boolToRunLoopOnlyOnce1)
                                {
                                    //to highlight the starting value for wife
                                    gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[1].Font.Bold = true;
                                    //to highlight the age
                                    gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[0].Font.Bold = true;
                                    //bool value made false to run the loop only once
                                    boolToRunLoopOnlyOnce1 = false;
                                    startBenefitAgeHusb = gvr.Cells[0].Text.Substring(0, 2);
                                }
                            }
                            //to highlight when change from spousal benefits to hybrid benefits for husb
                            if (combinedBenefts && gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(husbFRA.ToString()))
                            {
                                //to highlight the value for husb
                                gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[2].Font.Bold = true;
                                //to highlight the age
                                gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[1].Font.Bold = true;
                                gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[0].Font.Bold = true;
                                ClaimAndSuspendinFullAge = gvr.Cells[(int)disp_col.Age].Text;
                                ClaimAndSuspendinFullValue = Convert.ToDecimal(gvr.Cells[1].Text.Substring(1));
                                //bool value made false to run the loop only once
                                //combinedBenefts = false;
                            }
                        }
                        else if (decBeneHusb == decBeneWife)
                        {
                            //loop to highlight the starting value for husb
                            if (gvr.Cells[2].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[2].Text != Constants.ZeroWithAsterisk)
                            {
                                //bool value to run the loop only once
                                if (boolToRunLoopOnlyOnce)
                                {
                                    //to highlight the starting value fro husb
                                    gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[2].Font.Bold = true;
                                    //to highlight the age
                                    gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[0].Font.Bold = true;
                                    //bool value made false to run the loop only once
                                    boolToRunLoopOnlyOnce = false;
                                    startBenefitAgeWife = gvr.Cells[0].Text.Substring(3, 2);
                                }
                            }
                            //loop to highlight the starting value for wife
                            if (gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[1].Text != Constants.ZeroWithAsterisk)
                            {
                                //bool value to run the loop only once
                                if (boolToRunLoopOnlyOnce1)
                                {
                                    //to highlight the starting value for wife
                                    gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[1].Font.Bold = true;
                                    //to highlight the age
                                    gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[0].Font.Bold = true;
                                    //bool value made false to run the loop only once
                                    boolToRunLoopOnlyOnce1 = false;
                                    startBenefitAgeHusb = gvr.Cells[0].Text.Substring(0, 2);
                                }
                            }
                            if (Globals.Instance.BoolHighlightCummAtWife69Max && gridName == "Max")
                            {
                                if (gvr.Cells[0].Text.Substring(0, 2).Equals(Constants.Number69) && !gvr.Cells[5].Text.Equals(Constants.ZeroWithOutAsterisk))
                                {
                                    strCummulativeIncome = gvr.Cells[0].Text;
                                }
                            }
                            else
                            {
                                if (gvr.Cells[0].Text.Substring(3, 2).Equals(Constants.Number69) && !gvr.Cells[5].Text.Equals(Constants.ZeroWithOutAsterisk))
                                {
                                    strCummulativeIncome = gvr.Cells[0].Text;
                                }
                            }
                            if (combinedBenefts && gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(husbFRA.ToString()))
                            {
                                //to highlight the value for husb
                                gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[2].Font.Bold = true;
                                //to highlight the age
                                gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[1].Font.Bold = true;
                                gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[0].Font.Bold = true;
                                ClaimAndSuspendinFullAge = gvr.Cells[(int)disp_col.Age].Text;
                                ClaimAndSuspendinFullValue = Convert.ToDecimal(gvr.Cells[1].Text.Substring(1));
                                //bool value made false to run the loop only once
                                //combinedBenefts = false;
                            }
                        }
                        else
                        {   //loop to highlight the cummulative value for higher earner reaching age 69 and if benefit not 0
                            if (gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number69) && gvr.Cells[5].Text != Constants.ZeroWithOutAsterisk)
                            {
                                strCummulativeIncome = gvr.Cells[0].Text;
                            }
                            //loop to highlight the starting value for husb
                            if (gvr.Cells[2].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[2].Text != Constants.ZeroWithAsterisk)
                            {
                                //bool value to run the loop only once
                                if (boolToRunLoopOnlyOnce1)
                                {
                                    //to highlight the value for husb
                                    gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[2].Font.Bold = true;
                                    //to highlight the age
                                    gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[0].Font.Bold = true;
                                    //bool value made false to run the loop only once
                                    boolToRunLoopOnlyOnce1 = false;
                                    startBenefitAgeWife = gvr.Cells[0].Text.Substring(3, 2);
                                }
                            }
                            //loop to highlight the starting value for wife
                            if (gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[1].Text != Constants.ZeroWithAsterisk)
                            {
                                //bool value to run the loop only once
                                if (boolToRunLoopOnlyOnce)
                                {
                                    //to highlight the value for wife
                                    gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[1].Font.Bold = true;
                                    //to highlight the age
                                    gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[0].Font.Bold = true;
                                    //bool value made false to run the loop only once
                                    boolToRunLoopOnlyOnce = false;
                                    startBenefitAgeHusb = gvr.Cells[0].Text.Substring(0, 2);
                                }
                            }
                            //to highlight when change from spousal benefits to hybrid benefits for wife
                            if (combinedBenefts && gvr.Cells[2].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(wifeFRA.ToString()))
                            {
                                //to highlight the value for husb
                                gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[1].Font.Bold = true;
                                gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[2].Font.Bold = true;
                                //to highlight the age
                                gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[0].Font.Bold = true;
                                ClaimAndSuspendinFullAge = gvr.Cells[(int)disp_col.Age].Text;
                                ClaimAndSuspendinFullValue = Convert.ToDecimal(gvr.Cells[2].Text.Substring(1));
                            }
                        }
                        #endregion Code to highlight the starting benefist and the combined benefits

                        #region Common values for all strategis in Married
                        if (gridName.Equals("Full"))
                        {
                            if (gvr.Cells[0].Text.Substring(0, 2).Equals(Constants.Number70) && soloType == calc_solo.Married && Globals.Instance.StringSpousalChangeAgeHusb)
                            {
                                gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[2].Font.Bold = true;
                            }
                            if (gvr.Cells[0].Text.Substring(3, 2).Equals(Constants.Number70) && soloType == calc_solo.Married && Globals.Instance.StringSpousalChangeAgeWife)
                            {
                                gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[1].Font.Bold = true;
                            }
                        }
                        if (gridName.Equals("Max"))
                        {
                            if (gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number70) && Globals.Instance.BoolMaxStrategyAt70Wife)
                            {
                                gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[1].Font.Bold = true;
                                gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[0].Font.Bold = true;
                                Globals.Instance.StringSpousalChangeAgeWife = false;
                            }
                            if (gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number70) && Globals.Instance.BoolMaxStrategyAt70Husb)
                            {
                                gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[2].Font.Bold = true;
                                gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[0].Font.Bold = true;
                                Globals.Instance.StringSpousalChangeAgeHusb = false;
                            }
                        }
                        if (fBuildGridat70SpousalHusb && (gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number70)))
                        {
                            gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[2].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                            fBuildGridat70SpousalHusb = false;
                        }
                        if (fBuildGridat70SpousalWife && (gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number70)))
                        {
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                            fBuildGridat70SpousalWife = false;
                        }
                        //loop to highlight the wife value at age 70 in Full Retirement and CLaim and Suspend
                        if ((buildGridAt70) && (gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number70)))
                        {
                            gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[2].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                        }
                        //loop to highlight the husb value at age 70 in strategy1 and 2
                        if ((fBuildGridat70) && (gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number70)))
                        {
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                        }
                        //loop to highlight the husb value at age 70 in Full Retirement and CLaim and Suspend
                        if ((at70) && (gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number70)))
                        {
                            gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[2].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                        }
                        //loop to highlight the husb value at age 70 in strategy1 and 2
                        if ((FbuildGridAt70) && (gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number70)))
                        {
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                        }
                        //loop to highlight when there appears a asterik for husb
                        if (gvr.Cells[1].Text.Equals(Constants.ZeroWithAsterisk))
                        {
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                        }
                        //loop to highlight when there appears a asterik for wife
                        if (gvr.Cells[2].Text.Equals(Constants.ZeroWithAsterisk))
                        {
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                        }
                        //loop to highlight when the spousal to working change in Fullretirement for husb
                        if (Globals.Instance.CurrHusValue && gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(wifeFRA.ToString()) && gvr.Cells[1].Text != Constants.ZeroWithAsterisk && gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk)
                        {
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                            ClaimAndSuspendinFullAge = gvr.Cells[(int)disp_col.Age].Text;
                            ClaimAndSuspendinFullValue = Convert.ToDecimal(gvr.Cells[2].Text.Substring(1));
                        }
                        //loop to highlight when the spousal to working change in Fullretirement for wife
                        if (Globals.Instance.CurrWifeValue && gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(husbFRA.ToString()) && gvr.Cells[2].Text != Constants.ZeroWithAsterisk && gvr.Cells[2].Text != Constants.ZeroWithOutAsterisk)
                        {
                            gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[2].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                            ClaimAndSuspendinFullAge = gvr.Cells[(int)disp_col.Age].Text;
                            ClaimAndSuspendinFullValue = Convert.ToDecimal(gvr.Cells[1].Text.Substring(1));
                        }
                        //code to highlight when SSA adjustment done for husb
                        if (gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(workingBenefitsForHusb) && showWorkingBenefitsForHusb)
                        {
                            gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[2].Font.Bold = true;
                            showWorkingBenefitsForHusb = false;
                        }
                        //code to highlight when SSA adjustment done for wife
                        if (gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(workingBenefitsForWife) && showWorkingBenefitsForWife)
                        {
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].Font.Bold = true;
                            showWorkingBenefitsForWife = false;
                        }
                        if (gvr.Cells[0].BackColor == System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor))
                        {
                            combinedOncomeAge = gvr.Cells[0].Text;
                        }
                        #endregion Common values for all strategis in Married
                        #endregion Married Style
                    }
                    //highlight Divorce values in Grid
                    if (soloType == calc_solo.Divorced)
                    {
                        #region Divorce Style
                        //to highlight the starting benefits value
                        if (gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[1].Text != Constants.ZeroWithAsterisk)
                        {
                            if (boolToRunLoopOnlyOnce)
                            {
                                gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[1].Font.Bold = true;
                                gvr.Cells[0].Font.Bold = true;
                                startBenefitAgeWife = gvr.Cells[0].Text;
                                boolToRunLoopOnlyOnce = false;
                            }
                        }
                        //to highlight the value at age 70
                        if (dat70 && (gvr.Cells[(int)disp_col.Age].Text.Equals(Constants.Number70)))
                        {
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].Font.Bold = true;
                            gvr.Cells[0].Font.Bold = true;
                            dat70 = false;
                        }
                        if (gvr.Cells[(int)disp_col.Age].Text.Equals(DivorceSpousalBenefitChange) && gvr.Cells[(int)disp_col.WifeInc].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[(int)disp_col.WifeInc].Text != Constants.ZeroWithAsterisk)
                        {
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].Font.Bold = true;
                            gvr.Cells[0].Font.Bold = true;

                        }
                        if (gvr.Cells[0].BackColor == System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor))
                        {
                            //if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                            //{
                            //    newCombinedOncomeAgeDivorce = gvr.Cells[0].Text;
                            //}
                            //else
                            //{
                            //    if ((int.Parse(newCombinedOncomeAgeDivorce) + 1) < int.Parse(gvr.Cells[0].Text))
                            //    {
                            //        newCombinedOncomeAgeDivorce = gvr.Cells[0].Text;
                            //    }
                            //}
                            newCombinedOncomeAgeDivorce = gvr.Cells[0].Text;
                        }
                        #endregion Divorce Style
                    }
                    //highlight Widow values in Grid
                    if (soloType == calc_solo.Widowed)
                    {
                        Customers Customer = new Customers();
                        string WifraAtFra = Customer.WidowFRA(Convert.ToInt32(birthYearWife)).Substring(0, 2);
                        #region Widow Style
                        //highlight the cummulative value at age 69
                        //if (newDat70)
                        //{
                        //    if (gvr.Cells[(int)disp_col.Age].Text.ToString().Equals(Constants.Number69) && gvr.Cells[3].Text.ToString() != Constants.ZeroWithOutAsterisk)
                        //    {
                        //        gvr.Cells[3].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                        //        gvr.Cells[3].Font.Bold = true;
                        //        newDat70 = false;
                        //    }
                        //}
                        //else
                        //{
                        //    //highlight value change at FRA
                        //    if (gvr.Cells[(int)disp_col.Age].Text.ToString().Equals((wifeFRA - 1).ToString()) && gvr.Cells[3].Text.ToString() != Constants.ZeroWithOutAsterisk)
                        //    {
                        //        gvr.Cells[3].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                        //        gvr.Cells[3].Font.Bold = true;
                        //    }
                        //}
                        //highlight starting values
                        if (gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[1].Text != Constants.ZeroWithAsterisk)
                        {
                            if (boolToRunLoopOnlyOnce)
                            {
                                gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[1].Font.Bold = true;
                                gvr.Cells[0].Font.Bold = true;
                                startBenefitAgeWife = gvr.Cells[0].Text;
                                boolToRunLoopOnlyOnce = false;
                            }
                        }
                        //highlight value at age 70
                        if (dat70 && (gvr.Cells[(int)disp_col.Age].Text.Equals(Constants.Number70)))
                        {
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].Font.Bold = true;
                            gvr.Cells[0].Font.Bold = true;
                            dat70 = false;
                        }
                        //highlight if change at age 66 of FRA from buildsingle
                        if (wat66 && (gvr.Cells[(int)disp_col.Age].Text.Equals(WifraAtFra.ToString())) && gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk)
                        {
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].Font.Bold = true;
                            gvr.Cells[0].Font.Bold = true;

                            wat66 = false;
                        }
                        //highlight if change at age 66 of FRA from buildsinglewidow
                        if (swat66 && (gvr.Cells[(int)disp_col.Age].Text.Equals(WifraAtFra.ToString())) && gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk)
                        {
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].Font.Bold = true;
                            gvr.Cells[0].Font.Bold = true;
                            swat66 = false;
                        }
                        if (gvr.Cells[0].BackColor == System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor))
                        {
                            //if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                            //{
                            newCombinedOncomeAgeDivorce = gvr.Cells[0].Text;
                            //}
                            //else
                            //{
                            //    if ((int.Parse(newCombinedOncomeAgeDivorce) + 1) < int.Parse(gvr.Cells[0].Text))
                            //    {
                            //        newCombinedOncomeAgeDivorce = gvr.Cells[0].Text;
                            //    }
                            //}
                        }
                        #endregion Widow Style
                    }
                    //highlight Single values in Grid
                    if (soloType == calc_solo.Single)
                    {
                        #region Single Style

                        if (soloType.ToString() == Constants.Single)
                        {
                            //highlight cummulative value at age 69
                            //if (gvr.Cells[(int)disp_col.Age].Text.ToString().Equals(Constants.Number69) && gvr.Cells[3].Text.ToString() != Constants.ZeroWithOutAsterisk)
                            //{
                            //    gvr.Cells[3].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            //    gvr.Cells[3].Font.Bold = true;
                            //}
                            //highlight starting values
                            if (gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[1].Text != Constants.ZeroWithAsterisk)
                            {
                                if (boolToRunLoopOnlyOnce)
                                {
                                    gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[1].Font.Bold = true;
                                    gvr.Cells[0].Font.Bold = true;
                                    boolToRunLoopOnlyOnce = false;
                                }
                            }
                            if (gvr.Cells[0].BackColor == System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor))
                            {
                                // if (newCombinedOncomeAgeDivorce.Equals(string.Empty))
                                //{
                                newCombinedOncomeAgeDivorce = gvr.Cells[0].Text;
                                //}
                                //else
                                //{
                                //    if ((int.Parse(newCombinedOncomeAgeDivorce) + 1) < int.Parse(gvr.Cells[0].Text))
                                //    {
                                //        newCombinedOncomeAgeDivorce = gvr.Cells[0].Text;
                                //    }
                                //}
                            }
                        }
                        #endregion Single Style
                    }
                }

                #region loop run for the annual benefits and cummulative values for Married and Divorce
                foreach (GridViewRow gvr in Grid1.Rows)
                {
                    string regExp = "[^\\w]";
                    if (soloType == calc_solo.Married)
                    {
                        if (fRndMntHusb > 0 && gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(startBenefitAgeWife) && startBenefitAgeWife.Equals(combinedOncomeAge.Substring(3, 2)) && husbFRA >= int.Parse(startBenefitAgeWife) && int.Parse(startBenefitAgeWife) < int.Parse(Constants.Number70))
                            newCombinedOncomeAge = (int.Parse(combinedOncomeAge.Substring(0, 2)) + 1).ToString() + "/" + (int.Parse(combinedOncomeAge.Substring(3, 2)) + 1).ToString();
                        else if (fRndMnt > 0 && gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(startBenefitAgeHusb) && startBenefitAgeHusb.Equals(combinedOncomeAge.Substring(0, 2)) && wifeFRA >= int.Parse(startBenefitAgeHusb) && int.Parse(startBenefitAgeHusb) < int.Parse(Constants.Number70))
                            newCombinedOncomeAge = (int.Parse(combinedOncomeAge.Substring(0, 2)) + 1).ToString() + "/" + (int.Parse(combinedOncomeAge.Substring(3, 2)) + 1).ToString();
                        else
                        {
                            if (newCombinedOncomeAge.Equals(string.Empty))
                                newCombinedOncomeAge = combinedOncomeAge;
                        }
                    }
                    if (soloType == calc_solo.Married && gvr.Cells[(int)disp_col.Age].Text.Equals(strCummulativeIncome))
                    {
                        gvr.Cells[5].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                        gvr.Cells[5].Font.Bold = true;
                        cumm69 = decimal.Parse((gvr.Cells[5].Text.Substring(1)));
                        strCummulativeIncome = string.Empty;
                    }
                    if (gvr.Cells[(int)disp_col.Age].Text == newCombinedOncomeAge && soloType == calc_solo.Married)
                    {
                        if (gvr.Cells[4].Text.Contains(Constants.DoubleAsterisk) || gvr.Cells[4].Text.Contains(Constants.SingleAsterisk) || gvr.Cells[4].Text.Contains(Constants.SingleHash))
                        {
                            newCombinedOncomeAge = (int.Parse(combinedOncomeAge.Substring(0, 2)) + 1).ToString() + "/" + (int.Parse(combinedOncomeAge.Substring(3, 2)) + 1).ToString();
                        }
                        else
                        {
                            gvr.Cells[4].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[4].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                            valueAtAge70 = decimal.Parse(Regex.Replace(gvr.Cells[4].Text, regExp, ""));
                            CombinedIncomeAge = gvr.Cells[0].Text;
                            newCombinedOncomeAge = string.Empty;
                        }
                    }
                    if (soloType != calc_solo.Married)
                    {
                        if (gvr.Cells[(int)disp_col.Age].Text == newCombinedOncomeAgeDivorce && (soloType != calc_solo.Married))
                        {
                            if (!boolFirstAnnualIncome)
                            {
                                strAnnualIncomeAge = newCombinedOncomeAgeDivorce;//Set annual income age value to check with startin benefit age value
                                boolFirstAnnualIncome = true;
                            }
                            if (gvr.Cells[2].Text.Contains(Constants.DoubleAsterisk) || gvr.Cells[2].Text.Contains(Constants.SingleAsterisk) || gvr.Cells[2].Text.Contains(Constants.SingleHash))
                            {
                                newCombinedOncomeAgeDivorce = (int.Parse(newCombinedOncomeAgeDivorce) + 1).ToString();
                                newAnnualIncomeAge = newCombinedOncomeAgeDivorce;
                            }
                            else
                            {
                                gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[2].Font.Bold = true;
                                newAnnualIncomeAge = gvr.Cells[0].Text;
                                DivorceAnnualValue = decimal.Parse(Regex.Replace(gvr.Cells[2].Text, regExp, ""));
                                newCombinedOncomeAgeDivorce = string.Empty;
                                WidowAnnualIncomeValue = decimal.Parse(Regex.Replace(gvr.Cells[2].Text, regExp, ""));
                                SingleAnualValue = decimal.Parse(Regex.Replace(gvr.Cells[2].Text, regExp, ""));
                            }
                        }
                    }
                }
                foreach (GridViewRow gvr in Grid1.Rows)
                {
                    if (soloType != calc_solo.Married && gvr.Cells[0].BackColor == System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor))
                    { AgeAtLastChange = gvr.Cells[0].Text; ValueAtLastChange = decimal.Parse(gvr.Cells[1].Text.Substring(1)); }
                    if (gvr.Cells[2].BackColor == System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor))
                        AnnualIncomeAge = gvr.Cells[0].Text;
                    if (soloType == calc_solo.Divorced || soloType == calc_solo.Widowed)
                    {
                        if (!String.IsNullOrEmpty(newAnnualIncomeAge))
                        {
                            if (gvr.Cells[(int)disp_col.Age].Text.Equals((int.Parse(newAnnualIncomeAge) - 1).ToString()) && !gvr.Cells[3].Text.Equals(Constants.ZeroWithOutAsterisk) && !gvr.Cells[2].Text.Contains(Constants.SingleHash) && int.Parse(startBenefitAgeWife) < int.Parse(gvr.Cells[0].Text))
                            {
                                gvr.Cells[3].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[3].Font.Bold = true;
                                DCummValue = decimal.Parse(gvr.Cells[3].Text.Substring(1));
                                WCummValue = decimal.Parse(gvr.Cells[3].Text.Substring(1));
                                newAnnualIncomeAge = string.Empty;
                            }
                        }
                    }
                }
                #endregion loop run for the annual benefits and cummulative values for Married and Divorce
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// code to highlight the numbers in all the grids for all the strategies
        /// </summary>
        /// <param name="Grid1"></param>
        /// <param name="soloType"></param>
        public void HighlightRestrictIncome(GridView Grid1, calc_solo soloType, string gridName)
        {
            try
            {
                //bool values used to highlight the values only in grid or to run the loops only once
                bool boolToRunLoopOnlyOnce = true, boolToRunLoopOnlyOnce1 = true, boolIsSwithForDivorcWidowed = false, boolFirstAnnualIncome = false;
                string strCummulativeIncome = string.Empty;
                int intLastValChangRowNo = 0;
                // bool boolTofindValueForHusb = false, boolTofindValueForWife = false;
                string combinedOncomeAge = string.Empty, startBenefitAgeWife = string.Empty, startBenefitAgeHusb = string.Empty;
                string strAnnualIncomeAge = string.Empty;//set value of starting benefit age to check with annual income age to show/hide paid to wait number
                string newCombinedOncomeAge = string.Empty;
                string newAnnualIncomeAge = string.Empty;
                float fRndMnt, fRndMntHusb;
                float fFRAyr = FRA(birthYearWife);
                string strCombineIncomeAges = string.Empty, strCummIncomeAge = string.Empty;//For Restricted
                //get FRA month as per date of birth
                float iRndYr = Convert.ToInt32(fFRAyr);
                if (fFRAyr > iRndYr)
                    fRndMnt = (fFRAyr - iRndYr);
                else
                    fRndMnt = (iRndYr - fFRAyr);

                float fFRAyrHusb = FRA(birthYearHusb);
                //get FRA month as per date of birth
                float iRndYrHusb = Convert.ToInt32(fFRAyrHusb);
                if (fFRAyrHusb > iRndYrHusb)
                    fRndMntHusb = (fFRAyrHusb - iRndYrHusb);
                else
                    fRndMntHusb = (iRndYrHusb - fFRAyrHusb);

                #region Commented old married case
                foreach (GridViewRow gvr in Grid1.Rows)
                {
                    //highlight Married values in Grid
                    if (soloType == calc_solo.Married)
                    {
                        #region Married Style
                        #region Code to highlight the starting benefist and the combined benefits
                        if (decBeneHusb > decBeneWife)
                        {
                            //loop to highlight the cummulative value for higher earner reaching age 69 and if benefit not 0
                            if (gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number69) && gvr.Cells[5].Text != Constants.ZeroWithOutAsterisk)
                            {
                                strCummulativeIncome = gvr.Cells[0].Text;
                            }
                            //loop to highlight the starting value for husb
                            if (gvr.Cells[2].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[2].Text != Constants.ZeroWithAsterisk)
                            {
                                //bool value to run the loop only once
                                if (boolToRunLoopOnlyOnce)
                                {
                                    //to highlight the starting value fro husb
                                    gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[2].Font.Bold = true;
                                    //to highlight the age
                                    gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[0].Font.Bold = true;
                                    //bool value made false to run the loop only once
                                    boolToRunLoopOnlyOnce = false;
                                    startBenefitAgeWife = gvr.Cells[0].Text.Substring(3, 2);
                                }
                            }
                            //loop to highlight the starting value for wife
                            if (gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[1].Text != Constants.ZeroWithAsterisk)
                            {
                                //bool value to run the loop only once
                                if (boolToRunLoopOnlyOnce1)
                                {
                                    //to highlight the starting value for wife
                                    gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[1].Font.Bold = true;
                                    //to highlight the age
                                    gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[0].Font.Bold = true;
                                    //bool value made false to run the loop only once
                                    boolToRunLoopOnlyOnce1 = false;
                                    startBenefitAgeHusb = gvr.Cells[0].Text.Substring(0, 2);
                                }
                            }
                            //to highlight when change from spousal benefits to hybrid benefits for husb
                            if (combinedBenefts && gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(husbFRA.ToString()))
                            {
                                //to highlight the value for husb
                                gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[2].Font.Bold = true;
                                //to highlight the age
                                gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[1].Font.Bold = true;
                                gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[0].Font.Bold = true;
                                ClaimAndSuspendinFullAge = gvr.Cells[(int)disp_col.Age].Text;
                                ClaimAndSuspendinFullValue = Convert.ToDecimal(gvr.Cells[1].Text.Substring(1));
                                //bool value made false to run the loop only once
                                //combinedBenefts = false;
                            }
                        }
                        else if (decBeneHusb == decBeneWife)
                        {
                            //loop to highlight the starting value for husb
                            if (gvr.Cells[2].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[2].Text != Constants.ZeroWithAsterisk)
                            {
                                //bool value to run the loop only once
                                if (boolToRunLoopOnlyOnce)
                                {
                                    //to highlight the starting value fro husb
                                    gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[2].Font.Bold = true;
                                    //to highlight the age
                                    gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[0].Font.Bold = true;
                                    //bool value made false to run the loop only once
                                    boolToRunLoopOnlyOnce = false;
                                    startBenefitAgeWife = gvr.Cells[0].Text.Substring(3, 2);
                                }
                            }
                            //loop to highlight the starting value for wife
                            if (gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[1].Text != Constants.ZeroWithAsterisk)
                            {
                                //bool value to run the loop only once
                                if (boolToRunLoopOnlyOnce1)
                                {
                                    //to highlight the starting value for wife
                                    gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[1].Font.Bold = true;
                                    //to highlight the age
                                    gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[0].Font.Bold = true;
                                    //bool value made false to run the loop only once
                                    boolToRunLoopOnlyOnce1 = false;
                                    startBenefitAgeHusb = gvr.Cells[0].Text.Substring(0, 2);
                                }
                            }
                            if (Globals.Instance.BoolHighlightCummAtWife69Max && gridName == "Max")
                            {
                                if (gvr.Cells[0].Text.Substring(0, 2).Equals(Constants.Number69) && !gvr.Cells[5].Text.Equals(Constants.ZeroWithOutAsterisk))
                                {
                                    strCummulativeIncome = gvr.Cells[0].Text;
                                }
                            }
                            else
                            {
                                if (gvr.Cells[0].Text.Substring(3, 2).Equals(Constants.Number69) && !gvr.Cells[5].Text.Equals(Constants.ZeroWithOutAsterisk))
                                {
                                    strCummulativeIncome = gvr.Cells[0].Text;
                                }
                            }
                            if (combinedBenefts && gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(husbFRA.ToString()))
                            {
                                //to highlight the value for husb
                                gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[2].Font.Bold = true;
                                //to highlight the age
                                gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[1].Font.Bold = true;
                                gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[0].Font.Bold = true;
                                ClaimAndSuspendinFullAge = gvr.Cells[(int)disp_col.Age].Text;
                                ClaimAndSuspendinFullValue = Convert.ToDecimal(gvr.Cells[1].Text.Substring(1));
                                //bool value made false to run the loop only once
                                //combinedBenefts = false;
                            }
                        }
                        else
                        {   //loop to highlight the cummulative value for higher earner reaching age 69 and if benefit not 0
                            if (gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number69) && gvr.Cells[5].Text != Constants.ZeroWithOutAsterisk)
                            {
                                strCummulativeIncome = gvr.Cells[0].Text;
                            }
                            //loop to highlight the starting value for husb
                            if (gvr.Cells[2].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[2].Text != Constants.ZeroWithAsterisk)
                            {
                                //bool value to run the loop only once
                                if (boolToRunLoopOnlyOnce1)
                                {
                                    //to highlight the value for husb
                                    gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[2].Font.Bold = true;
                                    //to highlight the age
                                    gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[0].Font.Bold = true;
                                    //bool value made false to run the loop only once
                                    boolToRunLoopOnlyOnce1 = false;
                                    startBenefitAgeWife = gvr.Cells[0].Text.Substring(3, 2);
                                }
                            }
                            //loop to highlight the starting value for wife
                            if (gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[1].Text != Constants.ZeroWithAsterisk)
                            {
                                //bool value to run the loop only once
                                if (boolToRunLoopOnlyOnce)
                                {
                                    //to highlight the value for wife
                                    gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[1].Font.Bold = true;
                                    //to highlight the age
                                    gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[0].Font.Bold = true;
                                    //bool value made false to run the loop only once
                                    boolToRunLoopOnlyOnce = false;
                                    startBenefitAgeHusb = gvr.Cells[0].Text.Substring(0, 2);
                                }
                            }
                            //to highlight when change from spousal benefits to hybrid benefits for wife
                            if (combinedBenefts && gvr.Cells[2].Text != Constants.ZeroWithOutAsterisk && gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(wifeFRA.ToString()))
                            {
                                //to highlight the value for husb
                                gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[1].Font.Bold = true;
                                gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[2].Font.Bold = true;
                                //to highlight the age
                                gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[0].Font.Bold = true;
                                ClaimAndSuspendinFullAge = gvr.Cells[(int)disp_col.Age].Text;
                                ClaimAndSuspendinFullValue = Convert.ToDecimal(gvr.Cells[2].Text.Substring(1));
                            }
                        }
                        #endregion Code to highlight the starting benefist and the combined benefits

                        #region Common values for all strategis in Married
                        if (gridName.Equals("Full"))
                        {
                            if (gvr.Cells[0].Text.Substring(0, 2).Equals(Constants.Number70) && soloType == calc_solo.Married && Globals.Instance.StringSpousalChangeAgeHusb)
                            {
                                gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[2].Font.Bold = true;
                            }
                            if (gvr.Cells[0].Text.Substring(3, 2).Equals(Constants.Number70) && soloType == calc_solo.Married && Globals.Instance.StringSpousalChangeAgeWife)
                            {
                                gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[1].Font.Bold = true;
                            }
                        }
                        if (gridName.Equals("Max"))
                        {
                            if (gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number70) && Globals.Instance.BoolMaxStrategyAt70Wife)
                            {
                                gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[1].Font.Bold = true;
                                gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[0].Font.Bold = true;
                                Globals.Instance.StringSpousalChangeAgeWife = false;
                            }
                            if (gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number70) && Globals.Instance.BoolMaxStrategyAt70Husb)
                            {
                                gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[2].Font.Bold = true;
                                gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[0].Font.Bold = true;
                                Globals.Instance.StringSpousalChangeAgeHusb = false;
                            }
                        }
                        if (fBuildGridat70SpousalHusb && (gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number70)))
                        {
                            gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[2].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                            fBuildGridat70SpousalHusb = false;
                        }
                        if (fBuildGridat70SpousalWife && (gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number70)))
                        {
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                            fBuildGridat70SpousalWife = false;
                        }
                        //loop to highlight the wife value at age 70 in Full Retirement and CLaim and Suspend
                        if ((buildGridAt70) && (gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number70)))
                        {
                            gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[2].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                        }
                        //loop to highlight the husb value at age 70 in strategy1 and 2
                        if ((fBuildGridat70) && (gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number70)))
                        {
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;

                            if (RestrictAppIncomeProp.Instance.BoolRestrictNewCase)
                            {
                                gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[2].Font.Bold = true;
                                RestrictAppIncomeProp.Instance.strSwichdWorkValue = gvr.Cells[2].Text;
                            }
                        }
                        //loop to highlight the husb value at age 70 in Full Retirement and CLaim and Suspend
                        if ((at70) && (gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(Constants.Number70)))
                        {
                            gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[2].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;

                            if (RestrictAppIncomeProp.Instance.BoolRestrictNewCase)
                            {
                                gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[1].Font.Bold = true;
                                RestrictAppIncomeProp.Instance.strSwichdWorkValue = gvr.Cells[1].Text;
                            }

                        }
                        //loop to highlight the husb value at age 70 in strategy1 and 2
                        if ((FbuildGridAt70) && (gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(Constants.Number70)))
                        {
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                        }
                        //loop to highlight when there appears a asterik for husb
                        if (gvr.Cells[1].Text.Equals(Constants.ZeroWithAsterisk))
                        {
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                        }
                        //loop to highlight when there appears a asterik for wife
                        if (gvr.Cells[2].Text.Equals(Constants.ZeroWithAsterisk))
                        {
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                        }
                        //loop to highlight when the spousal to working change in Fullretirement for husb
                        if (Globals.Instance.CurrHusValue && gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(wifeFRA.ToString()) && gvr.Cells[1].Text != Constants.ZeroWithAsterisk && gvr.Cells[1].Text != Constants.ZeroWithOutAsterisk)
                        {
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                            ClaimAndSuspendinFullAge = gvr.Cells[(int)disp_col.Age].Text;
                            ClaimAndSuspendinFullValue = Convert.ToDecimal(gvr.Cells[2].Text.Substring(1));
                        }
                        //loop to highlight when the spousal to working change in Fullretirement for wife
                        if (Globals.Instance.CurrWifeValue && gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(husbFRA.ToString()) && gvr.Cells[2].Text != Constants.ZeroWithAsterisk && gvr.Cells[2].Text != Constants.ZeroWithOutAsterisk)
                        {
                            gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[2].Font.Bold = true;
                            gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[0].Font.Bold = true;
                            ClaimAndSuspendinFullAge = gvr.Cells[(int)disp_col.Age].Text;
                            ClaimAndSuspendinFullValue = Convert.ToDecimal(gvr.Cells[1].Text.Substring(1));
                        }
                        //code to highlight when SSA adjustment done for husb
                        if (gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(workingBenefitsForHusb) && showWorkingBenefitsForHusb)
                        {
                            gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[2].Font.Bold = true;
                            showWorkingBenefitsForHusb = false;
                        }
                        //code to highlight when SSA adjustment done for wife
                        if (gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(workingBenefitsForWife) && showWorkingBenefitsForWife)
                        {
                            gvr.Cells[1].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[1].Font.Bold = true;
                            showWorkingBenefitsForWife = false;
                        }
                        //To detect last value change cell
                        if (gvr.Cells[0].BackColor == System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor))
                        {
                            combinedOncomeAge = gvr.Cells[0].Text;
                            intLastValChangRowNo = gvr.RowIndex;
                        }
                        #endregion Common values for all strategis in Married



                        #endregion Married Style
                    }
                }
                #endregion Commented old married case

                #region Highlight Cumulative income for Restricted app

                if (soloType == calc_solo.Married)
                {
                    bool boolGoForNext = false;
                    foreach (GridViewRow gridRow in Grid1.Rows)
                    {
                        //Highlight cummulative income
                        if (gridName.Equals("Full"))
                        {
                            int intWifeLoopAge = int.Parse(gridRow.Cells[0].Text.Substring(0, 2));
                            int intHusbLoopAge = int.Parse(gridRow.Cells[0].Text.Substring(3, 2));
                            if (Globals.Instance.BoolIsHusbandClaimedBenefit || (RestrictAppIncomeProp.Instance.BoolRestrictSpecialCase && decBeneHusb >= decBeneWife))
                            {
                                //Highlight all spousal benefit values of wife upto age 69
                                if (intWifeLoopAge >= int.Parse(startBenefitAgeHusb) && intWifeLoopAge <= 69 && !Globals.Instance.BoolSkipWHBSwitchRestrict)
                                {
                                    //Apply border for cells from 66-69
                                    gridRow.Cells[1].Style.Add("border", "3px solid #228B22");
                                    gridRow.Cells[1].Font.Bold = true;
                                    //Display only single box from 66 to 69 (remove bottom border of cells expect 69)
                                    if (intWifeLoopAge < 69)
                                        gridRow.Cells[1].Style.Add("border-bottom-style", "hidden");
                                }
                                //Detect the cummulative income value
                                if (!Globals.Instance.BoolSkipWHBSwitchRestrict)
                                {
                                    if (gridRow.Cells[0].Text.Substring(0, 2).Equals(Constants.Number69) && !gridRow.Cells[5].Text.Equals(Constants.ZeroWithOutAsterisk))
                                        strCummulativeIncome = gridRow.Cells[0].Text;
                                }
                                else
                                {
                                    if (gridRow.RowIndex == (intLastValChangRowNo - 1) && !gridRow.Cells[5].Text.Equals(Constants.ZeroWithOutAsterisk))
                                        strCummulativeIncome = gridRow.Cells[0].Text;
                                }
                                //If claimed user switched at age 70 if his spousal benefit is greater than current benefit
                                if (gridRow.Cells[0].Text.Substring(0, 2).Equals(Constants.Number70) && RestrictAppIncomeProp.Instance.boolSwitchToWorkHistory)
                                {
                                    RestrictAppIncomeProp.Instance.strSwichdWorkValue = gridRow.Cells[2].Text;
                                    gridRow.Cells[2].BackColor = ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gridRow.Cells[2].Font.Bold = true;
                                }
                            }
                            else if (Globals.Instance.BoolIsWifeClaimedBenefit || (RestrictAppIncomeProp.Instance.BoolRestrictSpecialCase && decBeneHusb < decBeneWife))
                            {
                                //Highlight all spousal benefit values of Husband upto age 69
                                if (intHusbLoopAge >= int.Parse(startBenefitAgeWife) && intHusbLoopAge <= 69 && !Globals.Instance.BoolSkipWHBSwitchRestrict)
                                {
                                    //Apply border for cells from 66-69
                                    gridRow.Cells[2].Style.Add("border", "3px solid #228B22");
                                    gridRow.Cells[2].Font.Bold = true;

                                    //Display only single box from 66 to 69 (remove bottom border of cells expect 69)
                                    if (intHusbLoopAge < 69)
                                        gridRow.Cells[2].Style.Add("border-bottom-style", "hidden");
                                }
                                //Detect the cummulative income value
                                if (!Globals.Instance.BoolSkipWHBSwitchRestrict)
                                {
                                    if (gridRow.Cells[0].Text.Substring(3, 2).Equals(Constants.Number69) && !gridRow.Cells[5].Text.Equals(Constants.ZeroWithOutAsterisk))
                                        strCummulativeIncome = gridRow.Cells[0].Text;
                                }
                                else
                                {
                                    if (gridRow.RowIndex == (intLastValChangRowNo - 1) && !gridRow.Cells[5].Text.Equals(Constants.ZeroWithOutAsterisk))
                                        strCummulativeIncome = gridRow.Cells[0].Text;
                                }
                                //If claimed user switched at age 70 if his spousal benefit is greater than current benefit
                                if (gridRow.Cells[0].Text.Substring(3, 2).Equals(Constants.Number70) && RestrictAppIncomeProp.Instance.boolSwitchToWorkHistory)
                                {
                                    RestrictAppIncomeProp.Instance.strSwichdWorkValue = gridRow.Cells[1].Text;
                                    gridRow.Cells[1].BackColor = ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gridRow.Cells[1].Font.Bold = true;
                                }
                            }
                        }
                        //For other three strategies
                        else
                        {
                            if (gridRow.Cells[0].BackColor == ColorTranslator.FromHtml(Constants.gridBackgroundColor))
                            {
                                if (gridRow.Cells[4].Text.Contains("#") || gridRow.Cells[4].Text.Contains("*"))
                                    boolGoForNext = true;
                                else
                                    strCombineIncomeAges = gridRow.Cells[0].Text.Substring(0, 2);
                                strCummIncomeAge = (int.Parse(gridRow.Cells[0].Text.Substring(0, 2)) - 1).ToString();
                            }

                            if (!gridRow.Cells[4].Text.Contains("#") && !gridRow.Cells[4].Text.Contains("*") && boolGoForNext)
                            {
                                strCombineIncomeAges = gridRow.Cells[0].Text.Substring(0, 2);
                                boolGoForNext = false;
                            }
                        }
                    }


                    //Highlight Cummulative and combined income ages for other three strategies
                    if (!gridName.Equals("Full"))
                    {
                        foreach (GridViewRow gridRow in Grid1.Rows)
                        {
                            string regExp = "[^\\w]";
                            string strBenefit = string.Empty;
                            //Highlight cummulative income value
                            if (gridRow.Cells[0].Text.Substring(0, 2).Equals(strCummIncomeAge) && !gridRow.Cells[5].Text.Equals(Constants.ZeroWithOutAsterisk))
                            {
                                gridRow.Cells[5].BackColor = ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gridRow.Cells[5].Font.Bold = true;
                                strBenefit = gridRow.Cells[5].Text.TrimStart('$');
                                cumm69 = Convert.ToDecimal((Regex.Replace(strBenefit, regExp, "")));
                            }
                            //Highlight combined income value
                            if (gridRow.Cells[0].Text.Substring(0, 2).Equals(strCombineIncomeAges))
                            {
                                gridRow.Cells[4].BackColor = ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gridRow.Cells[4].Font.Bold = true;
                                strRestriCombValue = gridRow.Cells[4].Text;
                                strBenefit = gridRow.Cells[4].Text.TrimStart('$');
                                valueAtAge70 = Convert.ToDecimal((Regex.Replace(strBenefit, regExp, "")));
                                CombinedIncomeAge = gridRow.Cells[0].Text;
                            }
                        }
                    }

                }

                #endregion Highlight Cumulative income for Restricted app


                #region loop run for the annual benefits and cummulative values for Married and Divorce
                if (gridName.Equals("Full"))
                {
                    foreach (GridViewRow gvr in Grid1.Rows)
                    {
                        string regExp = "[^\\w]";
                        if (soloType == calc_solo.Married)
                        {
                            if (fRndMntHusb > 0 && gvr.Cells[(int)disp_col.Age].Text.Substring(3, 2).Equals(startBenefitAgeWife) && startBenefitAgeWife.Equals(combinedOncomeAge.Substring(3, 2)) && husbFRA >= int.Parse(startBenefitAgeWife) && int.Parse(startBenefitAgeWife) < int.Parse(Constants.Number70))
                                newCombinedOncomeAge = (int.Parse(combinedOncomeAge.Substring(0, 2)) + 1).ToString() + "/" + (int.Parse(combinedOncomeAge.Substring(3, 2)) + 1).ToString();
                            else if (fRndMnt > 0 && gvr.Cells[(int)disp_col.Age].Text.Substring(0, 2).Equals(startBenefitAgeHusb) && startBenefitAgeHusb.Equals(combinedOncomeAge.Substring(0, 2)) && wifeFRA >= int.Parse(startBenefitAgeHusb) && int.Parse(startBenefitAgeHusb) < int.Parse(Constants.Number70))
                                newCombinedOncomeAge = (int.Parse(combinedOncomeAge.Substring(0, 2)) + 1).ToString() + "/" + (int.Parse(combinedOncomeAge.Substring(3, 2)) + 1).ToString();
                            else
                            {
                                if (newCombinedOncomeAge.Equals(string.Empty))
                                    newCombinedOncomeAge = combinedOncomeAge;
                            }
                        }
                        if (soloType == calc_solo.Married && gvr.Cells[(int)disp_col.Age].Text.Equals(strCummulativeIncome))
                        {
                            gvr.Cells[5].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                            gvr.Cells[5].Font.Bold = true;
                            cumm69 = decimal.Parse((gvr.Cells[5].Text.Substring(1)));
                            strCummulativeIncome = string.Empty;
                        }
                        if (gvr.Cells[(int)disp_col.Age].Text == newCombinedOncomeAge && soloType == calc_solo.Married)
                        {
                            if (gvr.Cells[4].Text.Contains(Constants.DoubleAsterisk) || gvr.Cells[4].Text.Contains(Constants.SingleAsterisk) || gvr.Cells[4].Text.Contains(Constants.SingleHash))
                            {
                                newCombinedOncomeAge = (int.Parse(combinedOncomeAge.Substring(0, 2)) + 1).ToString() + "/" + (int.Parse(combinedOncomeAge.Substring(3, 2)) + 1).ToString();
                            }
                            else
                            {
                                gvr.Cells[4].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[4].Font.Bold = true;
                                gvr.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                gvr.Cells[0].Font.Bold = true;
                                valueAtAge70 = decimal.Parse(Regex.Replace(gvr.Cells[4].Text, regExp, ""));
                                CombinedIncomeAge = gvr.Cells[0].Text;
                                newCombinedOncomeAge = string.Empty;
                            }
                        }
                        if (soloType != calc_solo.Married)
                        {
                            if (gvr.Cells[(int)disp_col.Age].Text == newCombinedOncomeAgeDivorce && (soloType != calc_solo.Married))
                            {
                                if (!boolFirstAnnualIncome)
                                {
                                    strAnnualIncomeAge = newCombinedOncomeAgeDivorce;//Set annual income age value to check with startin benefit age value
                                    boolFirstAnnualIncome = true;
                                }
                                if (gvr.Cells[2].Text.Contains(Constants.DoubleAsterisk) || gvr.Cells[2].Text.Contains(Constants.SingleAsterisk) || gvr.Cells[2].Text.Contains(Constants.SingleHash))
                                {
                                    newCombinedOncomeAgeDivorce = (int.Parse(newCombinedOncomeAgeDivorce) + 1).ToString();
                                    newAnnualIncomeAge = newCombinedOncomeAgeDivorce;
                                }
                                else
                                {
                                    gvr.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[2].Font.Bold = true;
                                    newAnnualIncomeAge = gvr.Cells[0].Text;
                                    DivorceAnnualValue = decimal.Parse(Regex.Replace(gvr.Cells[2].Text, regExp, ""));
                                    newCombinedOncomeAgeDivorce = string.Empty;
                                    WidowAnnualIncomeValue = decimal.Parse(Regex.Replace(gvr.Cells[2].Text, regExp, ""));
                                    SingleAnualValue = decimal.Parse(Regex.Replace(gvr.Cells[2].Text, regExp, ""));
                                }
                            }
                        }
                    }
                    foreach (GridViewRow gvr in Grid1.Rows)
                    {
                        if (soloType != calc_solo.Married && gvr.Cells[0].BackColor == System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor))
                        { AgeAtLastChange = gvr.Cells[0].Text; ValueAtLastChange = decimal.Parse(gvr.Cells[1].Text.Substring(1)); }
                        if (gvr.Cells[2].BackColor == System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor))
                            AnnualIncomeAge = gvr.Cells[0].Text;
                        if (soloType == calc_solo.Divorced || soloType == calc_solo.Widowed)
                        {
                            if (!String.IsNullOrEmpty(newAnnualIncomeAge))
                            {
                                if (gvr.Cells[(int)disp_col.Age].Text.Equals((int.Parse(newAnnualIncomeAge) - 1).ToString()) && !gvr.Cells[3].Text.Equals(Constants.ZeroWithOutAsterisk) && !gvr.Cells[2].Text.Contains(Constants.SingleHash) && int.Parse(startBenefitAgeWife) < int.Parse(gvr.Cells[0].Text))
                                {
                                    gvr.Cells[3].BackColor = System.Drawing.ColorTranslator.FromHtml(Constants.gridBackgroundColor);
                                    gvr.Cells[3].Font.Bold = true;
                                    DCummValue = decimal.Parse(gvr.Cells[3].Text.Substring(1));
                                    WCummValue = decimal.Parse(gvr.Cells[3].Text.Substring(1));
                                    newAnnualIncomeAge = string.Empty;
                                }
                            }
                        }
                    }
                }
                #endregion loop run for the annual benefits and cummulative values for Married and Divorce
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        /// <summary>
        /// code to calculate the FRA for widow case as the FRA ages are diffrent from other cases
        /// it is 2 years more than other cases
        /// </summary>
        /// <param name="BirthYear"></param>
        /// <returns></returns>
        public float WidowFRA(int BirthYear)
        {
            try
            {
                float FRA_Age;
                if (BirthYear <= 1939)
                    BirthYear = 1937;
                if (BirthYear >= 1945 && BirthYear <= 1956)
                    BirthYear = 1938;
                if (BirthYear >= 1962)
                    BirthYear = 2000;
                switch (BirthYear)
                {
                    case 1937:
                        FRA_Age = 65;
                        break;
                    case 1938:
                        FRA_Age = 66;
                        break;
                    case 1940:
                        FRA_Age = 65.17f;
                        break;
                    case 1941:
                        FRA_Age = 65.33f;
                        break;
                    case 1942:
                        FRA_Age = 65.50f;
                        break;
                    case 1943:
                        FRA_Age = 65.67f;
                        break;
                    case 1944:
                        FRA_Age = 65.83f;
                        break;
                    case 1957:
                        FRA_Age = 66.17f;
                        break;
                    case 1958:
                        FRA_Age = 66.33f;
                        break;
                    case 1959:
                        FRA_Age = 66.50f;
                        break;
                    case 1960:
                        FRA_Age = 66.67f;
                        break;
                    case 1961:
                        FRA_Age = 66.83f;
                        break;
                    case 2000:
                        FRA_Age = 67;
                        break;
                    default:
                        FRA_Age = 0;
                        break;
                }
                return FRA_Age;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// to get the total number of months till the FRA according to the date of birth
        /// </summary>
        /// <param name="BirthYear"></param>
        /// <returns>total number of months</returns>
        public int FRAMonths(int BirthYear)
        {
            try
            {
                int FRA_Age;
                if (BirthYear >= 1900 && BirthYear <= 1937)
                    BirthYear = 1937;
                if (BirthYear >= 1943 && BirthYear <= 1954)
                    BirthYear = 1954;
                if (BirthYear >= 1960 && BirthYear <= 2000)
                    BirthYear = 2000;
                switch (BirthYear)
                {
                    case 1937:
                        FRA_Age = 780;
                        break;
                    case 1938:
                        FRA_Age = 782;
                        break;
                    case 1939:
                        FRA_Age = 784;
                        break;
                    case 1940:
                        FRA_Age = 786;
                        break;
                    case 1941:
                        FRA_Age = 788;
                        break;
                    case 1942:
                        FRA_Age = 790;
                        break;
                    case 1954:
                        FRA_Age = 792;
                        break;
                    case 1955:
                        FRA_Age = 794;
                        break;
                    case 1956:
                        FRA_Age = 796;
                        break;
                    case 1957:
                        FRA_Age = 798;
                        break;
                    case 1958:
                        FRA_Age = 800;
                        break;
                    case 1959:
                        FRA_Age = 802;
                        break;
                    case 2000:
                        FRA_Age = 804;
                        break;
                    default:
                        FRA_Age = 0;
                        break;
                }
                return FRA_Age;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// code to get the starting spousal benefits if no working benefit claimed earlier for wife
        /// </summary>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="currBeneWife"></param>
        /// <returns></returns>
        private decimal SpousalBenefitsWifeAtHusbAge62(int ageHusb, int ageWife, decimal currBeneWife)
        {
            try
            {
                decimal multiply, wifeReductionAmt = 0;
                int FRAmonths, Currentmonths, forExtraMonths, remainigMonths;

                if (intAgeHusb > husbFRA)
                    multiply = AgeAbove68YearsOfHusb();
                else
                    multiply = decBeneHusb;
                multiply = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                multiply = multiply / 2;

                FRAmonths = FRAMonths(birthYearWife);
                Currentmonths = ageWife * 12;
                forExtraMonths = 0;
                remainigMonths = (FRAmonths - Currentmonths);
                if (remainigMonths > 36)
                    forExtraMonths = remainigMonths - 36;

                if (remainigMonths > 36)
                {
                    wifeReductionAmt = 25m;
                    decimal wifeReductionAmt1 = (5m / 12m) * forExtraMonths;
                    decimal reductionfinal = wifeReductionAmt + wifeReductionAmt1;
                    decimal reductionfinal1 = 1 - (reductionfinal / 100m);
                    wifeReductionAmt = multiply * reductionfinal1;
                }
                else
                {
                    wifeReductionAmt = (25m / 36m) * remainigMonths;
                    wifeReductionAmt = 1 - (wifeReductionAmt / 100m);
                    wifeReductionAmt = multiply * wifeReductionAmt;
                }
                return wifeReductionAmt;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// code to get the starting spousal benefits if no working benefit claimed earlier for husb
        /// </summary>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="currBeneHusb"></param>
        /// <returns></returns>
        private decimal SpousalBenefitsHusbAtWifeAge62(int ageHusb, int ageWife, decimal currBeneHusb)
        {
            try
            {
                decimal multiply, husbReductionAmt = 0;
                int FRAmonths, Currentmonths, forExtraMonths, remainigMonths;
                if (intAgeWife > wifeFRA)
                    multiply = AgeAbove68YearsOfWife();
                else
                    multiply = decBeneWife;
                multiply = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                multiply = multiply / 2;

                FRAmonths = FRAMonths(birthYearHusb);
                Currentmonths = ageHusb * 12;
                forExtraMonths = 0;
                remainigMonths = (FRAmonths - Currentmonths);
                if (remainigMonths > 36)
                    forExtraMonths = remainigMonths - 36;
                if (remainigMonths > 36)
                {
                    husbReductionAmt = 25m;
                    decimal wifeReductionAmt1 = (5m / 12m) * forExtraMonths;
                    decimal reductionfinal = husbReductionAmt + wifeReductionAmt1;
                    decimal reductionfinal1 = 1 - (reductionfinal / 100m);
                    husbReductionAmt = multiply * reductionfinal1;
                }
                else
                {
                    husbReductionAmt = (25m / 36m) * remainigMonths;
                    husbReductionAmt = 1 - (husbReductionAmt / 100m);
                    husbReductionAmt = multiply * husbReductionAmt;
                }
                return husbReductionAmt;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }
        /// <summary>
        /// Used to calculate FRA age in month
        /// </summary>
        /// <param name="intNumofYears">Total number of Years</param>
        /// <param name="intNumofMonths">Total number of months</param>
        /// <returns>FRA Age in Months</returns>
        public int CalculateFRAAgeInMonth(int BirthYear)
        {
            try
            {

                int intFRA_Age;
                if (BirthYear >= 1900 && BirthYear <= 1937)
                    BirthYear = 1937;
                if (BirthYear >= 1943 && BirthYear <= 1954)
                    BirthYear = 1954;
                if (BirthYear >= 1960 && BirthYear <= 2000)
                    BirthYear = 2000;
                switch (BirthYear)
                {
                    case 1937:
                        intFRA_Age = 65 * 12;
                        break;
                    case 1938:
                        intFRA_Age = (65 * 12) + 2;
                        break;
                    case 1939:
                        intFRA_Age = (65 * 12) + 4;
                        break;
                    case 1940:
                        intFRA_Age = (65 * 12) + 6;
                        break;
                    case 1941:
                        intFRA_Age = (65 * 12) + 8;
                        break;
                    case 1942:
                        intFRA_Age = (65 * 12) + 10;
                        break;
                    case 1954:
                        intFRA_Age = 66 * 12;
                        break;
                    case 1955:
                        intFRA_Age = (66 * 12) + 2;
                        break;
                    case 1956:
                        intFRA_Age = (66 * 12) + 4;
                        break;
                    case 1957:
                        intFRA_Age = (66 * 12) + 6;
                        break;
                    case 1958:
                        intFRA_Age = (66 * 12) + 8;
                        break;
                    case 1959:
                        intFRA_Age = (66 * 12) + 10;
                        break;
                    case 2000:
                        intFRA_Age = 67 * 12;
                        break;
                    default:
                        intFRA_Age = 0;
                        break;
                }
                return intFRA_Age;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }
        /// <summary>
        /// function to get the reduction spousal benefits for husband when wife turns age 70 (Added on 07 Feb 2017)
        /// Reference URL :https://alohatechnology.basecamphq.com/projects/12167421-social-security-calculator/posts/101272474/comments#360289563
        /// </summary>
        /// <param name="ageHusb">current  age of husband</param>
        /// <param name="ageWife">current  age of wife</param>
        /// <param name="currBeneWife">current  benefits of wife</param>
        /// <returns></returns>
        private decimal SpousalBenefitsWifeAtHusbAge70Restrict(int ageHusb, int ageWife, decimal currBeneWife)
        {
            try
            {
                decimal multiply = 0; decimal result = 0; decimal wifeReductionAmt = 0; decimal FRABenefit = 0; decimal decBenAfterPercentApply = 0;
                int intClaimedAgeWife = RestrictAppIncomeProp.Instance.intWifeClaimAge;
                int intClaimedBeneWife = RestrictAppIncomeProp.Instance.intWifeClaimBenefit;
                int intCurrentBenWife = RestrictAppIncomeProp.Instance.intWifeCurrentBenefit;
                if (intAgeHusb > husbFRA)
                    multiply = AgeAbove68YearsOfHusb();
                else
                    multiply = decBeneHusb;
                multiply = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                multiply = multiply / 2;

                //Calculate FRA Benefit after adding Percentage of Claimed benefit
                FRABenefit = CalculateRestrictFRABenFromClaimBeneAge(intClaimedAgeWife, intClaimedBeneWife);

                //Calculate Reduction Ammount for wife
                if (intClaimedBeneWife == intCurrentBenWife)
                {
                    //If claimed benefit and current benefit are equal
                    wifeReductionAmt = ColaUpSpousal(ageWife, FRABenefit, birthYearWife, intAgeWife, wifeFRA);
                }
                else
                {
                    //Add percentage difference into Husband FRA benefit
                    multiply = CalculateRestrictFRABenFromClaimBeneAge(intAgeHusb, (int)decBeneHusb);
                    multiply = (multiply * (((decimal)intCurrentBenWife - (decimal)intClaimedBeneWife) / (decimal)intClaimedBeneWife)) + multiply;
                    multiply = ColaUpSpousal(ageHusb, multiply, birthYearHusb, intAgeHusb, husbFRA);
                    multiply = multiply / 2;
                    //Add percentage difference into Wife FRA benefit
                    decBenAfterPercentApply = (FRABenefit * (((decimal)intCurrentBenWife - (decimal)intClaimedBeneWife) / (decimal)intClaimedBeneWife)) + FRABenefit;
                    wifeReductionAmt = ColaUpSpousal(ageWife, decBenAfterPercentApply, birthYearWife, intAgeWife, wifeFRA);
                }
                result = currBeneWife + (Convert.ToInt32(multiply) - Convert.ToInt32(wifeReductionAmt));
                return result;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// function to get the reduction spousal benefits for wife when husband turns age 70
        /// </summary>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="currBeneHusb"></param>
        /// <returns></returns>
        private decimal SpousalBenefitsHusbAtWifeAge70Restrict(int ageHusb, int ageWife, decimal currBeneHusb)
        {
            try
            {
                decimal multiply = 0; decimal result = 0; decimal husbReductionAmt = 0; decimal FRABenefit = 0; decimal decBenAfterPercentApply = 0;
                int intClaimedAgeHusb = RestrictAppIncomeProp.Instance.intHusbandClaimAge;
                int intCurrentBenHusb = RestrictAppIncomeProp.Instance.intHusbCurrentBenefit;
                int intClaimedBeneHusb = RestrictAppIncomeProp.Instance.intHusbClaimBenefit;
                if (intAgeWife > wifeFRA)
                    multiply = AgeAbove68YearsOfWife();
                else
                    multiply = decBeneWife;
                multiply = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                multiply = multiply / 2;

                //Calculate FRA Benefit after adding Percentage of Claimed benefit
                FRABenefit = CalculateRestrictFRABenFromClaimBeneAge(intClaimedAgeHusb, intClaimedBeneHusb);

                //Calculate Reduction Ammount for Husband
                if (intClaimedBeneHusb == intCurrentBenHusb)
                {
                    husbReductionAmt = ColaUpSpousal(ageHusb, FRABenefit, birthYearHusb, intAgeHusb, husbFRA);
                }
                else
                {
                    //Add percentage difference into Wife FRA benefit
                    multiply = CalculateRestrictFRABenFromClaimBeneAge(intAgeWife, (int)decBeneWife);
                    multiply = (multiply * (((decimal)intCurrentBenHusb - (decimal)intClaimedBeneHusb) / (decimal)intClaimedBeneHusb)) + multiply;
                    multiply = ColaUpSpousal(ageWife, multiply, birthYearWife, intAgeWife, wifeFRA);
                    multiply = multiply / 2;
                    //Add percentage difference into Husband FRA benefit
                    decBenAfterPercentApply = (FRABenefit * (((decimal)intCurrentBenHusb - (decimal)intClaimedBeneHusb) / (decimal)intClaimedBeneHusb)) + FRABenefit;
                    husbReductionAmt = ColaUpSpousal(ageHusb, decBenAfterPercentApply, birthYearHusb, intAgeHusb, husbFRA);
                }
                result = currBeneHusb + (Convert.ToInt32(multiply) - Convert.ToInt32(husbReductionAmt));
                return result;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return 0;
            }
        }

        /// <summary>
        /// Calculate FRA yeara and months
        /// </summary>
        private void CalculateFRAYearsAndMonths(ref int FraYear, ref int FraRemainMonth, int birthYear)
        {
            try
            {
                Customers customer = new Customers();
                string strFra = customer.FRA(birthYear);
                if (strFra.Contains("Months"))
                {
                    //"66 Years and 2 Months"
                    string[] split = strFra.Split(new string[] { " and " }, StringSplitOptions.None);

                    FraYear = Convert.ToInt32(split[0].Substring(0, 2));
                    FraRemainMonth = Convert.ToInt32(split[1].Substring(0, 2).TrimEnd());
                }
                else
                {
                    FraYear = Convert.ToInt32(strFra);
                    FraRemainMonth = 0;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }


        /// <summary>
        /// Used to calculate reduced spousal benefit as per the Brian's post
        /// Reference Link : https://alohatechnology.basecamphq.com/projects/12167421-social-security-calculator/posts/102212400/comments
        /// </summary>
        /// <returns></returns>
        private decimal NewCalculateReducedSpousalBenefitofWife(int ageHusb, int ageWife, decimal currBeneWife)
        {
            try
            {
                decimal multiply = 0, newMultiplyAmount = 0, result = 0, wifeReductionAmt = 0, reducedMultiply = 0;
                int FRAmonths, Currentmonths, forExtraMonths, remainigMonths;

                newMultiplyAmount = CalculateRestrictFRABenFromClaimBeneAge(RestrictAppIncomeProp.Instance.intHusbandClaimAge, RestrictAppIncomeProp.Instance.intHusbClaimBenefit);//2100
                newMultiplyAmount = (decimal)(0.50 * (double)newMultiplyAmount);//1050


                //increase that amount by the same percentage increase
                decimal increasMultipyer = decBeneHusb - RestrictAppIncomeProp.Instance.intHusbClaimBenefit;
                increasMultipyer = increasMultipyer / RestrictAppIncomeProp.Instance.intHusbClaimBenefit;
                increasMultipyer = 1 + increasMultipyer;

                newMultiplyAmount = newMultiplyAmount * increasMultipyer;


                if (ageWife >= wifeFRA)
                {
                    wifeReductionAmt = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                }
                else
                {
                    FRAmonths = FRAMonths(birthYearWife);
                    //Currentmonths = ageWife * 12;
                    int currYear = 0, currMonth = 0;
                    SpouseAgeInMonth_Year(wifeBirthDate, out currMonth, out currYear);
                    Currentmonths = currYear * 12 + currMonth;

                    forExtraMonths = 0;
                    remainigMonths = (FRAmonths - Currentmonths);
                    if (remainigMonths > 36)
                        forExtraMonths = remainigMonths - 36;
                    if (ageWife < wifeFRA)
                    {
                        if (remainigMonths > 36)
                        {
                            reducedMultiply = 25m;
                            decimal wifeReductionAmt1 = (5m / 12m) * forExtraMonths;
                            decimal reductionfinal = reducedMultiply + wifeReductionAmt1;
                            decimal reductionfinal1 = 1 - (reductionfinal / 100m);
                            //multiply = multiply * reductionfinal1;

                            result = reductionfinal1 * newMultiplyAmount;

                            //decimal tempDecBeneWife = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                            //decimal wifeReductionAmtWork = 20m;
                            //decimal wifeReductionAmt1Work = (5m / 12m) * forExtraMonths;
                            //decimal reductionfinalWork = wifeReductionAmtWork + wifeReductionAmt1Work;
                            //decimal reductionfinal1Work = 1 - (reductionfinalWork / 100m);
                            //wifeReductionAmt = tempDecBeneWife * reductionfinal1Work;
                        }
                        else
                        {
                            reducedMultiply = (25m / 36m) * remainigMonths;
                            reducedMultiply = 1 - (reducedMultiply / 100m);

                            result = reducedMultiply * newMultiplyAmount;

                            //multiply = multiply * reducedMultiply;

                            //decimal tempDecBeneWife = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                            //decimal wifeReductionAmtWork = (5m / 9m) * remainigMonths;
                            //wifeReductionAmtWork = 1 - (wifeReductionAmtWork / 100m);
                            //wifeReductionAmt = tempDecBeneWife * wifeReductionAmtWork;
                        }
                    }
                }
                //result = currBeneWife + (Convert.ToInt32(multiply) - Convert.ToInt32(wifeReductionAmt));
                return result;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return result;
            }
        }

        /// <summary>
        /// Used to calculate reduced spousal benefit as per the Brian's post
        /// Reference Link : https://alohatechnology.basecamphq.com/projects/12167421-social-security-calculator/posts/102212400/comments
        /// </summary>
        /// <returns></returns>
        private decimal NewCalculateRestrictReducedSpousalBenefitofHusband(int ageWife, int ageHusb, decimal currBeneWife)
        {
            try
            {
                decimal newMultiplyAmount = 0, result = 0, wifeReductionAmt = 0, reducedMultiply = 0;
                int FRAmonths, Currentmonths, forExtraMonths, remainigMonths;

                newMultiplyAmount = CalculateRestrictFRABenFromClaimBeneAge(RestrictAppIncomeProp.Instance.intWifeClaimAge, RestrictAppIncomeProp.Instance.intWifeClaimBenefit);//2100
                newMultiplyAmount = (decimal)(0.50 * (double)newMultiplyAmount);//1050


                //increase that amount by the same percentage increase
                decimal increasMultipyer = decBeneWife - RestrictAppIncomeProp.Instance.intWifeClaimBenefit;
                increasMultipyer = increasMultipyer / RestrictAppIncomeProp.Instance.intWifeClaimBenefit;
                increasMultipyer = 1 + increasMultipyer;

                newMultiplyAmount = newMultiplyAmount * increasMultipyer;

                if (ageHusb >= husbFRA)
                {
                    wifeReductionAmt = ColaUpSpousal(ageWife, decBeneHusb, birthYearHusb, intAgeHusb, husbFRA);
                }
                else
                {
                    FRAmonths = FRAMonths(birthYearHusb);
                    //Currentmonths = ageWife * 12;
                    int currYear = 0, currMonth = 0;
                    SpouseAgeInMonth_Year(husbBirthDate, out currMonth, out currYear);
                    Currentmonths = currYear * 12 + currMonth;

                    forExtraMonths = 0;
                    remainigMonths = (FRAmonths - Currentmonths);
                    if (remainigMonths > 36)
                        forExtraMonths = remainigMonths - 36;
                    if (ageHusb < husbFRA)
                    {
                        if (remainigMonths > 36)
                        {
                            reducedMultiply = 25m;
                            decimal wifeReductionAmt1 = (5m / 12m) * forExtraMonths;
                            decimal reductionfinal = reducedMultiply + wifeReductionAmt1;
                            decimal reductionfinal1 = 1 - (reductionfinal / 100m);
                            //multiply = multiply * reductionfinal1;

                            result = reductionfinal1 * newMultiplyAmount;

                            //decimal tempDecBeneWife = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                            //decimal wifeReductionAmtWork = 20m;
                            //decimal wifeReductionAmt1Work = (5m / 12m) * forExtraMonths;
                            //decimal reductionfinalWork = wifeReductionAmtWork + wifeReductionAmt1Work;
                            //decimal reductionfinal1Work = 1 - (reductionfinalWork / 100m);
                            //wifeReductionAmt = tempDecBeneWife * reductionfinal1Work;
                        }
                        else
                        {
                            reducedMultiply = (25m / 36m) * remainigMonths;
                            reducedMultiply = 1 - (reducedMultiply / 100m);

                            result = reducedMultiply * newMultiplyAmount;

                            //multiply = multiply * reducedMultiply;

                            //decimal tempDecBeneWife = ColaUpSpousal(ageWife, decBeneWife, birthYearWife, intAgeWife, wifeFRA);
                            //decimal wifeReductionAmtWork = (5m / 9m) * remainigMonths;
                            //wifeReductionAmtWork = 1 - (wifeReductionAmtWork / 100m);
                            //wifeReductionAmt = tempDecBeneWife * wifeReductionAmtWork;
                        }
                    }
                }
                //result = currBeneWife + (Convert.ToInt32(multiply) - Convert.ToInt32(wifeReductionAmt));
                return result;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
                return result;
            }
        }


        /// <summary>
        /// Used to draw green box for restriected strategy
        /// </summary>
        /// <param name="gridView">Gridview to be update</param>
        /// <param name="claimedAge">starting age or claimed age</param>
        /// <param name="ClaimedPerson">who is claiming</param>
        public void DesignGreeBoxForRestrictStrategy(GridView gridView, string claimedAge, string ClaimedPerson)
        {
            try
            {
                claimedAge = claimedAge.Substring(0, 2);
                foreach (GridViewRow gridRow in gridView.Rows)
                {
                    int intWifeLoopAge = int.Parse(gridRow.Cells[0].Text.Substring(0, 2));
                    int intHusbLoopAge = int.Parse(gridRow.Cells[0].Text.Substring(3, 2));
                    if (ClaimedPerson.Equals("Husband"))
                    {
                        string strValue = gridRow.Cells[2].Text;
                        if (intHusbLoopAge >= int.Parse(claimedAge) && intHusbLoopAge <= 69 && !strValue.Equals(Constants.ZeroWithOutAsterisk))
                        {
                            //Apply border for cells from 66-69
                            gridRow.Cells[2].Style.Add("border", "3px solid #228B22");
                            gridRow.Cells[2].Font.Bold = true;
                            //Display only single box from 66 to 69 (remove bottom border of cells expect 69)
                            if (intHusbLoopAge < 69)
                                gridRow.Cells[2].Style.Add("border-bottom-style", "hidden");
                        }
                    }
                    else
                    {
                        string strValue = gridRow.Cells[1].Text;
                        if (intWifeLoopAge >= int.Parse(claimedAge) && intWifeLoopAge <= 69 && !strValue.Equals(Constants.ZeroWithOutAsterisk))
                        {
                            //Apply border for cells from 66-69
                            gridRow.Cells[1].Style.Add("border", "3px solid #228B22");
                            gridRow.Cells[1].Font.Bold = true;
                            //Display only single box from 66 to 69 (remove bottom border of cells expect 69)
                            if (intWifeLoopAge < 69)
                                gridRow.Cells[1].Style.Add("border-bottom-style", "hidden");
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalc, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }





        #endregion Methods
    }
}