﻿using Symbolics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Calculate.Objects
{
    public static class CommonVariables
    {
        #region Common Variables
        public static string strNoteHusband0 { get; set; }
        public static string strNoteWife0 { get; set; }
        public static string strNoteHusband1 { get; set; }
        public static string strNoteWife1 { get; set; }
        public static string strNoteHusband { get; set; }
        public static string strNoteWife { get; set; }
        public static string strNoteHusband3 { get; set; }
        public static string strNoteWife2 { get; set; }
        public static string strNoteWife3 { get; set; }
        public static string strNoteHusband5 { get; set; }
        public static string strNoteWife5 { get; set; }
        public static string strNoteHusband4 { get; set; }
        public static string strNoteWife4 { get; set; }
        public static string strNoteHusband6 { get; set; }
        public static string strNoteWife6 { get; set; }
        public static string strNoteHusband7 { get; set; }

        public static string strNoteHusband8 { get; set; }
        public static string strNoteHusband9 { get; set; }
        public static string strNoteHusband10 { get; set; }

        public static string strNoteWife7 { get; set; }
        public static string strNoteWife8 { get; set; }
        public static string strNoteWife9 { get; set; }
        public static string strNoteWife10 { get; set; }

        public static string strNoteWife11 { get; set; }
        public static string strNoteWife12 { get; set; }
        public static string strNoteWife13 { get; set; }

        public static string strHusbExplain1 { get; set; }
        public static string strHusbExplain2 { get; set; }
        public static string strHusbExplain3 { get; set; }
        public static string strHusbExplain4 { get; set; }
        public static string strHusbExplain5 { get; set; }

        public static string strWifeExplain1 { get; set; }
        public static string strWifeExplain2 { get; set; }
        public static string strWifeExplain3 { get; set; }
        public static string strWifeExplain4 { get; set; }
        public static string strWifeExplain5 { get; set; }

        public static string sortExpression { get; set; }

        //New Implementation
        public static string strKey1 = string.Empty, strKey2 = string.Empty, strKey3 = string.Empty, strRestrictKeyBest = string.Empty,   //For GridBest
                             strKey4 = string.Empty, strKey5 = string.Empty, strKey6 = string.Empty, strRestrictKeyMax = string.Empty,   //For Max
                             strKey7 = string.Empty, strKey8 = string.Empty, strKey9 = string.Empty, strRestrictKeyFull = string.Empty, strKeyDivorceRestrictIncome = string.Empty,//For Full
                             strKey10 = string.Empty, strKey11 = string.Empty, strKey12 = string.Empty, strRestrictKeyEarly = string.Empty,//For GridEarly
                             strKey13 = string.Empty, strKey14 = string.Empty, strKey15 = string.Empty;//For GridZero

        public static List<string> strings = new List<string>();

        public static string strStep1 = string.Empty, strStep2 = string.Empty, strStep3 = string.Empty,
                             strStep4 = string.Empty, strStep5 = string.Empty, strStep6 = string.Empty,
                             strStep7 = string.Empty, strStep8 = string.Empty, strStep9 = string.Empty,
                             strStep10 = string.Empty, strStep11 = string.Empty, strStep12 = string.Empty,
                             strStep13 = string.Empty, strStep14 = string.Empty, strStep15 = string.Empty,
                             strStep16 = string.Empty, strStep17 = string.Empty, strStep18 = string.Empty,
                             strStep19 = string.Empty, strStep20 = string.Empty, strStep21 = string.Empty, strStep22 = string.Empty,
                             strStep23 = string.Empty;

        public static string strUserName = string.Empty;

        #endregion

    }
}