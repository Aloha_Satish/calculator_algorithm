﻿using System;

namespace Calculate
{
    public class Globals
    {
        #region Singleton Implementation

        private static Globals m_instance = null;

        public static Globals Instance
        {
            get
            {
                if (m_instance == null)
                    m_instance = new Globals();
                return m_instance;
            }
        }

        private Globals()
        {
        }

        #endregion Singleton Implementation

        #region values used in the code as DateTime
        private DateTime m_WifeDateOfBirth;
        private DateTime m_HusbDateOfBirth;
        public DateTime WifeDateOfBirth
        {
            get { return m_WifeDateOfBirth; }
            set { m_WifeDateOfBirth = value; }
        }
        public DateTime HusbDateOfBirth
        {
            get { return m_HusbDateOfBirth; }
            set { m_HusbDateOfBirth = value; }
        }
        #endregion values used in the code as DateTime

        #region values used in the code as string
        private string m_SwitchNxtStepClaimSuspend;
        private string m_ChangeBanner = string.Empty;
        private string m_WifeStartingBenefitName = string.Empty;
        private string m_HusbStartingBenefitName = string.Empty;
        private string m_strDivorceStarting = string.Empty;
        private string m_strCurrentAgeHusband = string.Empty;
        private string m_strCurrentAgeWife = string.Empty;
        private string m_strDivorceStartingSpouse = string.Empty;
        private string m_strWidowStarting = string.Empty;
        private string m_strDivorceStartingAsap = string.Empty;
        private string m_strAdvisorPlan = string.Empty;
        private string m_ClaimedPerson = string.Empty;
        private string m_strAdvisorEmail = string.Empty;
        public string strWidowStarting
        {
            get { return m_strWidowStarting; }
            set { m_strWidowStarting = value; }
        }
        public string strCurrentAgeHusband
        {
            get { return m_strCurrentAgeHusband; }
            set { m_strCurrentAgeHusband = value; }
        }
        public string strCurrentAgeWife
        {
            get { return m_strCurrentAgeWife; }
            set { m_strCurrentAgeWife = value; }
        }
        public string strDivorceStartingSpouse
        {
            get { return m_strDivorceStartingSpouse; }
            set { m_strDivorceStartingSpouse = value; }
        }
        public string strDivorceStartingAsap
        {
            get { return m_strDivorceStartingAsap; }
            set { m_strDivorceStartingAsap = value; }
        }
        public string strDivorceStarting
        {
            get { return m_strDivorceStarting; }
            set { m_strDivorceStarting = value; }
        }
        public string WifeStartingBenefitName
        {
            get { return m_WifeStartingBenefitName; }
            set { m_WifeStartingBenefitName = value; }
        }
        public string HusbStartingBenefitName
        {
            get { return m_HusbStartingBenefitName; }
            set { m_HusbStartingBenefitName = value; }
        }
        public string SwitchNxtStepClaimSuspend
        {
            get { return m_SwitchNxtStepClaimSuspend; }
            set { m_SwitchNxtStepClaimSuspend = value; }
        }
        public string ChangeBanner
        {
            get { return m_ChangeBanner; }
            set { m_ChangeBanner = value; }
        }

        public string strAdvisorPlan
        {
            get { return m_strAdvisorPlan; }
            set { m_strAdvisorPlan = value; }
        }

        public string strAdvisorEmail
        {
            get { return m_strAdvisorEmail; }
            set { m_strAdvisorEmail = value; }
        }

        public string ClaimedPerson
        {
            get { return m_ClaimedPerson; }
            set { m_ClaimedPerson = value; }
        }
        
        #endregion values used in the code as string

        #region values used in the code as boolean
        private bool m_PDFShowClaimandSuspend = true;
        private bool m_ShowClaimAndSuspend = true;
        private bool m_CurrHusValue = false;
        private bool m_CurrWifeValue = false;
        private bool m_Age70ChangeForHusb = false;
        private bool m_Age70ChangeForWife = false;
        private bool m_BoolShowSecondDivorce = false;
        private bool m_BoolShowSecondStrat = false;
        private bool m_CurrHusValuePDF = false;
        private bool m_CurrWifeValuePDF = false;
        private bool m_WidowStrategy = false;
        private bool m_WidowChangeAt70 = false;
        private bool m_BoolShowSecondConditionDivorce = false;
        private bool m_BoolShowSecondDivorcePDF = false;
        private bool m_ShowSuspendStartingValueAtAge70 = false;
        private bool m_BoolWorkingBenefitsShown = false;
        private bool m_BoolShowSecondDivorcePDFLimit = false;
        private bool m_BoolSpGrternWrkgFRADivorce = true;
        private bool m_BoolShowSecondDivorceInFirst = false;
        private bool m_BoolSpousalChange = false;
        private bool m_BoolSpousalChangeAtAge70 = false;
        private bool m_StringSpousalChangeAgeWife = false;
        private bool m_StringSpousalChangeAgeHusb = false;
        private bool m_BoolMaxStrategyAt70Wife = false;
        private bool m_BoolMaxStrategyAt70Husb = false;
        private bool m_BoolShowMaxStrategy = false;
        private bool m_BoolSpousalCliamed = false;
        private bool m_BoolAge70ChangeHusb = false;
        private bool m_BoolAge70ChangeWife = false;
        private bool m_BoolSpousalCliamedHusb = false;
        private bool m_BoolSpousalCliamedWife = false;
        private bool m_BoolAge70StartingAgeHusb = false;
        private bool m_BoolAge70StartingAgeWife = false;
        private bool m_BoolHideAsapStrategyDivorce = false;
        private bool m_BoolSpousalBenefitTakenDivorce = false;
        private bool m_BoolMaxStrategyCumm = false;
        private bool m_BoolSpousalCliamedWifeSt1 = false;
        private bool m_BoolSpousalCliamedHusbSt1 = false;
        private bool m_BoolHighlightCummAtWife69Max = false;
        private bool m_boolHusbandAgeAdjusted = false;
        private bool m_boolWifeAgeAdjusted = false;
        private bool m_ShowAsterikForTwoStrategies = false;
        private bool m_BoolHideSpouseStrategyDivorce = false;
        private bool m_BoolHideSpouseStrategySingle = false;
        private bool m_BoolSpousalAfterWorkWife = false;
        private bool m_BoolSpousalAfterWorkHusb = false;
        private bool m_BoolWHBSwitchedWife = false;
        private bool m_BoolWHBSwitchedHusb = false;
        private bool m_BoolEmailReport = false;
        private bool m_BoolWHBSingleSwitchedWife = false;
        private bool m_BoolWHBSingleSwitchedHusb = false;
        private bool m_BoolIsMailSent = false;
        private bool m_BoolSpousalHubsAfter70 = false;
        private bool m_BoolSpousalWifeAfter70 = false;
        private bool m_BoolSpousalGreaterAtFRAandAge70 = false;
        private bool m_BoolSpousalAtFRAAgeCliamedHusb = false;
        private bool m_BoolSpousalAtFRAAgeCliamedWife = false;
        private bool m_BoolWHBTakenWidowAt66 = false;
        private bool m_BoolShowThirdDivorceStrategy = false;
        private bool m_SpousalWHBat70 = false;
        private bool m_BoolAdvisorDownloadReport = false;
        private bool m_BoolShowSecondZeroStrategy = false;
        private bool m_BoolFBUserSession = false;
        private bool m_BoolIsHusbandClaimedBenefit = false;
        private bool m_BoolIsWifeClaimedBenefit = false;
        private bool m_BoolIsShowRestrictFull = false;
        //To hide second strategy
        private bool m_BoolShowDivRestrictIncom = false;
        private bool m_BoolSkipWHBSwitchRestrict = false;
        private bool m_BoolRestrictSpecialCase = false;
        private bool m_BoolHideStrategyAge70 = false;

        private bool m_BoolRestrictSpousalAt62 = false;
        private bool m_BoolRestrictSpousalAt66 = false;

        private bool m_BoolShowRestOptionalCurrentAge = false;
        private bool m_BoolNoClaimRestrictForAdd2 = false;

        public bool BoolRestrictSpousalAt62
        {
            get { return m_BoolRestrictSpousalAt62; }
            set { m_BoolRestrictSpousalAt62 = value; }
        }

        public bool BoolRestrictSpousalAt66
        {
            get { return m_BoolRestrictSpousalAt66; }
            set { m_BoolRestrictSpousalAt66 = value; }
        }
        private bool m_BoolWHBHusbat70 = false;
        private bool m_BoolWHBWifeat70 = false;
        private bool m_BoolHideSecondStrat = false;

        //To change explanation if value contains #* for single user
        private bool m_BoolContainsHashStar = false;



        public bool BoolContainsHashStar
        {
            get { return m_BoolContainsHashStar; }
            set { m_BoolContainsHashStar = value; }
        }
        public bool BoolWHBHusbat70
        {
            get { return m_BoolWHBHusbat70; }
            set { m_BoolWHBHusbat70 = value; }
        }


        public bool BoolHideSecondStrat
        {
            get { return m_BoolHideSecondStrat; }
            set { m_BoolHideSecondStrat = value; }
        }

        public bool BoolWHBWifeat70
        {
            get { return m_BoolWHBWifeat70; }
            set { m_BoolWHBWifeat70 = value; }
        }


        public bool BoolShowDivRestrictIncom
        {
            get { return m_BoolShowDivRestrictIncom; }
            set { m_BoolShowDivRestrictIncom = value; }
        }

        public bool BoolSpousalWHBat70
        {
            get { return m_SpousalWHBat70; }
            set { m_SpousalWHBat70 = value; }
        }
        public bool BoolWHBTakenWidowAt66
        {
            get { return m_BoolWHBTakenWidowAt66; }
            set { m_BoolWHBTakenWidowAt66 = value; }
        }
        public bool BoolSpousalAtFRAAgeCliamedWife
        {
            get { return m_BoolSpousalAtFRAAgeCliamedWife; }
            set { m_BoolSpousalAtFRAAgeCliamedWife = value; }
        }
        public bool BoolSpousalAtFRAAgeCliamedHusb
        {
            get { return m_BoolSpousalAtFRAAgeCliamedHusb; }
            set { m_BoolSpousalAtFRAAgeCliamedHusb = value; }
        }

        public bool BoolSpousalGreaterAtFRAandAge70
        {
            get { return m_BoolSpousalGreaterAtFRAandAge70; }
            set { m_BoolSpousalGreaterAtFRAandAge70 = value; }
        }
        public bool BoolSpousalWifeAfter70
        {
            get { return m_BoolSpousalWifeAfter70; }
            set { m_BoolSpousalWifeAfter70 = value; }
        }
        public bool BoolSpousalHubsAfter70
        {
            get { return m_BoolSpousalHubsAfter70; }
            set { m_BoolSpousalHubsAfter70 = value; }
        }
        public bool BoolIsMailSent
        {
            get { return m_BoolIsMailSent; }
            set { m_BoolIsMailSent = value; }
        }
        public bool BoolEmailReport
        {
            get { return m_BoolEmailReport; }
            set { m_BoolEmailReport = value; }
        }
        public bool BoolWHBSingleSwitchedWife
        {
            get { return m_BoolWHBSingleSwitchedWife; }
            set { m_BoolWHBSingleSwitchedWife = value; }
        }
        public bool BoolWHBSingleSwitchedHusb
        {
            get { return m_BoolWHBSingleSwitchedHusb; }
            set { m_BoolWHBSingleSwitchedHusb = value; }
        }

        public bool BoolWHBSwitchedHusb
        {
            get { return m_BoolWHBSwitchedHusb; }
            set { m_BoolWHBSwitchedHusb = value; }
        }
        public bool BoolWHBSwitchedWife
        {
            get { return m_BoolWHBSwitchedWife; }
            set { m_BoolWHBSwitchedWife = value; }
        }
        public bool BoolSpousalAfterWorkWife
        {
            get { return m_BoolSpousalAfterWorkWife; }
            set { m_BoolSpousalAfterWorkWife = value; }
        }
        public bool BoolSpousalAfterWorkHusb
        {
            get { return m_BoolSpousalAfterWorkHusb; }
            set { m_BoolSpousalAfterWorkHusb = value; }
        }
        public bool BoolHideSpouseStrategySingle
        {
            get { return m_BoolHideSpouseStrategySingle; }
            set { m_BoolHideSpouseStrategySingle = value; }
        }
        public bool BoolHusbandAgeAdjusted
        {
            get { return m_boolHusbandAgeAdjusted; }
            set { m_boolHusbandAgeAdjusted = value; }
        }
        public bool BoolWifeAgeAdjusted
        {
            get { return m_boolWifeAgeAdjusted; }
            set { m_boolWifeAgeAdjusted = value; }
        }
        public bool BoolSpousalCliamedHusbSt1
        {
            get { return m_BoolSpousalCliamedHusbSt1; }
            set { m_BoolSpousalCliamedHusbSt1 = value; }
        }
        public bool BoolSpousalCliamedWifeSt1
        {
            get { return m_BoolSpousalCliamedWifeSt1; }
            set { m_BoolSpousalCliamedWifeSt1 = value; }
        }
        public bool BoolAge70StartingAgeHusb
        {
            get { return m_BoolAge70StartingAgeHusb; }
            set { m_BoolAge70StartingAgeHusb = value; }
        }
        public bool BoolAge70StartingAgeWife
        {
            get { return m_BoolAge70StartingAgeWife; }
            set { m_BoolAge70StartingAgeWife = value; }
        }
        public bool BoolAge70ChangeHusb
        {
            get { return m_BoolAge70ChangeHusb; }
            set { m_BoolAge70ChangeHusb = value; }
        }
        public bool BoolSpousalCliamedHusb
        {
            get { return m_BoolSpousalCliamedHusb; }
            set { m_BoolSpousalCliamedHusb = value; }
        }
        public bool BoolSpousalCliamedWife
        {
            get { return m_BoolSpousalCliamedWife; }
            set { m_BoolSpousalCliamedWife = value; }
        }
        public bool BoolAge70ChangeWife
        {
            get { return m_BoolAge70ChangeWife; }
            set { m_BoolAge70ChangeWife = value; }
        }
        public bool BoolSpousalCliamed
        {
            get { return m_BoolSpousalCliamed; }
            set { m_BoolSpousalCliamed = value; }
        }
        public bool BoolShowMaxStrategy
        {
            get { return m_BoolShowMaxStrategy; }
            set { m_BoolShowMaxStrategy = value; }
        }
        public bool BoolMaxStrategyAt70Wife
        {
            get { return m_BoolMaxStrategyAt70Wife; }
            set { m_BoolMaxStrategyAt70Wife = value; }
        }
        public bool BoolMaxStrategyAt70Husb
        {
            get { return m_BoolMaxStrategyAt70Husb; }
            set { m_BoolMaxStrategyAt70Husb = value; }
        }
        public bool StringSpousalChangeAgeWife
        {
            get { return m_StringSpousalChangeAgeWife; }
            set { m_StringSpousalChangeAgeWife = value; }
        }
        public bool StringSpousalChangeAgeHusb
        {
            get { return m_StringSpousalChangeAgeHusb; }
            set { m_StringSpousalChangeAgeHusb = value; }
        }
        public bool ShowClaimAndSuspend
        {
            get { return m_ShowClaimAndSuspend; }
            set { m_ShowClaimAndSuspend = value; }
        }
        public bool BoolSpousalChangeAtAge70
        {
            get { return m_BoolSpousalChangeAtAge70; }
            set { m_BoolSpousalChangeAtAge70 = value; }
        }
        public bool BoolSpousalChange
        {
            get { return m_BoolSpousalChange; }
            set { m_BoolSpousalChange = value; }
        }
        public bool WidowChangeAt70
        {
            get { return m_WidowChangeAt70; }
            set { m_WidowChangeAt70 = value; }
        }
        public bool CurrHusValue
        {
            get { return m_CurrHusValue; }
            set { m_CurrHusValue = value; }

        }
        public bool ShowSuspendStartingValueAtAge70
        {
            get { return m_ShowSuspendStartingValueAtAge70; }
            set { m_ShowSuspendStartingValueAtAge70 = value; }

        }
        public bool ShowAsterikForTwoStrategies
        {
            get { return m_ShowAsterikForTwoStrategies; }
            set { m_ShowAsterikForTwoStrategies = value; }

        }
        public bool WidowStrategy
        {
            get { return m_WidowStrategy; }
            set { m_WidowStrategy = value; }
        }
        public bool CurrHusValuePDF
        {
            get { return m_CurrHusValuePDF; }
            set { m_CurrHusValuePDF = value; }
        }
        public bool CurrWifeValuePDF
        {
            get { return m_CurrWifeValuePDF; }
            set { m_CurrWifeValuePDF = value; }
        }
        public bool Age70ChangeForWife
        {
            get { return m_Age70ChangeForWife; }
            set { m_Age70ChangeForWife = value; }
        }
        public bool Age70ChangeForHusb
        {
            get { return m_Age70ChangeForHusb; }
            set { m_Age70ChangeForHusb = value; }
        }
        public bool CurrWifeValue
        {
            get { return m_CurrWifeValue; }
            set { m_CurrWifeValue = value; }
        }
        public bool PDFShowClaimandSuspend
        {
            get { return m_PDFShowClaimandSuspend; }
            set { m_PDFShowClaimandSuspend = value; }
        }
        public bool BoolWorkingBenefitsShown
        {
            get { return m_BoolWorkingBenefitsShown; }
            set { m_BoolWorkingBenefitsShown = value; }
        }
        public bool BoolHideSpouseStrategyDivorce
        {
            get { return m_BoolHideSpouseStrategyDivorce; }
            set { m_BoolHideSpouseStrategyDivorce = value; }
        }
        public bool BoolHideAsapStrategyDivorce
        {
            get { return m_BoolHideAsapStrategyDivorce; }
            set { m_BoolHideAsapStrategyDivorce = value; }
        }
        public bool BoolShowSecondDivorce
        {
            get { return m_BoolShowSecondDivorce; }
            set { m_BoolShowSecondDivorce = value; }
        }

        public bool BoolShowSecondStrat
        {
            get { return m_BoolShowSecondStrat; }
            set { m_BoolShowSecondStrat = value; }
        }
        public bool BoolShowSecondDivorceInFirst
        {
            get { return m_BoolShowSecondDivorceInFirst; }
            set { m_BoolShowSecondDivorceInFirst = value; }
        }
        public bool BoolSpousalBenefitTakenDivorce
        {
            get { return m_BoolSpousalBenefitTakenDivorce; }
            set { m_BoolSpousalBenefitTakenDivorce = value; }
        }
        public bool BoolSpGrternWrkgFRADivorce
        {
            get { return m_BoolSpGrternWrkgFRADivorce; }
            set { m_BoolSpGrternWrkgFRADivorce = value; }
        }
        public bool BoolShowSecondDivorcePDF
        {
            get { return m_BoolShowSecondDivorcePDF; }
            set { m_BoolShowSecondDivorcePDF = value; }
        }
        public bool BoolShowSecondDivorcePDFLimit
        {
            get { return m_BoolShowSecondDivorcePDFLimit; }
            set { m_BoolShowSecondDivorcePDFLimit = value; }
        }
        public bool BoolShowSecondConditionDivorce
        {
            get { return m_BoolShowSecondConditionDivorce; }
            set { m_BoolShowSecondConditionDivorce = value; }
        }
        public bool BoolMaxStrategyCumm
        {
            get { return m_BoolMaxStrategyCumm; }
            set { m_BoolMaxStrategyCumm = value; }
        }
        public bool BoolHighlightCummAtWife69Max
        {
            get { return m_BoolHighlightCummAtWife69Max; }
            set { m_BoolHighlightCummAtWife69Max = value; }
        }
        public bool BoolShowThirdDivorceStrategy
        {
            get { return m_BoolShowThirdDivorceStrategy; }
            set { m_BoolShowThirdDivorceStrategy = value; }
        }
        public bool BoolAdvisorDownloadReport
        {
            get { return m_BoolAdvisorDownloadReport; }
            set { m_BoolAdvisorDownloadReport = value; }
        }
        public bool BoolShowSecondZeroStrategy
        {
            get { return m_BoolShowSecondZeroStrategy; }
            set { m_BoolShowSecondZeroStrategy = value; }
        }
        public bool BoolFBUserSession
        {
            get { return m_BoolFBUserSession; }
            set { m_BoolFBUserSession = value; }
        }
        public bool BoolIsHusbandClaimedBenefit
        {
            get { return m_BoolIsHusbandClaimedBenefit; }
            set { m_BoolIsHusbandClaimedBenefit = value; }
        }
        public bool BoolIsWifeClaimedBenefit
        {
            get { return m_BoolIsWifeClaimedBenefit; }
            set { m_BoolIsWifeClaimedBenefit = value; }
        }

        public bool BoolIsShowRestrictFull
        {
            get { return m_BoolIsShowRestrictFull; }
            set { m_BoolIsShowRestrictFull = value; }
        }


        public bool BoolSkipWHBSwitchRestrict
        {
            get { return m_BoolSkipWHBSwitchRestrict; }
            set { m_BoolSkipWHBSwitchRestrict = value; }
        }

        public bool BoolHideStrategyAge70
        {
            get { return m_BoolHideStrategyAge70; }
            set { m_BoolHideStrategyAge70 = value; }
        }
        public bool BoolShowRestOptionalCurrentAge
        {
            get { return m_BoolShowRestOptionalCurrentAge; }
            set { m_BoolShowRestOptionalCurrentAge = value; }
        }

        public bool BoolNoClaimRestrictForAdd2
        {
            get { return m_BoolNoClaimRestrictForAdd2; }
            set { m_BoolNoClaimRestrictForAdd2 = value; }
        }
        

        #endregion values used in the code as boolean

        #region values used in the code as int
        private int m_HusbandNumberofMonthsinNote = 0;
        private int m_WifeNumberofMonthinNote = 0;
        private int m_HusbandNumberofYears = 0;
        private int m_WifeNumberofYears = 0;
        private int m_HusbandNumberofMonthsinSteps = 0;
        private int m_WifeNumberofMonthsinSteps = 0;
        private int m_HusbNumberofMonthsBelowGrid = 0;
        private int m_WifeNumberofMonthsBelowGrid = 0;
        private int m_HusbandNumberofMonthsBelowGridAgePrior = 0;
        private int m_WifeNumberofMonthsBelowGridAgePrior = 0;
        private decimal m_WifeOriginalAnnualIncome = 0;
        private decimal m_HusbandOriginalAnnualIncome = 0;
        private int m_WifeNumberofMonthsAboveGrid = 0;
        private int m_HusbNumberofMonthsAboveGrid = 0;
        private int m_WifeNumberofYearsAbove66 = 0;
        private int m_HusbNumberofYearsAbove66 = 0;
        private decimal m_WifeSpousalBenefitAfter70 = 0;
        private decimal m_HusbSpousalBenefitAfter70 = 0;
        private decimal m_intWifeAge = 0;
        private decimal m_intWifeFRAAge = 0;
        private int m_HusbSpousalBenMonthAt69 = 0;
        private decimal m_HusbSpousalBenefitWhenWife66 = 0;
        private decimal m_HusbAnnualIcomeAt69 = 0;
        private decimal m_HusbAnnualIcomeAt70 = 0;
        //Restricted Application Strategy Variables
        private int m_intRestrictAppIncomeBestWife = 0;
        private int m_intRestrictAppIncomeBestHusb = 0;
        private int m_intRestrictAppIncomeFullWife = 0;
        private int m_intRestrictAppIncomeFullHusb = 0;
        private int m_intRestrictAppIncomeMaxWife = 0;
        private int m_intRestrictAppIncomeMaxHusb = 0;
        private int m_intRestrictAppIncomeEarlyWife = 0;
        private int m_intRestrictAppIncomeEarlyHusb = 0;

        private int m_intShowRestrictIncomeForBest = 0;
        private int m_intShowRestrictIncomeForFull = 0;
        private int m_intShowRestrictIncomeForMax = 0;
        private int m_intShowRestrictIncomeForEarly = 0;
        private int m_intShowRestrictIncomeForDivorce = 0;
        public int IntShowRestrictIncomeForMax
        {
            get { return m_intShowRestrictIncomeForMax; }
            set { m_intShowRestrictIncomeForMax = value; }
        }
        public int IntShowRestrictIncomeForFull
        {
            get { return m_intShowRestrictIncomeForFull; }
            set { m_intShowRestrictIncomeForFull = value; }
        }
        public int IntShowRestrictIncomeForBest
        {
            get { return m_intShowRestrictIncomeForBest; }
            set { m_intShowRestrictIncomeForBest = value; }
        }
        public int IntShowRestrictIncomeForEarly
        {
            get { return m_intShowRestrictIncomeForEarly; }
            set { m_intShowRestrictIncomeForEarly = value; }
        }
        public int IntShowRestrictIncomeForDivorce
        {
            get { return m_intShowRestrictIncomeForDivorce; }
            set { m_intShowRestrictIncomeForDivorce = value; }
        }


        public int intRestrictAppIncomeBestWife
        {
            get { return m_intRestrictAppIncomeBestWife; }
            set { m_intRestrictAppIncomeBestWife = value; }

        }
        public int intRestrictAppIncomeBestHusb
        {
            get { return m_intRestrictAppIncomeBestHusb; }
            set { m_intRestrictAppIncomeBestHusb = value; }

        }


        public int intRestrictAppIncomeFullWife
        {
            get { return m_intRestrictAppIncomeFullWife; }
            set { m_intRestrictAppIncomeFullWife = value; }

        }
        public int intRestrictAppIncomeFullHusb
        {
            get { return m_intRestrictAppIncomeFullHusb; }
            set { m_intRestrictAppIncomeFullHusb = value; }

        }

        public int intRestrictAppIncomeMaxWife
        {
            get { return m_intRestrictAppIncomeMaxWife; }
            set { m_intRestrictAppIncomeMaxWife = value; }

        }
        public int intRestrictAppIncomeMaxHusb
        {
            get { return m_intRestrictAppIncomeMaxHusb; }
            set { m_intRestrictAppIncomeMaxHusb = value; }

        }
        public int intRestrictAppIncomeEarlyWife
        {
            get { return m_intRestrictAppIncomeEarlyWife; }
            set { m_intRestrictAppIncomeEarlyWife = value; }

        }
        public int intRestrictAppIncomeEarlyHusb
        {
            get { return m_intRestrictAppIncomeEarlyHusb; }
            set { m_intRestrictAppIncomeEarlyHusb = value; }

        }




        public decimal intWifeFRAAge
        {
            get { return m_intWifeFRAAge; }
            set { m_intWifeFRAAge = value; }

        }
        public decimal intWifeAge
        {
            get { return m_intWifeAge; }
            set { m_intWifeAge = value; }

        }
        public decimal WifeSpousalBenefitAfter70
        {
            get { return m_WifeSpousalBenefitAfter70; }
            set { m_WifeSpousalBenefitAfter70 = value; }

        }
        public decimal HusbSpousalBenefitAfter70
        {
            get { return m_HusbSpousalBenefitAfter70; }
            set { m_HusbSpousalBenefitAfter70 = value; }

        }
        public int WifeNumberofYearsAbove66
        {
            get { return m_WifeNumberofYearsAbove66; }
            set { m_WifeNumberofYearsAbove66 = value; }

        }
        public int HusbNumberofYearsAbove66
        {
            get { return m_HusbNumberofYearsAbove66; }
            set { m_HusbNumberofYearsAbove66 = value; }
        }
        public decimal WifeOriginalAnnualIncome
        {
            get { return m_WifeOriginalAnnualIncome; }
            set { m_WifeOriginalAnnualIncome = value; }
        }
        public decimal HusbandOriginalAnnualIncome
        {
            get { return m_HusbandOriginalAnnualIncome; }
            set { m_HusbandOriginalAnnualIncome = value; }
        }
        public int WifeNumberofMonthsBelowGrid
        {
            get { return m_WifeNumberofMonthsBelowGrid; }
            set { m_WifeNumberofMonthsBelowGrid = value; }
        }
        public int HusbNumberofMonthsBelowGrid
        {
            get { return m_HusbNumberofMonthsBelowGrid; }
            set { m_HusbNumberofMonthsBelowGrid = value; }
        }
        public int WifeNumberofMonthsAboveGrid
        {
            get { return m_WifeNumberofMonthsAboveGrid; }
            set { m_WifeNumberofMonthsAboveGrid = value; }
        }
        public int HusbNumberofMonthsAboveGrid
        {
            get { return m_HusbNumberofMonthsAboveGrid; }
            set { m_HusbNumberofMonthsAboveGrid = value; }
        }

        public int HusbandNumberofMonthsBelowGridAgePrior
        {
            get { return m_HusbandNumberofMonthsBelowGridAgePrior; }
            set { m_HusbandNumberofMonthsBelowGridAgePrior = value; }
        }
        public int WifeNumberofMonthsBelowGridAgePrior
        {
            get { return m_WifeNumberofMonthsBelowGridAgePrior; }
            set { m_WifeNumberofMonthsBelowGridAgePrior = value; }
        }
        public int WifeNumberofYears
        {
            get { return m_WifeNumberofYears; }
            set { m_WifeNumberofYears = value; }
        }
        public int HusbandNumberofYears
        {
            get { return m_HusbandNumberofYears; }
            set { m_HusbandNumberofYears = value; }
        }
        public int WifeNumberofMonthinNote
        {
            get { return m_WifeNumberofMonthinNote; }
            set { m_WifeNumberofMonthinNote = value; }
        }
        public int HusbandNumberofMonthsinNote
        {
            get { return m_HusbandNumberofMonthsinNote; }
            set { m_HusbandNumberofMonthsinNote = value; }
        }
        public int WifeNumberofMonthsinSteps
        {
            get { return m_WifeNumberofMonthsinSteps; }
            set { m_WifeNumberofMonthsinSteps = value; }
        }
        public int HusbandNumberofMonthsinSteps
        {
            get { return m_HusbandNumberofMonthsinSteps; }
            set { m_HusbandNumberofMonthsinSteps = value; }
        }
        public int HusbSpousalBenMonthAt69
        {
            get { return m_HusbSpousalBenMonthAt69; }
            set { m_HusbSpousalBenMonthAt69 = value; }
        }

        public decimal HusbSpousalBenefitWhenWife66
        {
            get { return m_HusbSpousalBenefitWhenWife66; }
            set { m_HusbSpousalBenefitWhenWife66 = value; }
        }

        public decimal HusbAnnualIcomeAt70
        {
            get { return m_HusbAnnualIcomeAt70; }
            set { m_HusbAnnualIcomeAt70 = value; }
        }
        public decimal HusbAnnualIcomeAt69
        {
            get { return m_HusbAnnualIcomeAt69; }
            set { m_HusbAnnualIcomeAt69 = value; }
        }
        #endregion values used in the code as int


    }
}
