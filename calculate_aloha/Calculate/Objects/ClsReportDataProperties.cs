﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Calculate
{
    public static class ClsReportDataProperties
    {
        //Used to hold the MemoryStream object of PDF Strategy Report
        public static MemoryStream objMemoryStream { get; set; }

        //Used to hold the object of PDF Strategy Report
        public static Document pdfDocument { get; set; }

        //Used to hold the username
        public static string strUserName { get; set; }
    }
}