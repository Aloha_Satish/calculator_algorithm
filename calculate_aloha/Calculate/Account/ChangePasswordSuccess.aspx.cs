﻿using CalculateDLL;
using CalculateDLL.TO;
using Symbolics;
using System;
using System.Data;
using System.Web.UI;

namespace Calculate.Account
{
    public partial class ChangePasswordSuccess : System.Web.UI.Page
    {
        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion Events

        protected void btnRedirectLogin_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectLogin, false);
            }
            catch (Exception ex)
            {
                 ErrorLog.WriteError(Constants.ErrorRegister + "btnRedirectLogin_Click()" + ex.ToString());
                lblUserLoginError.Text = "Please enter the valid Login details!";
                lblUserLoginError.Visible = true;
            }
        }
    }
}
