﻿using CalculateDLL;
using CalculateDLL.TO;
using Symbolics;
using System;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculate.SuperAdmin
{
    public partial class ManageAdvisors : System.Web.UI.Page
    {
        #region Variables
        TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
        System.Collections.Generic.List<string> listState = new System.Collections.Generic.List<string> {"","AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI",
                                                                                                                 "ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN",
                                                                                                                 "MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH",
                                                                                                                 "OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA",
                                                                                                                 "WV","WI","WY"};
        #endregion Variables

        #region Events
        /// <summary>
        /// Function call when page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ValidateSession();
                if (!IsPostBack)
                {
                    lblErrorMessage.Visible = false;
                    if (!Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                    {
                        Institutional objInstitution = new Institutional();
                        DataTable dtInstitution;
                        if (!String.IsNullOrEmpty(Request.QueryString[Constants.QueryStringInstitutionAdminID]))
                            dtInstitution = objInstitution.getInstitutionalDetailsByID(Request.QueryString[Constants.QueryStringInstitutionAdminID]);
                        else
                        {
                            dtInstitution = objInstitution.getInstitutionalDetailsByID(Session[Constants.SessionAdminId].ToString());
                            //Session["StrInstitutionAdminID"] = Session[Constants.SessionAdminId];
                        }
                        if (dtInstitution.Rows.Count > 0)
                        {
                            lblDisplayName.Text = dtInstitution.Rows[Constants.zero][Constants.InstitutionName].ToString();
                            Session[Constants.StrInstitutionAdminID] = Request.QueryString[Constants.QueryStringInstitutionAdminID];
                        }
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, "manageBreadscrumb();", true);
                    }
                    if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                    {
                        Session[Constants.StrInstitutionAdminID] = Session[Constants.SessionUserId].ToString();
                    }
                    this.BindData();
                    Page.MaintainScrollPositionOnPostBack = true;
                    if (Request.QueryString[Constants.id] != null)
                    {
                        lblErrorMessage.Visible = true;
                        lblErrorMessage.Text = Constants.AdvisorAdded;
                        lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                        Advisor objAdvisor = new Advisor();
                        objAdvisor.ActivateAdvisor(Request.QueryString[Constants.reference].ToString());
                        this.BindData();
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                    }
                }
                /* Initialize ScriptManager for calling Javascript Function */
                ScriptManager sm = ScriptManager.GetCurrent(Page);
                if (sm.IsInAsyncPostBack)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.ClientScriptDatePickerName, Constants.ClientScriptDatePicker, true);
                else
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.ClientScriptDatePickerName, Constants.ClientScriptLoadDatePicker, true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Filter Records based on Date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dateFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Reset Search Filteration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Reset(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                txtSearch.Text = String.Empty;
                dateFilter.Text = String.Empty;
                nameSelection.SelectedIndex = 0;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Reset Search Filteration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                txtSearch.Text = String.Empty;
                dateFilter.Text = String.Empty;
                nameSelection.SelectedIndex = 0;
                FilterGridViewData();
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveOverlay, "removeoverlay(true);", true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Append Images while binding data into GridView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                /* Check if gridview row event matched with data control row */
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    /* Get instance of selected column */
                    ImageButton imgButton = (ImageButton)e.Row.FindControl(Constants.imgStatus);
                    Label EmailID = (Label)e.Row.FindControl(Constants.lblEmail);
                    Label CreatedOn = (Label)e.Row.FindControl(Constants.lblCreatedDate);
                    Label lblAdvisorAdminID = (Label)e.Row.FindControl(Constants.lblAdvisorAdminID);
                    CreatedOn.Text = string.Format(Constants.stringDateFormat, Convert.ToDateTime(CreatedOn.Text)).Replace("-", "/");
                    /* Create object of User */
                    Users objUser = new Users();
                    /* Get Active Flag for particular Email ID from User Login Table */
                    DataTable dtUserDetails = objUser.getUserDetails(EmailID.Text);
                    /* If Row count greater than 0,then we have data */
                    if (dtUserDetails.Rows.Count > Constants.zero)
                    {
                        /* Assign value into image alternate text and images into image URL */
                        imgButton.AlternateText = dtUserDetails.Rows[Constants.zero][Constants.UserActiveFlag].ToString();
                        if (Convert.ToString(dtUserDetails.Rows[Constants.zero][Constants.UserActiveFlag]).Equals(Constants.FlagYes))
                        {
                            imgButton.ImageUrl = Constants.ActivateImage;
                            if (!lblAdvisorAdminID.Text.Contains("D"))
                            {
                                imgButton.Attributes.Add(Constants.title, Constants.Suspend);
                            }
                            else
                            {
                                imgButton.Attributes.Add(Constants.title, Constants.ReAssign);
                            }
                        }
                        else
                        {
                            imgButton.ImageUrl = Constants.DeactivateImage;
                            imgButton.Attributes.Add(Constants.title, Constants.Activate);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Activate/Deactivate Institution
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Status(object sender, EventArgs e)
        {
            try
            {
                Session[Constants.Action] = Constants.Suspend;
                btnAssignDelete.Text = Constants.AssignandSuspend;
                lblDeleteAdvisor.Text = Constants.SuspendAdvisor;
                // Assign Deleted/Suspended user functionality to another advisor.
                using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                {
                    Label AdvisorAdminID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblAdvisorAdminID);
                    Session[Constants.AdvisorID] = AdvisorAdminID.Text;
                    ImageButton imgButton = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[0].FindControl(Constants.imgStatus);
                    Session[Constants.imgButton] = imgButton;
                    DataTable dtAllCustomer = BindCustomerList(AdvisorAdminID.Text);
                    if (dtAllCustomer.Rows.Count == 0)
                    {
                        if (!AdvisorAdminID.Text.Contains("D"))
                        {
                            SuspendAdvisor(Session[Constants.AdvisorID].ToString(), imgButton);
                            ShowMsgAdvisorStatusUpdate();
                        }
                        else
                        {
                            lblErrorMessage.Visible = true;
                            lblErrorMessage.Text = Constants.DefaultAdvisorDeleteText;
                            lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                        }
                    }
                    else
                    {
                        DataTable dtAllAdvisor;
                        Advisor objAdvisor = new Advisor();
                        /* Get all Advisor Data from database and bind to ddl Advisors control */
                        dtAllAdvisor = objAdvisor.getALLAdvisors(AdvisorAdminID.Text, Session[Constants.StrInstitutionAdminID].ToString());
                        if (dtAllAdvisor.Rows.Count > 0)
                        {
                            /* Fill Advisor Name into drop down list */
                            ddlAdvisors.Items.Clear();
                            ddlAdvisors.Items.Insert(Constants.zero, Constants.SelectAdvisor);
                            ddlAdvisors.Items[Constants.zero].Value = String.Empty;
                            ddlAdvisors.AppendDataBoundItems = true;
                            ddlAdvisors.DataSource = dtAllAdvisor;
                            ddlAdvisors.DataValueField = Constants.AdvisorID;
                            ddlAdvisors.DataTextField = Constants.AdvisorName;
                            ddlAdvisors.DataBind();
                            /* Show Popup with Advisor Fields data */
                            popupDelete.Show();
                            /* Display Panel */
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayPanelDelete();", true);
                        }
                        else
                        {
                            lblErrorMessage.Visible = true;
                            lblErrorMessage.Text = Constants.LastAdvisorText;
                            lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                        }
                    }
                    //BindData();
                }
                #region Old Code For Direct Suspend
                //lblErrorMessage.Visible = false;
                //updateActiveFlag(sender, gvDetails);
                //lblErrorMessage.Visible = true;
                //lblErrorMessage.Text = "Advisor status updated successfully";
                //lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                //ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                #endregion Old Code For Direct Suspend
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /* Method to Suspend Advisor Dev:- Aloha  Date:-03/12/2014*/
        /// <summary>
        ///  Suspend Advisor By Advisor ID
        /// </summary>
        /// <param name="sender">strAdvisorID</param>
        private void SuspendAdvisor(string InstitutionID, ImageButton imgButton)
        {
            try
            {
                lblErrorMessage.Visible = false;
                updateActiveFlag(InstitutionID, imgButton, gvDetails);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Used to show message after advisor's status update
        /// </summary>
        private void ShowMsgAdvisorStatusUpdate()
        {
            try
            {
                lblErrorMessage.Visible = true;
                lblErrorMessage.Text = Constants.AdvisorUpdated;
                lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        ///  Filter gridview data based on search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void nameSelection_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        ///  Filter gridview data based on search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Function call in pagination
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                this.BindData();
                gvDetails.PageIndex = e.NewPageIndex;
                /* Reload gridview with updated data */
                gvDetails.DataBind();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// We can edit Advisor details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void editGridViewDetails(object sender, EventArgs e)
        {
            try
            {
                /* Check if selected row contain imagebutton control  or not */
                using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                {
                    Label AdvisorAdminID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblAdvisorAdminID);

                    if (!AdvisorAdminID.Text.Contains("D"))
                    {
                        lblErrorMessage.Visible = false;
                        txtFirstname.Focus();
                        lblHeader.Text = Constants.LblEditHeaderAdvisor;
                        clsFlag.Flag = false;
                        #region Fill Advisor Details into Popup
                        /* Get content from gridview and assign to runtime created label */
                        Label Firstname = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblFirstname);
                        Label Lastname = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblLastname);
                        Label Email = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblEmail);
                        Label AdvisorName = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblAdvisorName);
                        Label Password = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblPassword);
                        Label State = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblState);
                        Label Dealer = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblDealer);

                        /*take backup of email for to verify user mail id*/
                        ViewState[Constants.UserEmail] = Email.Text;
                        /* Assign label data into textbox which are shown up in blockui popup */
                        txtEmail.Text = Email.Text;
                        txtLastname.Text = Lastname.Text;
                        txtFirstname.Text = Firstname.Text;
                        txtAdvisorID.Text = AdvisorAdminID.Text;
                        txtDealer.Text = Dealer.Text;

                        ddlState.DataSource = listState;
                        ddlState.DataBind();
                        ddlState.SelectedValue = State.Text;

                        //txtPassword.Text = Encryption.Decrypt(Password.Text);
                        //txtConfirmPassword.Text = Encryption.Decrypt(Password.Text);
                        //txtPassword.Attributes.Add(Constants.HtmlValue, Encryption.Decrypt(Password.Text));
                        //txtConfirmPassword.Attributes.Add(Constants.HtmlValue, Encryption.Decrypt(Password.Text));
                        #endregion
                        txtEmail.Enabled = false;
                        /* Show Popup with Advisor Fields data */
                        popup.Show();
                        /* Display Panel */
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayPanel();", true);
                    }
                    else
                    {
                        lblErrorMessage.Visible = true;
                        lblErrorMessage.Text = Constants.DefaultAdvisorText;
                        lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Add new Advisor details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddInstitution(object sender, EventArgs e)
        {
            try
            {
                clsFlag.Flag = true;
                lblErrorMessage.Visible = false;
                txtFirstname.Focus();
                lblHeader.Text = Constants.LblAddHeaderAdvisor;
                #region Clear all fields
                txtEmail.Text = String.Empty;
                txtLastname.Text = String.Empty;
                txtFirstname.Text = String.Empty;
                txtAdvisorID.Text = String.Empty;
                //txtPassword.Text = String.Empty;
                //txtConfirmPassword.Text = String.Empty;
                txtFirstname.ReadOnly = false;
                txtLastname.ReadOnly = false;
                txtDealer.Text = String.Empty;
                #endregion
                txtEmail.Enabled = true;

                ddlState.DataSource = listState;
                ddlState.DataBind();
                ddlState.SelectedValue = string.Empty;
                /* Show BlockUI PopUp */
                popup.Show();
                /* Display Panel */
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayPanel();", true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        protected void AddDefaultAdviser(object sender, EventArgs e)
        {
            try
            {
                clsFlag.Flag = true;
                lblErrorMessage.Visible = false;
                txtEmail.Focus();
                lblHeader.Text = Constants.LblAddHeaderAdvisor;
                #region Clear all fields
                txtEmail.Text = String.Empty;
                txtLastname.Text = Constants.DefaultAdvisorName;
                txtFirstname.Text = Constants.DefaultAdvisorName;
                txtLastname.ReadOnly = true;
                txtFirstname.ReadOnly = true;
                txtAdvisorID.Text = String.Empty;
                //txtPassword.Text = String.Empty;
                //txtConfirmPassword.Text = String.Empty;
                #endregion
                /* Show BlockUI PopUp */
                popup.Show();
                /* Display Panel */
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayPanel();", true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Delete selected gridview row 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete(object sender, EventArgs e)
        {
            try
            {
                Session[Constants.Action] = Constants.delete;
                btnAssignDelete.Text = Constants.AssignandDelete;
                lblDeleteAdvisor.Text = Constants.DeleteAdvisor;
                // Assign Deleted/Suspended user functionality to another advisor.
                using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                {
                    Label AdvisorAdminID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblAdvisorAdminID);
                    Session[Constants.AdvisorID] = AdvisorAdminID.Text;
                    // Check "D" in AdvisorAdminID.Text - IF found then show the message
                    // AdvisorAdminID.Text.Substring(0, 1).Equals("D");
                    if (!AdvisorAdminID.Text.Contains("D"))
                    {
                        DataTable dtAllCustomer = BindCustomerList(AdvisorAdminID.Text);
                        if (dtAllCustomer.Rows.Count == 0)
                            DeleteAdvisor(AdvisorAdminID.Text);
                        else
                        {
                            DataTable dtAllAdvisor;
                            Advisor objAdvisor = new Advisor();
                            /* Get all Advisor Data from database and bind to ddl Advisors control */
                            dtAllAdvisor = objAdvisor.getALLAdvisors(AdvisorAdminID.Text, Session[Constants.StrInstitutionAdminID].ToString());
                            if (dtAllAdvisor.Rows.Count > 0)
                            {
                                /* Fill Advisor Name into drop down list */
                                ddlAdvisors.Items.Clear();
                                ddlAdvisors.Items.Insert(Constants.zero, Constants.SelectAdvisor);
                                ddlAdvisors.Items[Constants.zero].Value = String.Empty;
                                ddlAdvisors.AppendDataBoundItems = true;
                                ddlAdvisors.DataSource = dtAllAdvisor;
                                ddlAdvisors.DataValueField = Constants.AdvisorID;
                                ddlAdvisors.DataTextField = Constants.AdvisorName;
                                ddlAdvisors.DataBind();
                                /* Show Popup with Advisor Fields data */
                                popupDelete.Show();
                                /* Display Panel */
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayPanelDelete();", true);
                            }
                            else
                            {
                                lblErrorMessage.Visible = true;
                                lblErrorMessage.Text = Constants.LastAdvisorText;
                                lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                            }
                        }
                    }
                    else
                    {
                        lblErrorMessage.Visible = true;
                        lblErrorMessage.Text = Constants.DeleteAdvisorText;
                        lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                    }
                }
                #region Old Code For Direct delete
                /* Check if selected row contain imagebutton control  or not */
                //using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                //{
                //    lblErrorMessage.Visible = false;
                //    /* Get Advisor Id from gridview and assign to runtime created label */
                //    Label AdvisorAdminID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("lblAdvisorAdminID");
                //    /* Initialize object */
                //    Advisor objAdvisor = new Advisor();
                //    AdvisorTO objAdvisorTO = new AdvisorTO();
                //    objAdvisorTO.AdvisorID = AdvisorAdminID.Text;
                //    /* Call function to delete selected row by passing id into function */
                //    objAdvisor.deleteAdvisorInfo(objAdvisorTO);
                //    /* Reload gridview with updated data */
                //    this.BindData();
                //    lblErrorMessage.Visible = true;
                //    lblErrorMessage.Text = "Advisor deleted successfully";
                //    lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                //    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                //}
                #endregion Old Code For Direct delete
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Get All customers for Advisor ID 
        /// </summary>
        /// <param name="strAdvisorID">AdvisorID</param>
        /// <returns>DataTable of ALL Customers </returns>
        private DataTable BindCustomerList(string strAdvisorID)
        {
            try
            {
                DataTable dtALLCustomers;
                Advisor objAdvisor = new Advisor();
                dtALLCustomers = objAdvisor.getALLCustomers(strAdvisorID);
                dtALLCustomers.Columns.Add("FullName", typeof(string), "FirstName + ' - ' + Email");
                //dtALLCustomers.Columns.Add("FullName", typeof(string), Constants.CustomerFirstName + Constants.UserEmail);
                //DataTable dtAllAdvisor;
                //dtAllAdvisor = objAdvisor.getAdvisorDetailsByID(strAdvisorID);
                lstCustomers.DataSource = dtALLCustomers;
                lstCustomers.DataTextField = "FullName";
                lstCustomers.DataValueField = Constants.CustomerId;
                lstCustomers.DataBind();
                return dtALLCustomers;
            }

            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                return null;
            }
        }

        protected void toggleSelection(bool isChecked)
        {

            try
            {
                if (isChecked)
                    for (int j = 0; j < lstCustomers.Items.Count; j++)
                        lstCustomers.Items[j].Selected = true;
                else
                    for (int j = 0; j < lstCustomers.Items.Count; j++)
                        lstCustomers.Items[j].Selected = false;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        protected void lstCustomers_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkSelectAll.Checked)
                {
                    for (int j = 0; j < lstCustomers.Items.Count; j++)
                    {
                        lstCustomers.Items[j].Selected = true;
                    }
                }
                else
                {
                    for (int j = 0; j < lstCustomers.Items.Count; j++)
                    {
                        lstCustomers.Items[j].Selected = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// AssignDelete selected gridview row 
        /// </summary>
        protected void AssignDelete(object sender, EventArgs e)
        {
            try
            {
                bool isAdvisorSelect, isCustomerSelect = false;
                if (ddlAdvisors.SelectedValue != string.Empty)
                    isAdvisorSelect = true;
                else
                    isAdvisorSelect = false;
                for (int i = 0; i < lstCustomers.Items.Count; i++)
                {
                    if (lstCustomers.Items[i].Selected)
                        isCustomerSelect = true;
                }
                if (isAdvisorSelect && isCustomerSelect)
                {
                    /* Create Object */
                    Advisor ObjAdvisor = new Advisor();
                    int intCntAssigned = 0;
                    //int intAllCustomer = lstCustomers.Items.Count;
                    //GET THE SELECTED VALUE FROM CHECKBOX LIST
                    for (int i = 0; i < lstCustomers.Items.Count; i++)
                    {
                        if (lstCustomers.Items[i].Selected)
                        {
                            //string strCustomerID = lstCustomers.Items[i].Value;
                            ObjAdvisor.updateAssignAdvisor(ddlAdvisors.SelectedValue, lstCustomers.Items[i].Value);
                            intCntAssigned++;
                        }
                    }
                    // Get all Customers and bind to customer list
                    DataTable dtAllCustomer = BindCustomerList(Session[Constants.AdvisorID].ToString());
                    string strAction = Session[Constants.Action].ToString();
                    if (dtAllCustomer.Rows.Count == 0)
                    {
                        if (strAction.Equals(Constants.delete))
                            DeleteAdvisor(Session[Constants.AdvisorID].ToString()); // Delete Advisor if No customer exists
                        else
                        {
                            if (!Session[Constants.AdvisorID].ToString().Contains("D"))
                            {
                                SuspendAdvisor(Session[Constants.AdvisorID].ToString(), (ImageButton)Session[Constants.imgButton]); // Suspend Advisor if No customer exists  Date:-03/12/2014
                                BindData();
                                ShowMsgAdvisorStatusUpdate();
                            }
                            else
                            {
                                lblErrorMessage.Visible = true;
                                lblErrorMessage.Text = Constants.DefaultAdvisorDeleteText; ;
                                lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                            }
                        }
                    }
                    else
                    {
                        lblErrorMessage.Visible = true;
                        lblErrorMessage.Text = intCntAssigned + Constants.CustomerAddedSuccessfuly + (lstCustomers.Items.Count - intCntAssigned) + Constants.Remaining;
                        lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                    }
                }
                else
                {
                    if (!isAdvisorSelect)
                    {
                        /* Show Popup with Advisor Fields data */
                        popupDelete.Show();
                        /* Display Panel */
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayPanelPopUpAssignAd(true);", true);
                    }
                    if (!isCustomerSelect)
                    {
                        /* Show Popup with Advisor Fields data */
                        popupDelete.Show();
                        /* Display Panel */
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayPanelPopUpAssignAd(false);", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Delete Advisor By Advisor ID
        /// </summary>
        /// <param name="strAdvisorID">AdvisorID</param>
        private void DeleteAdvisor(string strAdvisorID)
        {
            try
            {
                Advisor ObjAdvisor = new Advisor();
                lblErrorMessage.Visible = false;
                AdvisorTO objAdvisorTO = new AdvisorTO();
                objAdvisorTO.AdvisorID = strAdvisorID;
                /* Call function to delete selected row by passing id into function */
                ObjAdvisor.deleteAdvisorInfo(objAdvisorTO);
                /* Reload gridview with updated data */
                this.BindData();
                lblErrorMessage.Visible = true;
                lblErrorMessage.Text = Constants.AdvisorDeletedSucessfuly;
                lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// CancelDelete selected gridview row 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CancelDelete(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                txtSearch.Text = String.Empty;
                dateFilter.Text = String.Empty;
                nameSelection.SelectedIndex = 0;
                FilterGridViewData();
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveOverlay, "removeoverlay(true);", true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// view advisor content based on institutional Id
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void View(object sender, EventArgs e)
        {
            try
            {
                using (GridViewRow row = (GridViewRow)((Button)sender).Parent.Parent)
                {
                    lblErrorMessage.Visible = false;
                    /* Redirect to Advisor UI with institution Id */
                    Label CustomerID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblAdvisorAdminID);
                    Response.Redirect(Constants.RedirectManageAdvisorWithAdvisorID + CustomerID.Text, false);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Save institutional details into database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                /* check if popup open for add or edit mode */
                if (clsFlag.Flag)
                {
                    Institutional objInstitutional = new Institutional();
                    DataTable dtAdvisor = objInstitutional.getInstitutionalDetailsByID(Session[Constants.SessionAdminId].ToString());
                    if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) && dtAdvisor.Rows[Constants.zero][Constants.ChoosePlan].ToString().Equals(Constants.PerClient))
                    {
                        //passwordTextBox.Text = txtPassword.Text;
                        popup.Show();
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, "displayPanelPopUp(true);", true);
                        //txtPassword.Text = passwordTextBox.Text;
                        //txtConfirmPassword.Text = passwordTextBox.Text;
                    }
                    else
                    {
                        //code amended on 08/14/2015
                        //code to set the customer subscription to yes if the advisor is subscribed and charge is Flat Charge
                        //if (dtAdvisor.Rows[0][Constants.IsSubscription].ToString().Equals(Constants.FlagYes) && dtAdvisor.Rows[0][Constants.ChoosePlan].ToString().Equals(Constants.FlatCharge))
                        //    objAdvisor = addAdvisor(false,true);
                        //else
                        //objAdvisor = addAdvisor(false,false);
                        //comment this line and uncomment the above if else
                        AdvisorTO objAdvisor = addAdvisor(false);
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                    }
                }
                else
                {
                    /* Create object of Customer Class */
                    Customers objCustomer = new Customers();
                    objCustomer.Email = txtEmail.Text;
                    /*email address retrive from ViewState */
                    var emailText = ViewState[Constants.UserEmail];
                    /* Check if email address already exists or not */
                    if (!objCustomer.isCustomerExist(objCustomer) || txtEmail.Text.ToString() == emailText.ToString())
                    {
                        /* initialize Instance of classes */
                        AdvisorTO objAdvisorTO = new AdvisorTO();
                        Advisor objAdvisor = new Advisor();
                        #region set Update Parameters
                        objAdvisorTO.Firstname = textInfo.ToTitleCase(txtFirstname.Text);
                        objAdvisorTO.Lastname = textInfo.ToTitleCase(txtLastname.Text);
                        objAdvisorTO.Email = txtEmail.Text;
                        //objAdvisorTO.Password = Encryption.Encrypt(txtPassword.Text);
                        objAdvisorTO.AdvisorID = txtAdvisorID.Text;
                        objAdvisorTO.State = ddlState.SelectedValue;
                        objAdvisorTO.Dealer = txtDealer.Text;
                        #endregion set Update Parameters
                        /* Update Institution Details into database */
                        objAdvisor.updateAdvisorInfo(objAdvisorTO);
                        /* Reload gridview with updated data */
                        BindData();
                        lblErrorMessage.Visible = true;
                        lblErrorMessage.Text = Constants.AdvisorUpdatedSucessfuly;
                        lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Constants.alertMessage, Constants.AdvisorEmialExists, true);
                    }
                }
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveOverlay, "removeoverlay(true);", true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// btnTrial's Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnTrial_Click(object sender, EventArgs e)
        {
            try
            {
                //code amended on 08/14/2015
                //to set the advisor subscription to yes if the institution has flat charge and subscrition to yes
                //comment the current code and uncomment the below line
                //AdvisorTO objAdvisor = addAdvisor(true,false);
                AdvisorTO objAdvisor = addAdvisor(true);
                //Changes for Live Server
                if (!String.IsNullOrEmpty(objAdvisor.AdvisorID))
                    Response.Redirect(Constants.LiveAdvisorURL + objAdvisor.AdvisorID + Constants.FirstNameKeyInURL + objAdvisor.Firstname + Constants.LastNameKeyInURL + objAdvisor.Lastname + Constants.BillingFirstNameKeyInURL + objAdvisor.Firstname + Constants.BillingLastNameKeyInURL + objAdvisor.Lastname + Constants.EmailKeyInURL + objAdvisor.Email, true);
                //Changes for Aloha Test Server
                //if (!String.IsNullOrEmpty(objAdvisor.AdvisorID))
                // Response.Redirect(Constants.TestAdvisorURL + objAdvisor.AdvisorID + "&first_name=" + objAdvisor.Firstname + "&last_name=" + objAdvisor.Lastname + "&billing_first_name=" + objAdvisor.Firstname + "&billing_last_name=" + objAdvisor.Lastname + "&email=" + objAdvisor.Email, true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Sorting Gridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvDetails_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;
                lblErrorMessage.Visible = false;
                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, Constants.SortingDescending);
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, Constants.SortingAscending);
                }
                
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        #endregion Events

        #region Methods
        /// <summary>
        /// Validate Session
        /// </summary>
        public void ValidateSession()
        {
            try
            {
                //if user session is expired redirstc him to login again
                Session[Constants.SessionRegistered] = null;
                if (Session[Constants.SessionUserId] == null)
                {
                    Response.Redirect(Constants.RedirectLogin, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState[Constants.sortDirection] == null)
                    ViewState[Constants.sortDirection] = SortDirection.Ascending;

                return (SortDirection)ViewState[Constants.sortDirection];
            }
            set { ViewState[Constants.sortDirection] = value; }
        }

        /// <summary>
        /// Sorting Gridview
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="direction"></param>
        private void SortGridView(string sortExpression, string direction)
        {
            try
            {
                Advisor objAdvisor = new Advisor();
                DataTable tableRecords;
                /* Fill gridview detail based on filteration */
                if (!String.IsNullOrEmpty(Request.QueryString[Constants.QueryStringInstitutionAdminID]))
                    tableRecords = objAdvisor.getAdvisorDetailsFilter(nameSelection.Text, String.IsNullOrEmpty(dateFilter.Text) ? String.Empty : string.Format(Constants.stringDateFormat, Convert.ToDateTime(dateFilter.Text)), txtSearch.Text, Request.QueryString[Constants.QueryStringInstitutionAdminID]);
                else
                    tableRecords = objAdvisor.getAdvisorDetailsFilter(nameSelection.Text, String.IsNullOrEmpty(dateFilter.Text) ? String.Empty : string.Format(Constants.stringDateFormat, Convert.ToDateTime(dateFilter.Text)), txtSearch.Text, Session[Constants.SessionAdminId].ToString());
                DataView dv = new DataView(tableRecords);
                dv.Sort = sortExpression + direction;
                gvDetails.DataSource = dv;
                gvDetails.DataBind();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;

            }
        }

        /// <summary>
        /// Bind institutional details into gridview control
        /// </summary>
        private void BindData()
        {
            try
            {
                /* Create Object */
                DataTable tableRecords;
                Advisor objAdvisor = new Advisor();
                /* Get all institutional Data from database and bind to gridview control */
                if (!String.IsNullOrEmpty(Request.QueryString[Constants.QueryStringInstitutionAdminID]))
                    tableRecords = objAdvisor.getAdvisorDetails(Request.QueryString[Constants.QueryStringInstitutionAdminID]);
                else
                    tableRecords = objAdvisor.getAdvisorDetails(Session[Constants.SessionAdminId].ToString());
                lblPageTotalNumber.Text = tableRecords.Rows.Count.ToString();
                /* Check if datatable contain data or not */
                if (tableRecords.Rows.Count > Constants.zero)
                {
                    if (gvDetails.AllowSorting == false)
                        gvDetails.AllowSorting = true;
                    /* Fill data into gridview */
                    gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                    /* Fill Advisor Name into drop down list */
                    nameSelection.Items.Clear();
                    nameSelection.Items.Insert(Constants.zero, Constants.SelectAdvisor);
                    nameSelection.Items[Constants.zero].Value = String.Empty;
                    nameSelection.AppendDataBoundItems = true;
                    nameSelection.DataSource = tableRecords;
                    nameSelection.DataBind();
                    foreach (GridViewRow row in gvDetails.Rows)
                    {
                        Label lblAdvisorAdminID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblAdvisorAdminID);
                        if (lblAdvisorAdminID.Text.Contains("D"))
                        {
                            ImageButton imgSuspend = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("imgStatus");
                            ImageButton imgEdit = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("linkEdit");
                            ImageButton imgDelete = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("linkDelete");
                            imgSuspend.Visible = false;
                            imgEdit.Visible = false;
                            imgDelete.Visible = false;
                        }
                    }
                }
                else
                {
                    //gvDetails.AllowSorting = false;
                    ///* Show blank table data with message */
                    //tableRecords.Rows.Add(tableRecords.NewRow());
                    //gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                    ///* Get column count */
                    //int totalcolums = gvDetails.Rows[Constants.zero].Cells.Count;
                    //gvDetails.Rows[Constants.zero].Cells.Clear();
                    //gvDetails.Rows[Constants.zero].Cells.Add(new TableCell());
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].ColumnSpan = totalcolums;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].Text = Constants.GridViewDefaultAdvisor;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].HorizontalAlign = HorizontalAlign.Center;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].ForeColor = System.Drawing.Color.Red;
                    ///* Fill drop down list with default Data */
                    //nameSelection.Items.Clear();
                    //nameSelection.Items.Insert(Constants.zero, Constants.DropDownAdvisorDefault);
                    //nameSelection.Items[Constants.zero].Value = String.Empty;
                    //nameSelection.DataBind();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;

            }
        }

        /// <summary>
        /// Filter Grid View Data based on Selection Criteria
        /// </summary>
        protected void FilterGridViewData()
        {
            try
            {
                /* Create an instance for Institution Class */
                Advisor objAdvisor = new Advisor();
                DataTable tableRecords;
                /* Fill gridview detail based on filteration */
                if (!String.IsNullOrEmpty(Request.QueryString[Constants.QueryStringInstitutionAdminID]))
                    tableRecords = objAdvisor.getAdvisorDetailsFilter(nameSelection.Text, String.IsNullOrEmpty(dateFilter.Text) ? String.Empty : string.Format(Constants.stringDateFormat, Convert.ToDateTime(dateFilter.Text)), txtSearch.Text, Request.QueryString[Constants.QueryStringInstitutionAdminID]);
                else
                    tableRecords = objAdvisor.getAdvisorDetailsFilter(nameSelection.Text, String.IsNullOrEmpty(dateFilter.Text) ? String.Empty : string.Format(Constants.stringDateFormat, Convert.ToDateTime(dateFilter.Text)), txtSearch.Text, Session[Constants.SessionAdminId].ToString());
                /* Check if datatable contain data or not */
                if (tableRecords.Rows.Count > Constants.zero)
                {
                    /* Fill data into gridview */
                    gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                    foreach (GridViewRow row in gvDetails.Rows)
                    {
                        Label lblAdvisorAdminID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl(Constants.lblAdvisorAdminID);
                        if (lblAdvisorAdminID.Text.Contains("D"))
                        {
                            ImageButton imgSuspend = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("imgStatus");
                            ImageButton imgEdit = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("linkEdit");
                            ImageButton imgDelete = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("linkDelete");
                            imgSuspend.Visible = false;
                            imgEdit.Visible = false;
                            imgDelete.Visible = false;
                        }
                    }
                }
                else
                {
                    ///* Show blank table data with message */
                    //tableRecords.Rows.Add(tableRecords.NewRow());
                    //gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                    ///* Get column count */
                    //int totalcolums = gvDetails.Rows[Constants.zero].Cells.Count;
                    //gvDetails.Rows[Constants.zero].Cells.Clear();
                    //gvDetails.Rows[Constants.zero].Cells.Add(new TableCell());
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].ColumnSpan = totalcolums;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].Text = Constants.GridViewDefaultAdvisor;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].HorizontalAlign = HorizontalAlign.Center;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].ForeColor = System.Drawing.Color.Red;
                }
                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Update Active Flag 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="gvDetails"></param>
        public void updateActiveFlag(string InstitutionID, ImageButton imgButton, GridView gvDetails)
        {
            try
            {
                //using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                //{
                /* Redirect to Advisor UI with institution Id */
                //Label InstitutionID = (Label)gvDetails.Rows[row.RowIndex].Cells[0].FindControl("lblUserID");
                //ImageButton imgButton = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[0].FindControl("imgStatus");
                Customers objCustomer = new Customers();
                DataTable dtCustomer = objCustomer.getCustomerDetailsByAdvisor(InstitutionID);
                Users objUser = new Users();
                /* Check if Active flag matched with value */
                if (Convert.ToString(imgButton.AlternateText).Equals(Constants.FlagYes))
                {
                    /* Inverse the value and image url for UI */
                    imgButton.AlternateText = Constants.FlagNo;
                    imgButton.ImageUrl = Constants.DeactivateImage;
                    imgButton.Attributes.Add(Constants.title, Constants.Activate);
                }
                else
                {
                    /* Inverse the value and image url for UI */
                    imgButton.AlternateText = Constants.FlagYes;
                    imgButton.ImageUrl = Constants.ActivateImage;
                    imgButton.Attributes.Add(Constants.title, Constants.Suspend);
                }
                /* Update Active Flag into User login Details */
                objUser.updateUserDetails(InstitutionID, imgButton.AlternateText);
                //}
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }


        /// <summary>
        ///code amended on 08/14/2015 to subscribe a customer if advisor is subscribed
        ///comment the current function declaration and uncomment the below line
        ///private Customers addAdvisor(Boolean delFlag, Boolean subscriptionFlag)
        /// </summary>
        /// <param name="delFlag"></param>
        /// <returns></returns>
        private AdvisorTO addAdvisor(Boolean delFlag)
        {
            try
            {
                /* Create object of Customer Class */
                Customers objCustomer = new Customers();
                objCustomer.Email = txtEmail.Text;
                /* Check if email address already exists or not */
                if (!objCustomer.isCustomerExist(objCustomer))
                {
                    /* initialize Instance of classes */
                    AdvisorTO objAdvisorTO = new AdvisorTO();
                    Advisor objAdvisor = new Advisor();
                    #region set Insert Parameters
                    /* Assign textboxes content into institutional variables */
                    objAdvisorTO.AdvisorID = Database.GenerateGID("A");
                    if (!String.IsNullOrEmpty(Request.QueryString[Constants.QueryStringInstitutionAdminID]))
                        objAdvisorTO.InstitutionAdminID = Request.QueryString[Constants.QueryStringInstitutionAdminID];
                    else
                        objAdvisorTO.InstitutionAdminID = Session[Constants.SessionAdminId].ToString();
                    objAdvisorTO.AdvisorName = textInfo.ToTitleCase(txtFirstname.Text);
                    objAdvisorTO.Role = Constants.Advisor;
                    objAdvisorTO.Firstname = textInfo.ToTitleCase(txtFirstname.Text);
                    objAdvisorTO.Lastname = txtLastname.Text;
                    objAdvisorTO.Email = txtEmail.Text;
                    objAdvisorTO.State = ddlState.Text;
                    objAdvisorTO.Dealer = txtDealer.Text;
                    objAdvisorTO.City = txtCity.Text;
                    objAdvisorTO.isSubscription = Constants.FlagYes;

                    string stPassword = objCustomer.GeneratePassword();
                    objAdvisorTO.Password = Encryption.Encrypt(stPassword);

                    #region Mail credentials to Institute
                    string strBody = "Hi " + txtFirstname.Text + ", <br /><br />       You have been successfully registered with the Paid to Wait Calculator.  <br />Your login is " + txtEmail.Text + "<br />Your password is " + stPassword + " <br /> Please <a href='http://calculatemybenefits.com/?ref=login'>Click here</a> to login. <br /><br /> Thanks & Regards,<br /> Brian Doherty";
                    MailHelper.sendMail(txtEmail.Text, "Social Security Calculator", strBody, string.Empty);
                    #endregion Mail credentials to Institute
                    //code amended on 08/14/2015 to subscribe a customer if advisor is subscribed
                    //comment the current line and uncomment the below code
                    //if (subscriptionFlag)
                    //    objAdvisorTO.isInstitutionSubscription = Constants.FlagYes;
                    //else
                    //    objAdvisorTO.isInstitutionSubscription = Constants.FlagNo;
                    objAdvisorTO.isInstitutionSubscription = Constants.FlagNo;
                    if (delFlag)
                        objAdvisorTO.DelFlag = Constants.FlagYes;
                    else
                        objAdvisorTO.DelFlag = Constants.FlagNo;
                    objAdvisorTO.PasswordResetFlag = Constants.FlagNo;
                    #endregion set Insert Parameters
                    txtEmail.Enabled = true;
                    /* Insert institution details into Database */
                    objAdvisor.insertAdvisorDetails(objAdvisorTO);
                    clsFlag.Flag = false;
                    /* Reload gridview with updated data */
                    BindData();
                    lblErrorMessage.Visible = true;
                    lblErrorMessage.Text = Constants.AdvisorAdded;
                    lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                    return objAdvisorTO;
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Constants.alertMessage, Constants.AdvisorEmialExists, true);
                    return null;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                return null;
            }
        }

        /// <summary>
        /// Used to redirect at Upload advisors page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GoToUploadAdvisor(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(Request.QueryString[Constants.QueryStringInstitutionAdminID]))
                    Response.Redirect(Constants.RedirectToAdvisorRecordsWithID + Request.QueryString[Constants.QueryStringInstitutionAdminID], true);
                else
                    Response.Redirect(Constants.RedirectToAdvisorRecordsWithID + Session[Constants.SessionAdminId].ToString(), true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorManageAdvisors, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorManageAdvisors + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }
        #endregion Methods
    }
}
