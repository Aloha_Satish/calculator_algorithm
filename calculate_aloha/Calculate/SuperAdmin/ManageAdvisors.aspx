﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageAdvisors.aspx.cs" Inherits="Calculate.SuperAdmin.ManageAdvisors" MasterPageFile="~/AdminMaster.master" Title="Manage Advisors" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <div class="AccountDetails HomeImgBg">
        <div style="">
            <center>
            
                <h1 style="color: #85312f;" class="headfontsize setTopMargin">Manage Advisors</h1>
            </center>
        </div>
        <div class="breadcrumbs" style="display: none">
            <a href="ManageInstitutions.aspx" id="manageInstitution" style="color: #85312f;" class="active breadfontad">Manage Institutions > </a>
            <a id="displayName" style="color: #757575;" class="breadfontad"></a>
        </div>
        <asp:Label ID="lblDisplayName" class="displayNone" runat="server"></asp:Label>
        <br />
        <br />
        <ajaxToolkit:ToolkitScriptManager ID="smCustomer" runat="server" ScriptMode="Release">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="upGrid" runat="server">
            <ContentTemplate>
                <div style="min-height: 385px;">
                    <div class="table-responsive">
                        <table class="filterGridView table managegrid">
                            <tbody>
                                <tr>
                                    <td class="searchText invisibletd verticalalign">
                                        <asp:DropDownList Height="27px" CssClass="form-control floatLeft maxWidthDropDownList minWidthDropdownSearch" ID="nameSelection" OnSelectedIndexChanged="nameSelection_TextChanged" AutoPostBack="true" DataTextField="Firstname" DataValueField="Firstname" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="searchText invisibletd verticalalign">
                                        <asp:TextBox CssClass="form-control textEntry handIcon floatLeft minWidthDropdownSearch" ID="dateFilter" runat="server" OnTextChanged="dateFilter_TextChanged" placeholder="Date" AutoPostBack="true"></asp:TextBox>
                                    </td>
                                    <td class="searchText verticalalign">
                                        <div class="input-append" style="min-width: 170px;">
                                            <asp:TextBox CssClass="form-control handIcon textEntry floatLeft" ID="txtSearch" AutoPostBack="true" runat="server" placeholder="Search" OnTextChanged="txtSearch_TextChanged"></asp:TextBox>
                                            <asp:ImageButton runat="server" ID="searchImage" ClientIDMode="Static" ImageUrl="~/Images/search_1.png" OnClick="txtSearch_TextChanged" CssClass="searchGridView floatLeft" />
                                        </div>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" Width="55px" OnClick="Reset" CssClass="managegridButton floatLeft" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <asp:GridView ID="gvDetails" DataKeyNames="AdvisorID" HeaderStyle-Height="45px" runat="server"
                        AutoGenerateColumns="false" CssClass="mGrid tablegrid  table-striped table-hover table-responsive" HeaderStyle-BackColor="#8e8d8a" AllowPaging="true"
                        OnPageIndexChanging="OnPaging" PageSize="10" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" PagerStyle-CssClass="pgr" PagerStyle-HorizontalAlign="Right" AllowSorting="true" OnSorting="gvDetails_Sorting"
                        AlternatingRowStyle-CssClass="alt" OnRowDataBound="gvDetails_RowDataBound" ShowHeaderWhenEmpty="true" EmptyDataText="No Advisor Found">
                        <Columns>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="InstitutionAdminID" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblAdvisorAdminID" runat="server" Text='<%#Eval("AdvisorID") %>' />
                                    <asp:Label ID="lblUserID" runat="server" Text='<%#Eval("AdvisorID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="First Name" SortExpression="Firstname">
                                <ItemTemplate>
                                    <asp:Label ID="lblFirstname" runat="server" Text='<%#Eval("Firstname") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="Last Name" SortExpression="Lastname">
                                <ItemTemplate>
                                    <asp:Label ID="lblLastname" runat="server" Text='<%#Eval("Lastname") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="Password" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblPassword" runat="server" Text='<%#Eval("Password") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="City" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblCity" runat="server" Text='<%#Eval("City") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="State" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblState" runat="server" Text='<%#Eval("State") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="State" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblDealer" runat="server" Text='<%#Eval("Dealer") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="Email" SortExpression="Email">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("Email") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-BorderStyle="None" ControlStyle-CssClass="ellipsis" HeaderText="Created On" SortExpression="UpdateDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("UpdateDate") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="90px" HeaderStyle-CssClass="textCenter" HeaderStyle-BorderStyle="None" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                <ItemTemplate>
                                    <%-- <asp:ImageButton ID="imgtemp" runat="server" Visible="false" />--%>
                                    <asp:ImageButton ID="imgStatus" runat="server" OnClick="Status" CommandName="Select" /><%--OnClientClick="return showSuspendConfirm(this);"--%>
                                    <asp:ImageButton ID="linkEdit" ToolTip="Edit" runat="server" ImageUrl="~/Images/edit_new.png" OnClick="editGridViewDetails" />
                                    <asp:ImageButton ID="linkDelete" ToolTip="Delete" runat="server" OnClientClick="return showDeleteConfirm('Advisor');" ImageUrl="~/Images/delete_new.png" OnClick="Delete" />
                                    <%--OnClientClick="return showDeleteConfirm('Advisor');"--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderStyle-CssClass="textCenter" HeaderText="Details" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button ID="lnkDetails" runat="server" Text="View Clients" OnClick="View" CssClass="gridButton" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <%-- <AlternatingRowStyle BackColor="silver" />--%>
                    </asp:GridView>
                    <asp:Label ID="NoData" runat="server" Text="No Client Found" Style="display: none; text-align: center; font-weight: bold; color: brown"></asp:Label>
                    <div class="managePageCount">
                        <span>Showing <%= gvDetails.Rows.Count %> of <%= lblPageTotalNumber.Text %> Records &nbsp;</span>
                    </div>
                    <br />
                    <asp:Label ID="lblPageTotalNumber" runat="server" class="displayNone"></asp:Label>
                    <center>
                    <asp:Button ID="btnAdd" runat="server" Text="Add New Advisor" OnClick="AddInstitution" CssClass="button_login" />
                        &nbsp; &nbsp &nbsp; &nbsp
                    <asp:Button ID="btnUpload" runat="server" Text="Upload Advisors" OnClick="GoToUploadAdvisor" CssClass="button_login" />
                </center>
                    <br />
                    <center>
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="failureErrorNotification" Font-Bold="true" Visible="false"></asp:Label>
                </center>
                    <asp:Panel ID="pnlAddEdit" ScrollBars="Auto" runat="server" DefaultButton="btnSave" CssClass="modalAdvisorPopup12 displayNone">
                        <div class="modal-dialog modal-md">
                            <div class="h2PopUp modal-header">
                                <asp:Label ForeColor="white" Font-Bold="false" CssClass="marglefthead" ID="lblHeader" runat="server" Text="Add Advisor"></asp:Label>
                            </div>
                            <br />
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <asp:Label ID="lblFirstname" ForeColor="#787878" runat="server" Text="First Name"></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control" ID="txtAdvisorID" runat="server" Visible="false"></asp:TextBox>
                                        <asp:TextBox CssClass="textEntry form-control" TabIndex="2" ID="passwordTextBox" Visible="false" MaxLength="20" runat="server"></asp:TextBox>
                                        <asp:TextBox CssClass="textEntry form-control" TabIndex="2" ID="txtFirstname" MaxLength="20" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqFirstname" runat="server" ControlToValidate="txtFirstname" CssClass="failureErrorNotification" ErrorMessage="Advisor First name is required." ToolTip="Advisor First name is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*First name is required</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regFirstname" runat="server" ControlToValidate="txtFirstname" CssClass="failureErrorNotification" ErrorMessage="Please enter valid Advisor First name" ValidationExpression="^[A-Za-z][A-Za-z0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid Advisor first name</asp:RegularExpressionValidator>
                                    </div>
                                    <div class="col-md-6 text-left">
                                        <asp:Label ID="lblLastname" ForeColor="#787878" runat="server" Text="Last Name"></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control" TabIndex="3" ID="txtLastname" MaxLength="20" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="regLastname" runat="server" ControlToValidate="txtLastname" CssClass="failureErrorNotification" ErrorMessage="Advisor last name is required." ToolTip="Advisor last name is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Last name is required</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="reqLastname" runat="server" ControlToValidate="txtLastname" CssClass="failureErrorNotification" ErrorMessage="Please enter valid Advisor last name" ValidationExpression="^[A-Za-z][A-Za-z0-9]*$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid Advisor last name</asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-6 text-left">
                                        <asp:Label ID="lblEmailad" ForeColor="#787878" runat="server" Text="Email"></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control" TabIndex="4" AutoComplete="off" MaxLength="50" ID="txtEmail" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                        <asp:HiddenField ID="HiddenFieldEmail" runat="server" />
                                        <asp:RequiredFieldValidator ID="reqEmail" runat="server" ControlToValidate="txtEmail" CssClass="failureErrorNotification" ErrorMessage="E-mail is required." ToolTip="E-mail is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*E-mail is required.</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="regEmail" runat="server" ControlToValidate="txtEmail" CssClass="failureErrorNotification" ErrorMessage="Please enter valid E-mail" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Please enter valid E-mail</asp:RegularExpressionValidator>
                                    </div>
                                    <div class="col-md-6 text-left">
                                        <asp:Label ID="lblStateAd" ForeColor="#787878" runat="server" Text="State"></asp:Label><br />
                                        <%--<asp:TextBox CssClass="textEntry form-control" TabIndex="5" AutoComplete="on"  ID="txtState" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlState" Width="125px" TabIndex="5" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqState" runat="server" ControlToValidate="ddlState" CssClass="failureErrorNotification" ErrorMessage="<br/>*Please select a State" ToolTip="*Please select a State" ValidationGroup="RegisterUserValidationGroup" Display="Dynamic"><br />*Please select a State</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <br />
                                 <div class="row">
                                    <div class="col-md-6 text-left">
                                        <asp:Label ID="lblBrokerDealer" ForeColor="#787878" runat="server" Text="Name of Broker Dealer"></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control" TabIndex="6" AutoComplete="off" MaxLength="50" ID="txtDealer" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                        <asp:RequiredFieldValidator ID="reqDealer" runat="server" ControlToValidate="txtDealer" CssClass="failureErrorNotification" ErrorMessage="Broker Dealer is required." ToolTip="Broker Dealer is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*Broker Dealer is required.</asp:RequiredFieldValidator>
                                    </div>

                                      <div class="col-md-6">
                                        <asp:Label ID="lblCity" ForeColor="#787878" runat="server" Text="City"></asp:Label><br />
                                        <asp:TextBox CssClass="textEntry form-control" TabIndex="6" AutoComplete="off" MaxLength="50" ID="txtCity" runat="server" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqCity" runat="server" ControlToValidate="txtCity" CssClass="failureErrorNotification" ErrorMessage="City is required." ToolTip="City is required." ValidationGroup="RegisterUserValidationGroup" Display="Dynamic">*City is required.</asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <br />
                                
                                <div class="row popupbottom">
                                    <div class="col-md-12 col-sm-12 ">
                                        <center>
                                    <asp:Button TabIndex="9" ID="btnSave" runat="server" Text="Save" OnClick="Save" CssClass="button_login marginpopbottom" ValidationGroup="RegisterUserValidationGroup" />&nbsp;&nbsp;
                                            <asp:Button TabIndex="10" ID="btnCancel" runat="server" OnClick="Cancel" Text="Cancel" CssClass="button_login marginpopbottom" />
                                        </center>
                                    </div>
                                </div>
                                <div class="row popupbottom">
                                    <div class="col-md-12">
                                        <div class="failureForgotNotification widthfull displayNone" id="errorMessage"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="subscription" style="left: 20px; position: relative; width: 450px; margin-bottom: 20px;">
                            <div id="plans">
                                <h2 class="center" style="color: white; font-weight: bold;">Before Create New Advisor, Need to make Payment</h2>
                                <div class="plan astronaut" style="opacity: 0.75; left: 115px;">
                                    <h3>Subscribe Advisor</h3>
                                    <div class="plan-content">
                                        <p>User Subscribe by Advisor</p>
                                        <ul>
                                            <li>User have to pay $60</li>
                                        </ul>
                                        <asp:Button ID="btnTrial" runat="server" Text="Subscribe" CssClass="button_login" OnClick="btnTrial_Click" />
                                    </div>
                                    <!-- plan-content -->
                                </div>
                                <!-- plan -->
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
                    <cc1:ModalPopupExtender ID="popup" runat="server" PopupControlID="pnlAddEdit" TargetControlID="lnkFake" BackgroundCssClass="modalBackground">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlDelete" runat="server" CssClass="suspendModalPopup displayNone" ScrollBars="Auto">
                        <div class="h2PopUp col-md-12 col-sm-12">
                            <asp:Label ForeColor="white" Font-Bold="false" CssClass="marglefthead" ID="lblDeleteAdvisor" runat="server" Text="Delete Advisor"></asp:Label>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <br />
                            <asp:Label ID="lblNote" runat="server" Text="Before deleting/suspending Advisor, Please assign existing clients to some other advisor." Font-Bold="true"></asp:Label>
                        </div>
                        <center>
                    <div class="col-md-12 col-sm-12">
                        <br />
                        <div class="col-md-6 col-sm-6" >
                            <div class="col-md-12 col-sm-12">
                                <asp:Label ID="lblSelectAdvisor" runat="server" Text="Select Advisor:"></asp:Label><br />
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <br />
                                <asp:DropDownList ID="ddlAdvisors" Width="125px" runat="server"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="reqddlAdvisors" runat="server" ControlToValidate="ddlAdvisors" CssClass="failureErrorNotification" ErrorMessage="<br/>*Please select an Advisor" ToolTip="*Please select an Advisor" ValidationGroup="rgv1" Display="Dynamic"><br />*Please select an Advisor</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6" >
                            <div class="col-md-12 col-sm-12" style="text-align:left">
                                <asp:Label ID="lblSelectCustomer" runat="server" Text="Select Client : "></asp:Label>
                            </div>
                            <br />
                                <br />
                            <div class="col-md-12 col-sm-12 InstitutionListAdjustment" style="text-align:left" >
                                    <asp:CheckBox ID="chkSelectAll" runat="server" ValidationGroup="rgv1" Text="Select All" CssClass="test" OnCheckedChanged="lstCustomers_TextChanged" OnClick="return selectAll();" /><br />
                                    <%-- onClintClick="ChkSelectAllFun(this)"--%>
                                 <asp:Label ID="lblAdvisorName" runat="server"></asp:Label>
                                    <asp:CheckBoxList ID="lstCustomers" runat="server" ValidationGroup="rgv1" CssClass="test">
                                    </asp:CheckBoxList>
                            </div>
                        </div>
                    </div></center>
                        <div class="col-md-12 col-sm-12">
                            <div class="col-md-4 col-sm-4">
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <asp:CustomValidator ID="cuslstCustomers" ValidationGroup="rgv1" ErrorMessage="*Please select at least one Client."
                                    ClientValidationFunction="ValidateCheckBoxList" runat="server" CssClass="failureErrorNotification" />
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 popupbottom">
                            <div class="col-md-12 col-sm-12">
                                <asp:Button TabIndex="7" ID="btnAssignDelete" ValidationGroup="rgv1" runat="server" Text="Assign & Delete" OnClick="AssignDelete" CssClass="button_login marginpopbottom" />&nbsp;&nbsp; <%--OnClientClick="return validateAssignAdvisorData();"--%>
                                <asp:Button TabIndex="8" ID="btnCancelDelete" runat="server" OnClick="CancelDelete" Text="Cancel" CssClass="button_login" />
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:LinkButton ID="lnkDelete" runat="server"></asp:LinkButton>
                    <cc1:ModalPopupExtender ID="popupDelete" runat="server" PopupControlID="pnlDelete" TargetControlID="lnkDelete" BackgroundCssClass="modalBackground">
                    </cc1:ModalPopupExtender>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="gvDetails" />
                <asp:AsyncPostBackTrigger ControlID="btnSave" />
                <asp:AsyncPostBackTrigger ControlID="btnCancel" />
                <%--<asp:AsyncPostBackTrigger ControlID="chkSelectAll" />--%>
            </Triggers>
        </asp:UpdatePanel>
        <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
    </div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../Scripts/common.js" type="text/javascript"></script>
    <script type="text/javascript">
        var userID = '<%= Session["UserID"].ToString() %> ';
        $(document).ready(function () {
            //window.scrollTo(0, 400);
            $('#<%=btnTrial.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
            $('[id$=gvDetails]').parent().addClass('table-responsive');
        });

        $(function () {
            debugger;
            $("[id*=chkSelectAll]").bind("click", function () {
                if ($(this).is(":checked")) {
                    $("[id*=lstCustomers] input").attr("checked", "checked");
                } else {
                    $("[id*=lstCustomers] input").removeAttr("checked");
                }
            });
            $("[id*=lstCustomers] input").bind("click", function () {
                if ($("[id*=lstCustomers] input:checked").length == $("[id*=lstCustomers] input").length) {
                    $("[id*=chkSelectAll]").attr("checked", "checked");
                } else {
                    $("[id*=chkSelectAll]").removeAttr("checked");
                }
            });
        });
        function selectAll() {
            debugger;
            $("[id*=chkSelectAll]").bind("click", function () {
                if ($(this).is(":checked")) {
                    $("[id*=lstCustomers] input").attr("checked", "checked");
                } else {
                    $("[id*=lstCustomers] input").removeAttr("checked");
                }
            });
            $("[id*=lstCustomers] input").bind("click", function () {
                if ($("[id*=lstCustomers] input:checked").length == $("[id*=lstCustomers] input").length) {
                    $("[id*=chkSelectAll]").attr("checked", "checked");
                } else {
                    $("[id*=chkSelectAll]").removeAttr("checked");
                }
            });
        };
        function ValidateCheckBoxList(sender, args) {
            var checkBoxList = document.getElementById("<%=lstCustomers.ClientID %>");
            var checkboxes = checkBoxList.getElementsByTagName("input");
            var isValid = false;
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    isValid = true;
                    break;
                }
            }
            args.IsValid = isValid;
        }

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
</asp:Content>
