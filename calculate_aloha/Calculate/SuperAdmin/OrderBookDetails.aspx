﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderBookDetails.aspx.cs" Inherits="Calculate.SuperAdmin.OrderBookDetails" MasterPageFile="~/AdminMaster.master" Title="Order Book Details" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
     $(function fade() {
            setTimeout(function () {
                $("[id$=lblErrorMessage]").fadeOut(3000);
            }, 5000);

        });
        $(document).ready(function () {
            window.scrollTo(0, 300);
        });

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
    <div class="AccountDetails">
        <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
        <center>            
            <h1 style="color:#85312f;text-align:left;" class="headfontsize">                
                Manage Ordered Books
            </h1>
        </center>
        <center>        
            <ajaxToolkit:ToolkitScriptManager ID="smCustomer" runat="server">
        </ajaxToolkit:ToolkitScriptManager>
        <asp:UpdatePanel ID="upGrid" runat="server">
            <ContentTemplate>
                <div class="table-responsive">
                <table class="filterGridView table" style="border:2px solid #dddddd;">
                    <tbody>
                    <tr>
                        <td class="searchText invisibletd" >
                            <asp:DropDownList Height="27px" CssClass="form-control floatLeft maxWidthDropDownList minWidthDropdownSearch" ID="nameSelection" OnSelectedIndexChanged="nameSelection_TextChanged" AutoPostBack="true" DataTextField="BookName" DataValueField="BookName" runat="server" >
                            </asp:DropDownList>
                        </td>
                        <td class="searchText invisibletd" >
                            <asp:TextBox CssClass="form-control textEntry handIcon floatLeft minWidthDropdownSearch" ID="dateFilter" runat="server" placeholder="Date"  OnTextChanged="dateFilter_TextChanged" AutoPostBack="true"></asp:TextBox>
                        </td>
                        <td class="searchText" style="vertical-align:middle;max-width:100px;">
                            <div class="input-append" style="min-width: 170px;">
                            <asp:TextBox CssClass="form-control handIcon textEntry floatLeft" ID="txtSearch" AutoPostBack="true" runat="server" placeholder="Search" OnTextChanged="txtSearch_TextChanged"></asp:TextBox>
                            <asp:ImageButton runat="server" ID="searchImage" ClientIDMode="Static" ImageUrl="~/Images/search_1.png" OnClick="txtSearch_TextChanged" CssClass="searchGridView floatLeft" />
                            </div>
                        </td>
                        <td>
                            <asp:Button ID="btnReset" runat="server" Text="Reset" Width="55px" OnClick="Reset" CssClass="managegridButton floatLeft" />
                        </td>
                    </tr>
                    </tbody>
                </table>
                    </div>
                <div class="table-responsive">
                <asp:GridView ID="gvDetails" DataKeyNames="StoreID" runat="server"
                    AutoGenerateColumns="false" CssClass="mGrid table table-striped table-hover table-responsive" HeaderStyle-BackColor="#8e8d8a" AllowPaging="true"
                    OnPageIndexChanging="OnPaging" PageSize="10" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" PagerStyle-CssClass="pgr" AllowSorting="true" OnSorting="gvDetails_Sorting"
                    AlternatingRowStyle-CssClass="alt" ShowHeaderWhenEmpty="true" PagerStyle-HorizontalAlign="Right" EmptyDataText ="No Record Found">
                    <Columns>
                        <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="Book Name" SortExpression="BookName">
                            <ItemTemplate>
                                <asp:Label ID="lblBookName" runat="server" Text='<%#Eval("BookName") %>' />
                                 <asp:Label ID="lblBookID" Visible="false" runat="server" Text='<%#Eval("StoreID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="Total" SortExpression="Amount">
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("Amount") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="Unit Price" SortExpression="Price">
                            <ItemTemplate>
                                <asp:Label ID="lblPrice" runat="server" Text='<%#Eval("Price") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>                       
                        <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="Quantity" SortExpression="Unit">
                            <ItemTemplate>
                                <asp:Label ID="lblUnit" runat="server" Text='<%#Eval("Unit") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="Purchase Date" SortExpression="PaymentMade">
                            <ItemTemplate>
                                <asp:Label ID="lblPaymentMade" runat="server" Text='<%#Eval("PaymentMade") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="Customer Name" SortExpression="CustomerName">
                            <ItemTemplate>
                                <asp:Label ID="lblCustomerName" runat="server" Text='<%#Eval("CustomerName") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="Customer Email" SortExpression="CustomerEmail">
                            <ItemTemplate>
                                <asp:Label ID="lblCustomerEmail" runat="server" Text='<%#Eval("CustomerEmail") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>   
                        <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderText="Customer Shipping Address" SortExpression="CustomerShippingAddress">
                            <ItemTemplate>
                                <asp:Label ID="lblCustomerShippingAddress" runat="server" Text='<%#Eval("CustomerShippingAddress") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>     
                        <asp:TemplateField HeaderStyle-BackColor="#8e8d8a" HeaderStyle-HorizontalAlign="Center" HeaderStyle-BorderStyle="None" HeaderStyle-CssClass="textCenter" HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                            <ItemTemplate>
                                <asp:ImageButton ID="linkDelete" ToolTip="Delete" runat="server" ImageUrl="~/Images/delete_new.png" OnClick="Delete" OnClientClick="return showDeleteConfirm('Ordered Book');" />
                            </ItemTemplate>
                        </asp:TemplateField> 
                    </Columns>
                   <%-- <AlternatingRowStyle BackColor="silver" />--%>
                </asp:GridView>
                    </div>
                <div class="managePageCount">
                    <span>Showing <%= gvDetails.Rows.Count %> of <%= lblPageTotalNumber.Text %> Records &nbsp;</span>
                </div>
               <asp:Label ID="lblErrorMessage" runat="server" CssClass="failureErrorNotification" Font-Bold="true" Visible="false"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
              <asp:Label ID="lblPageTotalNumber" runat="server" class="displayNone"></asp:Label>
        </center>
    </div>
</asp:Content>
