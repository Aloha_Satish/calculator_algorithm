﻿using CalculateDLL;
using CalculateDLL.TO;
using Symbolics;
using System;
using System.Data;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculate.SuperAdmin
{
    public partial class OrderBookDetails : System.Web.UI.Page
    {
        #region Events
        /// <summary>
        /// Function call when page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ValidateSession();
                if (!IsPostBack)
                {
                    lblErrorMessage.Visible = false;
                    this.BindData();
                    Page.MaintainScrollPositionOnPostBack = true;
                }
                /* Initialize ScriptManager for calling Javascript Function */
                ScriptManager sm = ScriptManager.GetCurrent(Page);
                if (sm.IsInAsyncPostBack)
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.ClientScriptDatePickerName, Constants.ClientScriptDatePicker, true);
                else
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.ClientScriptDatePickerName, Constants.ClientScriptLoadDatePicker, true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorOrderBookDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorOrderBookDetails + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        /// <summary>
        /// Filter Records based on Date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void dateFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorOrderBookDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorOrderBookDetails + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        /// <summary>
        /// Reset Search Filteration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Reset(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                txtSearch.Text = String.Empty;
                dateFilter.Text = String.Empty;
                nameSelection.SelectedIndex = 0;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorOrderBookDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorOrderBookDetails + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        /// <summary>
        /// Reset Search Filteration
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                txtSearch.Text = String.Empty;
                dateFilter.Text = String.Empty;
                nameSelection.SelectedIndex = 0;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorOrderBookDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorOrderBookDetails + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }



        /// <summary>
        /// Activate/Deactivate Institution
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Status(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                updateActiveFlag(sender, gvDetails);
                lblErrorMessage.Visible = true;
                lblErrorMessage.Text = Constants.AdvisorUpdated;
                lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorOrderBookDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorOrderBookDetails + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;

            }
        }

        /// <summary>
        ///  Filter gridview data based on search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void nameSelection_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorOrderBookDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorOrderBookDetails + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        /// <summary>
        ///  Filter gridview data based on search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                FilterGridViewData();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorOrderBookDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorOrderBookDetails + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        /// <summary>
        /// Function call in pagination
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                lblErrorMessage.Visible = false;
                this.BindData();
                gvDetails.PageIndex = e.NewPageIndex;
                /* Reload gridview with updated data */
                gvDetails.DataBind();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorOrderBookDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorOrderBookDetails + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }


        /// <summary>
        /// Delete selected gridview row 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete(object sender, EventArgs e)
        {
            try
            {
                /* Check if selected row contain imagebutton control  or not */
                using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                {
                    lblErrorMessage.Visible = false;
                    /* Get Advisor Id from gridview and assign to runtime created label */
                    Label StoreID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("lblBookID");
                    /* Initialize object */
                    StoreTransaction objStore = new StoreTransaction();
                    StoreTO objStoreTO = new StoreTO();
                    objStoreTO.StoreID = StoreID.Text;
                    objStore.deleteBookInfo(objStoreTO);
                    /* Reload gridview with updated data */
                    this.BindData();
                    lblErrorMessage.Visible = true;
                    lblErrorMessage.Text = Constants.BookDetailsDeleted;
                    lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorOrderBookDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorOrderBookDetails + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        /// <summary>
        /// Sorting Gridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvDetails_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                string sortExpression = e.SortExpression;
                lblErrorMessage.Visible = false;
                if (GridViewSortDirection == SortDirection.Ascending)
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, Constants.SortingDescending);
                }
                else
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, Constants.SortingAscending);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorOrderBookDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorOrderBookDetails + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        #endregion Events

        #region Methods
        /// <summary>
        /// Validate Session
        /// </summary>
        public void ValidateSession()
        {
            try
            {
                //if user session is expired redirstc him to login again
                Session[Constants.SessionRegistered] = null;
                if (Session[Constants.SessionUserId] == null)
                {
                    Response.Redirect(Constants.RedirectLogin, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorOrderBookDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorOrderBookDetails + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState[Constants.sortDirection] == null)
                    ViewState[Constants.sortDirection] = SortDirection.Ascending;

                return (SortDirection)ViewState[Constants.sortDirection];
            }
            set { ViewState[Constants.sortDirection] = value; }
        }

        /// <summary>
        /// Sorting Gridview
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="direction"></param>
        private void SortGridView(string sortExpression, string direction)
        {
            try
            {
                StoreTransaction objStore = new StoreTransaction();
                DataTable tableRecords;
                /* Fill gridview detail based on filteration */
                tableRecords = objStore.getStoreDetailsFilter(nameSelection.Text, String.IsNullOrEmpty(dateFilter.Text) ? String.Empty : string.Format(Constants.stringDateFormat, Convert.ToDateTime(dateFilter.Text)), txtSearch.Text);
                DataView dv = new DataView(tableRecords);
                dv.Sort = sortExpression + direction;
                gvDetails.DataSource = dv;
                gvDetails.DataBind();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorOrderBookDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorOrderBookDetails + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        /// <summary>
        /// Bind institutional details into gridview control
        /// </summary>
        private void BindData()
        {
            try
            {
                /* Create Object */
                DataTable tableRecords;
                StoreTransaction objStore = new StoreTransaction();
                /* Get all institutional Data from database and bind to gridview control */
                tableRecords = objStore.getStoreTransactionDetails();
                lblPageTotalNumber.Text = tableRecords.Rows.Count.ToString();
                /* Check if datatable contain data or not */
                if (tableRecords.Rows.Count > Constants.zero)
                {
                    /* Fill data into gridview */
                    gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                    /* Fill Advisor Name into drop down list */
                    nameSelection.Items.Clear();
                    // nameSelection.Items.Insert(Constants.zero, "Select BookName");
                    nameSelection.Items.Insert(Constants.zero, Constants.DropDownAdvisorDefault);
                    nameSelection.Items[Constants.zero].Value = String.Empty;
                    nameSelection.AppendDataBoundItems = true;
                    nameSelection.DataSource = tableRecords;
                    nameSelection.DataBind();
                }
                else
                {
                    /* Show blank table data with message */
                    //tableRecords.Rows.Add(tableRecords.NewRow());
                    //gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                    /* Get column count */
                    //int totalcolums = gvDetails.Rows[Constants.zero].Cells.Count;
                    //gvDetails.Rows[Constants.zero].Cells.Clear();
                    //gvDetails.Rows[Constants.zero].Cells.Add(new TableCell());
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].ColumnSpan = totalcolums;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].Text = "No Ordered Book Found";
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].HorizontalAlign = HorizontalAlign.Center;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].ForeColor = System.Drawing.Color.Red;
                    ///* Fill drop down list with default Data */
                    //nameSelection.Items.Clear();
                    //nameSelection.Items.Insert(Constants.zero, Constants.DropDownAdvisorDefault);
                    //nameSelection.Items[Constants.zero].Value = String.Empty;
                    //nameSelection.DataBind();
                }
                RemoveDuplicateItems(nameSelection);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorOrderBookDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorOrderBookDetails + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        /// <summary>
        /// Filter Grid View Data based on Selection Criteria
        /// </summary>
        protected void FilterGridViewData()
        {
            try
            {
                /* Create an instance for Institution Class */
                StoreTransaction objStore = new StoreTransaction();
                DataTable tableRecords;
                /* Fill gridview detail based on filteration */
                tableRecords = objStore.getStoreDetailsFilter(nameSelection.Text, String.IsNullOrEmpty(dateFilter.Text) ? String.Empty : string.Format(Constants.stringDateFormat, Convert.ToDateTime(dateFilter.Text)), txtSearch.Text);
                /* Check if datatable contain data or not */
                if (tableRecords.Rows.Count > Constants.zero)
                {
                    /* Fill data into gridview */
                    gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                }
                else
                {
                    /* Show blank table data with message */
                    //tableRecords.Rows.Add(tableRecords.NewRow());
                    //gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                    ///* Get column count */
                    //int totalcolums = gvDetails.Rows[Constants.zero].Cells.Count;
                    //gvDetails.Rows[Constants.zero].Cells.Clear();
                    //gvDetails.Rows[Constants.zero].Cells.Add(new TableCell());
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].ColumnSpan = totalcolums;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].Text = "No Ordered Book Found";
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].HorizontalAlign = HorizontalAlign.Center;
                    //gvDetails.Rows[Constants.zero].Cells[Constants.zero].ForeColor = System.Drawing.Color.Red;
                }
                txtSearch.Focus();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorOrderBookDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorOrderBookDetails + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        /// <summary>
        /// Update Active Flag 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="gvDetails"></param>
        public void updateActiveFlag(object sender, GridView gvDetails)
        {
            try
            {
                using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                {
                    /* Redirect to Advisor UI with institution Id */
                    Label InstitutionID = (Label)gvDetails.Rows[row.RowIndex].Cells[0].FindControl(Constants.lblUserID);
                    ImageButton imgButton = (ImageButton)gvDetails.Rows[row.RowIndex].Cells[0].FindControl(Constants.imgStatus);
                    Customers objCustomer = new Customers();
                    DataTable dtCustomer = objCustomer.getCustomerDetailsByAdvisor(InstitutionID.Text.ToString());
                    Users objUser = new Users();
                    /* Check if Active flag matched with value */
                    if (Convert.ToString(imgButton.AlternateText).Equals(Constants.FlagYes))
                    {
                        /* Inverse the value and image url for UI */
                        imgButton.AlternateText = Constants.FlagNo;
                        imgButton.ImageUrl = Constants.DeactivateImage;
                    }
                    else
                    {
                        /* Inverse the value and image url for UI */
                        imgButton.AlternateText = Constants.FlagYes;
                        imgButton.ImageUrl = Constants.ActivateImage;
                    }
                    /* Update Active Flag into User login Details */
                    objUser.updateUserDetails(InstitutionID.Text, imgButton.AlternateText);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorOrderBookDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblErrorMessage.Text = Constants.ErrorOrderBookDetails + MethodBase.GetCurrentMethod() + ex.Message;
                lblErrorMessage.Visible = true;
            }
        }

        public static void RemoveDuplicateItems(DropDownList ddl)
        {

            try
            {
                for (int i = 0; i < ddl.Items.Count; i++)
                {
                    ddl.SelectedIndex = i;
                    string str = ddl.SelectedItem.ToString();
                    for (int counter = i + 1; counter < ddl.Items.Count; counter++)
                    {
                        ddl.SelectedIndex = counter;
                        string compareStr = ddl.SelectedItem.ToString();
                        if (str == compareStr)
                        {
                            ddl.Items.RemoveAt(counter);
                            counter = counter - 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorOrderBookDetails, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }
        #endregion Methods

    }
}
