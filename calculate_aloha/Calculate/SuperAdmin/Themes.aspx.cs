﻿using CalculateDLL;
using CalculateDLL.TO;
using Symbolics;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculate.SuperAdmin
{
    public partial class Themes : System.Web.UI.Page
    {
        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //if (Session[Constants.SessionUserId] == null)
                //{
                //    Response.Redirect(Constants.RedirectRegister, true);
                //}
                /* Check if page is post back or not */
                if (!IsPostBack)
                {
                    BindDataGrid();
                    BindData();
                    lblErrorMessage.Visible = false;
                    pnlAddEdit.Visible = false;
                    Page.MaintainScrollPositionOnPostBack = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorThemes + "Page_Load()" + ex.ToString());
            }
        }

        /// <summary>
        /// Function call in pagination
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                this.BindData();
                gvDetails.PageIndex = e.NewPageIndex;
                /* Reload gridview with updated data */
                gvDetails.DataBind();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorThemes + "OnPaging()" + ex.ToString());
            }
        }

        /// <summary>
        /// We can edit Advisor details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void editGridViewDetails(object sender, EventArgs e)
        {
            try
            {
                /* Check if selected row contain imagebutton control  or not */
                using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                {
                    lblErrorMessage.Visible = false;
                    lblHeader.Text = Constants.LblEditHeaderTheme;
                    clsFlag.Flag = false;
                    pnlAddEdit.Visible = true;
                    #region Fill Advisor Details into Popup
                    /* Get content from gridview and assign to runtime created label */
                    Label ThemeId = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("lblThemeId");
                    Label BGBodyColor = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("lblBackGroundBodyColor");
                    Label BGFooterColor = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("lblBackGroundFooterColor");
                    Label BGHeaderColor = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("lblBackGroundHeaderColor");
                    Label InstitutionName = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("lblInstitution");
                    /* Assign label data into textbox which are shown up in blockui popup */
                    txtHeaderColor.Text = BGHeaderColor.Text;
                    txtBodyColor.Text = BGBodyColor.Text;
                    selectedThemeID.Text = ThemeId.Text;
                    txtFooterColor.Text = BGFooterColor.Text;
                    txtBodyColor.BackColor = ColorTranslator.FromHtml(BGBodyColor.Text);
                    txtFooterColor.BackColor = ColorTranslator.FromHtml(BGFooterColor.Text);
                    txtHeaderColor.BackColor = ColorTranslator.FromHtml(BGHeaderColor.Text);
                    BindData();
                    ddlnameSelection.SelectedValue = InstitutionName.Text;
                    ddlnameSelection.Enabled = false;
                    Theme objTheme = new Theme();
                    byte[] bytes = objTheme.FetchImage(selectedThemeID.Text);
                    string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    img.ImageUrl = "data:image/png;base64," + base64String;
                    img.Attributes.Add("class", "imgResize");
                    img.Style.Add("display", "block");
                    popup.Show();
                    txtBodyColor.BackColor = ColorTranslator.FromHtml(BGBodyColor.Text);
                    txtFooterColor.BackColor = ColorTranslator.FromHtml(BGFooterColor.Text);
                    txtHeaderColor.BackColor = ColorTranslator.FromHtml(BGHeaderColor.Text);

                    string js = "changeEditThemeBackground('" + BGHeaderColor.Text + "','" + BGBodyColor.Text + "','" + BGFooterColor.Text + "');";
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, js, true);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorThemes + "editGridViewDetails()" + ex.ToString());
            }
        }

        /// <summary>
        /// Add new Theme details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddNewTheme(object sender, EventArgs e)
        {
            try
            {
                clsFlag.Flag = true;
                pnlAddEdit.Visible = true;
                lblErrorMessage.Visible = false;
                ScriptManager.RegisterStartupScript(this, typeof(string), "ShowOtherparams", "displayTableHeader(false);", true);
                lblHeader.Text = Constants.LblAddHeaderTheme;
                #region Clear all fields
                txtHeaderColor.Text = String.Empty;
                txtBodyColor.Text = String.Empty;
                txtFooterColor.Text = String.Empty;
                this.BindData();
                ddlnameSelection.Enabled = true;
                img.ImageUrl = "";
                img.Attributes.Remove("class");
                img.Style.Add("display", "none");
                #endregion
                /* Show BlockUI PopUp */
                popup.Show();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorThemes + "AddNewTheme()" + ex.ToString());
            }
        }

        protected void Cancel(object sender, EventArgs e)
        {

            try
            {
                txtHeaderColor.Text = String.Empty;
                txtBodyColor.Text = String.Empty;
                txtFooterColor.Text = String.Empty;
                ddlnameSelection.Enabled = false;
                img.ImageUrl = "";
                img.Attributes.Remove("class");
                img.Style.Add("display", "none");
                pnlAddEdit.Visible = false;
                lblErrorMessage.Visible = false;
                BindDataGrid();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorThemes + "Cancel()" + ex.ToString());
            }
        }

        /// <summary>
        /// Save institutional details into database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save(object sender, EventArgs e)
        {
            try
            {
                Boolean EditFlag = false;
                /* Initialize NewsLetter Object */
                Theme objTheme = new Theme();
                ThemesTO objThemesTO = new ThemesTO();
                /* Assign data into newsletter object variables */
                if (string.IsNullOrWhiteSpace(txtBodyColor.Text))
                    txtBodyColor.Text = "#FFFFFF";
                if (txtBodyColor.Text.Substring(0, 1) == "#")
                {
                    objThemesTO.BackGroundBodyColor = txtBodyColor.Text;
                }
                else
                {
                    objThemesTO.BackGroundBodyColor = "#" + txtBodyColor.Text;
                }
                if (string.IsNullOrWhiteSpace(txtFooterColor.Text))
                    txtFooterColor.Text = "#000000";
                if (txtFooterColor.Text.Substring(0, 1) == "#")
                {
                    objThemesTO.BackGroundFooterColor = txtFooterColor.Text;
                }
                else
                {
                    objThemesTO.BackGroundFooterColor = "#" + txtFooterColor.Text;
                }
                if (string.IsNullOrWhiteSpace(txtHeaderColor.Text))
                    txtHeaderColor.Text = "#FFFFFF";
                if (txtHeaderColor.Text.Substring(0, 1) == "#")
                {
                    objThemesTO.BackGroundHeaderColor = txtHeaderColor.Text;
                }
                else
                {
                    objThemesTO.BackGroundHeaderColor = "#" + txtHeaderColor.Text;
                }
                objThemesTO.InstitutionName = ddlnameSelection.SelectedItem.Value;
                /* check if popup open for add or edit mode */
                if (txtAttachmentLogo.HasFile)
                {
                     EditFlag = true;
                    byte[] fileData = null;
                    using (var binaryReader = new BinaryReader(txtAttachmentLogo.PostedFile.InputStream))
                    {
                        fileData = binaryReader.ReadBytes(txtAttachmentLogo.PostedFile.ContentLength);
                    }
                    objThemesTO.InstitutionLogo = fileData;
                }
                else
                    objThemesTO.InstitutionLogo = null;
                //Uri myUri = new Uri(txtDomainName.Text);
                //string host = myUri.Host;
                //objThemesTO.DomainName = host;
                //objThemesTO.URLString = host;
                //objTheme.insertBannerThemeDetails(objThemesTO);
                DataTable dtInstiId = objTheme.getInstitutionIdByName(ddlnameSelection.SelectedItem.Value); ;
                objThemesTO.InstitutionId = dtInstiId.Rows[0]["InstitutionAdminID"].ToString();
                if (clsFlag.Flag)
                {
                    if (objTheme.isThemeExist(ddlnameSelection.SelectedItem.Value))
                    {
                        popup.Show();
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, "displayPanelPopUpTheme();", true);
                    }
                    else
                    {
                        objTheme.insertThemeDetails(objThemesTO);
                        /* Reload gridview with updated data */
                        BindDataGrid();
                        lblErrorMessage.Visible = true;
                        lblErrorMessage.Text = "Theme added successfully";
                        lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                        pnlAddEdit.Visible = false;
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                    }
                }
                else
                {
                    objThemesTO.ThemeId = selectedThemeID.Text;
                    if (EditFlag)
                        objTheme.updateThemeInfo(objThemesTO);
                    else
                        objTheme.updateThemeInfoWithoutLogo(objThemesTO);
                    /* Reload gridview with updated data */
                    BindDataGrid();
                    lblErrorMessage.Visible = true;
                    lblErrorMessage.Text = "Theme updated successfully";
                    lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                    pnlAddEdit.Visible = false;
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                }
                //Boolean statusMail = MailHelper.sendMail("swapnild@alohatechnology.com", txtSubject.Text, txtBody.InnerText);                


            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorThemes + "Save() - " + ex.ToString());
            }
        }

        /// <summary>
        /// Delete selected gridview row 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete(object sender, EventArgs e)
        {
            try
            {
                /* Check if selected row contain imagebutton control  or not */
                using (GridViewRow row = (GridViewRow)((ImageButton)sender).Parent.Parent)
                {
                    /* Get Advisor Id from gridview and assign to runtime created label */
                    Label ThemeID = (Label)gvDetails.Rows[row.RowIndex].Cells[Constants.zero].FindControl("lblThemeId");
                    /* Initialize object */
                    Theme objTheme = new Theme();
                    /* Call function to delete selected row by passing id into function */
                    objTheme.deleteThemeInfo(ThemeID.Text);
                    /* Reload gridview with updated data */
                    this.BindDataGrid();
                    lblErrorMessage.Visible = true;
                    lblErrorMessage.Text = "Theme deleted successfully";
                    lblErrorMessage.ForeColor = System.Drawing.Color.Green;
                    pnlAddEdit.Visible = false;
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, Constants.ClientScriptSuccessMessage, true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorThemes + "Delete() - " + ex.ToString());
            }
        }

        #endregion Events

        #region Methods
        private void BindDataGrid()
        {
            try
            {
                /* Create Object */
                DataTable tableRecords;
                Theme objTheme = new Theme();
                /* Get all institutional Data from database and bind to gridview control */
                tableRecords = objTheme.getThemeDetails();
                /* Check if datatable contain data or not */
                if (tableRecords.Rows.Count > Constants.zero)
                {
                    /* Fill data into gridview */
                    gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                }
                else
                {
                    /* Show blank table data with message */
                    tableRecords.Rows.Add(tableRecords.NewRow());
                    gvDetails.DataSource = tableRecords;
                    gvDetails.DataBind();
                    /* Get column count */
                    int totalcolums = gvDetails.Rows[Constants.zero].Cells.Count;
                    gvDetails.Rows[Constants.zero].Cells.Clear();
                    gvDetails.Rows[Constants.zero].Cells.Add(new TableCell());
                    gvDetails.Rows[Constants.zero].Cells[Constants.zero].ColumnSpan = totalcolums;
                    gvDetails.Rows[Constants.zero].Cells[Constants.zero].Text = Constants.GridViewDefaultTheme;
                    gvDetails.Rows[Constants.zero].Cells[Constants.zero].HorizontalAlign = HorizontalAlign.Center;
                    gvDetails.Rows[Constants.zero].Cells[Constants.zero].ForeColor = System.Drawing.Color.Red;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorThemes + "BindDataGrid() - " + ex.ToString());
            }
        }

        /// <summary>
        /// Bind institutional details into gridview control
        /// </summary>
        private void BindData()
        {
            try
            {
                Institutional objInstitutional = new Institutional();
                /* Get all institutional Data from database and bind to gridview control */
                DataTable tableRecords = objInstitutional.getInstitutionalDetails();
                /* Check if datatable contain data or not */
                if (tableRecords.Rows.Count > Constants.zero)
                {
                    /* Fill Institution Name into drop down list */
                    ddlnameSelection.Items.Clear();
                    ddlnameSelection.Items.Insert(Constants.zero, Constants.DropDownInstitutionalDefault);
                    ddlnameSelection.Items[Constants.zero].Value = String.Empty;
                    ddlnameSelection.AppendDataBoundItems = true;
                    ddlnameSelection.DataSource = tableRecords;
                    ddlnameSelection.DataBind();
                }
                else
                {
                    /* Fill drop down list with default Data */
                    ddlnameSelection.Items.Clear();
                    ddlnameSelection.Items.Insert(Constants.zero, Constants.DropDownInstitutionalDefault);
                    ddlnameSelection.Items[Constants.zero].Value = String.Empty;
                    ddlnameSelection.DataBind();
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorThemes + "BindData() - " + ex.ToString());
            }
        }

        #endregion Methods
    }
}