﻿<%@ Page Title="Welcome" Language="C#" MasterPageFile="~/ClientMaster.master" AutoEventWireup="true" CodeBehind="Welcome.aspx.cs" Inherits="Calculate.Welcome" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="BodyContent">
    <script type="text/javascript" src="../Scripts/blockUI.min.js"></script>
    <script type="text/javascript">
        var userID = '<%= Session["UserID"].ToString() %> ';
        $(document).ready(function () {
            $('#<%=btnTrial.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
            $('#<%=btnWithoutTrial.ClientID%>').click(function () {
                $('#subscription').parent().appendTo($("form:first"));
                return true;
            });
            $('.blockPage').removeAttr('style');
            $('.blockPage').addClass('subpop');
        });

        //Google Analytics
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-88761127-1', 'auto');
        ga('send', 'pageview');
    </script>
    <center>
        <h1 class="headfontsize"> Welcome <asp:Label ID="lblWelcome"  runat="server"></asp:Label> !</h1>       
        <br />
        <div class="fontLarge text-center">
            <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
     Are you ready to find out how much you can get paid to wait?<br />
Before you begin, you will need to find out your projected monthly Social Security benefit at full retirement.<br />
 You may have received this information in a letter from the Social Security Administration (SSA) or you can obtain it by <a href="https://secure.ssa.gov/RIL/SiView.do"><u>visiting the SSA web site</u></a>.
Once you have this information, please click the button below to continue.
        </div>
        <br />
        <br />
        <asp:Button ID="btnSaveAccountDetails" runat="server" Text="My Social Security Analysis" CssClass="button_login btn" OnClick="redirectToSocialSecurity" TabIndex="0" />
        <br />
        <div class="text-center fontLarge">
            <span id="status" class="statusContent displayNone">Customer Registered Successfully.
            </span>
        </div>
    </center>

    <div id="subscription" class="modalPopup popupheight">
        <div class="modal-dialog modal-md" >
            <div id="plans">
                <h2 class="center" style="font-weight: bold;">Select your account which fits your need?</h2>
                <div class="modal-body">
                    <div class="row padbot">
                        <div class="col-md-6">
                            <div style="opacity: 0.65;">
                                <h3 class="head3">Trial Period</h3>
                                <div class="plan-content">
                                    <p>User Subscribe With Trial Period</p>
                                    <ul>
                                        <li>User have to pay $20</li>
                                        <li>1 day Trial Period</li>
                                    </ul>
                                    <asp:Button ID="btnTrial" runat="server" Text="Subscribe" CssClass="button_login" OnClick="btnTrial_Click" />
                                </div>
                                <!-- plan-content -->
                            </div>
                            <!-- plan -->
                        </div>
                        <div class="col-md-6">
                            <div style="opacity: 1;">
                                <h3 class="head3">Without Trial Period</h3>
                                <div class="plan-content">
                                    <p>User Subscribe Without Trial Period</p>
                                    <ul>
                                        <li>User have to pay $90</li>
                                        <li>Whole month</li>
                                    </ul>
                                    <asp:Button ID="btnWithoutTrial" runat="server" Text="Subscribe" CssClass="button_login" OnClick="btnWithoutTrial_Click" />
                                </div>
                                <!-- plan-content -->
                            </div>
                            <!-- plan -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
