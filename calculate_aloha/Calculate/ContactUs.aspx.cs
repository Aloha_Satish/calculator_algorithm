﻿using System;
using Symbolics;
using CalculateDLL;
using System.Reflection;

namespace Calculate
{
    public partial class ContactUs : System.Web.UI.Page
    {
        #region Events
        /// <summary>
        /// code called when page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Session[Constants.SessionRegistered] = null;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRegister, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }
        #endregion Events

    }
}