﻿using CalculateDLL;
using Symbolics;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Drawing;
using Calculate.Objects;
using Calculate.RestrictAppIncome;
namespace Calculate
{
    public partial class CustomerSuccess : System.Web.UI.Page
    {
        #region Variables
        string strRoleChar = string.Empty, strReference = string.Empty;
        Users objUsers = new Users();
        #endregion Variables

        #region Events
        /// <summary>
        /// CustomerSuccess Form's Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    InsfisuionSoftProcesses objUserAuth = new InsfisuionSoftProcesses();

                    string strUserID = string.Empty, strEmail = string.Empty;
                    int intInfusionID = 0;
                    //InfusionSoft
                    //Check whether User is subscribed or not
                    if (Session["UserEmail"] != null)
                    {
                        strEmail = Session["UserEmail"].ToString();

                        intInfusionID = objUserAuth.GetInfusionIdFromEmail(strEmail);
                        if (intInfusionID > -1)
                        {
                            DataTable dtUserDetails = objUsers.getUserDetails(strEmail);
                            strReference = dtUserDetails.Rows[0]["UserId"].ToString();
                            if (!string.IsNullOrEmpty(strReference))
                                strRoleChar = strReference.Substring(0, 1);
                            Session[Constants.UserId] = strReference;
                            Session[Constants.SessionUserId] = strReference;
                            //if (strRoleChar.Equals(Constants.C))
                            //{
                            //    //clickButton.Text = "Click here to access your report";
                            //}
                            //else if (strRoleChar.Equals(Constants.A))
                            //{
                            //    //clickButton.Text = "Click here to access your account";
                            //}

                            //Send credentials to email
                            SendMailWithCredentialsAndUpdateTable(strReference);
                            //Should redirect to Advisor account or Single Use account
                            RedirectToAccount();

                        }
                        else
                        {
                            ErrorFunction("Something went wrong, we will get back to you !");
                        }
                    }
                    else
                    {
                        //clickButton.Text = "Click here to login";
                        RedirectToAccount();//Should redirect to login page
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCustomerSuccess, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorFunction("Something went wrong, we will get back to you !");
            }
        }

        /// <summary>
        /// Used to send mail and update the account
        /// </summary>
        /// <param name="strUserID">User Id</param>
        private void SendMailWithCredentialsAndUpdateTable(string strUserID)
        {
            try
            {
                string strEmail = string.Empty, strPassword = string.Empty;
                //Send account details mail to user
                objUsers.GetUserCredentials(strUserID, ref strEmail, ref strPassword);
                string strEmailBody = Constants.AccountCreatedSuccessfully;
                strEmailBody += Constants.AccountDetailFollows;
                strEmailBody += Constants.UserNmEmail + strEmail + Constants.HtmlNewLine;
                strEmailBody += Constants.Pasword_ + strPassword;
                string strEmailSubject = Constants.AccountDetails;
                MailHelper.sendMail(strEmail, strEmailSubject, strEmailBody, string.Empty);
                //Uodate customer subscription details
                UpdateCustomerInformation(strUserID);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCustomerSuccess, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorFunction("Something went wrong, we will get back to you !");
            }
        }


        /// <summary>
        /// Used to update the user as subscribed user
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        //protected void OnClickHere(object Sender, EventArgs e)
        protected void RedirectToAccount()
        {
            try
            {
                //strReference = Request.QueryString[Constants.reference].ToString();
                Users objUsers = new Users();
                string strEmail = string.Empty;
                if (Session["UserEmail"] != null)
                {
                    strEmail = Session["UserEmail"].ToString();
                    DataTable dtUserDetails = objUsers.getUserDetails(strEmail);
                    strReference = dtUserDetails.Rows[0]["UserId"].ToString();

                    if (!string.IsNullOrEmpty(strReference))
                        strRoleChar = strReference.Substring(0, 1);
                    Customers customer = new Customers();
                    if (strRoleChar.Equals(Constants.C))
                    {
                        DataTable dtDetails = customer.getCustomerDetails(strReference);
                        string strMaritalStatus = dtDetails.Rows[0][Constants.CustomerMaritalStatus].ToString();
                        if (strMaritalStatus.Equals(Constants.Married))
                        {
                            Session["CustAfterTrans"] = Constants.Married;
                            //Response.Redirect(Constants.RedirectCalcMarried);

                            //Added on 5 may 2017 for restricted Application income
                            RedirectionOfMarriedStretegy(dtDetails);

                        }
                        else if (strMaritalStatus.Equals(Constants.Divorced))
                        {
                            Session["CustAfterTrans"] = Constants.Divorced;
                            Response.Redirect(Constants.RedirectCalcDivorced);
                        }
                        else if (strMaritalStatus.Equals(Constants.Single))
                        {
                            Session["CustAfterTrans"] = Constants.Single;
                            Response.Redirect(Constants.RedirectCalcSingle);
                        }
                        else
                        {
                            Session["CustAfterTrans"] = Constants.Widowed;
                            Response.Redirect(Constants.RedirectCalcWidowed);
                        }
                    }
                    else if (strRoleChar.Equals(Constants.A))
                    {
                        Response.Redirect(Constants.RedirectManageCustomers);
                    }
                }
                else
                {
                    //if Session expires then redirect to login page
                    Response.Redirect(Constants.RedirectToLogin);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCustomerSuccess, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorFunction("Something went wrong, we will get back to you !");
            }
        }
        #endregion Events

        #region Methods

        /// <summary>
        /// Decides Redirection of Married Strategy
        /// </summary>
        /// <param name="dtDetails"></param>
        private void RedirectionOfMarriedStretegy(DataTable dtDetails)
        {
            try
            {
                #region Restricted App Change
                DateTime UserDob = Convert.ToDateTime(dtDetails.Rows[0]["Birthdate"].ToString());
                DateTime SpouseDob = Convert.ToDateTime(dtDetails.Rows[0]["SpouseBirthdate"].ToString());
                DateTime dtLimit = new DateTime(1954, 01, 02);
                string strClaimedPerson = dtDetails.Rows[0]["ClaimedPerson"].ToString();
                string ClaimedBenefit = string.Empty, ClaimedAge = string.Empty, CurrentBenefit = string.Empty;
                if (!strClaimedPerson.Equals("NONE"))
                {
                    ClaimedBenefit = dtDetails.Rows[0]["ClaimedBenefit"].ToString();
                    ClaimedAge = dtDetails.Rows[0]["ClaimedAge"].ToString();
                    if (strClaimedPerson.Equals("HUSBAND"))
                    {
                        Globals.Instance.BoolIsHusbandClaimedBenefit = true;
                        CurrentBenefit = dtDetails.Rows[0]["HusbandsRetAgeBenefit"].ToString();
                    }
                    else if (strClaimedPerson.Equals("WIFE"))
                    {
                        Globals.Instance.BoolIsWifeClaimedBenefit = true;
                        CurrentBenefit = dtDetails.Rows[0]["WifesRetAgeBenefit"].ToString();
                    }
                }
                else
                {
                    Response.Redirect(Constants.RedirectCalcMarried);
                }
                if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                {
                    if (!string.IsNullOrEmpty(CurrentBenefit))
                        RestrictAppIncomeProp.Instance.intHusbCurrentBenefit = Convert.ToInt32(CurrentBenefit);
                    RestrictAppIncomeProp.Instance.intHusbandClaimAge = Convert.ToInt32(ClaimedAge);
                    if (!string.IsNullOrEmpty(ClaimedBenefit))
                        RestrictAppIncomeProp.Instance.intHusbClaimBenefit = Convert.ToInt32(ClaimedBenefit);
                }
                if (Globals.Instance.BoolIsWifeClaimedBenefit)
                {
                    if (!string.IsNullOrEmpty(CurrentBenefit))
                        RestrictAppIncomeProp.Instance.intWifeCurrentBenefit = Convert.ToInt32(CurrentBenefit);
                    RestrictAppIncomeProp.Instance.intWifeClaimAge = Convert.ToInt32(ClaimedAge);
                    if (!string.IsNullOrEmpty(ClaimedBenefit))
                        RestrictAppIncomeProp.Instance.intWifeClaimBenefit = Convert.ToInt32(ClaimedBenefit);
                }
                //Determine show/hide full strategy
                if (Globals.Instance.BoolIsHusbandClaimedBenefit || Globals.Instance.BoolIsWifeClaimedBenefit)
                {
                    if (!Globals.Instance.BoolIsHusbandClaimedBenefit)
                    {
                        if (UserDob < dtLimit)
                        {
                            Globals.Instance.BoolIsShowRestrictFull = true;
                            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                        }
                        else
                            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                    }
                    else if (!Globals.Instance.BoolIsWifeClaimedBenefit)
                    {
                        if (SpouseDob < dtLimit)
                        {
                            Globals.Instance.BoolIsShowRestrictFull = true;
                            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                        }
                        else
                            Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);
                    }
                    else
                        Response.Redirect(Constants.RedirectToRestrictMarriedStrategy, false);

                }


                #endregion Restricted App Change
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCustomerSuccess, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }




        /// <summary>
        /// Used to set transaction failure message
        /// </summary>
        private void ErrorFunction(string strMessage)
        {
            try
            {
                lblSuccessMsg.Text = strMessage;
                lblSuccessMsg.ForeColor = Color.Red;
                //clickButton.Visible = false;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCustomerSuccess, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }

        // <summary>
        // Used to update Customer Information after payment completion
        // </summary>
        // <param name="strCustReference">Customer Reference</param>
        protected void UpdateCustomerInformation(string strCustReference)
        {
            try
            {
                Customers cs = new Customers();
                cs.UpdateIsSubscription(strCustReference);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCustomerSuccess, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorFunction("Something went wrong, we will get back to you !");
            }
        }

        #endregion Methods
    }
}