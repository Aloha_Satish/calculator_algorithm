﻿using Calculate.Objects;
using CalculateDLL;
using Symbolics;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculate.RestrictAppIncome
{
    public partial class RestrictMarriedStrategy : System.Web.UI.Page
    {
        #region Variable Declaration
        // Input items
        int intAgeWife, intAgeHusb, intAgeWifeMonth, intAgeHusbMonth, intAgeWifeDays, intAgeHusbDays, intCurrentAgeWifeMonth, intCurrentAgeHusbMonth, intCurrentAgeWifeYear, intCurrentAgeHusbYear;
        decimal decBeneWife, decBeneHusb;
        decimal colaAmt = 1;
        int birthYearWife, birthYearHusb;
        decimal halfBenHusb;
        decimal halfbenWife;
        DataTable dtDetails;
        String customerFirstName, strUserFirstName, strUserSpouseFirstName;
        String customerSposeFirstName;
        #endregion

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Session["ShowButton"] = "false";
                if (!IsPostBack)
                {
                    #region Old Code
                    //Customers customer = new Customers();
                    //DateTime wifeBirthDate, husbandBirthDate;
                    //DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                    //wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                    //husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                    //BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                    //BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                    //birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                    //BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                    //BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                    //BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                    //BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                    //BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                    //PanelParms.Visible = false;
                    //BeneWife.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                    //BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                    //ValidateAndCalc();
                    #endregion OldCode

                    try
                    {
                        if (Session["Demo"] != null)
                        {
                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                            DateTime wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            ValidateAndCalc();

                            //Send mail to the demo user before loading the strategy page
                            //Globals.Instance.BoolEmailReport = true;
                            //exportAllToPdf();
                            //Globals.Instance.BoolEmailReport = false;

                        }
                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                        {
                            ///* Disabled textbox and radio button so that institutional user can only view customer details */
                            ////BtnChange.Text = Constants.BtnChangeText;
                            //BirthWifeYYYY.Enabled = false;
                            //BeneWife.Enabled = false;
                            //BirthHusbandYYYY.Enabled = false;
                            //BeneHusband.Enabled = false;
                            //CheckBoxSurv.Enabled = false;
                            //Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.ClientScriptDatePicker, "disableDatePicker();", true);
                            //// Initialize page variables
                            //ErrorMessage.Text = String.Empty;
                            //PanelGrids.Visible = false;
                            //PanelParms.Visible = false;
                            ////CheckBoxCola.Checked = true;
                            //BirthWifeMM.Focus();
                            //ShowAllInformation();

                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                            DateTime wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            //PanelEntry.Visible = false;
                            ValidateAndCalc();
                            //if (Globals.Instance.BoolEmailReport || Globals.Instance.BoolAdvisorDownloadReport)
                            //{
                            //    Globals.Instance.BoolAdvisorDownloadReport = false;

                            //    exportAllToPdf();
                            //    Response.Redirect(Constants.RedirectManageCustomers);
                            //}

                        }
                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                        {
                            ///* Disabled textbox and radio button so that institutional user can only view customer details */
                            ////BtnChange.Text = Constants.BtnChangeText;
                            //BirthWifeYYYY.Enabled = false;
                            //BeneWife.Enabled = false;
                            //BirthHusbandYYYY.Enabled = false;
                            //BeneHusband.Enabled = false;
                            //CheckBoxSurv.Enabled = false;
                            //Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.ClientScriptDatePicker, "disableDatePicker();", true);
                            //// Initialize page variables
                            //ErrorMessage.Text = String.Empty;
                            //PanelGrids.Visible = false;
                            //PanelParms.Visible = false;
                            ////CheckBoxCola.Checked = true;
                            //BirthWifeMM.Focus();
                            //ShowAllInformation();

                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                            DateTime wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            //PanelEntry.Visible = false;
                            ValidateAndCalc();

                            //if (Globals.Instance.BoolEmailReport || Globals.Instance.BoolAdvisorDownloadReport)
                            //{
                            //    Globals.Instance.BoolAdvisorDownloadReport = false;

                            //    exportAllToPdf();
                            //    Response.Redirect(Constants.RedirectManageCustomers);
                            //}

                        }
                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor))
                        {
                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                            DateTime wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            ValidateAndCalc();

                            //if (Globals.Instance.BoolEmailReport || Globals.Instance.BoolAdvisorDownloadReport)
                            //{
                            //    Globals.Instance.BoolAdvisorDownloadReport = false;

                            //    exportAllToPdf();
                            //    Response.Redirect(Constants.RedirectManageCustomers);
                            //}

                        }
                        else if (Session[Constants.SessionRole].ToString().Equals(Constants.Customer))
                        {
                            Customers customer = new Customers();
                            DateTime dtLimit = new DateTime(1954, 01, 02);
                            DateTime wifeBirthDate, husbandBirthDate;
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());

                            wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            PanelGrids.Visible = false;//Added for new development (on 16/09)
                                                       // PanelEntry.Visible = false;//Added for new development (on 16/09)
                            ValidateAndCalc();
                            //if (wifeBirthDate < dtLimit || husbandBirthDate < dtLimit)
                            //    btnHideShowStrat.Visible = true;

                            //if ((Convert.ToDecimal(BeneHusband.Text) < Convert.ToDecimal(BeneWife.Text) && husbandBirthDate < dtLimit && wifeBirthDate >= dtLimit) ||
                            //  (Convert.ToDecimal(BeneWife.Text) < Convert.ToDecimal(BeneHusband.Text) && wifeBirthDate < dtLimit && husbandBirthDate >= dtLimit))
                            //{
                            //    //Visible button
                            //    btnHideShowStrat.Visible = true;
                            //}

                            if (Convert.ToString(dtCustomerDetails.Rows[Constants.zero][Constants.IsSubscription]).Equals(Constants.FlagNo))
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "displaySubscriptionPopUp(false);", true);
                                ValidateAndCalc();
                                //to show the Paid to wait value on the popup for new user
                                if (Session[Constants.MaxCumm] != null)
                                {
                                    decimal amt = decimal.Parse(Session[Constants.MaxCumm].ToString());
                                    MaxCumm.Text = Constants.SubscriptionPopupText + Environment.NewLine + amt.ToString(Constants.AMT_FORMAT);
                                }
                            }
                            //Commented for new development (on 16/09)
                            //else
                            //    ValidateAndCalc();

                        }
                        else if (!string.IsNullOrEmpty(Session["CustAfterTrans"] as string))
                        {
                            Customers customer = new Customers();
                            DataTable dtCustomerDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                            DateTime wifeBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerSpouseBirthDate].ToString());
                            DateTime husbandBirthDate = DateTime.Parse(dtCustomerDetails.Rows[0][Constants.CustomerBirthDate].ToString());
                            BirthWifeMM.Text = wifeBirthDate.Month.ToString();
                            BirthWifeDD.Text = wifeBirthDate.Day.ToString();
                            birthWifeTempYYYY.Text = wifeBirthDate.Year.ToString();
                            BirthHusbandMM.Text = husbandBirthDate.Month.ToString();
                            BirthHusbandDD.Text = husbandBirthDate.Day.ToString();
                            BirthHusbandTempYYYY.Text = husbandBirthDate.Year.ToString();
                            BirthWifeYYYY.Text = wifeBirthDate.ToShortDateString();
                            BirthHusbandYYYY.Text = husbandBirthDate.ToShortDateString();
                            BeneWife.Text = dtCustomerDetails.Rows[0][Constants.WifesRetAgeBenefit].ToString();
                            BeneHusband.Text = dtCustomerDetails.Rows[0][Constants.HusbandsRetAgeBenefit].ToString();
                            PanelParms.Visible = false;
                            ValidateAndCalc();
                        }
                        else
                        {
                            // Initialize page variables
                            ErrorMessage.Text = String.Empty;
                            PanelGrids.Visible = false;
                            PanelParms.Visible = false;
                            //CheckBoxCola.Checked = true;
                            BirthWifeMM.Focus();
                            //ShowAllInformation();
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                        ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                        ErrorMessagelable.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRestrictCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRestrictCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;

            }

        }

        /// <summary>
        /// Validation and Calculation
        /// </summary>
        private void ValidateAndCalc()
        {
            try
            {

                if (Session["Demo"] == null)
                {
                    Customers customer = new Customers();
                    if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor) || Session[Constants.SessionRole].ToString().Equals(Constants.Institutional) || Session[Constants.SessionRole].ToString().Equals(Constants.SuperAdmin))
                        dtDetails = customer.getCustomerDetails(Session["AdvisorCustomer"].ToString());
                    else
                        dtDetails = customer.getCustomerDetails(Session[Constants.UserId].ToString());
                    customerFirstName = dtDetails.Rows[0][Constants.CustomerFirstName].ToString();
                    customerSposeFirstName = dtDetails.Rows[0][Constants.CustomerSpouseFirstName].ToString();

                }
                else
                {
                    customerFirstName = Session["DemoHusbandName"].ToString();
                    customerSposeFirstName = Session["DemoWifeName"].ToString();
                }

                //customerFirstName = Session["FBUsername"].ToString();
                //customerSposeFirstName = Session["FBSpousename"].ToString();
                Session["FBUsername"] = customerFirstName;
                Session["FBSpousename"] = customerSposeFirstName;

                string WifeBirth = BirthWifeYYYY.Text.ToString();
                char[] delimiters = new char[] { '/' };
                string[] WifeBirthArray = WifeBirth.Split(delimiters, 3);
                birthWifeTempYYYY.Text = WifeBirthArray[2];
                BirthWifeMM.Text = WifeBirthArray[0];
                BirthWifeDD.Text = WifeBirthArray[1];
                string HusbandBirth = BirthHusbandYYYY.Text.ToString();
                string[] HusbandBirthArray = HusbandBirth.Split(delimiters, 3);
                BirthHusbandTempYYYY.Text = HusbandBirthArray[2];
                BirthHusbandMM.Text = HusbandBirthArray[0];
                BirthHusbandDD.Text = HusbandBirthArray[1];
                if (ValidateInput())
                {
                    BuildPage();
                    // Save All Details to respective tables According to martial status
                    //UpdateCustomerFinancialDetails();
                    /* Render GridView Content into Chart */
                    SiteNew.renderGridViewIntoStackedChart(GridStrategySingle, ChartSingle1, LabelBest_1, Constants.Married, true);
                    SiteNew.renderGridViewIntoStackedChart(GridStrategy70, Chart70, LabelLater, Constants.Married, true);
                    SiteNew.renderGridViewIntoStackedChart(GridStrategyFRA, ChartFRA, LabelLater, Constants.Married, true);
                    SiteNew.renderGridViewIntoStackedChart(GridStrategy62, Chart62, LabelLater, Constants.Married, true);
                    SiteNew.renderGridViewIntoStackedChart(GridBest, ChartBest, LabelLater, Constants.Married, true);
                    SiteNew.renderGridViewIntoStackedChart(GridBestLater, ChartBestLater, LabelLater, Constants.Married, true);
                    SiteNew.renderGridViewIntoStackedChart(GridMax, ChartMax, LabelLater, Constants.Married, true);
                    SiteNew.renderGridViewIntoStackedChart(GridAdd1, ChartAdd1, LabelLater, Constants.Married, true);
                    SiteNew.renderGridViewIntoStackedChart(GridAdd2, ChartAdd2, LabelLater, Constants.Married, true);
                    SiteNew.renderGridViewIntoStackedChart(GridStrategyOptional, ChartOptional, LabelLater, Constants.Married, true);

                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRestrictCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRestrictCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// Check input values
        /// </summary>
        /// <returns></returns>
        protected bool ValidateInput()
        {
            try
            {
                /* Initialize Variable */
                ErrorMessage.Text = string.Empty;
                bool isValid = true;
                int birthYYYY, birthMM, birthDD;
                DateTime today = DateTime.Now;
                DateTime wifeBD = today;
                Calc calc = new Calc();
                /* Call function to validate numeric range for DOB */
                birthMM = SiteNew.ValidateNumeric(BirthWifeMM, 1, 12, Constants.Text0, ref isValid, ErrorMessage);
                birthDD = SiteNew.ValidateNumeric(BirthWifeDD, 1, 31, Constants.Text1, ref isValid, ErrorMessage);
                birthYYYY = SiteNew.ValidateNumeric(birthWifeTempYYYY, 1900, 2020, Constants.Text2, ref isValid, ErrorMessage);
                if (isValid)
                {
                    birthYearWife = int.Parse(birthWifeTempYYYY.Text);
                    wifeBD = new DateTime(birthYYYY, birthMM, birthDD);
                }
                intAgeWife = intAgeForWife(wifeBD);
                if (wifeBD.Day >= 28 && wifeBD.Day != DateTime.Now.Day)
                {
                    intAgeWifeMonth = wifeBD.Month + 1;
                    intAgeWifeDays = wifeBD.Day;
                }
                else
                {
                    intAgeWifeMonth = wifeBD.Month;
                    intAgeWifeDays = wifeBD.Day;
                }
                DateTime husbBD = today;
                birthMM = SiteNew.ValidateNumeric(BirthHusbandMM, 1, 12, Constants.SpouseText0, ref isValid, ErrorMessage);
                birthDD = SiteNew.ValidateNumeric(BirthHusbandDD, 1, 31, Constants.SpouseText1, ref isValid, ErrorMessage);
                birthYYYY = SiteNew.ValidateNumeric(BirthHusbandTempYYYY, 1900, 2020, Constants.SpouseText2, ref isValid, ErrorMessage);
                if (isValid)
                {
                    birthYearHusb = int.Parse(BirthHusbandTempYYYY.Text);
                    husbBD = new DateTime(birthYYYY, birthMM, birthDD);
                }
                intAgeHusb = intAgeForHusband(husbBD);
                if (husbBD.Day >= 28 && husbBD.Day != DateTime.Now.Day)
                {
                    intAgeHusbMonth = husbBD.Month + 1;
                    intAgeHusbDays = husbBD.Day;
                }
                else
                {
                    intAgeHusbMonth = husbBD.Month;
                    intAgeHusbDays = husbBD.Day;
                }
                //try and parse the benefits for husband and wife
                decimal.TryParse(this.BeneWife.Text, out decBeneWife);
                decimal.TryParse(this.BeneHusband.Text, out decBeneHusb);
                if (isValid)
                {
                    Customers customer = new Customers();
                    PanelGrids.Visible = true;
                    //PanelEntry.Visible = false;
                    PanelParms.Visible = true;
                    lblCustomerName.Text = customerFirstName;
                    lblCustomerSpouseName.Text = customerSposeFirstName;
                    lblCustomerDOB.Text = husbBD.ToShortDateString();
                    lblCustomerSpouseDOB.Text = wifeBD.ToShortDateString();
                    lblCustomerFRA.Text = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                    lblCustomerSpouseFRA.Text = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                    lblCustomerBenefit.Text = decBeneHusb.ToString(Constants.AMT_FORMAT);
                    lblCustomerSpouseBenefit.Text = decBeneWife.ToString(Constants.AMT_FORMAT);
                }
                else
                {
                    ErrorMessage.Text += ".";
                }
                if (!String.IsNullOrEmpty(ErrorMessage.Text))
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "displayErrorMessage();", true);
                else
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClinetScriptSpouseInformationName, "hideErrorMessage();", true);
                return isValid;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRestrictCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRestrictCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                return false;
            }
        }

        /// <summary>
        /// calculate current age of Wife
        /// </summary>
        /// <param name="husbBD"></param>
        /// <returns></returns>
        protected int intAgeForWife(DateTime wifeBD)
        {
            try
            {
                TimeSpan Span = DateTime.Now - wifeBD;
                DateTime Age = DateTime.MinValue + Span;
                int Years = Age.Year - 1;
                int Months = Age.Month - 1;
                int Days = Age.Day - 1;
                intCurrentAgeWifeYear = Years;
                Globals.Instance.strCurrentAgeWife = Years.ToString() + " years and " + Months.ToString() + " months ";
                if (Days >= 28)
                    Months += 1;
                if (Months >= 6)
                    Years += 1;
                intCurrentAgeWifeMonth = Months;
                return Years;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRestrictCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRestrictCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                return 0;
            }
        }

        /// <summary>
        /// calculate current age of husband
        /// </summary>
        /// <param name="husbBD"></param>
        /// <returns></returns>
        protected int intAgeForHusband(DateTime husbBD)
        {
            try
            {
                TimeSpan Span = DateTime.Now - husbBD;
                DateTime Age = DateTime.MinValue + Span;
                int Years = Age.Year - 1;
                int Months = Age.Month - 1;
                int Days = Age.Day - 1;
                intCurrentAgeHusbYear = Years;
                Globals.Instance.strCurrentAgeHusband = Years.ToString() + " years and " + Months.ToString() + " months ";
                if (Days >= 28)
                    Months += 1;
                if (Months >= 6)
                    Years += 1;
                intCurrentAgeHusbMonth = Months;
                return Years;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRestrictCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRestrictCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                return 0;
            }
        }

        struct Summ
        {
            public string description;
        }

        /// <summary>
        /// Build the page - normally once but repeat for testing
        /// </summary>
        protected void BuildPage()
        {
            try
            {
                ResetValues();

                #region get the age in years from DOB to 01/01/2016
                //loop set to get the changes based on date for new change policy  according to Brian
                //get the age in years from DOB to 01/01/2016
                DateTime HusbDOB = DateTime.Parse(BirthHusbandYYYY.Text.ToString());
                DateTime WifeDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                DateTime limit = DateTime.Parse("05/01/1950");
                DateTime limit1 = DateTime.Parse("01/01/1954");
                DateTime StrategyUsageLimit = DateTime.Parse("05/01/2016");
                //TimeSpan ageYearsh = limit.Subtract(HusbDOB);
                //DateTime Age = DateTime.MinValue + ageYearsh;
                //int Years = Age.Year - 1;
                #endregion get the age in years from DOB to 01/01/2016

                #region code to set basic variable for the function
                // Set calculation properties
                Calc Calc = new Calc()
                {
                    intAgeWife = intAgeWife,
                    intAgeHusb = intAgeHusb,
                    decBeneWife = decBeneWife,
                    decBeneHusb = decBeneHusb,
                    birthYearWife = birthYearWife,
                    birthYearHusb = birthYearHusb,
                    intAgeWifeMonth = intAgeWifeMonth,
                    intAgeHusbMonth = intAgeHusbMonth,
                    husbBirthDate = HusbDOB,
                    intAgeHusbDays = intAgeHusbDays,
                    intAgeWifeDays = intAgeWifeDays,
                    intCurrentAgeHusbMonth = intCurrentAgeHusbMonth,
                    intCurrentAgeWifeMonth = intCurrentAgeWifeMonth,
                    intCurrentAgeWifeYear = intCurrentAgeWifeYear,
                    intCurrentAgeHusbYear = intCurrentAgeHusbYear
                };
                Globals.Instance.WifeDateOfBirth = Convert.ToDateTime(BirthWifeYYYY.Text);
                Globals.Instance.HusbDateOfBirth = Convert.ToDateTime(BirthHusbandYYYY.Text);
                halfBenHusb = decBeneHusb / 2;
                halfbenWife = decBeneWife / 2;
                Session["HusbandBenefit"] = BeneHusband.Text;
                Session["WifeBenefit"] = BeneWife.Text;
                Calc.husbBirthDate = Convert.ToDateTime(BirthHusbandYYYY.Text);
                Calc.wifeBirthDate = Convert.ToDateTime(BirthWifeYYYY.Text);
                Calc.Your_Name = Your_Name.Text;
                Calc.Spouse_Name = Spouse_Name.Text;
                //if (CheckBoxCola.Checked)
                //    Calc.colaAmt = 1.025m;
                //else
                //    Calc.colaAmt = 1;
                Calc.colaAmt = 1.025m;
                colaAmt = Calc.colaAmt;
                //if (CheckBoxSurv.Checked)
                //    Calc.showSurvivor = true;
                //else
                //    Calc.showSurvivor = false;
                //ParmsText3.Text = Calc.roundNote;
                //if (ParmsText3.Text == String.Empty)
                //    PanelAdj3.Visible = false;
                //else
                //    PanelAdj3.Visible = true;



                // Reset changes from prior calculations
                if (LabelLater.Text.EndsWith(Constants.NOT_REC)) LabelLater.Text = LabelLater.Text.Remove(LabelLater.Text.Length - Constants.NOT_REC.Length);
                if (LabelNowLater.Text.EndsWith(Constants.NOT_REC)) LabelNowLater.Text = LabelNowLater.Text.Remove(LabelNowLater.Text.Length - Constants.NOT_REC.Length);
                if (LabelSuspend.Text.EndsWith(Constants.NOT_REC)) LabelSuspend.Text = LabelSuspend.Text.Remove(LabelSuspend.Text.Length - Constants.NOT_REC.Length);


                PanelLater.Visible = true;
                PanelNowLater.Visible = true;
                PanelSuspend.Visible = true;
                PanelBestLater1.Visible = true;
                //ExplainBestLater.Visible = true;
                //ExplainNowLater.Visible = true;
                //ExplainLater.Visible = true;
                //ExplainSuspend.Visible = true;
                //pnlText3.Visible = true;
                Summ summSuspend;
                // Build all of the alternative grids
                bool bBadCandS = false;
                Customers customer = new Customers();

                strUserFirstName = Session["FBUsername"].ToString();
                strUserSpouseFirstName = Session["FBSpousename"].ToString();
                Calc.Your_Name = strUserSpouseFirstName;
                Calc.Spouse_Name = strUserFirstName;
                CommonVariables.strUserName = strUserFirstName;

                #endregion code to set basic variable for the function



                var listRegularStrat = new List<KeyValuePair<string, int>>();
                var listRestrictStrat = new List<KeyValuePair<string, int>>();
                var listAdditionalStrat = new List<KeyValuePair<string, int>>();
                List<string> listCombValues = new List<string>();

                #region New Special Cases implemented on 24 march 2007
                if (RestrictAppIncomeProp.Instance.BoolRestrictNewCase)
                {
                    //Redirect to new strategies of 62 and 66 mentioned on 24/02/2017
                    //Strategy Explaination
                    //Strategy-1 : Lower Benefit spouse will claim WHB at his age 62.. Higher benefit spouse will claim Maxed out WHB at 70.. Lower benefit will switches to spousal benefit when Higher claims Maxed Out WHB at 70
                    //Strategy-2 : Lower Benefit spouse will claim WHB at his age 66.. Higher benefit spouse will claim Maxed out WHB at 70.. Lower benefit will switches to spousal benefit when Higher claims Maxed Out WHB at 70


                    #region Strategy 1 at age 62
                    Calc.BuildGridSuspendRestrictApp(GridNoClaim62, Calc.calc_restrict_strategy.NoClaim62, Calc.calc_start.max70, ref NoteLater, "Full");

                    //highlighting the values in the grid
                    Calc.HighlightRestrictIncome(GridNoClaim62, Calculate.Calc.calc_solo.Married, "Strategy3");
                    Session["CummOne"] = Calc.cumm69;
                    ///lblCombinedIncome3.Text = Calc.valueAtAge70.ToString(Constants.AMT_FORMAT);
                    listCombValues.Add(Calc.valueAtAge70.ToString(Constants.AMT_FORMAT));
                    //code to show the keys and steps in the grid
                    renderKeysIntoGridNoClaim62(Calc.cumm69, Calc.valueAtAge70, Calc.lastBenefitsChangedAge70Value, Calc.startingAgeValueHusb, Calc.startingAgeValueWife, Calc.startingBenefitsValueHusb, Calc.startingBenefitsValueWife, Calc.AgeValueForHigherEarner70, Calc.ValueForHigherEarner70, Calc.ClaimAndSuspendinFullAge, Calc.ClaimAndSuspendinFullValue, Calc.CombinedIncomeAge, Calc.AgeForHusb70, Calc.ValueForHusb70, Calc.AgeForWife70, Calc.ValueForWife70, Calc.AgeValueForSpousalAt70, Calc.ValueForWifeAt70);
                    Calc.cumm69 = 0;
                    ResetAnualIncomeVariables();
                    #endregion Strategy 1 at age 62

                    #region Strategy 2 at FRA age
                    Calc.BuildGridSuspendRestrictApp(GridNoClaim66, Calc.calc_restrict_strategy.NoClaim66, Calc.calc_start.max70, ref NoteLater, "Full");

                    //highlighting the values in the grid
                    Calc.HighlightRestrictIncome(GridNoClaim66, Calculate.Calc.calc_solo.Married, "Strategy2");
                    Session["CummTwo"] = Calc.cumm69;
                    //lblCombinedIncome2.Text = Calc.valueAtAge70.ToString(Constants.AMT_FORMAT);
                    listCombValues.Add(Calc.valueAtAge70.ToString(Constants.AMT_FORMAT));
                    //code to show the keys and steps in the grid
                    renderKeysIntoGridNoClaim66(Calc.cumm69, Calc.valueAtAge70, Calc.lastBenefitsChangedAge70Value, Calc.startingAgeValueHusb, Calc.startingAgeValueWife, Calc.startingBenefitsValueHusb, Calc.startingBenefitsValueWife, Calc.AgeValueForHigherEarner70, Calc.ValueForHigherEarner70, Calc.ClaimAndSuspendinFullAge, Calc.ClaimAndSuspendinFullValue, Calc.CombinedIncomeAge, Calc.AgeForHusb70, Calc.ValueForHusb70, Calc.AgeForWife70, Calc.ValueForWife70, Calc.AgeValueForSpousalAt70, Calc.ValueForWifeAt70);
                    Calc.cumm69 = 0;
                    ResetAnualIncomeVariables();
                    #endregion Strategy 2 at FRA age

                    listRegularStrat.Add(new KeyValuePair<string, int>("NoClaim62", int.Parse(Session["CummOne"].ToString())));
                    listRegularStrat.Add(new KeyValuePair<string, int>("NoClaim66", int.Parse(Session["CummTwo"].ToString())));

                    lblCombinedIncome1.Text = listCombValues[0];
                    lblCombinedIncome3.Text = listCombValues[1];

                    lblPaidNumber1.Text = listRegularStrat[0].Value.ToString(Constants.AMT_FORMAT);
                    lblPaidNumber3.Text = listRegularStrat[1].Value.ToString(Constants.AMT_FORMAT);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showTwoStrategy('{0}','{1}');", listRegularStrat[0].Key.ToString(), listRegularStrat[1].Key.ToString()), true);


                }
                #endregion New Special Cases implemented on 24 march 2007
                else
                {
                    if (RestrictAppIncomeProp.Instance.BoolRestrictSpecialCase)
                    {
                        #region Code to Select 5 or 3 Plan if its special case
                        int intAgeDiff = 0;

                        #region Code to show other two strategies
                        #region Additional Strategy 1
                        Calc.BuildGridSuspendRestrictApp(GridAdd1, Calc.calc_restrict_strategy.Additional1, Calc.calc_start.max70, ref NoteLater, "Full");
                        Calc.HighlightRestrictIncome(GridAdd1, Calculate.Calc.calc_solo.Married, "Full");
                        renderKeysIntoGridAdditional1(Calc.cumm69, Calc.valueAtAge70, Calc.lastBenefitsChangedAge70Value, Calc.startingAgeValueHusb, Calc.startingAgeValueWife, Calc.startingBenefitsValueHusb, Calc.startingBenefitsValueWife, Calc.AgeValueForHigherEarner70, Calc.ValueForHigherEarner70, Calc.ClaimAndSuspendinFullAge, Calc.ClaimAndSuspendinFullValue, Calc.CombinedIncomeAge, Calc.AgeForHusb70, Calc.ValueForHusb70, Calc.AgeForWife70, Calc.ValueForWife70, Calc.AgeValueForSpousalAt70, Calc.ValueForWifeAt70);
                        //cummulative value at age 69
                        Session["CummAdditional1"] = Calc.cumm69;
                        //value at age 69 for higher PIA person
                        Session["valueAtAge69Add1"] = Calc.valueAtAge69;
                        //value at age 70 for higher PIA person
                        Session["valueAtAge70Add1"] = Calc.valueAtAge70;
                        //starting value for husb
                        Session["startingBenefitsValueHusbAdd1"] = Calc.startingBenefitsValueHusb;
                        //starting value for wife
                        Session["startingBenefitsValueWifeAdd1"] = Calc.startingBenefitsValueWife;
                        //to show 3 strategies when age sapn is above 10 years
                        Session[Constants.OdlerGreater] = Calc.odlgerGreater;



                        listCombValues.Add(Calc.valueAtAge70.ToString(Constants.AMT_FORMAT));
                        Calc.cumm69 = 0;
                        ResetAnualIncomeVariables();
                        #endregion Additional Strategy 1

                        #region Additional Strategy 2
                        Calc.BuildGridSuspendRestrictApp(GridAdd2, Calc.calc_restrict_strategy.Additional2, Calc.calc_start.max70, ref NoteLater, "Full");
                        Calc.HighlightRestrictIncome(GridAdd2, Calculate.Calc.calc_solo.Married, "Full");
                        renderKeysIntoGridAdditional2(Calc.cumm69, Calc.valueAtAge70, Calc.lastBenefitsChangedAge70Value, Calc.startingAgeValueHusb, Calc.startingAgeValueWife, Calc.startingBenefitsValueHusb, Calc.startingBenefitsValueWife, Calc.AgeValueForHigherEarner70, Calc.ValueForHigherEarner70, Calc.ClaimAndSuspendinFullAge, Calc.ClaimAndSuspendinFullValue, Calc.CombinedIncomeAge, Calc.AgeForHusb70, Calc.ValueForHusb70, Calc.AgeForWife70, Calc.ValueForWife70, Calc.AgeValueForSpousalAt70, Calc.ValueForWifeAt70);
                        //cummulative value at age 69
                        Session["CummAdditional2"] = Calc.cumm69;
                        //value at age 69 for higher PIA person
                        Session["valueAtAge69Add2"] = Calc.valueAtAge69;
                        //value at age 70 for higher PIA person
                        Session["valueAtAge70Add2"] = Calc.valueAtAge70;
                        //starting value for husb
                        Session["startingBenefitsValueHusbAdd2"] = Calc.startingBenefitsValueHusb;
                        //starting value for wife
                        Session["startingBenefitsValueWifeAdd2"] = Calc.startingBenefitsValueWife;
                        //to show 3 strategies when age sapn is above 10 years
                        Session[Constants.OdlerGreater] = Calc.odlgerGreater;

                        listCombValues.Add(Calc.valueAtAge70.ToString(Constants.AMT_FORMAT));

                        Calc.cumm69 = 0;
                        ResetAnualIncomeVariables();

                        #endregion Additional Strategy 2
                        #endregion Code to show other two strategies

                        #region Build Final Strategy Plan
                        //listRegularStrat.Add(new KeyValuePair<string, int>(Constants.BestStrategy, int.Parse(Session[Constants.BestCumm].ToString())));
                        //listRegularStrat.Add(new KeyValuePair<string, int>(Constants.FullStrategy, int.Parse(Session[Constants.FullCumm].ToString())));

                        //if (Convert.ToDecimal(Session[Constants.valueAtAge70Full].ToString()) != Convert.ToDecimal(Session[Constants.valueAtAge70Max].ToString()) && Globals.Instance.BoolShowMaxStrategy)
                        //    listRegularStrat.Add(new KeyValuePair<string, int>(Constants.MaxStrategy, int.Parse(Session[Constants.MaxxCumm].ToString())));

                        ////Commented as said Brian
                        ////if (int.Parse(Session[Constants.MaxxCumm].ToString()) < int.Parse(Session[Constants.FullCumm].ToString())
                        ////     && int.Parse(Session[Constants.MaxxCumm].ToString()) < int.Parse(Session[Constants.BestCumm].ToString()))
                        ////{
                        ////    list.RemoveAt(2);
                        ////}


                        //////Code to Adjust Strategy Title if strategies are 3
                        //if (listRegularStrat.Count == 2)
                        //{
                        //    lblCombinedIncome1.Text = listCombValues[0];
                        //    lblCombinedIncome3.Text = listCombValues[1];

                        //    lblPaidNumber1.Text = listRegularStrat[0].Value.ToString(Constants.AMT_FORMAT);
                        //    lblPaidNumber3.Text = listRegularStrat[1].Value.ToString(Constants.AMT_FORMAT);
                        //    Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showTwoStrategy('{0}','{1}');", listRegularStrat[0].Key.ToString(), listRegularStrat[1].Key.ToString()), true);
                        //    Session["ShowButton"] = "false";
                        //}

                        ////Code to Adjust Strategy Title if strategies are 3
                        //if (listRegularStrat.Count == 3)
                        //{
                        //    lblCombinedIncome1.Text = listCombValues[0];
                        //    lblCombinedIncome2.Text = listCombValues[1];
                        //    lblCombinedIncome3.Text = listCombValues[2];

                        //    lblPaidNumber1.Text = listRegularStrat[0].Value.ToString(Constants.AMT_FORMAT);
                        //    lblPaidNumber2.Text = listRegularStrat[1].Value.ToString(Constants.AMT_FORMAT);
                        //    lblPaidNumber3.Text = listRegularStrat[2].Value.ToString(Constants.AMT_FORMAT);
                        //    Page.ClientScript.RegisterStartupScript(this.GetType(),
                        //                                            Constants.JsFunc,
                        //                                            String.Format("showThreeStrategy('{0}','{1}','{2}');",
                        //                                                                        listRegularStrat[0].Key.ToString(),
                        //                                                                        listRegularStrat[1].Key.ToString(),
                        //                                                                        listRegularStrat[2].Key.ToString()),
                        //                                            true);

                        //    Session["ShowButton"] = "false";
                        //}

                        bool AgeDiffLessThan8 = CalculateAgeDiffernce(out intAgeDiff);
                        bool IsAgeLessThanFRAand70 = CheckAgeLessThanFRA(Calc.husbFRA, Calc.wifeFRA);
                        if (!Globals.Instance.BoolSkipWHBSwitchRestrict && AgeDiffLessThan8)
                        {
                            if (IsAgeLessThanFRAand70)
                                listAdditionalStrat.Add(new KeyValuePair<string, int>("Additional1", int.Parse(Session["CummAdditional1"].ToString())));
                            if (intAgeDiff <= 3)
                                listAdditionalStrat.Add(new KeyValuePair<string, int>("Additional2", int.Parse(Session["CummAdditional2"].ToString())));
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(),
                                                                   "ShowErrorMessage",
                                                                  "alert('Unable to create additional strategies')",
                                                                   true);
                        }
                        //Code to Adjust Strategy Title if additional strategies are 1
                        if (listAdditionalStrat.Count == 1)
                        {
                            //lblRestrictAppIncomeTitle4.Visible = true;
                            lblCombinedIncome2.Text = listCombValues[0];
                            lblPaidNumber2.Text = listAdditionalStrat[0].Value.ToString(Constants.AMT_FORMAT);
                            Page.ClientScript.RegisterStartupScript(this.GetType(),
                                                                    "ShowAdditionalOneFunction",
                                                                    String.Format("showAdditionalOne('{0}');", listAdditionalStrat[0].Key.ToString()),
                                                                    true);


                            string restrictValue1 = string.Empty, restrictValue2 = string.Empty;
                            if (Session["Add1RestrictAppIncome"] != null)
                                restrictValue1 = Session["Add1RestrictAppIncome"].ToString();
                            if (Session["Add2RestrictAppIncome"] != null)
                                restrictValue2 = Session["Add2RestrictAppIncome"].ToString();
                            if (listAdditionalStrat[0].Key.ToString().Equals("Additional1"))
                            {
                                if (!restrictValue1.Equals("$0"))
                                {
                                    lblRestrictAppIncomeValue2.Text = restrictValue1;
                                    lblRestrictAppIncomeValue2.Visible = true;
                                    lblRestrictAppIncomeTitle2.Visible = true;
                                }
                            }
                            else
                            {
                                if (!restrictValue2.Equals("$0"))
                                {
                                    lblRestrictAppIncomeValue2.Text = restrictValue2;
                                    lblRestrictAppIncomeValue2.Visible = true;
                                    lblRestrictAppIncomeTitle2.Visible = true;
                                }
                            }

                            //Session["ShowButton"] = "true";
                        }
                        //Code to Adjust Strategy Title if additional strategies are 2
                        else if (listAdditionalStrat.Count == 2)
                        {



                            string restrictValue1 = string.Empty, restrictValue2 = string.Empty;
                            if (Session["Add1RestrictAppIncome"] != null)
                                restrictValue1 = Session["Add1RestrictAppIncome"].ToString();
                            if (Session["Add2RestrictAppIncome"] != null)
                                restrictValue2 = Session["Add2RestrictAppIncome"].ToString();

                            if (!restrictValue1.Equals("$0"))
                            {
                                if (!string.IsNullOrEmpty(restrictValue1))
                                {
                                    lblRestrictAppIncomeTitle1.Visible = true;
                                    lblRestrictAppIncomeValue1.Text = restrictValue1;
                                    lblRestrictAppIncomeValue1.Visible = true;
                                }
                            }
                            else
                                lblRestrictAppIncomeTitle1.Visible = false;

                            if (!restrictValue2.Equals("$0"))
                            {
                                if (!string.IsNullOrEmpty(restrictValue2))
                                {
                                    lblRestrictAppIncomeTitle3.Visible = true;
                                    lblRestrictAppIncomeValue3.Text = restrictValue2;
                                    lblRestrictAppIncomeValue3.Visible = true;
                                }
                            }
                            else
                                lblRestrictAppIncomeTitle3.Visible = false;


                            lblCombinedIncome1.Text = listCombValues[0];
                            lblCombinedIncome3.Text = listCombValues[1];

                            lblPaidNumber1.Text = listAdditionalStrat[0].Value.ToString(Constants.AMT_FORMAT);
                            lblPaidNumber3.Text = listAdditionalStrat[1].Value.ToString(Constants.AMT_FORMAT);

                            Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowAdditionalTwoFunction",
                                                                    String.Format("showAdditionalTwo('{0}','{1}');",
                                                                    listAdditionalStrat[0].Key.ToString(),
                                                                    listAdditionalStrat[1].Key.ToString()),
                                                                    true);
                            //Session["ShowButton"] = "true";
                        }

                        #endregion Build Final Strategy Plan

                        #endregion Code to Select 5 or 3 Plan if its special case
                    }
                    else
                    {
                        #region code to build all alternate grids and charts
                        #region Single strategy spousal strategy
                        if (Globals.Instance.BoolIsShowRestrictFull)
                        {
                            #region Single Strategy
                            Calc.BuildGridSuspendRestrictApp(GridStrategySingle, Calc.calc_restrict_strategy.SingleStrategy, Calc.calc_start.max70, ref NoteLater, "Full");
                            Calc.HighlightRestrictIncome(GridStrategySingle, Calculate.Calc.calc_solo.Married, "Full");

                            //cummulative value at age 69
                            Session["CummSingle"] = Calc.cumm69;
                            //value at age 69 for higher PIA person
                            Session[Constants.valueAtAge69Full] = Calc.valueAtAge69;
                            //value at age 70 for higher PIA person
                            Session[Constants.valueAtAge70Full] = Calc.valueAtAge70;
                            //starting value for husb
                            Session[Constants.startingBenefitsValueHusbFull] = Calc.startingBenefitsValueHusb;
                            //starting value for wife
                            Session[Constants.startingBenefitsValueWifeFull] = Calc.startingBenefitsValueWife;
                            //to show 3 strategies when age sapn is above 10 years
                            Session[Constants.OdlerGreater] = Calc.odlgerGreater;

                            listCombValues.Add(Calc.valueAtAge70.ToString(Constants.AMT_FORMAT));
                            //lblCombinedIncome2.Text = Calc.valueAtAge70.ToString(Constants.AMT_FORMAT);
                            //code to show the keys and steps in the grid
                            renderKeysIntoGridFull(Calc.cumm69, Calc.valueAtAge70, Calc.lastBenefitsChangedAge70Value, Calc.startingAgeValueHusb, Calc.startingAgeValueWife, Calc.startingBenefitsValueHusb, Calc.startingBenefitsValueWife, Calc.AgeValueForHigherEarner70, Calc.ValueForHigherEarner70, Calc.ClaimAndSuspendinFullAge, Calc.ClaimAndSuspendinFullValue, Calc.CombinedIncomeAge, Calc.AgeForHusb70, Calc.ValueForHusb70, Calc.AgeForWife70, Calc.ValueForWife70, Calc.AgeValueForSpousalAt70, Calc.ValueForWifeAt70);
                            Calc.cumm69 = 0;
                            ResetAnualIncomeVariables();
                            if (Globals.Instance.BoolSkipWHBSwitchRestrict)
                                lblRestrictAppIncomeTitle2.Visible = false;
                            else
                                lblRestrictAppIncomeTitle2.Visible = true;
                            #endregion Single Strategy

                            #region Optional Strategy (Implemented on 25 April 2017)
                            Calc.BuildGridSuspendRestrictApp(GridStrategyOptional, Calc.calc_restrict_strategy.RestrOptionAtCurrent, Calc.calc_start.max70, ref NoteLater, "Full");
                            Calc.HighlightRestrictIncome(GridStrategyOptional, Calc.calc_solo.Married, "Optional");

                            //cummulative value at age 69
                            Session["CummOptional"] = Calc.cumm69;
                            //value at age 69 for higher PIA person
                            Session[Constants.valueAtAge69Full] = Calc.valueAtAge69;
                            //value at age 70 for higher PIA person
                            Session["valueAtAge70Optional"] = Calc.valueAtAge70;
                            //starting value for husb
                            Session[Constants.startingBenefitsValueHusbFull] = Calc.startingBenefitsValueHusb;
                            //starting value for wife
                            Session[Constants.startingBenefitsValueWifeFull] = Calc.startingBenefitsValueWife;
                            //to show 3 strategies when age sapn is above 10 years
                            Session[Constants.OdlerGreater] = Calc.odlgerGreater;
                            listCombValues.Add(Calc.valueAtAge70.ToString(Constants.AMT_FORMAT));
                            //lblCombinedIncome3.Text = Calc.valueAtAge70.ToString(Constants.AMT_FORMAT);
                            //code to show the keys and steps in the grid
                            renderKeysIntoGridOptional(Calc.cumm69, Calc.valueAtAge70, Calc.lastBenefitsChangedAge70Value, Calc.startingAgeValueHusb, Calc.startingAgeValueWife, Calc.startingBenefitsValueHusb, Calc.startingBenefitsValueWife, Calc.AgeValueForHigherEarner70, Calc.ValueForHigherEarner70, Calc.ClaimAndSuspendinFullAge, Calc.ClaimAndSuspendinFullValue, Calc.CombinedIncomeAge, Calc.AgeForHusb70, Calc.ValueForHusb70, Calc.AgeForWife70, Calc.ValueForWife70, Calc.AgeValueForSpousalAt70, Calc.ValueForWifeAt70);
                            Calc.cumm69 = 0;
                            ResetAnualIncomeVariables();

                            #endregion Optional Strategy (Implemented on 25 April 2017)

                        }
                        #endregion Single strategy spousal strategy

                        #region Other three strategies
                        else
                        {
                            lblRestrictAppIncomeTitle2.Visible = false;

                            #region Strategy 1 at age 70
                            Calc.BuildGridSuspendRestrictApp(GridStrategy70, Calc.calc_restrict_strategy.At70, Calc.calc_start.max70, ref NoteLater, "Full");

                            //highlighting the values in the grid
                            Calc.HighlightRestrictIncome(GridStrategy70, Calculate.Calc.calc_solo.Married, "Strategy1");
                            Session["CummOne"] = Calc.cumm69;
                            //lblCombinedIncome1.Text = Calc.valueAtAge70.ToString(Constants.AMT_FORMAT);
                            listCombValues.Add(Calc.valueAtAge70.ToString(Constants.AMT_FORMAT));
                            //code to show the keys and steps in the grid
                            renderKeysIntoGrid70(Calc.cumm69, Calc.valueAtAge70, Calc.lastBenefitsChangedAge70Value, Calc.startingAgeValueHusb, Calc.startingAgeValueWife, Calc.startingBenefitsValueHusb, Calc.startingBenefitsValueWife, Calc.AgeValueForHigherEarner70, Calc.ValueForHigherEarner70, Calc.ClaimAndSuspendinFullAge, Calc.ClaimAndSuspendinFullValue, Calc.CombinedIncomeAge, Calc.AgeForHusb70, Calc.ValueForHusb70, Calc.AgeForWife70, Calc.ValueForWife70, Calc.AgeValueForSpousalAt70, Calc.ValueForWifeAt70);
                            Calc.cumm69 = 0;
                            ResetAnualIncomeVariables();
                            #endregion Strategy 1 at age 70

                            #region Strategy 2 at FRA age
                            Calc.BuildGridSuspendRestrictApp(GridStrategyFRA, Calc.calc_restrict_strategy.AtFra, Calc.calc_start.max70, ref NoteLater, "Full");

                            //highlighting the values in the grid
                            Calc.HighlightRestrictIncome(GridStrategyFRA, Calculate.Calc.calc_solo.Married, "Strategy2");
                            Session["CummTwo"] = Calc.cumm69;
                            //lblCombinedIncome2.Text = Calc.valueAtAge70.ToString(Constants.AMT_FORMAT);
                            listCombValues.Add(Calc.valueAtAge70.ToString(Constants.AMT_FORMAT));
                            //code to show the keys and steps in the grid
                            renderKeysIntoGridFRA(Calc.cumm69, Calc.valueAtAge70, Calc.lastBenefitsChangedAge70Value, Calc.startingAgeValueHusb, Calc.startingAgeValueWife, Calc.startingBenefitsValueHusb, Calc.startingBenefitsValueWife, Calc.AgeValueForHigherEarner70, Calc.ValueForHigherEarner70, Calc.ClaimAndSuspendinFullAge, Calc.ClaimAndSuspendinFullValue, Calc.CombinedIncomeAge, Calc.AgeForHusb70, Calc.ValueForHusb70, Calc.AgeForWife70, Calc.ValueForWife70, Calc.AgeValueForSpousalAt70, Calc.ValueForWifeAt70);
                            Calc.cumm69 = 0;
                            ResetAnualIncomeVariables();
                            #endregion Strategy 2 at FRA age

                            #region Strategy 3 at age 62
                            Calc.BuildGridSuspendRestrictApp(GridStrategy62, Calc.calc_restrict_strategy.At62, Calc.calc_start.max70, ref NoteLater, "Full");

                            //highlighting the values in the grid
                            Calc.HighlightRestrictIncome(GridStrategy62, Calculate.Calc.calc_solo.Married, "Strategy3");
                            Session["CummThree"] = Calc.cumm69;
                            ///lblCombinedIncome3.Text = Calc.valueAtAge70.ToString(Constants.AMT_FORMAT);
                            listCombValues.Add(Calc.valueAtAge70.ToString(Constants.AMT_FORMAT));
                            //code to show the keys and steps in the grid
                            renderKeysIntoGrid62(Calc.cumm69, Calc.valueAtAge70, Calc.lastBenefitsChangedAge70Value, Calc.startingAgeValueHusb, Calc.startingAgeValueWife, Calc.startingBenefitsValueHusb, Calc.startingBenefitsValueWife, Calc.AgeValueForHigherEarner70, Calc.ValueForHigherEarner70, Calc.ClaimAndSuspendinFullAge, Calc.ClaimAndSuspendinFullValue, Calc.CombinedIncomeAge, Calc.AgeForHusb70, Calc.ValueForHusb70, Calc.AgeForWife70, Calc.ValueForWife70, Calc.AgeValueForSpousalAt70, Calc.ValueForWifeAt70);
                            Calc.cumm69 = 0;
                            ResetAnualIncomeVariables();
                            #endregion Strategy 3 at age 62
                        }
                        #endregion Other three strategies
                        #endregion code to build all alternate grids and charts
                        //Regular Restrict Scenario 2 or 1
                        #region Code select Best Plan if its not a special case
                        if (Globals.Instance.BoolIsShowRestrictFull)
                        {
                            listRestrictStrat.Add(new KeyValuePair<string, int>(Constants.RestrSingle, int.Parse(Session["CummSingle"].ToString())));
                            if (Globals.Instance.BoolShowRestOptionalCurrentAge)
                                listRestrictStrat.Add(new KeyValuePair<string, int>(Constants.RestrOptional, int.Parse(Session["CummOptional"].ToString())));

                            if (listRestrictStrat.Count == 1)
                            {
                                lblPaidNumber2.Text = listRestrictStrat[0].Value.ToString(Constants.AMT_FORMAT);
                                lblCombinedIncome2.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Full]).ToString(Constants.AMT_FORMAT);
                                Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showOneStrategy('{0}');", listRestrictStrat[0].Key.ToString()), true);
                            }
                            else if (listRestrictStrat.Count == 2)
                            {
                                lblPaidNumber1.Text = listRestrictStrat[0].Value.ToString(Constants.AMT_FORMAT);
                                lblPaidNumber3.Text = listRestrictStrat[1].Value.ToString(Constants.AMT_FORMAT);

                                lblCombinedIncome1.Text = listCombValues[0];
                                lblCombinedIncome3.Text = listCombValues[1];
                                Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showTwoStrategy('{0}','{1}');", listRestrictStrat[0].Key.ToString(), listRestrictStrat[1].Key.ToString()), true);
                            }


                        }
                        else
                        {
                            if (!Globals.Instance.BoolHideStrategyAge70)
                                listRegularStrat.Add(new KeyValuePair<string, int>("1", int.Parse(Session["CummOne"].ToString())));
                            listRegularStrat.Add(new KeyValuePair<string, int>("2", int.Parse(Session["CummTwo"].ToString())));
                            listRegularStrat.Add(new KeyValuePair<string, int>("3", int.Parse(Session["CummThree"].ToString())));

                            if (listRegularStrat.Count == 3)
                            {
                                lblPaidNumber1.Text = listRegularStrat[0].Value.ToString(Constants.AMT_FORMAT);
                                lblPaidNumber2.Text = listRegularStrat[1].Value.ToString(Constants.AMT_FORMAT);
                                lblPaidNumber3.Text = listRegularStrat[2].Value.ToString(Constants.AMT_FORMAT);
                                lblCombinedIncome1.Text = listCombValues[0];
                                lblCombinedIncome2.Text = listCombValues[1];
                                lblCombinedIncome3.Text = listCombValues[2];
                                Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showThreeStrategy('{0}','{1}','{2}');", listRegularStrat[0].Key.ToString(), listRegularStrat[1].Key.ToString(), listRegularStrat[2].Key.ToString()), true);
                            }
                            else if (listRegularStrat.Count == 2)
                            {
                                lblPaidNumber1.Text = listRegularStrat[0].Value.ToString(Constants.AMT_FORMAT);
                                lblPaidNumber3.Text = listRegularStrat[1].Value.ToString(Constants.AMT_FORMAT);

                                lblCombinedIncome1.Text = listCombValues[1];
                                lblCombinedIncome3.Text = listCombValues[2];
                                Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showTwoStrategy('{0}','{1}');", listRegularStrat[0].Key.ToString(), listRegularStrat[1].Key.ToString()), true);
                            }
                            else if (listRegularStrat.Count == 1)
                            {
                                lblPaidNumber2.Text = listRegularStrat[0].Value.ToString(Constants.AMT_FORMAT);
                                lblCombinedIncome2.Text = Convert.ToDecimal(Session[Constants.valueAtAge70Full]).ToString(Constants.AMT_FORMAT);
                                Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, String.Format("showOneStrategy('{0}');", listRegularStrat[0].Key.ToString()), true);
                            }
                        }


                        //Add Paid to wait and combined annual income values
                        //AddPaidAndAnnualValForPDF(list);
                        #endregion Code select Best Plan if its not a special case
                    }
                }


                #region code to set the popdup message when HERE link is clicked under the Grids
                //LabelBest.Text = LabelNowLater.Text;
                LabelBestLater1.Text = LabelLater.Text;
                //StrategyBest.Text = StrategyNowLater.Text;
                //StrategyBestLater.Text = StrategyLater.Text;
                PanelNowLater.Visible = false;
                PanelLater.Visible = false;
                ExplainNowLater.Visible = false;
                ExplainLater.Visible = false;
                if (bBadCandS)
                {
                    // When Claim and suspend is bad, hide it
                    PanelSuspend.Visible = false;
                    ExplainSuspend.Visible = false;
                    summSuspend.description = string.Empty;
                }
                //Page.ClientScript.RegisterStartupScript(this.GetType(), Constants.JsFunc, "HideEmptyTextLabel();",true);


                #endregion code to set the popdup message when HERE link is clicked under the Grids
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRestrictCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRestrictCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        private bool CheckAgeLessThanFRA(int husbFRA, int wifeFRA)
        {
            try
            {
                Calc calc = new Calc();
                if (decBeneHusb >= decBeneWife)
                {
                    if (intAgeHusb < husbFRA && intAgeWife < 70)
                        return true;
                }
                else
                {
                    if (intAgeWife < wifeFRA && intAgeHusb < 70)
                        return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRestrictCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRestrictCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                return false;
            }
        }

        /// <summary>
        /// Calculates Age Difference
        /// </summary>
        /// <returns></returns>
        private bool CalculateAgeDiffernce(out int ageDiff)
        {
            try
            {
                int diff = 0;
                if (intAgeWife > intAgeHusb)
                    diff = intAgeWife - intAgeHusb;
                else
                    diff = intAgeHusb - intAgeWife;

                ageDiff = diff;

                if (diff >= 8)
                    return false;
                else
                    return true;

            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorRestrictCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorRestrictCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
                ageDiff = 0;
                return false;
            }

        }

        /// <summary>
        /// restting the values to null for the gloabal variables and lable texts
        /// </summary>
        public void ResetValues()
        {
            try
            {
                #region reset values
                Globals.Instance.BoolSpousalChange = false;
                Globals.Instance.BoolShowMaxStrategy = false;
                Globals.Instance.StringSpousalChangeAgeWife = false;
                Globals.Instance.WifeStartingBenefitName = string.Empty;
                Globals.Instance.HusbStartingBenefitName = string.Empty;
                Globals.Instance.BoolSpousalCliamedHusb = false;
                Globals.Instance.BoolSpousalCliamedWife = false;
                Globals.Instance.BoolAge70ChangeHusb = false;
                Globals.Instance.BoolAge70ChangeWife = false;
                Globals.Instance.BoolAge70StartingAgeHusb = false;
                Globals.Instance.BoolAge70StartingAgeWife = false;
                Globals.Instance.BoolHighlightCummAtWife69Max = false;
                Globals.Instance.BoolHusbandAgeAdjusted = false;
                Globals.Instance.BoolWifeAgeAdjusted = false;
                Globals.Instance.BoolWHBSwitchedHusb = false;
                Globals.Instance.BoolSpousalAfterWorkWife = false;
                Globals.Instance.BoolSpousalAfterWorkHusb = false;
                Globals.Instance.BoolWHBSwitchedWife = false;
                Globals.Instance.intRestrictAppIncomeBestHusb = 0;
                Globals.Instance.intRestrictAppIncomeBestWife = 0;
                Globals.Instance.intRestrictAppIncomeMaxWife = 0;
                Globals.Instance.intRestrictAppIncomeMaxHusb = 0;
                Globals.Instance.intRestrictAppIncomeFullWife = 0;
                Globals.Instance.intRestrictAppIncomeFullHusb = 0;
                Globals.Instance.intRestrictAppIncomeEarlyWife = 0;
                Globals.Instance.intRestrictAppIncomeEarlyHusb = 0;
                Globals.Instance.IntShowRestrictIncomeForFull = 0;
                Globals.Instance.IntShowRestrictIncomeForMax = 0;
                Globals.Instance.IntShowRestrictIncomeForBest = 0;
                Globals.Instance.IntShowRestrictIncomeForEarly = 0;
                Globals.Instance.CurrHusValue = false;
                Globals.Instance.BoolSkipWHBSwitchRestrict = false;
                Globals.Instance.BoolShowRestOptionalCurrentAge = false;
                Globals.Instance.BoolNoClaimRestrictForAdd2 = false;

                //Make keys empty
                //lblKey1.Text = string.Empty;
                //lblKey2.Text = string.Empty;
                //lblKey3.Text = string.Empty;
                //lblKey4.Text = string.Empty;
                //lblKey5.Text = string.Empty;
                //lblKey6.Text = string.Empty;
                lblKey71.Text = string.Empty;
                lblKey81.Text = string.Empty;
                lblKey91.Text = string.Empty;
                lblKeys1_1.Text = string.Empty;
                lblKeys2_1.Text = string.Empty;
                lblKeys3_1.Text = string.Empty;
                //Make steps empty
                lblSteps01.Text = string.Empty;
                lblSteps11.Text = string.Empty;
                lblSteps21.Text = string.Empty;
                lblSteps31.Text = string.Empty;
                //lblStep10.Text = string.Empty;
                //lblStep11.Text = string.Empty;
                //lblStep12.Text = string.Empty;
                //lblStep13.Text = string.Empty;
                lblSteps141.Text = string.Empty;
                lblSteps11.Text = string.Empty;
                lblStep1_1.Text = string.Empty;
                lblStep2_1.Text = string.Empty;
                lblStep3_1.Text = string.Empty;
                //lblStep4.Text = string.Empty;
                //lblStep6.Text = string.Empty;
                //lblStep7.Text = string.Empty;
                //lblStep8.Text = string.Empty;
                //lblStep9.Text = string.Empty;
                //lblStep10.Text = string.Empty;
                //lblStep11.Text = string.Empty;
                //lblStep12.Text = string.Empty;
                //lblStep13.Text = string.Empty;
                //lblStepsSuspend1.Text = string.Empty;
                //lblStepsSuspend2.Text = string.Empty;
                //lblStepsSuspend3.Text = string.Empty;
                //lblStepsSuspend4.Text = string.Empty;
                //lblStepsSuspend5.Text = string.Empty;
                //lblSuspendKey1.Text = string.Empty;
                //lblSuspendKey2.Text = string.Empty;
                //lblSuspendKey3.Text = string.Empty;

                #endregion  reset values
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }

        }

        /// <summary>
        /// code to print the keys and steps to the grid bestlater ie strategy Full retirement
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        public void renderKeysIntoGridFull(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, string ClaimAndSuspendinFullAge, decimal ClaimAndSuspendinFullValue, string CombinedIncomeAge, string AgeForHusb70, decimal ValueForHusb70, string AgeForWife70, decimal ValueForWife70, decimal AgeValueForSpousalAt70, decimal ValueForWifeAt70)
        {
            try
            {
                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName, sWifeFRA, sHusbFRA = string.Empty;
                string strTempClaimeUser = string.Empty, strTempUnClaimedUser = string.Empty, strStartAgeForClaimed = string.Empty, strStartAgeForUnClaimed = string.Empty, strStartColumnForUnClaimed = string.Empty;
                string strValue70ForUnclaimed = string.Empty, strValue70ForClaimed = string.Empty, strAgeForUnclaimed = string.Empty, strColumnForClaimed = string.Empty;
                string strColumnName = string.Empty;
                string strHusbStep = string.Empty, strWifeStep = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                DateTime HusbDOB = DateTime.Parse(BirthHusbandYYYY.Text.ToString());
                DateTime WifeDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                DateTime limit = DateTime.Parse("05/01/1950");
                DateTime limit1 = DateTime.Parse("01/01/1954");
                sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strDoubleAsteriskIncludes = string.Empty;
                if (Globals.Instance.WifeNumberofMonthinNote > 0)
                    strageWife = Globals.Instance.WifeNumberofYearsAbove66 + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                else
                    strageWife = ageWife.Substring(0, 2);
                if (Globals.Instance.HusbandNumberofMonthsinNote > 0)
                    strageHusb = Globals.Instance.HusbNumberofYearsAbove66 + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                else
                    strageHusb = ageWife.Substring(3, 2);
                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    strName = strUserSpouseFirstName;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnName = Constants.keyFigures13;
                }
                //to check if he cummulative income is not zero
                if (cumm69 != 0 && !Globals.Instance.BoolSkipWHBSwitchRestrict)
                {
                    lblKey71.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + combineBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
                    lblKey71.Visible = true;
                }
                else
                {
                    lblKey71.Visible = false;
                }

                //To show maxed out benefit at key section
                if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                {
                    strTempClaimeUser = strUserFirstName;
                    strTempUnClaimedUser = strUserSpouseFirstName;
                    strStartAgeForClaimed = strageHusb;
                    //strStartAgeForUnClaimed = ageWife.Substring(0, 2); 
                    strStartAgeForUnClaimed = strageWife;
                    strStartColumnForUnClaimed = Constants.steps10;
                    strValue70ForUnclaimed = ValueForWife70.ToString(Constants.AMT_FORMAT);
                    strColumnForClaimed = Constants.steps12;
                    if (Globals.Instance.HusbNumberofMonthsAboveGrid != 0)
                        strValue70ForClaimed = ageForHigherEarnerAt70.Substring(3, 2) + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months;
                    else
                        strValue70ForClaimed = ageForHigherEarnerAt70.Substring(3, 2);
                }
                else if (Globals.Instance.BoolIsWifeClaimedBenefit)
                {
                    strTempClaimeUser = strUserSpouseFirstName;
                    strTempUnClaimedUser = strUserFirstName;
                    strStartAgeForClaimed = strageWife;
                    //strStartAgeForUnClaimed = ageWife.Substring(3, 2);
                    strStartAgeForUnClaimed = strageHusb;
                    strStartColumnForUnClaimed = Constants.steps12;
                    strValue70ForUnclaimed = ValueForHusb70.ToString(Constants.AMT_FORMAT);
                    strColumnForClaimed = Constants.steps10;
                    if (Globals.Instance.WifeNumberofMonthsAboveGrid != 0)
                        strValue70ForClaimed = ageForHigherEarnerAt70.Substring(0, 2) + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months;
                    else
                        strValue70ForClaimed = ageForHigherEarnerAt70.Substring(0, 2);
                }

                #region Key and Explaination
                if (combineBenefits != 0 && !Globals.Instance.BoolSkipWHBSwitchRestrict)
                    lblKey81.Text = string.Format(combineBenefits.ToString(Constants.AMT_FORMAT) + strColumnName + Constants.steps7 + strTempUnClaimedUser + Constants.keyFigures8);
                else
                    lblKey81.Text = string.Empty;
                if (valueAtAge70 != 0)
                    lblKey91.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);
                else
                    lblKey91.Text = string.Empty;
                //notes for husband and wife
                if (Globals.Instance.BoolHusbandAgeAdjusted)
                {
                    if (Globals.Instance.BoolSpousalWHBat70 && Globals.Instance.HusbAnnualIcomeAt70 != 0 && Globals.Instance.HusbAnnualIcomeAt69 != 0)//husband age is smaller than 70 when spouse is age 66 (FRA) // Added on 29/09/2016
                        lblNoteHusband51.Text = "# includes " + Globals.Instance.HusbSpousalBenMonthAt69 + Constants.strSpousal_WHB1 + Globals.Instance.HusbAnnualIcomeAt69.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB2 + Globals.Instance.HusbAnnualIcomeAt70.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB3 + strUserFirstName;
                    else
                        lblNoteHusband51.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                }
                if (Globals.Instance.BoolWifeAgeAdjusted)
                    lblNoteWife51.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                    lblHusbandExplanation31.Text = Constants.includes + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                    lblWifeExplanation31.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //notes for husband and wife
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblHusbandExplanation31.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblNoteHusband41.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                }
                else if (Globals.Instance.BoolWHBSwitchedWife)
                {
                    lblWifeExplanation31.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                    lblNoteWife41.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                }
                #endregion Key and Explaination


                #region Code to restrict app steps
                if (!Globals.Instance.BoolSkipWHBSwitchRestrict)
                {
                    Label61.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strTempUnClaimedUser + Constants.steps8 + strStartAgeForUnClaimed + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + strStartColumnForUnClaimed + Constants.steps13 + strTempUnClaimedUser + Constants.steps14 + Constants.SpousalText);
                    lblSteps11.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strTempUnClaimedUser + Constants.steps8 + "70" + Constants.steps18 + strValue70ForUnclaimed + strStartColumnForUnClaimed + Constants.steps15 + Constants.Age70Text);
                    if (RestrictAppIncomeProp.Instance.boolSwitchToWorkHistory)
                    {
                        lblSteps31.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strTempClaimeUser + Constants.steps8 + strValue70ForClaimed + Constants.steps1 + RestrictAppIncomeProp.Instance.strSwichdWorkValue + strColumnForClaimed);
                        lblSteps141.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    }
                    else
                    {
                        lblSteps31.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        lblSteps141.Text = string.Empty;
                    }
                }
                else
                {
                    Label61.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strTempUnClaimedUser + Constants.steps8 + strStartAgeForUnClaimed + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + strStartColumnForUnClaimed + ".");
                    lblSteps31.Text = string.Format(Constants.Line2 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    lblRestrictAppIncomeTitle2.Visible = false;
                }
                #endregion Code to restrict app steps

                #region Get Restricted Application Number
                if (!Globals.Instance.BoolSkipWHBSwitchRestrict)
                {
                    if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                    {
                        Globals.Instance.IntShowRestrictIncomeForFull = Globals.Instance.intRestrictAppIncomeFullWife;
                        lblKeyRestrictIncomFull1.Text = Globals.Instance.intRestrictAppIncomeFullWife.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                        lblRestrictAppIncomeValue2.Text = Globals.Instance.intRestrictAppIncomeFullWife.ToString(Constants.AMT_FORMAT);
                        //lblKeyRestrictIncomFull11.Text = "When " + strUserSpouseFirstName + Constants.RestrictExplain1 + strStartAgeForUnClaimed + Constants.RestrictExplain2 + Globals.Instance.intRestrictAppIncomeFullWife.ToString(Constants.AMT_FORMAT)
                        //                          + Constants.RestrictExplain3 + cumm69.ToString(Constants.AMT_FORMAT);

                        lblKeyRestrictIncomFull11.Text = Constants.RestrictExplainNew1 + strUserSpouseFirstName + Constants.RestrictExplainNew2 + strUserSpouseFirstName + Constants.RestrictExplainNew3 + strStartAgeForUnClaimed + Constants.RestrictExplainNew4 + Globals.Instance.intRestrictAppIncomeFullWife.ToString(Constants.AMT_FORMAT) + Constants.RestrictExplainNew5;


                    }
                    else if (Globals.Instance.BoolIsWifeClaimedBenefit)
                    {
                        Globals.Instance.IntShowRestrictIncomeForFull = Globals.Instance.intRestrictAppIncomeFullHusb;
                        lblKeyRestrictIncomFull1.Text = Globals.Instance.intRestrictAppIncomeFullHusb.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                        lblRestrictAppIncomeValue2.Text = Globals.Instance.intRestrictAppIncomeFullHusb.ToString(Constants.AMT_FORMAT);
                        //lblKeyRestrictIncomFull11.Text = "When " + strUserFirstName + Constants.RestrictExplain1 + strStartAgeForUnClaimed + Constants.RestrictExplain2 + Globals.Instance.intRestrictAppIncomeFullHusb.ToString(Constants.AMT_FORMAT)
                        //                                + Constants.RestrictExplain3 + cumm69.ToString(Constants.AMT_FORMAT);

                        lblKeyRestrictIncomFull11.Text = Constants.RestrictExplainNew1 + strUserFirstName + Constants.RestrictExplainNew2 + strUserFirstName + Constants.RestrictExplainNew3 + strStartAgeForUnClaimed + Constants.RestrictExplainNew4 + Globals.Instance.intRestrictAppIncomeFullHusb.ToString(Constants.AMT_FORMAT) + Constants.RestrictExplainNew5;
                    }
                }
                #endregion Get Restricted Application Number
                //Set Keys and Steps values
                //SetKeyAndStepsValuesForPDF("Full");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to print the keys and steps to the grid bestlater ie strategy Full retirement
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        public void renderKeysIntoGrid70(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, string ClaimAndSuspendinFullAge, decimal ClaimAndSuspendinFullValue, string CombinedIncomeAge, string AgeForHusb70, decimal ValueForHusb70, string AgeForWife70, decimal ValueForWife70, decimal AgeValueForSpousalAt70, decimal ValueForWifeAt70)
        {
            try
            {
                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName, sWifeFRA, sHusbFRA = string.Empty;
                string strColumnName = string.Empty;
                string strHusbStep = string.Empty, strWifeStep = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                DateTime HusbDOB = DateTime.Parse(BirthHusbandYYYY.Text.ToString());
                DateTime WifeDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                DateTime limit = DateTime.Parse("05/01/1950");
                DateTime limit1 = DateTime.Parse("01/01/1954");
                sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strDoubleAsteriskIncludes = string.Empty;
                bool boolRestrictIcomeWife = false, boolRestrictIncomHusb = false;
                if (Globals.Instance.WifeNumberofMonthinNote > 0)
                    strageWife = Globals.Instance.WifeNumberofYearsAbove66 + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                else
                    strageWife = ageWife.Substring(0, 2);
                if (Globals.Instance.HusbandNumberofMonthsinNote > 0)
                    strageHusb = Globals.Instance.HusbNumberofYearsAbove66 + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                else
                    strageHusb = ageHusb.Substring(3, 2);
                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    strName = strUserSpouseFirstName;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnName = Constants.keyFigures13;
                }


                //To show maxed out benefit at key section
                //to check if he cummulative income is not zero
                if (cumm69 != 0)
                {
                    lblKeys1_1.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + combineBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
                    lblKeys1_1.Visible = true;
                }
                else
                {
                    lblKeys1_1.Visible = false;
                }

                #region Key and Explaination
                //if (combineBenefits != 0)
                //    lblKeys2.Text = string.Format(combineBenefits.ToString(Constants.AMT_FORMAT) + strColumnName + Constants.steps7 + strTempUnClaimedUser + Constants.keyFigures8);
                //else
                //    lblKeys2.Text = string.Empty;
                if (valueAtAge70 != 0)
                    lblKeys2_1.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);
                else
                    lblKeys2_1.Text = string.Empty;
                //notes for husband and wife
                if (Globals.Instance.BoolHusbandAgeAdjusted)
                {
                    if (Globals.Instance.BoolSpousalWHBat70 && Globals.Instance.HusbAnnualIcomeAt70 != 0 && Globals.Instance.HusbAnnualIcomeAt69 != 0)//husband age is smaller than 70 when spouse is age 66 (FRA) // Added on 29/09/2016
                        lblNoteHusband51.Text = "# includes " + Globals.Instance.HusbSpousalBenMonthAt69 + Constants.strSpousal_WHB1 + Globals.Instance.HusbAnnualIcomeAt69.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB2 + Globals.Instance.HusbAnnualIcomeAt70.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB3 + strUserFirstName;
                    else
                        lblNoteHusband51.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                }
                if (Globals.Instance.BoolWifeAgeAdjusted)
                    lblNoteWife51.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                    lblHusbandExplanation31.Text = Constants.includes + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                    lblWifeExplanation31.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //notes for husband and wife
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblHusbandExplanation31.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblNoteHusband41.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                }
                else if (Globals.Instance.BoolWHBSwitchedWife)
                {
                    lblWifeExplanation31.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                    lblNoteWife41.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                }
                #endregion Key and Explaination


                #region Code to restrict app steps
                if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                {
                    //lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                    if (startingValueWife > RestrictAppIncomeProp.Instance.intHusbCurrentBenefit)
                        lblStep1_1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15);
                    else
                        lblStep1_1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    lblStep2_1.Text = string.Format(Constants.Line2 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                else
                {
                    if (startingValueWife > RestrictAppIncomeProp.Instance.intWifeCurrentBenefit)
                        lblStep1_1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWife.Substring(3, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15);
                    else
                        lblStep1_1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + ageWife.Substring(3, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                    lblStep2_1.Text = string.Format(Constants.Line2 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }


                #endregion Code to restrict app steps

                //Set Keys and Steps values
                //SetKeyAndStepsValuesForPDF("Full");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to print the keys and steps to the grid bestlater ie strategy Full retirement
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        public void renderKeysIntoGridFRA(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, string ClaimAndSuspendinFullAge, decimal ClaimAndSuspendinFullValue, string CombinedIncomeAge, string AgeForHusb70, decimal ValueForHusb70, string AgeForWife70, decimal ValueForWife70, decimal AgeValueForSpousalAt70, decimal ValueForWifeAt70)
        {
            try
            {
                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName, sWifeFRA, sHusbFRA = string.Empty;
                string strTempClaimeUser = string.Empty, strTempUnClaimedUser = string.Empty, strStartAgeForClaimed = string.Empty, strStartAgeForUnClaimed = string.Empty, strStartColumnForUnClaimed = string.Empty;
                string strValue70ForUnclaimed = string.Empty, strValue70ForClaimed = string.Empty, strAgeForUnclaimed = string.Empty, strColumnForClaimed = string.Empty;
                string strColumnName = string.Empty;
                string strHusbStep = string.Empty, strWifeStep = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                DateTime HusbDOB = DateTime.Parse(BirthHusbandYYYY.Text.ToString());
                DateTime WifeDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                DateTime limit = DateTime.Parse("05/01/1950");
                DateTime limit1 = DateTime.Parse("01/01/1954");
                sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strDoubleAsteriskIncludes = string.Empty;
                //bool boolRestrictIcomeWife = false, boolRestrictIncomHusb = false;

                if (Globals.Instance.WifeNumberofMonthinNote > 0)
                    strageWife = Globals.Instance.WifeNumberofYearsAbove66 + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                else
                    strageWife = ageWife.Substring(0, 2);
                if (Globals.Instance.HusbandNumberofMonthsinNote > 0)
                    strageHusb = Globals.Instance.HusbNumberofYearsAbove66 + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                else
                    strageHusb = ageWife.Substring(3, 2);
                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    strName = strUserSpouseFirstName;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnName = Constants.keyFigures13;
                }

                //to check if he cummulative income is not zero
                if (cumm69 != 0)
                {
                    string attachBenefitWords = string.Empty;
                    if (Globals.Instance.BoolRestrictSpousalAt66)
                        attachBenefitWords = Constants.RestrKeyClaimToSpousal;
                    else
                        attachBenefitWords = Constants.RestrKeyClaimToWork;

                    if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                        lblKey1_2.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strUserSpouseFirstName + attachBenefitWords + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.CummuText3s + strageWife + "," + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
                    else
                        lblKey1_2.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserSpouseFirstName + Constants.And + strUserFirstName + Constants.keyFigures17 + Constants.CummuText1 + strUserFirstName + Constants.RestrKeyClaimToWork + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.CummuText3s + strageHusb + "," + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
                    lblKey1_2.Visible = true;
                }
                else
                {
                    lblKey1_2.Visible = false;
                }

                #region Key and Explaination
                //if (combineBenefits != 0)
                //    lblKeys2.Text = string.Format(combineBenefits.ToString(Constants.AMT_FORMAT) + strColumnName + Constants.steps7 + strTempUnClaimedUser + Constants.keyFigures8);
                //else
                //    lblKeys2.Text = string.Empty;
                if (valueAtAge70 != 0)
                    lblKey2_2.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);
                else
                    lblKey2_2.Text = string.Empty;
                //notes for husband and wife
                if (Globals.Instance.BoolHusbandAgeAdjusted)
                {
                    //if (Globals.Instance.BoolSpousalWHBat70 && Globals.Instance.HusbAnnualIcomeAt70 != 0 && Globals.Instance.HusbAnnualIcomeAt69 != 0)//husband age is smaller than 70 when spouse is age 66 (FRA) // Added on 29/09/2016
                    //    lblNoteHusband5.Text = "# includes " + Globals.Instance.HusbSpousalBenMonthAt69 + Constants.strSpousal_WHB1 + Globals.Instance.HusbAnnualIcomeAt69.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB2 + Globals.Instance.HusbAnnualIcomeAt70.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB3 + strUserFirstName;
                    //else
                    lblNoteHusband1_2.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                }
                if (Globals.Instance.BoolWifeAgeAdjusted)
                    lblNoteWife1_2.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                //if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                //    lblHusbandExplanation3.Text = Constants.includes + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                //else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                //    lblWifeExplanation3.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                ////notes for husband and wife
                //if (Globals.Instance.BoolWHBSwitchedHusb)
                //{
                //    lblHusbandExplanation3.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                //    lblNoteHusband4.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                //}
                //else if (Globals.Instance.BoolWHBSwitchedWife)
                //{
                //    lblWifeExplanation3.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                //    lblNoteWife4.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //}
                #endregion Key and Explaination

                if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                {
                    //lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);

                    if (Globals.Instance.BoolRestrictSpousalAt66)
                        lblStep8_2.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.FRAText + Constants.SpousalSwitch + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    else
                        lblStep8_2.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.FRAText + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    Label5_2.Text = string.Format(Constants.Line2 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                else
                {
                    //lblStep8.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageHusb.Substring(0, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    if (Globals.Instance.BoolRestrictSpousalAt66)
                        lblStep8_2.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.FRAText + Constants.SpousalSwitch + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                    else
                        lblStep8_2.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.FRAText + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                    Label5_2.Text = string.Format(Constants.Line2 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                #region Code to restrict app steps
                //lblStep9.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);

                #endregion Code to restrict app steps

                //Set Keys and Steps values
                //SetKeyAndStepsValuesForPDF("Full");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to print the keys and steps to the grid bestlater ie strategy Full retirement
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        public void renderKeysIntoGrid62(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, string ClaimAndSuspendinFullAge, decimal ClaimAndSuspendinFullValue, string CombinedIncomeAge, string AgeForHusb70, decimal ValueForHusb70, string AgeForWife70, decimal ValueForWife70, decimal AgeValueForSpousalAt70, decimal ValueForWifeAt70)
        {
            try
            {
                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName, sWifeFRA, sHusbFRA = string.Empty;
                string strTempClaimeUser = string.Empty, strTempUnClaimedUser = string.Empty, strStartAgeForClaimed = string.Empty, strStartAgeForUnClaimed = string.Empty, strStartColumnForUnClaimed = string.Empty;
                string strValue70ForUnclaimed = string.Empty, strValue70ForClaimed = string.Empty, strAgeForUnclaimed = string.Empty, strColumnForClaimed = string.Empty;
                string strColumnName = string.Empty;
                string strHusbStep = string.Empty, strWifeStep = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                DateTime HusbDOB = DateTime.Parse(BirthHusbandYYYY.Text.ToString());
                DateTime WifeDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                DateTime limit = DateTime.Parse("05/01/1950");
                DateTime limit1 = DateTime.Parse("01/01/1954");
                sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strDoubleAsteriskIncludes = string.Empty;
                if (Globals.Instance.WifeNumberofMonthinNote > 0)
                    strageWife = Globals.Instance.WifeNumberofYearsAbove66 + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                else
                    strageWife = ageWife.Substring(0, 2);
                if (Globals.Instance.HusbandNumberofMonthsinNote > 0)
                    strageHusb = Globals.Instance.HusbNumberofYearsAbove66 + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                else
                    strageHusb = ageHusb.Substring(3, 2);
                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    strName = strUserSpouseFirstName;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnName = Constants.keyFigures13;
                }
                //to check if he cummulative income is not zero
                if (cumm69 != 0)
                {
                    lblKey4_3.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + combineBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
                    lblKey4_3.Visible = true;
                }
                else
                {
                    lblKey4_3.Visible = false;
                }

                #region Key and Explaination
                //if (combineBenefits != 0)
                //    lblKeys2.Text = string.Format(combineBenefits.ToString(Constants.AMT_FORMAT) + strColumnName + Constants.steps7 + strTempUnClaimedUser + Constants.keyFigures8);
                //else
                //    lblKeys2.Text = string.Empty;
                if (valueAtAge70 != 0)
                    lblKey5_3.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);
                else
                    lblKey5_3.Text = string.Empty;
                //notes for husband and wife
                if (Globals.Instance.BoolHusbandAgeAdjusted)
                {
                    if (Globals.Instance.BoolSpousalWHBat70 && Globals.Instance.HusbAnnualIcomeAt70 != 0 && Globals.Instance.HusbAnnualIcomeAt69 != 0)//husband age is smaller than 70 when spouse is age 66 (FRA) // Added on 29/09/2016
                        lblNoteHusband0_3.Text = "# includes " + Globals.Instance.HusbSpousalBenMonthAt69 + Constants.strSpousal_WHB1 + Globals.Instance.HusbAnnualIcomeAt69.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB2 + Globals.Instance.HusbAnnualIcomeAt70.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB3 + strUserFirstName;
                    else
                        lblNoteHusband0_3.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                }
                if (Globals.Instance.BoolWifeAgeAdjusted)
                    lblNoteWife0_3.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                    lblHusbandExplanation1_3.Text = Constants.includes + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                    lblWifeExplanation1_3.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //notes for husband and wife
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblHusbandExplanation1_3.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblNoteHusband_3.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                }
                else if (Globals.Instance.BoolWHBSwitchedWife)
                {
                    lblWifeExplanation1_3.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                    lblNoteWife_3.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                }
                #endregion Key and Explaination


                #region Code to restrict app steps

                if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                {
                    //lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                    if (Globals.Instance.BoolRestrictSpousalAt62)
                        lblStep5_3.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.ReducedSpousalBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    else
                        lblStep5_3.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    lblStep7_3.Text = string.Format(Constants.Line2 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                else
                {
                    //lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageHusb.Substring(0, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    if (Globals.Instance.BoolRestrictSpousalAt62)
                        lblStep4_3.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.ReducedSpousalBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                    else
                        lblStep4_3.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + strageHusb+ Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                    lblStep7_3.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                //lblStep5.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);

                #endregion Code to restrict app steps

                //Set Keys and Steps values
                //SetKeyAndStepsValuesForPDF("Full");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to print the keys and steps to the grid bestlater ie strategy Full retirement
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        public void renderKeysIntoGridOptional(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, string ClaimAndSuspendinFullAge, decimal ClaimAndSuspendinFullValue, string CombinedIncomeAge, string AgeForHusb70, decimal ValueForHusb70, string AgeForWife70, decimal ValueForWife70, decimal AgeValueForSpousalAt70, decimal ValueForWifeAt70)
        {
            try
            {
                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName, sWifeFRA, sHusbFRA = string.Empty;
                string strTempClaimeUser = string.Empty, strTempUnClaimedUser = string.Empty, strStartAgeForClaimed = string.Empty, strStartAgeForUnClaimed = string.Empty, strStartColumnForUnClaimed = string.Empty;
                string strValue70ForUnclaimed = string.Empty, strValue70ForClaimed = string.Empty, strAgeForUnclaimed = string.Empty, strColumnForClaimed = string.Empty, strStartValueUnclaimed = string.Empty;
                string strColumnName = string.Empty;
                string strHusbStep = string.Empty, strWifeStep = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                DateTime HusbDOB = DateTime.Parse(BirthHusbandYYYY.Text.ToString());
                DateTime WifeDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                DateTime limit = DateTime.Parse("05/01/1950");
                DateTime limit1 = DateTime.Parse("01/01/1954");
                sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strDoubleAsteriskIncludes = string.Empty;
                if (Globals.Instance.WifeNumberofMonthinNote > 0)
                    strageWife = Globals.Instance.WifeNumberofYearsAbove66 + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                else
                    strageWife = ageWife.Substring(0, 2);
                if (Globals.Instance.HusbandNumberofMonthsinNote > 0)
                    strageHusb = Globals.Instance.HusbNumberofYearsAbove66 + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                else
                    strageHusb = ageHusb.Substring(3, 2);
                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    strName = strUserSpouseFirstName;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnName = Constants.keyFigures13;
                }

                //To show maxed out benefit at key section
                if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                {
                    strTempClaimeUser = strUserFirstName;
                    strTempUnClaimedUser = strUserSpouseFirstName;
                    strStartAgeForClaimed = strageHusb;
                    strStartAgeForUnClaimed = strageWife;
                    strStartColumnForUnClaimed = Constants.steps10;
                    strValue70ForUnclaimed = ValueForWife70.ToString(Constants.AMT_FORMAT);
                    strColumnForClaimed = Constants.steps12;
                    if (Globals.Instance.HusbNumberofMonthsAboveGrid != 0)
                        strValue70ForClaimed = ageForHigherEarnerAt70.Substring(3, 2) + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months;
                    else
                        strValue70ForClaimed = ageForHigherEarnerAt70.Substring(3, 2);
                    strStartValueUnclaimed = startingValueWife.ToString(Constants.AMT_FORMAT);
                }
                else if (Globals.Instance.BoolIsWifeClaimedBenefit)
                {
                    strTempClaimeUser = strUserSpouseFirstName;
                    strTempUnClaimedUser = strUserFirstName;
                    strStartAgeForClaimed = strageWife;
                    strStartAgeForUnClaimed = strageHusb;
                    strStartColumnForUnClaimed = Constants.steps12;
                    strValue70ForUnclaimed = ValueForHusb70.ToString(Constants.AMT_FORMAT);
                    strColumnForClaimed = Constants.steps10;
                    if (Globals.Instance.WifeNumberofMonthsAboveGrid != 0)
                        strValue70ForClaimed = ageForHigherEarnerAt70.Substring(0, 2) + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months;
                    else
                        strValue70ForClaimed = ageForHigherEarnerAt70.Substring(0, 2);
                    strStartValueUnclaimed = startingValueHusb.ToString(Constants.AMT_FORMAT);
                }



                //to check if he cummulative income is not zero
                if (cumm69 != 0)
                {
                    lblKey4_Opt.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + combineBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
                    lblKey4_Opt.Visible = true;
                }
                else
                {
                    lblKey4_Opt.Visible = false;
                }

                #region Key and Explaination
                //if (combineBenefits != 0)
                //    lblKeys2.Text = string.Format(combineBenefits.ToString(Constants.AMT_FORMAT) + strColumnName + Constants.steps7 + strTempUnClaimedUser + Constants.keyFigures8);
                //else
                //    lblKeys2.Text = string.Empty;
                if (valueAtAge70 != 0)
                    lblKey5_Opt.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);
                else
                    lblKey5_Opt.Text = string.Empty;
                //notes for husband and wife
                if (Globals.Instance.BoolHusbandAgeAdjusted)
                {
                    if (Globals.Instance.BoolSpousalWHBat70 && Globals.Instance.HusbAnnualIcomeAt70 != 0 && Globals.Instance.HusbAnnualIcomeAt69 != 0)//husband age is smaller than 70 when spouse is age 66 (FRA) // Added on 29/09/2016
                        lblNoteHusband0_Opt.Text = "# includes " + Globals.Instance.HusbSpousalBenMonthAt69 + Constants.strSpousal_WHB1 + Globals.Instance.HusbAnnualIcomeAt69.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB2 + Globals.Instance.HusbAnnualIcomeAt70.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB3 + strUserFirstName;
                    else
                        lblNoteHusband0_Opt.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                }
                if (Globals.Instance.BoolWifeAgeAdjusted)
                    lblNoteWife0_Opt.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                    lblHusbandExplanation1_Opt.Text = Constants.includes + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                    lblWifeExplanation1_Opt.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //notes for husband and wife
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblHusbandExplanation1_Opt.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblNoteHusband41.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                }
                else if (Globals.Instance.BoolWHBSwitchedWife)
                {
                    lblWifeExplanation1_Opt.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                    lblNoteWife_Opt.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                }
                #endregion Key and Explaination


                #region Code to restrict app steps

                if (Globals.Instance.BoolIsHusbandClaimedBenefit)
                {
                    //lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                    //if (Globals.Instance.BoolRestrictSpousalAt62)
                    lblStep5_Opt.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.ReducedSpousalBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    //else
                    //    lblStep5_Opt.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    lblStep7_Opt.Text = string.Format(Constants.Line2 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                else
                {
                    //lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageHusb.Substring(0, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                    //if (Globals.Instance.BoolRestrictSpousalAt62)
                    lblStep4_Opt.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.ReducedSpousalBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                    //else
                    //    lblStep4_Opt.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                    lblStep7_Opt.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                //lblStep5.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);

                #endregion Code to restrict app steps

                //Set Keys and Steps values
                //SetKeyAndStepsValuesForPDF("Full");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }


        /// <summary>
        /// code to print the keys and steps to the grid best strategy1 ie Claim Now, Claim More Later
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        public void renderKeysIntoGridBest(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, string ClaimAndSuspendinFullAge, decimal ClaimAndSuspendinFullValue, string CombinedIncomeAge, string AgeForHusb70, decimal ValueForHusb70, string AgeForWife70, decimal ValueForWife70, decimal AgeValueForSpousalAt70, decimal ValueForWifeAt70)
        {
            try
            {
                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName, strColumnNameHusb = string.Empty;
                string strColumnName = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                string sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                string sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strDoubleAsteriskIncludes = string.Empty;
                bool boolRestrictIncomeWife = false;
                bool boolRestrictIncomeHusb = false;
                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    strName = strUserSpouseFirstName;
                    //strColumnNameWife = Constants.keyFigures14;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnNameHusb = Constants.keyFigures13;
                    strColumnName = Constants.keyFigures13;
                }

                if (Globals.Instance.WifeNumberofMonthinNote > 0)
                    strageWife = Globals.Instance.WifeNumberofYearsAbove66 + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                else
                    strageWife = ageWife.Substring(0, 2);

                if (Globals.Instance.HusbandNumberofMonthsinNote > 0)
                    strageHusb = Globals.Instance.HusbNumberofYearsAbove66 + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                else
                    strageHusb = ageHusb.Substring(3, 2);

                //to check if he cummulative income is not zero
                if (cumm69 != 0)
                {
                    lblKey4.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + combineBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
                    lblKey4.Visible = true;
                }
                else
                {
                    lblKey4.Visible = false;
                }
                if (combineBenefits != 0)
                    lblKey5.Text = string.Format(combineBenefits.ToString(Constants.AMT_FORMAT) + strColumnName + Constants.steps7 + strName + Constants.keyFigures8);
                else
                    lblKey5.Text = string.Empty;
                if (valueAtAge70 != 0)
                    lblKey6.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);
                else
                    lblKey6.Text = string.Empty;

                if (Globals.Instance.BoolHusbandAgeAdjusted && Globals.Instance.HusbandNumberofMonthsinNote > 0)
                    lblNoteHusband0.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                if (Globals.Instance.BoolWifeAgeAdjusted && Globals.Instance.WifeNumberofMonthinNote > 0)
                    lblNoteWife0.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                    lblHusbandExplanation1.Text = Constants.includes + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                    lblWifeExplanation1.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //notes for husband and wife
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblHusbandExplanation1.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblNoteHusband.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                }
                else if (Globals.Instance.BoolWHBSwitchedWife)
                {
                    lblWifeExplanation1.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                    lblNoteWife.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                }

                //to check whos value is greater husband or wife
                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    //if (Globals.Instance.ShowAsterikForTwoStrategies)
                    //{

                    //Label4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ClaimAndSuspendinFullAge + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.CliamAndSuspendWife + Constants.ClaimandSuspendWarning + strUserSpouseFirstName + Constants.CliamAndSuspend1 + strUserFirstName + Constants.CliamAndSuspend2);
                    //lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ClaimAndSuspendinFullAge + Constants.when + strUserFirstName + Constants.steps8 + ClaimAndSuspendinFullAge.Substring(3, 2) + Constants.steps1 + ClaimAndSuspendinFullValue.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.MarriedSpousalText);
                    //if (startingValueHusb != ValueForHusb70)
                    //{
                    //    lblStep4.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps18 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.Age70Text);
                    //    lblStep6.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + Constants.steps9 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    //    lblStep7.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    //    Label4.Visible = true;
                    //    lblStep4.Visible = true;
                    //    lblStep5.Visible = true;
                    //    lblStep6.Visible = true;
                    //    lblStep7.Visible = true;
                    //}
                    //else
                    //{
                    //    lblStep4.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + Constants.steps9 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    //    lblStep6.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    //    Label4.Visible = true;
                    //    lblStep4.Visible = true;
                    //    lblStep5.Visible = true;
                    //    lblStep6.Visible = true;
                    //    lblStep7.Visible = false;
                    //}
                    //Globals.Instance.CurrHusValue = false;
                    //Globals.Instance.CurrHusValuePDF = true;
                    //Globals.Instance.ShowAsterikForTwoStrategies = false;

                    //}
                    //else
                    //{
                    if (sHusbFRA.Substring(0, 2).Equals((intHusbFRA.ToString())) && int.Parse(sHusbFRA.Substring(0, 2)) > int.Parse(strageHusb.Substring(0, 2)))
                    {
                        lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.ReducedWorkBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                    }
                    else
                    {
                        if (sHusbFRA.Substring(0, 2).Equals((intHusbFRA.ToString())))
                            lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.FRAText + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                        else if (int.Parse(sHusbFRA.Substring(0, 2)) > int.Parse(intHusbFRA.ToString()))
                            lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.ReducedWorkBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                        else
                            lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                    }
                    if (startingValueWife != ValueForWife70)
                    {
                        if (intWifeFRA.ToString().Equals(sWifeFRA.Substring(0, 2)))
                            lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                        else
                            lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                        boolRestrictIncomeWife = true;
                        if (Globals.Instance.BoolSpousalChangeAtAge70)
                        {
                            if (ValueForWifeAt70 != 0)
                            {
                                if (Globals.Instance.BoolWHBSwitchedHusb)
                                    lblStep13.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                else
                                    lblStep13.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + AgeValueForSpousalAt70 + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                lblStep13.Visible = true;
                            }
                            else
                            {
                                lblStep13.Visible = false;
                            }
                            lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + Constants.steps18 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                            lblStep7.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            lblStep4.Visible = true;
                            lblStep5.Visible = true;
                            lblStep6.Visible = true;
                            lblStep7.Visible = true;
                        }
                        else
                        {
                            lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + Constants.steps18 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                            lblStep7.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            lblStep4.Visible = true;
                            lblStep5.Visible = true;
                            lblStep6.Visible = true;
                            lblStep7.Visible = true;
                        }
                    }
                    else
                    {
                        if (Globals.Instance.BoolSpousalChangeAtAge70 && ValueForWifeAt70 != 0)
                        {
                            if (Globals.Instance.BoolWHBSwitchedHusb)
                                lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                            else
                                lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + AgeValueForSpousalAt70 + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                            lblStep7.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            lblStep6.Visible = true;
                        }
                        else
                        {
                            lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            lblStep6.Visible = true;
                        }
                        if (intWifeFRA.ToString() == Constants.Number70)
                            lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                        else
                            lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                        lblStep5.Visible = true;
                        lblStep7.Visible = true;
                    }
                    //}
                }
                else
                {
                    //if (Globals.Instance.ShowAsterikForTwoStrategies)
                    //{
                    //    Label4.Text = string.Format(Constants.Line2 + Constants.Age + " " + ClaimAndSuspendinFullAge + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + Constants.CliamAndSuspendHusb + Constants.ClaimandSuspendWarning + strUserFirstName + Constants.CliamAndSuspend1 + strUserSpouseFirstName + Constants.CliamAndSuspend2);
                    //    lblStep5.Text = string.Format(Constants.Line3 + Constants.Age + " " + ClaimAndSuspendinFullAge + Constants.when + strUserSpouseFirstName + Constants.steps8 + ClaimAndSuspendinFullAge.Substring(0, 2) + Constants.steps1 + ClaimAndSuspendinFullValue.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.MarriedSpousalText);
                    //    if (startingValueWife != ValueForWife70)
                    //    {
                    //        lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps18 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                    //        lblStep6.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps9 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    //        lblStep7.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    //        Label4.Visible = true;
                    //        lblStep5.Visible = true;
                    //        lblStep4.Visible = true;
                    //        lblStep6.Visible = true;
                    //        lblStep7.Visible = true;
                    //    }
                    //    else
                    //    {
                    //        lblStep6.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps9 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    //        lblStep7.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    //        Label4.Visible = true;
                    //        lblStep5.Visible = true;
                    //        lblStep4.Visible = true;
                    //        lblStep6.Visible = true;
                    //        lblStep7.Visible = true;
                    //    }
                    //    Globals.Instance.CurrWifeValue = false;
                    //    Globals.Instance.CurrWifeValuePDF = true;
                    //    Globals.Instance.ShowAsterikForTwoStrategies = false;
                    //}
                    //else
                    //{
                    if (strageHusb != Constants.Number70)
                    {
                        //if (sHusbFRA.Substring(0, 2).Equals((strageHusb.Substring(0, 2))))
                        lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                        boolRestrictIncomeHusb = true;
                        //else
                        // lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                    }
                    else
                    {
                        if (Globals.Instance.BoolSpousalCliamedWifeSt1)
                            lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        else
                            lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    }
                    //to check if the starting benefits for husb is same to his age 70 value
                    if (startingValueHusb != ValueForHusb70)
                    {
                        if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                            lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.ReducedWorkBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                            lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        else
                            //lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        if (Globals.Instance.BoolSpousalChangeAtAge70)
                        {
                            if (ValueForWifeAt70 != 0)
                            {

                                lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps18 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                                if (Globals.Instance.BoolWHBSwitchedWife)
                                    lblStep13.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                else
                                    lblStep13.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + AgeValueForSpousalAt70 + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                lblStep7.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStep13.Visible = true;
                            }
                            else
                            {
                                lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps18 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                                lblStep7.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStep13.Visible = false;
                            }
                            lblStep5.Visible = true;
                            lblStep4.Visible = true;
                            lblStep6.Visible = true;
                            lblStep7.Visible = true;
                        }
                        else
                        {
                            lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps18 + valueforHIgherEarnerAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                            lblStep7.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            lblStep5.Visible = true;
                            lblStep4.Visible = true;
                            lblStep6.Visible = true;
                            lblStep7.Visible = true;
                        }
                    }
                    else
                    {
                        if (Globals.Instance.BoolSpousalChangeAtAge70 && ValueForWifeAt70 != 0)
                        {
                            if (Globals.Instance.BoolWHBSwitchedWife)
                                lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            else
                                lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + AgeValueForSpousalAt70 + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            lblStep7.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                        else
                        {
                            lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                        if (intWifeFRA.ToString() == Constants.Number70)
                        {
                            lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps18 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        }
                        else
                        {
                            if (AgeValueForSpousalAt70.ToString() != strageWife)
                            {
                                if (Globals.Instance.BoolSpousalCliamedWifeSt1)
                                {
                                    if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                        lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.ReducedSpousalBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                    else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                        lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                    else
                                        lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                }
                                else
                                {
                                    if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                        //lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.ReducedWorkBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                        lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.ReducedWorkBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                    else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                        lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                    else
                                        lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                                }
                            }
                            else
                            {
                                if (Globals.Instance.BoolWHBSwitchedWife)
                                    lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                else
                                    lblStep5.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + AgeValueForSpousalAt70 + Constants.SpousalSwitch + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                lblStep4.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                                lblStep6.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStep7.Text = "";

                            }
                        }
                        lblStep5.Visible = true;
                        lblStep4.Visible = true;
                        lblStep6.Visible = true;
                        lblStep7.Visible = true;
                    }
                    //}
                }

                if (boolRestrictIncomeWife)
                {
                    Globals.Instance.IntShowRestrictIncomeForBest = Globals.Instance.intRestrictAppIncomeBestWife;
                    lblKeyRestrictIncomBest.Text = Globals.Instance.intRestrictAppIncomeBestWife.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                }
                else if (boolRestrictIncomeHusb)
                {
                    Globals.Instance.IntShowRestrictIncomeForBest = Globals.Instance.intRestrictAppIncomeBestHusb;
                    lblKeyRestrictIncomBest.Text = Globals.Instance.intRestrictAppIncomeBestHusb.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                }
                //Set Keys and Steps values
                //SetKeyAndStepsValuesForPDF("GridBest");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }


        /// <summary>
        /// code to print the keys and steps to the grid max strategy5 ie claim at age 70
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        public void renderKeysIntoGridMax(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, decimal ValueForWife70, decimal ValueForHusb70, string AgeForWife70, string AgeForHusb70, string CombinedIncomeAge)
        {
            try
            {
                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName = string.Empty;
                string strColumnName = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                string sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                string sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strWifeSurvivorText, strHusbSurvivorText;
                string strDoubleAsteriskIncludes = string.Empty;
                bool boolRestrictIncomHusb = false, boolRestrictIncomWife = false;
                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    strName = strUserSpouseFirstName;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnName = Constants.keyFigures13;
                }
                if (ValueForHusb70 > ValueForWife70)
                {
                    strHusbSurvivorText = Constants.steps15;
                    strWifeSurvivorText = string.Empty;
                    lblKeys2.Text = string.Format(ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures13 + Constants.steps7 + strUserFirstName + Constants.keyFigures8);
                }
                else
                {
                    strWifeSurvivorText = Constants.steps15;
                    strHusbSurvivorText = string.Empty;
                    lblKeys2.Text = string.Format(ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures14 + Constants.steps7 + strUserSpouseFirstName + Constants.keyFigures8);
                }
                if (Globals.Instance.WifeNumberofMonthinNote > 0)
                {
                    strageWife = Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                }
                else
                {
                    strageWife = ageWife.Substring(0, 2);
                }

                if (Globals.Instance.HusbandNumberofMonthsinNote > 0)
                {
                    strageHusb = Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                }
                else
                {
                    strageHusb = ageHusb.Substring(3, 2);
                }
                if (cumm69 != 0)
                    lblKeys1.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + combineBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);

                lblKeys3.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);

                //notes for husband and wife
                if (Globals.Instance.BoolHusbandAgeAdjusted)
                    lblNoteHusband7.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                if (Globals.Instance.BoolWifeAgeAdjusted)
                    lblNoteWife7.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                    lblHusbandExplanation4.Text = Constants.includes + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                    lblWifeExplanation4.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //notes for husband and wife
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblHusbandExplanation4.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblNoteHusband6.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                }
                else if (Globals.Instance.BoolWHBSwitchedWife)
                {
                    lblWifeExplanation4.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                    lblNoteWife6.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                }

                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    if (intHusbFRA.ToString().Equals(Constants.Number70) && intWifeFRA.ToString().Equals(Constants.Number70))
                    {
                        if (int.Parse(ageWife.Substring(3, 2)) < intWifeFRA)
                        {
                            lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                            lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + Constants.steps23 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                        }
                        else
                        {
                            lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + Constants.steps23 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                            lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                        }
                        lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        lblStep1.Visible = true;
                        lblStep2.Visible = true;
                        lblStep3.Visible = true;
                    }
                    else
                    {
                        if (intWifeFRA > intHusbFRA)
                        {
                            if (Globals.Instance.BoolMaxStrategyAt70Husb)
                            {
                                if (sHusbFRA.Substring(0, 2).Equals(intHusbFRA.ToString()))
                                    lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                                else
                                    lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                                boolRestrictIncomHusb = true;
                                lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + Constants.steps23 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                                lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + AgeForHusb70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps18 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                                lblStepMax4.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStepMax4.Visible = true;
                                lblStep1.Visible = true;
                                lblStep2.Visible = true;
                                lblStep3.Visible = true;
                            }
                            else
                            {
                                lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                                lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStepMax4.Visible = false;
                                lblStep1.Visible = true;
                                lblStep2.Visible = true;
                                lblStep3.Visible = true;
                            }
                        }
                        else
                        {
                            if (Globals.Instance.BoolMaxStrategyAt70Wife)
                            {
                                if (sWifeFRA.Substring(0, 2).Equals(intWifeFRA.ToString()))
                                    lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                                else
                                    lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                                boolRestrictIncomWife = true;
                                lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                                lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + AgeForWife70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + Constants.steps18 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                                lblStepMax4.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStepMax4.Visible = true;
                                lblStep1.Visible = true;
                                lblStep2.Visible = true;
                                lblStep3.Visible = true;
                            }
                            else
                            {
                                lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                                lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStepMax4.Visible = false;
                                lblStep1.Visible = true;
                                lblStep2.Visible = true;
                                lblStep3.Visible = true;
                            }
                        }
                    }
                }
                else
                {
                    if (intHusbFRA.ToString().Equals(Constants.Number70) && intWifeFRA.ToString().Equals(Constants.Number70))
                    {
                        if (int.Parse(ageHusb.Substring(0, 2)) < intHusbFRA)
                        {
                            lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                            lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + Constants.steps23 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                        }
                        else
                        {
                            lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                            lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + Constants.steps23 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                        }
                        lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        lblStep1.Visible = true;
                        lblStep2.Visible = true;
                        lblStep3.Visible = true;
                        lblStepMax4.Visible = false;
                    }
                    else
                    {

                        if (intWifeFRA > intHusbFRA)
                        {
                            if (Globals.Instance.BoolMaxStrategyAt70Husb)
                            {
                                if (sHusbFRA.Substring(0, 2).Equals(intHusbFRA.ToString()))
                                    lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                                else
                                    lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                                boolRestrictIncomHusb = true;
                                lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + AgeForHusb70 + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps18 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                                lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + Constants.steps23 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                                lblStepMax4.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStep1.Visible = true;
                                lblStep2.Visible = true;
                                lblStep3.Visible = true;
                                lblStepMax4.Visible = true;
                            }
                            else
                            {
                                lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                                lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStep1.Visible = true;
                                lblStep2.Visible = true;
                                lblStep3.Visible = true;
                                lblStepMax4.Visible = false;
                            }
                        }
                        else
                        {
                            if (Globals.Instance.BoolMaxStrategyAt70Wife)
                            {
                                if (sWifeFRA.Substring(0, 2).Equals(intWifeFRA.ToString()))
                                    lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                                else
                                    lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                                boolRestrictIncomWife = true;
                                lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + Constants.Number70 + Constants.steps23 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                                lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + AgeForWife70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Constants.Number70 + Constants.steps18 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strWifeSurvivorText + Constants.Age70Text);
                                lblStepMax4.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStep1.Visible = true;
                                lblStep2.Visible = true;
                                lblStep3.Visible = true;
                                lblStepMax4.Visible = true;
                            }
                            else
                            {
                                lblStep1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strHusbSurvivorText + Constants.Age70Text);
                                lblStep2.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                lblStep3.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                                lblStep1.Visible = true;
                                lblStep2.Visible = true;
                                lblStep3.Visible = true;
                                lblStepMax4.Visible = false;
                            }
                        }
                    }
                }

                if (boolRestrictIncomWife)
                {
                    Globals.Instance.IntShowRestrictIncomeForMax = Globals.Instance.intRestrictAppIncomeMaxWife;
                    lblKeyRestrictIncomMax.Text = Globals.Instance.intRestrictAppIncomeMaxWife.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                }
                else if (boolRestrictIncomHusb)
                {
                    Globals.Instance.IntShowRestrictIncomeForMax = Globals.Instance.intRestrictAppIncomeMaxHusb;
                    lblKeyRestrictIncomMax.Text = Globals.Instance.intRestrictAppIncomeMaxHusb.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                }


                //Set Keys and Steps values
                //SetKeyAndStepsValuesForPDF("Max");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to print the keys and steps to the grid bestlater ie strategy Full retirement
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        public void renderKeysIntoGridFullRegular(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, string ClaimAndSuspendinFullAge, decimal ClaimAndSuspendinFullValue, string CombinedIncomeAge, string AgeForHusb70, decimal ValueForHusb70, string AgeForWife70, decimal ValueForWife70, decimal AgeValueForSpousalAt70, decimal ValueForWifeAt70)
        {
            try
            {
                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName, sWifeFRA, sHusbFRA = string.Empty;
                string strColumnName = string.Empty;
                string strHusbStep = string.Empty, strWifeStep = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                DateTime HusbDOB = DateTime.Parse(BirthHusbandYYYY.Text.ToString());
                DateTime WifeDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                DateTime limit = DateTime.Parse("05/01/1950");
                DateTime limit1 = DateTime.Parse("01/01/1954");
                sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strDoubleAsteriskIncludes = string.Empty;
                bool boolRestrictIcomeWife = false, boolRestrictIncomHusb = false;
                if (Globals.Instance.WifeNumberofMonthinNote > 0)
                    strageWife = Globals.Instance.WifeNumberofYearsAbove66 + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                else
                    strageWife = ageWife.Substring(0, 2);
                if (Globals.Instance.HusbandNumberofMonthsinNote > 0)
                    strageHusb = Globals.Instance.HusbNumberofYearsAbove66 + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                else
                    strageHusb = ageHusb.Substring(3, 2);
                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneWife.Text) > Convert.ToDecimal(BeneHusband.Text))
                {
                    strName = strUserSpouseFirstName;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnName = Constants.keyFigures13;
                }
                //to check if he cummulative income is not zero
                if (cumm69 != 0)
                {
                    lblKey7.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + combineBenefits.ToString(Constants.AMT_FORMAT) + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
                    lblKey7.Visible = true;
                }
                else
                {
                    lblKey7.Visible = false;
                }
                if (combineBenefits != 0)
                    lblKey8.Text = string.Format(combineBenefits.ToString(Constants.AMT_FORMAT) + strColumnName + Constants.steps7 + strName + Constants.keyFigures8);
                else
                    lblKey8.Text = string.Empty;
                if (valueAtAge70 != 0)
                    lblKey9.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);
                else
                    lblKey9.Text = string.Empty;
                //notes for husband and wife
                if (Globals.Instance.BoolHusbandAgeAdjusted)
                {
                    if (Globals.Instance.BoolSpousalWHBat70 && Globals.Instance.HusbAnnualIcomeAt70 != 0 && Globals.Instance.HusbAnnualIcomeAt69 != 0)//husband age is smaller than 70 when spouse is age 66 (FRA) // Added on 29/09/2016
                        lblNoteHusband5.Text = "# includes " + Globals.Instance.HusbSpousalBenMonthAt69 + Constants.strSpousal_WHB1 + Globals.Instance.HusbAnnualIcomeAt69.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB2 + Globals.Instance.HusbAnnualIcomeAt70.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB3 + strUserFirstName;
                    else
                        lblNoteHusband5.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                }
                if (Globals.Instance.BoolWifeAgeAdjusted)
                    lblNoteWife5.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                    lblHusbandExplanation3.Text = Constants.includes + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                    lblWifeExplanation3.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //notes for husband and wife
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblHusbandExplanation3.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblNoteHusband4.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                }
                else if (Globals.Instance.BoolWHBSwitchedWife)
                {
                    lblWifeExplanation3.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                    lblNoteWife4.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                }

                #region when husband cliams WHB and then spousal benefits
                if (Globals.Instance.BoolSpousalCliamedHusb && Globals.Instance.HusbStartingBenefitName.Equals(Constants.Working))
                {
                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.Working))
                    {
                        if (intHusbFRA.ToString().Equals(Constants.Number70))
                        {
                            lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        }
                        else
                        {
                            if (intHusbFRA < int.Parse(sHusbFRA.Substring(0, 2)))
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.ReducedWorkBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                            else if (intHusbFRA == int.Parse(sHusbFRA.Substring(0, 2)))
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                            else
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                        }
                    }
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal))
                    {
                        if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife && WifeDOB < limit1)
                        {
                            Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                            boolRestrictIcomeWife = true;
                        }
                        else
                        {
                            if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal))
                            {
                                if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.ReducedSpousalBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                else
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            }
                        }
                    }
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife)
                    {
                        lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + Constants.steps18 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeWife)
                            lblSteps1.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + Constants.steps23 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    }
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife && Globals.Instance.BoolSpousalCliamedHusb)
                    {
                        if (Globals.Instance.BoolWHBSwitchedHusb)
                            lblSteps2.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months + Constants.steps1 + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                        else
                            lblSteps2.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + AgeValueForSpousalAt70 + Constants.steps1 + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                        lblSteps3.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    }
                    else
                    {
                        if (Globals.Instance.BoolSpousalCliamedHusb)
                        {
                            if (Globals.Instance.BoolWHBSwitchedHusb)
                                lblSteps2.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months + Constants.steps1 + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                            else
                                lblSteps2.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + AgeValueForSpousalAt70 + Constants.steps1 + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                        }
                        lblSteps3.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    }
                }
                #endregion when husband cliams WHB and then spousal benefits

                #region when wife cliams WHB and then spousal benefits
                else if (Globals.Instance.BoolSpousalCliamedWife && Globals.Instance.WifeStartingBenefitName.Equals(Constants.Working))
                {
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.Working))
                    {
                        if (intWifeFRA.ToString().Equals(Constants.Number70))
                        {
                            lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                        }
                        else
                        {
                            if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.ReducedWorkBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            else
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        }
                    }
                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal))
                    {
                        if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb && HusbDOB < limit1)
                        {
                            Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                            boolRestrictIncomHusb = true;
                        }
                        else
                        {
                            if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal))
                            {
                                if (intHusbFRA < int.Parse(sHusbFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.ReducedSpousalBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                else if (intHusbFRA == int.Parse(sHusbFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                else
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                            }
                        }
                    }
                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb)
                    {
                        lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + Constants.steps18 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeHusb)
                            lblSteps1.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + Constants.steps23 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);

                    }
                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb && Globals.Instance.BoolSpousalCliamedWife)
                    {
                        if (Globals.Instance.BoolWHBSwitchedWife)
                            lblSteps2.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months + Constants.steps1 + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                        else
                            lblSteps2.Text = string.Format(Constants.Line4 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + AgeValueForSpousalAt70 + Constants.steps1 + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                        lblSteps3.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    }
                    else
                    {
                        if (Globals.Instance.BoolSpousalCliamedWife)
                        {
                            if (Globals.Instance.BoolWHBSwitchedWife)
                                lblSteps2.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months + Constants.steps1 + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            else
                                lblSteps2.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + AgeValueForSpousalAt70 + Constants.steps1 + ValueForWifeAt70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                        }
                        lblSteps3.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    }
                }
                #endregion when wife cliams WHB and then spousal benefits

                #region when husband cliams and suspends
                else if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb && !strageHusb.Equals(Constants.Number70))
                {
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.Working))
                    {
                        if (intWifeFRA.ToString().Equals(Constants.Number70))
                        {
                            lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                        }
                        else
                        {
                            if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.ReducedWorkBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            else
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        }
                    }
                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal))
                    {
                        if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb && HusbDOB < limit1)
                        {
                            Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                            boolRestrictIncomHusb = true;
                        }
                        else
                        {
                            if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal))
                            {
                                if (intHusbFRA < int.Parse(sHusbFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.ReducedSpousalBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                else if (intHusbFRA == int.Parse(sHusbFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                else
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                            }
                        }
                    }
                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb)
                    {
                        lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + Constants.steps18 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeHusb)
                            lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + Constants.steps23 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    }
                    lblSteps2.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                #endregion when husband cliams and suspends

                #region when wife cliams and suspends
                else if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife && !strageWife.Equals(Constants.Number70))
                {
                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.Working))
                    {
                        if (intHusbFRA.ToString().Equals(Constants.Number70))
                        {
                            lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        }
                        else
                        {
                            if (intHusbFRA < int.Parse(sHusbFRA.Substring(0, 2)))
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.ReducedWorkBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                            else if (intHusbFRA == int.Parse(sHusbFRA.Substring(0, 2)))
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                            else
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                        }
                    }
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal))
                    {
                        if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife && WifeDOB < limit1)
                        {
                            Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                            boolRestrictIcomeWife = true;
                        }
                        else
                        {
                            if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal))
                            {
                                if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.ReducedSpousalBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                else
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            }
                        }
                    }
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife)
                    {
                        lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + Constants.steps18 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeWife)
                            lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + Constants.steps23 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    }
                    lblSteps2.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                #endregion when wife cliams and suspends

                #region when wife cliams sposual and husband working at age 70
                else if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && !Globals.Instance.BoolAge70ChangeWife && Globals.Instance.BoolAge70ChangeHusb)
                {
                    if (!ageForHigherEarnerAt70.Substring(3, 2).Equals(strageHusb) && Globals.Instance.BoolAge70ChangeHusb)
                    {
                        lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + Constants.steps18 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeHusb)
                            lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + Constants.steps23 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    }
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal))
                    {
                        if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife && WifeDOB < limit1)
                        {
                            Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                        }
                        else
                        {
                            if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal))
                            {
                                if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.ReducedSpousalBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);

                                else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                {
                                    if (Globals.Instance.WifeNumberofMonthinNote > 0)
                                        Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                    else
                                        Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                }
                                else
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                            }
                        }
                    }
                    lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                #endregion when wife cliams sposual abd husband working at age 70

                #region when husb cliams sposual and wife working at age 70
                else if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && !Globals.Instance.BoolAge70ChangeHusb && Globals.Instance.BoolAge70ChangeWife)
                {
                    if (!ageForHigherEarnerAt70.Substring(0, 2).Equals(strageWife) && Globals.Instance.BoolAge70ChangeWife)
                    {
                        lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + Constants.steps18 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeWife)
                            lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + Constants.steps23 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                    }

                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal))
                    {
                        if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb && HusbDOB < limit1)
                        {
                            Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                            boolRestrictIncomHusb = true;

                        }
                        else
                        {
                            if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal))
                            {
                                if (intHusbFRA < int.Parse(sHusbFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.ReducedSpousalBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                else if (intHusbFRA == int.Parse(sHusbFRA.Substring(0, 2)))
                                {
                                    if (Globals.Instance.HusbandNumberofMonthsinNote > 0)
                                        Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                    else
                                        Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                }
                                else
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                            }
                        }
                    }
                    lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                #endregion when husb cliams sposual abd wife working at age 70

                #region when wife cliams working abd husb working at age 70
                else if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.Working) && !Globals.Instance.BoolAge70ChangeWife && Globals.Instance.BoolAge70ChangeHusb)
                {
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.Working))
                    {
                        if (intWifeFRA.ToString().Equals(Constants.Number70))
                        {
                            strWifeStep = string.Format(Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                        }
                        else
                        {
                            if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                strWifeStep = string.Format(Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.ReducedWorkBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                strWifeStep = string.Format(Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                            else
                                strWifeStep = string.Format(Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                        }
                    }
                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb)
                    {
                        if (ageForHigherEarnerAt70.Substring(3, 2).Equals(Constants.Number70))
                        {
                            strHusbStep = string.Format(Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + Constants.steps9 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        }
                        else
                        {
                            strHusbStep = string.Format(Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + Constants.steps18 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        }
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeHusb)
                            strHusbStep = string.Format(Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(3, 2) + Constants.steps23 + ValueForHusb70.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                    }

                    //Set step sequence on the basis of age
                    if (intWifeFRA < int.Parse(ageForHigherEarnerAt70.Substring(0, 2)))
                    {
                        lblSteps0.Text = string.Format(Constants.Line1 + strWifeStep);
                        Label6.Text = string.Format(Constants.Line2 + strHusbStep);
                    }
                    else
                    {
                        lblSteps0.Text = string.Format(Constants.Line1 + strHusbStep);
                        Label6.Text = string.Format(Constants.Line2 + strWifeStep);
                    }
                    lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                #endregion when wife cliams working abd husb working at age 70

                #region when husband cliams working abd wife working at age 70
                else if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.Working) && !Globals.Instance.BoolAge70ChangeHusb && Globals.Instance.BoolAge70ChangeWife)
                {

                    if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.Working))
                    {
                        if (intHusbFRA.ToString().Equals(Constants.Number70))
                        {
                            strHusbStep = Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text;
                        }
                        else
                        {
                            if (intHusbFRA < int.Parse(sHusbFRA.Substring(0, 2)))
                                strHusbStep = Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.ReducedWorkBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12;
                            else if (intHusbFRA == int.Parse(sHusbFRA.Substring(0, 2)))
                                strHusbStep = Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12;
                            else
                                strHusbStep = Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12;
                        }
                    }
                    if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife)
                    {
                        strWifeStep = Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + Constants.steps18 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text;
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeWife)
                            strWifeStep = Constants.Age + " " + ageForHigherEarnerAt70 + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageForHigherEarnerAt70.Substring(0, 2) + Constants.steps23 + ValueForWife70.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text;
                    }

                    //Set step sequence on the basis of age
                    if (int.Parse(ageHusb.Substring(0, 2)) < int.Parse(ageForHigherEarnerAt70.Substring(0, 2)))
                    {
                        lblSteps0.Text = string.Format(Constants.Line1 + strHusbStep);
                        Label6.Text = string.Format(Constants.Line2 + strWifeStep);
                    }
                    else
                    {
                        lblSteps0.Text = string.Format(Constants.Line1 + strWifeStep);
                        Label6.Text = string.Format(Constants.Line2 + strHusbStep);
                    }
                    lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }
                #endregion when husband cliams working and wife working at age 70

                #region when husband and wife working at age 70
                else if (Globals.Instance.BoolAge70StartingAgeWife)
                {
                    if (Globals.Instance.BoolAge70StartingAgeHusb)
                    {

                        if (intWifeFRA > intHusbFRA)
                        {
                            if (ValueForHusb70 > ValueForWife70)
                            {
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                                Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                            }
                            else
                            {
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.Age70Text);
                                Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                            }
                        }
                        else
                        {
                            if (ValueForHusb70 > ValueForWife70)
                            {
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                                Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                            }
                            else
                            {
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                                Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.Age70Text);
                            }

                        }
                        lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    }
                    else
                    {
                        if (Globals.Instance.BoolAge70ChangeWife)
                        {
                            lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                        }
                        if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeHusb && HusbDOB < limit1)
                        {
                            Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps11 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps13 + strUserFirstName + Constants.steps14 + Constants.SpousalText);
                            boolRestrictIncomHusb = true;
                        }
                        else
                        {
                            if (Globals.Instance.HusbStartingBenefitName.Equals(Constants.spousal))
                            {
                                if (intHusbFRA < int.Parse(sHusbFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.ReducedSpousalBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                else if (intHusbFRA == int.Parse(sHusbFRA.Substring(0, 2)))
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.FRAText + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                                else
                                    Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.steps1 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + strUserFirstName + Constants.SpousalSwitch1 + strUserSpouseFirstName + Constants.SpousalSwitch2);
                            }
                        }
                        if (Globals.Instance.BoolAge70ChangeHusb)
                        {
                            lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                            lblSteps2.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                        else
                        {
                            lblSteps2.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                    }
                }

                else if (Globals.Instance.BoolAge70StartingAgeHusb)
                {
                    if (Globals.Instance.BoolAge70StartingAgeWife)
                    {
                        if (intWifeFRA > intHusbFRA)
                        {
                            if (ValueForHusb70 > ValueForWife70)
                            {
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                                Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                            }
                            else
                            {
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.Age70Text);
                                Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                            }
                        }
                        else
                        {
                            if (ValueForHusb70 > ValueForWife70)
                            {
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.Age70Text);
                                Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                            }
                            else
                            {
                                lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                                Label6.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.Age70Text);
                            }

                        }
                        lblSteps1.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    }
                    else
                    {

                        if (Globals.Instance.BoolAge70ChangeHusb)
                        {
                            lblSteps0.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + ageHusb.Substring(3, 2) + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12 + Constants.steps15 + Constants.Age70Text);
                        }
                        if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal))
                        {
                            if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal) && Globals.Instance.BoolAge70ChangeWife && WifeDOB < limit1)
                            {
                                lblSteps1.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps11 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps13 + strUserSpouseFirstName + Constants.steps14 + Constants.SpousalText);
                                boolRestrictIcomeWife = true;
                            }
                            else
                            {
                                if (Globals.Instance.WifeStartingBenefitName.Equals(Constants.spousal))
                                {
                                    if (intWifeFRA < int.Parse(sWifeFRA.Substring(0, 2)))
                                        lblSteps1.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.ReducedSpousalBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                    else if (intWifeFRA == int.Parse(sWifeFRA.Substring(0, 2)))
                                        lblSteps1.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.FRAText + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                    else
                                        lblSteps1.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + strUserSpouseFirstName + Constants.SpousalSwitch1 + strUserFirstName + Constants.SpousalSwitch2);
                                }
                            }
                        }
                        if (Globals.Instance.BoolAge70ChangeWife)
                        {
                            lblSteps2.Text = string.Format(Constants.Line3 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + ageWife.Substring(0, 2) + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10 + Constants.steps15 + Constants.Age70Text);
                            lblSteps3.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                        else
                        {
                            lblSteps3.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                    }
                }
                #endregion when husband and wife working at age 70

                if (boolRestrictIcomeWife)
                {
                    Globals.Instance.IntShowRestrictIncomeForFull = Globals.Instance.intRestrictAppIncomeFullWife;
                    lblKeyRestrictIncomFull.Text = Globals.Instance.intRestrictAppIncomeFullWife.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                }
                else if (boolRestrictIncomHusb)
                {
                    Globals.Instance.IntShowRestrictIncomeForFull = Globals.Instance.intRestrictAppIncomeFullHusb;
                    lblKeyRestrictIncomFull.Text = Globals.Instance.intRestrictAppIncomeFullHusb.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                }
                //Set Keys and Steps values
                //SetKeyAndStepsValuesForPDF("Full");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }


        /// <summary>
        /// code to print the keys and steps to the grid bestlater ie strategy Full retirement
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        public void renderKeysIntoGridAdditional1(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, string ClaimAndSuspendinFullAge, decimal ClaimAndSuspendinFullValue, string CombinedIncomeAge, string AgeForHusb70, decimal ValueForHusb70, string AgeForWife70, decimal ValueForWife70, decimal AgeValueForSpousalAt70, decimal ValueForWifeAt70)
        {
            try
            {
                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName, sWifeFRA, sHusbFRA = string.Empty;
                string strTempHigherUserName = string.Empty, strTempLowerUserName = string.Empty, strStartAgeForHigher = string.Empty, strStartAgeForLower = string.Empty, strStartColumnForLower = string.Empty;
                string strValue70ForLower = string.Empty, strValue70ForHigher = string.Empty, strAgeForLower = string.Empty, strColumnForHigher = string.Empty,
                       strLastAgeForHigher = string.Empty, str70SwitchAgeofLower = string.Empty, strHighlitedColForLower = string.Empty, strHighlitedColFOrHigher = string.Empty,
                       strSpousalClaimedAgeOfLower = string.Empty, strSpusalValueForLower = string.Empty;
                string strColumnName = string.Empty;
                string strHusbStep = string.Empty, strWifeStep = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                decimal age70valueHigher = 0, age70valueLower = 0;
                DateTime HusbDOB = DateTime.Parse(BirthHusbandYYYY.Text.ToString());
                DateTime WifeDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                DateTime limit = DateTime.Parse("05/01/1950");
                DateTime limit1 = DateTime.Parse("01/01/1954");
                sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strDoubleAsteriskIncludes = string.Empty;
                if (Globals.Instance.WifeNumberofMonthsinSteps > 0 && Globals.Instance.WifeNumberofMonthsinSteps != 12)
                    strageWife = Globals.Instance.WifeNumberofYearsAbove66 + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                else
                    strageWife = ageWife.Substring(0, 2);
                if (Globals.Instance.HusbandNumberofMonthsinSteps > 0 && Globals.Instance.HusbandNumberofMonthsinSteps != 12)
                    strageHusb = Globals.Instance.HusbNumberofYearsAbove66 + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                else
                    strageHusb = ageHusb.Substring(3, 2);

                string FRAageofLower = string.Empty;
                //To show maxed out benefit at key section
                if (decBeneHusb >= decBeneWife)
                {
                    Globals.Instance.IntShowRestrictIncomeForFull = Globals.Instance.intRestrictAppIncomeFullWife;
                    strTempHigherUserName = strUserFirstName;
                    strTempLowerUserName = strUserSpouseFirstName;
                    //strStartAgeForHigher = ageHusb.Substring(3, 2).ToString();
                    //strStartAgeForLower = ageWife.Substring(0, 2).ToString();
                    strStartAgeForHigher = strageHusb;
                    strStartAgeForLower = strageWife;
                    strStartColumnForLower = Constants.steps10;
                    strValue70ForLower = ValueForWife70.ToString(Constants.AMT_FORMAT);
                    strColumnForHigher = Constants.steps12;
                    if (Globals.Instance.HusbNumberofMonthsAboveGrid != 0)
                        strValue70ForHigher = Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months;
                    else
                        strValue70ForHigher = AgeForWife70.Substring(3, 2);
                    str70SwitchAgeofLower = AgeForWife70;
                    strHighlitedColForLower = Constants.keyFigures14;
                    strSpousalClaimedAgeOfLower = ageWife;
                    strSpusalValueForLower = startingValueWife.ToString(Constants.AMT_FORMAT);
                    age70valueHigher = ValueForHusb70;
                    age70valueLower = ValueForWife70;

                    FRAageofLower = sWifeFRA;
                }
                else
                {
                    Globals.Instance.IntShowRestrictIncomeForFull = Globals.Instance.intRestrictAppIncomeFullHusb;
                    strTempHigherUserName = strUserSpouseFirstName;
                    strTempLowerUserName = strUserFirstName;
                    //strStartAgeForHigher = ageWife.Substring(0, 2).ToString();
                    //strStartAgeForLower = ageHusb.Substring(3, 2).ToString();
                    strStartAgeForHigher = strageWife;
                    strStartAgeForLower = strageHusb;
                    strStartColumnForLower = Constants.steps12;
                    strValue70ForLower = ValueForHusb70.ToString(Constants.AMT_FORMAT);
                    strColumnForHigher = Constants.steps10;
                    if (Globals.Instance.WifeNumberofMonthsAboveGrid != 0)
                        strValue70ForHigher = Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months;
                    else
                        strValue70ForHigher = AgeForHusb70.Substring(0, 2);

                    str70SwitchAgeofLower = AgeForHusb70;
                    strHighlitedColForLower = Constants.keyFigures13;
                    strSpousalClaimedAgeOfLower = ageHusb;
                    strSpusalValueForLower = startingValueHusb.ToString(Constants.AMT_FORMAT);
                    age70valueHigher = ValueForWife70;
                    age70valueLower = ValueForHusb70;

                    FRAageofLower = sHusbFRA;
                }


                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneHusband.Text) >= Convert.ToDecimal(BeneWife.Text))
                {
                    strName = strUserSpouseFirstName;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnName = Constants.keyFigures13;
                }

                #region Key and Explaination
                //to check if he cummulative income is not zero
                if (cumm69 != 0)
                {
                    lblAdd1Key1.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + strValue70ForLower + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
                    lblAdd1Key1.Visible = true;
                }
                else
                {
                    lblAdd1Key1.Visible = false;
                }
                //Add maxed out work history benefit keys
                lblAdd1Key2.Text = string.Format(strValue70ForLower + strHighlitedColForLower + Constants.steps7 + strTempLowerUserName + Constants.keyFigures8);

                //Add Combined benefit keys
                if (valueAtAge70 != 0)
                    lblAdd1Key3.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);
                else
                    lblAdd1Key3.Text = string.Empty;

                //Add Extra Income Keys
                if (!Globals.Instance.BoolSkipWHBSwitchRestrict)
                {

                    lblAdd1RestrictKey1.Text = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                    lblRestrictAppIncomeValue4.Text = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT);
                    //lblAdd1RestrictKey2.Text = "When " + strTempLowerUserName + Constants.RestrictExplain1 + strStartAgeForLower + Constants.RestrictExplain2 + Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT)
                    //                                + Constants.RestrictExplain3 + cumm69.ToString(Constants.AMT_FORMAT);
                    lblAdd1RestrictKey2.Text = Constants.RestrictExplainNew1 + strTempLowerUserName + Constants.RestrictExplainNew2 + strTempLowerUserName + Constants.RestrictExplainNew3 + strStartAgeForLower + Constants.RestrictExplainNew4 + Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT) + Constants.RestrictExplainNew5;
                    lblRestrictAppIncomeValue4.Visible = true;
                    Session["Add1RestrictAppIncome"] = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT);
                }

                //notes for husband and wife
                if (Globals.Instance.BoolHusbandAgeAdjusted)
                {
                    if (Globals.Instance.BoolSpousalWHBat70 && Globals.Instance.HusbAnnualIcomeAt70 != 0 && Globals.Instance.HusbAnnualIcomeAt69 != 0)//husband age is smaller than 70 when spouse is age 66 (FRA) // Added on 29/09/2016
                        lblAdd1NoteHusb1.Text = "# includes " + Globals.Instance.HusbSpousalBenMonthAt69 + Constants.strSpousal_WHB1 + Globals.Instance.HusbAnnualIcomeAt69.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB2 + Globals.Instance.HusbAnnualIcomeAt70.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB3 + strUserFirstName;
                    else
                        lblAdd1NoteHusb1.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                }
                if (Globals.Instance.BoolWifeAgeAdjusted)
                    lblAdd1NoteWife1.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                    lblAdd1NoteHusb2.Text = Constants.includes + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                    lblAdd1NoteWife2.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //notes for husband and wife
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblAdd1NoteHusb2.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblAdd1NoteHusb3.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                }
                else if (Globals.Instance.BoolWHBSwitchedWife)
                {
                    lblAdd1NoteWife2.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                    lblAdd1NoteWife3.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                }
                #endregion Key and Explaination


                #region Code to restrict app steps

                if (decBeneHusb >= decBeneWife)
                    lblAdd1Step1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + strageHusb + Constants.ReducedWorkBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                else
                    lblAdd1Step1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + strageWife + Constants.ReducedWorkBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);

                if (!Globals.Instance.BoolSkipWHBSwitchRestrict)
                {
                    lblAdd1Step3.Text = string.Format(Constants.Line2 + Constants.Age + " " + strSpousalClaimedAgeOfLower + Constants.when + strTempLowerUserName + Constants.steps8 + strStartAgeForLower + Constants.steps11 + strSpusalValueForLower + strStartColumnForLower + Constants.steps13 + strTempLowerUserName + Constants.steps14 + Constants.SpousalText);
                    if (age70valueLower > age70valueHigher)
                        lblAdd1Step4.Text = string.Format(Constants.Line3 + Constants.Age + " " + str70SwitchAgeofLower + Constants.when + strTempLowerUserName + Constants.steps8 + "70" + Constants.steps18 + strValue70ForLower + strStartColumnForLower + Constants.steps15 + Constants.Age70Text);
                    else
                        lblAdd1Step4.Text = string.Format(Constants.Line3 + Constants.Age + " " + str70SwitchAgeofLower + Constants.when + strTempLowerUserName + Constants.steps8 + "70" + Constants.steps18 + strValue70ForLower + strStartColumnForLower);
                    if (RestrictAppIncomeProp.Instance.boolSwitchToWorkHistory)
                    {
                        lblAdd1Step5.Text = string.Format(Constants.Line4 + Constants.Age + " " + str70SwitchAgeofLower + Constants.when + strTempHigherUserName + Constants.steps8 + strValue70ForHigher + Constants.steps1 + RestrictAppIncomeProp.Instance.strSwichdWorkValue + strColumnForHigher);
                        lblAdd1Step6.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    }
                    else
                    {
                        lblAdd1Step5.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        lblAdd1Step6.Text = string.Empty;
                    }
                }
                else
                {
                    lblAdd1Step3.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strTempLowerUserName + Constants.steps8 + strStartAgeForLower + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + strStartColumnForLower + ".");
                    lblAdd1Step5.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    lblRestrictAppIncomeTitle4.Visible = false;
                }

                #endregion Code to restrict app steps

                //Set Keys and Steps values
                //SetKeyAndStepsValuesForPDF("Full");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }


        /// <summary>
        /// code to print the keys and steps to the grid bestlater ie strategy Full retirement
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        public void renderKeysIntoGridAdditional2(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, string ClaimAndSuspendinFullAge, decimal ClaimAndSuspendinFullValue, string CombinedIncomeAge, string AgeForHusb70, decimal ValueForHusb70, string AgeForWife70, decimal ValueForWife70, decimal AgeValueForSpousalAt70, decimal ValueForWifeAt70)
        {
            try
            {
                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName, sWifeFRA, sHusbFRA = string.Empty;
                string strTempHigherUserName = string.Empty, strTempLowerUserName = string.Empty, strStartAgeForHigher = string.Empty, strStartAgeForLower = string.Empty, strStartColumnForLower = string.Empty;
                string strValue70ForLower = string.Empty, strValue70ForHigher = string.Empty, strAgeForLower = string.Empty, strColumnForHigher = string.Empty,
                       strLastAgeForHigher = string.Empty, str70SwitchAgeofLower = string.Empty, strHighlitedColForLower = string.Empty, strHighlitedColFOrHigher = string.Empty,
                       strSpousalClaimedAgeOfLower = string.Empty, strSpusalValueForLower = string.Empty;
                string strColumnName = string.Empty;
                string strHusbStep = string.Empty, strWifeStep = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                decimal age70valueHigher = 0, age70valueLower = 0;
                DateTime HusbDOB = DateTime.Parse(BirthHusbandYYYY.Text.ToString());
                DateTime WifeDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                DateTime limit = DateTime.Parse("05/01/1950");
                DateTime limit1 = DateTime.Parse("01/01/1954");
                sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strDoubleAsteriskIncludes = string.Empty;
                if (Globals.Instance.WifeNumberofMonthinNote > 0)
                    strageWife = Globals.Instance.WifeNumberofYearsAbove66 + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                else
                    strageWife = ageWife.Substring(0, 2);
                if (Globals.Instance.HusbandNumberofMonthsinNote > 0)
                    strageHusb = Globals.Instance.HusbNumberofYearsAbove66 + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                else
                    strageHusb = ageHusb.Substring(3, 2);

                //To show maxed out benefit at key section
                if (decBeneHusb >= decBeneWife)
                {
                    Globals.Instance.IntShowRestrictIncomeForFull = Globals.Instance.intRestrictAppIncomeFullWife;
                    strTempHigherUserName = strUserFirstName;
                    strTempLowerUserName = strUserSpouseFirstName;
                    strStartAgeForHigher = ageHusb.Substring(3, 2);
                    strStartAgeForLower = strageWife;
                    strStartColumnForLower = Constants.steps10;
                    strValue70ForLower = ValueForWife70.ToString(Constants.AMT_FORMAT);
                    strColumnForHigher = Constants.steps12;
                    if (Globals.Instance.HusbNumberofMonthsAboveGrid != 0)
                        strValue70ForHigher = Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months;
                    else
                        strValue70ForHigher = AgeForWife70.Substring(3, 2);
                    str70SwitchAgeofLower = AgeForWife70;
                    strHighlitedColForLower = Constants.keyFigures14;
                    strSpousalClaimedAgeOfLower = ageWife;
                    strSpusalValueForLower = startingValueWife.ToString(Constants.AMT_FORMAT);
                    age70valueHigher = ValueForHusb70;
                    age70valueLower = ValueForWife70;
                }
                else
                {
                    Globals.Instance.IntShowRestrictIncomeForFull = Globals.Instance.intRestrictAppIncomeFullHusb;
                    strTempHigherUserName = strUserSpouseFirstName;
                    strTempLowerUserName = strUserFirstName;
                    strStartAgeForHigher = ageWife.Substring(0, 2).ToString();
                    strStartAgeForLower = strageHusb;
                    strStartColumnForLower = Constants.steps12;
                    strValue70ForLower = ValueForHusb70.ToString(Constants.AMT_FORMAT);
                    strColumnForHigher = Constants.steps10;
                    if (Globals.Instance.WifeNumberofMonthsAboveGrid != 0)
                        strValue70ForHigher = Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months;
                    else
                        strValue70ForHigher = AgeForHusb70.Substring(0, 2);

                    str70SwitchAgeofLower = AgeForHusb70;
                    strHighlitedColForLower = Constants.keyFigures13;
                    strSpousalClaimedAgeOfLower = ageHusb;
                    strSpusalValueForLower = startingValueHusb.ToString(Constants.AMT_FORMAT);

                    age70valueHigher = ValueForWife70;
                    age70valueLower = ValueForHusb70;
                }


                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneHusband.Text) >= Convert.ToDecimal(BeneWife.Text))
                {
                    strName = strUserSpouseFirstName;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnName = Constants.keyFigures13;
                }

                #region Key and Explaination
                //to check if he cummulative income is not zero
                if (cumm69 != 0)
                {
                    lblAdd2Key1.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + strValue70ForLower + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
                    lblAdd2Key1.Visible = true;
                }
                else
                {
                    lblAdd2Key1.Visible = false;
                }
                //Add maxed out work history benefit keys
                lblAdd2Key2.Text = string.Format(strValue70ForLower + strHighlitedColForLower + Constants.steps7 + strTempLowerUserName + Constants.keyFigures8);

                //Add Combined benefit keys
                if (valueAtAge70 != 0)
                    lblAdd2Key3.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);
                else
                    lblAdd2Key3.Text = string.Empty;

                //Add Extra Income Keys
                if (!Globals.Instance.BoolSkipWHBSwitchRestrict)
                {

                    lblAdd2RestrictKey1.Text = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                    //lblRestrictAppIncomeValue5.Text = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT);
                    //lblAdd2RestrictKey2.Text = "When " + strTempLowerUserName + Constants.RestrictExplain1 + strStartAgeForLower + Constants.RestrictExplain2 + Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT)
                    //                                + Constants.RestrictExplain3 + cumm69.ToString(Constants.AMT_FORMAT);

                    lblAdd2RestrictKey2.Text = Constants.RestrictExplainNew1 + strTempLowerUserName + Constants.RestrictExplainNew2 + strTempLowerUserName + Constants.RestrictExplainNew3 + strStartAgeForLower + Constants.RestrictExplainNew4 + Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT) + Constants.RestrictExplainNew5;
                    Session["Add2RestrictAppIncome"] = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT);

                }

                //notes for husband and wife
                if (Globals.Instance.BoolHusbandAgeAdjusted)
                {
                    if (Globals.Instance.BoolSpousalWHBat70 && Globals.Instance.HusbAnnualIcomeAt70 != 0 && Globals.Instance.HusbAnnualIcomeAt69 != 0)//husband age is smaller than 70 when spouse is age 66 (FRA) // Added on 29/09/2016
                        lblAdd2NoteHusb1.Text = "# includes " + Globals.Instance.HusbSpousalBenMonthAt69 + Constants.strSpousal_WHB1 + Globals.Instance.HusbAnnualIcomeAt69.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB2 + Globals.Instance.HusbAnnualIcomeAt70.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB3 + strUserFirstName;
                    else
                        lblAdd2NoteHusb1.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                }
                if (Globals.Instance.BoolWifeAgeAdjusted)
                    lblAdd2NoteWife1.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                    lblAdd2NoteHusb2.Text = Constants.includes + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                    lblAdd2NoteWife2.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //notes for husband and wife
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblAdd2NoteHusb2.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblAdd2NoteHusb3.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                }
                else if (Globals.Instance.BoolWHBSwitchedWife)
                {
                    lblAdd2NoteWife2.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                    lblAdd2NoteWife3.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                }
                #endregion Key and Explaination


                #region Code to restrict app steps

                if (decBeneHusb >= decBeneWife)
                    lblAdd2Step1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                else
                    lblAdd2Step1.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);

                if (!Globals.Instance.BoolSkipWHBSwitchRestrict)
                {
                    if (!Globals.Instance.BoolNoClaimRestrictForAdd2)
                    {
                        lblAdd2Step3.Text = string.Format(Constants.Line2 + Constants.Age + " " + strSpousalClaimedAgeOfLower + Constants.when + strTempLowerUserName + Constants.steps8 + strStartAgeForLower + Constants.steps11 + strSpusalValueForLower + strStartColumnForLower + Constants.steps13 + strTempLowerUserName + Constants.steps14 + Constants.SpousalText);
                        if (age70valueLower > age70valueHigher)
                            lblAdd2Step4.Text = string.Format(Constants.Line3 + Constants.Age + " " + str70SwitchAgeofLower + Constants.when + strTempLowerUserName + Constants.steps8 + "70" + Constants.steps18 + strValue70ForLower + strStartColumnForLower + Constants.steps15 + Constants.Age70Text);
                        else
                            lblAdd2Step4.Text = string.Format(Constants.Line3 + Constants.Age + " " + str70SwitchAgeofLower + Constants.when + strTempLowerUserName + Constants.steps8 + "70" + Constants.steps18 + strValue70ForLower + strStartColumnForLower);
                        if (RestrictAppIncomeProp.Instance.boolSwitchToWorkHistory)
                        {
                            lblAdd2Step5.Text = string.Format(Constants.Line4 + Constants.Age + " " + str70SwitchAgeofLower + Constants.when + strTempHigherUserName + Constants.steps8 + strValue70ForHigher + Constants.steps1 + RestrictAppIncomeProp.Instance.strSwichdWorkValue + strColumnForHigher);
                            lblAdd2Step6.Text = string.Format(Constants.Line5 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                        else
                        {
                            lblAdd2Step5.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            lblAdd2Step6.Text = string.Empty;
                        }
                    }
                    else
                    {
                        if (age70valueLower > age70valueHigher)
                            lblAdd2Step4.Text = string.Format(Constants.Line2 + Constants.Age + " " + str70SwitchAgeofLower + Constants.when + strTempLowerUserName + Constants.steps8 + "70" + Constants.steps23 + strValue70ForLower + strStartColumnForLower + Constants.steps15 + Constants.Age70Text);
                        else
                            lblAdd2Step4.Text = string.Format(Constants.Line2 + Constants.Age + " " + str70SwitchAgeofLower + Constants.when + strTempLowerUserName + Constants.steps8 + "70" + Constants.steps23 + strValue70ForLower + strStartColumnForLower);

                        if (RestrictAppIncomeProp.Instance.boolSwitchToWorkHistory)
                        {
                            lblAdd2Step5.Text = string.Format(Constants.Line3 + Constants.Age + " " + str70SwitchAgeofLower + Constants.when + strTempHigherUserName + Constants.steps8 + strValue70ForHigher + Constants.steps1 + RestrictAppIncomeProp.Instance.strSwichdWorkValue + strColumnForHigher);
                            lblAdd2Step6.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                        }
                        else
                        {
                            lblAdd2Step5.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                            lblAdd2Step6.Text = string.Empty;
                        }
                    }
                }
                else
                {
                    lblAdd2Step3.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strTempLowerUserName + Constants.steps8 + strStartAgeForLower + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + strStartColumnForLower + ".");
                    lblAdd2Step5.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    lblRestrictAppIncomeTitle5.Visible = false;
                }

                #endregion Code to restrict app steps

                //Set Keys and Steps values
                //SetKeyAndStepsValuesForPDF("Full");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to print the keys and steps to the grid bestlater ie strategy Full retirement
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        public void renderKeysIntoGridNoClaim62(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, string ClaimAndSuspendinFullAge, decimal ClaimAndSuspendinFullValue, string CombinedIncomeAge, string AgeForHusb70, decimal ValueForHusb70, string AgeForWife70, decimal ValueForWife70, decimal AgeValueForSpousalAt70, decimal ValueForWifeAt70)
        {
            try
            {
                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName, sWifeFRA, sHusbFRA = string.Empty;
                string strTempHigherUserName = string.Empty, strTempLowerUserName = string.Empty, strStartAgeForHigher = string.Empty, strStartAgeForLower = string.Empty, strStartColumnForLower = string.Empty;
                string strValue70ForLower = string.Empty, strValue70ForHigher = string.Empty, strAgeForLower = string.Empty, strColumnForHigher = string.Empty,
                       strLastAgeForHigher = string.Empty, str70SwitchAgeofLower = string.Empty, strHighlitedColForLower = string.Empty, strHighlitedColFOrHigher = string.Empty,
                       strSpousalClaimedAgeOfLower = string.Empty, strSpusalValueForLower = string.Empty;
                string strColumnName = string.Empty;
                string strHusbStep = string.Empty, strWifeStep = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                decimal age70valueHigher = 0, age70valueLower = 0;
                DateTime HusbDOB = DateTime.Parse(BirthHusbandYYYY.Text.ToString());
                DateTime WifeDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strDoubleAsteriskIncludes = string.Empty;
                if (Globals.Instance.WifeNumberofMonthsinSteps > 0 && Globals.Instance.WifeNumberofMonthsinSteps != 12)
                    strageWife = Globals.Instance.WifeNumberofYearsAbove66 + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                else
                    strageWife = ageWife.Substring(0, 2);
                if (Globals.Instance.HusbandNumberofMonthsinSteps > 0 && Globals.Instance.HusbandNumberofMonthsinSteps != 12)
                    strageHusb = Globals.Instance.HusbNumberofYearsAbove66 + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                else
                    strageHusb = ageHusb.Substring(3, 2);

                //To show maxed out benefit at key section
                if (decBeneHusb < decBeneWife)
                {
                    Globals.Instance.IntShowRestrictIncomeForFull = Globals.Instance.intRestrictAppIncomeFullWife;
                    strTempHigherUserName = strUserFirstName;
                    strTempLowerUserName = strUserSpouseFirstName;
                    //strStartAgeForHigher = ageHusb.Substring(3, 2).ToString();
                    //strStartAgeForLower = ageWife.Substring(0, 2).ToString();
                    strStartAgeForHigher = strageHusb;
                    strStartAgeForLower = strageWife;
                    strStartColumnForLower = Constants.steps10;
                    strValue70ForLower = ValueForWife70.ToString(Constants.AMT_FORMAT);
                    strColumnForHigher = Constants.steps12;
                    if (Globals.Instance.HusbNumberofMonthsAboveGrid != 0)
                        strValue70ForHigher = Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months;
                    else
                        strValue70ForHigher = AgeForWife70.Substring(3, 2);
                    str70SwitchAgeofLower = AgeForWife70;
                    strHighlitedColForLower = Constants.keyFigures14;
                    strSpousalClaimedAgeOfLower = ageWife;
                    strSpusalValueForLower = startingValueWife.ToString(Constants.AMT_FORMAT);
                    age70valueHigher = ValueForHusb70;
                    age70valueLower = ValueForWife70;
                }
                else
                {
                    Globals.Instance.IntShowRestrictIncomeForFull = Globals.Instance.intRestrictAppIncomeFullHusb;
                    strTempHigherUserName = strUserSpouseFirstName;
                    strTempLowerUserName = strUserFirstName;
                    //strStartAgeForHigher = ageWife.Substring(0, 2).ToString();
                    //strStartAgeForLower = ageHusb.Substring(3, 2).ToString();
                    strStartAgeForHigher = strageWife;
                    strStartAgeForLower = strageHusb;
                    strStartColumnForLower = Constants.steps12;
                    strValue70ForLower = ValueForHusb70.ToString(Constants.AMT_FORMAT);
                    strColumnForHigher = Constants.steps10;
                    if (Globals.Instance.WifeNumberofMonthsAboveGrid != 0)
                        strValue70ForHigher = Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months;
                    else
                        strValue70ForHigher = AgeForHusb70.Substring(0, 2);

                    str70SwitchAgeofLower = AgeForHusb70;
                    strHighlitedColForLower = Constants.keyFigures13;
                    strSpousalClaimedAgeOfLower = ageHusb;
                    strSpusalValueForLower = startingValueHusb.ToString(Constants.AMT_FORMAT);
                    age70valueHigher = ValueForWife70;
                    age70valueLower = ValueForHusb70;
                }


                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneHusband.Text) >= Convert.ToDecimal(BeneWife.Text))
                {
                    strName = strUserSpouseFirstName;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnName = Constants.keyFigures13;
                }

                #region Key and Explaination
                //to check if he cummulative income is not zero
                if (cumm69 != 0)
                {
                    lblKey1NoClaim62.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + strValue70ForLower + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
                    lblKey1NoClaim62.Visible = true;
                }
                else
                {
                    lblKey1NoClaim62.Visible = false;
                }
                //Add maxed out work history benefit keys
                lblKey2NoClaim62.Text = string.Format(strValue70ForLower + strHighlitedColForLower + Constants.steps7 + strTempLowerUserName + Constants.keyFigures8);

                //Add Combined benefit keys
                if (valueAtAge70 != 0)
                    lblKey3NoClaim62.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);
                else
                    lblKey3NoClaim62.Text = string.Empty;

                //Add Extra Income Keys
                //if (!Globals.Instance.BoolSkipWHBSwitchRestrict)
                //{

                //    lblAdd1RestrictKey1.Text = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                //    lblRestrictAppIncomeValue4.Text = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT);
                //    lblAdd1RestrictKey2.Text = "When " + strTempLowerUserName + Constants.RestrictExplain1 + strStartAgeForLower + Constants.RestrictExplain2 + Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT)
                //                                    + Constants.RestrictExplain3 + cumm69.ToString(Constants.AMT_FORMAT);
                //    lblRestrictAppIncomeValue4.Visible = true;
                //}

                //notes for husband and wife
                if (Globals.Instance.BoolHusbandAgeAdjusted)
                {
                    if (Globals.Instance.BoolSpousalWHBat70 && Globals.Instance.HusbAnnualIcomeAt70 != 0 && Globals.Instance.HusbAnnualIcomeAt69 != 0)//husband age is smaller than 70 when spouse is age 66 (FRA) // Added on 29/09/2016
                        lblNote1HusbClaim62.Text = "# includes " + Globals.Instance.HusbSpousalBenMonthAt69 + Constants.strSpousal_WHB1 + Globals.Instance.HusbAnnualIcomeAt69.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB2 + Globals.Instance.HusbAnnualIcomeAt70.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB3 + strUserFirstName;
                    else
                        lblNote1HusbClaim62.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                }
                if (Globals.Instance.BoolWifeAgeAdjusted)
                    lblNote1WifeClaim62.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                    lblNote2HusbClaim62.Text = Constants.includes + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                    lblNote2WifeClaim62.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //notes for husband and wife
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblNote2HusbClaim62.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblNote3HusbClaim62.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                }
                else if (Globals.Instance.BoolWHBSwitchedWife)
                {
                    lblNote2WifeClaim62.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                    lblNote3WifeClaim62.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                }
                #endregion Key and Explaination


                #region Code to restrict app steps

                if (decBeneHusb < decBeneWife)
                    lblStep1NoClaim62.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + "62" + Constants.ReducedWorkBenefit + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                else
                    lblStep1NoClaim62.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + "62" + Constants.ReducedWorkBenefit + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);

                if (!Globals.Instance.BoolSkipWHBSwitchRestrict)
                {
                    // lblStep3NoClaim62.Text = string.Format(Constants.Line2 + Constants.Age + " " + strSpousalClaimedAgeOfLower + Constants.when + strTempLowerUserName + Constants.steps8 + strStartAgeForLower + Constants.steps11 + strSpusalValueForLower + strStartColumnForLower + Constants.steps13 + strTempLowerUserName + Constants.steps14 + Constants.SpousalText);
                    if (age70valueLower > age70valueHigher)
                        lblStep4NoClaim62.Text = string.Format(Constants.Line2 + Constants.Age + " " + str70SwitchAgeofLower + Constants.when + strTempLowerUserName + Constants.steps8 + "70" + Constants.steps23 + strValue70ForLower + strStartColumnForLower + Constants.steps15 + Constants.Age70Text);
                    else
                        lblStep4NoClaim62.Text = string.Format(Constants.Line2 + Constants.Age + " " + str70SwitchAgeofLower + Constants.when + strTempLowerUserName + Constants.steps8 + "70" + Constants.steps23 + strValue70ForLower + strStartColumnForLower);
                    //if (RestrictAppIncomeProp.Instance.boolSwitchToWorkHistory)
                    //{
                    lblStep5NoClaim62.Text = string.Format(Constants.Line3 + Constants.Age + " " + str70SwitchAgeofLower + Constants.when + strTempHigherUserName + Constants.steps8 + strStartAgeForHigher + Constants.spousalswitch + RestrictAppIncomeProp.Instance.strSwichdWorkValue + strColumnForHigher);
                    lblStep6NoClaim62.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    //}
                    //else
                    //{
                    //    lblStep5NoClaim62.Text = string.Format(Constants.Line4 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    //    lblStep6NoClaim62.Text = string.Empty;
                    //}
                }
                else
                {
                    //lblStep3NoClaim62.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strTempLowerUserName + Constants.steps8 + strStartAgeForLower + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + strStartColumnForLower + ".");
                    lblStep5NoClaim62.Text = string.Format(Constants.Line2 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                    lblRestrictAppIncomeTitle4.Visible = false;
                }

                #endregion Code to restrict app steps

                //Set Keys and Steps values
                //SetKeyAndStepsValuesForPDF("Full");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// code to print the keys and steps to the grid bestlater ie strategy Full retirement
        /// </summary>
        /// <param name="cumm69"></param>
        /// <param name="valueAtAge70"></param>
        /// <param name="combineBenefits"></param>
        /// <param name="ageHusb"></param>
        /// <param name="ageWife"></param>
        /// <param name="startingValueHusb"></param>
        /// <param name="startingValueWife"></param>
        /// <param name="ageForHigherEarnerAt70"></param>
        /// <param name="valueforHIgherEarnerAt70"></param>
        public void renderKeysIntoGridNoClaim66(decimal cumm69, decimal valueAtAge70, decimal combineBenefits, string ageHusb, string ageWife, decimal startingValueHusb, decimal startingValueWife, string ageForHigherEarnerAt70, decimal valueforHIgherEarnerAt70, string ClaimAndSuspendinFullAge, decimal ClaimAndSuspendinFullValue, string CombinedIncomeAge, string AgeForHusb70, decimal ValueForHusb70, string AgeForWife70, decimal ValueForWife70, decimal AgeValueForSpousalAt70, decimal ValueForWifeAt70)
        {
            try
            {
                Calc calc = new Calc();
                Customers customer = new Customers();
                string strName, sWifeFRA, sHusbFRA = string.Empty;
                string strTempHigherUserName = string.Empty, strTempLowerUserName = string.Empty, strStartAgeForHigher = string.Empty, strStartAgeForLower = string.Empty, strStartColumnForLower = string.Empty;
                string strValue70ForLower = string.Empty, strValue70ForHigher = string.Empty, strAgeForLower = string.Empty, strColumnForHigher = string.Empty,
                       strLastAgeForHigher = string.Empty, str70SwitchAgeofLower = string.Empty, strHighlitedColForLower = string.Empty, strHighlitedColFOrHigher = string.Empty,
                       strSpousalClaimedAgeOfLower = string.Empty, strSpusalValueForLower = string.Empty;
                string strColumnName = string.Empty;
                string strHusbStep = string.Empty, strWifeStep = string.Empty;
                string strageHusb = ageHusb.Substring(3, 2);
                string strageWife = ageWife.Substring(0, 2);
                decimal age70valueHigher = 0, age70valueLower = 0;
                DateTime HusbDOB = DateTime.Parse(BirthHusbandYYYY.Text.ToString());
                DateTime WifeDOB = DateTime.Parse(BirthWifeYYYY.Text.ToString());
                DateTime limit = DateTime.Parse("05/01/1950");
                DateTime limit1 = DateTime.Parse("01/01/1954");
                sWifeFRA = customer.FRA(Convert.ToInt32(birthWifeTempYYYY.Text));
                sHusbFRA = customer.FRA(Convert.ToInt32(BirthHusbandTempYYYY.Text));
                int intWifeFRA = Convert.ToInt32(strageWife.Substring(0, 2));
                int intHusbFRA = Convert.ToInt32(strageHusb.Substring(0, 2));
                string strDoubleAsteriskIncludes = string.Empty;
                if (Globals.Instance.WifeNumberofMonthinNote > 0)
                    strageWife = Globals.Instance.WifeNumberofYearsAbove66 + Constants.years + Globals.Instance.WifeNumberofMonthsinSteps + Constants.months;
                else
                    strageWife = ageWife.Substring(0, 2);
                if (Globals.Instance.HusbandNumberofMonthsinNote > 0)
                    strageHusb = Globals.Instance.HusbNumberofYearsAbove66 + Constants.years + Globals.Instance.HusbandNumberofMonthsinSteps + Constants.months;
                else
                    strageHusb = ageHusb.Substring(3, 2);

                //To show maxed out benefit at key section
                if (decBeneHusb < decBeneWife)
                {
                    Globals.Instance.IntShowRestrictIncomeForFull = Globals.Instance.intRestrictAppIncomeFullWife;
                    strTempHigherUserName = strUserFirstName;
                    strTempLowerUserName = strUserSpouseFirstName;
                    strStartAgeForHigher = ageHusb.Substring(3, 2);
                    strStartAgeForLower = strageWife;
                    strStartColumnForLower = Constants.steps10;
                    strValue70ForLower = ValueForWife70.ToString(Constants.AMT_FORMAT);
                    strColumnForHigher = Constants.steps12;
                    //if (Globals.Instance.HusbNumberofMonthsAboveGrid != 0)
                    //    strValue70ForHigher = Globals.Instance.HusbandNumberofYears + Constants.years + Globals.Instance.HusbNumberofMonthsAboveGrid + Constants.months;
                    //else
                    //    strValue70ForHigher = AgeForWife70.Substring(3, 2);
                    strValue70ForHigher = strageHusb;
                    str70SwitchAgeofLower = AgeForWife70;
                    strHighlitedColForLower = Constants.keyFigures14;
                    strSpousalClaimedAgeOfLower = ageWife;
                    strSpusalValueForLower = startingValueWife.ToString(Constants.AMT_FORMAT);
                    age70valueHigher = ValueForHusb70;
                    age70valueLower = ValueForWife70;
                }
                else
                {
                    Globals.Instance.IntShowRestrictIncomeForFull = Globals.Instance.intRestrictAppIncomeFullHusb;
                    strTempHigherUserName = strUserSpouseFirstName;
                    strTempLowerUserName = strUserFirstName;
                    strStartAgeForHigher = ageWife.Substring(0, 2).ToString();
                    strStartAgeForLower = strageHusb;
                    strStartColumnForLower = Constants.steps12;
                    strValue70ForLower = ValueForHusb70.ToString(Constants.AMT_FORMAT);
                    strColumnForHigher = Constants.steps10;
                    //if (Globals.Instance.WifeNumberofMonthsAboveGrid != 0)
                    //    strValue70ForHigher = Globals.Instance.WifeNumberofYears + Constants.years + Globals.Instance.WifeNumberofMonthsAboveGrid + Constants.months;
                    //else
                    //    strValue70ForHigher = AgeForHusb70.Substring(0, 2);
                    strValue70ForHigher = strageWife;
                    str70SwitchAgeofLower = AgeForHusb70;
                    strHighlitedColForLower = Constants.keyFigures13;
                    strSpousalClaimedAgeOfLower = ageHusb;
                    strSpusalValueForLower = startingValueHusb.ToString(Constants.AMT_FORMAT);

                    age70valueHigher = ValueForWife70;
                    age70valueLower = ValueForHusb70;
                }


                //loop to set the name and cloumn name of the user and spouse as per their benefits
                //this is set to show thw higher earner's name and corresponding column
                if (Convert.ToDecimal(BeneHusband.Text) >= Convert.ToDecimal(BeneWife.Text))
                {
                    strName = strUserSpouseFirstName;
                    strColumnName = Constants.keyFigures14;
                }
                else
                {
                    strName = strUserFirstName;
                    strColumnName = Constants.keyFigures13;
                }

                #region Key and Explaination
                //to check if he cummulative income is not zero
                if (cumm69 != 0)
                {
                    lblKey1NoClaim66.Text = string.Format(cumm69.ToString(Constants.AMT_FORMAT) + Constants.keyFigures12 + Constants.steps7 + Constants.keyFigures6 + strUserFirstName + Constants.And + strUserSpouseFirstName + Constants.keyFigures17 + Constants.CummuText1 + strName + Constants.CummuText2 + strValue70ForLower + Constants.CummuText3 + Constants.CummuText4 + cumm69.ToString(Constants.AMT_FORMAT) + Constants.CummuText5);
                    lblKey1NoClaim66.Visible = true;
                }
                else
                {
                    lblKey1NoClaim66.Visible = false;
                }
                //Add maxed out work history benefit keys
                lblKey2NoClaim66.Text = string.Format(strValue70ForLower + strHighlitedColForLower + Constants.steps7 + strTempLowerUserName + Constants.keyFigures8);

                //Add Combined benefit keys
                if (valueAtAge70 != 0)
                    lblKey3NoClaim66.Text = string.Format(valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.keyFigures11 + Constants.steps7 + Constants.keyFigures9);
                else
                    lblKey3NoClaim66.Text = string.Empty;

                //Add Extra Income Keys
                //if (!Globals.Instance.BoolSkipWHBSwitchRestrict)
                //{

                //    lblAdd2RestrictKey1.Text = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT) + Constants.RestrictedAppIcomeText;
                //    lblRestrictAppIncomeValue5.Text = Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT);
                //    lblAdd2RestrictKey2.Text = "When " + strTempLowerUserName + Constants.RestrictExplain1 + strStartAgeForLower + Constants.RestrictExplain2 + Globals.Instance.IntShowRestrictIncomeForFull.ToString(Constants.AMT_FORMAT)
                //                                    + Constants.RestrictExplain3 + cumm69.ToString(Constants.AMT_FORMAT);
                //    lblRestrictAppIncomeValue5.Visible = true;
                //}

                //notes for husband and wife
                if (Globals.Instance.BoolHusbandAgeAdjusted)
                {
                    if (Globals.Instance.BoolSpousalWHBat70 && Globals.Instance.HusbAnnualIcomeAt70 != 0 && Globals.Instance.HusbAnnualIcomeAt69 != 0)//husband age is smaller than 70 when spouse is age 66 (FRA) // Added on 29/09/2016
                        lblNote1HusbClaim66.Text = "# includes " + Globals.Instance.HusbSpousalBenMonthAt69 + Constants.strSpousal_WHB1 + Globals.Instance.HusbAnnualIcomeAt69.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB2 + Globals.Instance.HusbAnnualIcomeAt70.ToString(Constants.AMT_FORMAT) + " per month " + Constants.strSpousal_WHB3 + strUserFirstName;
                    else
                        lblNote1HusbClaim66.Text = "# includes " + Globals.Instance.HusbandNumberofMonthsinNote + Constants.strNote1 + strUserFirstName;
                }
                if (Globals.Instance.BoolWifeAgeAdjusted)
                    lblNote1WifeClaim66.Text = "# includes " + Globals.Instance.WifeNumberofMonthinNote + Constants.strNote1 + strUserSpouseFirstName;
                if (Globals.Instance.BoolWHBSingleSwitchedHusb)
                    lblNote2HusbClaim66.Text = Constants.includes + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                else if (Globals.Instance.BoolWHBSingleSwitchedWife)
                    lblNote2WifeClaim66.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                //notes for husband and wife
                if (Globals.Instance.BoolWHBSwitchedHusb)
                {
                    lblNote2HusbClaim66.Text = Constants.includes + Globals.Instance.HusbNumberofMonthsBelowGrid + Constants.strNote1 + strUserFirstName;
                    lblNote3HusbClaim66.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserFirstName;
                }
                else if (Globals.Instance.BoolWHBSwitchedWife)
                {
                    lblNote2WifeClaim66.Text = Constants.includes + Globals.Instance.WifeNumberofMonthsBelowGrid + Constants.strNote1 + strUserSpouseFirstName;
                    lblNote3WifeClaim66.Text = Constants.DoubleAsterisk + " includes " + Globals.Instance.WifeNumberofMonthsBelowGridAgePrior + Constants.strNote1 + strUserSpouseFirstName;
                }
                #endregion Key and Explaination


                #region Code to restrict app steps
                if (RestrictAppIncomeProp.Instance.boolClaimToWorkHistory)
                {
                    if (decBeneHusb < decBeneWife)
                        lblStep1NoClaim66.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageHusb + Constants.when + strUserFirstName + Constants.steps8 + sHusbFRA + Constants.steps9 + startingValueHusb.ToString(Constants.AMT_FORMAT) + Constants.steps12);
                    else
                        lblStep1NoClaim66.Text = string.Format(Constants.Line1 + Constants.Age + " " + ageWife + Constants.when + strUserSpouseFirstName + Constants.steps8 + sWifeFRA + Constants.steps9 + startingValueWife.ToString(Constants.AMT_FORMAT) + Constants.steps10);
                }
                if (!Globals.Instance.BoolSkipWHBSwitchRestrict)
                {
                    lblStep4NoClaim66.Text = string.Format(Constants.Line1 + Constants.Age + " " + str70SwitchAgeofLower + Constants.when + strTempHigherUserName + Constants.steps8 + strValue70ForHigher + Constants.spousalswitch + RestrictAppIncomeProp.Instance.strSwichdWorkValue + strColumnForHigher);
                    if (age70valueLower > age70valueHigher)
                        lblStep5NoClaim66.Text = string.Format(Constants.Line2 + Constants.Age + " " + str70SwitchAgeofLower + Constants.when + strTempLowerUserName + Constants.steps8 + "70" + Constants.steps23 + strValue70ForLower + strStartColumnForLower + Constants.steps15 + Constants.Age70Text);
                    else
                        lblStep5NoClaim66.Text = string.Format(Constants.Line2 + Constants.Age + " " + str70SwitchAgeofLower + Constants.when + strTempLowerUserName + Constants.steps8 + "70" + Constants.steps23 + strValue70ForLower + strStartColumnForLower);
                    lblStep6NoClaim66.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);

                }
                else
                {
                    lblStep3NoClaim66.Text = string.Format(Constants.Line2 + Constants.Age + " " + ageWife + Constants.when + strTempLowerUserName + Constants.steps8 + strStartAgeForLower + Constants.steps1 + startingValueWife.ToString(Constants.AMT_FORMAT) + strStartColumnForLower + ".");
                    lblStep4NoClaim66.Text = string.Format(Constants.Line3 + Constants.Age + " " + CombinedIncomeAge + Constants.steps7 + Constants.steps16 + valueAtAge70.ToString(Constants.AMT_FORMAT) + Constants.steps17);
                }

                #endregion Code to restrict app steps

                //Set Keys and Steps values
                //SetKeyAndStepsValuesForPDF("Full");
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

        /// <summary>
        /// to reset the annual Income variables after each startegy
        /// </summary>
        public void ResetAnualIncomeVariables()
        {
            try
            {
                Globals.Instance.BoolWHBSingleSwitchedWife = false;
                Globals.Instance.BoolHusbandAgeAdjusted = false;
                Globals.Instance.BoolWifeAgeAdjusted = false;
                Globals.Instance.HusbandNumberofMonthsinNote = 0;
                Globals.Instance.WifeNumberofMonthinNote = 0;
                Globals.Instance.WifeNumberofMonthsinSteps = 0;
                Globals.Instance.HusbandNumberofMonthsinSteps = 0;
                Globals.Instance.HusbNumberofMonthsAboveGrid = 0;
                Globals.Instance.WifeNumberofMonthsAboveGrid = 0;
                Globals.Instance.HusbandNumberofYears = 0;
                Globals.Instance.WifeNumberofYears = 0;
                Globals.Instance.BoolWHBSwitchedHusb = false;
                Globals.Instance.BoolWHBSwitchedWife = false;
                Globals.Instance.HusbNumberofMonthsBelowGrid = 0;
                Globals.Instance.WifeNumberofMonthsBelowGrid = 0;
                Globals.Instance.WifeOriginalAnnualIncome = 0;
                Globals.Instance.HusbandOriginalAnnualIncome = 0;
                Globals.Instance.HusbandNumberofMonthsBelowGridAgePrior = 0;
                Globals.Instance.WifeNumberofMonthsBelowGridAgePrior = 0;
                Globals.Instance.BoolSpousalAfterWorkWife = false;
                Globals.Instance.BoolSpousalAfterWorkHusb = false;
                Globals.Instance.BoolWHBSingleSwitchedHusb = false;
                Globals.Instance.HusbNumberofYearsAbove66 = 0;
                Globals.Instance.WifeNumberofYearsAbove66 = 0;
                Globals.Instance.BoolSpousalAtFRAAgeCliamedWife = false;
                Globals.Instance.BoolSpousalWHBat70 = false;
                Globals.Instance.HusbSpousalBenMonthAt69 = 0;
                Globals.Instance.HusbAnnualIcomeAt69 = 0;
                Globals.Instance.HusbAnnualIcomeAt70 = 0;
                Globals.Instance.HusbSpousalBenefitWhenWife66 = 0;
                RestrictAppIncomeProp.Instance.boolSwitchToWorkHistory = false;
                RestrictAppIncomeProp.Instance.strSwichdWorkValue = string.Empty;
                Globals.Instance.BoolRestrictSpousalAt66 = false;
                Globals.Instance.BoolRestrictSpousalAt62 = false;
                RestrictAppIncomeProp.Instance.strSwichdWorkValue = string.Empty;


            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorCalculate, MethodBase.GetCurrentMethod(), ex.ToString()));
                ErrorMessagelable.Text = Constants.ErrorCalculate + MethodBase.GetCurrentMethod() + ex.Message;
                ErrorMessagelable.Visible = true;
            }
        }

    }
}