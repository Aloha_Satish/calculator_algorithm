﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Calculate.RestrictAppIncome
{
    public class RestrictAppIncomeProp
    {

        private static RestrictAppIncomeProp _instance = null;
        public static RestrictAppIncomeProp Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new RestrictAppIncomeProp();
                return _instance;
            }
        }



        private int _intHusbandClaimAge = 0;

        public int intHusbandClaimAge
        {
            get
            {
                return _intHusbandClaimAge;
            }
            set
            {
                _intHusbandClaimAge = value;
            }
        }
        private int _intWifeClaimAge = 0;

        public int intWifeClaimAge
        {
            get
            {
                return _intWifeClaimAge;
            }
            set
            {
                _intWifeClaimAge = value;
            }
        }

        private int _intWifeClaimBenefit = 0;
        public int intWifeClaimBenefit
        {
            get
            {
                return _intWifeClaimBenefit;
            }
            set
            {
                _intWifeClaimBenefit = value;
            }
        }

        private int _intHusbClaimBenefit = 0;
        public int intHusbClaimBenefit
        {
            get
            {
                return _intHusbClaimBenefit;
            }
            set
            {
                _intHusbClaimBenefit = value;
            }
        }
        private int _intHusbCurrentBenefit = 0;
        public int intHusbCurrentBenefit
        {
            get
            {
                return _intHusbCurrentBenefit;
            }
            set
            {
                _intHusbCurrentBenefit = value;
            }
        }

        private int _intWifeCurrentBenefit = 0;
        public int intWifeCurrentBenefit
        {
            get
            {
                return _intWifeCurrentBenefit;
            }
            set
            {
                _intWifeCurrentBenefit = value;
            }
        }

        private bool _boolSwitchToWorkHistory;
        public bool boolSwitchToWorkHistory
        {
            get
            {
                return _boolSwitchToWorkHistory;
            }
            set
            {
                _boolSwitchToWorkHistory = value;
            }
        }

        private bool _boolClaimToWorkHistory;
        public bool boolClaimToWorkHistory
        {
            get
            {
                return _boolClaimToWorkHistory;
            }
            set
            {
                _boolClaimToWorkHistory = value;
            }
        }


        private string _strSwichdWorkValue;
        public string strSwichdWorkValue 
        {
            get
            {
                return _strSwichdWorkValue;
            }
            set
            {
                _strSwichdWorkValue = value;
            }
        }

        /// <summary>
        /// Check is Restricted special case available or not
        /// </summary>
        private bool m_BoolRestrictSpecialCase = false;
        public bool BoolRestrictSpecialCase 
        {
            get
            {
                return m_BoolRestrictSpecialCase;
            }
            set
            {
                m_BoolRestrictSpecialCase = value;
            }
        }


        /// <summary>
        /// Check is Restricted New case available or not
        /// When neither spouse claimed and not satisfying any case.
        /// </summary>
        private bool m_BoolRestrictNewCase = false;
        public bool BoolRestrictNewCase
        {
            get
            {
                return m_BoolRestrictNewCase;
            }
            set
            {
                m_BoolRestrictNewCase = value;
            }
        }


    }
}