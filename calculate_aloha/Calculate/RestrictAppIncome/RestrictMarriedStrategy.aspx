﻿<%@ Page Title="Restricted Application Married Strategy" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="RestrictMarriedStrategy.aspx.cs" Inherits="Calculate.RestrictAppIncome.RestrictMarriedStrategy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/blockUI.min.js"></script>
    <script type="text/javascript" src="../Scripts/IRR.js"></script>
    <link href="../css/calculator.css" rel="stylesheet" />
    <script type="text/javascript" src="../Scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.blockUI.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="images/ssc.ico" />
    <script type="text/javascript">
        function showOneStrategy(sFirstStartegy) {
            $("#ctl00_MainContent_strategy1").css('display', 'none');
            $("#ctl00_MainContent_strategy3").css('display', 'none');
            $("#ctl00_MainContent_strategy4").css('display', 'none');
            $("#ctl00_MainContent_strategy5").css('display', 'none');
            $("#ctl00_MainContent_lblStrategy2").text("Strategy 1");
            $("#ctl00_MainContent_rdbtnStrategy2").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "black");
                $("#ctl00_MainContent_lblStrategy2").css("color", "brown");
                $("#ctl00_MainContent_lblStrategy3").css("color", "black");
                $("#strategy-" + sFirstStartegy).css('display', 'block');
            });


            $("span:empty").css("display", "none");

        }

        function showTwoStrategy(sFirstStartegy, sThirdStartegy) {
            $("#ctl00_MainContent_strategy2").css('display', 'none');
            $("#ctl00_MainContent_lblStrategy3").text("Strategy 2");
            $("#ctl00_MainContent_rdbtnStrategy1").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "brown");
                $("#ctl00_MainContent_lblStrategy2").css("color", "black");
                $("#ctl00_MainContent_lblStrategy3").css("color", "black");
                $('.strategy-container').css('display', 'none');
                $('#strategy-' + sFirstStartegy).css('display', 'block');
            });
            $("#ctl00_MainContent_rdbtnStrategy3").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "black");
                $("#ctl00_MainContent_lblStrategy2").css("color", "black");
                $("#ctl00_MainContent_lblStrategy3").css("color", "brown");
                $('.strategy-container').css('display', 'none');
                $('#strategy-' + sThirdStartegy).css('display', 'block');
            });

            $("span:empty").css("display", "none");

        }

        function showThreeStrategy(sFirstStartegy, sSecondStartegy, sThirdStartegy) {
            //$("#ctl00_MainContent_strategy4").css('display', 'none');
            //$("#ctl00_MainContent_strategy5").css('display', 'none');
            $("#ctl00_MainContent_lblStrategy2").text("Strategy 2");
            $("#ctl00_MainContent_lblStrategy3").text("Strategy 3");

            $("#ctl00_MainContent_rdbtnStrategy1").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "brown");
                $("#ctl00_MainContent_lblStrategy2").css("color", "black");
                $("#ctl00_MainContent_lblStrategy3").css("color", "black");
                $(".strategy-container").css('display', 'none');
                $("#strategy-" + sFirstStartegy).css('display', 'block');
            });
            $("#ctl00_MainContent_rdbtnStrategy2").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "black");
                $("#ctl00_MainContent_lblStrategy2").css("color", "brown");
                $("#ctl00_MainContent_lblStrategy3").css("color", "black");
                $(".strategy-container").css('display', 'none');
                $("#strategy-" + sSecondStartegy).css('display', 'block');
            });

            $("#ctl00_MainContent_rdbtnStrategy3").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "black");
                $("#ctl00_MainContent_lblStrategy2").css("color", "black");
                $("#ctl00_MainContent_lblStrategy3").css("color", "brown");
                $(".strategy-container").css('display', 'none');
                $("#strategy-" + sThirdStartegy).css('display', 'block');
            });

            $("span:empty").css("display", "none");

        }


        function showFourStrategy(sFirstStartegy, sSecondStartegy, sThirdStartegy, sFourthStartegy) {
            $("#ctl00_MainContent_strategy5").css('display', 'none');
            $("#ctl00_MainContent_rdbtnStrategy1").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "brown");
                $("#ctl00_MainContent_lblStrategy2").css("color", "black");
                $("#ctl00_MainContent_lblStrategy3").css("color", "black");
                $("#ctl00_MainContent_lblStrategy4").css("color", "black");

                $(".strategy-container").css('display', 'none');
                $("#strategy-" + sFirstStartegy).css('display', 'block');
            });
            $("#ctl00_MainContent_rdbtnStrategy2").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "black");
                $("#ctl00_MainContent_lblStrategy2").css("color", "brown");
                $("#ctl00_MainContent_lblStrategy3").css("color", "black");
                $("#ctl00_MainContent_lblStrategy4").css("color", "black");

                $(".strategy-container").css('display', 'none');
                $("#strategy-" + sSecondStartegy).css('display', 'block');
            });

            $("#ctl00_MainContent_rdbtnStrategy3").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "black");
                $("#ctl00_MainContent_lblStrategy2").css("color", "black");
                $("#ctl00_MainContent_lblStrategy3").css("color", "brown");
                $("#ctl00_MainContent_lblStrategy4").css("color", "black");
                $("#ctl00_MainContent_lblStrategy5").css("color", "black");
                $(".strategy-container").css('display', 'none');
                $("#strategy-" + sThirdStartegy).css('display', 'block');
            });
            $("#ctl00_MainContent_rdbtnStrategy4").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "black");
                $("#ctl00_MainContent_lblStrategy2").css("color", "black");
                $("#ctl00_MainContent_lblStrategy3").css("color", "black");
                $("#ctl00_MainContent_lblStrategy4").css("color", "brown");

                $(".strategy-container").css('display', 'none');
                $("#strategy-" + sFourthStartegy).css('display', 'block');
            });


            $("span:empty").css("display", "none");

        }

        function showAdditionalTwo(sFirstStartegy, sSecondStartegy) {
            $("#ctl00_MainContent_lblStrategy3").text("Strategy 2");
            $("#ctl00_MainContent_strategy2").css('display', 'none');
            $("#ctl00_MainContent_rdbtnStrategy1").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "brown");
                $("#ctl00_MainContent_lblStrategy2").css("color", "black");
                $("#ctl00_MainContent_lblStrategy3").css("color", "black");
                $("#ctl00_MainContent_lblStrategy4").css("color", "black");
                $("#ctl00_MainContent_lblStrategy5").css("color", "black");
                $(".strategy-container").css('display', 'none');
                $("#strategy-" + sFirstStartegy).css('display', 'block');
            });
            $("#ctl00_MainContent_rdbtnStrategy3").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "black");
                $("#ctl00_MainContent_lblStrategy2").css("color", "black");
                $("#ctl00_MainContent_lblStrategy3").css("color", "brown");
                $("#ctl00_MainContent_lblStrategy4").css("color", "black");
                $("#ctl00_MainContent_lblStrategy5").css("color", "brown");
                $(".strategy-container").css('display', 'none');
                $("#strategy-" + sSecondStartegy).css('display', 'block');
            });

        }
        function showAdditionalOne(sFirstStartegy) {
            $("#ctl00_MainContent_strategy1").css('display', 'none');
            $("#ctl00_MainContent_strategy3").css('display', 'none');
            $("#ctl00_MainContent_lblStrategy2").text("Strategy 1");
            $("#ctl00_MainContent_rdbtnStrategy2").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "black");
                $("#ctl00_MainContent_lblStrategy2").css("color", "brown");
                $("#ctl00_MainContent_lblStrategy3").css("color", "black");
                $("#ctl00_MainContent_lblStrategy4").css("color", "black");
                $("#ctl00_MainContent_lblStrategy5").css("color", "black");
                $(".strategy-container").css('display', 'none');
                $("#strategy-" + sFirstStartegy).css('display', 'block');
            });
            $("span:empty").css("display", "none");
        }


        function showFiveStrategy(sFirstStartegy, sSecondStartegy, sThirdStartegy, sFourthStartegy, sFifthStartegy) {

            $("#ctl00_MainContent_rdbtnStrategy1").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "brown");
                $("#ctl00_MainContent_lblStrategy2").css("color", "black");
                $("#ctl00_MainContent_lblStrategy3").css("color", "black");
                $("#ctl00_MainContent_lblStrategy4").css("color", "black");
                $("#ctl00_MainContent_lblStrategy5").css("color", "black");
                $(".strategy-container").css('display', 'none');
                $("#strategy-" + sFirstStartegy).css('display', 'block');
            });
            $("#ctl00_MainContent_rdbtnStrategy2").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "black");
                $("#ctl00_MainContent_lblStrategy2").css("color", "brown");
                $("#ctl00_MainContent_lblStrategy3").css("color", "black");
                $("#ctl00_MainContent_lblStrategy4").css("color", "black");
                $("#ctl00_MainContent_lblStrategy5").css("color", "black");
                $(".strategy-container").css('display', 'none');
                $("#strategy-" + sSecondStartegy).css('display', 'block');
            });

            $("#ctl00_MainContent_rdbtnStrategy3").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "black");
                $("#ctl00_MainContent_lblStrategy2").css("color", "black");
                $("#ctl00_MainContent_lblStrategy3").css("color", "brown");
                $("#ctl00_MainContent_lblStrategy4").css("color", "black");
                $("#ctl00_MainContent_lblStrategy5").css("color", "black");
                $(".strategy-container").css('display', 'none');
                $("#strategy-" + sThirdStartegy).css('display', 'block');
            });
            $("#ctl00_MainContent_rdbtnStrategy4").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "black");
                $("#ctl00_MainContent_lblStrategy2").css("color", "black");
                $("#ctl00_MainContent_lblStrategy3").css("color", "black");
                $("#ctl00_MainContent_lblStrategy4").css("color", "brown");
                $("#ctl00_MainContent_lblStrategy5").css("color", "black");
                $(".strategy-container").css('display', 'none');
                $("#strategy-" + sFourthStartegy).css('display', 'block');
            });

            $("#ctl00_MainContent_rdbtnStrategy5").click(function () {
                $("#ctl00_MainContent_lblStrategy1").css("color", "black");
                $("#ctl00_MainContent_lblStrategy2").css("color", "black");
                $("#ctl00_MainContent_lblStrategy3").css("color", "black");
                $("#ctl00_MainContent_lblStrategy4").css("color", "black");
                $("#ctl00_MainContent_lblStrategy5").css("color", "brown");
                $(".strategy-container").css('display', 'none');
                $("#strategy-" + sFifthStartegy).css('display', 'block');
            });

            $("span:empty").css("display", "none");

        }

        function HideShowStrategy() {
            if ($('.additonalStrat').is(':visible')) {
                $('.additonalStrat').css('display', 'none');
                $("#strategy-Additional1").css('display', 'none');
                $("#strategy-Additional2").css('display', 'none');
                $('#btnHideShowStrat').val('Show Additional Strategies');
            }
            else {
                $('.additonalStrat').css('display', 'block');
                $('#btnHideShowStrat').val('Hide Additional Strategies');
            }

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <asp:Panel ID="PanelParms" runat="server">
        <div class="col-md-12 col-sm-12 failureForgotNotification displayNone" id="displayErrorMessage">
            <asp:Label ID="ErrorMessage" runat="server"></asp:Label>
        </div>
        <div class="row">
            <br />
            <div class="col-md-7 col-sm-7 ">
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Customer Name:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerName" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <asp:Label ID="lblCustomerSpouseName" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Date of Birth :
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerDOB" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <asp:Label ID="lblCustomerSpouseDOB" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Full Retirement Age :
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerFRA" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <asp:Label ID="lblCustomerSpouseFRA" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Full Retirement Age Benefit:
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        <asp:Label ID="lblCustomerBenefit" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                        <asp:Label ID="lblCustomerSpouseBenefit" ForeColor="Black" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold ">
                        Marital Status :
                    </div>
                    <div class="col-md-4 col-sm-4 ">
                        Married
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 ">
                    <div class="col-md-5 col-sm-5 fontBold">
                        Annual Cost of Living Increase:
                    </div>
                    <div class="col-md-4 col-sm-4">
                        2.5%
                    </div>
                    <div class="col-md-3 col-sm-3 ">
                    </div>
                </div>
            </div>

            <%--Download, Back To Button--%>
            <%--<div class="col-md-5 col-sm-5">
                <br />
                <%if (Session["AdminID"] != null)
                  {%>
                <asp:Button ID="btnManageCustomer" runat="server" CssClass="button_login "  Text="Back to All Clients" />&nbsp;&nbsp;&nbsp;<asp:Button ID="btnDownloadReport" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download Report" />
                <%}
                  else
                  {
                      if (Session["Demo"] == null)
                      {%>
                <asp:Button ID="BtnChange" runat="server" CssClass="button_login "  Text="Edit Information" />
                <%} %>
                <asp:Button ID="btnExplain" CssClass="button_login" runat="server" Width="163px" Text="Explanation of Strategies" />
                <asp:Button ID="Button2" runat="server" CssClass="button_login" Text="Download Report" />
                <%}%>
            </div>--%>
        </div>
    </asp:Panel>

    <asp:Panel ID="PanelGrids" runat="server">
        <br />
        <br />
        <p class="margileft fontBold text-justify">Please click on the circle below the numbered Strategy to see your benefit numbers for that suggested Strategy.</p>
        <div>
            <br />
            <table class="text-center">
                <tr class="fontLarge">

                    <td runat="server" id="strategy1" class="text-center">
                        <asp:Label ID="lblStrategy1" Text="Strategy 1" Font-Bold="true" runat="server"></asp:Label>
                        <br />
                        <asp:RadioButton ID="rdbtnStrategy1" GroupName="marriedScenario" runat="server" />
                        <br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber1" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Combined Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome1" runat="server" ForeColor="blue" /><br />
                        <p class="marginTop">
                            <asp:Label runat="server" ID="lblRestrictAppIncomeTitle1" Visible="false">Extra Income from <br /> Restricted Application</asp:Label>
                        </p>
                        <asp:Label runat="server" ID="lblRestrictAppIncomeValue1" ForeColor="Blue" Visible="false"></asp:Label>

                    </td>
                    <td runat="server" id="strategy2" class="text-center">
                        <asp:Label ID="lblStrategy2" Text="Strategy 2" Font-Bold="true" runat="server"></asp:Label>
                        <br />
                        <asp:RadioButton ID="rdbtnStrategy2" GroupName="marriedScenario" runat="server" />
                        <br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber2" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Combined Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome2" runat="server" ForeColor="blue" /><br />
                        <p class="marginTop">
                            <asp:Label runat="server" ID="lblRestrictAppIncomeTitle2" Visible="false">Extra Income from <br /> Restricted Application</asp:Label>
                        </p>
                        <asp:Label runat="server" ID="lblRestrictAppIncomeValue2" ForeColor="Blue" Visible="true"></asp:Label>
                    </td>
                    <td runat="server" id="strategy3" class="text-center">
                        <asp:Label ID="lblStrategy3" Text="Strategy 3" Font-Bold="true" runat="server"></asp:Label>
                        <br />
                        <asp:RadioButton ID="rdbtnStrategy3" runat="server" GroupName="marriedScenario" />
                        <br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber3" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Combined Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome3" runat="server" ForeColor="blue" /><br />
                        <p class="marginTop">
                            <asp:Label runat="server" ID="lblRestrictAppIncomeTitle3" Visible="false">Extra Income from <br /> Restricted Application</asp:Label>
                        </p>
                        <asp:Label runat="server" ID="lblRestrictAppIncomeValue3" Visible="false" ForeColor="Blue"></asp:Label>

                    </td>

                    <%--  <%if (Session["ShowButton"].ToString().Equals("true"))
                      { %>
                    <td>
                        <input type="button" class="other-buttons" style="width: 186px; height: 40px; font-size: small !important" id="btnHideShowStrat" value="Show Additional Strategies" onclick="HideShowStrategy();" />
                    </td>
                    <%} %>--%>
                </tr>


            </table>
            <table class="text-center">
                <tr class="fontLarge additonalStrat displayNone" style="margin-left: 50px">

                    <td runat="server" id="strategy4" class="text-center" style="margin-right: 50px">
                        <asp:Label ID="lblStrategy4" Text="Strategy 4" Font-Bold="true" runat="server"></asp:Label>
                        <br />
                        <asp:RadioButton ID="rdbtnStrategy4" runat="server" GroupName="marriedScenario" />
                        <br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber4" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Combined Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome4" runat="server" ForeColor="blue" /><br />
                        <p class="marginTop">
                            <asp:Label runat="server" ID="lblRestrictAppIncomeTitle4" Visible="false">Extra Income from <br /> Restricted Application</asp:Label>
                        </p>
                        <asp:Label runat="server" ID="lblRestrictAppIncomeValue4" Visible="false" ForeColor="Blue"></asp:Label>
                    </td>
                    <td style="width: 50px"></td>
                    <td runat="server" id="strategy5" class="text-center">
                        <asp:Label ID="lblStrategy5" Text="Strategy 5" Font-Bold="true" runat="server"></asp:Label>
                        <br />
                        <asp:RadioButton ID="rdbtnStrategy5" runat="server" GroupName="marriedScenario" />
                        <br />
                        <p>
                            Paid To Wait Amount With<br />
                            This Strategy<br />
                        </p>
                        <asp:Label ID="lblPaidNumber5" runat="server" ForeColor="blue" />
                        <p class="marginTop">
                            Combined Annual Income<br />
                            When Strategy is Complete
                        </p>
                        <asp:Label ID="lblCombinedIncome5" runat="server" ForeColor="blue" /><br />
                        <p class="marginTop">
                            <asp:Label runat="server" ID="lblRestrictAppIncomeTitle5" Visible="false">Extra Income from <br /> Restricted Application</asp:Label>
                        </p>
                        <asp:Label runat="server" ID="lblRestrictAppIncomeValue5" Visible="false" ForeColor="Blue"></asp:Label>
                    </td>


                </tr>
            </table>
            <br />
            <br />
        </div>


        <div id="strategy-Single" class="strategy-container">
            <asp:Panel ID="PanelBestLater1" runat="server">
                <asp:Panel runat="server" ID="ExplainBestLater">
                    <asp:Label ID="LabelBestLater1" runat="server" class="displayNone" Text="Claim Early Claim Late "></asp:Label>
                    <asp:Label ID="lblKeyFigures11" runat="server" Text="Key Benefit Numbers:" CssClass="margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey71" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey81" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey91" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyRestrictIncomFull1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyRestrictIncomFull11" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsFigures11" runat="server" Text="Next Steps:" CssClass="margileft spaceInLblKeysAndSteps" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="stepsage41" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps01" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="Label61" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps11" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps21" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps31" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps141" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <br />
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center> 
               
                     <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                     <asp:Button ID="btnFullDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" /></center>
                <%} %>--%>
                <div class="table-responsive">
                    <br />
                    <asp:GridView ID="GridStrategySingle" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife51" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband51" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblWifeExplanation31" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblHusbandExplanation31" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife41" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband41" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center>  <div class="table-responsive" >
                    <asp:Chart ID="ChartSingle1" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div></center>
            </asp:Panel>
        </div>
        <!-- end Single Strategy -->

        <div id="strategy-Optional" class="strategy-container">
            <asp:Panel ID="PanelBest_Opt" runat="server">
                <asp:Panel ID="pnlText2_Opt" runat="server" class="text-justify  margiright">
                    <asp:Label ID="Label1_Opt" runat="server" Text="Full Retirement & Claim Late" class="displayNone"></asp:Label>
                    <asp:Label ID="lblKeys_Opt" runat="server" Text="Key Benefit Numbers:" CssClass="margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey4_Opt" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey5_Opt" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey6_Opt" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyRestrictIncomBest_Opt" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblSteps_Opt" runat="server" Text="Next Steps:" CssClass="spaceInLblKeysAndSteps margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep4_Opt" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="Label4_Opt" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep5_Opt" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep6_Opt" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep13_Opt" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep7_Opt" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <br />
                <%--<%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                    <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                    <asp:Button ID="btnBestDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" />
                </center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridStrategyOptional" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife0_Opt" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband0_Opt" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>


                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblWifeExplanation1_Opt" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblHusbandExplanation1_Opt" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife_Opt" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband_Opt" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center><div class="table-responsive">
                    <asp:Chart ID="ChartOptional" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div></center>
            </asp:Panel>
        </div>
        <!-- end Strategy Optional-->

        <div id="strategy-1" class="strategy-container">
            <asp:Label ID="LabelBest_1" runat="server" Text="Full Retirement & Claim Late" class="displayNone"></asp:Label>
            <asp:Panel ID="PanelMax_1" runat="server">
                <asp:Panel runat="server" ID="Panel2_1" CssClass="text-justify margiright">
                    <asp:Label ID="lblKeys4_1" runat="server" Font-Bold="true" Font-Names="Arial" CssClass="margileft" ForeColor="Black" Text="Key Benefit Numbers:"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeys1_1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeys2_1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeys3_1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyRestrictIncomMax_1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblSteps4_1" Font-Bold="true" runat="server" CssClass="margileft spaceInKeysAndSteps" Font-Names="Arial" ForeColor="Black" Text="Next Steps:"></asp:Label>

                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="stepsage5_1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep1_1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep2_1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep3_1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStepMax4_1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                     <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                     <asp:Button ID="btnMaxDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" /></center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridStrategy70" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife6_1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband6_1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblWifeExplanation4_1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblHusbandExplanation4_1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife7_1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband7_1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center> 
                    <div class="col-md-12 col-sm-12">
                <div class="table-responsive">
                    <asp:Chart ID="Chart70" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" ToolTip="Value of X: #VALY Value of Y #VALY" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div>
                    </div>
                </center>
            </asp:Panel>
        </div>
        <!-- end Strategy 1-->
        <div id="strategy-2" class="strategy-container">
            <asp:Panel ID="EarlySuspend_2" runat="server">
                <asp:Panel runat="server" ID="pnlText1_2" CssClass="text-justify margiright">
                    <asp:Label ID="LabelEarlySuspend_2" runat="server" Text="Early Claim & Suspend" class="displayNone"></asp:Label>
                    <asp:Label ID="lblKeyFigures_2" runat="server" Font-Bold="true" Font-Names="Arial" CssClass="margileft" ForeColor="Black" Text="Key Benefit Numbers:"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey1_2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey2_2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey3_2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyRestrictIncomEarly_2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsFigures_2" Font-Bold="true" runat="server" CssClass="margileft spaceInKeysAndSteps" Font-Names="Arial" ForeColor="Black" Text="Next Steps:"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep8_2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="Label5_2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep9_2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep10_2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep11_2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep12_2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                     <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                    <asp:Button ID="btnEarlyDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" />
                </center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridStrategyFRA" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife1_2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband1_2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblWifeExplanation2_2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblHusbandExplanation2_2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife3_2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband3_2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center> 
                    <div class="col-md-12 col-sm-12">
                <div class="table-responsive">
                    <asp:Chart ID="ChartFRA" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" ToolTip="Value of X: #VALY Value of Y #VALY" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div>
                    </div>
                </center>
            </asp:Panel>
        </div>
        <%--end strategy-2--%>
        <div id="strategy-3" class="strategy-container">
            <asp:Panel ID="PanelBest_3" runat="server">
                <asp:Panel ID="pnlText2_3" runat="server" class="text-justify  margiright">
                    <asp:Label ID="Label1_3" runat="server" Text="Full Retirement & Claim Late" class="displayNone"></asp:Label>
                    <asp:Label ID="lblKeys_3" runat="server" Text="Key Benefit Numbers:" CssClass="margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey4_3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey5_3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey6_3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyRestrictIncomBest_3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblSteps_3" runat="server" Text="Next Steps:" CssClass="spaceInLblKeysAndSteps margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep4_3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="Label4_3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep5_3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep6_3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep13_3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep7_3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <br />
                <%--<%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                    <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                    <asp:Button ID="btnBestDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" />
                </center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridStrategy62" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife0_3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband0_3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>


                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblWifeExplanation1_3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblHusbandExplanation1_3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife_3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband_3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center><div class="table-responsive">
                    <asp:Chart ID="Chart62" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div></center>
            </asp:Panel>
        </div>
        <!-- end strategy-3 -->


        <!-- Regular Strategies -->
        <div id="strategy-EarlyStrategy" class="strategy-container">
            <asp:Panel ID="EarlySuspend" runat="server">
                <asp:Panel runat="server" ID="pnlText1" CssClass="text-justify margiright">
                    <asp:Label ID="LabelEarlySuspend" runat="server" Text="Early Claim & Suspend" class="displayNone"></asp:Label>
                    <asp:Label ID="lblKeyFigures" runat="server" Font-Bold="true" Font-Names="Arial" CssClass="margileft" ForeColor="Black" Text="Key Benefit Numbers:"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyRestrictIncomEarly" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsFigures" Font-Bold="true" runat="server" CssClass="margileft spaceInKeysAndSteps" Font-Names="Arial" ForeColor="Black" Text="Next Steps:"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep8" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="Label5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep9" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep10" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep11" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep12" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                     <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                    <asp:Button ID="btnEarlyDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" />
                </center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridEarly" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblWifeExplanation2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblHusbandExplanation2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center> 
                    <div class="col-md-12 col-sm-12">
                <div class="table-responsive">
                    <asp:Chart ID="ChartEarly" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" ToolTip="Value of X: #VALY Value of Y #VALY" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div>
                    </div>
                </center>
            </asp:Panel>
        </div>
        <!-- end strategy-1 -->
        <div id="strategy-BestStrategy" class="strategy-container">
            <asp:Panel ID="PanelBest" runat="server">
                <asp:Panel ID="pnlText2" runat="server" class="text-justify  margiright">
                    <asp:Label ID="LabelBest" runat="server" Text="Full Retirement & Claim Late" class="displayNone"></asp:Label>
                    <asp:Label ID="lblKeys" runat="server" Text="Key Benefit Numbers:" CssClass="margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyRestrictIncomBest" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblSteps" runat="server" Text="Next Steps:" CssClass="spaceInLblKeysAndSteps margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="Label4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep13" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep7" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <br />
                <%--<%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                    <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                    <asp:Button ID="btnBestDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" />
                </center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridBest" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife0" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband0" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>


                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblWifeExplanation1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblHusbandExplanation1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center><div class="table-responsive">
                    <asp:Chart ID="ChartBest" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div></center>
            </asp:Panel>
        </div>
        <!-- end strategy-2 -->
        <div id="strategy-FullStrategy" class="strategy-container">
            <asp:Panel ID="PanelBestLater" runat="server">
                <asp:Panel runat="server" ID="Panel1">
                    <asp:Label ID="LabelBestLater" runat="server" class="displayNone" Text="Claim Early Claim Late "></asp:Label>
                    <asp:Label ID="lblKeyFigures1" runat="server" Text="Key Benefit Numbers:" CssClass="margileft" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey7" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey8" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey9" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyRestrictIncomFull" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblStepsFigures1" runat="server" Text="Next Steps:" CssClass="margileft spaceInLblKeysAndSteps" Font-Bold="true" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="stepsage4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps0" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="Label6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblSteps14" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <br />
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center> 
               
                     <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                     <asp:Button ID="btnFullDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" /></center>
                <%} %>--%>
                <div class="table-responsive">
                    <br />
                    <asp:GridView ID="GridBestLater" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblWifeExplanation3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblHusbandExplanation3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center>  <div class="table-responsive" >
                    <asp:Chart ID="ChartBestLater" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div></center>
            </asp:Panel>
        </div>
        <!-- end strategy-4 -->
        <div id="strategy-MaxStrategy" class="strategy-container">
            <asp:Panel ID="PanelMax" runat="server">
                <asp:Panel runat="server" ID="Panel2" CssClass="text-justify margiright">
                    <asp:Label ID="lblKeys4" runat="server" Font-Bold="true" Font-Names="Arial" CssClass="margileft" ForeColor="Black" Text="Key Benefit Numbers:"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeys1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeys2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeys3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKeyRestrictIncomMax" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="lblSteps4" Font-Bold="true" runat="server" CssClass="margileft spaceInKeysAndSteps" Font-Names="Arial" ForeColor="Black" Text="Next Steps:"></asp:Label>

                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="stepsage5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStepMax4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                     <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                     <asp:Button ID="btnMaxDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" /></center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridMax" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblWifeExplanation4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblHusbandExplanation4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteWife7" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNoteHusband7" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center> 
                    <div class="col-md-12 col-sm-12">
                <div class="table-responsive">
                    <asp:Chart ID="ChartMax" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" ToolTip="Value of X: #VALY Value of Y #VALY" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div>
                    </div>
                </center>
            </asp:Panel>
        </div>
        <!-- Regular Strategies End-->

        <%--Addtional Strategies--%>

        <%-- Start Strategy-1--%>
        <div id="strategy-Additional1" class="strategy-container">
            <asp:Panel ID="PanelAdditional1" runat="server">
                <asp:Panel runat="server" ID="Panel4" CssClass="text-justify margiright">
                    <asp:Label ID="lblKeyFiguresAdd1" runat="server" Font-Bold="true" Font-Names="Arial" CssClass="margileft" ForeColor="Black" Text="Key Benefit Numbers:"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd1Key1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd1Key2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd1Key3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd1Key4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd1RestrictKey1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd1RestrictKey2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="Label9" Font-Bold="true" runat="server" CssClass="margileft spaceInKeysAndSteps" Font-Names="Arial" ForeColor="Black" Text="Next Steps:"></asp:Label>

                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd1Step1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd1Step2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd1Step3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd1Step4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd1Step5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd1Step6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                     <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                     <asp:Button ID="btnMaxDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" /></center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridAdd1" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblAdd1NoteHusb1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblAdd1NoteWife1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblAdd1NoteHusb2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblAdd1NoteWife2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblAdd1NoteHusb3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblAdd1NoteWife3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center> 
                    <div class="col-md-12 col-sm-12">
                <div class="table-responsive">
                    <asp:Chart ID="ChartAdd1" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" ToolTip="Value of X: #VALY Value of Y #VALY" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div>
                    </div>
                </center>
            </asp:Panel>
        </div>
        <%--End Strategy-1--%>

        <%--Start Strategy-2--%>
        <div id="strategy-Additional2" class="strategy-container">
            <asp:Panel ID="Panel3" runat="server">
                <asp:Panel runat="server" ID="Panel5" CssClass="text-justify margiright">
                    <asp:Label ID="lblKeyFiguresAdd2" runat="server" Font-Bold="true" Font-Names="Arial" CssClass="margileft" ForeColor="Black" Text="Key Benefit Numbers:"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd2Key1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd2Key2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd2Key3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd2Key4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd2RestrictKey1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd2RestrictKey2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="Label10" Font-Bold="true" runat="server" CssClass="margileft spaceInKeysAndSteps" Font-Names="Arial" ForeColor="Black" Text="Next Steps:"></asp:Label>

                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd2Step1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd2Step2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd2Step3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd2Step4" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd2Step5" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblAdd2Step6" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                     <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                     <asp:Button ID="btnMaxDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" /></center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridAdd2" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblAdd2NoteHusb1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblAdd2NoteWife1" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblAdd2NoteHusb2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblAdd2NoteWife2" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblAdd2NoteHusb3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblAdd2NoteWife3" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center> 
                    <div class="col-md-12 col-sm-12">
                <div class="table-responsive">
                    <asp:Chart ID="ChartAdd2" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" ToolTip="Value of X: #VALY Value of Y #VALY" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div>
                    </div>
                </center>
            </asp:Panel>
        </div>
        <%--End Strategy-2--%>


        <%--Addtional Strategies End--%>

        <%--New Strategies if spouse no claims their benefit--%>

        <div id="strategy-NoClaim62" class="strategy-container">
            <asp:Panel ID="Panel6" runat="server">
                <asp:Panel runat="server" ID="Panel7" CssClass="text-justify margiright">
                    <asp:Label ID="lblKeyFiguresAddNoClaim62" runat="server" Font-Bold="true" Font-Names="Arial" CssClass="margileft" ForeColor="Black" Text="Key Benefit Numbers:"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey1NoClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey2NoClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey3NoClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey4NoClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey5NoClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey6NoClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="Label13" Font-Bold="true" runat="server" CssClass="margileft spaceInKeysAndSteps" Font-Names="Arial" ForeColor="Black" Text="Next Steps:"></asp:Label>

                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep1NoClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep2NoClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep3NoClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep4NoClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep5NoClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep6NoClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                     <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                     <asp:Button ID="btnMaxDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" /></center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridNoClaim62" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNote1HusbClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNote1WifeClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNote2HusbClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNote2WifeClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNote3HusbClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNote3WifeClaim62" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center> 
                    <div class="col-md-12 col-sm-12">
                <div class="table-responsive">
                    <asp:Chart ID="ChartNoClaim62" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" ToolTip="Value of X: #VALY Value of Y #VALY" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div>
                    </div>
                </center>
            </asp:Panel>
        </div>

        <div id="strategy-NoClaim66" class="strategy-container">
            <asp:Panel ID="Panel8" runat="server">
                <asp:Panel runat="server" ID="Panel9" CssClass="text-justify margiright">
                    <asp:Label ID="Label1" runat="server" Font-Bold="true" Font-Names="Arial" CssClass="margileft" ForeColor="Black" Text="Key Benefit Numbers:"></asp:Label>
                    <div class="text-justify margin200 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey1NoClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey2NoClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey3NoClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey4NoClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey5NoClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblKey6NoClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                    <br />
                    <asp:Label ID="Label2" Font-Bold="true" runat="server" CssClass="margileft spaceInKeysAndSteps" Font-Names="Arial" ForeColor="Black" Text="Next Steps:"></asp:Label>

                    <div class="text-justify margin100 margiright">
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep1NoClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep2NoClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep3NoClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep4NoClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep5NoClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                        <div class="spaceInKeysAndSteps text-justify">
                            <asp:Label ID="lblStep6NoClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <%-- <%if (Session["AdvisorCustomer"] == null)
                  {%>
                <center>
                     <p class="fontLarge fontColorBlack">Click Here to have the Strategies downloaded to your computer</p>
                     <asp:Button ID="btnMaxDownload" runat="server" CssClass="button_login " OnClick="PrintAll_Click" Text="Download" /></center>
                <%} %>--%>
                <div class="table-responsive">
                    <asp:GridView ID="GridNoClaim66" runat="server">
                        <RowStyle BackColor="#F9F9F9" />
                    </asp:GridView>
                </div>
                <div class="text-justify margiright">
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNote1HusbClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNote1WifeClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNote2HusbClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNote2WifeClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNote3HusbClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>
                    <div class="spaceInKeysAndSteps text-justify margileft">
                        <asp:Label ID="lblNote3WifeClaim66" runat="server" Font-Names="Arial" ForeColor="Black"></asp:Label>
                    </div>

                </div>
                <br />
                <center> 
                    <div class="col-md-12 col-sm-12">
                <div class="table-responsive">
                    <asp:Chart ID="ChartNoClaim66" runat="server" BorderlineDashStyle="Dash">
                        <Titles>
                            <asp:Title Alignment="TopCenter" TextStyle="Shadow" BackColor="white" ForeColor="#595a59" BorderColor="White" Font="Times New Roman, 14.25pt, style=Bold" BorderDashStyle="NotSet" BorderWidth="0" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default" LegendStyle="Row" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" Color="#747474" ChartType="StackedColumn" ToolTip="Value of X: #VALY Value of Y #VALY" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                            <asp:Series Name="Series2" Color="#b85f5b" ChartType="StackedColumn" XValueMember="10" YValueMembers="5" BorderWidth="0" CustomProperties="DrawingStyle=Default, PointWidth=0.6"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                        </ChartAreas>
                    </asp:Chart>
                </div>
                    </div>
                </center>
            </asp:Panel>
        </div>

        <%--End New Strategies if spouse no claims their benefit--%>

        <div class="strategy-container displayNone">
            <asp:Panel ID="PanelNowLater" runat="server">
                <div class="clearfix">
                    <div class="col-md-10 column">
                        <h2>
                            <asp:Label ID="LabelNowLater" runat="server" Text="Claim Early Claim Late"></asp:Label></h2>
                    </div>
                    <div class="col-md-2 column">
                        <asp:Button ID="Button4" runat="server" CssClass="button_login downloadbtn" Text="Download" />
                    </div>
                </div>
                <div class="table-responsive">
                    <asp:GridView ID="GridNowLater" runat="server" ForeColor="Black"></asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="ExplainNowLater">
                <p>
                    <asp:Label ID="StrategyNowLater" runat="server">
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="NoteNowLater" runat="server"></asp:Label>
                </p>
            </asp:Panel>
        </div>
        <!-- end -->
        <div class="strategy-container displayNone">
            <asp:Panel ID="PanelLater" runat="server">
                <h2>
                    <asp:Label ID="LabelLater" runat="server" Text="Full Retirement & Claim Late"></asp:Label></h2>
                <div class="button-row">
                </div>
                <div class="table-responsive">
                    <asp:GridView ID="GridLater" runat="server" ForeColor="Black"></asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="ExplainLater">
                <p>
                    <asp:Label ID="StrategyLater" runat="server">
          In this strategy the spouse with the lower benefit starts social security at full
          retirement age. The spouse with the higher benefit uses the spousal benefit to get
          some additional income while their work history benefit grows until age 70.
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="NoteLater" runat="server"></asp:Label>
                    <br />
                    <br />
                    If you compare this strategy to the Claim Now, Claim More Later Stratege above,
          you will see that the age 70 benefit is higher while the income prior to age 70
          is lower. This strategy can be better in the long run if you can afford to defer
          the income.
                </p>
            </asp:Panel>
        </div>
        <!-- end -->
        <div class="strategy-container displayNone">
            <asp:Panel ID="PanelSuspend" runat="server">
                <h2>
                    <asp:Label ID="LabelSuspend" runat="server" Text="Early Claim and Suspend"></asp:Label></h2>
                <!--<asp:HyperLink ID="HyperLinkSuspend" runat="server">  (Show)</asp:HyperLink>-->
                <div class="button-row">
                </div>
                <div class="table-responsive">
                    <asp:GridView ID="GridSuspend" runat="server" ForeColor="Black"></asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="ExplainSuspend">
                <p>
                    <asp:Label ID="StrategySuspend" runat="server">
            In this strategy the spouse with the lower benefit starts social security as soon as they are eligable. The spouse with the higher benefit claims and suspends so that the spousal benefit can be used while their work history benefit grows until age 70.
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="NoteSuspend" runat="server"></asp:Label>
                </p>
            </asp:Panel>
        </div>
        <!-- end -->
        <div class="strategy-container displayNone">
            <asp:Panel ID="PanelStandard" runat="server">
                <h2>
                    <asp:Label ID="LabelStandard" runat="server" Text="Compromise Strategy"></asp:Label></h2>
                <!--<asp:HyperLink ID="HyperLinkStandard" runat="server">  (Show)</asp:HyperLink>-->
                <div class="button-row">
                </div>
                <div class="table-responsive">
                    <asp:GridView ID="GridStandard" runat="server" ForeColor="Black"></asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="ExplainStandard">
                <p>
                    <asp:Label ID="StrategyStandard" runat="server">
          In this strategy both husband and wife start social security at full retirement age.
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="NoteStandard" runat="server"></asp:Label>
                    <br />
                    <br />
                    This strategy uses the standard claim at full retirement age (usually 66). It is
          better than claiming as soon as possible because the age 70 benefit is higher and
          the survivor's benefit is higher. However, the other strategies (Claim Now Claim
          More Later, Claim and Suspend, etc.) offer much better age 70 and survivor benefits.
                </p>
            </asp:Panel>
        </div>
        <!-- end -->
        <div class="strategy-container displayNone">
            <asp:Panel ID="PanelLatest" runat="server">
                <h2>
                    <asp:Label ID="LabelLatest" runat="server" Font-Bold="True" Font-Names="Arial" Text="Claim as Late as Possible" ForeColor="Black"></asp:Label></h2>
                <!--<asp:HyperLink ID="HyperLinkLatest" runat="server">  (Show)</asp:HyperLink>-->
                <div class="button-row">
                </div>
                <div class="table-responsive">
                    <asp:GridView ID="GridLatest" runat="server" ForeColor="Black"></asp:GridView>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="ExplainLatest">
                <p>
                    <asp:Label ID="StrategyLatest" runat="server">
          In this strategy both husband and wife start social security at age 70.
                    </asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="NoteLatest" runat="server"></asp:Label>
                </p>
            </asp:Panel>
        </div>
        <!-- end -->
        <br />
        <br />

        <div class="col-md-12 col-sm-12">
            <br />
            <asp:TextBox ID="BeneHusband" Visible="false" runat="server" CssClass="input-text textEntry form-control" MaxLength="7" TabIndex="1"></asp:TextBox>
            <asp:TextBox ID="BirthWifeMM" runat="server" MaxLength="2" Width="20px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
            <asp:TextBox ID="BirthWifeDD" runat="server" MaxLength="2" Width="20px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
            <asp:TextBox ID="birthWifeTempYYYY" runat="server" MaxLength="4" Width="20px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
            <asp:TextBox ID="BirthWifeYYYY" runat="server" MaxLength="4" Width="80px" Visible="false" CssClass="textEntry"></asp:TextBox>
            <asp:TextBox ID="Your_Name" runat="server" Width="80px" Visible="false" CssClass="textEntry"></asp:TextBox>

            <asp:TextBox ID="Spouse_Name" runat="server" Width="80px" Visible="false" CssClass="textEntry"></asp:TextBox>
            <asp:TextBox ID="BeneWife" Visible="false" runat="server" CssClass="input-text textEntry form-control" MaxLength="7" TabIndex="2" ValidationGroup="RegisterUserValidationGroup"></asp:TextBox>
            <asp:TextBox Visible="false" ID="BirthHusbandMM" MaxLength="2" runat="server" Width="20px" CssClass="input-text textEntry"></asp:TextBox>
            <asp:TextBox ID="BirthHusbandDD" Visible="false" MaxLength="2" runat="server" Width="20px" CssClass="input-text textEntry"></asp:TextBox>
            <asp:TextBox ID="BirthHusbandTempYYYY" Visible="false" runat="server" Width="20px" CssClass="input-text textEntry"></asp:TextBox>
            <asp:TextBox ID="BirthHusbandYYYY" MaxLength="4" runat="server" Width="80px" Visible="false" CssClass="input-text textEntry"></asp:TextBox>
        </div>
        <br />
        <br />

        <a href="../FrmDisclaimer.aspx">Disclaimer </a>
    </asp:Panel>

    <div id="subscription" class="testmodalPopup">
        <center>  <div id="plans" class="plan-contentnew">
          <br />
          <asp:Label ID="MaxCumm"  class="subspopuplabelsColor" runat="server"></asp:Label>
          <br />
          <h2 class="subspopupText">While growing your Social Security Check to the largest amount possible! <br />
               <br />  
              Find out how, for just 39.95 dollars!
                <br />
                <br />
                <asp:Button ID="btnTrial" runat="server" Text="Subscribe" CssClass="button_login"  />&nbsp;&nbsp;<asp:Button ID="CloseWindow" runat="server" CssClass="button_login" Text="Cancel" />
            </h2>
          <br />
        </div></center>
        <asp:Label ID="ErrorMessagelable" runat="server" CssClass="failureForgotNotification" Visible="False"></asp:Label>
    </div>
</asp:Content>
