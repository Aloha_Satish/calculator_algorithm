﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Calculate
{
    /// <summary>
    /// Summary description for DownloadSampleReport
    /// </summary>
    public class DownloadSampleReport : IHttpHandler
    {
        /// <summary>
        /// Used to download Sample report
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            string strFilePath = HttpContext.Current.Server.MapPath(@"\Sample Report\Sample Report - Paid to Wait Calculator.pdf");
            HttpResponse response = HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = "Application/pdf";
            response.AddHeader("Content-Disposition", "attachment; filename=Sample_Report_Paid_to_Wait_Calculator.pdf;");
            response.TransmitFile(strFilePath);
            response.Flush();
            response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}