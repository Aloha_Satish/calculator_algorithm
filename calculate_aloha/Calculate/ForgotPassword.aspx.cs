﻿using CalculateDLL;
using Symbolics;
using System;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;

namespace Calculate.Account
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        #region Events

        /// <summary>
        /// code called when page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                txtEmail.Focus();
                Session["txtText"] = txtEmail.Text;
                //lblUserExist.Visible = false;
                if (!IsPostBack)
                {
                    txtEmail.Focus();
                    Session["SavedText"] = "1";
                    txtEmail.Text = string.Empty;
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "ClearText();", true);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorForgotPassword, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblUserLoginError.Text = Constants.ErrorForgotPassword + MethodBase.GetCurrentMethod() + ex.Message;
                lblUserLoginError.Visible = true;
            }
        }

        /// <summary>
        /// code to send the mail when forgot password button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                //session variable taken because on reload on page same pop up should not be shown
                //if (!Session["SavedText"].ToString().Equals(Session["txtText"].ToString()))
                //{
                    sendPasswordRecoveryEmail();
                //}
                //else
                //{
                    //txtEmail.Text = string.Empty;
                    //txtEmail.Focus();
                    //ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "ClearText();", true);
                //}
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorForgotPassword, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblUserLoginError.Text = Constants.EnterAgainEmailID;
                lblUserLoginError.Visible = true;

            }
        }


        /// <summary>
        /// code to send the mail when forgot password button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GoHome(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectToLogin, false);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorForgotPassword, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblUserLoginError.Text = Constants.EnterAgainEmailID;
                lblUserLoginError.Visible = true;
            }
        }
        #endregion Events

        #region Methods

        /// <summary>
        /// code for sending mail to user the password
        /// </summary>
        public void sendPasswordRecoveryEmail()
        {
            try
            {
                Customers customer = new Customers();
                //DataTable dtid=customer.getCustomerDetails();
                sendMailStatus.Visible = false;
                /* Check if specified email exists in database or not */
                Users objUsers = new Users();
                DataTable dtUserDetails = objUsers.getUserDetails(txtEmail.Text);
                if (dtUserDetails.Rows.Count == 0)
                {
                    string message = Constants.InvalidEmailID;
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append("<script type = 'text/javascript'>");
                    sb.Append("window.onload=function(){");
                    sb.Append("alert('");
                    sb.Append(message);
                    sb.Append("')};");
                    sb.Append("</script>");
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
                    //lblUserExist.Visible = true;
                    //lblUserExist.Text = " Invalid Email Address";
                    txtEmail.Focus();
                    Session["SavedText"] = txtEmail.Text;
                    txtEmail.Text = string.Empty;
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "ClearText();", true);
                }
                else
                {
                    DataTable userDetails = objUsers.getUserDetails(txtEmail.Text.ToString());
                    String cid = string.Empty;// userDetails.Rows[Constants.zero][Constants.SessionUserId].ToString();
                    //lblUserExist.Visible = false;
                    /* Logic for sending mail */
                    var toAddress = txtEmail.Text.ToString();
                    // Passing the values and make a email format to display
                    string subject = Constants.ForgotPwdSubject;
                    //code to generate the random password hash to be sent to user
                    string sPasswordGenerated = GetRandomAlphaNumeric();
                    string body = Constants.ForgotPwdText + (Encryption.Encrypt(sPasswordGenerated));
                    body += Constants.ForgotPwdBody;
                    Boolean statusMail = MailHelper.sendMail(toAddress, subject, body, cid);
                    if (statusMail)
                    {
                        Customers objCustomers = new Customers();
                        objCustomers.Email = txtEmail.Text;
                        objCustomers.PasswordResetFlag = Constants.FlagYes;
                        objCustomers.Password = Encryption.Encrypt(sPasswordGenerated);
                        objCustomers.updateCustomerPasswordStatus(objCustomers);
                        sendMailStatus.Visible = true;
                        sendMailStatus.Text = Constants.ForgotPwdStatusSuccess;
                        sendMailStatus.CssClass = "setSuccessPassword";
                        txtEmail.Focus();
                        Session["SavedText"] = txtEmail.Text;
                        txtEmail.Text = string.Empty;
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "ClearText();", true);
                    }
                    else
                    {
                        sendMailStatus.Visible = true;
                        sendMailStatus.Text = Constants.ForgotPwdStatusFailure;
                        sendMailStatus.CssClass = "setFailurePassword";
                        txtEmail.Focus();
                        Session["SavedText"] = txtEmail.Text;
                        txtEmail.Text = string.Empty;
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptdisplaySubscriptionPopUp, "ClearText();", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorForgotPassword, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblUserLoginError.Text = Constants.EnterAgainEmailID;
                lblUserLoginError.Visible = true;
            }
        }

        /// <summary>
        /// code to get random password hash to be sent to user when forgot password
        /// </summary>
        /// <returns></returns>
        public static string GetRandomAlphaNumeric()
        {
            try
            {
                Random random = new Random();
                 var chars = "abcdefghijklmnopqrstuvwxyz0123456789";
                return new string(chars.Select(c => chars[random.Next(chars.Length)]).Take(8).ToArray());
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorForgotPassword, MethodBase.GetCurrentMethod(), ex.ToString()));
                return null;
            }
        }

        /// <summary>
        /// function when cancel button is clicked to redirect to login screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(Constants.RedirectLogin, true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorForgotPassword, MethodBase.GetCurrentMethod(), ex.ToString()));
                lblUserLoginError.Text = Constants.TryAgain;
                lblUserLoginError.Visible = true;
            }
        }
        #endregion Methods


    }
}