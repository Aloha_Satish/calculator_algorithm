﻿using CalculateDLL;
using Symbolics;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Calculate
{
    public partial class NewUISite : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ValidateUserOnPageLoad();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }


        /// <summary>
        /// code when logout button is clicked
        /// session made null and the banner url again sent to the home page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Logout(object sender, EventArgs e)
        {
            try
            {
                LogOutApplication();
            }
            catch (Exception ex)
            {
                ClearSession();
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }



        #region Methods

        /// <summary>
        /// Abandon Sessions
        /// </summary>
        public void LogOutApplication()
        {
            try
            {
                if (HttpContext.Current.Session[Constants.BannerImage] != null)
                {
                    if (HttpContext.Current.Session[Constants.BannerImage].ToString().Equals("Y"))
                        HttpContext.Current.Response.Redirect(Constants.RedirectToLogin);
                    else
                        HttpContext.Current.Response.Redirect(Constants.RedirectToLogin);
                }
                else
                {
                    HttpContext.Current.Response.Redirect(Constants.RedirectToLogin);
                }
                ClearSession();
            }
            catch (Exception ex)
            {
                ClearSession();
                ErrorLog.WriteError(Constants.ErrorSiteMaster + "Logout() - " + ex.ToString());

            }
        }

        /// <summary>
        /// code to clear the sessions
        /// </summary>
        private void ClearSession()
        {
            try
            {
                HttpContext.Current.Session.Clear();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(Constants.ErrorSiteMaster + "ClearSession() - " + ex.ToString());
            }
        }



        /// <summary>
        /// Validate User
        /// </summary>
        public void ValidateUserOnPageLoad()
        {
            try
            {
                //GenerateMenus();
                if (Session[Constants.SessionRole] != null)
                {
                    string url = Request.Url.AbsoluteUri;
                    DataTable dtCustomerDetails = new DataTable();
                    // Theme Implementation
                    if (Session[Constants.SessionRole].ToString().Equals(Constants.Institutional))
                    {
                        Theme objTheme = new CalculateDLL.Theme();
                        DataTable dtTheme = objTheme.getThemeDetailsByID(Session[Constants.SessionAdminId].ToString());
                        string js = String.Empty;
                        if (dtTheme.Rows.Count > Constants.zero)
                        {
                            string headerColor = dtTheme.Rows[Constants.zero][Constants.BackGroundHeaderColor].ToString();
                            string backgroundColor = dtTheme.Rows[Constants.zero][Constants.BackGroundBodyColor].ToString();
                            string footerColor = dtTheme.Rows[Constants.zero][Constants.BackGroundFooterColor].ToString();
                            byte[] imageBytes = (byte[])dtTheme.Rows[Constants.zero][Constants.InstitutionLogo];
                            // heading.InnerText = dtTheme.Rows[Constants.zero][Constants.InstitutionName].ToString();
                            // heading.Attributes.Remove(Constants.classCss);
                            // heading.Attributes.Add(Constants.classCss, "textsize headingettheme");
                            string src = string.Empty;
                            if (imageBytes != null)
                                src = Constants.ImgUrl + Convert.ToBase64String(imageBytes);
                            js = Constants.changeBackground + footerColor + "','" + headerColor + "','" + backgroundColor + "','" + src + "');";
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, js, true);
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveLogoAttrName, Constants.ClientScripRemoveLogoAttr, true);

                        Institutional objInstitutional = new Institutional();
                        dtCustomerDetails = objInstitutional.getInstitutionalDetailsByID(Session[Constants.SessionAdminId].ToString());
                        //if (!dtCustomerDetails.Rows[0]["isSubscription"].ToString().Equals("Y") && !url.Contains("WelcomeAdmin.aspx"))
                        //    Response.Redirect(Constants.RedirectWelcomeAdmin, false);
                    }
                    else if (Session[Constants.SessionRole].ToString().Equals(Constants.Advisor))
                    {
                        Theme objTheme = new CalculateDLL.Theme();
                        string InstitutionId = objTheme.isThemeExistForAdvisor(Session[Constants.SessionAdminId].ToString());
                        if (InstitutionId != string.Empty)
                        {
                            DataTable dtTheme = objTheme.getThemeDetailsByID(InstitutionId);
                            if (dtTheme.Rows.Count > Constants.zero)
                            {
                                string headerColor = dtTheme.Rows[Constants.zero][Constants.BackGroundHeaderColor].ToString();
                                string backgroundColor = dtTheme.Rows[Constants.zero][Constants.BackGroundBodyColor].ToString();
                                string footerColor = dtTheme.Rows[Constants.zero][Constants.BackGroundFooterColor].ToString();
                                byte[] imageBytes = (byte[])dtTheme.Rows[Constants.zero][Constants.InstitutionLogo];
                                //heading.InnerText = dtTheme.Rows[Constants.zero][Constants.InstitutionName].ToString();
                                // heading.Attributes.Remove(Constants.classCss);
                                //heading.Attributes.Add(Constants.classCss, "textsize headingettheme");
                                string src = Constants.ImgUrl + Convert.ToBase64String(imageBytes);
                                string js = Constants.changeBackground + footerColor + "','" + headerColor + "','" + backgroundColor + "','" + src + "');";
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, js, true);
                            }
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveLogoAttrName, Constants.ClientScripRemoveLogoAttr, true);
                    }
                    else if (Session[Constants.SessionRole].ToString().Equals(Constants.Customer))
                    {
                        Theme objTheme = new CalculateDLL.Theme();
                        string InstitutionId = objTheme.isThemeExistForCustomer(Session[Constants.SessionUserId].ToString());
                        if (InstitutionId != string.Empty)
                        {
                            DataTable dtTheme = objTheme.getThemeDetailsByID(InstitutionId);
                            if (dtTheme.Rows.Count > Constants.zero)
                            {
                                string headerColor = dtTheme.Rows[Constants.zero][Constants.BackGroundHeaderColor].ToString();
                                string backgroundColor = dtTheme.Rows[Constants.zero][Constants.BackGroundBodyColor].ToString();
                                string footerColor = dtTheme.Rows[Constants.zero][Constants.BackGroundFooterColor].ToString();
                                byte[] imageBytes = (byte[])dtTheme.Rows[Constants.zero][Constants.InstitutionLogo];
                                //heading.InnerText = dtTheme.Rows[Constants.zero][Constants.InstitutionName].ToString();
                                //heading.Attributes.Remove(Constants.classCss);
                                // heading.Attributes.Add(Constants.classCss, "textsize headingettheme");
                                string src = Constants.ImgUrl + Convert.ToBase64String(imageBytes);
                                string js = Constants.changeBackground + footerColor + "','" + headerColor + "','" + backgroundColor + "','" + src + "');";
                                ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScriptSuccessMessageName, js, true);
                            }
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveLogoAttrName, Constants.ClientScripRemoveLogoAttr, true);
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveLogoAttrName, Constants.ClientScripRemoveLogoAttr, true);
                }
                else
                    ScriptManager.RegisterStartupScript(this, typeof(string), Constants.ClientScripRemoveLogoAttrName, Constants.ClientScripRemoveLogoAttr, true);
                SetBanner();
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }


        /// <summary>
        /// Used to set banner image
        /// </summary>
        private void SetBanner()
        {
            if (Session[Constants.BannerImage].ToString().Equals(Constants.SessionBanner))
            {
                string strUrlName = Request.Url.Host.ToLower();
                Theme themes = new Theme();
                DataTable dtTheme = themes.GetBannerDetailsByDomainName(strUrlName);
                if (dtTheme.Rows.Count > 0)
                {
                    byte[] imageBytes = (byte[])dtTheme.Rows[Constants.zero][Constants.BannerImage];
                    Globals.Instance.ChangeBanner = Constants.ImgUrl + Convert.ToBase64String(imageBytes);
                }
                else
                {
                    Globals.Instance.ChangeBanner = Constants.DefaultBanner;
                }
            }
            else
            {
                Globals.Instance.ChangeBanner = Constants.DefaultBanner;
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), "JsFunc", String.Format("ChangeBanner('{0}');", Globals.Instance.ChangeBanner), true);

            try
            {
                if (Session[Constants.BannerImage].ToString().Equals("Y"))
                    Globals.Instance.ChangeBanner = "/Images/BannerGrand.jpg";
                else
                    Globals.Instance.ChangeBanner = "/Images/BannerRegular.png";

                Page.ClientScript.RegisterStartupScript(this.GetType(), "JsFunc", String.Format("ChangeBanner('{0}');", Globals.Instance.ChangeBanner), true);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteError(string.Format("{0} - {1} - {2}", Constants.ErrorSiteMaster, MethodBase.GetCurrentMethod(), ex.ToString()));
            }
        }
        #endregion
    }
}