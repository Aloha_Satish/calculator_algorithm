function showOneStrategy(sFirstStartegy) {
    $("#ctl00_ctl00_MainContent_BodyContent_strategy1").css('display', 'none');
    $("#ctl00_ctl00_MainContent_BodyContent_strategy3").css('display', 'none');
    $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy2").text("Strategy 1");
    $("#ctl00_ctl00_MainContent_BodyContent_rdbtnStrategy2").click(function () {
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy1").css("color", "black");
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy2").css("color", "brown");
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy3").css("color", "black");
        $("#strategy-" + sFirstStartegy).css('display', 'block');
    });


    $("span:empty").css("display", "none");

}
function showTwoStrategy(sFirstStartegy, sThirdStartegy) {
    $("#ctl00_ctl00_MainContent_BodyContent_strategy2").css('display', 'none');
    $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy3").text("Strategy 2");
    $("#ctl00_ctl00_MainContent_BodyContent_rdbtnStrategy1").click(function () {
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy1").css("color", "brown");
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy2").css("color", "black");
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy3").css("color", "black");
        $('.strategy-container').css('display', 'none');
        $('#strategy-' + sFirstStartegy).css('display', 'block');
    });
    $("#ctl00_ctl00_MainContent_BodyContent_rdbtnStrategy3").click(function () {
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy1").css("color", "black");
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy2").css("color", "black");
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy3").css("color", "brown");
        $('.strategy-container').css('display', 'none');
        $('#strategy-' + sThirdStartegy).css('display', 'block');
    });

    $("span:empty").css("display", "none");

}

function showThreeStrategy(sFirstStartegy, sSecondStartegy, sThirdStartegy) {
    $("#ctl00_ctl00_MainContent_BodyContent_rdbtnStrategy1").click(function () {
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy1").css("color", "brown");
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy2").css("color", "black");
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy3").css("color", "black");
        $(".strategy-container").css('display', 'none');
        $("#strategy-" + sFirstStartegy).css('display', 'block');
    });
    $("#ctl00_ctl00_MainContent_BodyContent_rdbtnStrategy2").click(function () {
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy1").css("color", "black");
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy2").css("color", "brown");
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy3").css("color", "black");
        $(".strategy-container").css('display', 'none');
        $("#strategy-" + sSecondStartegy).css('display', 'block');
    });

    $("#ctl00_ctl00_MainContent_BodyContent_rdbtnStrategy3").click(function () {
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy1").css("color", "black");
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy2").css("color", "black");
        $("#ctl00_ctl00_MainContent_BodyContent_lblStrategy3").css("color", "brown");
        $(".strategy-container").css('display', 'none');
        $("#strategy-" + sThirdStartegy).css('display', 'block');
    });

    $("span:empty").css("display", "none");

}




















